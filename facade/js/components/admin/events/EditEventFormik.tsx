import React from 'react';
import * as Yup from 'yup';
import moment from 'moment';
import 'moment-timezone';

import { connect } from 'react-redux';
import { Formik } from 'formik';
import isEqual from 'lodash/isEqual';

import { Button } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

import {
	TextFieldFormik,
	RichTextEditor,
	InlineDatePickerFormik,
	AutocompletePostcodeFormik,
	ImageDropzoneFormik,
	SelectFieldFormik,
	SwitchFieldFormik,
} from '../../inputs';

import Pill from './../../general/Pill';

import { ValidationEvent } from './../../../helpers/validations';
import { ApiService, withServices } from '../../../services';
import {
	IApiSituation,
	IApiEvent,
	IStaticStates,
	IApiStaff,
	//IApiTopic
} from '../../../../js/interfaces';
import { IStoreState } from './../../../redux/store';

import { ISelectOption } from '../../../interfaces';
import { eventTypes } from './CreateEventFormik';
const ausZoneArr = moment.tz.zonesForCountry('AU');
const nzZoneArr = moment.tz.zonesForCountry('NZ');
const timeZones: ISelectOption[] = [];
ausZoneArr &&
	ausZoneArr.map(t => {
		const obj = { label: '', value: '' };
		obj['label'] = t;
		obj['value'] = t;
		timeZones.push(obj);
		return obj;
	});

nzZoneArr &&
	nzZoneArr.map((t: string) => {
		const obj = { label: '', value: '' };
		const result = t.replace('Pacific', 'Newzealand');
		obj['label'] = result;
		obj['value'] = t;
		timeZones.push(obj);
		return obj;
	});

// const eventTypes: ISelectOption[] = [
// 	{ label: 'Drop In', value: 'DropIn' },
// 	{ label: 'Overnight Program', value: 'OvernightProgram' },
// 	{ label: 'Recreational Event', value: 'RecreationalEvent' },
// 	{ label: 'Recreational Activity', value: 'RecreationalActivity' },
// 	{ label: 'Program', value: 'Program' },
// 	{ label: 'Series', value: 'Series' },
// 	{ label: 'Digital', value: 'Digital' },
// 	{ label: 'Other', value: 'Other' },
// ];
interface IProps {
	isSubmitting: boolean;
	eventData: IApiEvent;
	states: IStaticStates[];
	situations: IApiSituation[];
	apiService: ApiService;
	onSubmit(values: any): void;
	onDuplicate(values: any): void;
	dispatch(action: any): void;
	onDelete(event?: IApiEvent): void;
	staffs: IApiStaff[];
	//topics:IApiTopic[],
}

interface IState {
	activeUserSituations: string[];
	activeState: string;
	headerImageUploading: boolean;
	staffOptions: ISelectOption[];
	//topicOptions : ISelectOption[];
}
enum IDs {
	title = 'title',
	event_type = 'event_type',
	event_admin = 'event_admin',
	email_address = 'email_address',
	description = 'description',
	image = 'image',
	situations = 'situations',
	last_joining_date = 'last_joining_date',
	start_date = 'start_date',
	start_date_time = 'start_date_time',
	end_date = 'end_date',
	end_date_time = 'end_date_time',
	event_time_zone = 'event_time_zone',
	states = 'states',
	promo_video_url = 'promo_video_url',
}

class EditEventFormik extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);
		this.state = {
			headerImageUploading: false,
			activeUserSituations: this.generateSituations(),
			activeState: this.props.eventData.state.slug,
			staffOptions: [],
			//topicOptions : [],
		};

		this.renderSituations = this.renderSituations.bind(this);
		this.renderStates = this.renderStates.bind(this);
		this.handleSituationPillToggle = this.handleSituationPillToggle.bind(
			this,
		);
		this.handleStatePillToggle = this.handleStatePillToggle.bind(this);
		this.generateDate = this.generateDate.bind(this);
		this.generateTime = this.generateTime.bind(this);
	}

	public componentDidMount() {
		const staffOptions: ISelectOption[] = this.props.staffs.map(s => {
			return {
				label: s.first_name + ' ' + s.last_name,
				value: s.id.toString(),
			};
		});
		// const topicOptions : ISelectOption[]=this.props.topics.map(t=>{
		// 	return {
		// 		label : t.title,
		// 		value : t.id.toString(),
		// 	};
		// });
		//this.setState({staffOptions,topicOptions});
		this.setState({ staffOptions });
	}

	public render() {
		const { staffOptions } = this.state;
		const validationSchema = Yup.object().shape({
			title: ValidationEvent.title,
			description: ValidationEvent.description,
			situations: ValidationEvent.situations,
			start_date: ValidationEvent.start_date,
			start_date_time: ValidationEvent.start_date,
			end_date: ValidationEvent.end_date,
			end_date_time: ValidationEvent.end_date,
			latitude: ValidationEvent.latitude,
			longitude: ValidationEvent.longitude,
			location: ValidationEvent.address,
			state: ValidationEvent.state,
			email_address: ValidationEvent.email_address,
			event_type: ValidationEvent.eventtype,
			event_admin: ValidationEvent.eventAdmin,
			// event_time_zone: ValidationEvent.state,
			promo_video_url: ValidationEvent.promo_url,
			feature_image: ValidationEvent.eventImage,
			last_joining_date: ValidationEvent.last_registration_date,
			//topic_id : ValidationEvent.topic_id,
		});
		return (
			<Formik
				initialValues={{
					title: this.props.eventData.title,
					description: this.props.eventData.description,
					situations: [],
					start_date: this.generateDate(
						this.props.eventData.start_date.date,
					),
					start_date_time: this.generateTime(
						this.props.eventData.start_date.date,
					),
					end_date: this.generateDate(
						this.props.eventData.end_date.date,
					),
					end_date_time: this.generateTime(
						this.props.eventData.end_date.date,
					),
					location: this.props.eventData.location,
					location_data: this.createAddressData(this.props),
					email_address: this.props.eventData.email_address,
					state: this.props.eventData.state,
					feature_image: this.props.eventData.feature_image,
					age_limit:
						this.props.eventData.max_age > 0 ||
						this.props.eventData.min_age > 0,
					min_age: this.props.eventData.min_age,
					max_age: this.props.eventData.max_age,
					event_admin: this.props.eventData.event_admin
						? this.props.eventData.event_admin
						: '',
					event_time_zone: this.props.eventData.event_time_zone
						? this.props.eventData.event_time_zone
						: '',
					event_type: this.props.eventData.event_type
						? this.props.eventData.event_type
						: '',
					is_travel_cover: this.props.eventData.is_travel_cover
						? this.props.eventData.is_travel_cover
						: false,
					promo_video_url: this.props.eventData.promo_video_url
						? this.props.eventData.promo_video_url
						: '',
					max_participents: this.props.eventData.max_participents
						? this.props.eventData.max_participents
						: 0,
					last_joining_date: this.props.eventData.last_joining_date
						? this.generateDate(
								this.props.eventData.last_joining_date.date,
						  )
						: '',
					show_rsvp:
						this.props.eventData.show_rsvp === 1 ? true : false,
					//	topic_id : this.props.eventData.topic_id? this.props.eventData.topic_id.toString():'',
				}}
				validationSchema={validationSchema}
				onSubmit={formValues => {
					const values = this.processFormValues(formValues);
					this.props.onSubmit(values);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					//isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					const dropzoneClasses = ['discussion_modal-dropzone'];

					if ('feature_image' in values && values.feature_image) {
						dropzoneClasses.push(
							'discussion_modal-dropzone--image',
						);
					}
					if (errors.feature_image) {
						dropzoneClasses.push('discussion_modal-content-error');
					}

					if (
						!isEqual(
							this.state.activeUserSituations.sort(),
							inputProps.values.situations.sort(),
						)
					) {
						inputProps.setFieldValue(
							'situations',
							this.state.activeUserSituations,
						);
					}

					if (
						this.state.activeState !==
						String(inputProps.values.state)
					) {
						inputProps.setFieldValue(
							'state',
							this.state.activeState,
						);
					}

					return (
						<form
							className="create_event_form form"
							onSubmit={e => {
								e.preventDefault();
								if (errors.title || values.title === '') {
									this.handleScroll(IDs.title);
								} else if (errors.event_type) {
									this.handleScroll(IDs.event_type);
								} else if (errors.event_admin) {
									this.handleScroll(IDs.event_admin);
								} else if (errors.email_address) {
									this.handleScroll(IDs.email_address);
								} else if (errors.situations) {
									this.handleScroll(IDs.situations);
								} else if (errors.description) {
									this.handleScroll(IDs.description);
								} else if (errors.feature_image) {
									this.handleScroll(IDs.image);
								} else if (errors.last_joining_date) {
									this.handleScroll(IDs.last_joining_date);
								} else if (errors.start_date) {
									this.handleScroll(IDs.start_date);
								} else if (errors.start_date_time) {
									this.handleScroll(IDs.start_date_time);
								} else if (errors.end_date) {
									this.handleScroll(IDs.end_date);
								} else if (errors.end_date_time) {
									this.handleScroll(IDs.end_date_time);
								} else if (errors.event_time_zone) {
									this.handleScroll(IDs.event_time_zone);
								} else if (errors.state) {
									this.handleScroll(IDs.states);
								} else if (errors.promo_video_url) {
									this.handleScroll(IDs.promo_video_url);
								}
								handleSubmit(e);
							}}
						>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Event Title *"
										name="title"
										id={IDs.title}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="Select Event Type *"
										id={IDs.event_type}
										name="event_type"
										disabled={this.props.isSubmitting}
										options={eventTypes}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="Select Staff Member *"
										name="event_admin"
										id={IDs.event_admin}
										options={staffOptions}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="email"
										label="Event Email Address *"
										id={IDs.email_address}
										name="email_address"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div
									className="form-column"
									id={IDs.situations}
								>
									{this.renderSituations()}
									<br />
									{errors.situations && (
										<span className="event-form-helper-text">
											Event situation is required
										</span>
									)}
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<div className="form-input-label ">
										Event Details
									</div>

									<RichTextEditor
										toolbarId="create-event-toolbar"
										name="description"
										value={inputProps.values.description}
										profile={[
											'imageUpload',
											'embedVideo',
											'insertGif',
										]}
										bounds=".modal--blog .modal-content_container"
										scrollingContainer=".modal--blog .modal-content_container"
										{...inputProps}
									/>
								</div>
							</div>
							{errors.description && (
								<span
									id="description"
									className="event-form-helper-text"
								>
									Event description must be at most 5000
									characters
									<br />
									<br />
								</span>
							)}
							<div className="form-row" id={IDs.image}>
								<div className="form-column">
									<ImageDropzoneFormik
										values={inputProps.values}
										dropzoneClasses={dropzoneClasses}
										setFieldValue={inputProps.setFieldValue}
										apiUrl="/api/event-images"
										label="Set Event Image *"
										max_size={3}
									/>
									{values.feature_image === null && (
										<span className="event-form-helper-text">
											Event image is required
										</span>
									)}
								</div>
							</div>
							<div className="form-divider" />
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<div>
										<SwitchFieldFormik
											label="Age Limit"
											name="age_limit"
											{...inputProps}
										/>
										{values.age_limit && (
											<div className="form-row form-row">
												<div className="form-column">
													<TextFieldFormik
														name="min_age"
														label="Minimum Age Limit"
														type="number"
														{...inputProps}
													/>
												</div>
												<div className="form-column">
													<TextFieldFormik
														name="max_age"
														label="Maximum Age Limit"
														type="number"
														{...inputProps}
													/>
												</div>
											</div>
										)}

										<TextFieldFormik
											type="number"
											label="Maximum Number of Participant"
											name="max_participents"
											disabled={this.props.isSubmitting}
											{...inputProps}
										/>

										<br />
										<br />
									</div>
								</div>
							</div>
							<div className="form-divider" />
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<InlineDatePickerFormik
										label="Event start date *"
										name="start_date"
										id={IDs.start_date}
										format="DD/MM/YYYY"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>

								<div className="form-column">
									<TextFieldFormik
										type="time"
										label="Event start time *"
										id={IDs.start_date_time}
										name="start_date_time"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<InlineDatePickerFormik
										label="Event end date *"
										name="end_date"
										id={IDs.end_date}
										format="DD/MM/YYYY"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>

								<div className="form-column">
									<TextFieldFormik
										type="time"
										label="Event end time *"
										name="end_date_time"
										id={IDs.end_date_time}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<InlineDatePickerFormik
										format="DD/MM/YYYY"
										label="Last Date For Registration "
										name="last_joining_date"
										id={IDs.last_joining_date}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row form-row--space_bottom">
								<div className="form-column">
									<SelectFieldFormik
										label="Select Event Time Zone"
										name="event_time_zone"
										disabled={this.props.isSubmitting}
										options={timeZones}
										id={IDs.event_time_zone}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-divider" />
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<AutocompletePostcodeFormik
										type="postcode"
										label="Event location"
										name="location"
										name_data="location_data"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column" id={IDs.states}>
									{this.renderStates()}
									<br />
									{errors.state && (
										<span className="event-form-helper-text">
											Event state is required
										</span>
									)}
								</div>
							</div>
							<div className="form-divider" />
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Promo Video Url"
										name="promo_video_url"
										id={IDs.promo_video_url}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>
							{/* <div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="Select Discussion Topic"
										name="topic_id"
										options={topicOptions}
										noEmpty
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div> */}
							<div className="form-row">
								<div className="form-column">
									<SwitchFieldFormik
										label="Toggle if event covers travel cost"
										name="is_travel_cover"
										{...inputProps}
									/>
								</div>
								<div className="form-column">
									<SwitchFieldFormik
										label="Show RSVP"
										name="show_rsvp"
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-divider" />
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={this.props.isSubmitting}
									>
										Edit Event
									</Button>
								</div>
							</div>
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="contained"
										color="secondary"
										className="form-submit"
										type="button"
										disabled={this.props.isSubmitting}
										onClick={() => {
											const processedValues = this.processFormValues(
												values,
											);
											this.props.onDuplicate(
												processedValues,
											);
										}}
									>
										Duplicate Event
									</Button>
									<Button
										variant="contained"
										color="secondary"
										className="form-submit hm-l8"
										type="button"
										disabled={this.props.isSubmitting}
										onClick={() =>
											this.props.onDelete(
												this.props.eventData,
											)
										}
									>
										delete event
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		);
	}

	protected processFormValues(formValues: any): any {
		const values = JSON.parse(JSON.stringify(formValues));

		if (values.start_date && values.start_date_time) {
			const formattedStartDate = this.formatDateTime(
				values.start_date,
				values.start_date_time,
			);

			values['start_date'] = formattedStartDate;

			//Delete unused values
			delete values.start_date_time;
		}

		if (values.end_date && values.end_date_time) {
			const formattedEndDate = this.formatDateTime(
				values.end_date,
				values.end_date_time,
			);

			values['end_date'] = formattedEndDate;
			values.max_participents;
			//Delete unused values
			delete values.end_date_time;
		}
		if (values.last_joining_date) {
			const formattedEndDate = this.formatDateTime(
				values.last_joining_date,
				'23:59',
			);
			values['last_joining_date'] = formattedEndDate;
		} else {
			values['last_joining_date'] = values.start_date;
		}
		values['latitude'] = values.location_data
			? values.location_data.geometry.location.lat + ''
			: '';
		values['longitude'] = values.location_data
			? values.location_data.geometry.location.lng + ''
			: '';
		delete values.age_limit;
		values.is_online = values.state === 'online';
		if (values.show_rsvp) {
			values.show_rsvp = 1;
		} else {
			values.show_rsvp = 0;
		}

		return values;
	}

	protected createAddressData(props: IProps) {
		return {
			geometry: {
				location: {
					lat: parseFloat(props.eventData.latitude),
					lng: parseFloat(props.eventData.longitude),
				},
			},
		};
	}

	protected renderSituations() {
		return (
			<React.Fragment>
				<div className="form-input-label">Event Situation *</div>
				{!!this.props.situations && this.props.situations.length === 0 && (
					<div
						style={{
							display: 'flex',
							justifyContent: 'center',
						}}
					>
						<CircularProgress />
					</div>
				)}

				{!!this.props.situations &&
					this.props.situations.length > 0 &&
					this.props.situations.map((situation, index) => {
						const isActive =
							this.state.activeUserSituations.indexOf(
								situation.slug,
							) >= 0;
						return (
							<Pill
								key={index}
								label={situation.name}
								active={isActive}
								disabled={false}
								clickable={true}
								onClick={() => {
									this.handleSituationPillToggle(
										situation.slug,
									);
								}}
							/>
						);
					})}
			</React.Fragment>
		);
	}

	protected renderStates() {
		return (
			<React.Fragment>
				<div className="form-input-label">Event State *</div>
				{this.props.states.map((state, index) => {
					const isActive = this.state.activeState === state.value;

					return (
						<Pill
							key={index}
							label={state.label}
							active={isActive}
							disabled={false}
							clickable={true}
							onClick={() => {
								this.handleStatePillToggle(state.value);
							}}
						/>
					);
				})}
			</React.Fragment>
		);
	}

	protected handleSituationPillToggle(situation: string) {
		const activeSituations = [...this.state.activeUserSituations];
		const indexOf = activeSituations.indexOf(situation);

		if (indexOf === -1) {
			this.setState({
				activeUserSituations: [...activeSituations, situation],
			});
		} else if (indexOf >= 0) {
			const filterActivesituations = activeSituations.filter(
				filteredSituationSlug => {
					if (filteredSituationSlug !== situation) {
						return filteredSituationSlug;
					}
				},
			);

			this.setState({
				activeUserSituations: filterActivesituations,
			});
		}
	}

	protected handleStatePillToggle(state: string) {
		this.setState({
			activeState: state,
		});
	}

	protected formatDateTime(eventDate, eventTime) {
		const actualFormatDate = moment(eventDate).format('YYYY-MM-DD');

		let combindedDateTime = moment(
			actualFormatDate + ' ' + eventTime,
			'YYYY-MM-DD HH:mm',
		);

		let combindedDateTimeFormatted = moment(combindedDateTime).format(
			'YYYY-MM-DD HH:mm',
		);

		return `${combindedDateTimeFormatted}:00`;
	}

	protected generateDate(dateTime: string) {
		const momentDate = moment(dateTime).format('YYYY-MM-DD');
		return momentDate;
	}

	protected generateTime(dateTime: string) {
		const momentTime = moment(dateTime).format('HH:mm');
		return momentTime;
	}

	protected generateSituations() {
		let activeSituation = [];

		this.props.eventData.situations.map(situation => {
			activeSituation.push(situation.slug);
		});

		return activeSituation;
	}
	protected handleScroll = (id: string) => {
		//console.log('id === >', id);
		try {
			const element = document.getElementById(id);
			element.scrollIntoView({ behavior: 'smooth' });
		} catch (e) {
			console.log('ee', e);
		}
	};
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(withServices(EditEventFormik));
