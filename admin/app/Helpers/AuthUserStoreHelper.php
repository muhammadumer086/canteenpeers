<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class AuthUserStoreHelper {
	protected $authorizationMap = [];

	/**
	 * Call this method to get singleton
	 *
	 * @return AuthUserStoreHelper
	 */
	public static function Instance() {
		static $inst = null;
		if ($inst === null) {
			$inst = new AuthUserStoreHelper();
		}
		return $inst;
	}

	/**
	 * Private ctor so nobody else can instantiate it
	 *
	 */
	private function __construct() {}

	public function getAuthUser(Request $request = null) {
		if (!$request) {
			$request = request();
		}

		if ($request->headers->has('Authorization')) {
			$authorization = $request->headers->get('Authorization');
			if (!array_key_exists($authorization, $this->authorizationMap)) {
				$this->authorizationMap[$authorization] = Auth::guard('api')->authenticate($request);
			}

			return $this->authorizationMap[$authorization];
		}

		return null;
	}

}
