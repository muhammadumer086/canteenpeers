import React from 'react';
import { connect } from 'react-redux';
import ReactSVG from 'react-svg';
import algoliasearch from 'algoliasearch';

import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';
import ClearIcon from '@material-ui/icons/Clear';

import {
	IStoreState,
	searchModalHide,
	userPanelDisplay,
	apiError,
} from '../../redux/store';
import { ApiService, withServices } from '../../services';
import SearchResult from './SearchResult';
import { IApiSettings, IApiUser } from '../../interfaces';

interface IProps {
	settings: IApiSettings | null;
	apiService: ApiService;
	userData: IApiUser;
	userAuthenticated: boolean | null;
	searchModalDisplay: boolean | null;
	dispatch(action: any): void;
}

export interface ISearchResult {
	data?: string;
	excerpt?: string;
	id?: string;
	image?: string | null;
	objectID?: string;
	title: string;
	type: string;
	hashtags?: string[];
}

interface ISearchResultsGroup {
	total: number;
	results: ISearchResult[];
}

interface IQueryResults {
	top: ISearchResultsGroup;
	discussions: ISearchResultsGroup;
	users: ISearchResultsGroup;
	blogs: ISearchResultsGroup;
	resources: ISearchResultsGroup;
	hashtags: ISearchResultsGroup;
}

interface IState {
	selectedFilter: string;
	isUpdating: boolean;
	searchQuery: string;
	queryResults: IQueryResults;
	filteredResults: ISearchResult[];
}

export class SearchModal extends React.Component<IProps, IState> {
	protected algoliaClient: algoliasearch.Client = null;
	protected algoliaIndex: algoliasearch.Index = null;

	protected searchFacets = {
		top: '',
		discussions: 'type:discussion',
		users: 'type:user',
		blogs: 'type:blog',
		resources: 'type:resource',
		hashtags: 'type:hashtag',
	};

	constructor(props: IProps) {
		super(props);

		this.state = {
			selectedFilter: 'top',
			isUpdating: false,
			searchQuery: '',
			queryResults: this.getEmptyQueryResults(),
			filteredResults: [],
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleQueryChange = this.handleQueryChange.bind(this);
		this.handleSearch = this.handleSearch.bind(this);
		this.handleUserClick = this.handleUserClick.bind(this);
		this.filterResults = this.filterResults.bind(this);
	}

	public componentDidMount() {
		if (this.props.settings) {
			this.algoliaClient = algoliasearch(
				this.props.settings.algolia_app_id,
				this.props.settings.algolia_search_key,
			);
			this.algoliaIndex = this.algoliaClient.initIndex(
				this.props.settings.search_index,
			);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.settings &&
			JSON.stringify(prevProps.settings) !==
				JSON.stringify(this.props.settings)
		) {
			this.algoliaClient = algoliasearch(
				this.props.settings.algolia_app_id,
				this.props.settings.algolia_search_key,
			);
			this.algoliaIndex = this.algoliaClient.initIndex(
				this.props.settings.search_index,
			);
		}
	}

	public render() {
		const placeholderText = 'start typing...';

		const suggestedLinks = [
			{
				type: 'user',
				title: 'People like me',
				link: {
					href: '/people-like-me',
					as: '/people-like-me',
				},
			},
			{
				type: 'event',
				title: 'Events',
				link: {
					href: '/events',
					as: '/events',
				},
			},
			{
				type: 'resource',
				title: 'Resources useful to me',
				link: {
					href: '/resources',
					as: '/resources',
				},
			},
			{
				type: 'discussion',
				title: 'Most active discussions',
				link: {
					href: '/discussions',
					as: '/discussions',
				},
			},
		];

		const resultsToDisplay = this.state.queryResults[
			this.state.selectedFilter
		].results;

		return (
			<Modal
				aria-labelledby="search-modal"
				open={this.props.searchModalDisplay}
				onClose={this.handleClose}
			>
				<Paper
					className="modal modal--search theme--accent"
					square={true}
				>
					<button
						onClick={this.handleClose}
						className="close_button close_button--primary"
						title="Dismiss"
					>
						<ClearIcon className="close_button-icon" />
					</button>
					<div className="modal_search">
						<div className="modal_search-container">
							<div className="modal_search-title">
								<div className="modal_search-search_icon">
									<ReactSVG
										path={`${
											process.env.STATIC_PATH
										}/icons/search.svg`}
									/>
								</div>

								<div>What can we help you find? </div>
							</div>

							<div className="modal_search-query">
								<input
									type="text"
									placeholder={placeholderText}
									onChange={this.handleQueryChange}
									autoFocus={true}
									value={this.state.searchQuery}
								/>
							</div>

							{(this.state.searchQuery.length > 0 &&
								this.state.searchQuery[0] !== '#') ||
							this.state.searchQuery.length > 1 ? (
								<div className="modal_search-results">
									<div className="modal_search-results_filter">
										{this.renderFilterList()}
									</div>
									{resultsToDisplay.map((data, index) => {
										return (
											<SearchResult
												key={index}
												resultData={data}
												handleUserClick={
													this.handleUserClick
												}
											/>
										);
									})}
								</div>
							) : (
								<div className="modal_search-suggested">
									<div className="modal_search-results_filter">
										<span className="modal_search-sub_title">
											Suggestions
										</span>
									</div>

									{suggestedLinks.map((data, index) => {
										return (
											<SearchResult
												key={index}
												resultData={data}
												resultLink={data.link}
												handleUserClick={
													this.handleClose
												}
											/>
										);
									})}
								</div>
							)}
						</div>
					</div>
				</Paper>
			</Modal>
		);
	}

	protected renderFilterList() {
		const keys = Object.keys(this.searchFacets);

		return (
			<div className="modal_search-filter_list">
				{keys.map((key, index) => {
					return (
						<div
							key={index}
							className={`modal_search-filter modal_search-filter--${key}`}
						>
							<ButtonBase
								focusRipple={true}
								disabled={
									this.state.queryResults[key].total === 0
								}
								onClick={() => {
									this.filterResults(key);
								}}
							>
								<span className="modal_search-filter_icon">
									<ReactSVG
										path={`${
											process.env.STATIC_PATH
										}/icons/${key}.svg`}
									/>
								</span>

								<span className="modal_search-filter_label">
									{key === 'top'
										? 'Top Results'
										: `${key} (${
												this.state.queryResults[key]
													.total
										  })`}{' '}
								</span>
							</ButtonBase>
						</div>
					);
				})}
			</div>
		);
	}

	protected filterResults(filterKey: string) {
		this.setState({
			selectedFilter: filterKey,
		});
	}

	protected handleUserClick(resultDetails: ISearchResult): void {
		if (resultDetails.type === 'user') {
			const username = resultDetails.id;

			this.setState(
				{
					isUpdating: true,
				},
				() => {
					this.props.apiService
						.queryGET(`/api/users/${username}`)
						.then((userResponse: { user: IApiUser }) => {
							this.setState(
								{
									isUpdating: false,
								},
								() => {
									this.props.dispatch(
										userPanelDisplay(userResponse.user),
									);
								},
							);
						})
						.catch(error => {
							this.props.dispatch(apiError([error.message]));
						});
				},
			);
			this.handleClose();
		} else if (resultDetails.type === 'hashtag') {
			this.setState(
				{
					searchQuery: `#${resultDetails.id}`,
					selectedFilter: 'top',
				},
				() => {
					this.handleSearch();
				},
			);
		} else {
			this.handleClose();
		}
	}

	protected handleQueryChange(event: React.FormEvent<EventTarget>) {
		let target = event.target as HTMLInputElement;

		this.setState(
			{
				searchQuery: target.value,
			},
			() => {
				this.handleSearch();
			},
		);
	}

	protected handleSearch() {
		if (
			((this.state.searchQuery.length > 0 &&
				this.state.searchQuery[0] !== '#') ||
				this.state.searchQuery.length > 1) &&
			this.props.settings &&
			this.algoliaIndex !== null
		) {
			const queries: any[] = [];

			let queryBase: any = {
				indexName: this.props.settings.search_index,
				query: this.state.searchQuery,
				params: {
					facetFilters: [],
				},
			};

			const splitString = this.state.searchQuery.split('');
			if (splitString[0] === '#') {
				// Get the rest of the query
				const hashtag = this.state.searchQuery.substring(
					1,
					this.state.searchQuery.length,
				);

				queryBase = {
					indexName: this.props.settings.search_index,
					params: {
						facetFilters: [[`hashtags:${hashtag}`]],
					},
				};
			}

			for (const key in this.searchFacets) {
				if (this.searchFacets.hasOwnProperty(key)) {
					const query = JSON.parse(JSON.stringify(queryBase));
					if (this.searchFacets[key] !== '') {
						query.params.facetFilters.push([
							this.searchFacets[key],
						]);
					}

					queries.push(query);
				}
			}

			this.algoliaClient.search(queries, (err, content: any) => {
				if (err) {
					throw err;
				}

				const queryResults: IQueryResults = this.getEmptyQueryResults();

				const keys = Object.keys(this.searchFacets);

				keys.forEach((key, index) => {
					queryResults[key].total = content.results[index].nbHits;
					queryResults[key].results = content.results[index].hits;
				});

				this.setState({
					queryResults,
				});
			});
		}
	}

	protected handleClose() {
		this.setState(
			{
				searchQuery: '',
			},
			() => {
				this.props.dispatch(searchModalHide());
			},
		);
	}

	protected getEmptyQueryResults(): IQueryResults {
		return {
			top: {
				total: 0,
				results: [],
			},
			discussions: {
				total: 0,
				results: [],
			},
			users: {
				total: 0,
				results: [],
			},
			blogs: {
				total: 0,
				results: [],
			},
			resources: {
				total: 0,
				results: [],
			},
			hashtags: {
				total: 0,
				results: [],
			},
		};
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { searchModalDisplay } = state.searchModal;
	const { settings } = state.settings;
	return {
		settings,
		userAuthenticated,
		searchModalDisplay,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(SearchModal));
