import React from 'react';
import { connect } from 'react-redux';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import { apiError, apiSuccess } from '../../redux/store';
import buildUrl from 'build-url';

import { IApiUser } from '../../interfaces';
import { ApiService, AuthService } from '../../services';
import { ETabs, ESettingsScreens, EAccountScreens } from './UserPanelAccount';

import UserAvatar from '../general/UserAvatar';
import ListLoading from '../general/ListLoading';

interface IProps {
	userData: IApiUser;
	submitting: boolean;
	apiService: ApiService;
	authService: AuthService;
	changeScreen(tab: ETabs, screen: EAccountScreens | ESettingsScreens);
	onSubmit(values: any, resetForm?: () => void);
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	blockedUsers: IApiUser[];
}

class UserAccountBlockedUsers extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			submitting: false,
			blockedUsers: [],
		};

		this.handleClickBlock = this.handleClickBlock.bind(this);
		this.handleClickUnblock = this.handleClickUnblock.bind(this);
		this.loadBlockedUsers = this.loadBlockedUsers.bind(this);
		this.unblockUser = this.unblockUser.bind(this);
		this.blockUser = this.blockUser.bind(this);
	}

	public componentDidMount() {
		this.loadBlockedUsers();
	}

	public render() {
		return (
			<div className="user_account_panel user_account_panel--account_delete">
				<div className="user_panel-back">
					<ButtonBase
						focusRipple={true}
						className="user_panel-back-button"
						onClick={() => {
							this.props.changeScreen(
								ETabs.SETTINGS,
								ESettingsScreens.OVERVIEW,
							);
						}}
						disabled={this.state.submitting}
					>
						<Icon className="user_panel-back-back_icon">
							arrow_back
						</Icon>
						Settings
					</ButtonBase>
				</div>

				<div className="user_panel-heading">
					<h4 className="font--h4 theme-title">Blocked Users</h4>
				</div>

				<div className="blocked-users">
					<p className="blocked-users-text theme-title">
						They won’t be able to contact you directly or mention
						you in discussions. Canteen won’t let them know that
						you’ve blocked them.
					</p>

					{this.state.blockedUsers.length === 0 &&
						!!this.state.submitting && (
							<ListLoading active={!!this.state.submitting} />
						)}

					{this.state.blockedUsers.length === 0 &&
						!this.state.submitting && (
							<p className="blocked-users-text theme-title">
								Your list is empty.
							</p>
						)}

					{this.state.blockedUsers.length > 0 && (
						<div className="blocked-users-list">
							{this.state.blockedUsers.map((user, index) => (
								<div className="blocked-user" key={index}>
									<div className="blocked-user-wrapper">
										<div className="blocked-user-avatar">
											<UserAvatar user={user} />
										</div>

										<div className="blocked-user-nick">
											{user.username}
										</div>

										<div className="blocked-user-actions">
											{user.is_blocked ? (
												<Button
													variant="outlined"
													color="primary"
													className="blocked-user-actions-block"
													type="button"
													disabled={
														this.state.submitting
													}
													onClick={() => {
														this.handleClickUnblock(
															user,
														);
													}}
												>
													Unblock
												</Button>
											) : (
												<Button
													variant="outlined"
													color="primary"
													className="blocked-user-actions-block"
													type="button"
													disabled={
														this.state.submitting
													}
													onClick={() => {
														this.handleClickBlock(
															user,
														);
													}}
												>
													Undo
												</Button>
											)}
										</div>
									</div>
								</div>
							))}
						</div>
					)}
				</div>
			</div>
		);
	}

	protected loadBlockedUsers() {
		// Load the blocked users
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryGET('/api/users/blocks')
					.then((response: any) => {
						if ('data' in response && 'users' in response.data) {
							let blockedUsers: IApiUser[] = response.data.users.sort(
								(a, b) => a.id - b.id,
							);
							this.setState({
								blockedUsers,
							});
						}
						this.setState({
							submitting: false,
						});
					})
					.catch(error => {
						this.props.dispatch(apiError(error));
					});
			},
		);
	}

	protected handleClickUnblock(user: IApiUser) {
		this.unblockUser(user);
	}

	protected handleClickBlock(user: IApiUser) {
		this.blockUser(user);
	}

	protected unblockUser(user: IApiUser) {
		const username = user.username;
		const url = buildUrl(null, {
			path: '/api/users/unblock/' + username,
		});
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(url, null)
					.then(() => {
						const currentUnblockedUser = user;
						currentUnblockedUser.is_blocked = !currentUnblockedUser.is_blocked;
						const blockedUsersWithoutCurrentUnblockedUser = this.state.blockedUsers.filter(
							user => user.id !== currentUnblockedUser.id,
						);
						const blockedUsers = [
							...blockedUsersWithoutCurrentUnblockedUser,
							currentUnblockedUser,
						].sort((a, b) => a.id - b.id);
						this.setState(
							{
								submitting: false,
								blockedUsers,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										`${
											user.username
										} unblocked. You can block them again at any time from their profile panel.`,
									]),
								);
							},
						);
					})
					.catch(error => {
						// dispatch the error message
						() => {
							this.props.dispatch(apiError([error.message]));
							this.setState({
								submitting: false,
							});
						};
					});
			},
		);
	}

	protected blockUser(user: IApiUser) {
		const username = user.username;
		const url = buildUrl(null, {
			path: '/api/users/block/' + username,
		});
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(url, null)
					.then(() => {
						const currentBlockedUser = user;
						currentBlockedUser.is_blocked = !currentBlockedUser.is_blocked;
						const blockedUsersWithoutCurrentBlockedUser = this.state.blockedUsers.filter(
							user => user.id !== currentBlockedUser.id,
						);
						const blockedUsers = [
							...blockedUsersWithoutCurrentBlockedUser,
							currentBlockedUser,
						].sort((a, b) => a.id - b.id);
						this.setState(
							{
								submitting: false,
								blockedUsers,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										`${
											user.username
										} blocked. You can unblock them again at any time from their profile panel.`,
									]),
								);
							},
						);
					})
					.catch(error => {
						// dispatch the error message
						() => {
							this.props.dispatch(apiError([error.message]));
							this.setState({
								submitting: false,
							});
						};
					});
			},
		);
	}
}

const mapStateToProps = () => {
	return {};
};
export default connect(mapStateToProps)(UserAccountBlockedUsers);
