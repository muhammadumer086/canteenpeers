<?php

namespace App\Http\Controllers\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Psr\Http\Message\ResponseInterface;
use Validator;
use App\User;
use App\Event;

class SideOfStageController extends Controller {
	/**
	 * @param String $token
	 * @return mixed|ResponseInterface
	 * @throws GuzzleException
	 */
	public function register(string $token) {
		$client = new Client();
		$response = $client->request(
			'GET',
			url(config('side-of-stage.api') . "/entry/$token")
		);

		return $response;
	}

	public function eventRegistration(Request $request) {
		$validator = Validator::make($request->all(), [
			'id' => 'bail|required|string',
			'email' => 'bail|required|email',
			'key' => 'bail|required|string',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		if ($request->input('key') !== config('side-of-stage.access_key')) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		// Find if the user and the event exist
		$user = User::where('email', $request->input('email'))->first();
		$event = Event::where('sos_id', $request->input('id'))->first();

		if ($user && $event && !$event->users->contains($user)) {
			// Register the user to the event
			$event
				->users()
				->attach($user, ['registration_origin' => 'side-of-stage']);
		}

		return response()->json(
			[
				'success' => true,
			],
			200
		);
	}
}
