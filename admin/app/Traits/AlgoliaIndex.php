<?php

namespace App\Traits;

/**
 * Get the algolia index
 */
trait AlgoliaIndex {
	protected function privateAlgoliaIndex() {
		return config('scout.prefix') . config('canteen.algolia_private_prefix') . 'index';
	}
	protected function publicAlgoliaIndex() {
		return config('scout.prefix') . config('canteen.algolia_public_prefix') . 'index';
	}
	protected function algoliaIndex($publicIndex = null) {
		if (is_null($publicIndex)) {
			if (defined('ALGOLIA_PUBLIC') && ALGOLIA_PUBLIC) {
				return $this->publicAlgoliaIndex();
			} else {
				return $this->privateAlgoliaIndex();
			}
		} else if ($publicIndex === false) {
			return $this->privateAlgoliaIndex();
		}

		return $this->publicAlgoliaIndex();
	}
}
