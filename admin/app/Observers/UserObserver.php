<?php

namespace App\Observers;

use UserActivityHelper;

use App\User;

class UserObserver {
	/**
	 * Handle the user "created" event.
	 *
	 * @param  \App\User  $user
	 * @return void
	 */
	public function created(User $user) {
		$additionalData = null;
		if ($user->registration_origin) {
			$additionalData = [
				'origin' => $user->registration_origin,
			];
		}
		UserActivityHelper::create(
			$user,
			$user,
			'User Created',
			$additionalData
		);
	}

	/**
	 * Handle the user "updated" event.
	 *
	 * @param  \App\User  $user
	 * @return void
	 */
	public function updated(User $user) {
		// User activity is tracked on the "update" function of the UserController
	}

	/**
	 * Handle the user "deleted" event.
	 *
	 * @param  \App\User  $user
	 * @return void
	 */
	public function deleted(User $user) {
		//  User activity is tracked on the "destroy" function of the UserController
	}

	/**
	 * Handle the user "restored" event.
	 *
	 * @param  \App\User  $user
	 * @return void
	 */
	public function restored(User $user) {
		UserActivityHelper::create($user, $user, 'User Restored', null);
	}

	/**
	 * Handle the user "force deleted" event.
	 *
	 * @param  \App\User  $user
	 * @return void
	 */
	public function forceDeleted(User $user) {
		//
	}
}
