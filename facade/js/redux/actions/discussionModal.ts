import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { IApiDiscussion } from 'js/interfaces';

export const discussionModalDisplay = (
	discussionData?: IApiDiscussion,
	predefinedTopic?: string,
	predefinedSituations?: string[],
) => (dispatch, getState: () => IStoreState) => {
	if (getState().discussionModal.discussionModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.DISCUSSION_MODAL_DISPLAY,
		discussionData: discussionData ? discussionData : null,
		predefinedTopic: predefinedTopic ? predefinedTopic : null,
		predefinedSituations: predefinedSituations
			? predefinedSituations
			: null,
	});
};

export const discussionModalHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().discussionModal.discussionModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.DISCUSSION_MODAL_HIDE,
	});
};
