<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_images', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('path');
            $table->unsignedBigInteger('link_id');
            $table->unsignedBigInteger('width');
            $table->unsignedBigInteger('height');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_images');
    }
}
