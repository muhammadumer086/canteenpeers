@extends('layouts.email')

@section('plain_text')
	It looks like you haven’t had a chance to check out Canteen Connect (our brand new online community) yet. Just activate your account to join the convo and see all the great changes you asked for in action!
@endsection

@section('content')
	@component('emails.components.paragraph')
		@if (isset($user->first_name))
			Hi {{$user->first_name}},
		@else
			Hi,
		@endif
	@endcomponent

	@component('emails.components.paragraph')
		It looks like you haven’t had a chance to check out Canteen Connect (our brand new online community) yet. Just activate your account to join the convo and see all the great changes you asked for in action!
	@endcomponent

	@component('emails.components.button', ['url' => $facadeUrl])
		Activate Your Account
	@endcomponent

	@component('emails.components.paragraph')
		Hear from some of the <a style="color: #ff5f33;" href="https://canteenconnect.org/discussions/introduce-yourself-2">other young people</a> who have already posted on Canteen Connect, check out this discussion about <a style="color: #ff5f33;" href="https://canteenconnect.org/discussions/dark-cancer-humour">dark cancer humour</a> or read these helpful tips on <a style="color: #ff5f33;" href="https://canteenconnect.org/discussions/school-work-tips">how to manage study</a> when cancer's messing with your life.
	@endcomponent

	@component('emails.components.paragraph')
		Canteen Connect also lets you register your interest for events that are coming up. <a style="color: #ff5f33;" href="https://canteenconnect.org/events">Check out what’s happening</a> in your area.
	@endcomponent

	@component('emails.components.paragraph')
		So what are you waiting for!
	@endcomponent

	@component('emails.components.button', ['url' => $facadeUrl])
		Activate Your Account
	@endcomponent

	@component('emails.components.paragraph')
		If you have any trouble activating your account, email <a style="color: #ff5f33;" href="mailto:online@canteen.org.au">online@canteen.org.au</a> or call us on <a style="color: #ff5f33;" href="tel:1800835932">1800 835 932</a>.
	@endcomponent

@endsection
