import React from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';

import {
	IStoreState,
	newBlogModalHide,
	apiError,
	apiSuccess,
	updateData,
} from '../../redux/store';
import { withServices, ApiService } from '../../services';

import { ModalDialog } from '../../components/modals/local';

import CreateNewBlogForm from './CreateNewBlogForm';
import {
	IApiSituation,
	ISelectOption,
	IApiTopic,
	IApiUser,
	IApiBlog,
} from '../../interfaces';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';

interface IState {
	isSubmitting: boolean;
	isUpdating: boolean;
	userSituations: IApiSituation[];
	topicOptions: ISelectOption[];
}

interface IProps {
	situations: IApiSituation[];
	userData: IApiUser;
	userAuthenticated: boolean | null;
	apiService: ApiService;
	blogData: IApiBlog;
	topics: IApiTopic[];
	newBlogModalDisplay: boolean;
	predefinedTopic: string | null;
	dispatch(action: any): void;
}

class CreateNewBlogModal extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props: IProps) {
		super(props);

		this.state = {
			isSubmitting: false,
			isUpdating: false,
			topicOptions: [],
			userSituations: [],
		};

		this.handleClose = this.handleClose.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.updatedSituations = this.updatedSituations.bind(this);
	}

	public componentWillMount() {
		this.updateData();
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.userData !== prevProps.userData
		) {
			this.updateData();
		}
	}

	public render() {
		let modalTitle = this.props.blogData
			? `Edit ${this.props.blogData.title}`
			: 'Write a Blog Post';

		return (
			<ModalDialog
				isActive={this.props.newBlogModalDisplay}
				modalTitle={modalTitle}
				ariaTag="blog-modal"
				handleClose={this.handleClose}
			>
				<CreateNewBlogForm
					onSubmit={this.submitForm}
					isSubmitting={this.state.isSubmitting}
					situations={this.props.situations}
					userSituations={this.state.userSituations}
					topics={this.state.topicOptions}
					blogData={this.props.blogData}
					updatedSituations={this.updatedSituations}
					predefinedTopic={this.props.predefinedTopic}
				/>
			</ModalDialog>
		);
	}

	protected handleClose() {
		this.props.dispatch(newBlogModalHide());
	}

	protected submitForm(values) {
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				if (!this.props.blogData) {
					this.createBlog(values);
				} else {
					this.updateBlog(values);
				}
			},
		);
	}

	protected createBlog(values) {
		this.props.apiService
			.queryPOST('/api/blogs', values, 'blog')
			.then((newBlog: IApiBlog) => {
				const newBlogSlug = newBlog.slug;
				this.setState(
					{
						isSubmitting: false,
					},
					() => {
						this.handleClose();

						this.GTM.pushEventToDataLayer({
							event: 'createBlog',
						});

						this.props.dispatch(
							apiSuccess(['Blog has been created']),
						);
						Router.push(
							`/blogs-single?id=${newBlogSlug}`,
							`/blogs/${newBlogSlug}`,
						);
					},
				);
			})
			.catch(error => {
				this.setState({
					isSubmitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}

	protected updateBlog(values) {
		this.props.apiService
			.queryPUT(`/api/blogs/${this.props.blogData.slug}`, values, 'blog')
			.then(() => {
				this.setState(
					{
						isSubmitting: false,
					},
					() => {
						this.handleClose();

						this.GTM.pushEventToDataLayer({
							event: 'updateBlog',
						});

						this.props.dispatch(
							apiSuccess(['Blog has been updated']),
						);
						this.props.dispatch(updateData());
					},
				);
			})
			.catch(error => {
				this.setState({
					isSubmitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}

	protected updateData() {
		// Fetch the user situation and topics
		if (this.props.userData) {
			const userSituations = this.props.userData.situations;

			if (userSituations.length) {
				this.setState({
					userSituations,
				});
			} else {
				// Load all situations from API
				this.props.apiService
					.queryGET('/api/situations', 'situations')
					.then((situationsList: IApiSituation[]) => {
						const userSituations: IApiSituation[] = [];
						if (situationsList instanceof Array) {
							situationsList.map((situation: IApiSituation) => {
								if (!situation.cancer_type) {
									userSituations.push(situation);
								}
							});
						}

						this.setState(
							{
								userSituations,
							},
							() => {
								this.updateTopics();
							},
						);
					})
					.catch(error => {
						this.props.dispatch(apiError(error));
					});
			}
		}

		this.updateTopics();
	}

	protected updateTopics(situations?: string[]) {
		const topicKey = 'blogs';

		const topics: IApiTopic[] = this.props.topics.filter(singleTopic => {
			if (singleTopic.category === topicKey) {
				// If situations are provided, make sure the topic match all the situations
				if (situations) {
					for (const situationSlug of situations) {
						if (
							!singleTopic.situations.find(topicSituation => {
								return topicSituation.slug === situationSlug;
							})
						) {
							return false;
						}
					}
				}
				return singleTopic;
			}
		});

		const topicOptions: ISelectOption[] = topics.map(singleTopic => {
			return {
				label: singleTopic.title,
				value: singleTopic.slug,
			};
		});

		this.setState({
			isUpdating: false,
			topicOptions,
		});
	}

	protected updatedSituations(situations: string[]) {
		this.updateTopics(situations);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const {
		newBlogModalDisplay,
		blogData,
		predefinedTopic,
	} = state.newBlogModal;
	const { situations } = state.situations;
	const { topics } = state.topics;
	return {
		userAuthenticated,
		userData,
		newBlogModalDisplay,
		blogData,
		predefinedTopic,
		situations,
		topics,
	};
}

export default connect(mapStateToProps)(withServices(CreateNewBlogModal));
