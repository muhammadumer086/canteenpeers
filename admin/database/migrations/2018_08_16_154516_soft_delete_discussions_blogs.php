<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeleteDiscussionsBlogs extends Migration {
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up() {
		Schema::table('blogs', function ($table) {
			$table->softDeletes();
		});

		Schema::table('discussions', function ($table) {
			$table->unsignedInteger('closed')->nullable();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->dropColumn('deleted_at');
		});

		Schema::table('discussions', function (Blueprint $table) {
			$table->dropColumn('closed');
		});
	}
}
