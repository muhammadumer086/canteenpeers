<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class CacheFlush extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:cacheFlush {--no-optimise : Doesn\'t run the optimisation tasks}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Flush the caches and optimise the configuration';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		Cache::flush();
		$this->call('config:clear');
		$this->call('view:clear');

		if (!$this->option('no-optimise')) {
			$this->call('config:cache');
			$this->call('view:cache');
		}
	}
}
