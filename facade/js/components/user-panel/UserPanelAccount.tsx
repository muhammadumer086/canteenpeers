import React from 'react';
import { connect } from 'react-redux';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { ApiService, AuthService } from '../../services';
import {
	IStoreState,
	apiError,
	apiSuccess,
	userPanelUpdated,
	userPanelChangeTab,
	updateData,
} from '../../redux/store';
import {
	IApiUser,
	IApiSituation,
	ISituationOption,
	IApiLanguage,
	IApiCountry,
	IApiHeardAbout,
	IApiPersonalSetting,
	IApiRole,
	ISelectOption,
} from '../../interfaces';
import UserAccountOverview from './UserAccountOverview';
import UserAccountPassword from './UserAccountPassword';
import UserAccountOptional from './UserAccountOptional';
import UserAccountPublicDetails from './UserAccountPublicDetails';
import UserAccountPrivateDetails from './UserAccountPrivateDetails';
import UserAccountGuardianDetails from './UserAccountGuardianDetails';
import UserSettingsOverview from './UserSettingsOverview';
import UserSettingsDelete from './UserSettingsDelete';
import UserSettingsBlockedUsers from './UserSettingsBlockedUsers';
import UserAccountRole from './UserAccountRole';

import { GTMDataLayer } from '../../../js/helpers/dataLayer';

export enum ETabs {
	MY_PROFILE,
	SETTINGS,
	CONNECT_MY_CHILD,
}

export enum EAccountScreens {
	OVERVIEW,
	PUBLIC_DETAILS,
	PRIVATE_DETAILS,
	GUARDIAN_DETAILS,
	PASSWORD,
	OPTIONAL,
	ROLE,
}

export enum ESettingsScreens {
	OVERVIEW,
	BLOCKED_USERS,
	DELETE_ACCOUNT,
}

interface IProps {
	userData: IApiUser | null;
	userPanelData: IApiUser | null;
	userPanelTab: ETabs;
	apiService: ApiService;
	authService: AuthService;
	deletePermission: boolean;
	allPermission: boolean;
	closePane(): void;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	accountScreen: EAccountScreens;
	settingsScreen: ESettingsScreens;
	userPanelData: IApiUser | null;
	situations: ISituationOption[];
	situationsList: IApiSituation[];
	languages: IApiLanguage[];
	countries: IApiCountry[];
	heardAbouts: IApiHeardAbout[];
	personalSettings: IApiPersonalSetting[];
	rolesOption: ISelectOption[];
}

class UserPanelAccount extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props: IProps) {
		super(props);

		this.state = {
			submitting: false,
			accountScreen: EAccountScreens.OVERVIEW,
			settingsScreen: ESettingsScreens.OVERVIEW,
			userPanelData: JSON.parse(JSON.stringify(props.userPanelData)),
			situations: [],
			situationsList: [],
			languages: [],
			countries: [],
			heardAbouts: [],
			personalSettings: [],
			rolesOption: [],
		};

		this.handleTabChange = this.handleTabChange.bind(this);
		this.handleChangeAccountScreen = this.handleChangeAccountScreen.bind(
			this,
		);
		this.handleUserUpdate = this.handleUserUpdate.bind(this);
		this.handleUserDelete = this.handleUserDelete.bind(this);
		this.updateUserRole = this.updateUserRole.bind(this);
	}

	public componentDidMount() {
		// Load the situations
		this.props.apiService
			.queryGET('/api/situations', 'situations')
			.then((situationsList: IApiSituation[]) => {
				const situations: ISituationOption[] = [];

				if (situationsList instanceof Array) {
					situationsList.map((situation: IApiSituation) => {
						if (!situation.cancer_type) {
							situations.push({
								name: situation.name,
								slug: situation.main_slug,
								cancer_type: situation.cancer_type,
								date: situation.date ? situation.date : null,
							});
						}
					});
				}

				this.setState({
					situations,
					situationsList,
				});
			})
			.catch(error => {
				this.props.dispatch(apiError(error));
			});

		// Load the languages
		this.props.apiService
			.queryGET('/api/languages', 'languages')
			.then((languages: IApiLanguage[]) => {
				this.setState({
					languages,
				});
			})
			.catch(error => {
				this.props.dispatch(apiError(error));
			});

		// Load the countries
		this.props.apiService
			.queryGET('/api/countries', 'countries')
			.then((countries: IApiCountry[]) => {
				this.setState({
					countries,
				});
			})
			.catch(error => {
				this.props.dispatch(apiError(error));
			});

		// Load the heard about
		this.props.apiService
			.queryGET('/api/heard-abouts', 'heard-abouts')
			.then((heardAbouts: IApiHeardAbout[]) => {
				this.setState({
					heardAbouts,
				});
			})
			.catch(error => {
				this.props.dispatch(apiError(error));
			});

		// Load the personal settings
		this.props.apiService
			.queryGET('/api/personal-settings', 'personal-settings')
			.then((personalSettings: IApiPersonalSetting[]) => {
				this.setState({
					personalSettings,
				});
			})
			.catch(error => {
				this.props.dispatch(apiError(error));
			});
		if (
			this.props.userData &&
			this.props.userData.role_names[0] === 'admin'
		) {
			this.props.apiService
				.queryGET('/api/roles')
				.then(response => {
					const data: IApiRole[] = response.roles.data;
					const rolesOption: ISelectOption[] = data.map(r => {
						return {
							label: r.name,
							value: r.name,
							tooltip: r.id.toString(), //saving ids to toolpit for later use
						};
					});
					this.setState({
						rolesOption,
					});
				})
				.catch(error => {
					this.props.dispatch(apiError([error.message]));
				});
		}
		// Load the Roles
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			JSON.stringify(this.props.userData) !==
				JSON.stringify(prevProps.userData) ||
			JSON.stringify(this.props.userPanelData) !==
				JSON.stringify(prevProps.userPanelData)
		) {
			this.setState({
				userPanelData: JSON.parse(
					JSON.stringify(this.props.userPanelData),
				),
			});
		}
	}

	public render() {
		const userAccountProps = {
			userData: this.state.userPanelData,
			submitting: this.state.submitting,
			situations: this.state.situations,
			languages: this.state.languages,
			countries: this.state.countries,
			heardAbouts: this.state.heardAbouts,
			personalSettings: this.state.personalSettings,
			situationsList: this.state.situationsList,
			changeScreen: this.handleChangeAccountScreen,
			onSubmit: this.handleUserUpdate,
			isAdminEdit:
				this.props.userData &&
				this.props.userPanelData &&
				this.props.userData.id !== this.props.userPanelData.id,
			apiService: this.props.apiService,
			authService: this.props.authService,
			allPermission: this.props.allPermission,
			deletePermission: this.props.deletePermission,
		};

		const userDeleteProps = Object.assign({}, userAccountProps, {
			onSubmit: this.handleUserDelete,
		});

		return (
			<div className="user_panel user_panel--account theme--main">
				{this.state.userPanelData && (
					<React.Fragment>
						<header className="user_panel-header">
							<h3 className="user_panel-header-heading hp-t16 hp-b16">
								My Account
							</h3>
						</header>
						<div className="user_panel-tabs">
							<Tabs
								value={this.props.userPanelTab}
								onChange={this.handleTabChange}
								indicatorColor="primary"
								textColor="primary"
							>
								<Tab
									label="My Profile"
									value={ETabs.MY_PROFILE}
								/>
								<Tab label="Settings" value={ETabs.SETTINGS} />
								{/* <Tab label="Connect My Child" value={ETabs.CONNECT_MY_CHILD} /> */}
							</Tabs>
						</div>
						<section className="user_panel-content">
							{this.props.userPanelTab === ETabs.MY_PROFILE && (
								<React.Fragment>
									{this.state.accountScreen ===
										EAccountScreens.OVERVIEW && (
										<UserAccountOverview
											{...userAccountProps}
										/>
									)}
									{this.state.accountScreen ===
										EAccountScreens.PUBLIC_DETAILS && (
										<UserAccountPublicDetails
											{...userAccountProps}
										/>
									)}
									{this.state.accountScreen ===
										EAccountScreens.PRIVATE_DETAILS && (
										<UserAccountPrivateDetails
											{...userAccountProps}
										/>
									)}
									{this.state.accountScreen ===
										EAccountScreens.GUARDIAN_DETAILS && (
										<UserAccountGuardianDetails
											{...userAccountProps}
										/>
									)}
									{this.state.accountScreen ===
										EAccountScreens.PASSWORD && (
										<UserAccountPassword
											{...userAccountProps}
										/>
									)}
									{this.state.accountScreen ===
										EAccountScreens.OPTIONAL && (
										<UserAccountOptional
											{...userAccountProps}
										/>
									)}
									{this.state.accountScreen ===
										EAccountScreens.ROLE &&
										this.props.userData &&
										this.props.userData.role_names[0] ===
											'admin' && (
											<UserAccountRole
												updateUserRole={
													this.updateUserRole
												}
												rolesOptions={
													this.state.rolesOption
												}
												{...userAccountProps}
											/>
										)}
								</React.Fragment>
							)}
							{this.props.userPanelTab === ETabs.SETTINGS && (
								<React.Fragment>
									{this.state.settingsScreen ===
										ESettingsScreens.OVERVIEW && (
										<UserSettingsOverview
											{...userAccountProps}
										/>
									)}
									{(this.props.deletePermission ||
										this.props.userData.id ===
											this.props.userPanelData.id) &&
										this.state.settingsScreen ===
											ESettingsScreens.DELETE_ACCOUNT && (
											<UserSettingsDelete
												{...userDeleteProps}
											/>
										)}
									{this.state.settingsScreen ===
										ESettingsScreens.BLOCKED_USERS && (
										<UserSettingsBlockedUsers
											{...userDeleteProps}
										/>
									)}
								</React.Fragment>
							)}
							{/* {
								this.state.tab === ETabs.CONNECT_MY_CHILD &&
								<div>Connect My Child</div>
							} */}
						</section>
					</React.Fragment>
				)}
			</div>
		);
	}

	protected handleTabChange(_event, tab: ETabs) {
		if (!this.state.submitting) {
			this.setState(
				{
					accountScreen: EAccountScreens.OVERVIEW,
					settingsScreen: ESettingsScreens.OVERVIEW,
				},
				() => {
					this.props.dispatch(userPanelChangeTab(tab));
				},
			);
		}
	}

	protected scrollToTop() {
		if (
			document.body.contains(
				document.querySelector('.user_panel-content'),
			)
		) {
			document.querySelector('.user_panel-content').scrollTo(0, 0);
		}
	}

	protected handleChangeAccountScreen(
		tab: ETabs,
		screen: EAccountScreens | ESettingsScreens,
	) {
		if (!this.state.submitting) {
			if (tab === ETabs.MY_PROFILE && screen in EAccountScreens) {
				this.setState(
					{
						accountScreen: screen as EAccountScreens,
					},
					() => {
						this.props.dispatch(userPanelChangeTab(tab));
					},
				);
			} else if (tab === ETabs.SETTINGS && screen in ESettingsScreens) {
				this.setState(
					{
						settingsScreen: screen as ESettingsScreens,
					},
					() => {
						this.props.dispatch(userPanelChangeTab(tab));
					},
				);
			}
		}
		this.scrollToTop();
	}

	protected handleUserUpdate(values: any, resetForm?: () => void) {
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/users/${this.state.userPanelData.username}`,
						values,
						'user',
					)
					.then((userData: IApiUser) => {
						this.setState({
							submitting: false,
							accountScreen: EAccountScreens.OVERVIEW,
							settingsScreen: ESettingsScreens.OVERVIEW,
						});
						this.props.dispatch(userPanelUpdated(userData));
						this.props.dispatch(apiSuccess(['Account Updated']));
						if (resetForm) {
							resetForm();
						}
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected updateUserRole(role: string) {
		this.setState(
			{
				submitting: true,
			},
			() => {
				const roleIndex = this.state.rolesOption.findIndex(
					r => r.value === role,
				);
				const roleId =
					roleIndex > -1
						? this.state.rolesOption[roleIndex].tooltip
						: -1; //saved ids in toolpit
				this.props.apiService
					.queryPUT(
						`api/roles/${this.state.userPanelData.username}/${roleId}/assignrole`,
						{},
					)
					.then((_userData: IApiUser) => {
						this.setState({
							submitting: false,
							accountScreen: EAccountScreens.OVERVIEW,
							settingsScreen: ESettingsScreens.OVERVIEW,
						});
						const { userPanelData } = this.props;
						const obj = { ...userPanelData };
						obj.role_names = [role];
						this.props.dispatch(userPanelUpdated(obj));
						this.props.dispatch(apiSuccess(['Account Updated']));
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected handleUserDelete(values: any) {
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryDELETE(
						`/api/users/${this.state.userPanelData.username}`,
						values,
					)
					.then(() => {
						this.props.dispatch(apiSuccess(['Account Deleted']));

						this.GTM.pushEventToDataLayer({
							event: 'deleteAccount',
						});

						if (
							this.state.userPanelData.id ===
							this.props.userData.id
						) {
							this.props.authService.logOut();
						}

						this.props.closePane();
						this.props.dispatch(updateData());
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	const { userPanelData, userPanelTab } = state.userPanel;
	return {
		userData,
		userPanelData,
		userPanelTab,
	};
}

export default connect(mapStateToProps)(UserPanelAccount);
