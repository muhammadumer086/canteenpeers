import React, { useState } from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import { TextFieldFormik } from '../inputs';
import ReactCodeInput from 'react-verification-code-input';

interface IProps {
	submitting: boolean;
	sentOnce: boolean;
	number: string;
	sendCode(first?: boolean): void;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	code: string;
}

export const VerifyBySmsForm: React.FC<IProps> = (props: IProps) => {
	const [code, setCode] = useState('');
	const validationFormat = Yup.object().shape({
		// code: Yup.string()
		// 	.length(6)
		// 	.matches(/^[0-9]*$/, 'Please enter numbers only')
		// 	.required('Please Enter 6 digit code'),
		code: Yup.string(),
	});
	const getHiddenNumber = () => {
		const number = props.number;
		const firstPart = number.substring(0, number.length - 7);
		const lastPart = number.substring(number.length - 3);
		return firstPart + '****' + lastPart;
	};
	return (
		<div>
			<Formik
				initialValues={{
					code: props.number,
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					//	isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form
							className="auth_form form hp-v0"
							onSubmit={handleSubmit}
						>
							{props.sentOnce ? (
								<div>
									<div className="form-row">
										<h3 className="auth_form-verify-heading">
											Enter code
										</h3>
									</div>
									<div className="form-row">
										<p className="auth_form-verify-text">
											Please enter code we send to{' '}
											{getHiddenNumber()}
										</p>
									</div>
									<div className="form-row">
										<div className="form-column hp-0">
											<ReactCodeInput
												onChange={c => {
													setCode(c);
												}}
												disabled={props.submitting}
												className="register_verify-code"
											/>
										</div>
									</div>

									<div className="form-row form-row--submit">
										<div className="form-column hp-0 hp-v8">
											<Button
												variant="contained"
												color="primary"
												className="form-submit"
												type="submit"
												onClick={e => {
													e.preventDefault();
													props.onSubmit({
														code: code,
													});
												}}
												fullWidth
												disabled={code.length !== 6}
											>
												Verify now
											</Button>
										</div>
									</div>
								</div>
							) : (
								<div>
									<div className="form-row">
										<h3 className="auth_form-verify-heading">
											Your mobile phone
										</h3>
									</div>
									<div className="form-row">
										<div className="form-column hp-0">
											<TextFieldFormik
												type="text"
												label=""
												name="code"
												disabled={true}
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row form-row--submit">
										<div className="form-column hp-h0">
											<Button
												variant="contained"
												color="primary"
												className="form-submit"
												onClick={() => {
													props.sendCode(true);
												}}
												fullWidth
											>
												Send Code
											</Button>
										</div>
									</div>
								</div>
							)}
						</form>
					);
				}}
			/>
		</div>
	);
};
