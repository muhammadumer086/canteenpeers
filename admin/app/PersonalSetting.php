<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalSetting extends Model {
	public static $BLOG_PUBLIC = 'blog-public';
	public static $ALLOW_DISPLAY_CANCER_TYPE = 'allow-display-cancer-type';
	public static $ALLOW_DIRECT_MESSAGE = 'allow-direct-messages';
	public static $ALLOW_DISPLAY_GENDER = 'allow-display-gender';
	public static $ALLOW_CONTACTED_RESEARCH = 'allow-contaced-research';
	public static $ALLOW_NEWSLETTER = 'allow-newsletter';
	public static $ALLOW_NOTIF_DISCUSSION_MENTION = 'allow-notification-discussion-mentions';
	public static $ALLOW_PUSH_NOTIF_DISCUSSION_MENTION = 'allow-push-notification-discussion-mentions';
	public static $ALLOW_EMAIL_DISCUSSION_MENTION = 'allow-email-discussion-mentions';
	public static $ALLOW_NOTIF_DISCUSSION_REPLIES = 'allow-notification-discussion-replies';
	public static $ALLOW_PUSH_NOTIF_DISCUSSION_REPLIES = 'allow-push-notification-discussion-replies';
	public static $ALLOW_EMAIL_DISCUSSION_REPLIES = 'allow-email-discussion-replies';
	public static $ALLOW_NOTIF_DISCUSSION_FOLLOWED = 'allow-notification-discussion-followed';
	public static $ALLOW_PUSH_NOTIF_DISCUSSION_FOLLOWED = 'allow-push-notification-discussion-followed';
	public static $ALLOW_EMAIL_DISCUSSION_FOLLOWED = 'allow-email-discussion-followed';
	public static $ALLOW_EMAIL_MESSAGES = 'allow-email-messages';
	public static $ALLOW_PUSH_NOTIF_MESSAGES = 'allow-push-notification-messages';
	public static $ALLOW_NOTIF_ALERTS = 'allow-notification-alerts';
	public static $ALLOW_PUSH_NOTIF_ALERTS = 'allow-push-notification-alerts';
	public static $ALLOW_EMAIL_ALERTS = 'allow-email-alerts';
	public static $ALLOW_NOTIF_BLOGS = 'allow-notification-blogs';
	public static $ALLOW_EMAIL_BLOGS = 'allow-email-blogs';

	public static function getDefaultSettings() {
		return [
			self::$BLOG_PUBLIC,
			self::$ALLOW_DISPLAY_CANCER_TYPE,
			self::$ALLOW_DISPLAY_GENDER,
			self::$ALLOW_DIRECT_MESSAGE,
			self::$ALLOW_CONTACTED_RESEARCH,
			self::$ALLOW_NEWSLETTER,
			self::$ALLOW_NOTIF_DISCUSSION_MENTION,
			self::$ALLOW_PUSH_NOTIF_DISCUSSION_MENTION,
			self::$ALLOW_EMAIL_DISCUSSION_MENTION,
			self::$ALLOW_NOTIF_DISCUSSION_REPLIES,
			self::$ALLOW_PUSH_NOTIF_DISCUSSION_REPLIES,
			self::$ALLOW_EMAIL_DISCUSSION_REPLIES,
			self::$ALLOW_NOTIF_DISCUSSION_FOLLOWED,
			self::$ALLOW_PUSH_NOTIF_DISCUSSION_FOLLOWED,
			self::$ALLOW_EMAIL_DISCUSSION_FOLLOWED,
			self::$ALLOW_EMAIL_MESSAGES,
			self::$ALLOW_PUSH_NOTIF_MESSAGES,
			self::$ALLOW_NOTIF_ALERTS,
			self::$ALLOW_PUSH_NOTIF_ALERTS,
			self::$ALLOW_EMAIL_ALERTS,
		];
	}

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'personal_settings';

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['id', 'created_at', 'updated_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['slug', 'name', 'category', 'order', 'type'];

	/**
	 * The users associated with the personal setting
	 */
	public function users() {
		return $this->belongsToMany('App\Users');
	}
}
