<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\UpdateUsersBans',
		'App\Console\Commands\AlgoliaIndex',
		'App\Console\Commands\UpdateCounts',
		'App\Console\Commands\ResendVerificationEmail',
		'App\Console\Commands\HandleNotificationMessageCooldowns',
		'App\Console\Commands\SynchronizeEvents',
		'App\Console\Commands\CacheFlush',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule) {
		$schedule->command('canteen:cacheFlush')->dailyAt('00:00');
		$schedule->command('canteen:updateUsersBans')->dailyAt('01:00');
		$schedule->command('canteen:algoliaIndex')->hourly();
		$schedule->command('canteen:updateCounts')->hourly();
		$schedule->command('canteen:resendVerificationEmail')->hourly();
		$schedule
			->command('canteen:handleNotificationMessageCooldowns')
			->everyMinute();
		$schedule->command('canteen:synchronizeEvents')->hourly();
		$schedule->command('canteen:deleteNotifications')->dailyAt('03:00');
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
