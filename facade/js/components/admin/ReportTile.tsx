import React from 'react';
import TimeAgo from 'react-timeago';
import { connect } from 'react-redux';

import ButtonBase from '@material-ui/core/ButtonBase';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Tooltip from '@material-ui/core/Tooltip';

import { IApiDiscussionReport, IReport, IApiUser } from '../../interfaces';

import {
	IStoreState,
	userPanelDisplay,
	apiError,
	apiSuccess,
} from '../../redux/store';

import { withServices, ApiService } from '../../services';

import DiscussionTile from '../discussions/DiscussionTile';
import { generateAndFormatTimezoneDate } from '../../../js/helpers/generateTimezoneDate';

interface IProps {
	deletePermission: boolean;
	allPermission: boolean;
	report: IApiDiscussionReport;
	apiService: ApiService;
	apiError(message: string[]): void;
	apiSuccess(message: string[]): void;
}

interface IState {
	moderated: boolean;
	loading: boolean;
}

interface IReportProps {
	deletePermission: boolean;
	allPermission: boolean;
	report: IReport;
	deleted: boolean;
	apiService: ApiService;
	userPanelDisplay(user: IApiUser): void;
	apiError(message: string[]): void;
	apiSuccess(message: string[]): void;
}

interface IReportState {
	deleted: boolean;
	loading: boolean;
}

class ReportContent extends React.Component<IReportProps, IReportState> {
	constructor(props) {
		super(props);

		this.state = {
			deleted: false,
			loading: false,
		};

		this.deleteReport = this.deleteReport.bind(this);
	}

	public render() {
		const { deletePermission } = this.props;
		const classes = ['report'];

		if (this.props.deleted || this.state.deleted) {
			classes.push('report--deleted');
		}

		const formatTimezoneDate = generateAndFormatTimezoneDate(
			this.props.report.created_at.date,
			'Australia/Sydney',
		);

		return (
			<div className={classes.join(' ')}>
				<div className="report_tile-content">
					<div className="report_tile-meta">
						<span>Reported by&nbsp;</span>
						<span>
							<ButtonBase
								className="theme-text--accent"
								onClick={() => {
									this.props.userPanelDisplay(
										this.props.report.user,
									);
								}}
							>
								{this.props.report.user.full_name} (
								{this.props.report.user.email})
							</ButtonBase>
							&nbsp;
						</span>
						<span>
							<TimeAgo date={formatTimezoneDate} />
						</span>
					</div>
					<div className="report_tile-title font--h5 theme-title">
						{this.props.report.title}
					</div>
					{!!this.props.report.comment && (
						<div className="report_tile-comment hm-t8">
							{this.props.report.comment}
						</div>
					)}
				</div>
				<div className="report_tile-actions">
					{!(this.props.deleted || this.state.deleted) && (
						<React.Fragment>
							{this.state.loading ? (
								<div className="report-action-loading">
									<CircularProgress size={24} />
								</div>
							) : (
								deletePermission && (
									<Tooltip title="Delete the report">
										<IconButton onClick={this.deleteReport}>
											<Icon>delete</Icon>
										</IconButton>
									</Tooltip>
								)
							)}
						</React.Fragment>
					)}
				</div>
			</div>
		);
	}

	protected deleteReport() {
		this.setState(
			{
				loading: true,
			},
			() => {
				this.props.apiService
					.queryDELETE(
						`/api/report-discussions/${this.props.report.id}`,
					)
					.then(() => {
						this.setState({
							deleted: true,
							loading: false,
						});
						this.props.apiSuccess(['Report successfully removed']);
					})
					.catch(error => {
						this.setState({
							loading: false,
						});
						this.props.apiError(error);
					});
			},
		);
	}
}

function mapStateToPropsReport(_state: IStoreState) {
	return {};
}

function mapDispatchToPropsReport(dispatch) {
	return {
		userPanelDisplay: (user: IApiUser) => dispatch(userPanelDisplay(user)),
		apiError: (message: string[]) => dispatch(apiError(message)),
		apiSuccess: (message: string[]) => dispatch(apiSuccess(message)),
	};
}

const Report = connect(
	mapStateToPropsReport,
	mapDispatchToPropsReport,
)(withServices(ReportContent));

class ReportTile extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			moderated: false,
			loading: false,
		};

		this.deletePost = this.deletePost.bind(this);
		this.ageSensitivePost = this.ageSensitivePost.bind(this);
	}

	public render() {
		const { deletePermission, allPermission } = this.props;
		const classes = ['report_tile'];

		if (this.state.moderated) {
			classes.push('report_tile--deleted');
		}

		return (
			<div className={classes.join(' ')}>
				<DiscussionTile
					discussion={this.props.report.discussion}
					noActions={true}
					hideReport={true}
					multiline={true}
				/>
				{!this.state.moderated && (
					<div className="report_tile-action">
						{this.renderTileAction()}
					</div>
				)}
				<div className="report_tile-reports theme--main">
					<Report
						deletePermission={deletePermission}
						allPermission={allPermission}
						report={this.props.report}
						deleted={this.state.moderated}
					/>
					{this.props.report.reports.map(report => {
						return (
							<Report
								deletePermission={deletePermission}
								allPermission={allPermission}
								report={report}
								deleted={this.state.moderated}
								key={report.id}
							/>
						);
					})}
				</div>
			</div>
		);
	}

	protected renderTileAction() {
		if (this.state.loading) {
			return (
				<div className="report_tile-action-loading">
					<CircularProgress size={24} />
				</div>
			);
		}

		if (this.props.report.age_sensitive && this.props.allPermission) {
			return (
				<Tooltip title="Make the discussion age sensitive">
					<IconButton onClick={this.ageSensitivePost}>
						<Icon>priority_high</Icon>
					</IconButton>
				</Tooltip>
			);
		}
		if (this.props.deletePermission) {
			return (
				<Tooltip title="Delete the discussion">
					<IconButton onClick={this.deletePost}>
						<Icon>delete</Icon>
					</IconButton>
				</Tooltip>
			);
		}
	}

	protected deletePost() {
		this.setState(
			{
				loading: true,
			},
			() => {
				this.props.apiService
					.queryDELETE(
						`/api/discussions/${this.props.report.discussion.id}`,
					)
					.then(() => {
						this.setState({
							moderated: true,
							loading: false,
						});
						this.props.apiSuccess([
							'Discussion successfully removed',
						]);
					})
					.catch(error => {
						this.setState({
							loading: false,
						});
						this.props.apiError(error);
					});
			},
		);
	}

	protected ageSensitivePost() {
		this.setState(
			{
				loading: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/discussions/${this.props.report.discussion.id}`,
						{
							ageSensitive: true,
						},
					)
					.then(() => {
						this.setState({
							moderated: true,
							loading: false,
						});
						this.props.apiSuccess([
							'Discussion successfully set as age-sensitive',
						]);
					})
					.catch(error => {
						this.setState({
							loading: false,
						});
						this.props.apiError(error);
					});
			},
		);
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

function mapDispatchToProps(dispatch) {
	return {
		apiError: (message: string[]) => dispatch(apiError(message)),
		apiSuccess: (message: string[]) => dispatch(apiSuccess(message)),
	};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(withServices(ReportTile));
