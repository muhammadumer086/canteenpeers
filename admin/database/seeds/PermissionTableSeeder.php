<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$roles = [
			'user' => [
				'app-read-authenticated',
			],
			'admin' => [
				'app-manage',
				'app-read-authenticated',
			],
		];

		foreach ($roles as $roleName => $permissions) {
			if (!Role::where('name', '=', $roleName)->exists()) {
				Role::create(['name' => $roleName]);
			}
			$role = Role::where('name', '=', $roleName)->first();

			if (sizeof($permissions)) {
				foreach ($permissions as $permissionName) {
					if (!Permission::where('name', '=', $permissionName)->exists()) {
						Permission::create(['name' => $permissionName]);
					}
					$permission = Permission::where('name', '=', $permissionName)->first();
					if (!$role->hasPermissionTo($permission)) {
						$role->givePermissionTo($permission);
					}
				}
			}
		}
	}
}
