@extends('layouts.email')

@section('plain_text')
	@component('emails.components.paragraph')
		Community member {{$user->first_name}} {{$user->last_name}} has registered for {{$event->title}}.
	@endcomponent
@endsection

@section('content')
	@component('emails.components.paragraph')
		Community member {{$user->first_name}} {{$user->last_name}} ({{$user->username}} - {{$user->email}}) has registered for {{$event->title}}.
	@endcomponent

	@component('emails.components.paragraph')
		Contact phone: {{ $phone }}
	@endcomponent

	@component('emails.components.paragraph')
		{{ $content }}
	@endcomponent

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
