import React from 'react';
import moment from 'moment';

interface IProps {
	messageDateTime: any;
}

const MessageDateTime = (props: IProps) => {

	const updatedAtTimeframe = () => {
		const updatedAt = moment(props.messageDateTime);

		if (updatedAt.isSame(moment(), 'day')) {
			return updatedAt.format("h:mma");
		} else if (updatedAt.isSame(moment(), 'week')) {
			return updatedAt.format('dddd');
		} else {
			return updatedAt.format('DD/MM/YYYY');
		}
	}

	return (
		<div className="message_date_time">
			<span>{updatedAtTimeframe()}</span>
		</div>
	)
}

export default MessageDateTime;

