<?php

namespace App\Http\Resources;

use App\StaffImage;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StaffCollection extends ResourceCollection
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'data' => ['staffs' => $this->collection]
		];
	}
}
