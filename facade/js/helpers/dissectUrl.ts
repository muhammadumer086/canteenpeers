interface IDissectedLink {
	type: string;
	href: string;
	hrefAs: string;
}

export enum EStaticLinkType {
	LINK = 'LINK',
	EXTERNAL = 'EXTERNAL',
	CUSTOM = 'CUSTOM',
}

function splitUrlPath(url: string): string[] | null {
	const split = url.split('/');

	if (split.length) {
		const removeEmpty = split.filter(path => {
			if (path !== '') {
				return path;
			}
		});

		return removeEmpty;
	}

	return null;
}

function formatUrlType(
	splitPath: string[],
	sectionReference: string,
): IDissectedLink {
	let formattedLink: IDissectedLink = {
		type: EStaticLinkType.LINK,
		hrefAs: `/${sectionReference}`,
		href: `/${sectionReference}`,
	};

	if (splitPath.length > 1) {
		if (splitPath.length > 2) {
			if (splitPath[1] === 'user') {
				formattedLink = {
					type: EStaticLinkType.LINK,
					hrefAs: `/${sectionReference}/user/${splitPath[2]}`,
					href: `/${sectionReference}-user?id=${splitPath[2]}`,
				};
			}
		} else {
			formattedLink = {
				type: EStaticLinkType.LINK,
				hrefAs: `/${sectionReference}/${splitPath[1]}`,
				href: `/${sectionReference}-single?id=${splitPath[1]}`,
			};
		}
	}

	return formattedLink;
}

export function dissectUrl(url: string): IDissectedLink | null {
	if (!url || url === '') {
		return null;
	}
	let urlInformation: IDissectedLink | null = null;
	const regExp = new RegExp(process.env.APP_URL + '|(?:^/)');
	const isInternal = regExp.test(url);

	if (isInternal) {
		// TODO remove host
		const regExpHost = new RegExp(process.env.APP_URL);
		const regExpQueryParams = /\?.*$/;
		// Remove host from url
		url = url.replace(regExpHost, '');
		// Remove query params from URL
		url = url.replace(regExpQueryParams, '');

		const regExpBlogSection = /^\/blogs(?:\/.*)?/;
		const regExpResourcesSection = /^\/resources(?:\/.*)?/;
		const regExpDiscusssionsSection = /^\/discussions(?:\/.*)?/;
		const regExpEventsSection = /^\/events(?:\/.*)?/;
		const regExpPeopleLikeMeSection = /^\/people-like-me(?:\/.*)?/;
		const regExpMeetTheTeamSection = /^\/meet-the-team(?:\/.*)?/;

		// Case for the home
		if (url === '' || url === '/') {
			// It's the homepage
			const urlData = {
				type: EStaticLinkType.LINK,
				hrefAs: `/`,
				href: `/`,
			};

			return urlData;
		} else {
			const splitPath = splitUrlPath(url);
			const urlInformation = formatUrlType(splitPath, splitPath[0]);

			if (regExpBlogSection.test(url)) {
				return urlInformation;
			} else if (regExpResourcesSection.test(url)) {
				return urlInformation;
			} else if (regExpDiscusssionsSection.test(url)) {
				return urlInformation;
			} else if (regExpEventsSection.test(url)) {
				return urlInformation;
			} else if (regExpPeopleLikeMeSection.test(url)) {
				return urlInformation;
			} else if (regExpMeetTheTeamSection.test(url)) {
				return urlInformation;
			}
		}
	} else {
		// If link is external
		urlInformation = {
			type: EStaticLinkType.EXTERNAL,
			href: url,
			hrefAs: url,
		};

		return urlInformation;
	}
	return null;
}
