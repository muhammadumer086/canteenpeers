<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UserActivityFacade extends Facade {
    protected static function getFacadeAccessor() {
        return 'userActivityHelper';
    }
}