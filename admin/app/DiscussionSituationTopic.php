<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscussionSituationTopic extends Model {

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'discussion_situation_topic';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * The discussions related to the situation topic
	 */
	public function discussions() {
		return $this->belongsTo('App\Discussion');
	}

	/**
	 * The blogs related to the situation topic
	 */
	public function situation_topics() {
		return $this->belongsTo('App\SituationTopic');
	}

}
