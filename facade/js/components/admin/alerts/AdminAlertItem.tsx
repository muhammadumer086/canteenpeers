import React from 'react';
import moment from 'moment';

import { IconButton, Icon, Menu, MenuItem } from '@material-ui/core';
import { IApiAnnouncement } from '../../../interfaces';
import Flag from '../../general/Flag';

interface IProps {
	data: IApiAnnouncement;
	toggleEditAlertModal(data: IApiAnnouncement): void;
	toggleDeleteAlertModal(data: IApiAnnouncement): void;
}

interface IState {
	anchorEl: any | null;
}

class AdminAlertItem extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
		};

		this.handleMenuClose = this.handleMenuClose.bind(this);
		this.handleMenuOpen = this.handleMenuOpen.bind(this);
	}

	public render() {
		const className = ['edit_alert', 'theme--main'];

		if (!!this.props.data.feature_image) {
			className.push('edit_alert--with_image');
		}

		const createdAt = moment(this.props.data.created_at.date);

		const onDeleteClick = () => {
			this.props.toggleDeleteAlertModal(this.props.data);
			this.handleMenuClose();
		};

		const onEditClick = () => {
			this.props.toggleEditAlertModal(this.props.data);
			this.handleMenuClose();
		};

		const styles = {};

		if (!!this.props.data.feature_image) {
			styles[
				'backgroundImage'
			] = `url(${this.props.data.feature_image.url})`;
		}
		return (
			<div className={className.join(' ')}>
				<div className="edit_alert-image_container">
					<div className="edit_alert-image" style={styles} />
				</div>

				<div className="edit_alert-body">
					{(this.props.toggleEditAlertModal ||
						this.props.toggleDeleteAlertModal) && (
						<div className="edit_alert-actions">
							<IconButton
								className="discussion_report-button"
								onClick={this.handleMenuOpen}
							>
								<Icon>more_vert</Icon>
							</IconButton>
							<Menu
								anchorEl={this.state.anchorEl}
								open={Boolean(this.state.anchorEl)}
								onClose={this.handleMenuClose}
							>
								{this.props.toggleEditAlertModal && (
									<MenuItem onClick={onEditClick}>
										Edit Alert
									</MenuItem>
								)}
								{this.props.toggleDeleteAlertModal && (
									<MenuItem onClick={onDeleteClick}>
										Delete Alert
									</MenuItem>
								)}
							</Menu>
						</div>
					)}

					<div className="edit_alert-content">
						{!!this.props.data.public && (
							<div className="edit_alert-flags">
								<Flag>Public</Flag>
							</div>
						)}
						<h5 className="font--h5 theme-title edit_alert-title">
							{this.props.data.title}
						</h5>
						<div className="edit_alert-message">
							<div
								className=" markdown markdown--inline"
								dangerouslySetInnerHTML={{
									__html: this.props.data.message,
								}}
							/>
						</div>

						<span className="edit_alert-created">
							Created at {createdAt.format('MMMM D, YYYY')}
						</span>
					</div>
				</div>
			</div>
		);
	}

	protected handleMenuClose() {
		this.setState({
			anchorEl: null,
		});
	}

	protected handleMenuOpen(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}
}

export default AdminAlertItem;
