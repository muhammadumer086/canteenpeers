<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Situation as SituationResource;

class Language extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('language', $this->id, $this->updated_at, true, false)) {
			return $apiStore->getApiResource('language', $this->id, $this->updated_at, true, false);
		}

		$params = [
			'slug',
			'name',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$apiStore->setApiResource($result, 'language', $this->id, $this->updated_at, true, false);

		return $result;
	}
}
