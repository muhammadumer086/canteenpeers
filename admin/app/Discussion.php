<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Laravel\Scout\Searchable;

use App\Topic;
use App\Hashtag;
use App\Mention;
use App\User;

use App\Notifications\UserMentionedNotification;

use App\Traits\AuthUser;
use App\Traits\HasHashtags;
use App\Traits\AlgoliaIndex;
use App\Traits\HasSeoAttribute;

class Discussion extends Model {
	use Searchable;
	use SoftDeletes;
	use AlgoliaIndex;
	use AuthUser;
	use HasHashtags;
	use HasSeoAttribute;

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
		'last_activity_at',
	];

	/**
	 * All of the relationships to be touched.
	 *
	 * @var array
	 */
	protected $touches = ['mainDiscussion', 'replyToDiscussion', 'user'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'migration_id',
		'user_id',
		'title',
		'slug',
		'content',
		'main_discussion_id',
		'feature_image',
		'reply_to_id',
		'created_at',
		'last_activity_at',
		'for_event',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = [
		'likes_count',
		'hugs_count',
		'replies_count',
		'thread_count',
		// 'topics',
		'followed',
		'liked',
		'hugged',
		'seo',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'user_id',
		'likes',
		'hugs',
		'deleted_at',
		'main_discussion_id',
		'reply_to_id',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'private' => 'boolean',
		'age_sensitive' => 'boolean',
		'reply_to_id' => 'integer',
		'main_discussion_id' => 'integer',
	];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'discussions';

	/**
	 * Get the index name for the model.
	 *
	 * @return string
	 */
	public function searchableAs() {
		return $this->algoliaIndex();
	}

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {
		if ($this->main_discussion_id === null && !$this->private) {
			$type = str_slug((new \ReflectionClass($this))->getShortName());
			$id = $this->slug;
			$image = null;
			$title = $this->title;
			$excerpt = str_limit(strip_tags($this->content), 250);
			$data = str_limit(strip_tags($this->content), 5000);
			$hashtags = $this->hashtags()
				->pluck('name')
				->toArray();

			if (
				$this->feature_image &&
				is_array($this->feature_image) &&
				isset($this->feature_image['url'])
			) {
				$image = $this->feature_image['url'];
			}

			// Result structure
			$result = [
				'type' => $type,
				'id' => $id,
				'image' => $image,
				'title' => $title,
				'excerpt' => $excerpt,
				'data' => $data,
				'hashtags' => $hashtags,
			];

			return $result;
		}

		return [];
	}

	/**
	 * Get the value used to index the model.
	 *
	 * @return mixed
	 */
	public function getScoutKey() {
		return $this->algolia_id;
	}

	public function reportNumbers() {
		$this->count_likes = $this->likes()->count();
		$this->count_hugs = $this->hugs()->count();

		if ($this->main_discussion_id) {
			// Reply to a discussion
			$this->count_replies = $this->repliesDiscussions()->count();
		} else {
			// Main discussion
			$this->count_replies = $this->childDiscussions()->count();
		}

		$this->count_direct_replies = $this->repliesDiscussions()->count();

		$this->count_thread = $this->repliesThread()->count();

		$this->count_report = $this->reportDiscussions()->count();

		$this->count_view = $this->userActivities()
			->where('type', 'Discussion Read')
			->count();
	}

	/**
	 * The user that created this discussion
	 */
	public function user() {
		return $this->belongsTo('App\User')->withTrashed();
	}

	/**
	 * The likes associated with the discussion
	 */
	public function likes() {
		return $this->hasMany('App\Like');
	}

	/**
	 * The hugs associated with the discussion
	 */
	public function hugs() {
		return $this->hasMany('App\Hug');
	}

	public function userLikes() {
		return $this->belongsToMany('App\User', 'likes');
	}

	public function userHugs() {
		return $this->belongsToMany('App\User', 'hugs');
	}

	/**
	 * The following associated with the user/discusions
	 */
	public function followingUsers() {
		return $this->belongsToMany(
			'App\User',
			'followings',
			'discussion_id',
			'user_id'
		);
	}

	/**
	 * The reports attached to the discussions
	 */
	public function reportDiscussions() {
		return $this->hasMany('App\ReportDiscussion');
	}

	/**
	 * The user that created this discussion
	 */
	public function topic() {
		return $this->belongsTo('App\Topic');
	}

	/**
	 * The situation topics related to this discussion
	 */
	public function situations() {
		return $this->belongsToMany('App\Situation');
	}

	/**
	 * The main discussion
	 */
	public function mainDiscussion() {
		return $this->belongsTo('App\Discussion', 'main_discussion_id');
	}

	/**
	 * The users in the discussion
	 */
	public function usersInDiscussion() {
		return $this->belongsToMany('App\User', 'users_in_discussion');
	}

	public function supportUsersInDiscussion() {
		return $this->belongsToMany(
			'App\User',
			'support_users_in_discussion'
		)->withTimestamps();
	}

	public function removeSupport($userId, $supportType) {
		$support = DB::table('support_users_in_discussion')
			->where('user_id', $userId)
			->where('discussion_id', $this->id)
			->first();

		if ($support) {
			$shouldDelete =
				($supportType === 'reply' &&
					!$support->like &&
					!$support->hug) ||
				($supportType === 'like' &&
					!$support->reply &&
					!$support->hug) ||
				($supportType === 'hug' && !$support->like && !$support->reply);

			if ($shouldDelete) {
				DB::table('support_users_in_discussion')
					->where('user_id', $userId)
					->where('discussion_id', $this->id)
					->delete();
			} else {
				$this->supportUsersInDiscussion()->syncWithoutDetaching([
					$userId => [$supportType => false],
				]);
			}
		}
	}

	/**
	 * The reply to discussion
	 */
	public function replyToDiscussion() {
		return $this->belongsTo('App\Discussion', 'reply_to_id');
	}

	/**
	 * The child discussion of the main discussion
	 */
	public function childDiscussions() {
		return $this->hasMany('App\Discussion', 'main_discussion_id');
	}

	/**
	 * The replies to the discussion
	 */
	public function repliesDiscussions() {
		return $this->hasMany('App\Discussion', 'reply_to_id');
	}

	public function repliesThread() {
		return $this->hasMany('App\Discussion', 'thread_id');
	}

	public function userActivities() {
		return $this->morphMany('App\UserActivity', 'activity');
	}

	public function getFollowedAttribute() {
		$authUser = $this->getAuthUser();
		if ($authUser && !$this->main_discussion_id) {
			return $authUser
				->followedDiscussions()
				->where('followings.discussion_id', $this->id)
				->exists();
		}

		return false;
	}

	public function getLikedAttribute() {
		$authUser = $this->getAuthUser();
		if ($authUser) {
			return $authUser
				->likedDiscussions()
				->where('likes.discussion_id', $this->id)
				->exists();
		}
		return false;
	}

	public function getHuggedAttribute() {
		$authUser = $this->getAuthUser();
		if ($authUser) {
			return $authUser
				->huggedDiscussions()
				->where('hugs.discussion_id', $this->id)
				->exists();
		}
		return false;
	}

	/**
	 * Get the number of likes
	 *
	 * @return number
	 */
	public function getLikesCountAttribute() {
		return $this->count_likes;
	}

	/**
	 * Get the number of hugs
	 *
	 * @return number
	 */
	public function getHugsCountAttribute() {
		return $this->count_hugs;
	}

	public function getRepliesCountAttribute() {
		return $this->count_replies;
	}

	public function getDirectRepliesCountAttribute() {
		return $this->count_direct_replies;
	}

	public function getThreadCountAttribute() {
		return $this->count_thread;
	}

	public function getReportCountAttribute() {
		return $this->count_report;
	}

	public function getViewCountAttribute() {
		return $this->count_view;
	}

	/**
	 * Get the number of views
	 *
	 * @return bool
	 */
	public function getViewsCountAttribute() {
		return $this->userActivities()
			->where('type', 'Discussion Read')
			->count();
	}

	public function getAlgoliaIdAttribute() {
		$type = str_slug((new \ReflectionClass($this))->getShortName());
		return "$type/{$this->id}";
	}

	public function getMentions() {
		preg_match_all('/@([\w\-]+)/', $this->content, $matches);
		return $matches[1];
	}

	public function mentions() {
		return $this->morphToMany('App\Mention', 'mentionable');
	}
}
