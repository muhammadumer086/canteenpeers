import React from "react";
import { ModalDialog } from '../../modals/local/ModalDialog';
import {IApiPermission} from "../../../interfaces";
import PermissionGrid from "./PermissionGrid";
import { ApiService } from '../../../services';

interface IProps {
    handleClose():void;
    apiService : ApiService;
};
interface IState {
    permission:IApiPermission[];
};

class PermissionModal extends React.Component<IProps, IState>{
    constructor(p: IProps) {
        super(p);
        this.state={
            permission : [],
        }
    }
    public render() {
        return (
            <ModalDialog
                isActive={true}
                modalTitle="Are you sure you want to delete this Role?"
                ariaTag="event-delete-modal"
                handleClose={this.props.handleClose}
            >
               <PermissionGrid role={{name : "",id:1,guard_name:"",created_at:"",updated_at:""}} apiService={this.props.apiService}  />
            </ModalDialog>
        );
    }
}
export default PermissionModal;