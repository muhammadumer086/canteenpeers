<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Discussion;
use App\Notifications\UserMentionedNotification;

class Mention extends Model {
	protected $fillable = [
		'mentioned_username',
		'mentioned_user_id',
		'sender_user_id',
	];

	protected $hidden = ['created_at', 'updated_at'];

	public function discussion() {
		return $this->morphTo('App\Discussion', 'mentionable');
	}

	public function mentioned() {
		return $this->belongsTo('App\User', 'mentioned_user_id');
	}

	public function sender() {
		return $this->belongsTo('App\User', 'sender_user_id');
	}
}
