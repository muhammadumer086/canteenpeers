import React from 'react';
import { ButtonBase } from '@material-ui/core';
import ReactSVG from 'react-svg';
import EditGroupMessageFormik, {
	IEditGroupMessageFormikFormValues,
} from './EditGroupMessageFormik';
import { IApiConversationDetail, IApiUser } from 'js/interfaces';

interface IProps {
	isUpdating: boolean;
	activeUsers: IApiUser[];
	conversationInformationActive: boolean;
	conversationDetail: IApiConversationDetail;
	updateConversation(
		payload: IEditGroupMessageFormikFormValues,
	): Promise<boolean>;
	toggleConversationDetail(): void;
	onConversationLeave(): void;
}

const MessageInformation: React.StatelessComponent<IProps> = props => {
	const arrowIconUrl = `${process.env.STATIC_PATH}/icons/back-arrow.svg`;
	const imageDimensions = {
		width: 24,
		height: 24,
	};

	return (
		<div
			className={`user_message_sidepanel-information_panel ${
				props.conversationInformationActive
					? 'user_message_sidepanel-information_panel--active'
					: ''
			}`}
		>
			{
				<div className="user_message_sidepanel-information_container">
					<div className="user_messages-message_information">
						<div className="user_message_sidepanel-close_detail">
							<ButtonBase
								disableRipple={true}
								onClick={props.toggleConversationDetail}
							>
								<ReactSVG
									path={arrowIconUrl}
									svgStyle={{ ...imageDimensions }}
								/>
							</ButtonBase>
						</div>
						<span>Group Chat Detail</span>
					</div>
					<div style={{ position: 'relative' }}>
						{props.conversationInformationActive && (
							<EditGroupMessageFormik
								isUpdating={props.isUpdating}
								activeUsers={props.activeUsers}
								conversationDetail={props.conversationDetail}
								submit={props.updateConversation}
								onLeaveConversation={props.onConversationLeave}
							/>
						)}
					</div>
				</div>
			}
		</div>
	);
};

export default MessageInformation;
