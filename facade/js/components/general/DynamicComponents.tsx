import React from 'react';
import dynamic from 'next/dynamic';
import EventRegisterInterestModal from '../events/EventRegisterInterestModal';

/**
 * Used for any of the modals and panels present in the application
 */
const loading=()=>{
	return null;
}
const DynamicComponents = dynamic(
	({
		modules: () => {
			const components = {
				DiscussionModal: import('../discussions/DiscussionModal'),
				SupportModal: import('../discussions/SupportModal'),
				ReportDiscussionModal: import(
					'../discussions/ReportDiscussionModal'
				),
				QuickLinksModal: import('../dashboard/QuickLinksModal'),
				UserPanel: import('../user-panel/UserPanel'),
				Notifications: import('../notifications/Notifications'),
				ContactModal: import('../contact/ContactModal'),
				SearchModal: import('../general/SearchModal'),
				CreateNewMessageModal: import(
					'../messages/CreateNewMessageModal'
				),
				NewBlogModal: import('../blogs/CreateNewBlogModal'),
				LandingVideoModal: import('../landing/LandingVideoModal'),
				EventRegisterInterestModal: import(
					'../events/EventRegisterInterestModal'
				),
				WelcomeModal: import('../general/WelcomeModal'),
			};

			return components;
		},
		render: (
			_props,
			{
				DiscussionModal,
				SupportModal,
				ReportDiscussionModal,
				QuickLinksModal,
				UserPanel,
				Notifications,
				ContactModal,
				SearchModal,
				CreateNewMessageModal,
				NewBlogModal,
				LandingVideoModal,
				WelcomeModal,
			},
		) => {
			return (
				<React.Fragment>
					<DiscussionModal />
					<SupportModal />
					<ReportDiscussionModal />
					<QuickLinksModal />
					<UserPanel />
					<Notifications />
					<ContactModal />
					<SearchModal />
					<CreateNewMessageModal />
					<NewBlogModal />
					<LandingVideoModal />
					<EventRegisterInterestModal />
					<WelcomeModal />
				</React.Fragment>
			);
		},
	} as any) as Promise<any>,
	{
		ssr: false,
		loading
	},
);

export default DynamicComponents;
