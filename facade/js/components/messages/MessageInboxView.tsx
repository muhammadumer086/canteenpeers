import React from 'react';
import ReactMarkdown from 'react-markdown';
import { connect } from 'react-redux';
import moment from 'moment';

import ButtonBase from '@material-ui/core/ButtonBase';

import {
	IApiConversationDetail,
	IApiNotification,
	IApiUser,
} from '../../interfaces';

import { getExcerpt } from '../../helpers/getExcerpt';

import { IStoreState } from '../../redux/store';

import UserAvatar from '../general/UserAvatar';
import { Avatar } from '@material-ui/core';

interface IProps {
	messageData: IApiConversationDetail;
	isActive: boolean;
	notificationsData: IApiNotification[] | null;
	userData: IApiUser | null;
	inboxAction(id: number): void;
}

const MessageInboxView: React.StatelessComponent<IProps> = props => {
	const classNames = ['message_inbox_view'];

	const getUserName = () => {
		if (props.messageData.name && props.messageData.name.length) {
			return props.messageData.name;
		}
		const limit = 2;
		const userNames = [];
		const userData = props.messageData.users.filter(user => {
			if (props.userData && user.id !== props.userData.id) {
				return true;
			}
			return false;
		});

		userData.slice(0, limit).forEach(data => {
			userNames.push(data.full_name);
		});

		if (userData.length > limit) {
			return `${userNames.join(', ')} + ${userData.length - 2}`;
		}

		return `${userNames.join(' and ')}`;
	};

	const updatedAtTimeframe = () => {
		const updatedAt = moment(
			props.messageData.last_message.updated_at.date,
		);

		if (updatedAt.isSame(moment(), 'day')) {
			return updatedAt.format('h:mma');
		} else if (updatedAt.isSame(moment(), 'week')) {
			return updatedAt.format('dddd');
		} else {
			return updatedAt.format('DD/MM/YYYY');
		}
	};

	const excerpt = getExcerpt(props.messageData.last_message.message, 16);

	if (props.isActive) {
		classNames.push('message_inbox_view--is_current_message');
	}

	if (
		props.notificationsData &&
		props.notificationsData.find(notification => {
			return (
				notification.type === 'MessageReceivedNotification' &&
				notification.data &&
				notification.data.hasOwnProperty('conversation') &&
				notification.data.conversation.id &&
				notification.read_at === null &&
				notification.data.conversation.id === props.messageData.id
			);
		})
	) {
		classNames.push('message_inbox_view--unread_messages');
	}

	const users = props.messageData.users.filter(user => {
		if (props.userData && user.id !== props.userData.id) {
			return true;
		}
		return false;
	});

	const isGroupChat = users.length > 1;
	const groupChatIconUrl = `${process.env.STATIC_PATH}/icons/group_chat_white.svg`;

	if (isGroupChat) {
		classNames.push('message_inbox_view--group');
	}

	return (
		<div className={classNames.join(' ')}>
			<ButtonBase
				focusRipple={true}
				onClick={() => {
					props.inboxAction(props.messageData.id);
				}}
				className="message_inbox_view-button"
			/>

			<div className="message_inbox_view-avatar_container">
				<div
					className={`message_inbox_view-avatar ${
						isGroupChat ? 'message_inbox_view-avatar--group' : ''
					}`}
				>
					{isGroupChat && !!users.length ? (
						<Avatar
							className="message_inbox_view-avatar_group"
							src={groupChatIconUrl}
						/>
					) : (
						<UserAvatar user={users[0]} />
					)}
				</div>
			</div>

			<div className="message_inbox_view-meta">
				<div className="message_inbox_view-information">
					<span className="message_inbox_view-usernames font--small">
						{getUserName()}
					</span>
					<span className="message_inbox_view-time">
						{updatedAtTimeframe()}
					</span>
				</div>

				<div className="message_inbox_view-message_content">
					<ReactMarkdown
						className="markdown markdown--inline font--small"
						source={excerpt}
						linkTarget="_blank"
					/>
				</div>
			</div>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { notificationsData } = state.notifications;
	const { userData } = state.user;

	return {
		notificationsData,
		userData,
	};
}

export default connect(mapStateToProps)(MessageInboxView);
