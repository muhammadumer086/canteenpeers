import React from 'react';
import Router from 'next/router';

import LinearProgress from '@material-ui/core/LinearProgress';

import { AuthService } from '../../services/AuthService';

interface IProps {
	authService: AuthService;
}

interface IState {
	loading: boolean;
}

class PageLoader extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
		};

		this.startLoading = this.startLoading.bind(this);
		this.endLoading = this.endLoading.bind(this);

		Router.events.on('beforeHistoryChange' , newUrl => {
			if (!this.props.authService.shouldRedirectToAuth(newUrl)) {
				return false;
			}
			return true;
		});

		Router.events.on('routeChangeStart' ,() => {
			this.startLoading();
		});
		Router.events.on('routeChangeComplete',() => {
			this.endLoading();
		});
		Router.events.on('routeChangeError' , () => {
			this.endLoading();
		});
	}

	public render() {
		return (
			<div className="page_loader_wrapper">
				{!!this.state.loading && (
					<LinearProgress color="secondary" className="page_loader" />
				)}
			</div>
		);
	}

	protected startLoading() {
		this.setState({
			loading: true,
		});
	}

	protected endLoading() {
		this.setState({
			loading: false,
		});
		if (typeof window !== 'undefined') {
			const dashboardContainer = document.querySelector(
				'.layout_dashboard-content_container',
			);
			if (dashboardContainer) {
				dashboardContainer.scrollTo(0, 0);
			}
		}
	}
}

export default PageLoader;
