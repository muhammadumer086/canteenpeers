import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';

import { Button } from '@material-ui/core';

import { IApiTeam, IApiStaff } from '../../../../../js/interfaces';
import {
	TextFieldFormik,
	ImageDropzoneFormik,
} from '../../../../../js/components/inputs';
import Pill from '../../../../../js/components/general/Pill';
import { IFormValues } from './CreateStaffFormik';

interface IProps {
	isSubmitting: boolean;
	teams: IApiTeam[];
	values: IApiStaff;
	onSubmit(values: IFormValues);
}

const EditStaffFormik: React.StatelessComponent<IProps> = props => {
	const validationSchema = Yup.object().shape({
		first_name: Yup.string().required('A first name is required'),
		last_name: Yup.string().required('A last name is required'),
		team: Yup.string().required('A team is required'),
		role: Yup.string().required('A company role is required'),
		biography: Yup.string()
			.required('A biography is required')
			.max(500),
		image_id: Yup.string(),
		feature_image: Yup.object().shape({
			dimensions: Yup.object().shape({
				width: Yup.number(),
				height: Yup.number(),
			}),
			alt: Yup.string().nullable(true),
			copyright: Yup.string().nullable(true),
			url: Yup.string(),
		}),
	});

	return (
		<Formik
			initialValues={{
				first_name: props.values.first_name,
				last_name: props.values.last_name,
				team: props.values.team.slug,
				role: props.values.role,
				biography: props.values.biography,
				image_id: props.values.image_id,
				feature_image: props.values.feature_image,
			}}
			validationSchema={validationSchema}
			onSubmit={props.onSubmit}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				handleSubmit,
				setFieldValue,
				setFieldTouched,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
				};

				const dropzoneClasses = ['discussion_modal-dropzone'];

				if ('feature_image' in values && values.feature_image) {
					dropzoneClasses.push('discussion_modal-dropzone--image');
				}

				return (
					<form
						onSubmit={handleSubmit}
						className="create_staff_form form"
					>
						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="First name"
									name="first_name"
									disabled={props.isSubmitting}
									{...inputProps}
								/>
							</div>

							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="Last name"
									name="last_name"
									disabled={props.isSubmitting}
									{...inputProps}
								/>
							</div>
						</div>

						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="Role"
									name="role"
									disabled={props.isSubmitting}
									{...inputProps}
								/>
							</div>
						</div>

						<div className="form-row">
							<div className="form-column">
								<ImageDropzoneFormik
									values={inputProps.values}
									dropzoneClasses={dropzoneClasses}
									setFieldValue={inputProps.setFieldValue}
									apiUrl="/api/staff-images"
									label="Set the staff profile image"
								/>
							</div>
						</div>

						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="Biography"
									name="biography"
									multiline={true}
									disabled={props.isSubmitting}
									{...inputProps}
								/>
							</div>
						</div>

						<div className="form-row">
							<div className="form-column">
								<div className="form-input-label">Team</div>

								{props.teams.map((team, index) => {
									const isActive =
										inputProps.values.team === team.slug;

									return (
										<Pill
											key={index}
											clickable={true}
											active={isActive}
											disabled={props.isSubmitting}
											label={team.name}
											onClick={() => {
												inputProps.setFieldValue(
													'team',
													team.slug,
												);
											}}
										/>
									);
								})}
							</div>
						</div>

						<div className="form-row form-row--submit">
							<div className="form-column">
								<Button
									variant="contained"
									color="primary"
									className="form-submit"
									type="submit"
									disabled={!isValid || props.isSubmitting}
								>
									Edit Staff Member
								</Button>
							</div>
						</div>
					</form>
				);
			}}
		/>
	);
};

export { EditStaffFormik };
