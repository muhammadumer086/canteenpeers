<?php

namespace App\Http\Controllers\API;

use App\AgeRange;
use App\Announcement;
use App\AnnouncementImage;
use App\Helpers\UserActivityHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAnnouncementRequest;
use App\Http\Resources\Announcement as AnnouncementResource;
use App\Http\Resources\AnnouncementCollection;
use App\Situation;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Validator;
use UserPermissionHelper;

class AnnouncementController extends Controller {
	const PAGINATION_SIZE = 10;

	/**
	 * Get a list of announcements.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["alert_management_read","alert_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}
		$announcements = Announcement::latest()->paginate(
			self::PAGINATION_SIZE
		);

		return new AnnouncementCollection($announcements);
	}

	/**
	 * Get the most recent announcement.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function recent(Request $request) {
		$announcement = auth()
			->user()
			->announcements()
			->latest()
			->firstOrFail();

		return new AnnouncementResource($announcement);
	}

	/**
	 * Create an annoucement for all users.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreAnnouncementRequest $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["alert_management_add","alert_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}
		$validated = $request->validated();

		$usersQuery = User::whereNotNull('id');

		$data = array_except($validated, ['url']);

		$announcement = new Announcement();

		$announcement->fill($data);

		$announcement->url_href = array_get($validated, 'url.href');
		$announcement->url_href_as = array_get($validated, 'url.hrefAs');
		$announcement->url_type = array_get($validated, 'url.type');

		$announcement->user()->associate(auth()->user()->id);

		$announcement->save();

		if (!$request->input('public')) {
			if (
				$request->filled('situations') &&
				!empty($validated['situations'])
			) {
				$situationIds = Situation::whereIn(
					'slug',
					$validated['situations']
				)->pluck('id');
				$userSituationIds = Situation::whereIn('id', $situationIds)
					->orWhereIn('parent_id', $situationIds)
					->pluck('id');

				$announcement->situations()->sync($situationIds);

				$usersQuery->whereIn(
					'id',
					DB::table('situation_user')
						->whereIn('situation_id', $userSituationIds)
						->pluck('user_id')
				);
			}

			if ($request->filled('states') && !empty($validated['states'])) {
				$announcement->states()->sync(
					State::all()
						->whereIn('slug', $validated['states'])
						->pluck('id')
				);

				$statesInput = $validated['states'];
				$statesInput = array_map('strtoupper', $statesInput);
				if (in_array('NZ', $statesInput)) {
					// Split NZ from the rest of the input
					$key = array_search('NZ', $statesInput);
					unset($statesInput[$key]);
					$usersQuery->where(function ($query) use ($statesInput) {
						if (!empty($statesInput)) {
							$query
								->where('country_slug', 'NZ')
								->orWhereIn('state', $statesInput);
						} else {
							$query->where('country_slug', 'NZ');
						}
					});
				} else {
					// Process the states normally
					$usersQuery->whereIn('state', $statesInput);
				}
			}

			if (
				$request->filled('age_ranges') &&
				!empty($validated['age_ranges'])
			) {
				$announcement->ageRanges()->sync(
					AgeRange::all()
						->whereIn('slug', $validated['age_ranges'])
						->pluck('id')
				);
				$usersQuery->whereNotNull('dob');
				// Generate the age array
				$ages = [];
				foreach ($request->input('age_ranges') as $age) {
					if ($age === 'under12') {
						for ($i = 0; $i < 12; $i++) {
							$ages[] = $i;
						}
					} else {
						$range = explode('-', $age);
						for ($i = $range[0]; $i <= $range[1]; $i++) {
							$ages[] = $i;
						}
					}
				}

				$usersQuery->whereIn(
					DB::raw('YEAR(CURDATE()) - YEAR(dob)'),
					$ages
				);
			}

			if ($request->filled('image_id') && $validated['image_id']) {
				if ($announcement->image) {
					$oldImage = $announcement->image;
					$oldImage->announcement()->dissociate();
					$oldImage->save();
				}
				$announcementImage = AnnouncementImage::where(
					'id',
					$validated['image_id']
				)->firstOrFail();
				$announcementImage
					->announcement()
					->associate($announcement->id);
				$announcementImage->save();

				$announcement->image_id = $announcementImage->id;
				$announcement->save();
			}

			// Match all the users for notification
			// $usersQuery->chunk(50, function ($users) use ($announcement) {
			// 	foreach ($users as $user) {
			// 		$user->sendAnnouncementNotification($announcement);
			// 	}
			// });
		}

		$announcement->touch();

		return new AnnouncementResource($announcement);
	}

	/**
	 * Update an announcement.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(
		StoreAnnouncementRequest $request,
		Announcement $announcement
	) {

		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["alert_management_edit","alert_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$validated = $request->validated();

		$data = array_except($validated, ['url']);

		$announcement->fill($validated);

		$announcement->url_href = array_get($validated, 'url.href');
		$announcement->url_href_as = array_get($validated, 'url.hrefAs');
		$announcement->url_type = array_get($validated, 'url.type');

		if (!$request->input('public')) {
			if ($request->filled('situations')) {
				$announcement->situations()->sync(
					Situation::all()
						->whereIn('slug', $validated['situations'])
						->pluck('id')
				);
			} else {
				$announcement->situations()->detach();
			}

			if ($request->filled('states')) {
				$announcement->states()->sync(
					State::all()
						->whereIn('slug', $validated['states'])
						->pluck('id')
				);
			} else {
				$announcement->states()->detach();
			}

			if ($request->filled('age_ranges')) {
				$announcement->ageRanges()->sync(
					AgeRange::all()
						->whereIn('slug', $validated['age_ranges'])
						->pluck('id')
				);
			} else {
				$announcement->ageRanges()->detach();
			}

			if ($request->filled('image_id')) {
				if ($announcement->image) {
					$oldImage = $announcement->image;
					$oldImage->announcement()->dissociate();
					$oldImage->save();
				}
				$announcementImage = AnnouncementImage::where(
					'id',
					$validated['image_id']
				)->firstOrFail();
				$announcementImage
					->announcement()
					->associate($announcement->id);
				$announcementImage->save();
			}
		}

		$announcement->save();

		return new AnnouncementResource($announcement);
	}

	/**
	 * Delete an announcement.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, Announcement $announcement) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["alert_management_delete","alert_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}
		$announcement->delete();
	}

	/**
	 * Close an announcement for the authenticated user.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function closeAnnouncement(
		Request $request,
		Announcement $announcement
	) {
		$announcement->users()->attach(auth()->user()->id);
		$announcement->save();

		return response()->json([
			'success' => true,
		]);
	}
}
