import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import moment from 'moment';
import Button from '@material-ui/core/Button';

import { ISelectOption } from '../../interfaces';

import {
AutocompleteSelectFormik,
InlineDatePickerFormik,
SelectFieldFormik,
} from '../inputs';
import AdminFilterUsersPartial from './AdminFilterUsersPartial';

interface IProps {
initialValues: any;
handleFilter(filterValues: any): void;
handleClear?(): void;
showUserFilter?: boolean;
}

const anyActivityType: Map<string, string[]> = new Map<string, string[]>([
[
'Any engagement Activity',
[
 'Blog Created',
 'Blog Read',
 'Blog Updated',
// 'Blog Deleted',
 'Shared Blog',
'Discussion Created',
'Discussion Reply',
'Discussion Read',
'Discussion I Get It',
'Discussion Hugged',
'Resource Read',
'Shared Resource',
// 'User Login',
'User Event Registration',
],
],
[
'Any blog activity',
[
'Blog Created',
'Blog Read',
'Shared Blog',
'Blog Updated',
'Updated Blog',
'Blog Deleted',
'Deleted Blog',
'Blog Restored',
],
],
[
'Any discussion activity',
[
'Discussion Created',
'Discussion Reply',
'Discussion Read',
'Discussion I Get It',
'Discussion Removed I Get It',
'Discussion Hugged',
'Discussion Removed Hug',
'Discussion Follow',
'Discussion Unfollow',
'Discussion Deleted',
'Deleted Discussion',
'Discussion Reported',
],
],
['Any resource activity', ['Resource Read', 'Shared Resource']],
[
'Any user account activity',
[
'User Created',
'User Verified',
'User Verified With Password Reset',
'User Password Forgot',
'User Password Reset',
'User Login',
'User Email Updated',
'User DoB Updated',
'User Name Updated',
'User Password Updated',
'User Username Updated',
'User Cancer Experience(s) Updated',
'User Banned',
'User Ban Removed',
'User Deleted',
'User Restored',
'User Event Registration',
],
],
]);

const activityTypes: string[] = [
'Blog Created',
'Blog Read',
'Shared Blog',
'Blog Updated',
'Updated Blog',
'Blog Deleted',
'Deleted Blog',
'Blog Restored',
'Discussion Created',
'Discussion Reply',
'Discussion Read',
'Discussion I Get It',
'Discussion Removed I Get It',
'Discussion Hugged',
'Discussion Removed Hug',
'Discussion Follow',
'Discussion Unfollow',
'Discussion Deleted',
'Deleted Discussion',
'Discussion Reported',
'Migrated User Activation',
'Reported Discussion',
'Resource Read',
'Shared Resource',
'User Created',
'User Verified',
'User Verified With Password Reset',
'User Password Forgot',
'User Password Reset',
'User Login',
'User Email Updated',
'User DoB Updated',
'User Name Updated',
'User Password Updated',
'User Username Updated',
'User Cancer Experience(s) Updated',
'User Banned',
'User Ban Removed',
'User Deleted',
'User Restored',
'User Event Registration',
];

const anyActivityKeys = Array.from(anyActivityType.keys());

const anyActivityOptions: ISelectOption[] = anyActivityKeys.map(activity => {
return {
label: activity,
value: activity,
};
});

const activityOptions: ISelectOption[] = anyActivityOptions.concat(
activityTypes.map(activity => {
return {
label: activity,
value: activity,
};
}),
);

const platformOptions: ISelectOption[] = [
{
label: 'Web',
value: 'web',
},
{
label: 'App',
value: 'app',
},
];

const AdminFilterActivities: React.SFC<IProps> = (props: IProps) => {
const validationFormat = Yup.object().shape({});

const initialValues = JSON.parse(JSON.stringify(props.initialValues));

return (
<Formik
initialValues={initialValues}
validationSchema={validationFormat}
onSubmit={values => {
const valuesModified = JSON.parse(JSON.stringify(values));

if ('from' in valuesModified && valuesModified.from) {
valuesModified.from = moment(valuesModified.from).format(
'YYYY-MM-DD',
);
} else {
valuesModified.from = undefined;
}

if ('to' in valuesModified && valuesModified.to) {
valuesModified.to = moment(valuesModified.to).format(
'YYYY-MM-DD',
);
} else {
valuesModified.to = undefined;
}

props.handleFilter(valuesModified);
}}
render={({
values,
touched,
errors,
handleChange,
handleBlur,
handleSubmit,
setFieldValue,
setFieldTouched,
isValid,
}) => {
const inputProps = {
values,
touched,
errors,
handleChange,
handleBlur,
setFieldValue,
setFieldTouched,
};

return (
<form className="form" onSubmit={handleSubmit}>
<div className="form-row">
<div className="form-column">
<AutocompleteSelectFormik
label="Activity type"
name="type"
options={activityOptions}
values={props.initialValues.sort_by}
emptyLabel="Please select"
multi={true}
{...inputProps}
customHandleChange={customHandleChange}
/>
</div>
</div>
<div className="form-row">
<div className="form-column">
<InlineDatePickerFormik
label="Activities from"
name="from"
format="DD/MM/YYYY"
{...inputProps}
/>
</div>
</div>
<div className="form-row">
<div className="form-column">
<InlineDatePickerFormik
label="Activities to"
name="to"
format="DD/MM/YYYY"
{...inputProps}
/>
</div>
</div>
<div className="form-row">
<div className="form-column">
<SelectFieldFormik
label="Platform"
name="platformFilter"
options={platformOptions}
values={props.initialValues.platformFilter}
emptyLabel="All"
{...inputProps}
/>
</div>
</div>
{props.showUserFilter && (
<AdminFilterUsersPartial
initialValues={props.initialValues}
inputProps={inputProps}
/>
)}
<div className="form-row form-row--submit_left">
<div className="form-column">
<Button
variant="contained"
color="primary"
className="form-submit"
type="submit"
disabled={!isValid || !touched}
>
Filter
</Button>
</div>
</div>
<div className="form-row form-row--submit_left">
<div className="form-column">
<Button
variant="contained"
color="secondary"
className="form-clear"
type="button"
onClick={props.handleClear}
>
Clear
</Button>
</div>
</div>
</form>
);
}}
/>
);

function customHandleChange(
values: ISelectOption[],
setFieldValue: (value: any) => void,
): void {
if (values) {
// re-map the values to an array of string
const stringValues = values.map(value => value.value);
let filteredValues = stringValues.filter(
value => anyActivityKeys.indexOf(value as string) < 0,
);
// Get all the "any" cases in the current values
const anyCases = stringValues.filter(
value => anyActivityKeys.indexOf(value as string) >= 0,
);

if (anyCases.length) {
anyCases.forEach(value => {
filteredValues = filteredValues.concat(
anyActivityType.get(value as string),
);
});
}

// Remove duplicate case
filteredValues = [...new Set(filteredValues)];

setFieldValue(filteredValues);
} else {
setFieldValue([]);
}
}
};

export default AdminFilterActivities;