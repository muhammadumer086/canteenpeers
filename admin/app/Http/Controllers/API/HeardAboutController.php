<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\HeardAbout;
use App\Http\Resources\HeardAbout as HeardAboutResource;

class HeardAboutController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$countries = HeardAbout::orderBy('slug', 'ASC')->get();

		return response()->json([
			'heard-abouts' => HeardAboutResource::collection($countries),
		]);
	}
}
