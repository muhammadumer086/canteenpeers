import React from 'react';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

interface IProps {
	tabData: ITabData[];
	activeTab: string;
	handleChange(_event, value: string): void;
}

export interface ITabData {
	label: string;
	value: string;
}

const FixedNavgationBarTabs: React.StatelessComponent<IProps> = props => {
	return (
		<div className="fixed_bar fixed_bar--with_tabs theme--accent">
			<Tabs
				className="fixed_bar-tabs"
				value={props.activeTab}
				onChange={props.handleChange}
				indicatorColor="secondary"
				textColor="inherit"
				variant="scrollable"
			>
				{props.tabData.map((single, index) => {
					return (
						<Tab
							key={index}
							label={single.label}
							value={single.value}
						/>
					);
				})}
			</Tabs>
		</div>
	);
};

export default FixedNavgationBarTabs;
