import React from 'react';
import { connect } from 'react-redux';

import { IStoreState, apiError } from '../../redux/store';

import { ApiService } from '../../services';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';

import Pill from '../general/Pill';

interface IProps {
	type: 'iGetIt' | 'hug';
	count: number;
	discussionId: number;
	discussionTitle: string;
	liked?: boolean;
	disabled?: boolean;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	count: number;
	liked: boolean;
}

class DiscussionGetIt extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			count: props.count,
			liked: !!props.liked,
		};

		this.handleClick = this.handleClick.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.liked !== prevProps.liked) {
			this.setState({
				liked: this.props.liked,
			});
		}
	}

	public render() {
		return (
			<div className="discussion_tile-content-get_it">
				<Pill
					label={this.props.type === 'iGetIt' ? 'I Get It' : 'Hug'}
					active={this.state.liked}
					disabled={false}
					clickable={!this.props.disabled}
					onClick={this.handleClick}
					statusCount={this.state.count}
					isPost={true}
					isReply={false}
					showIfZero={true}
				/>
			</div>
		);
	}

	protected handleClick() {
		const originalCount = this.state.count;
		let newCount = originalCount + 1;
		if (this.state.liked) {
			newCount = originalCount - 1;
		}
		newCount = Math.max(0, newCount);
		this.setState(
			{
				count: newCount,
				liked: !this.state.liked,
			},
			() => {
				let url = `/api/discussions/${this.props.discussionId}/`;
				if (this.props.type === 'iGetIt') {
					url += 'toggle-like';
				} else {
					url += 'toggle-hug';
				}

				this.props.apiService
					.queryPOST(url, {})
					.then(data => {
						if (data.liked) {
							this.GTM.pushEventToDataLayer({
								event: 'iGetThisDiscussion',
								title: this.props.discussionTitle,
							});
						}
						if (data.hugged) {
							this.GTM.pushEventToDataLayer({
								event: 'huggedThisDiscussion',
								title: this.props.discussionTitle,
							});
						}
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							count: originalCount,
							liked: !this.state.liked,
						});
					});
			},
		);
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(DiscussionGetIt);
