<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Discussion;

class AssignUpdatedAtInDiscussion extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:assignUpdatedAtInDiscussion';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command to modify the update_at in discussions';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		\App\Discussion::disableSearchSyncing();

		$this->line('');
		$this->info('Update the updated_at time for the discussions');

		// Update the updated_at time for the main discussions
		$mainDiscussionsQuery = Discussion::whereNull('main_discussion_id');
		$bar = $this->output->createProgressBar($mainDiscussionsQuery->count());
		$bar->start();
		$mainDiscussionsQuery->chunk(10, function ($discussions) use ($bar) {
			foreach ($discussions as $singleMainDiscussion) {
				// Get the most recent comment in this discussion
				$mostRecentResponse = Discussion::where(
					'main_discussion_id',
					$singleMainDiscussion->id
				)
					->orderBy('created_at', 'DESC')
					->first();
				if ($mostRecentResponse) {
					$singleMainDiscussion->updated_at =
						$mostRecentResponse->created_at;
				} else {
					$singleMainDiscussion->updated_at =
						$singleMainDiscussion->created_at;
				}
				$singleMainDiscussion->save(['timestamps' => false]);
			}
		});
		$bar->finish();

		$this->info("\r\nFinished modifying the updated_at in discussions");
		$this->line('');
	}
}
