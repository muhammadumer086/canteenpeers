<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

use App\Traits\AlgoliaIndex;
use App\Traits\HasSeoAttribute;

class Resource extends Model {
	use Searchable;
	use SoftDeletes;
	use AlgoliaIndex;
	use HasSeoAttribute;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'prismic_id',
		'title',
		'slug',
		'content',
		'is_ycs',
		'feature_image',
		'featured',
		'first_publication_date',
		'last_publication_date',
		'topic_id',
		'seo_title',
		'seo_description',
		'seo_image',
		'deleted_at',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'feature_image' => 'array',
		'seo_image' => 'array',
		'is_ycs' => 'boolean',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['saved', 'seo'];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
		'first_publication_date',
		'last_publication_date',
	];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'resources';

	/**
	 * Get the index name for the model.
	 *
	 * @return string
	 */
	public function searchableAs() {
		return $this->algoliaIndex();
	}

	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName() {
		return 'slug';
	}

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		$id = $this->slug;
		$image = null;
		$title = $this->title;
		$excerpt = str_limit(strip_tags($this->content), 250);
		$data = str_limit(strip_tags($this->content), 5000);

		if (
			$this->feature_image &&
			is_array($this->feature_image) &&
			isset($this->feature_image['url'])
		) {
			$image = $this->feature_image['url'];
		}

		// Result structure
		$result = [
			'type' => $type,
			'id' => $id,
			'image' => $image,
			'title' => $title,
			'excerpt' => $excerpt,
			'data' => $data,
		];

		return $result;
	}

	/**
	 * Get the value used to index the model.
	 *
	 * @return mixed
	 */
	public function getScoutKey() {
		return $this->algolia_id;
	}

	/**
	 * Topic relating to this resource
	 */
	public function topic() {
		return $this->belongsTo('App\Topic');
	}

	/**
	 * The Users that have the resource saved
	 */
	public function users() {
		return $this->belongsToMany('App\User', 'saved_resources');
	}

	public function userActivities() {
		return $this->morphMany('App\UserActivity', 'activity');
	}

	/**
	 * Topic relating to this resource
	 */
	public function situations() {
		return $this->belongsToMany('App\Situation');
	}

	public function getSavedAttribute() {
		$request = request();
		if ($request->headers->has('Authorization')) {
			$authUser = Auth::guard('api')->authenticate($request);
			if ($authUser) {
				return $authUser
					->savedResources()
					->where('saved_resources.resource_id', $this->id)
					->exists();
			}
		}

		return false;
	}

	public function getAlgoliaIdAttribute() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		return "$type/{$this->slug}";
	}

	/**
	 * Get the number of views
	 *
	 * @return bool
	 */
	public function getViewsCountAttribute() {
		return $this->userActivities()
			->where('type', 'Resource Read')
			->count();
	}
}
