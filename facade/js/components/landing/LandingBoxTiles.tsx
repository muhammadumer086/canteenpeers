import React from 'react';

interface IProps {

}

const LandingBoxTile: React.FC<IProps> = (props) => {
	return (
		<div className="landing_box_tile">
			{props.children}
		</div>
	)
}

export default LandingBoxTile
