import React from 'react';
import { connect } from 'react-redux';

import { IStoreState } from '../../redux/store';
import { IApiUser } from 'js/interfaces';
import EditGroupMessageUser from './EditGroupMessageUser';

interface IProps {
	userData: IApiUser | null;
	name: string;
	value: IApiUser[];
	touched: any;
	errors: any;
	disabled?: boolean;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
}

const EditGroupUserList: React.StatelessComponent<IProps> = props => {
	return (
		<React.Fragment>
			{props.value.map((user, index) => {
				if (props.userData && user.id === props.userData.id) {
					return null;
				}
				return (
					<EditGroupMessageUser
						key={index}
						user={user}
						disabled={props.disabled}
						removeUser={removeUser}
					/>
				);
			})}
		</React.Fragment>
	);

	function removeUser(removedUser: IApiUser) {
		// Create a copy of the props users
		let users: IApiUser[] = JSON.parse(JSON.stringify(props.value));
		users = users.filter(user => {
			if (user.id !== removedUser.id) {
				return user;
			} else {
				return null;
			}
		});

		props.setFieldTouched(props.name, true, false);
		props.setFieldValue(props.name, users);
	}
};

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;

	return {
		userData,
	};
}

export default connect(mapStateToProps)(EditGroupUserList);
