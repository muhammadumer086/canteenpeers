<?php

namespace App\Notifications;

use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;


use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;


use App\User;

class MessagesNotification extends Notification {
	use Queueable;

	protected $user;
	protected $count;
	protected $usernames;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($user) {
		$this->user = $user;

		$queryNotifications = $user
			->notifications()
			->whereNull('read_at')
			->where('type', 'App\\Notifications\\MessageReceivedNotification');

		$this->usernames = $this->generateUserNames(
			$queryNotifications->limit(10)
		);

		$this->count = $queryNotifications->count();
	}

	protected function generateUserNames($query) {
		$usernames = [];
		// Extract the usernames from the notifications
		$notifications = $query->get();
		foreach ($notifications as $singleNotification) {
			if (
				isset(
					$singleNotification->data,
					$singleNotification->data['user'],
					$singleNotification->data['user']['username']
				)
			) {
				$usernames[] = $singleNotification->data['user']['username'];
			}
		}

		$usernames = array_unique($usernames);

		if (sizeof($usernames) === 0) {
			return null;
		} elseif (sizeof($usernames) === 1) {
			return $usernames[0];
		} elseif (sizeof($usernames) >= 3) {
			$usernames = array_slice($usernames, 0, 3);
			$usernames[] = 'others';
		}

		$lastUsername = array_pop($usernames);
		return implode(', ', $usernames) . " and {$lastUsername}";
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		$result = [];
		if ($this->user->is_deleted) {
			return $result;
		}
		if ($this->count && $this->usernames) {
			if ($this->user->allowEmailMessages()) {
				$result[] = 'mail';
			}
			/*if ($this->user->allowPushNotificationMessages()) {
				$result[] = FcmChannel::class;
			}*/
		}
		return $result;
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title =
			'You have ' .
			$this->count .
			' ' .
			Str::plural('message', $this->count) .
			' waiting for you';

		return (new MailMessage())
			->subject($title)
			->view('emails.user-message', [
				'user' => $this->user,
				'usernames' => $this->usernames,
				'count' => $this->count,
				'messages' => Str::plural('message', $this->count),
			]);
	}

	public function toFcm($notifiable) {
		/*
		$title = Str::plural('Message', $this->count) . ' received';
		$content =
			'You have ' .
			$this->count .
			' ' .
			Str::plural('message', $this->count) .
			' waiting for you';
		// The FcmNotification holds the notification parameters
		$fcmNotification = FcmNotification::create()
			->setTitle($title)
			->setBody($content)
			->setTag('MessagesNotification');


		// The FcmMessage contains other options for the notification
		return FcmMessage::create()
			->setPriority(FcmMessage::PRIORITY_HIGH)
			->setTimeToLive(86400)
			->setNotification($fcmNotification);
		*/
	}
}
