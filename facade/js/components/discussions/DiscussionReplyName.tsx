import React           from 'react';
import ReplyIcon       from '@material-ui/icons/Reply';

import Pill from '../general/Pill';

interface IProps {
	deletable?: boolean;
	disabled?: boolean;
	onClick?(): void;
}

const DiscussionReplyName: React.FC<IProps> = (props) => {
	if (
		props.disabled === undefined ||
		props.disabled
	) {
		return (
			<span
				className="pill pill--inactive pill--withIcon pill--greyedOut"
			>
				<ReplyIcon className="pill-icon" />
				{ props.children }
			</span>
		);
	}
	return (
		<Pill
			label={props.children.toString()}
			active={false}
			disabled={false}
			clickable={props.deletable && !props.disabled}
			icon="reply"
			isGreyedOut={true}
			deletable={props.deletable}
			onClick={props.onClick}
		>
			<ReplyIcon className="pill-icon" />
		</Pill>
	)
};

export default DiscussionReplyName;
