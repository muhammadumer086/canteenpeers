<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Situation as SituationResource;

class LeavingReason extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$params = [
			'id',
			'leaving_reasons_data',
			'created_at',
			'updated_at',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);


		return $result;
	}
}
