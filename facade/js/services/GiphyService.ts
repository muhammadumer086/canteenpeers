import fetch from 'isomorphic-unfetch';
import { apiError, apiErrorDismiss } from '../redux/store';

export class GiphyService {
	protected dispatch: (value: any) => void | null = null;

	public constructor(dispatch: (value: any) => void) {
		this.dispatch = dispatch;
	}

	/**
	 * GET query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryGET(endpoint: string, returnProperty?: string): Promise<any> {
		return this.handleApiResponses(
			fetch(endpoint, {
				method: 'GET',
			}),
			returnProperty,
		);
	}

	protected handleApiResponses(fetchQuery: any, returnProperty?: string) {
		// Dismiss the API error
		if (this.dispatch) {
			this.dispatch(apiErrorDismiss());
		}

		return fetchQuery
			.then(response => {
				if (!response.ok && response.status !== 400) {
					if (typeof window !== 'undefined') {
						console.warn('Error response:', response);
					}
				} else {
					return response.json();
				}
			})
			.then(data => {
				if (!!data && data.hasOwnProperty('error')) {
					if (
						data.hasOwnProperty('message') &&
						typeof data.message === 'string'
					) {
						throw new Error(data.message);
					}

					if (typeof data.error === 'string') {
						throw new Error(data.error);
					}
					console.warn(`Error data:`, data);
					throw new Error('Invalid format when receiving API error.');
				}

				if (returnProperty) {
					if (data.hasOwnProperty(returnProperty)) {
						return data[returnProperty];
					} else {
						throw new Error(
							`Couldn't find property ${returnProperty} in API response`,
						);
					}
				}

				return data;
			})
			.catch(error => {
				if (this.dispatch) {
					this.dispatch(apiError(error));
				} else {
					console.warn(
						'error in GiphyService.handleApiResponses',
						error,
					);
				}
			});
	}

	public getGiphyApiUrl(endpoint: string): string {
		let url = endpoint;
		// Add the first slash in the endpoint just in case
		if (endpoint.length) {
			if (
				!endpoint.match(/^https?\:\/\//) &&
				endpoint.charAt(0) !== '/'
			) {
				url = `/${endpoint}`;
			}
		}
		// return url;
		return `${process.env.GIPHY_URL}${url}`;
	}
}
