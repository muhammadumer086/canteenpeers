import React from 'react';
import { ButtonBase } from '@material-ui/core';
import { IApiGIFObject } from '../../../../js/interfaces';

interface IProps {
	data: IApiGIFObject;
	onGifSelection(gifData: IApiGIFObject): void; 
}

const GiphySelectionResultListItem: React.StatelessComponent<
	IProps
> = props => {
	return (
		<div className="giphy_select_result_item">
			<ButtonBase
				className="giphy_select_result_item-button"
				onClick={() => {
					props.onGifSelection(props.data);
				}}
			/>

			<img
				className="giphy_select_result_item-image"
				src={props.data.images.fixed_height_small.url}
				alt={props.data.slug}
			/>
		</div>
	);
};

export default GiphySelectionResultListItem;
