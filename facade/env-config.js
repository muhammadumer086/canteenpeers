const prod = process.env.NODE_ENV === 'production';
const config = require('dotenv').config();

const exportData = {};

if (
	config &&
	config.hasOwnProperty('parsed') &&
	config.parsed
) {
	for (const key in config.parsed) {
		if (config.parsed.hasOwnProperty(key)) {
			exportData[`process.env.${key}`] = config.parsed[key];
		}
	}
} else {
	throw new Error(`Can't find "parsed" in config`);
}

module.exports = exportData;
