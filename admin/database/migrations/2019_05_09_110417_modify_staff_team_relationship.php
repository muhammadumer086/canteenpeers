<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyStaffTeamRelationship extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('staff', function (Blueprint $table) {
			$table->unsignedInteger('team_id')->nullable();

			$table
				->foreign('team_id')
				->references('id')
				->on('teams')
				->onDelete('cascade');
		});

		Schema::dropIfExists('staff_team');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('staff', function (Blueprint $table) {
			$table->dropForeign('staff_team_id_foreign');
			$table->dropColumn('team_id');
		});

		Schema::create('staff_team', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('staff_id');
			$table->unsignedInteger('team_id');
			$table->timestamps();

			$table
				->foreign('staff_id')
				->references('id')
				->on('staff')
				->onDelete('cascade');
			$table
				->foreign('team_id')
				->references('id')
				->on('teams')
				->onDelete('cascade');
		});
	}
}
