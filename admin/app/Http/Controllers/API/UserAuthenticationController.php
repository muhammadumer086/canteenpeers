<?php

namespace App\Http\Controllers\API;
use App\Http\Resources\UserCollection;
use App\Helpers\UserPermissionHelper;
use App\Http\Resources\User as UserResource;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Log;
use Mail;
use Password;
use phpDocumentor\Reflection\Types\Null_;
use UserVerification;
use Validator;
use UserActivityHelper;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;


use App\Rules\ContainsNumber;
use App\Rules\SpecialChars;

use App\User;
use App\Helpers\CampaignMonitorSubscription;

class UserAuthenticationController extends Controller
{
	/**
	 * Send the verification email again to the user.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function resendVerification(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required|email',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Try to get the user based on the email address
		if (User::where('email', '=', $request->input('email'))->exists()) {
			$user = User::where(
				'email',
				'=',
				$request->input('email')
			)->first();
			UserVerification::generate($user);
			UserVerification::send($user);
		}

		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Verify the user.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function verify(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required|email',
			'password' => [
				'bail',
				'required',
				'string',
				'min:8',
				new ContainsNumber(),
				new SpecialChars(),
			],
			'token' => 'bail|required',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		if (
		!User::where('email', $request->input('email'))
			->where('verification_token', $request->input('token'))
			->exists()
		) {
			return response()->json(
				[
					'error' => 'Invalid credentials or token',
				],
				400
			);
		}

		// Try to get the user based on the email address and password
		$authAttemptParams = [
			'email' => $request->input('email'),
			'password' => $request->input('password'),
		];

		if (Auth::attempt($authAttemptParams)) {
			try {
				UserVerification::process(
					$request->input('email'),
					$request->input('token'),
					'users'
				);

				$request->request->add([
					'grant_type' => 'password',
					'client_id' => config('canteen.auth_client_id'),
					'client_secret' => config('canteen.auth_client_secret'),
					'scope' => '*',
					'username' => $request->input('email'),
				]);

				$user = User::where(
					'email',
					$request->input('email')
				)->firstOrFail();
				$user->email_verified_at = Carbon::parse(date("Y-m-d H:i:s"));
				$user->verified = true;
				$user->save();

				//Add data to CM and Qualtrics
				$this->addCMQData($user, $request->ip(), true, true);

				// forward the request to the oauth token request endpoint
				$tokenRequest = Request::create('/oauth/token', 'post');
				return Route::dispatch($tokenRequest);
			} catch (\Exception $e) {
				return response()->json(
					[
						'error' => $e->getMessage(),
					],
					400
				);
			}
		} else {
			return response()->json(
				[
					'error' => 'Email or password invalid',
				],
				400
			);
		}

		return response()->json(
			[
				'error' => 'Unknown error',
			],
			400
		);
	}

	private function addCMQData(User $user, $ip, $resetPasswordVerification=true, $markVerified=false)
	{
		$user->sendWelcomeNotification();

		$subscription = new CampaignMonitorSubscription();
		if ($user->allowNews())
		{
			$customFields = [
				'Key' => 'verificationdate',
				'Value' => Carbon::now()->format('Y-m-d'),
			];

			$subscription->subscribeUserToCampaignMonitor(
				$user->email,
				$user->first_name,
				$user->last_name,
				$user->location_data,
				$user->dob,
				$user->situations,
				true,
				true,
				$customFields
			);
		} else {
			$subscription->unsubscribeUserFromCampaignMonitor(
				$user->email
			);
		}

		// Add contact in QUALTRICS
		$phone = isset($user->phone)?$user->phone:" ";
		$indigenous = isset($user->indigenous_australian)?$user->indigenous_australian:" ";

		$situations = '';
		foreach ($user->situations as $situation)
		{
			$situations .= $situation->name.',';
		}

		$data = [];

		if(isset($user->location_data) && isset($user->location_data ["address_components"]))
		{
			if(isset($user->location_data ["address_components"][0]))
			{
				$data['postcode'] = $user->location_data ["address_components"][0]["long_name"];
			}
			if(isset($user->location_data ["address_components"][1]))
			{
				$data['suburb'] = $user->location_data ["address_components"][1]["long_name"];
			}

			if(isset($user->location_data ["address_components"][2]))
			{
				$data['state'] = $user->location_data ["address_components"][2]["long_name"];
			}

			if(isset($user->location_data ["address_components"][3]))
			{
				$data['country'] = $user->location_data ["address_components"][3]["long_name"];
			}

		}

		$data = [
			'email'=>$user->email,
			'firstName'=>$user->first_name,
			'lastName'=>$user->last_name,
			'phone'=>$phone,
			"embeddedData"=>[
				"Source"=> "CanteenConnectYP",
				'username'=>$user->username,
				'ccuid'=>$user->id,
				'situations'=>substr($situations, 0, strlen($situations)-1),
				'dob'=>$user->dob->toDateString(),
				'gender'=>$user->gender,
				'address'=>$user->location,
				'indigenous_australian'=>$indigenous,
				//'postcode'=>isset($user->location_data)?$user->location_data ["address_components"][0]["long_name"]:null,
				//'suburb'=>isset($user->location_data)?$user->location_data ["address_components"][1]["long_name"]:null,
				//'state'=>isset($user->location_data)?$user->location_data ["address_components"][2]["long_name"]:null,
				//'country'=>isset($user->location_data)?$user->location_data ["address_components"][3]["long_name"]:null,
				'Allow Contact Research Opportunities'=> "yes",
				'self Identify'=> $user->self_identify
			]
		];
		$this->addQualtricsContact($data);

		if($markVerified == true)
		{
			$user2 = User::where(
				'email',
				$user->email,
			)->firstOrFail();
			$user2->verified = true;
			$user2->save();
		}

		if($resetPasswordVerification) {
			UserActivityHelper::create($user, $user, 'User Verified', [
				'ip' => $ip,
			]);
		}

	}

	private function addQualtricsContact($data)
	{
		# Setting user Parameters
		$apiToken = config('canteen.qualtrics_token');
		$dataCenter =  config('canteen.data_center_id');
		$directoryId = config('canteen.qualtrics_dic');
		$mailingListId = config('canteen.mailing_list');

		$baseUrl = "https://$dataCenter.qualtrics.com/API/v3/directories/$directoryId/mailinglists/$mailingListId/contacts";
		$headers = [
			"x-api-token" => $apiToken,
			"Content-Type" => "application/json"
		];

		$client = new Client([
			'base_uri' => $baseUrl,
		]);
		try {
			  $response = $client->request('POST', $baseUrl, [
				'headers' => $headers,
				'json' => $data,
			]);

		} catch (\Exception $e) {
			Log::error($e);
		}
	}

	/**
	 * Send the verification email again to the user.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function verifyMobileCode(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required|email',
			'mobileCode' => 'required'
		]);

		if($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Try to get the user based on the email address
		$user = User::where('email', $request->input('email'))->first();
		if (!$user) {
			return response()->json(
				[
					'error' => 'No user found with this email',
				],
				400
			);
		}

		// check if code is verified within 300 sec
		$startTime = Carbon::parse($user->updated_at);
		$finishTime = Carbon::parse(date("Y-m-d H:i:s"));

		$totalDuration = $finishTime->diffInSeconds($startTime);
		if($totalDuration > 302)
		{
			return response()->json(
				[
					'error' => 'Code expired',
				],
				400
			);
		}

		if((int)$user->mobile_verification_code !== (int)$request->input('mobileCode'))
		{
			return response()->json(
				[
					'error' => 'Invalid Code',
				],
				400
			);
		}

		$user->is_mobile_verified = true;
		$user->mobile_verified_at = Carbon::parse(date("Y-m-d H:i:s"));
		$user->save();

		// if not added already on CM and Qualtrics
		// if($user->verified != true && $user->is_mobile_verified !=true)
		{
			$this->addCMQData($user, $request->ip());
		}

		return response()->json(
			[
				'success' => 'User Verified'
			],
			200
		);

	}


	/**
	 * Send the verification email again to the user.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function getSMSCode(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required|email',
			//'countryCode' => 'bail|required',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}
		// Try to get the user based on the email address
		$user = User::where('email', $request->input('email'))->first();


		if ($user) {

			$randomSixDigitInt = random_int(100000, 999999);
			if($request->input('medium') && $request->input('medium') == 'android'){
				$message = "<#> Welcome to ".config('canteen.app_name'). " - Your verification code is $randomSixDigitInt       ".config('canteen.app_sign');
			}
			else
			{
				$message = "Welcome to ".config('canteen.app_name'). " - Your verification code is $randomSixDigitInt";

			}
			// Check if user is already verified
			if($user->verified || $user->is_mobile_verified)
			{
				return response()->json([
					'success' => false,
					'message' => 'User already verified.'
				]);

			}
			// Check if phone number exists and valid
			if(!$user->phone)
			{
				return response()->json([
					'success' => false,
					'message' => 'Phone number not found.'
				]);
			}
			$userPhone = $user->phone;// $request->input('countryCode').substr($user->phone, 1,strlen($user->phone)-1);

			$this->sendMessage($message, $userPhone);
			$user->mobile_verification_code = $randomSixDigitInt;
			$user->save();
		}

		return response()->json([
			'success' => true,
		]);
	}


	/**
	 * Sends sms to user using Twilio's programmable sms client
	 * @param String $message Body of sms
	 * @param Number $recipients Number of recipient
	 */
	private function sendMessage($message, $recipientPhone)
	{
		$account_sid = config('canteen.twillio_id');
		$auth_token = config('canteen.twilio_auth_token');
		$twilio_number = config('canteen.twilio_number');
		$client = new \Twilio\Rest\Client($account_sid, $auth_token);
		$client->messages->create($recipientPhone, ['from' => $twilio_number, 'body' => $message]);
	}

	 /* Mark user as verified
	 * Add data to Qualtircs/CM
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */

	public function markVerified(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all"]);

		if (!$this->hasAdminPermission() && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();
		$user->email_verified_at = Carbon::parse(date("Y-m-d H:i:s"));
		$user->verified = true;
		$user->save();

		//Add data to CM and Qualtrics
		$this->addCMQData($user, $request->ip());

		return response()->json([
			'verified' => true,
		]);
	}



	public function markUnverified(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all"]);

		if (!$this->hasAdminPermission() && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();
		$user->verified = false;
		$user->save();

		return response()->json([
			'verified' => true,
		]);
	}


	protected function getPlatform() {
		$platform = request('platform', 'web');
		if ($platform !== 'web' && $platform !== 'app') {
			$platform = 'web';
		}
		return $platform;
	}

	/**
	 * Send the reset password email
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function passwordForgot(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required|email',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Try to get the user based on the email address
		if (User::where('email', '=', $request->input('email'))->exists()) {
			$user = User::where(
				'email',
				'=',
				$request->input('email')
			)->first();
			// Generate a new token
			$token = Password::getRepository()->create($user);
			// Send the reset email
			$user->sendPasswordResetNotification($token);

			UserActivityHelper::create($user, $user, 'User Password Forgot', [
				'ip' => $request->ip(),
			]);

			return response()->json([
				'success' => true,
			]);

		}
		else
		{
			return response()->json([
				'success' => false,
				'message' => 'This email is not registered with Canteen. Would you like to <a href="/auth/password-forgot">signup</a>?'
			]);
		}
	}

	/**
	 * Activate the migrated user
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function activateMigrated(Request $request)
	{
		return $this->resetUserPassword($request, 'Migrated User Activation');
	}



	/**
	 * Reset the user's password
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function passwordReset(Request $request)
	{
		return $this->resetUserPassword($request, 'User Password Reset');
	}

	protected function resetUserPassword(Request $request, $activityName)
	{
		$validator = Validator::make($request->all(), [
			'token' => 'bail|required',
			'email' => 'bail|required|email',
			'password' => [
				'bail',
				'required',
				'string',
				'min:8',
				new ContainsNumber(),
				new SpecialChars(),
			],
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Try to get the user based on the email address
		if (User::where('email', '=', $request->input('email'))->exists()) {
			$broker = Password::broker();
			// Reset the user's password
			$response = $broker->reset($this->credentials($request), function (
				$user,
				$password
			) use ($request, $activityName) {
				if (
					!$user->verified &&
					$activityName === 'User Password Reset'
				) {

					$user->is_mobile_verified = true;
					$user->mobile_verified_at = Carbon::parse(date("Y-m-d H:i:s"));
					$user->save();
					//Add data to CM and Qualtrics
					$this->addCMQData($user, $request->ip(),false);

					UserActivityHelper::create(
						$user,
						$user,
						'User Verified With Password Reset',
						[
							'ip' => $request->ip(),
						]
					);
				}

				$user
					->forceFill([
						'verified' => true,
						'verification_token' => null,
						'password' => Hash::make($password),
						'remember_token' => Str::random(60),
						'inactive' => false,
					])
					->save();
			});

			if ($response === Password::PASSWORD_RESET) {
				$user = User::where(
					'email',
					'=',
					$request->input('email')
				)->first();
				// Revoke all the user's access tokens
				$userTokens = $user->tokens;
				foreach ($userTokens as $token) {
					$token->revoke();
				}

				UserActivityHelper::create($user, $user, $activityName, [
					'ip' => $request->ip(),
				]);

				return response()->json([
					'success' => true,
				]);
			} else {
				return response()->json(
					[
						'error' => trans($response),
					],
					400
				);
			}
		}

		return response()->json(
			[
				'error' => 'Unknown error',
			],
			400
		);
	}

	/**
	 * Get the password reset credentials from the request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	protected function credentials(Request $request)
	{
		$data = $request->only('email', 'password', 'token');
		// Map the password confirmation to the password
		$data['password_confirmation'] = $data['password'];

		return $data;
	}

	/**
	 * Check for duplicate username or email during registration
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function verifyDuplicates(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required_without:username|email|unique:users|max:191',
			'username' => 'bail|required_without:email|alpha_dash|unique:users|max:191|not_in:me',
		]);

		if ($validator->fails()) {
			// Validation fails
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Validation succeeds
		return response()->json([
			'message' => 'No duplicates found.'
		], 200);
	}

	/**
	 * Authenticate a user
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function validateUser(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Check if multiple users have same phone number
		$users = User::where('email', '=', $request->input('email'))
			->orWhere('phone', 'LIKE', '%'.$request->input('email'))
			->get();

		if(!$users || count($users)<=0)
		{
			return response()->json(
				[
					'error' => "User not found",
				],
				400
			);
		}

		if(count($users) > 1)
		{
			return response()->json(
				[
					'error' => 'Multiple accounts registered with this email.',
					'user' =>  new UserCollection($users)
				],
				200
			);
		}



		return response()->json(
			[
				'user' =>  new UserCollection($users)
			],
			200
		);
	}

	/**
	 * Authenticate a user
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function authenticate(Request $request)
	{
		$validator = Validator::make($request->all(), [
			//'email' => 'bail|required|email',
			'password' => 'bail|required|string|min:6',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}
		$field = 'email';

		if(is_numeric(substr($request->input('email'), 1,strlen($request->input('email'))-1))){
			$field = 'phone';
            // Check if multiple users have same phone number
			$users = User::where('email', '=', $request->input('email'))
				->orWhere('phone', '=', $request->input('email'))
				->get();

			if(count($users) > 1)
			{
				return response()->json(
					[
						'error' => 'Multiple accounts registered with this email.',
						'user' =>  new UserCollection($users)
					],
					200
				);
			}
		}


		$authAttemptParams = [
			$field => $request->input('email'),
			'password' => $request->input('password'),
		];

		$user = User::where('email', '=', $request->input('email'))
			->orWhere('phone', '=', $request->input('email'))
			->first();


		if (Auth::attempt($authAttemptParams)) {

			if (!$user->verified && !$user->is_mobile_verified) {
				$userResource = new UserResource($user);
				$userResource->forceAuth = true;
				return response()->json(
					[
						'error' => 'Please verify your account',
						'user' =>  $userResource->toArray($request)
					],
					400
				);
			}

			if ($user->is_banned) {
				if ($user->ban < 999) {
					return response()->json(
						[
							'error' =>
								"Your account has been banned for {$user->ban} " .
								str_plural('day', $user->ban),
						],
						400
					);
				} else {
					return response()->json(
						[
							'error' =>
								'Your account has been banned indefinitely',
						],
						400
					);
				}
			}



			if ($user->is_graduate) {

				return response()->json(
					[
						'error' =>
							'user is graduated',
						'statuscode' => 501
					],
					401
				);

			}

			$user->last_login = date('Y-m-d H:i:s');
			$user->last_login_ip = $request->ip();
			$user->save();

			$request->request->add([
				'grant_type' => 'password',
				'client_id' => config('canteen.auth_client_id'),
				'client_secret' => config('canteen.auth_client_secret'),
				'scope' => '*',
				'username' => $request->input('email')
			]);

			// forward the request to the oauth token request endpoint
			$tokenRequest = Request::create('/oauth/token', 'post');

			UserActivityHelper::create($user, $user, 'User Login', [
				'platform' => $request->input('platform', 'web'),
				'ip' => $request->ip()
			]);

			return Route::dispatch($tokenRequest);

		} elseif ($user) {
			//check if user if verified from mobile or email
			if (!$user->verified && !$user->is_mobile_verified) {
				$userResource = new UserResource($user);
				$userResource->forceAuth = true;
				return response()->json(
					[
						'error' => 'Please verify your account',
						'user' =>  $userResource->toArray($request)
					],
					400
				);
			}

			if ($user->is_banned) {
				if ($user->ban < 999) {
					return response()->json(
						[
							'error' =>
								"Your account has been banned for {$user->ban} " .
								str_plural('day', $user->ban),
						],
						400
					);
				} else {
					return response()->json(
						[
							'error' =>
								'Your account has been banned indefinitely',
						],
						400
					);
				}
			}
		}

		return response()->json(
			[
				'error' => 'Email/Phone or password invalid',
			],
			400
		);
	}

	/**
	 * Refresh a user auth token
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function refresh(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'refresh_token' => 'bail|required|string',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$request->request->add([
			'grant_type' => 'refresh_token',
			'client_id' => config('canteen.auth_client_id'),
			'client_secret' => config('canteen.auth_client_secret'),
			'scope' => '*',
		]);

		// forward the request to the oauth token request endpoint
		$tokenRequest = Request::create('/oauth/token', 'post');
		return Route::dispatch($tokenRequest);
	}
}
