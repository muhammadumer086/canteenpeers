import React from 'react';

import TextField from '@material-ui/core/TextField';

interface IProps {
	id?: string;
	type: string;
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	disabled?: boolean;
	multiline?: boolean;
	autoComplete?: string;
	helperText?: string;
	setRef?(any): any;
	handleChange(e: React.ChangeEvent<any>): void;
	handleBlur(e: any): void;
}

export const TextFieldFormik: React.FC<IProps> = (props: IProps) => {
	return (
		<TextField
			className="form-input"
			id={props.id}
			type={props.type}
			label={props.label}
			name={props.name}
			onChange={props.handleChange}
			onBlur={props.handleBlur}
			multiline={props.multiline}
			disabled={props.disabled}
			value={props.values[props.name]}
			error={props.touched[props.name] && !!props.errors[props.name]}
			helperText={
				props.touched[props.name] && props.errors[props.name]
					? props.errors[props.name]
					: props.helperText
			}
			autoComplete={props.autoComplete}
			inputRef={props.setRef ? props.setRef : undefined}
		/>
	);
};
