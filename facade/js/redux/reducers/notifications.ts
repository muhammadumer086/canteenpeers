import { actionTypes } from '../actions';
import { IApiNotification } from '../../interfaces';

export interface IStoreState {
	notificationsPanelDisplay: boolean;
	notificationLoading: boolean;
	notificationUnreadCount: number;
	notificationMessagesCount: number;
	notificationsData: IApiNotification[];
	notificationsMaxPage: number;
}

export const notifications = (
	state: IStoreState = {
		notificationsPanelDisplay: false,
		notificationLoading: false,
		notificationUnreadCount: 0,
		notificationMessagesCount: 0,
		notificationsData: [],
		notificationsMaxPage: 0,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.NOTIFICATIONS_PANEL_DISPLAY:
			return Object.assign({}, state, {
				notificationsPanelDisplay: true,
			});
		case actionTypes.NOTIFICATIONS_PANEL_HIDE:
			return Object.assign({}, state, {
				notificationsPanelDisplay: false,
			});
		case actionTypes.NOTIFICATION_LOADING:
			return Object.assign({}, state, {
				notificationLoading: true,
			});
		case actionTypes.NOTIFICATION_LOADED:
			return Object.assign({}, state, {
				notificationLoading: false,
				notificationUnreadCount: action.notificationUnreadCount,
				notificationMessagesCount: action.notificationMessagesCount,
				notificationsData: action.notificationsData,
				notificationsMaxPage: action.notificationsMaxPage,
			});
		default:
			return state;
	}
};
