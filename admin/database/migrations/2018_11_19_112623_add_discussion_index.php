<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscussionIndex extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->dropColumn('private');
			$table->dropColumn('closed');
			$table->dateTime('closed_at')->nullable();
			$table->integer('discussion_index')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->unsignedInteger('private')->default(0);
			$table->boolean('closed')->nullable();
			$table->dropColumn('closed_at');
			$table->dropColumn('discussion_index');
		});
	}
}
