<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Discussion;
use App\User;

class UpdateCounts extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:updateCounts {--all : Update everything}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the count attributes';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$discussionsQuery = null;
		$usersQuery = null;

		\App\User::disableSearchSyncing();
		\App\Discussion::disableSearchSyncing();

		$this->line('');
		$this->info('Updating counts');

		if ($this->option('all')) {
			$discussionsQuery = Discussion::whereNull('deleted_at');
			$usersQuery = User::whereNull('deleted_at');
		} else {
			$discussionsQuery = Discussion::where(
				'updated_at',
				'>=',
				Carbon::now()->subMinute()
			);
			$usersQuery = User::where(
				'updated_at',
				'>=',
				Carbon::now()->subMinute()
			);
		}

		$this->reportNumbers('Discussions', $discussionsQuery);
		$this->line('');
		$this->reportNumbers('Users', $usersQuery);

		$this->info("\r\nFinished updating counts");
		$this->info('Updating support count');
		$this->updateDiscussionSupport();
		$this->info("\r\nFinished updating support count");
		$this->line("\r\n");
	}

	protected function reportNumbers($name, $query) {
		$this->line($name);
		$bar = $this->output->createProgressBar($query->count());
		$bar->start();
		$bar->setRedrawFrequency(10);
		$query->chunk(10, function ($entities) use ($bar) {
			foreach ($entities as $entity) {
				$entity->timestamps = false;
				$entity->reportNumbers();
				$entity->save();
				$bar->advance();
			}
		});
		$bar->finish();
	}

	protected function updateDiscussionSupport() {
		$query = Discussion::whereNull('deleted_at');
		$bar = $this->output->createProgressBar($query->count());
		$bar->start();
		$bar->setRedrawFrequency(10);
		$query->chunk(10, function ($discussions) use ($bar) {
			foreach ($discussions as $discussion) {
				// Clear the discussion supports
				DB::table('support_users_in_discussion')
					->where('discussion_id', $discussion->id)
					->delete();
				// Rebuild the discussion I get it
				$discussion
					->likes()
					->chunk(10, function ($likes) use ($discussion) {
						foreach ($likes as $like) {
							$userId = $like->user->id;
							$discussion
								->supportUsersInDiscussion()
								->syncWithoutDetaching([
									$userId => ['like' => true],
								]);
						}
					});
				// Rebuild the discussion hugs
				$discussion
					->hugs()
					->chunk(10, function ($hugs) use ($discussion) {
						foreach ($hugs as $hug) {
							$userId = $hug->user->id;
							$discussion
								->supportUsersInDiscussion()
								->syncWithoutDetaching([
									$userId => ['hug' => true],
								]);
						}
					});
				// Rebuild the discussion replies
				if ($discussion->main_discussion_id === null) {
					$discussion
						->childDiscussions()
						->chunk(10, function ($replies) use ($discussion) {
							foreach ($replies as $reply) {
								$userId = $reply->user->id;
								$discussion
									->supportUsersInDiscussion()
									->syncWithoutDetaching([
										$userId => ['reply' => true],
									]);
							}
						});
				} else {
					$discussion
						->repliesDiscussions()
						->chunk(10, function ($replies) use ($discussion) {
							foreach ($replies as $reply) {
								$userId = $reply->user->id;
								$discussion
									->supportUsersInDiscussion()
									->syncWithoutDetaching([
										$userId => ['reply' => true],
									]);
							}
						});
				}
				$bar->advance();
			}
		});
		$bar->finish();
	}
}
