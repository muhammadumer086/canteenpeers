<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupportUsersInDiscussion extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('support_users_in_discussion', function (
			Blueprint $table
		) {
			$table->increments('id');
			$table->unsignedInteger('discussion_id');
			$table->unsignedInteger('user_id');
			$table->boolean('reply')->default(false);
			$table->boolean('like')->default(false);
			$table->boolean('hug')->default(false);
			$table->timestamps();

			$table
				->foreign('discussion_id')
				->references('id')
				->on('discussions')
				->onDelete('cascade');

			$table
				->foreign('user_id')
				->references('id')
				->on('users')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('support_users_in_discussion');
	}
}
