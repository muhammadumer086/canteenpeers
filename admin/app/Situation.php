<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situation extends Model {
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['slug', 'name', 'cancer_type', 'parent_id'];

	/**
	 * All of the relationships to be touched.
	 *
	 * @var array
	 */
	protected $touches = ['parentSituation'];

	/**
	 * The users that belong to this situation
	 */
	public function users() {
		return $this->belongsToMany('App\User');
	}

	/**
	 * The topics connected to this situation
	 */
	public function topics() {
		return $this->belongsToMany('App\Topic');
	}

	/**
	 * The discussions connected to this situation
	 */
	public function discussions() {
		return $this->belongsToMany('App\Discussion');
	}

	/**
	 * The blogs connected to this situation
	 */
	public function blogs() {
		return $this->belongsToMany('App\Blog');
	}

	/**
	 * The situation topics related to this situation
	 */
	public function situationTopics() {
		return $this->hasMany('App\SituationTopic');
	}

	/**
	 * The parent situation
	 */
	public function parentSituation() {
		return $this->belongsTo('App\Situation', 'parent_id');
	}

	/**
	 * The child situations
	 */
	public function childSituations() {
		return $this->hasMany('App\Situation', 'parent_id');
	}
}
