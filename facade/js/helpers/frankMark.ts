export function frankMark() {
	if (typeof window !== 'undefined') {
		const frankMark = [
			'                                  ',
			'     ┌─┐                          ',
			' ┌─┬─┼─┘                          ',
			' ├─┼─┘   Website by Frank Digital ',
			' └─┘     www.frankdigital.com.au  ',
			'                                  ',
		];
		const frankStyles = [
			'color: #ff4229',
			'display: block',
		];
		console.log('%c' + frankMark.join('\r\n'), frankStyles.join(';'));
	}
}
