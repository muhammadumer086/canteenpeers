import React       from 'react';
import { connect } from 'react-redux';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { Breakpoint } from '@material-ui/core/styles/createBreakpoints';

import { IStoreState } from "../../redux/store";
import DashboardSidebar from './DashboardSidebar';
import { menuPanelDisplay, menuPanelHide } from '../../redux/actions/menuPanel';

interface IProps {
	menuPanelDisplay: boolean;
	width: Breakpoint;
	dispatch(action: any): void;
}

const MenuPanel: React.FC<IProps> = (props: IProps) => {
	if (isWidthDown('sm', props.width)) {
		return (
			<SwipeableDrawer
				open={props.menuPanelDisplay}
				onOpen={() => {
					props.dispatch(menuPanelDisplay());
				}}
				onClose={() => {
					props.dispatch(menuPanelHide());
				}}
			>
				<DashboardSidebar />
			</SwipeableDrawer>
		);
	}
	return null;
}

function mapStateToProps(state: IStoreState) {
	const {
		menuPanelDisplay
	} = state.menuPanel;
	return {
		menuPanelDisplay,
	};
}

export default connect(mapStateToProps)(withWidth()(MenuPanel));
