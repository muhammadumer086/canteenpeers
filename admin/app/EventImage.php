<?php

namespace App;

use App\Event;
use App\User;
use BinaryCabin\LaravelUUID\Traits\HasUUID;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class EventImage extends Model {
	use HasUUID;

	protected $uuidFieldName = 'id';
	protected $table = 'event_images';
	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $fillable = ['id'];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['image_url'];

	/**
	 * The users that created this image
	 */
	public function user() {
		return $this->belongsTo(User::class);
	}

	/**
	 * The event associated with this image
	 */
	public function event() {
		return $this->belongsTo(Event::class);
	}

	public function getImageUrlAttribute() {
		if (substr($this->path, 0, 4) === 'http') {
			return $this->path;
		}
		return Storage::disk('s3')->url($this->path);
	}
}
