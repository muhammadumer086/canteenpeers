import React from 'react';
import { connect } from 'react-redux';
import AnimateHeight from 'react-animate-height';
import dotProp from 'dot-prop-immutable';

import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
// import FilterListIcon from '@material-ui/icons/FilterList';

import ReactSVG from 'react-svg';
import CloseIcon from '@material-ui/icons/Close';
import Switch from '@material-ui/core/Switch';

import { EContactModalTypes, FilterOrder ,ISelectOption} from '../../interfaces';
import { IStoreState, contactModalDisplay } from '../../redux/store';

import Pill from '../general/Pill';

import RadioGroup from '../inputs/RadioGroup';

export interface IFilterValue {
	label: string;
	value: string;
	active: boolean;
}

export interface IFiltersGroup {
	label: string | null;
	withToggle?: boolean;
	filterToggle?: boolean;
	paramName: string;
	noBorder?: boolean;
	singleValue?: boolean;
	filters: IFilterValue[];
}

export interface IFilters {
	filtersTitle: string;
	withToggle: boolean;
	filterToggleLabel: string;
	filterToggle: boolean;
	filterGroups: IFiltersGroup[];
}

export interface IFiltersSelected {
	filters: {
		paramName: string;
		filterValues: IFilterValue | IFilterValue[];
	}[];
}

interface IProps {
	filters: IFilters;
	selectedFilters: IFiltersSelected;
	suggestTopic?: boolean;
	total: number;
	totalString: {
		noResults: string;
		singular: string;
		plural: string;
	};
	order?: FilterOrder;
	userAuthenticated: boolean;
	onOrderChange?(order: FilterOrder): void;
	gtmDataLayer?(value: string): void;
	onToggle(): void;
	onUpdateFilters(filters: IFilters): void;
	onReset(): void;
	onSubmit(selectedFilters: IFiltersSelected): void;
	dispatch(action: any): void;
	loadMyEvents?(append: boolean, past_events: string): void;
	myFilterOptions? : ISelectOption[];
}

interface IState {
	active: boolean;
	totalFilters: number;
	filterEvent: string;
}

export class FilterResultsHelpers {
	public updateFiltersBasedOnSelectedFilters(
		selectedFilters: IFiltersSelected,
		originalFilters: IFilters,
	): IFilters {
		const filters: IFilters = JSON.parse(JSON.stringify(originalFilters));
		this.resetFilterValues(filters);
		selectedFilters.filters.forEach(singleFilter => {
			if (singleFilter.filterValues instanceof Array) {
				singleFilter.filterValues.forEach(
					(singleFilterValue: IFilterValue) => {
						this.updateFiltersValue(
							filters,
							singleFilterValue,
							singleFilter.paramName,
						);
					},
				);
			} else {
				this.updateFiltersValue(
					filters,
					singleFilter.filterValues,
					singleFilter.paramName,
				);
			}
		});

		return filters;
	}

	protected resetFilterValues(filters: IFilters) {
		filters.filterGroups.forEach((value: IFiltersGroup) => {
			value.filters.forEach((filterSingleValue: IFilterValue) => {
				filterSingleValue.active = false;
			});
		});
	}

	protected updateFiltersValue(
		filters: IFilters,
		filterValue: IFilterValue,
		paramName: string,
	) {
		filters.filterGroups.forEach((value: IFiltersGroup) => {
			if (value.paramName === paramName) {
				value.filters.forEach((filterSingleValue: IFilterValue) => {
					if (filterSingleValue.value === filterValue.value) {
						filterSingleValue.active = true;
					}
				});
			}
		});
	}
}

class FilterResults extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			active: false,
			totalFilters: this.getTotalFilters(props.selectedFilters),
			filterEvent: 'all',
		};

		this.handleOpenToggle = this.handleOpenToggle.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleMyFilterChange = this.handleMyFilterChange.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
	}

	public componentDidUpdate(oldProps: IProps) {
		if (
			JSON.stringify(this.props.selectedFilters) !==
			JSON.stringify(oldProps.selectedFilters)
		) {
			this.setState({
				totalFilters: this.getTotalFilters(this.props.selectedFilters),
			});
		}
	}

	public render() {
		const arrowIconUrl = `${process.env.STATIC_PATH}/icons/filters.svg`;
		const classNames = ['theme--primary', 'filters'];
		const { userAuthenticated, loadMyEvents,myFilterOptions } = this.props;
		if (this.state.active) {
			classNames.push('filters--active');
		}

		return (
			<div className={classNames.join(' ')}>
				<div className="filters-container">
					<div className="filters-actions">
						<Button
							className="filters-actions-button"
							onClick={this.handleOpenToggle}
							variant={
								this.state.active ? 'contained' : 'outlined'
							}
							color={this.state.active ? 'primary' : 'primary'}
						>
							<span>
								{this.props.filters.filtersTitle}
								{this.state.totalFilters > 0 && (
									<span>
										&nbsp;({this.state.totalFilters})
									</span>
								)}
							</span>
							<span className="filters-actions-button-icon">
								<ReactSVG
									path={arrowIconUrl}
									// svgStyle={{ 'fill': `${(this.state.active) ? '#FFFFFF' : '#FF5F33'}` }}
									svgStyle={{ fill: 'inherit' }}
								/>
							</span>
						</Button>
						{loadMyEvents && userAuthenticated && (
							<div className="auth_form" >
							<div className="form-row">
								<div className="form-column">
									<RadioGroup
										label="Selcet Filter"
										value={this.state.filterEvent}
										handleChange={this.handleMyFilterChange}
										options={myFilterOptions?myFilterOptions : [
											{
												label: 'Upcoming Events',
												value: 'all',
											},
											{
												label: 'My Events',
												value: 'my_events',
											},
											{
												label: 'My Past Events',
												value: 'past_events',
											},
										]}
									/>
								</div>
							</div>
							</div>
						)}
						{/* {loadMyEvents && userAuthenticated && (
							<Button
								onClick={() => loadMyEvents(false, false)}
								className="event_filter-button"
								color="primary"
								variant="outlined"
								style={{ margin: '0 5px' }}
							>
								My Events
							</Button>
						)}
						{loadMyEvents && userAuthenticated && (
							<Button
								onClick={() => loadMyEvents(false, true)}
								className="event_filter-button"
								color="primary"
								variant="outlined"
							>
								Past Events
							</Button>
						)} */}

						{this.props.onOrderChange && this.props.order && (
							<Select
								value={this.props.order}
								onChange={evt => {
									this.props.onOrderChange(
										evt.target.value as any,
									);
								}}
								name="order"
								className="filters-actions-order"
							>
								<MenuItem value="DESC">
									Newest to Oldest
								</MenuItem>
								<MenuItem value="ASC">
									Oldest to Newest
								</MenuItem>
							</Select>
						)}
					</div>

					<AnimateHeight
						duration={400}
						height={this.state.active ? 'auto' : 0}
					>
						<div className="filters-content_container theme--main">
							<div className="filters-content">
								<div className="filters-list">
									{this.props.filters.withToggle && (
										<div className="filters-list-section filters-list-section--small">
											<label className="filters-list-section-label theme-title">
												
													<b>
													{
														this.props.filters
															.filterToggleLabel
													}
													</b>
												
												<Switch
													className="filters-list-section-label-switch"
													checked={
														this.props.filters
															.filterToggle
													}
													onChange={
														this.props.onToggle
													}
												/>
											</label>
										</div>
									)}

									{this.props.filters.filterGroups.map(
										(filterGroup, groupIndex) => {
											const groupClassNames = [
												'filters-list-section',
											];
											if (filterGroup.noBorder) {
												groupClassNames.push(
													'filters-list-section--no_border',
												);
											}

											return (
												<div
													className={groupClassNames.join(
														' ',
													)}
													key={groupIndex}
												>
													{!!filterGroup.label && (
														<div className="filters-list-section_label hm-b16">
															<label>
																<b className="theme-title">
																	{
																		filterGroup.label
																	}
																</b>
																{!!filterGroup.withToggle && (
																	<Switch
																		checked={
																			filterGroup.filterToggle
																		}
																		onChange={() => {
																			this.handleFiltersToggle(
																				groupIndex,
																			);
																		}}
																	/>
																)}
															</label>
														</div>
													)}
													{(!filterGroup.filterToggle ||
														filterGroup.filterToggle ===
															undefined) && (
														<div className="filters-list-section_options">
															{filterGroup.filters.map(
																(
																	filterOption,
																	filterIndex,
																) => (
																	<Pill
																		key={
																			filterIndex
																		}
																		label={
																			filterOption.label
																		}
																		active={
																			filterOption.active
																		}
																		disabled={
																			false
																		}
																		clickable={
																			true
																		}
																		onClick={() => {
																			this.handleFiltersUpdate(
																				groupIndex,
																				filterIndex,
																			);
																			this.props.gtmDataLayer(
																				filterOption.value,
																			);
																		}}
																	/>
																),
															)}
														</div>
													)}
												</div>
											);
										},
									)}
									{!!this.props.suggestTopic && (
										<div className="filters-list-section filters-list-section--no_border filters-list-section--small-bottom">
											<button
												className="filters-list-section-link"
												onClick={() => {
													this.props.dispatch(
														contactModalDisplay(
															EContactModalTypes.SUGGEST_TOPIC,
														),
													);
												}}
											>
												Suggest a new topic
											</button>
										</div>
									)}
									{this.renderFilterSubmit()}
								</div>
							</div>

							<button
								onClick={this.handleOpenToggle}
								className="close_button close_button--primary"
								title="Close filters"
							>
								<CloseIcon className="close_button-icon" />
							</button>
						</div>
					</AnimateHeight>

					{!!this.props.selectedFilters.filters.length && (
						<div className="filters-pills_list">
							<div className="filters-pills_list-label">
								{this.props.total === 0 && (
									<span>
										{this.props.totalString.noResults} with
										the following filters
									</span>
								)}
								{this.props.total === 1 && (
									<span>
										{this.props.totalString.singular} with
										the following filters
									</span>
								)}
								{this.props.total > 1 && (
									<span>
										{this.props.total}{' '}
										{this.props.totalString.plural} with the
										following filters
									</span>
								)}
							</div>
							<div className="filters-pills_list-content">
								{this.props.selectedFilters.filters.map(
									(filter, filterIndex) => {
										if (
											filter.filterValues instanceof Array
										) {
											return filter.filterValues.map(
												(filterValue, valueIndex) => {
													return (
														<Pill
															key={`${filter.paramName}-${filterValue.value}`}
															label={
																filterValue.label
															}
															active={true}
															disabled={false}
															clickable={true}
															deletable={true}
															onClick={() => {
																this.handleRemoveSelectedFilter(
																	filterIndex,
																	valueIndex,
																);
															}}
														/>
													);
												},
											);
										} else {
											return (
												<Pill
													key={`${filter.paramName}-${filter.filterValues.value}`}
													label={
														filter.filterValues
															.label
													}
													active={true}
													disabled={false}
													clickable={true}
													deletable={true}
													onClick={() => {
														this.handleRemoveSelectedFilter(
															filterIndex,
														);
													}}
												/>
											);
										}
									},
								)}
							</div>
						</div>
					)}
				</div>
			</div>
		);
	}

	protected handleOpenToggle() {
		this.setState(
			{
				active: !this.state.active,
			},
			() => {
				if (!this.state.active) {
					this.handleSubmit();
				}
			},
		);
	}

	protected handleRemoveSelectedFilter(
		filterIndex: number,
		valueIndex?: number,
	) {
		let selectedFilters: IFiltersSelected;
		if (
			typeof valueIndex !== 'undefined' &&
			this.props.selectedFilters.filters[filterIndex]
				.filterValues instanceof Array &&
			(this.props.selectedFilters.filters[filterIndex]
				.filterValues as IFilterValue[]).length > 1
		) {
			selectedFilters = dotProp.delete(
				this.props.selectedFilters,
				`filters.${filterIndex}.filterValues.${valueIndex}`,
			);
		} else {
			selectedFilters = dotProp.delete(
				this.props.selectedFilters,
				`filters.${filterIndex}`,
			);
		}

		this.props.onSubmit(selectedFilters);
	}

	protected handleFiltersUpdate(groupIndex: number, optionIndex: number) {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.props.filters),
		);
		// Get the group to be updated
		const filterGroup: IFiltersGroup = filters.filterGroups[groupIndex];

		if (filterGroup.singleValue) {
			// Remove all the options set to true
			filterGroup.filters.forEach((filterValue, index) => {
				if (index === optionIndex) {
					filterValue.active = !filterValue.active;
				} else {
					filterValue.active = false;
				}
			});
		} else {
			filterGroup.filters[optionIndex].active = !filterGroup.filters[
				optionIndex
			].active;
		}

		this.props.onUpdateFilters(filters);
	}

	protected handleFiltersToggle(groupIndex: number) {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.props.filters),
		);
		// Get the group to be updated
		const filterGroup: IFiltersGroup = filters.filterGroups[groupIndex];

		filterGroup.filterToggle = !filterGroup.filterToggle;

		this.props.onUpdateFilters(filters);
	}

	protected renderFilterSubmit() {
		return (
			<div className="filters-list-section filters-list-section--submit">
				<Button
					className="filters-list-section-button filters-list-section-button--submit"
					variant="contained"
					color="primary"
					onClick={this.handleSubmit}
				>
					Go
				</Button>
				<Button
					className="filters-list-section-button filters-list-section-button--reset"
					variant="outlined"
					color="primary"
					onClick={this.props.onReset}
				>
					Reset
				</Button>
			</div>
		);
	}

	protected handleSubmit() {
		this.setState({
			active: false,
			filterEvent : "all"
		});

		// Process the selected filters
		const selectedFilters: IFiltersSelected = {
			filters: [],
		};

		this.props.filters.filterGroups.forEach(filterGroup => {
			let hasFilters = false;
			let value: IFilterValue = null;
			const values: IFilterValue[] = [];

			if (
				!filterGroup.filterToggle ||
				filterGroup.filterToggle === undefined
			) {
				filterGroup.filters.forEach(singleValue => {
					if (singleValue.active) {
						hasFilters = true;
						if (filterGroup.singleValue) {
							value = singleValue;
						} else {
							values.push(singleValue);
						}
					}
				});
			}

			if (hasFilters) {
				selectedFilters.filters.push({
					paramName: filterGroup.paramName,
					filterValues: filterGroup.singleValue ? value : values,
				});
			}
		});

		this.props.onSubmit(selectedFilters);
	}

	protected getTotalFilters(selectedFilters: IFiltersSelected): number {
		let total = 0;
		if (selectedFilters && selectedFilters.filters.length > 0) {
			selectedFilters.filters.forEach(filter => {
				if (filter.filterValues instanceof Array) {
					total += filter.filterValues.length;
				} else {
					total += 1;
				}
			});
		}
		return total;
	}
	protected handleMyFilterChange = e => {
		this.setState({ filterEvent: e.target.value },()=>{
			if(this.state.filterEvent!=="all")
			{
				this.props.onReset();
			}
		});
		this.props.loadMyEvents(false, e.target.value);
	};
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return { userAuthenticated };
}

export default connect(mapStateToProps)(FilterResults);
