<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Conversation;

class UpdateGroupConversations extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:updateGroupConversations';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the conversations to assign the group_conversation';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$conversationsQuery = Conversation::whereNull('deleted_at');

		$conversationsQuery->chunk(10, function ($conversations) {
			foreach ($conversations as $conversation) {
				$conversation->group_conversation =
					$conversation->users()->count() > 2;
				$conversation->save(['timestamps' => false]);
			}
		});
	}
}
