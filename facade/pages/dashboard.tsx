import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import { connect } from 'react-redux';

import {
	IApiDashboard,
	IRelevantData,
	IRelevantMeta,
	IRelevantContent,
} from '../js/interfaces';

import { ApiService, AuthService } from '../js/services';
import Footer from '../js/components/general/Footer';
import EventTileList from '../js/components/events/EventTileList';
import LayoutDashboard from '../js/components/general/LayoutDashboard';
import Announcements from '../js/components/dashboard/Announcements';
import RelevantDiscussions from '../js/components/dashboard/RelevantDiscussions';
import NewsletterSignupForm from '../js/components/dashboard/NewsletterSignupForm';
import SubTitleDetail from '../js/components/general/SubTitleDetail';
import RelevantTiles from '../js/components/dashboard/RelevantTiles';
import SectionTitle from '../js/components/general/SectionTitle';
import QuickLinkList from '../js/components/general/QuickLinks/QuickLinksList';
//import ChatToCounsellor from '../js/components/general/ChatToCounsellor';

import { IStoreState, apiError, apiSuccess } from '../js/redux/store';

 
import CircularProgress from '@material-ui/core/CircularProgress';

interface IProps {
	userAuthenticated: boolean | null;
	userVerified: boolean | null;
	dashboardData: IApiDashboard;
	apiService: ApiService;
	authService: AuthService;
	updateDataCount: number;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	dashboardData: IApiDashboard | null;
	relevantData: IRelevantData[];
	isSubmitting: boolean;
	isSubscribed: boolean;
	sectionsTitles: {
		discussions: IRelevantMeta;
		blogs: IRelevantMeta;
		resources: IRelevantMeta;
	};
	showStaticTiles: boolean;
}

interface INewsletterFormValues {
	email: string;
	name: string;
}

class Index extends React.Component<IProps, IState> {
	// static async getInitialProps({ apiService }) {
	// 	const dashboardData = await (apiService as ApiService).queryGET(
	// 		'/api/dashboard',
	// 	);

	// 	return {
	// 		dashboardData,
	// 	};
	// }

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			dashboardData: props.dashboardData,
			relevantData: this.renderRelevantData(),
			isSubmitting: false,
			isSubscribed: false,
			sectionsTitles: this.getSectionTitles(),
			showStaticTiles: false,
		};

		this.closeAnnouncement = this.closeAnnouncement.bind(this);
		this.updateData = this.updateData.bind(this);
		this.subscribeToNewsletter = this.subscribeToNewsletter.bind(this);
		this.handleQuickLinksClose = this.handleQuickLinksClose.bind(this);
		this.isSessionValueStored = this.isSessionValueStored.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.updateDataCount !== prevProps.updateDataCount
		) {
			this.updateData();
		}
	}

	public componentDidMount() {
		this.isSessionValueStored('showStaticTiles');
		this.updateData();
	}

	public render() {
		// const pageTitle = `Canteen Connect For Teens - Main Dashboard`;
		const { dashboardData, isUpdating } = this.state;
		const url = buildUrl(process.env.APP_URL, {
			path: `/dashboard`,
		});

		// const seoDescription =
		// 	'Check out the main dashboard of our website which you can use to manage your communications. Register to connect with other teens &amp; get support.';
		const seoFeaturedImage = `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;
		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
					<meta name="og:url" property="og:url" content={url} />
					<meta name="og:type" property="og:type" content="website" />
					

					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>
				</Head>
				<LayoutDashboard>
					{isUpdating ? (
						<div style={{display : "flex",justifyContent : "center",alignItems : "center",height : "100%",width : "100%"}}>
							<CircularProgress />
						</div>
					) : (
						<React.Fragment>
							{dashboardData && dashboardData.announcement && (
								<Announcements
									announcement={
										this.state.dashboardData.announcement
									}
									closeAnnouncement={this.closeAnnouncement}
								/>
							)}

							{dashboardData &&
								dashboardData.discussions &&
								!!this.state.dashboardData.discussions
									.length && (
									<RelevantDiscussions
										title={
											this.state.sectionsTitles
												.discussions.title
										}
										discussions={
											this.state.dashboardData.discussions
										}
									/>
								)}

							{this.props.userAuthenticated &&
								this.state.showStaticTiles &&
								dashboardData &&
								dashboardData.links.length > 0 && (
									<div className="dashboard_quick_links">
										<SectionTitle
											label="Suggested for you"
											iconSlug="star-outline"
										/>

										<QuickLinkList
											quickLinks={
												this.state.dashboardData.links
											}
											handleClose={
												this.handleQuickLinksClose
											}
										/>
									</div>
								)}

							{dashboardData &&
								dashboardData.events.length &&
								this.renderEvents(dashboardData)}

							{((dashboardData && dashboardData.blogs.length) ||
								(dashboardData &&
									dashboardData.resources.length)) && (
								<div className="relevant_sections">
									<div className="relevant_sections-container">
										{dashboardData &&
											dashboardData.blogs.length && (
												<React.Fragment>
													<SubTitleDetail
														{...this.state
															.sectionsTitles
															.blogs}
													/>
													<RelevantTiles
														type="blog"
														linkPrefix="/blogs"
														content={
															dashboardData.blogs
														}
													/>
												</React.Fragment>
											)}
										{dashboardData &&
											dashboardData.resources.length && (
												<React.Fragment>
													<SubTitleDetail
														{...this.state
															.sectionsTitles
															.resources}
													/>
													<RelevantTiles
														type="resource"
														linkPrefix="/resources"
														content={
															dashboardData.resources
														}
													/>
												</React.Fragment>
											)}
									</div>
								</div>
							)}

							{!this.props.userAuthenticated && (
								<section className="newsletter_signup">
									<div className="newsletter_signup-container theme-main">
										{this.state.isSubscribed ? (
											<h3 className="newsletter_signup-header theme-title font--h4">
												Thanks for signing up to our
												newsletter!
											</h3>
										) : (
											<React.Fragment>
												<h3 className="newsletter_signup-header theme-title font--h4">
													Get the latest news and
													updates for Canteen Connect
												</h3>

												<NewsletterSignupForm
													isSubmitting={
														this.state.isSubmitting
													}
													onSubmit={
														this
															.subscribeToNewsletter
													}
												/>
											</React.Fragment>
										)}
									</div>
								</section>
							)}
						</React.Fragment>
					)}
					<Footer />
					{/* <ChatToCounsellor /> */}
				</LayoutDashboard>
			</div>
		);
	}

	protected updateData() {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				this.props.apiService
					.queryGET('/api/dashboard')
					.then((dashboardData: IApiDashboard) => {
						this.setState({
							isUpdating: false,
							dashboardData,
							sectionsTitles: this.getSectionTitles(),
						});
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isUpdating: false,
						});
					});
			},
		);
	}

	protected closeAnnouncement(id: number) {
		if (
			this.props.userAuthenticated &&
			this.props.dashboardData.announcement &&
			'announcement' in this.props.dashboardData
		) {
			this.setState(
				{
					isUpdating: true,
				},
				() => {
					this.props.apiService
						.queryGET(`/api/announcements/${id}/close`)
						.then(() => {
							this.setState({
								isUpdating: false,
							});
							this.updateData();
						})
						.catch(error => {
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isUpdating: false,
							});
						});
				},
			);
		}
	}

	protected renderEvents(dashboardData: IApiDashboard) {
		const headingConfig = {
			icon: 'events',
			title: 'Events you might be interested in',
			linkLabel: 'All events',
			linkData: {
				prefix: '/events',
				href: '/events',
				as: '/events',
			},
		};

		return (
			dashboardData &&
			dashboardData.events.length && (
				<div className="dashboard_events">
					<SubTitleDetail {...headingConfig} />

					<EventTileList events={this.state.dashboardData.events} />
				</div>
			)
		);
	}

	protected renderRelevantData(): IRelevantData[] | null {
		const numberOfDataSets = 2;
		const contentData = [];

		if (
			!this.props.dashboardData ||
			!('blogs' in this.props.dashboardData) ||
			!('resources' in this.props.dashboardData) ||
			this.props.dashboardData.blogs.length === 0 ||
			this.props.dashboardData.resources.length === 0
		) {
			return null;
		}

		contentData.push(
			JSON.parse(JSON.stringify(this.props.dashboardData.blogs)),
		);
		contentData.push(
			JSON.parse(JSON.stringify(this.props.dashboardData.resources)),
		);

		const metaData = [
			{
				icon: 'blogs',
				title: 'Helpful blogs you may be interested in',
				linkLabel: 'All blogs',
				linkData: {
					prefix: '/blog',
					href: '/blogs',
					as: '/blog',
				},
			},
			{
				icon: 'resources',
				title: 'Resources just for you',
				linkLabel: 'All resources',
				linkData: {
					prefix: '/resource',
					href: '/resources',
					as: '/resources',
				},
			},
		];

		const dataSet = this.buildDataSet(
			numberOfDataSets,
			metaData,
			contentData,
		);

		return dataSet;
	}

	protected buildDataSet(
		numberOfSets: number,
		metaData: IRelevantMeta[],
		contentData: IRelevantContent[],
	): IRelevantData[] {
		const result = [];

		for (let i = 0; i < numberOfSets; i++) {
			result.push({});

			result[i].relevantMeta = metaData[i];
			result[i].relevantContent = contentData[i];
		}

		return result;
	}

	protected subscribeToNewsletter(values: INewsletterFormValues) {
		if (!values) {
			return;
		}

		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/newsletter/subscribe', values)
					.then(() => {
						this.setState({
							isSubmitting: false,
							isSubscribed: true,
						});
						this.props.dispatch(
							apiSuccess([
								'Successfully signed up to the newsletter',
							]),
						);
					})
					.catch(error => {
						this.setState({
							isSubmitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected getSectionTitles() {
		return {
			discussions: {
				icon: 'discussions',
				title:
					this.props.userAuthenticated === true
						? 'Discussions relevant to you'
						: 'Newest discussions',
				linkData: {
					href: '/discussions',
					as: '/discussions',
				},
				linkLabel: 'All discussions',
			},
			blogs: {
				icon: 'blogs',
				title:
					this.props.userAuthenticated === true
						? 'Latest Blogs for you'
						: 'Latest Blogs',
				linkLabel: 'All blogs',
				linkData: {
					prefix: '/blog',
					href: '/blogs',
					as: '/blogs',
				},
			},
			resources: {
				icon: 'resources',
				title:
					this.props.userAuthenticated === true
						? 'Resources Relevant to you'
						: 'Helpful Resources',
				linkLabel: 'All resources',
				linkData: {
					prefix: '/resource',
					href: '/resources',
					as: '/resources',
				},
			},
		};
	}

	protected handleQuickLinksClose() {
		sessionStorage.setItem('showStaticTiles', 'false');

		this.setState({
			showStaticTiles: false,
		});
	}

	protected isSessionValueStored(value) {
		let data = sessionStorage.getItem(value);

		if (data && data === 'false') {
			this.setState({
				showStaticTiles: false,
			});
		} else {
			this.setState({
				showStaticTiles: true,
			});
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userVerified } = state.user;
	const { updateDataCount } = state.updateData;
	return {
		userAuthenticated,
		userVerified,
		updateDataCount,
	};
}

export default connect(mapStateToProps)(Index);
