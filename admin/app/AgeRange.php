<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgeRange extends Model {
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];
}
