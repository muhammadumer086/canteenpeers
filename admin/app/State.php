<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Event;

class State extends Model
{
	public $timestamps = false;

	public function events() {
		return $this->hasMany(Event::class);
	}
}
