<?php

return [
	/*
	|---------------------------------------------------------------------------
	| Events synchronisation endpoint
	|---------------------------------------------------------------------------
	|
	*/
	'api' => env(
		'SIDE_OF_STAGE_SYNC',
		isset($_SERVER['SIDE_OF_STAGE_API'])
			? $_SERVER['SIDE_OF_STAGE_API']
			: 'https://staging.chookdigital.net/sideofstage.org.au/api'
	),

	'event_email_address' => env(
		'SIDE_OF_STAGE_EVENT_EMAIL',
		isset($_SERVER['SIDE_OF_STAGE_EVENT_EMAIL'])
			? $_SERVER['SIDE_OF_STAGE_EVENT_EMAIL']
			: 'digitech@canteen.org.au'
	),

	/*
	|---------------------------------------------------------------------------
	| The key Side of stage needs to provide in order to register its users to the platform
	|---------------------------------------------------------------------------
	|
	*/
	'access_key' => env(
		'SIDE_OF_STAGE_ACCESS_KEY',
		isset($_SERVER['SIDE_OF_STAGE_ACCESS_KEY'])
			? $_SERVER['SIDE_OF_STAGE_ACCESS_KEY']
			: 'sUNc3LYkV0zlBMcPh17uTEHno4t095uRmiV5LDxuwR1zFh66Yr'
	),
];
