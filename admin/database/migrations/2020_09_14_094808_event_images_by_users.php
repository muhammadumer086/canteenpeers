<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventImagesByUsers extends Migration
{
	public function up() {
		Schema::table('event_images', function (Blueprint $table) {
			$table->unsignedInteger('user_id')->nullable();
			$table->boolean('is_featured')->default(false);
			$table
				->foreign('user_id')
				->references('id')
				->on('users')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('event_images', function (Blueprint $table) {
			$table->dropForeign('event_images_user_id_foreign');
			$table->dropColumn('user_id');
			$table->dropColumn('is_featured');
		});
	}
}
