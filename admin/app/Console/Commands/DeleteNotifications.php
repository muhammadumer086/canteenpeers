<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Http\Resources\Notification as NotificationResource;
use Illuminate\Support\Facades\DB;

class DeleteNotifications extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:deleteNotifications';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete any old notification or notifications related to a deleted entry';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$idsToDelete = [];
		$maxReadNotification = Carbon::today()->subDays(15);
		$maxNotification = Carbon::today()->subDays(60);

		DB::table('notifications')
			->orderBy('created_at', 'ASC')
			->chunk(100, function ($notifications) use (
				&$idsToDelete,
				$maxReadNotification,
				$maxNotification
			) {
				foreach ($notifications as $notification) {
					// Create a resource from the notification
					$processedNotification = new NotificationResource(
						$notification
					);

					$shouldDelete =
						// Related model deleted
						!$processedNotification->shouldDisplayNotification() ||
						// Old read notification
						($notification->read_at &&
							Carbon::parse($notification->read_at)->lte(
								$maxReadNotification
							)) ||
						// Old notification
						Carbon::parse($notification->created_at)->lte(
							$maxNotification
						);

					if ($shouldDelete) {
						$idsToDelete[] = $notification->id;
					}
				}
			});

		if (sizeof($idsToDelete)) {
			DB::table('notifications')
				->whereIn('id', $idsToDelete)
				->delete();
		}
	}
}
