<?php

namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use App\EventImage;
use App\Http\Controllers\Controller;
use App\Http\Resources\EventImage as EventImageResource;
use App\Event;

class EventImageController extends Controller {
	/**
	 * Store a newly created event image in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$messages = [
			'image' => 'An image is required',
			'max' => 'The image maximum size is 3MB',
			'dimensions' =>
				'The image must be below 4000 pixels width and height',
		];

		/*
		$validator = Validator::make(
			$request->all(),
			[
				'image' => 'required|image|max:3072|dimensions:max_width=4000,max_height=4000',
			],
			$messages
		);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}*/

		if(!$request->has('image'))
		{
			return response()->json(
				[
					'error' => "An image is required",
				],
				400
			);

		}

		$pathPartial =
			'event-images/' .
			md5(date('Y-m-d-h:i:s')) .
			'/' .
			md5(date('Y-m-d-h:i:s') .
			rand());

		// Upload the file to filesystem
		$path = $request->file('image')->store($pathPartial, 's3');
		Storage::disk('s3')->setVisibility($path, 'public');
		$imageSize = getimagesize($request->file('image'));

		// Create the event image
		$eventImage = new EventImage();
		$eventImage->path = $path;
		$eventImage->width = $imageSize[0];
		$eventImage->height = $imageSize[1];
		$eventImage->save();

		// Return the image's path and ID
		return response()->json([
			'image' => new EventImageResource($eventImage),
		]);
	}
}
