<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;


use App\Http\Resources\User as UserResource;
use App\Http\Resources\Discussion as DiscussionResource;

use App\User;
use App\Discussion;

use Log;

class UserMentionedNotification extends Notification {
	use Queueable;

	protected $mention;
	protected $sender;
	protected $mentioned;
	protected $discussion;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($mention, $discussion) {
		$this->mention = $mention;

		$this->mentioned = $this->mention->mentioned;
		$this->sender = $this->mention->sender;
		$this->discussion = $discussion;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		$result = [];
		if ($this->mentioned->is_deleted || $this->mentioned->inactive) {
			return $result;
		}
		if ($this->mentioned->allowEmailMentioned()) {
			$result[] = 'mail';
		}
		if ($this->mentioned->allowNotificationMentioned()) {
			$result[] = 'database';
		}
		/*if ($this->mentioned->allowPushNotificationMentioned()) {
			$result[] = FcmChannel::class;
		}*/

		return $result;
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "{$this->sender->first_name} mentioned you";

		return (new MailMessage())
			->subject($title)
			->view('emails.user-mentioned', [
				'user' => $this->mentioned,
				'sender' => $this->sender,
				'discussion' => $this->discussion,
				'mainDiscussion' =>
					$this->discussion->main_discussion_id !== null
						? $this->discussion->mainDiscussion
						: $this->discussion,
			]);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			'author' => new UserResource($this->sender),
			'mentioned' => new UserResource($this->mentioned),
			'discussion' => new DiscussionResource($this->discussion),
		];
	}

	public function toFcm($notifiable) {
		/*
		$title = "{$this->sender->first_name} mentioned you";
		// The FcmNotification holds the notification parameters
		$fcmNotification = FcmNotification::create()
			->setTitle($title)
			->setBody($title);

		// The FcmMessage contains other options for the notification
		return FcmMessage::create()
			->setData([
				'discussion_slug' =>
					$this->discussion->main_discussion_id !== null
						? $this->discussion->mainDiscussion->slug
						: $this->discussion->slug,
			])
			->setPriority(FcmMessage::PRIORITY_HIGH)
			->setTimeToLive(86400)
			->setNotification($fcmNotification);
		*/
	}
}
