<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

use App\Blog;
use App\Discussion;
use App\Traits\AlgoliaIndex;

class Hashtag extends Model {
	use Searchable;
	use AlgoliaIndex;

	protected $fillable = ['name'];

	protected $hidden = ['pivot', 'created_at', 'updated_at'];

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		$id = $this->name;
		$image = null;
		$title = $this->name;
		$excerpt = null;
		$data = null;

		// Result structure
		$result = [
			'type' => $type,
			'id' => $id,
			'image' => $image,
			'title' => $title,
			'excerpt' => $excerpt,
			'data' => $data,
		];

		return $result;
	}

	/**
	 * Get the value used to index the model.
	 *
	 * @return mixed
	 */
	public function getScoutKey() {
		return $this->algolia_id;
	}

	public function getAlgoliaIdAttribute() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		return "$type/{$this->name}";
	}

	/**
	 * Get the index name for the model.
	 *
	 * @return string
	 */
	public function searchableAs() {
		return $this->algoliaIndex();
	}

	public function discussions() {
		return $this->morphToMany(Discussion::class, 'hashtaggable');
	}

	public function blogs() {
		return $this->morphToMany(Blog::class, 'hashtaggable');
	}

	public static function attachHashtags(&$model) {
		$hashtagIds = [];
		foreach ($model->getHashtags() as $name) {
			$hashtag = Hashtag::firstOrCreate([
				'name' => $name,
			]);

			$hashtagIds[] = $hashtag->id;
		}

		$model->hashtags()->sync($hashtagIds);
	}
}
