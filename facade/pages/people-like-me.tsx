import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import { connect } from 'react-redux';
import Router, { withRouter, SingletonRouter } from 'next/router';

 

import { IStoreState, apiError } from '../js/redux/store';
import {
	IApiUser,
	IApiPagination,
	IApiSituation,
	IApiTopic,
} from '../js/interfaces';
import { ApiService } from '../js/services';

import {
	IFilters,
	IFiltersSelected,
	IFiltersGroup,
	IFilterValue,
	FilterResultsHelpers,
} from '../js/components/general/FilterResults';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import UserTile from '../js/components/general/UserTile';
import FilterResults from '../js/components/general/FilterResults';
import ListLoading from '../js/components/general/ListLoading';
import Footer from '../js/components/general/Footer';
import HeroHeading from '../js/components/general/HeroHeading';

import { GTMDataLayer } from '../js/helpers/dataLayer';
import isAdmin from '../js/helpers/isAdmin';

export interface IFormValues {
	cancerTypes: string[];
	activeAgeRange: string[];
	gender: string[];
}

interface IProps {
	peopleLikeMeData: {
		data: {
			users: IApiUser[];
		};
		meta: IApiPagination;
		filters: {
			cancerTypes: IFilterValue[];
		};
	};
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	situationsLoading: boolean;
	situations: IApiSituation[];
	apiService: ApiService;
	router?: SingletonRouter;
	topics: IApiTopic[];
	updateDataCount: number;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	peopleLikeMeData: IApiUser[];
	currentPage: number;
	maxPages: number;
	total: number;
	isLoadingUsers: boolean;
	filters: IFilters;
	selectedFilters: IFiltersSelected;
}

class PeopleLikeMe extends React.Component<IProps, IState> {
	protected cancerFilters: IFiltersGroup;
	protected filterHelpers: FilterResultsHelpers = new FilterResultsHelpers();
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ apiService }) {
		const peopleLikeMeData = await (apiService as ApiService).queryGET(
			'/api/users/me/similar-users',
		);

		return {
			peopleLikeMeData,
		};
	}

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			peopleLikeMeData: props.peopleLikeMeData.data.users,
			currentPage: props.peopleLikeMeData.meta.current_page,
			maxPages: props.peopleLikeMeData.meta.last_page,
			isLoadingUsers: false,
			total: props.peopleLikeMeData.meta.total,
			filters: this.generateFilters(),
			selectedFilters: {
				filters: [],
			},
		};

		this.loadUsers = this.loadUsers.bind(this);
		this.handleFiltersToggle = this.handleFiltersToggle.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
		this.handleFiltersReset = this.handleFiltersReset.bind(this);
		this.handleFiltersSubmit = this.handleFiltersSubmit.bind(this);
		this.trackFilterUpdate = this.trackFilterUpdate.bind(this);
		this.trackUserPanelOpen = this.trackUserPanelOpen.bind(this);
		this.handleUserUpdate = this.handleUserUpdate.bind(this);
		this.onUserUpdated = this.onUserUpdated.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			JSON.stringify(this.props.userData) !==
				JSON.stringify(prevProps.userData) ||
			this.props.situations !== prevProps.situations
		) {
			if (!this.props.userData) {
				Router.push('/auth/login');
			} else {
				this.setState({
					filters: this.generateFilters(),
				});
			}
		} else if (this.props.updateDataCount !== prevProps.updateDataCount) {
			this.loadUsers(false);
		}
	}

	public render() {
		const pageTitle = 'People Like Me';

		let hostname;
		if (typeof window !== 'undefined') {
			hostname = window.location.origin;
		}
		const url = buildUrl(hostname, {
			path: `/people-like-me/`,
		});

		const sharingUrl = url;

		const featuredImageUrl = `${
			process.env.STATIC_PATH
		}/images/og-facebook-default.jpg`;

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />

					<meta
						name="og:image"
						property="og:image"
						content={featuredImageUrl}
					/>
					<meta
						name="og:url"
						property="og:url"
						content={sharingUrl}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/> 
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadUsers}>
					<header className="hero theme--accent">
						<HeroHeading
							title={pageTitle}
							iconSlug="people-like-me"
						/>
					</header>

					<FilterResults
						filters={this.state.filters}
						selectedFilters={this.state.selectedFilters}
						onToggle={this.handleFiltersToggle}
						onUpdateFilters={this.handleFiltersUpdate}
						onReset={this.handleFiltersReset}
						onSubmit={this.handleFiltersSubmit}
						total={this.state.total}
						totalString={{
							noResults: 'No people',
							singular: '1 person',
							plural: 'people',
						}}
						gtmDataLayer={this.trackFilterUpdate}
					/>

					<div className="people_like_me-tiles">
						<div className="people_like_me-tiles_container">
							{this.state.peopleLikeMeData.map((data, index) => {
								return (
									<UserTile
										key={index}
										userData={data}
										gtmDataLayer={this.trackUserPanelOpen}
									/>
								);
							})}
						</div>
					</div>
					<ListLoading active={this.state.isLoadingUsers} />

					<Footer />
				</LayoutDashboard>
			</div>
		);
	}

	protected handleFiltersUpdate(filters: IFilters) {
		this.setState({
			filters,
		});
	}

	protected handleFiltersReset() {
		this.setState({
			selectedFilters: {
				filters: [],
			},
			filters: this.generateFilters(),
		});
	}

	protected onUserUpdated() {
		console.log('user updated.');
	}

	protected handleUserUpdate() {
		console.log('refresh users list');
	}

	protected loadUsers(append: boolean = true) {
		if (
			!this.state.isLoadingUsers &&
			!this.state.isUpdating &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			const params: any = this.processQueryParams(
				this.state.selectedFilters,
			);

			if (append) {
				params.page = this.state.currentPage + 1;
			}

			const url = buildUrl(null, {
				path: `/api/users/me/similar-users`,
				queryParams: params,
			});

			this.setState(
				{
					isUpdating: !append,
					isLoadingUsers: append,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							let users: IApiUser[] = response.data.users;

							if (append) {
								users = JSON.parse(
									JSON.stringify(this.state.peopleLikeMeData),
								);
								(response.data.users as IApiUser[]).forEach(
									user => {
										users.push(user);
									},
								);
							}

							this.setState({
								isUpdating: false,
								isLoadingUsers: false,
								peopleLikeMeData: users,
								currentPage: response.meta.current_page,
								maxPages: response.meta.last_page,
								total: response.meta.total,
							});
						})
						.catch(error => {
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isUpdating: false,
								isLoadingUsers: false,
							});
						});
				},
			);
		}
	}

	protected handleFiltersSubmit(selectedFilters: IFiltersSelected) {
		this.setState(
			{
				selectedFilters,
				filters: this.filterHelpers.updateFiltersBasedOnSelectedFilters(
					selectedFilters,
					this.state.filters,
				),
			},
			() => {
				this.loadUsers(false);
			},
		);
	}

	protected handleFiltersToggle() {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.state.filters),
		);

		filters.filterToggle = !filters.filterToggle;

		if (filters.filterToggle) {
			if (this.props.userAuthenticated) {
				const cancerTypesIndex = filters.filterGroups.findIndex(
					value => {
						return value.paramName === 'situations';
					},
				);
				if (cancerTypesIndex >= 0) {
					filters.filterGroups.splice(cancerTypesIndex, 1);
				}

				// Remove the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					filters.filterGroups[
						topicsIndex
					].filters = filters.filterGroups[
						topicsIndex
					].filters.filter(filterValue => {
						const result = this.props.topics.find(topic => {
							return (
								topic.category === 'blogs' &&
								topic.slug === filterValue.value &&
								(topic.is_user_topic ||
									isAdmin(this.props.userData))
							);
						});
						return !!result;
					});
				}
			}
		} else {
			if (this.props.userAuthenticated) {
				// Add the situations at the beginning
				const situationsGroup: IFiltersGroup = {
					label: null,
					noBorder: true,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: !!this.props.userData.situations.find(
									userSituation => {
										return (
											userSituation.name ===
											situation.name
										);
									},
								),
							};
						}),
				};
				filters.filterGroups.unshift(situationsGroup);

				// add the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					this.props.topics.forEach(topic => {
						if (
							!topic.is_user_topic &&
							!isAdmin(this.props.userData) &&
							topic.category === 'blogs'
						) {
							filters.filterGroups[topicsIndex].filters.push({
								label: topic.title,
								value: topic.slug,
								active: false,
							});
						}
					});
				}
			}
		}

		this.setState({
			filters,
		});
	}

	protected generateFilters(): IFilters {
		const filters: IFilters = {
			filtersTitle: 'Filter',
			withToggle: false,
			filterToggleLabel: '',
			filterToggle: true,
			filterGroups: [],
		};

		if (!this.props.situationsLoading) {
			filters.filterGroups.push({
				label:
					'Only show people who share the following cancer experience',
				noBorder: false,
				paramName: 'situations',
				filters: this.props.situations
					.filter(situation => {
						return situation.parent_situation === null;
					})
					.map(situation => {
						return {
							label: situation.name,
							value: situation.name,
							active: !!this.props.userData.situations.find(
								userSituation => {
									return (
										userSituation.name === situation.name
									);
								},
							),
						};
					}),
			});

			filters.filterGroups.push({
				label:
					'Only show people impacted by these cancer types (you can set your default cancer type in your profile settings)',
				noBorder: false,
				paramName: 'cancerTypes',
				filters: (JSON.parse(
					JSON.stringify(
						this.props.peopleLikeMeData.filters.cancerTypes,
					),
				) as IFilterValue[]).map(cancerType => {
					return {
						label: cancerType.label,
						value: cancerType.value,
						active: !!this.props.userData.situations.find(
							userSituation => {
								return (
									userSituation.cancer_type ===
									cancerType.value
								);
							},
						),
					};
				}),
			});

			// Add the age filter
			filters.filterGroups.push({
				label: 'Show people in the following age range',
				paramName: 'age',
				filters: [
					{
						label: '12 - 14',
						value: '12-14',
						active: false,
					},
					{
						label: '15 - 17',
						value: '15-17',
						active: false,
					},
					{
						label: '18 - 20',
						value: '18-20',
						active: false,
					},
					{
						label: '21 - 25',
						value: '21-25',
						active: false,
					},
				],
			});
			// Add the gender filter
			filters.filterGroups.push({
				label: 'Show people who identify as',
				paramName: 'gender',
				filters: [
					{
						label: 'Female',
						value: 'female',
						active: false,
					},
					{
						label: 'Female Trans',
						value: 'female-trans',
						active: false,
					},
					{
						label: 'Male',
						value: 'male',
						active: false,
					},
					{
						label: 'Male Trans',
						value: 'male-trans',
						active: false,
					},
					{
						label: 'Other',
						value: 'other',
						active: false,
					},
				],
			});
			// Add the location filter
			filters.filterGroups.push({
				label: 'Show people in the following location',
				paramName: 'location',
				singleValue: true,
				filters: [
					{
						label: 'All of Australia',
						value: 'australia',
						active: false,
					},
					{
						label: 'Within my State (AU)',
						value: 'state',
						active: false,
					},
					{
						label: 'Within a 5km Radius',
						value: '5radius',
						active: false,
					},
					{
						label: 'Within a 20km Radius',
						value: '20radius',
						active: false,
					},
					{
						label: 'All of New Zealand',
						value: 'new-zealand',
						active: false,
					},
				],
			});
		}

		return filters;
	}

	protected processQueryParams(selectedFilters: IFiltersSelected): any {
		const params: any = {};

		selectedFilters.filters.forEach(singleFilter => {
			if (singleFilter.filterValues instanceof Array) {
				singleFilter.filterValues.forEach((filterValue, index) => {
					params[`${singleFilter.paramName}[${index}]`] =
						filterValue.value;
				});
			} else {
				params[singleFilter.paramName] =
					singleFilter.filterValues.value;
			}
		});

		return params;
	}

	protected trackFilterUpdate(value: string) {
		this.GTM.pushEventToDataLayer({
			event: 'filterPeople',
			filter: value,
		});
	}

	protected trackUserPanelOpen() {
		this.GTM.pushEventToDataLayer({
			event: 'viewProfile',
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { situations } = state.situations;
	const { topics } = state.topics;
	const { updateDataCount } = state.updateData;
	return {
		userAuthenticated,
		userData,
		situations,
		situationsLoading: state.situations.loading,
		topics,
		updateDataCount,
	};
}

export default connect(mapStateToProps)(withRouter(PeopleLikeMe as any));
