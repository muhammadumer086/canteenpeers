import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';

import { IStoreState } from '../../redux/store';

import { throttle } from '../../helpers/throttle';

import DashboardHeader from './DashboardHeader';
import DashboardSidebar from './DashboardSidebar';
import MenuPanel from './MenuPanel';

import { isIOS, isMobile } from 'react-device-detect';

interface IProps {
	userAuthenticated: boolean | null;
	children: any;
	scrollTopReached?: () => void;
	scrollBottomReached?: () => void;
}

const LayoutDashboard: React.FC<IProps> = (props: IProps) => {
	const scrollThreshold = !!(process as any).browser
		? window.innerHeight / 2
		: 500;
	useEffect(()=>{
		const myDiv=document.getElementById('scroll_container');
		myDiv.addEventListener('scroll',handleScroll,true);
		return ()=>{
			myDiv.removeEventListener('scroll',handleScroll);
		}
	},[1]);
	
	const throttleScroll: any = throttle((target: Element) => {
		const topDistance = target.scrollTop;
		const bottomDistance =
			target.scrollHeight - (target.clientHeight + target.scrollTop);

		if (props.scrollTopReached && topDistance <= scrollThreshold) {
			props.scrollTopReached();
		}

		if (props.scrollBottomReached && bottomDistance <= scrollThreshold) {
			props.scrollBottomReached();
		}
	}, 200);

	function handleScroll(e) {
		throttleScroll(e.target);
	}

	function getWindowHeight() {
		return window.innerHeight;
	}

	function handleIosMobile() {
		const windowHeight = getWindowHeight();

		return windowHeight;
	}

	let isIOSStyle = {
		height: '100vh',
	};

	if (isIOS && isMobile) {
		const windowHeight = handleIosMobile();

		isIOSStyle = {
			height: `${windowHeight}px`,
		};
	}
	return (
		<section className="layout_dashboard theme--main" style={isIOSStyle}>
			<header className="layout_dashboard-header">
				<DashboardHeader />
			</header>
			<aside className="layout_dashboard-sidebar">
				<DashboardSidebar />
			</aside>
			<div id="scroll_container" className="layout_dashboard-content" >
				{props.userAuthenticated === null && (
					<div className="layout_dashboard-content_container layout_dashboard-content_container--loading">
						<div>
							<CircularProgress />
						</div>
					</div>
				)}
				{props.userAuthenticated !== null && (
					<div className="layout_dashboard-content_container">
						{props.children}
					</div>
				)}
			</div>
			<MenuPanel />
		</section>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(LayoutDashboard);
