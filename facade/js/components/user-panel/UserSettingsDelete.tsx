import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { connect } from 'react-redux';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import {
	IApiUser,
	IApiLanguage,
	IApiCountry,
	ISelectOption,
	IApiHeardAbout,
} from '../../interfaces';
import { ETabs, ESettingsScreens, EAccountScreens } from './UserPanelAccount';
import { TextFieldFormik, CheckboxGroupFormik } from '../inputs';

import { IStoreState } from '../../../js/redux/store';

interface IProps {
	userData: IApiUser;
	currentUser: IApiUser;
	submitting: boolean;
	countries: IApiCountry[];
	languages: IApiLanguage[];
	heardAbouts: IApiHeardAbout[];
	changeScreen(tab: ETabs, screen: EAccountScreens | ESettingsScreens);
	onSubmit(values: any, resetForm?: () => void);
}

const UserSettingsDelete: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		leaving_reasons: Yup.array()
			.of(Yup.string().max(191))
			.nullable(true),
		feedbacks: Yup.string()
			.max(2500)
			.nullable(true),
	});

	const reasons = [
		'I’m getting too many emails',
		'I’m not getting value from the community',
		'I have a privacy concern',
		'I’m receiving unwanted contact',
		'Other',
	];

	const leavingReasons: ISelectOption[] = reasons.map(
		(singleReason): ISelectOption => {
			return {
				value: singleReason,
				label: singleReason,
			};
		},
	);

	const initialValues = {
		leaving_reasons: [],
		feedbacks: '',
	};

	return (
		<div className="user_account_panel user_account_panel--account_delete">
			<div className="user_panel-back">
				<ButtonBase
					focusRipple={true}
					className="user_panel-back-button"
					onClick={() => {
						props.changeScreen(
							ETabs.SETTINGS,
							ESettingsScreens.OVERVIEW,
						);
					}}
					disabled={props.submitting}
				>
					<Icon className="user_panel-back-back_icon">
						arrow_back
					</Icon>
					Settings
				</ButtonBase>
			</div>
			<div className="user_panel-heading">
				<h4 className="font--h4 theme-title">
					{props.userData.first_name}, we're sorry to see you go.
				</h4>
			</div>
			<Formik
				initialValues={initialValues}
				validationSchema={validationFormat}
				onSubmit={values => {
					props.onSubmit(values);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>
							<div className="form-row">
								<div className="form-column">
									<h5 className="font--h5 theme-title hm-b16">
										Before you go ...
									</h5>

									<p className="form-text font--body theme-title">
										If you’re sick of getting email
										notifications from us, you can&nbsp;
										<span
											className="user_panel-link"
											tabIndex={1}
											onClick={() => {
												props.changeScreen(
													ETabs.SETTINGS,
													ESettingsScreens.OVERVIEW,
												);
											}}
										>
											disable them here
										</span>
										.
									</p>

									<p className="form-text font--body theme-title">
										If you want to change your
										username,&nbsp;
										<span
											className="user_panel-link"
											tabIndex={1}
											onClick={() => {
												props.changeScreen(
													ETabs.MY_PROFILE,
													EAccountScreens.PUBLIC_DETAILS,
												);
											}}
										>
											you&nbsp;can&nbsp;here
										</span>
										.
									</p>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Can you let us know why you’re leaving?"
										name="leaving_reasons"
										options={leavingReasons}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Your feedback matters, what else should we know?"
										name="feedbacks"
										multiline={true}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit_left">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit form-submit--normal"
										type="button"
										disabled={props.submitting}
										onClick={() => {
											props.changeScreen(
												ETabs.SETTINGS,
												ESettingsScreens.OVERVIEW,
											);
										}}
									>
										Cancel
									</Button>
								</div>
							</div>
							<div className="form-row form-row--submit_left">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit form-submit--error"
										type="submit"
										disabled={props.submitting}
									>
										Delete Account
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
};

const mapStateToProps = (state: IStoreState) => {
	const currentUser = state.user.userData;

	return {
		currentUser,
	};
};

export default connect(mapStateToProps)(UserSettingsDelete);
