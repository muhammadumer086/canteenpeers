import React from 'react';
import { connect } from 'react-redux';

import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import ClearIcon from '@material-ui/icons/Clear';

import { ApiService, withServices } from '../../services';
import { IApiUser } from '../../interfaces';

import {
	IStoreState,
	leaveConversationModalHide,
	apiError,
	apiSuccess,
} from '../../redux/store';

interface IProps {
	userData: IApiUser | null;
	leaveConversationModalDisplay: boolean;
	conversationId: number;
	apiService: ApiService;
	onLeaveConversation(): void;
	dispatch(action: any): void;
}

interface IState {
	isSubmitting: boolean;
}

class LeaveConversationConfirmModal extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			isSubmitting: false,
		};

		this.handleClose = this.handleClose.bind(this);
		this.leaveConversation = this.leaveConversation.bind(this);
	}

	render() {
		return (
			<Modal
				aria-labelledby="contact-modal"
				open={this.props.leaveConversationModalDisplay}
				onClose={this.handleClose}
			>
				<Paper className="modal theme--main" square={true}>
					<button
						onClick={this.handleClose}
						className="close_button close_button--primary"
						title="Dismiss"
					>
						<ClearIcon className="close_button-icon" />
					</button>
					<div className="modal-content_container">
						<div className="modal-content leave_conversation_modal">
							<h2 className="modal-content_title font--h4 theme-title hm-t16">
								Are you sure you want to leave this
								conversation?
							</h2>

							<div className="leave_conversation_modal form">
								<div className="form-row">
									<div className="form-column">
										<Button
											variant="contained"
											color="primary"
											className="form-submit"
											type="button"
											disabled={false}
											onClick={this.handleClose}
										>
											Cancel
										</Button>

										<Button
											variant="outlined"
											color="primary"
											className="form-submit form-submit--error"
											type="button"
											onClick={this.leaveConversation}
											disabled={false}
										>
											Leave
										</Button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Paper>
			</Modal>
		);
	}

	protected handleClose() {
		this.props.dispatch(leaveConversationModalHide());
	}

	protected leaveConversation() {
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryDELETE(
						`/api/conversations/${this.props.conversationId}/leave`,
					)
					.then(() => {
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									leaveConversationModalHide(),
								);
								this.props.dispatch(
									apiSuccess([
										'You have successfully left the conversation',
									]),
								);

								// Router.push('/messages');
								this.props.onLeaveConversation();
							},
						);
					})
					.catch(error => {
						this.setState({
							isSubmitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const {
		leaveConversationModalDisplay,
		conversationId,
	} = state.leaveConversationModal;

	return {
		leaveConversationModalDisplay,
		conversationId,
	};
}

export default connect(mapStateToProps)(
	withServices(LeaveConversationConfirmModal),
);
