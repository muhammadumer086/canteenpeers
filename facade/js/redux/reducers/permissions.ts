import { actionTypes } from '../actions';
import { IApiPermission } from '../../interfaces';

export interface IStoreState {
    isLoaded : boolean;
	loading: boolean;
	permissions: IApiPermission[];
}

export const permission = (state: IStoreState = {
    isLoaded : false,
	loading: false,
	permissions: [],
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.PERMISSIONS_LOAD:
			return Object.assign({}, state, {
				loading: true,
			});
		case actionTypes.PERMISSIONS_LOADED:
			return Object.assign({}, state, {
                isLoaded:true,
				permissions: action.permissions,
				loading: false,
			});
		case actionTypes.PERMISSIONS_LOAD_ERROR:
			return Object.assign({}, state, {
				loading: false,
			});
		default:
			return state;
	}
}
