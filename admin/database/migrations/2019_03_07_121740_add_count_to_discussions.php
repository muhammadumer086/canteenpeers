<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountToDiscussions extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->unsignedInteger('count_likes')->default(0);
			$table->unsignedInteger('count_replies')->default(0);
			$table->unsignedInteger('count_direct_replies')->default(0);
			$table->unsignedInteger('count_thread')->default(0);
			$table->unsignedInteger('count_report')->default(0);
			$table->unsignedInteger('count_view')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->dropColumn('count_likes');
			$table->dropColumn('count_replies');
			$table->dropColumn('count_direct_replies');
			$table->dropColumn('count_thread');
			$table->dropColumn('count_report');
			$table->dropColumn('count_view');
		});
	}
}
