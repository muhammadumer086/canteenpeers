<?php

namespace App\Http\Resources;
use App\Situation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection {
	use \App\Traits\AuthUser;

	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return ['users' => $this->collection];
	}

	/**
	 * Get additional data that should be returned with the resource array.
	 *
	 * @param \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function with($request) {
		if (Route::currentRouteName() === 'similarUsers') {
			$authUser = $this->getAuthUser($request);

			if ($authUser) {
				$cancerTypes = [];

				$cancerTypesList = Situation::whereNotNull('cancer_type')->groupBy('cancer_type')->pluck('cancer_type');
				foreach ($cancerTypesList as $cancerName) {

					$cancerTypes[] = [
						'label' => $cancerName,
						'value' => $cancerName,
						'active' => false,
					];
				}
			}

			return [
				'filters' => [
					'cancerTypes' => $cancerTypes
				],
			];
		}
		return [];
	}
}
