import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';

import { Button } from '@material-ui/core';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';

import { IAdminProps } from '../../../components/admin/withAdminPage';
import HeroHeading from '../../../components/general/HeroHeading';
import EditEventFormik from '../events/EditEventFormik';
import CreateEventFormik from '../events/CreateEventFormik';
import { eventTypes } from '../events/CreateEventFormik';
import { IStoreState, apiError, apiSuccess } from '../../../redux/store';
import {
	IApiEvent,
	IStaticStates,
	IApiSituation,
	IApiStaff,
	IApiTopic,
	IApiUser,
	FilterOrder,
} from '../../../../js/interfaces';
import { connect } from 'react-redux';

// import {
// 	IFilters, IFiltersSelected,
// 	IFiltersGroup,
// 	FilterResultsHelpers,
// } from '../../general/FilterResults';
// import FilterResults from '../../general/FilterResults';
import {
	IFilters,
	IFiltersSelected,
	IFiltersGroup,
	FilterResultsHelpers,
} from '../../general/FilterResults';
import FilterResults from '../../general/FilterResults';
import { ModalDialog } from '../../modals/local/ModalDialog';
import ModalDialogDeleteConfirm from '../../modals/ModalDialogDeleteConfirm';

import { generateStatesEvents } from '../../../../js/helpers/generateStaticUserData';
import EventTile from '../../events/EventTile';
import { CircularProgress } from '@material-ui/core';
import moment from 'moment';
import 'moment-timezone';
import { GTMDataLayer } from '../../../helpers/dataLayer';
import isAdmin from '../../../helpers/isAdmin';

import { checkPermission } from '../../../helpers/checkPermission';
export enum EEventManagementPermissions {
	READ = 'events_management_read',
	ADD = 'events_management_add',
	EDIT = 'events_management_edit',
	DELETE = 'events_management_delete',
	ALL = 'events_management_all',
}

const ausZoneArr = moment.tz.zonesForCountry('AU');
const nzZoneArr = moment.tz.zonesForCountry('NZ');
interface IProps extends IAdminProps {
	topicsLoading: boolean;
	topics: IApiTopic[];
	situationsLoading: boolean;
	situations: IApiSituation[];
	updateDataCount: number;
	userData: IApiUser;
}

interface IState {
	data: IApiEvent[];
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	isLoading: boolean;
	isCreateEventModalActive: boolean;
	isEditEventModalActive: boolean;
	eventData: IApiEvent | null;
	eventToDelete: IApiEvent | null;
	isDeleteEventModalActive: boolean;
	situations: IApiSituation[];
	states: IStaticStates[] | null;
	staffData: IApiStaff[];
	filters: IFilters;
	selectedFilters: IFiltersSelected;
	order: FilterOrder;
}

class EventsManagement extends React.Component<IProps, IState> {
	protected filterHelpers: FilterResultsHelpers = new FilterResultsHelpers();
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			data: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isLoading: false,
			isCreateEventModalActive: false,
			isEditEventModalActive: false,
			isDeleteEventModalActive: false,
			eventData: null,
			situations: null,
			eventToDelete: null,
			states: generateStatesEvents(),
			staffData: [],
			filters: this.generateFilters(),
			selectedFilters: {
				filters: [],
			},
			order: 'DESC',
		};

		this.handleCreateEventModal = this.handleCreateEventModal.bind(this);
		this.handleCreateNewEvent = this.handleCreateNewEvent.bind(this);

		this.handleEditEventModal = this.handleEditEventModal.bind(this);
		this.setEventToEdit = this.setEventToEdit.bind(this);
		this.handleEditEvent = this.handleEditEvent.bind(this);

		this.setEventToDelete = this.setEventToDelete.bind(this);
		this.submitDeleteEvent = this.submitDeleteEvent.bind(this);
		this.toggleDeleteModal = this.toggleDeleteModal.bind(this);

		this.loadEvents = this.loadEvents.bind(this);
		this.loadStaffData = this.loadStaffData.bind(this);
		this.handleFiltersToggle = this.handleFiltersToggle.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
		this.handleFiltersReset = this.handleFiltersReset.bind(this);
		this.handleFiltersSubmit = this.handleFiltersSubmit.bind(this);
		this.trackFilterUpdate = this.trackFilterUpdate.bind(this);
		this.loadMyEvents = this.loadMyEvents.bind(this);
		this.handleOrderChange = this.handleOrderChange.bind(this);
		this.buildQueryParams = this.buildQueryParams.bind(this);
	}

	public componentDidMount() {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EEventManagementPermissions.ALL,
				EEventManagementPermissions.READ,
			);
		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.userAdmin) {
			this.initialise(this.props, prevProps);
		}
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.situations !== prevProps.situations ||
			this.props.topics !== prevProps.topics ||
			this.props.updateDataCount !== prevProps.updateDataCount
		) {
			this.setState(
				{
					filters: this.generateFilters(),
					selectedFilters: {
						filters: [],
					},
				},
				() => {
					this.loadEvents(false);
				},
			);
		}
	}

	public render() {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EEventManagementPermissions.ALL,
				EEventManagementPermissions.READ,
			);
		const onLoadMore =
			this.state.currentPage < this.state.maxPages && readPermission
				? this.loadEvents
				: undefined;

		let heroInfo = `Showing ${this.state.data.length} of ${this.state.total} event`;
		if (this.state.isLoading && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}
		const addPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EEventManagementPermissions.ALL,
				EEventManagementPermissions.ADD,
			);
		const editPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EEventManagementPermissions.ALL,
				EEventManagementPermissions.EDIT,
			);

		return (
			<div>
				<Head>
					<title>Events Management — {process.env.APP_NAME}</title>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading
							title="Event Management"
							subInfo={heroInfo}
						/>
						{addPermission && (
							<span className="hero-actions">
								<Button
									variant="outlined"
									color="primary"
									onClick={this.handleCreateEventModal}
								>
									Add Event
								</Button>
							</span>
						)}
					</header>
					<div className="admin_content admin_content-events">
						{/* <FilterResults
							filters={this.state.filters}
							selectedFilters={this.state.selectedFilters}
							onToggle={this.handleFiltersToggle}
							onUpdateFilters={this.handleFiltersUpdate}
							onReset={this.handleFiltersReset}
							onSubmit={this.handleFiltersSubmit}
							total={this.state.total}
							totalString={{
								noResults: 'No events',
								singular: '1 event',
								plural: 'events',
							}}
							gtmDataLayer={this.trackFilterUpdate}
							order={this.state.order}
							onOrderChange={this.handleOrderChange}
						/> */}
						<FilterResults
							filters={this.state.filters}
							selectedFilters={this.state.selectedFilters}
							onToggle={this.handleFiltersToggle}
							onUpdateFilters={this.handleFiltersUpdate}
							onReset={this.handleFiltersReset}
							onSubmit={this.handleFiltersSubmit}
							total={this.state.total}
							totalString={{
								noResults: 'No events',
								singular: '1 event',
								plural: 'events',
							}}
							gtmDataLayer={this.trackFilterUpdate}
							// loadMyEvents={this.loadMyEvents}
							// myFilterOptions={allFilterOptions}
							order={this.state.order}
							onOrderChange={this.handleOrderChange}
						/>
						<div className="events-tiles">
							{this.state.data.map((event, ind) => {
								return (
									<div
										className="events-tiles-item-admin"
										key={event.id}
									>
										<EventTile
											index={ind}
											event={event}
											onRowClick={
												editPermission
													? this.setEventToEdit
													: undefined
											}
											ausZoneArr={ausZoneArr}
											nzZoneArr={nzZoneArr}
										/>
									</div>
								);
							})}
						</div>
						{onLoadMore && (
							<div className="admin_loadMore-button-continer">
								<Button
									className="admin_table-footer-load_more"
									onClick={() => onLoadMore()}
									disabled={this.state.isLoading}
									variant="contained"
								>
									{this.state.isLoading ? (
										<CircularProgress
											color="secondary"
											size={15}
											className="admin_table-footer-loader"
										/>
									) : (
										'Load More Results'
									)}
								</Button>
							</div>
						)}
					</div>

					{this.state.isCreateEventModalActive && (
						<ModalDialog
							isActive={this.state.isCreateEventModalActive}
							modalTitle="Create Event"
							ariaTag="create-event-modal"
							handleClose={this.handleCreateEventModal}
						>
							<CreateEventFormik
								isSubmitting={this.state.isLoading}
								onSubmit={this.handleCreateNewEvent}
								userSituations={this.state.situations}
								states={this.state.states}
								staffs={this.state.staffData}
								topics={this.props.topics}
							/>
						</ModalDialog>
					)}

					{this.state.isEditEventModalActive && (
						<ModalDialog
							isActive={this.state.isEditEventModalActive}
							modalTitle="Edit Event"
							ariaTag="edit-event-modal"
							handleClose={this.handleEditEventModal}
						>
							<EditEventFormik
								isSubmitting={this.state.isLoading}
								eventData={this.state.eventData}
								states={this.state.states}
								situations={this.state.situations}
								onSubmit={(values: any) =>
									this.handleEditEvent(values, false)
								}
								onDuplicate={(values: any) =>
									this.handleEditEvent(values, true)
								}
								staffs={this.state.staffData}
								onDelete={this.setEventToDelete}

								//topics={this.props.topics}
							/>
						</ModalDialog>
					)}

					{this.state.isDeleteEventModalActive && (
						<ModalDialog
							isActive={this.state.isDeleteEventModalActive}
							modalTitle="Are you sure you want to delete this event?"
							ariaTag="event-delete-modal"
							handleClose={this.toggleDeleteModal}
						>
							<ModalDialogDeleteConfirm
								name={`${this.state.eventToDelete.title}`}
								loading={this.state.isLoading}
								submit={this.submitDeleteEvent}
								cancel={this.toggleDeleteModal}
							/>
						</ModalDialog>
					)}
				</LayoutDashboard>
			</div>
		);
	}

	protected handleCreateEventModal() {
		this.setState({
			isCreateEventModalActive: !this.state.isCreateEventModalActive,
		});
	}

	protected handleEditEventModal() {
		this.setState({
			isEditEventModalActive: !this.state.isEditEventModalActive,
		});
	}

	protected setEventToEdit(eventData: IApiEvent) {
		this.setState(
			{
				eventData,
			},
			() => {
				this.handleEditEventModal();
			},
		);
	}
	protected buildQueryParams(withPage: boolean = false) {
		const params: any = {};

		this.state.selectedFilters.filters.forEach(singleFilter => {
			if (singleFilter.filterValues instanceof Array) {
				singleFilter.filterValues.forEach((filterValue, index) => {
					// params[`${singleFilter.paramName}[${index}]`] =
					//  filterValue.value;
					params[`${singleFilter.paramName}[${index}]`] =
						filterValue.value;
				});
			} else {
				if (
					singleFilter.paramName === 'past_or_upcoming' &&
					singleFilter.filterValues.value === 'admin_all_events'
				) {
					params['include_past'] = true;
				} else {
					params[`${singleFilter.paramName}`] =
						singleFilter.filterValues.value;
				}
			}
		});
		if (this.state.selectedFilters.filters.length === 0) {
			params['past_or_upcoming'] = 'admin_upcoming_events';
		}
		if (withPage) {
			params.page = this.state.currentPage + 1;
		}

		if (!this.state.filters.filterToggle) {
			params.anySituation = true;
		}
		if (this.state.order) {
			params.order = this.state.order;
		}

		return buildUrl(null, {
			path: '/api/events',
			queryParams: params,
		});
	}
	protected loadEvents(append: boolean = true) {
		if (!this.state.isLoading) {
			const url = this.buildQueryParams(append);
			this.setState(
				{
					isLoading: true,
					isCreateEventModalActive: false,
					isEditEventModalActive: false,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'events' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								const data: IApiEvent[] = append
									? JSON.parse(
											JSON.stringify(this.state.data),
									  )
									: [];

								data.push(...response.data.events);
								this.setState({
									data,
									isLoading: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
								});
							}
						})
						.catch(error => {
							this.setState({
								isLoading: false,
							});

							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		}
	}

	protected loadSituations() {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryGET('/api/situations', 'situations')
					.then((situationsList: IApiSituation[]) => {
						const userSituations: IApiSituation[] = [];
						if (situationsList instanceof Array) {
							situationsList.map((situation: IApiSituation) => {
								if (!situation.cancer_type) {
									userSituations.push(situation);
								}
							});
						}

						this.setState({
							situations: userSituations,
							isLoading: false,
						});
					})
					.catch(error => {
						this.props.dispatch(apiError(error));

						this.setState({
							situations: null,
							isLoading: false,
						});
					});
			},
		);
	}

	protected handleEditEvent(values, isDuplicate: boolean = false) {
		if (
			values.location_data &&
			'geometry' in values.location_data &&
			values.location_data.geometry &&
			'location' in values.location_data.geometry &&
			values.location_data.geometry.location &&
			'lat' in values.location_data.geometry.location &&
			values.location_data.geometry.location.lat !== null &&
			'lng' in values.location_data.geometry.location &&
			values.location_data.geometry.location.lng !== null
		) {
			values[
				'longitude'
			] = values.location_data.geometry.location.lng.toString();
			values[
				'latitude'
			] = values.location_data.geometry.location.lat.toString();
		}

		delete values.location_data;

		const queryUrl = isDuplicate
			? `/api/events/${this.state.eventData.slug}/duplicate`
			: `/api/events/${this.state.eventData.slug}`;

		this.setState(
			{
				isLoading: true,
			},
			() => {
				// Get lat/lng out of the values
				this.props.apiService
					.queryPUT(queryUrl, values, 'event')
					.then(newEvent => {
						this.setState({
							isLoading: false,
						});
						const message: string = isDuplicate
							? `${newEvent.title} was duplicated successfully`
							: `${newEvent.title} was edited successfully`;
						this.props.dispatch(apiSuccess([message]));

						this.loadEvents(false);
					})
					.catch(error => {
						this.setState({
							isLoading: false,
						});

						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected handleCreateNewEvent(values) {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/events', values, 'event')
					.then(newEvent => {
						this.setState({
							isLoading: false,
						});

						this.props.dispatch(
							apiSuccess([
								`${newEvent.title} was created successfully`,
							]),
						);

						this.loadEvents(false);
					})
					.catch(error => {
						this.setState({
							isLoading: false,
						});

						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected setEventToDelete(eventToDelete: IApiEvent | null = null) {
		this.setState(
			{
				eventToDelete,
			},
			() => {
				this.toggleDeleteModal();
			},
		);
	}

	protected toggleDeleteModal() {
		this.setState({
			isDeleteEventModalActive: !this.state.isDeleteEventModalActive,
		});
	}

	protected submitDeleteEvent() {
		if (this.state.eventToDelete !== null) {
			this.setState(
				{
					isLoading: true,
				},
				async () => {
					const { success } = await this.props.apiService.queryDELETE(
						`/api/events/${this.state.eventToDelete.slug}`,
					);

					if (success) {
						this.setState(
							{
								eventToDelete: null,
								isDeleteEventModalActive: false,
								isLoading: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess(['Event successfully delete']),
								);

								this.loadEvents(false);
							},
						);
					} else {
						this.setState(
							{
								eventToDelete: null,
								isDeleteEventModalActive: false,
								isLoading: false,
							},
							() => {
								this.props.dispatch(
									apiError([
										'There was an error when trying to delete the event',
									]),
								);
							},
						);
					}
				},
			);
		}
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				props.permissions,
				EEventManagementPermissions.ALL,
				EEventManagementPermissions.READ,
			);
		const permissions = prevProps ? prevProps.permissions : [];
		const prevReadPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				permissions,
				EEventManagementPermissions.ALL,
				EEventManagementPermissions.READ,
			);
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin) ||
				(!prevReadPermission && readPermission)
		) {
			this.loadEvents(false);
			this.loadSituations();
			this.loadStaffData();
		}
	}

	protected loadStaffData() {
		if (!this.state.isLoading) {
			this.setState(
				{
					isLoading: true,
				},
				async () => {
					let params: any = {
						page_size: 500,
						all: 1,
					};

					const url = buildUrl(null, {
						path: '/api/staff',
						queryParams: params,
					});

					const {
						data,
						error,
					} = await this.props.apiService.queryGET(url);
					if (error) {
						this.props.dispatch(apiError([error]));

						this.setState({
							isLoading: false,
						});
					}

					if (data && 'staffs' in data) {
						this.setState({
							isLoading: false,
							staffData: data.staffs,
						});
					}
				},
			);
		}
	}

	protected generateFilters(): IFilters {
		const filters: IFilters = {
			filtersTitle: 'Filter',
			withToggle: this.props.userAuthenticated,
			filterToggleLabel: 'Filter by event situation',
			filterToggle: false,
			filterGroups: [],
		};

		if (!this.props.situationsLoading && !this.props.topicsLoading) {
			// Add the location filter
			filters.filterGroups.push({
				label: 'In the following locations',
				paramName: 'state',
				singleValue: false,
				filters: [
					{
						label: 'All of Australia',
						value: 'all-of-australia',
						active: false,
					},
					{
						label: 'NSW',
						value: 'nsw',
						active: false,
					},
					{
						label: 'VIC',
						value: 'vic',
						active: false,
					},
					{
						label: 'QLD',
						value: 'qld',
						active: false,
					},
					{
						label: 'North QLD',
						value: 'qld-north',
						active: false,
					},
					{
						label: 'ACT',
						value: 'act',
						active: false,
					},
					{
						label: 'TAS',
						value: 'tas',
						active: false,
					},
					{
						label: 'SA',
						value: 'sa',
						active: false,
					},
					{
						label: 'NT',
						value: 'nt',
						active: false,
					},
					{
						label: 'WA',
						value: 'wa',
						active: false,
					},
					{
						label: 'New Zealand',
						value: 'nz',
						active: false,
					},
					{
						label: 'Online',
						value: 'online',
						active: false,
					},
				],
			});

			// Add the duration filter
			filters.filterGroups.push({
				label: 'Duration',
				paramName: 'duration',
				singleValue: false,
				filters: [
					{
						label: 'Overnight',
						value: 'overnight',
						active: false,
					},
					{
						label: 'Weekend',
						value: 'weekend',
						active: false,
					},
				],
			});

			// Add the time filter
			filters.filterGroups.push({
				label: 'By time',
				paramName: 'time',
				singleValue: false,
				filters: [
					{
						label: 'Daytime',
						value: 'daytime',
						active: false,
					},
					{
						label: 'Evening',
						value: 'evening',
						active: false,
					},
				],
			});
			filters.filterGroups.push({
				label: 'By type',
				paramName: 'event_type',
				singleValue: false,
				filters: eventTypes.map(singleType => {
					return { ...singleType, active: false };
				}),
			});
			filters.filterGroups.push({
				label: 'Past or upcoming',
				paramName: 'past_or_upcoming',
				singleValue: true,
				filters: [
					{
						label: 'All Events',
						value: 'admin_all_events',
						active: false,
					},
					{
						label: 'Upcoming Events',
						value: 'admin_upcoming_events',
						active: true,
					},
					{
						label: 'Past Events',
						value: 'admin_past_events',
						active: false,
					},
				],
			});
		}

		return filters;
	}
	protected trackFilterUpdate(value: string) {
		this.GTM.pushEventToDataLayer({
			event: 'filterEvents',
			filter: value,
		});
	}
	protected handleFiltersSubmit(selectedFilters: IFiltersSelected) {
		this.setState(
			{
				selectedFilters,
				filters: this.filterHelpers.updateFiltersBasedOnSelectedFilters(
					selectedFilters,
					this.state.filters,
				),
			},
			() => {
				this.loadEvents(false);
			},
		);
	}

	protected handleFiltersUpdate(filters: IFilters) {
		this.setState({
			filters,
		});
	}

	protected handleFiltersReset() {
		this.setState({
			selectedFilters: {
				filters: [],
			},
			filters: this.generateFilters(),
		});
	}

	protected handleFiltersToggle() {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.state.filters),
		);

		filters.filterToggle = !filters.filterToggle;

		if (!filters.filterToggle) {
			if (this.props.userAuthenticated) {
				const cancerTypesIndex = filters.filterGroups.findIndex(
					value => {
						return value.paramName === 'situations';
					},
				);
				if (cancerTypesIndex >= 0) {
					filters.filterGroups.splice(cancerTypesIndex, 1);
				}

				// Remove the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					filters.filterGroups[
						topicsIndex
					].filters = filters.filterGroups[
						topicsIndex
					].filters.filter(filterValue => {
						const result = this.props.topics.find(topic => {
							return (
								topic.category === 'discussions' &&
								topic.slug === filterValue.value &&
								(topic.is_user_topic ||
									isAdmin(this.props.userData))
							);
						});
						return !!result;
					});
				}
			}
		} else {
			if (this.props.userAuthenticated) {
				// Add the situations at the beginning
				const situationsGroup: IFiltersGroup = {
					label: null,
					noBorder: true,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: !!this.props.userData.situations.find(
									userSituation => {
										return (
											userSituation.name ===
											situation.name
										);
									},
								),
							};
						}),
				};
				filters.filterGroups.unshift(situationsGroup);

				// add the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					this.props.topics.forEach(topic => {
						if (
							!topic.is_user_topic &&
							!isAdmin(this.props.userData) &&
							topic.category === 'discussions'
						) {
							filters.filterGroups[topicsIndex].filters.push({
								label: topic.title,
								value: topic.slug,
								active: false,
							});
						}
					});
				}
			}
		}

		this.setState({
			filters,
		});
	}

	protected loadMyEvents(_append: boolean, value: string) {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				//const url = this.buildQueryParams(append);
				const params = {};
				if (value === 'past_events') {
					params['admin_past_events'] = 1;
				} else if (value === 'upcoming_events') {
					params['include_past'] = false;
				}
				const url = buildUrl(null, {
					path: '/api/events',
					queryParams: params,
				});
				this.props.apiService
					.queryGET(url)
					.then((response: any) => {
						if (
							'data' in response &&
							'events' in response.data &&
							'meta' in response &&
							'current_page' in response.meta &&
							'last_page' in response.meta
						) {
							let events: IApiEvent[] = response.data.events;
							console.log('response ====> ', response);
							this.setState({
								isLoading: false,
								currentPage: response.meta.current_page,
								maxPages: response.meta.last_page,
								total: response.meta.total,
								data: events,
							});
						} else {
							this.props.dispatch(
								apiError(['Invalid data returned by API']),
							);
						}
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isLoading: false,
						});
					});
			},
		);
	}
	protected handleOrderChange(order: FilterOrder) {
		this.setState(
			{
				order,
			},
			() => {
				this.loadEvents(false);
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { situations } = state.situations;
	const { topics } = state.topics;
	const { updateDataCount } = state.updateData;

	return {
		userAuthenticated,
		userData,
		situations,
		situationsLoading: state.situations.loading,
		topics,
		topicsLoading: state.topics.loading,
		updateDataCount,
	};
}

export default connect(mapStateToProps, {})(EventsManagement);
