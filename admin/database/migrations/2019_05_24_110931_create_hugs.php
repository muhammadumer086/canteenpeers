<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHugs extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('hugs', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('discussion_id');
			$table->timestamps();

			$table
				->foreign('user_id')
				->references('id')
				->on('users')
				->onDelete('cascade');

			$table
				->foreign('discussion_id')
				->references('id')
				->on('discussions')
				->onDelete('cascade');
		});

		Schema::table('users', function (Blueprint $table) {
			$table->unsignedInteger('count_hugs')->default(0);
		});

		Schema::table('discussions', function (Blueprint $table) {
			$table->unsignedInteger('count_hugs')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('hugs');

		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('count_hugs');
		});

		Schema::table('discussions', function (Blueprint $table) {
			$table->dropColumn('count_hugs');
		});
	}
}
