import React from 'react';
import ReactSVG from 'react-svg';
import { ILinkData, IApiResource } from '../../interfaces';
import TileTitle from '../general/TileTitle';
import Pill from '../general/Pill';
import { ButtonBaseLink } from '../general/ButtonBaseLink';

interface IProps {
	hasAction: boolean;
	index: number;
	tileData?: IApiResource;
	tileLink: ILinkData;
}

const ResourceFeaturedTile: React.FC<IProps> = (props: IProps) => {
	if (!props.tileData) {
		return null;
	}
	const arrowIconUrl = `${process.env.STATIC_PATH}/icons/arrow.svg`;
	const classes = ['featured_resource_tile'];

	if (props.index === 0) {
		classes.push('featured_resource_tile--left');
	}

	if (props.index === 1) {
		classes.push('featured_resource_tile--right');
	}

	let imageStyles = {};
	if (props.tileData.feature_image) {
		imageStyles = {
			backgroundImage: `url(${props.tileData.feature_image.url})`,
		};
	}

	return (
		<div className={classes.join(' ')}>
			{props.hasAction && (
				<ButtonBaseLink
					focusRipple={true}
					className="featured_resource_tile-button"
					href={props.tileLink.href}
					hrefAs={props.tileLink.as}
					title={props.tileData.title}
				/>
			)}

			<div className="featured_resource_tile-image" style={imageStyles} />

			<div className="featured_resource_tile-content theme--main">
				{props.tileData.topic && (
					<Pill
						label={props.tileData.topic.title}
						active={true}
						disabled={false}
						clickable={true}
						href={`/resources?topic=${props.tileData.topic.slug}`}
					/>
				)}

				<div className="hm-t16">
					<TileTitle content={props.tileData.title} clamp={false} />
				</div>

				<div className="sub_title_detail-action default_link hm-t8">
					<span className="sub_title_detail-label">
						View Resource
					</span>

					<span className="sub_title_detail-label_icon">
						<ReactSVG path={arrowIconUrl} />
					</span>
				</div>
			</div>
		</div>
	);
};

export default ResourceFeaturedTile;
