<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class AuthApplicationTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$name = 'Canteen Facade';
		if (!DB::table('oauth_clients')->where('name', '=', $name)->exists()) {
			Artisan::call('passport:client', [
				'--name' => $name,
				'--password' => true,
			]);
		}
	}
}
