<?php

namespace App\Providers;

use App\Blog;
use App\Discussion;
use App\Message;
use App\Observers\BlogObserver;
use App\Observers\DiscussionObserver;
use App\Observers\UserObserver;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Log;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		Schema::defaultStringLength(191);

		Discussion::observe(DiscussionObserver::class);
		Blog::observe(BlogObserver::class);
		User::observe(UserObserver::class);

		if (env('LOG_DATABASE')) {
			DB::listen(function ($sql) {
				Log::info(
					'Query:, ' .
						join(', ', [
							'"' . $sql->time . '"',
							'"' . $sql->sql . '"',
						])
				);
			});
		}

		Builder::macro('whereLike', function ($attributes, string $searchTerm) {
			$this->where(function (Builder $query) use (
				$attributes,
				$searchTerm
			) {
				foreach (array_wrap($attributes) as $attribute) {
					$query->when(
						str_contains($attribute, '.'),
						function (Builder $query) use (
							$attribute,
							$searchTerm
						) {
							[$relationName, $relationAttribute] = explode(
								'.',
								$attribute
							);

							$query->orWhereHas($relationName, function (
								Builder $query
							) use ($relationAttribute, $searchTerm) {
								$query->where(
									$relationAttribute,
									'LIKE',
									"%{$searchTerm}%"
								);
							});
						},
						function (Builder $query) use (
							$attribute,
							$searchTerm
						) {
							$query->orWhere(
								$attribute,
								'LIKE',
								"%{$searchTerm}%"
							);
						}
					);
				}
			});

			return $this;
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		if (env('APP_ENV') === 'production' || env('APP_ENV') === 'staging') {
			$this->app['url']->forceScheme('https');
		}
	}
}
