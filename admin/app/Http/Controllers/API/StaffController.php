<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStaffRequest;
use App\Http\Requests\UpdateStaffRequest;
use App\Http\Resources\Staff as StaffResource;
use App\Http\Resources\StaffCollection;
use App\Staff;
use App\StaffImage;
use App\Team;
use Illuminate\Http\Request;

use Validator;
use UserPermissionHelper;


class StaffController extends Controller {
	const PAGINATION_SIZE = 20;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["staff_management_read","staff_management_all"]);

		// Validate the query params
		$validator = Validator::make($request->all(), [
			'team' => 'exists:teams,slug',
			'all'  => 'boolean',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		if ($request->has('team')) {
			// Query the team by slug
			$team = Team::where('slug', $request->input('team'))->firstOrFail();
		} else {
			// Select the first team based on order
			$team = Team::orderBy('order', 'ASC')->firstOrFail();
		}
		$pageSize = self::PAGINATION_SIZE;

		if($request->has('page_size'))
		{
			$pageSize = intval($request->page_size);
		}

		if (($this->hasAdminPermission() || $userHasPermission ) && $request->has('all')) {
			return new StaffCollection(Staff::orderBy('order')->paginate($pageSize));
		}

		return new StaffCollection($team->staffs()->orderBy('order')->paginate($pageSize));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["staff_management_add","staff_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$validator = Validator::make($request->all(), [
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'role' => 'required|string',
			'biography' => 'required|string|max:500',
			'order' => 'integer',
			'team' => 'required|exists:teams,slug',
			'image_id' => 'required|string'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$staff = new Staff();

		$staff->first_name = $request->first_name;
		$staff->last_name = $request->last_name;
		$staff->role = $request->role;
		$staff->biography = $request->biography;

		$team = Team::where('slug', $request->team)->firstOrFail();
		$staff->team()->associate($team);

		if ($request->filled('image_id')) {
			$staffImage = StaffImage::where('id', $request->image_id)->firstOrFail();
			$staffImage->staff()->associate($staff->id);
			$staffImage->save();

			$staff->image_id = $staffImage->id;
		}

		$staff->save();

		return response()->json([
			'staff' => new StaffResource($staff),
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Staff  $staff
	 * @return \Illuminate\Http\Response
	 */
	public function show(Staff $staff) {
		return new StaffResource($staff);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Staff  $staff
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Staff $staff) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["staff_management_edit","staff_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}
		$validator = Validator::make($request->all(), [
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'role' => 'required|string',
			'biography' => 'required|string|max:500',
			'order' => 'integer',
			'team' => 'required|exists:teams,slug',
			'image_id' => 'required|string'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$staff->first_name = $request->first_name;
		$staff->last_name = $request->last_name;
		$staff->role = $request->role;
		$staff->biography = $request->biography;

		$team = Team::where('slug', $request->team)->firstOrFail();
		$staff->team()->associate($team);

		if ($request->filled('image_id')) {
			if ($staff->image) {
				$oldImage = $staff->image;
				$oldImage->staff()->dissociate();
				$oldImage->save();
			}
			$staffImage = StaffImage::where('id', $request->image_id)->firstOrFail();
			$staffImage->staff()->associate($staff->id);
			$staffImage->save();

			$staff->image_id = $staffImage->id;
		}


		$staff->save();

		return new StaffResource($staff);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Staff  $staff
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Staff $staff) {
		$request = request();
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["staff_management_delete","staff_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$staff->delete();

		return response()->json([
			'success' => true,
		]);
	}
}
