module.exports = {
	plugins: {
		'autoprefixer': {
			grid: true,
		},
		'cssnano': {
			discardUnused: false,
			minifyFontValues: false,
			zindex: false
		},
	}
}
