import React from 'react';
import buildUrl from 'build-url';
import { connect } from 'react-redux';
import { debounce } from '../../../helpers/debounce';

import { GiphySelectionResultsList } from './../Giphy';

import { ApiService, withServices, GiphyService } from '../../../services';

import { IStoreState } from '../../../../js/redux/store';
import { IApiGIFObject } from '../../../../js/interfaces';
import ListLoading from '../ListLoading';

interface IProps {
	apiService: ApiService;
	onGifSelection(gifData: IApiGIFObject): void;
	dispatch(value: any): void;
}

interface IState {
	isLoading: boolean;
	searchQuery: string;
	trendingGifs: IApiGIFObject[] | null;
	searchedGifs: IApiGIFObject[] | null;
}

class GiphySelection extends React.Component<IProps, IState> {
	protected giphyService: GiphyService;

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			searchQuery: '',
			trendingGifs: null,
			searchedGifs: null,
		};

		this.setSearchQuery = this.setSearchQuery.bind(this);
		this.giphySearchRequest = debounce(
			this.giphySearchRequest.bind(this),
			500,
			false,
		);
		this.getTrendingGiphy = this.getTrendingGiphy.bind(this);
	}

	public componentDidMount() {
		this.giphyService = new GiphyService(this.props.dispatch);

		this.getTrendingGiphy();
	}

	public render() {
		return (
			<div id="giphy_selection" className="giphy_selection">
				<div className="giphy_selection-input">
					{this.state.isLoading && (
						<ListLoading active={true} size={20} />
					)}
					<input
						placeholder="Search for your Gif"
						onChange={this.setSearchQuery}
					/>
				</div>

				{!!this.state.trendingGifs && (
					<React.Fragment>
						{!!this.state.searchedGifs &&
						this.state.searchedGifs.length &&
						this.state.searchQuery.length > 0 ? (
							<div className="giphy_selection-results">
								<GiphySelectionResultsList
									onGifSelection={this.props.onGifSelection}
									data={this.state.searchedGifs}
								/>
							</div>
						) : (
							<div className="giphy_selection-results">
								<GiphySelectionResultsList
									onGifSelection={this.props.onGifSelection}
									data={this.state.trendingGifs}
								/>
							</div>
						)}
					</React.Fragment>
				)}
			</div>
		);
	}

	protected setSearchQuery(event: React.FormEvent<HTMLInputElement>) {
		const searchQuery = event.currentTarget.value;

		this.setState(
			{
				searchQuery,
				isLoading: true,
			},
			() => {
				this.giphySearchRequest();
			},
		);
	}

	protected getTrendingGiphy() {
		this.setState(
			{
				isLoading: true,
			},
			async () => {
				const params = {
					api_key: 'Rug7F7F1WueZR33CW7SgfhDvSWX2AxcR',
					limit: 6,
				};

				const url = buildUrl(process.env.GIPHY_URL, {
					path: '/v1/gifs/trending',
					queryParams: params,
				});

				const { data } = await this.giphyService.queryGET(url);

				this.setState({
					trendingGifs: data,
					isLoading: false,
				});
			},
		);
	}

	protected giphySearchRequest() {
		this.setState(
			{
				isLoading: true,
			},
			async () => {
				const params = {
					api_key: 'Rug7F7F1WueZR33CW7SgfhDvSWX2AxcR',
					q: this.state.searchQuery,
					limit: 20,
				};

				const url = buildUrl(process.env.GIPHY_URL, {
					path: '/v1/gifs/search',
					queryParams: params,
				});

				const { data } = await this.giphyService.queryGET(url);

				this.setState({
					searchedGifs: data,
					isLoading: false,
				});
			},
		);
	}
}

const mapStateToProps = (_state: IStoreState) => {
	return {};
};

export default connect(mapStateToProps)(withServices(GiphySelection));
