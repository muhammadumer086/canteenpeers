<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Mail;

use App\Mail\DiscussionReported;
use App\Mail\UserReported;
use App\Mail\UserBlocked;

use App\User;
use App\Discussion;

/**
 * Send admin email to a pre-defined list of users
 */
class AdminEmail {
	protected $emails = ['digitech@canteen.org.au'];

	public function sendDiscussionReported(
		$discussion,
		$author,
		$ageSensitive
	) {
		Mail::to($this->emails)->send(
			new DiscussionReported($discussion, $author, $ageSensitive)
		);
	}

	public function sendUserReported(
		$reportedUser,
		$author,
		$reportReason,
		$reportInformation
	) {
		Mail::to($this->emails)->send(
			new UserReported(
				$reportedUser,
				$author,
				$reportReason,
				$reportInformation
			)
		);
	}

	public function sendUserBlocked($blocker, $blocked) {
		$emails = $this->emails;
		/*if (config('app.debug')) {
			$emails[] = 'yoann@frankdigital.com.au';
		}*/
		Mail::to($emails)->send(new UserBlocked($blocker, $blocked));
	}
}
