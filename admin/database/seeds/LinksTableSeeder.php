<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Link;
use App\LinkImage;

class LinksTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$links = [
			[
				'title' => 'Counselling',
				'subtitle' => 'Chat to a counsellor',
				'image' => '/images/quick-links/1.jpg',
				'url' => [
					'type' => 'custom',
					'data' => 'OPEN_COUNSELLOR_CHAT',
				],
			],
			[
				'title' => 'Connect with peers',
				'subtitle' => 'People',
				'image' => '/images/quick-links/2.jpg',
				'url' => [
					'type' => 'link',
					'data' => '/people-like-me',
				],
			],
			[
				'title' => 'Share my story',
				'subtitle' => 'Discussions',
				'image' => '/images/quick-links/3.jpg',
				'url' => [
					'type' => 'custom',
					'data' => 'OPEN_NEW_DISCUSSION_MODAL',
				],
			],
			[
				'title' => 'Get involved',
				'subtitle' => 'Events',
				'image' => '/images/quick-links/4.jpg',
				'url' => [
					'type' => 'link',
					'data' => '/events',
				],
			],
			[
				'title' => 'Get information',
				'subtitle' => 'Resources',
				'image' => '/images/quick-links/5.jpg',
				'url' => [
					'type' => 'link',
					'data' => '/resources',
				],
			],
		];

		// Clear all the links and link images
		LinkImage::whereNotNull('id')->delete();
		Link::whereNotNull('id')->delete();

		foreach ($links as $value) {
			// Upload the asset to S3
			$pathPartial = 'link-images/' . md5(date('Y-m-d-h:i:s') . rand());
			$file = new File(resource_path() . $value['image']);
			$path = Storage::disk('s3')->putFile($pathPartial, $file, 'public');
			$imageSize = getimagesize($file);

			$linkImage = new LinkImage();
			$linkImage->path = $path;
			$linkImage->width = $imageSize[0];
			$linkImage->height = $imageSize[1];
			$linkImage->save();

			$link = new Link();
			$link->title = $value['title'];
			$link->subtitle = $value['subtitle'];
			$link->url = $value['url'];
			$link->image_id = $linkImage->id;
			$link->save();

			$linkImage->link_id = $link->id;
			$linkImage->save();
		}
	}
}
