<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Language;
use App\Http\Resources\Language as LanguageResource;

class LanguageController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$languages = Language::orderBy('slug', 'ASC')->get();

		return response()->json([
			'languages' => LanguageResource::collection($languages),
		]);
	}
}
