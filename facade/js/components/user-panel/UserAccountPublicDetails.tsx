import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import moment from 'moment';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import {
	IApiUser,
	ISelectOption,
	IApiSituation,
	ISituationOption,
} from '../../interfaces';
import { EAccountScreens, ETabs } from './UserPanelAccount';
import isAdmin from '../../helpers/isAdmin';
import { validationsUser } from '../../helpers/validations';
import {
	TextFieldFormik,
	CheckboxGroupSituationsFormik,
	SelectFieldFormik,
	InlineDatePickerFormik,
	RadioGroupFormik,
} from '../inputs';
import { TextField } from '@material-ui/core';

interface IProps {
	isAdminEdit: boolean;
	userData: IApiUser | null;
	submitting: boolean;
	situations: ISituationOption[];
	situationsList: IApiSituation[];
	changeScreen(tab: ETabs, screen: EAccountScreens);
	onSubmit(values: any, resetForm?: () => void);
}

interface IAdditionalRelatedQuestions {
	label: string;
	value: string;
	name: string;
}

const UserAccountPublicDetails: React.FC<IProps> = (props: IProps) => {
	let validationFormat: Yup.ObjectSchema<any> = Yup.object().shape({
		new_username: validationsUser.username,
		dob: Yup.string().nullable(true),
		gender: validationsUser.gender,
		self_identify: validationsUser.self_identify,
		situations: Yup.array()
			.of(
				Yup.object().shape({
					slug: Yup.string().required(),
					name: Yup.string().required(),
					cancer_type: Yup.string().nullable(true),
				}),
			)
			.required('Please select at least one option'),
		about: validationsUser.about,
	});

	if (isAdmin(props.userData)) {
		validationFormat = Yup.object().shape({
			new_username: validationsUser.username,
			dob: Yup.string().nullable(true),
			gender: validationsUser.gender,
			self_identify: validationsUser.self_identify,
			situations: Yup.array(),
			about: validationsUser.about,
		});
	}

	const initialValues = getInitialValues();

	return (
		<div className="user_account_panel user_account_panel--account_public_details">
			<div className="user_panel-back">
				<ButtonBase
					focusRipple={true}
					className="user_panel-back-button"
					onClick={() => {
						props.changeScreen(
							ETabs.MY_PROFILE,
							EAccountScreens.OVERVIEW,
						);
					}}
					disabled={props.submitting}
				>
					<Icon className="user_panel-back-back_icon">
						arrow_back
					</Icon>
					Profile Overview
				</ButtonBase>
			</div>
			<div className="user_panel-heading">
				<h4 className="font--h4 theme-title">Public Details</h4>
			</div>
			<Formik
				initialValues={initialValues}
				validationSchema={validationFormat}
				onSubmit={values => {
					const reducedValues = JSON.parse(JSON.stringify(values));

					if ('dob' in reducedValues && reducedValues.dob) {
						reducedValues.dob = moment(reducedValues.dob).format(
							'YYYY-MM-DD',
						);
					}

					for (const key in reducedValues) {
						if (key.startsWith('cancer_type_')) {
							const keySlug = key.replace('cancer_type_', '');
							(reducedValues.situations as ISituationOption[]).forEach(
								singleSituation => {
									if (singleSituation.slug === keySlug) {
										singleSituation.cancer_type =
											reducedValues[key];
									}
								},
							);
							delete reducedValues[key];
						}
						if (key.startsWith('date_cancer_type_')) {
							const keySlug = key.replace(
								'date_cancer_type_',
								'',
							);
							(reducedValues.situations as ISituationOption[]).forEach(
								singleSituation => {
									if (singleSituation.slug === keySlug) {
										if (reducedValues[key]) {
											singleSituation.date = moment(
												reducedValues[key],
											).format('YYYY-MM-DD');
										} else {
											singleSituation.date = null;
										}
									}
								},
							);
							delete reducedValues[key];
						}
					}

					props.onSubmit(reducedValues);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Username (not your real name)"
										name="new_username"
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									{props.isAdminEdit ||
									isAdmin(props.userData) ? (
										<InlineDatePickerFormik
											label="Date of Birth"
											name="dob"
											format="DD/MM/YYYY"
											disableFuture={true}
											disabled={props.submitting}
											{...inputProps}
										/>
									) : (
										<TextField
											className="form-input"
											label="Date of Birth"
											type="text"
											name="dob"
											disabled={true}
											value={moment(
												inputProps.values.dob,
											).format('DD/MM/YYYY')}
										/>
									)}

									<p className="font--small theme-text--body_light">
										Only your age will be visible to the
										community.
									</p>
									{!props.isAdminEdit &&
										!isAdmin(props.userData) && (
											<p className="font--small theme-text--body_light">
												If you made a mistake with your
												date of birth please{' '}
												<a
													href="mailto:online@canteen.org.au?subject=Canteen Connect - Date of birth change request"
													target="_blank"
												>
													contact an Admin
												</a>
												.
											</p>
										)}
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<RadioGroupFormik
										label="Gender"
										name="gender"
										options={[
											{
												label: 'Female',
												value: 'female',
											},
											{
												label: 'Female Trans',
												value: 'female-trans',
											},
											{
												label: 'Male',
												value: 'male',
											},
											{
												label: 'Male Trans',
												value: 'male-trans',
											},
											{
												label: `I don't identify with the options above`,
												value: 'other',
											},
										]}
										disabled={props.submitting}
										{...inputProps}
									/>
									<p className="font--small theme-text--body_light">
										This can be hidden to other users in
										settings
									</p>
								</div>
							</div>
							{values['gender'] === 'other' && (
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Please let us know how you self-identify"
											name="self_identify"
											disabled={props.submitting}
											{...inputProps}
										/>
										<br />
										<br />
									</div>
								</div>
							)}

							{!isAdmin(props.userData) && (
								<React.Fragment>
									<div className="form-row">
										<div className="form-column">
											<CheckboxGroupSituationsFormik
												label="What best describes your cancer experience"
												name="situations"
												options={props.situations}
												disabled={props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									{values.situations.map(
										(singleSituation: ISituationOption) => {
											return renderSituationOptions(
												inputProps,
												singleSituation,
											);
										},
									)}
								</React.Fragment>
							)}

							<div className="form-row form-row--space_above">
								<div className="form-column">
									<h5 className="font--h6 theme-title">
										A little about yourself
									</h5>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="About me"
										name="about"
										multiline={true}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit_left">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid ||
											!touched ||
											props.submitting
										}
									>
										Update
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);

	function getInitialValues() {
		return {
			new_username: props.userData.username,
			dob: props.userData.dob,
			gender: props.userData.gender,
			self_identify: props.userData.self_identify || '',
			situations: getUserSituations(props.userData.situations),
			about: props.userData.about ? props.userData.about : '',
			...props.userData.situations.reduce(
				(accum, current) =>
					Object.assign(accum, {
						[`cancer_type_${
							current.main_slug
						}`]: current.cancer_type,
						[`date_cancer_type_${current.main_slug}`]: current.date,
					}),
				{},
			),
		};
	}

	function getUserSituations(
		situations: IApiSituation[],
	): ISituationOption[] {
		const result = [];
		if (!isAdmin(props.userData)) {
			situations.forEach((singleSituation: IApiSituation) => {
				if (result.indexOf(singleSituation.main_slug) === -1) {
					result.push({
						slug: singleSituation.main_slug,
						name: singleSituation.name,
						cancer_type: singleSituation.cancer_type,
					});
				}
			});
		}
		return result;
	}

	function renderSituationOptions(
		inputProps: any,
		singleSituation: ISituationOption,
	) {
		// Get the list of cancer types associated with this situation
		const cancerTypes: ISelectOption[] = [];
		props.situationsList.forEach(situation => {
			if (
				situation.main_slug === singleSituation.slug &&
				situation.cancer_type
			) {
				cancerTypes.push({
					label: situation.cancer_type,
					value: situation.cancer_type,
				});
			}
		});

		const additionalQuestions: IAdditionalRelatedQuestions[] = filterAdditionQuestionBySlug(
			singleSituation,
		);

		if (!cancerTypes.length) {
			return null;
		}

		return (
			<React.Fragment key={singleSituation.slug}>
				<div className="form-row form-row--space_above">
					<div className="form-column">
						<h5 className="font--h6 theme-title">
							{singleSituation.name}
						</h5>
					</div>
				</div>
				<div className="form-row">
					<div className="form-column">
						<SelectFieldFormik
							label="Cancer Type"
							name={`cancer_type_${singleSituation.slug}`}
							options={cancerTypes}
							values={singleSituation.cancer_type}
							disabled={props.submitting}
							emptyLabel="Please select"
							{...inputProps}
						/>
					</div>
				</div>

				{!!additionalQuestions.length &&
					renderSituationRelatedQuestion(
						additionalQuestions,
						inputProps,
						singleSituation.slug,
					)}
			</React.Fragment>
		);
	}

	function renderSituationRelatedQuestion(
		additionalQuestions: IAdditionalRelatedQuestions[],
		inputProps: any,
		situationSlug: string,
	) {
		return additionalQuestions.map((question, index) => {
			return (
				<React.Fragment key={index}>
					<div className="form-row form-row--space_above">
						<div className="form-column">
							<InlineDatePickerFormik
								label={question.label}
								name={`${question.name}_${situationSlug}`}
								format="DD/MM/YYYY"
								disabled={props.submitting}
								disableFuture={true}
								{...inputProps}
							/>
							<p className="font--small theme-text--body_light">
								Not visible to the community.
							</p>
						</div>
					</div>
				</React.Fragment>
			);
		});
	}

	function filterAdditionQuestionBySlug(singleSituation: ISituationOption) {
		let additionalQuestions: IAdditionalRelatedQuestions[] = [];

		let label = null;

		switch (singleSituation.slug) {
			case 'my-parent-has-cancer':
			case 'my-sibling-has-cancer':
			case 'im-a-young-person-with-cancer':
				label = 'Date of Diagnosis';
				break;
			case 'i-survived-cancer':
			case 'my-parent-survived-cancer':
			case 'my-sibling-survived-cancer':
				label = 'Date treatment ended';
				break;
			case 'my-parent-died-from-cancer':
			case 'my-sibling-died-from-cancer':
				label = 'Date of death';
				break;
		}

		if (label) {
			additionalQuestions.push({
				label,
				value: singleSituation.date,
				name: 'date_cancer_type',
			});
		}

		return additionalQuestions;
	}
};

export default UserAccountPublicDetails;
