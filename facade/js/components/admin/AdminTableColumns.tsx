import React from 'react';
import moment from 'moment';
import Link from 'next/link';

import {
	IApiUser,
	IApiUserActivity,
	IApiDiscussion,
	IApiBlog,
	IApiResource,
} from '../../interfaces';
import UserAvatar from '../general/UserAvatar';
import Pill from '../general/Pill';

interface IPropColumnUser {
	data: IApiUser;
}

interface IPropColumnCount {
	data: number | null;
}

interface IPropColumnActivity {
	data: IApiUserActivity;
}

export const ColumnNameSituation: React.FC<IPropColumnUser> = props => {
	const contentClasses = ['admin_table-column-user_profile-content'];
	if (props.data.inactive) {
		contentClasses.push(
			'admin_table-column-user_profile-content--inactive',
		);
	}
	return (
		<div className="admin_table-column-user_profile">
			<div className="admin_table-column-user_profile-avatar">
				<UserAvatar user={props.data} />
			</div>
			<div className={contentClasses.join(' ')}>
				{props.data.first_name} {props.data.last_name} (
				{props.data.username})<br />
				<i>{props.data.email}</i>
				<br />
				<i>User ID: {props.data.id}</i>
			</div>
		</div>
	);
};

export const ColumnCount: React.FC<IPropColumnCount> = props => {
	return <div>{props.data ? props.data : '—'}</div>;
};

export const ColumnAge: React.FC<IPropColumnUser> = props => {
	let age: string | number = '—';
	if (props.data.dob) {
		age = moment().diff(props.data.dob, 'years');
	}
	return <div>{age}</div>;
};

export const ColumnState: React.FC<IPropColumnUser> = props => {
	let result: string = '—';
	if (props.data.state) {
		result = props.data.state;
	} else if (props.data.country_slug) {
		result = props.data.country_slug;
	}
	return <div>{result}</div>;
};

export const ColumnGender: React.FC<IPropColumnUser> = props => {
	return <div>{props.data.gender ? props.data.gender : '—'}</div>;
};

export const ColumnSituation: React.FC<IPropColumnUser> = props => {
	let situations: string[] = ['—'];
	if (props.data.situations && props.data.situations.length) {
		situations = props.data.situations.map(situation => {
			return (
				situation.name +
				(situation.cancer_type ? ` (${situation.cancer_type})` : '')
			);
		});
	}

	return (
		<div>
			{situations.map((situation, index) => (
				<div key={index}>{situation}</div>
			))}
		</div>
	);
};

export const ColumnNumberChildren: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.number_children} />;
};

export const ColumnLastLogin: React.FC<IPropColumnUser> = props => {
	return <div>{props.data.last_login ? props.data.last_login : '—'}</div>;
};

export const ColumnTotalLogins: React.FC<IPropColumnUser> = props => {
	return <div>{props.data.total_logins ? props.data.total_logins : '—'}</div>;
};

export const ColumnTotalBlogs: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.blogs_count} />;
};

export const ColumnTotalDiscussions: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.active_discussions_count} />;
};

export const ColumnTotalLikes: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.likes_count} />;
};

export const ColumnTotalHugs: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.hugs_count} />;
};

export const ColumnTotalFollowedDiscussions: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.following_count} />;
};

export const ColumnTotalSavedResources: React.FC<IPropColumnUser> = props => {
	return <ColumnCount data={props.data.saved_resources_count} />;
};
export const ColumnRole: React.FC<IPropColumnUser> = props => {
	return <div>{props.data.role_names[0]}</div>;
};

export const ColumnActivityLog: React.FC<IPropColumnUser> = props => {
	return (
		<div>
			<span className="admin_table-clickable">
				<Link
					href={`/admin-user-activities?id=${props.data.username}`}
					as={`/admin/users/${props.data.username}/activities`}
				>
					<Pill
						label="Activity Log"
						active={true}
						clickable={true}
						disabled={false}
					/>
				</Link>
			</span>
		</div>
	);
};

export const ColumnActivityUser: React.FC<IPropColumnActivity> = props => {
	if (!props.data.user) {
		return <div>—</div>;
	}

	return <ColumnNameSituation data={props.data.user} />;
};

export const ColumnType: React.FC<IPropColumnActivity> = props => {
	return <div>{props.data.type}</div>;
};

export const ColumnPlatform: React.FC<IPropColumnActivity> = props => {
	return <div>{props.data.platform}</div>;
};

export const ColumnActivityTime: React.FC<IPropColumnActivity> = props => {
	const createdAt = moment(props.data.created_at.date);
	return <div>{createdAt.format('YYYY-MM-DD HH:mm:ss')}</div>;
};

export const ColumnTitle: React.FC<IPropColumnActivity> = props => {
	let label = '—';
	let deleted = false;
	let url = null;
	let urlAs = null;
	if (props.data.activity) {
		if (props.data.activity_model_name === 'discussion') {
			const discussion: IApiDiscussion = props.data
				.activity as IApiDiscussion;
			const mainDiscussion = getMainDiscussion(discussion);

			if (mainDiscussion == null) {
				return;
			}

			const slug = mainDiscussion.slug;
			label = mainDiscussion.title;
			deleted = !!(mainDiscussion.deleted_at || discussion.deleted_at);
			if (!deleted) {
				url = `/discussions-single?id=${slug}`;
				urlAs = `/discussions/${slug}`;
			}
		} else if (props.data.activity_model_name === 'blog') {
			const blog: IApiBlog = props.data.activity as IApiBlog;
			label = blog.title;
			deleted = !!blog.deleted_at;
			if (!deleted) {
				url = `/blogs-single?id=${blog.slug}`;
				urlAs = `/blogs/${blog.slug}`;
			}
		} else if (props.data.activity_model_name === 'resource') {
			const resource: IApiResource = props.data.activity as IApiResource;
			label = resource.title;
			deleted = !!resource.deleted_at;
			if (!deleted) {
				url = `/resources-single?id=${resource.slug}`;
				urlAs = `/resources/${resource.slug}`;
			}
		}
	}

	return (
		<div
			style={{
				textDecoration: deleted ? 'line-through' : undefined,
			}}
		>
			{!!url ? (
				<Link href={url} as={urlAs}>
					<a className="admin_table-clickable">{label}</a>
				</Link>
			) : (
				<span>{label}</span>
			)}
		</div>
	);
};

export const ColumnSituations: React.FC<IPropColumnActivity> = props => {
	let situations = ['—'];

	if (props.data.activity_model_name === 'discussion') {
		const discussion = getMainDiscussion(
			props.data.activity as IApiDiscussion,
		);
		if (discussion && discussion.situations) {
			situations = discussion.situations.map(situation => {
				return situation.name;
			});
		}
	} else if (props.data.activity_model_name === 'blog') {
		const blog: IApiBlog = props.data.activity as IApiBlog;
		if (blog && blog.situations) {
			situations = blog.situations.map(situation => {
				return situation.name;
			});
		}
	} else if (props.data.activity_model_name === 'resource') {
		const resource: IApiResource = props.data.activity as IApiResource;
		if (resource && resource.situations) {
			situations = resource.situations.map(situation => {
				return situation.name;
			});
		}
	}

	return (
		<div>
			{situations.map((item, index) => (
				<div key={index}>{item}</div>
			))}
		</div>
	);
};

export const ColumnTopic: React.FC<IPropColumnActivity> = props => {
	let topic = '—';

	if (props.data.activity_model_name === 'discussion') {
		const discussion = getMainDiscussion(
			props.data.activity as IApiDiscussion,
		);
		if (discussion && discussion.topic) {
			topic = discussion.topic.title;
		}
	} else if (props.data.activity_model_name === 'blog') {
		const blog: IApiBlog = props.data.activity as IApiBlog;
		if (blog && blog.topic) {
			topic = blog.topic.title;
		}
	} else if (props.data.activity_model_name === 'resource') {
		const resource: IApiResource = props.data.activity as IApiResource;
		if (resource && resource.topic) {
			topic = resource.topic.title;
		}
	}

	return <div>{topic}</div>;
};

export const ColumnViewsCount: React.FC<IPropColumnActivity> = props => {
	let count: string | number = '—';

	if (props.data.activity_model_name === 'discussion') {
		const discussion = getMainDiscussion(
			props.data.activity as IApiDiscussion,
		);
		if (discussion !== null) {
			count = discussion.views_count;
		}
	} else if (props.data.activity_model_name === 'blog') {
		const blog: IApiBlog = props.data.activity as IApiBlog;
		if (blog) {
			count = blog.views_count;
		}
	} else if (props.data.activity_model_name === 'resource') {
		const resource: IApiResource = props.data.activity as IApiResource;
		if (resource) {
			count = resource.views_count;
		}
	}

	return <div>{count}</div>;
};

export const ColumnGetItCount: React.FC<IPropColumnActivity> = props => {
	let count: string | number = '—';

	if (props.data.activity_model_name === 'discussion') {
		const discussion = getMainDiscussion(
			props.data.activity as IApiDiscussion,
		);

		if (discussion !== null) {
			count = discussion.likes_count;
		}
	}

	return <div>{count}</div>;
};

export const ColumnHugCount: React.FC<IPropColumnActivity> = props => {
	let count: string | number = '—';

	if (props.data.activity_model_name === 'discussion') {
		const discussion = getMainDiscussion(
			props.data.activity as IApiDiscussion,
		);

		if (discussion !== null) {
			count = discussion.hugs_count;
		}
	}

	return <div>{count}</div>;
};

export const ColumnRepliesCount: React.FC<IPropColumnActivity> = props => {
	let count: string | number = '—';

	if (props.data.activity_model_name === 'discussion') {
		const discussion = getMainDiscussion(
			props.data.activity as IApiDiscussion,
		);

		if (discussion !== null) {
			count = discussion.replies_count;
		}
	}

	return <div>{count}</div>;
};

export const ColumnAdditionalData: React.FC<IPropColumnActivity> = props => {
	let content = ['—'];
	if (props.data.content) {
		content = [];
		for (let key in props.data.content) {
			let contentData = props.data.content[key];
			if (contentData instanceof Array) {
				contentData = contentData.join(', ');
			}
			content.push(`${key}: ${contentData}`);
		}
	}
	return (
		<div>
			{content.map((item, index) => (
				<div key={index}>{item}</div>
			))}
		</div>
	);
};

function getMainDiscussion(discussion: IApiDiscussion): IApiDiscussion | null {
	if (!!discussion) {
		return discussion.main_discussion
			? discussion.main_discussion
			: discussion;
	}

	return null;
}
