import React from "react";
import { ModalDialog } from '../modals/local/ModalDialog';
import { IApiEvent } from '../../interfaces';
import ReactPlayer from 'react-player';

interface IProps {
    event : IApiEvent,
	toggleModel(any): void;
}
class EventPromoModal extends React.Component<IProps>{
    public render(){
        return(
            <ModalDialog
						modalTitle={this.props.event.title}
						isActive={true}
						handleClose={this.props.toggleModel}
					>
						<div className="player-wrapper mt-1">
							<ReactPlayer
								className="react-player"
								url={
									this.props.event.promo_video_url !== ''
										? this.props.event.promo_video_url
										: `${process.env.STATIC_PATH}/videos/landing_hero_loop.mp4`
								}
								controls
								width="100%"
								height="100%"
								loop
								playing={true}
							/>
						</div>
					</ModalDialog>
        )
    }
}
export default EventPromoModal;