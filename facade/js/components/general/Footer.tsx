import React from 'react';
import { connect } from 'react-redux';

import { Icon } from '@material-ui/core';

import { IconFacebook, IconTwitter, IconYouTube, IconInstagram } from './Icons';
import { dissectUrl, EStaticLinkType } from '../../../js/helpers/dissectUrl';

import {
	IStoreState,
	contactModalDisplay,
	userVerified,
} from '../../redux/store';
import { ButtonBaseLink } from './ButtonBaseLink';
import { getCountry } from '../../helpers/getCountry';
import { IApiUser } from '../../interfaces';

interface IProps {
	userAuthenticated: boolean;
	userData: IApiUser;
	isLandingPage?: boolean;
	dispatch(action: any): void;
}

interface ILink {
	label: string;
	url?: {
		href: string;
		hrefAs: string;
		type: string;
	};
	action?: () => void;
	icon?: React.SFC<any>;
	requireAuthenticated?: boolean;
}

interface ILinkGroup {
	id: string;
	title: string;
	clickable: boolean;
	links: ILink[];
}

interface IState {
	linkGroups: ILinkGroup[];
	openGroups: string[];
	country: string;
}

class Footer extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);
		this.state = {
			linkGroups: [],
			openGroups: [],
			country: 'AU',
		};

		this.renderGroup = this.renderGroup.bind(this);
		this.renderLink = this.renderLink.bind(this);
		this.handleGroupToggle = this.handleGroupToggle.bind(this);
		this.getLinkGroups = this.getLinkGroups.bind(this);
	}
	public componentDidUpdate(prevP: IProps) {
		if (prevP.userAuthenticated !== this.props.userAuthenticated) {
			const country = getCountry(
				this.props.userAuthenticated,
				this.props.userData,
			);
			this.setState({ country, linkGroups: this.getLinkGroups(country) });
		}
	}
	public componentDidMount() {
		const country = getCountry(
			this.props.userAuthenticated,
			this.props.userData,
		);
		this.setState({ country, linkGroups: this.getLinkGroups(country) });
	}
	public render() {
		const footerClass = ['footer'];
		const { country } = this.state;
		if (country !== 'AU') {
			footerClass.push('footer--nz');
		}
		if (this.props.isLandingPage) {
			footerClass.push('footer--landing_page');
			if (country !== 'AU') {
				footerClass.push('footer--landing_page-nz');
			}
		}

		return (
			<footer className={footerClass.join(' ')}>
				<div className="footer-content_container">
					<div className="footer-mobile-footer">
						<img
							src={`${process.env.STATIC_PATH}/images/canteen-connect-logo.png`}
							alt={`${process.env.APP_URL}`}
						/>
					</div>

					{this.state.linkGroups.map(this.renderGroup)}
				</div>
			</footer>
		);
	}

	protected renderGroup(linkGroup: ILinkGroup, index) {
		const classNames = [
			'footer-content',
			`footer-content--${linkGroup.id}`,
		];

		if (linkGroup.clickable) {
			classNames.push('footer-content--clickable');

			if (this.state.openGroups.indexOf(linkGroup.id) >= 0) {
				classNames.push('footer-content--active');
			}
		}

		return (
			<aside className={classNames.join(' ')} key={index}>
				{linkGroup.clickable && (
					<h5
						className="footer-content-title font--body theme-title"
						onClick={() => {
							this.handleGroupToggle(linkGroup.id);
						}}
						tabIndex={0}
					>
						{linkGroup.title}

						<div className="footer-content-title-icon">
							<Icon className="footer-content-title-icon-svg">
								arrow_left
							</Icon>
						</div>
					</h5>
				)}
				{!linkGroup.clickable && (
					<h5 className="footer-content-title font--body theme-title">
						{linkGroup.title}
					</h5>
				)}
				<ul className="footer-content-list">
					{linkGroup.links.map(this.renderLink)}
				</ul>
			</aside>
		);
	}

	protected renderLink(link: ILink, index) {
		if (link.requireAuthenticated && !this.props.userAuthenticated) {
			return null;
		}
		if (link.url) {
			if (link.url.type === EStaticLinkType.EXTERNAL) {
				return (
					<li className="footer-content-list-item" key={index}>
						<a
							className="footer-content-link font--body theme-text--accent_dark"
							href={link.url.hrefAs}
							target="_blank"
							rel="noopener"
						>
							{!!link.icon && (
								<span className="footer-content-link-icon_container">
									<link.icon className="footer-content-link-icon" />
								</span>
							)}
							<span className="footer-content-link-label">
								{link.label}
							</span>
						</a>
					</li>
				);
			}

			if (link.url.type === EStaticLinkType.LINK) {
				return (
					<li className="footer-content-list-item" key={index}>
						<ButtonBaseLink
							disableRipple={true}
							href={link.url.href}
							hrefAs={link.url.hrefAs}
						>
							{!!link.icon && (
								<span className="footer-content-link-icon_container">
									<link.icon className="footer-content-link-icon" />
								</span>
							)}
							<span className="footer-content-link-label">
								{link.label}
							</span>
						</ButtonBaseLink>
					</li>
				);
			}
		} else if (link.action) {
			return (
				<li className="footer-content-list-item" key={index}>
					<span
						className="footer-content-link font--body theme-text--accent_dark"
						onClick={link.action}
						tabIndex={0}
					>
						{!!link.icon && (
							<span className="footer-content-link-icon_container">
								<link.icon className="footer-content-link-icon" />
							</span>
						)}
						<span className="footer-content-link-label">
							{link.label}
						</span>
					</span>
				</li>
			);
		}
	}

	protected handleGroupToggle(id: string) {
		const index = this.state.openGroups.indexOf(id);
		const openGroups: string[] = JSON.parse(
			JSON.stringify(this.state.openGroups),
		);
		if (index >= 0) {
			openGroups.splice(index, 1);
		} else {
			openGroups.push(id);
		}
		this.setState({
			openGroups,
		});
	}
	protected getLinkGroups = (country: string) => {
		let emergencyLinks = [
			{
				label: 'Triple Zero',
				url: dissectUrl('https://www.triplezero.gov.au/'),
			},
			{
				label: 'Lifeline',
				url: dissectUrl('https://www.lifeline.org.au/'),
			},
			{
				label: 'Kids Helpline',
				url: dissectUrl('https://kidshelpline.com.au/'),
			},
		];
		if (country === 'NZ') {
			emergencyLinks = [
				{
					label: 'Lifeline Aotearoa',
					url: dissectUrl('https://www.lifeline.org.nz/'),
				},
				{
					label: 'Youth Health Services',
					url: dissectUrl('https://www.countiesmanukau.health.nz/'),
				},
				{
					label: 'Youthline NZ',
					url: dissectUrl('https://www.youthline.co.nz'),
				},
				{
					label: 'Suicide Crisis Helpline',
					url: dissectUrl(
						'https://www.lifeline.org.nz/services/suicide-crisis-helpline',
					),
				},
			];
		}
		return [
			{
				id: 'emergency',
				title: 'Emergency Links',
				clickable: true,
				links: emergencyLinks,
			},
			{
				id: 'other',
				title: 'Other Links',
				clickable: true,
				links: [
					{
						label: 'Contact Us',
						action: () => {
							this.props.dispatch(contactModalDisplay());
						},
					},
					{
						label: 'Tour',
						action: () => {
							this.props.dispatch(userVerified());
						},
						requireAuthenticated: true,
					},
					{
						label: 'Meet the team',
						url: dissectUrl('/meet-the-team/'),
						requireAuthenticated: true,
					},
					{
						label: 'Terms & Conditions',
						url: dissectUrl('/resources/terms-and-conditions'),
					},
					{
						label: 'Privacy Policy',
						url: dissectUrl(
							'https://www.canteen.org.au/privacy-policy/',
						),
					},
				],
			},
			{
				id: 'our-sites',
				title: 'Our Sites',
				clickable: true,
				links: [
					{
						label: 'Australia',
						url: dissectUrl('https://www.canteen.org.au/'),
					},
					{
						label: 'New Zealand',
						url: dissectUrl('https://www.canteen.org.nz/'),
					},
				],
			},
			{
				id: 'connect',
				title: 'Connect with CanTeen',
				clickable: false,
				links: [
					{
						label: 'Facebook',
						url: dissectUrl(
							`https://www.facebook.com/${
								country === 'AU'
									? 'CanTeenAus/'
									: 'CanTeenAotearoa/'
							}`,
						),
						icon: IconFacebook,
					},
					{
						label: 'YouTube',

						url: dissectUrl(
							`https://www.youtube.com${
								country === 'AU'
									? '/user/CanTeenAustralia'
									: '/CanTeenAotearoa'
							}`,
						),
						icon: IconYouTube,
					},
					{
						label: 'Twitter',
						url: dissectUrl(
							`https://twitter.com/${
								country === 'AU'
									? 'canteenaus'
									: 'CanTeenAotearoa'
							}`,
						),
						icon: IconTwitter,
					},
					{
						label: 'Instagram',
						url: dissectUrl(
							`https://www.instagram.com/${
								country === 'AU'
									? 'canteen_aus/'
									: 'canteen_aotearoa/'
							}`,
						),
						icon: IconInstagram,
					},
				],
			},
		];
	};
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return { userAuthenticated, userData };
}

export default connect(mapStateToProps)(Footer);
