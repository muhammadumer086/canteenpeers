const defaultBlogFeaturedImage =`${process.env.STATIC_PATH}/images/blog-user_featured.jpg`;

export default defaultBlogFeaturedImage;
