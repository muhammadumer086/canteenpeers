import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import moment from 'moment';
import Button from '@material-ui/core/Button';

import { IApiUserActivity } from '../../../interfaces';
import { processQueryParams } from '../../../helpers/processQueryParams';

import { apiError, userPanelDisplay } from '../../../redux/store';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import HeroHeading from '../../../components/general/HeroHeading';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import AdminTable, {
	IAdminTableColumn,
} from '../../../components/admin/AdminTable';
import {
	IAdminProps,
	IAdminState,
} from '../../../components/admin/withAdminPage';
import {
	ColumnActivityUser,
	ColumnType,
	ColumnTitle,
	ColumnActivityTime,
	ColumnSituations,
	ColumnTopic,
	ColumnViewsCount,
	ColumnGetItCount,
	ColumnHugCount,
	ColumnRepliesCount,
	ColumnAdditionalData,
	ColumnPlatform,
} from '../../../components/admin/AdminTableColumns';
import AdminFilter from '../../../components/admin/AdminFilter';
import AdminFilterActivities from '../../../components/admin/AdminFilterActivities';
import { downloadCsvFile } from '../../../helpers/downloadFile';
import { checkPermission } from "../../../helpers/checkPermission";
export enum EActivitiesPermissions{
	READ="all_users_activities_read",
	ADD="all_users_activities_add",
	EDIT="all_users_activities_edit",
	DELETE="all_users_activities_delete",
	ALL="all_users_activities_all",
}
interface IProps extends IAdminProps { }

interface IState extends IAdminState {
	data: IApiUserActivity[];
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	isLoadingUsers: boolean;
	filterOpen: boolean;
	filterValues: any;
	reportGenerating: boolean;
	isLoadingPermission: boolean;
	permissions: string[];
}

class Activities extends React.Component<IProps, IState> {
	protected tableColumns: IAdminTableColumn[] = [
		{
			header: 'User',
			size: 20,
			renderer: ColumnActivityUser,
		},
		{
			header: 'Activity Type',
			size: 20,
			renderer: ColumnType,
			sortable: 'type',
		},
		{
			header: 'Title',
			size: 20,
			renderer: ColumnTitle,
		},
		{
			header: 'Date & Time of Activity',
			size: 14,
			renderer: ColumnActivityTime,
			sortable: 'created_at',
		},
		{
			header: 'Posted in Situation(s)',
			size: 15,
			renderer: ColumnSituations,
		},
		{
			header: 'Posted in Topic',
			size: 12,
			renderer: ColumnTopic,
		},
		{
			header: 'Post Views',
			size: 6,
			renderer: ColumnViewsCount,
		},
		{
			header: 'I Get It',
			size: 6,
			renderer: ColumnGetItCount,
		},
		{
			header: 'Hugs',
			size: 6,
			renderer: ColumnHugCount,
		},
		{
			header: 'Replies',
			size: 6,
			renderer: ColumnRepliesCount,
		},
		{
			header: 'Platform',
			size: 10,
			renderer: ColumnPlatform,
		},
		{
			header: 'Additional Data',
			size: 20,
			renderer: ColumnAdditionalData,
		},
	];

	protected originalFilterValues = {
		activities: [],
		user_id: '',
		name: '',
		username: '',
		email: '',
		age: [],
		state: [],
		gender: [],
		situations: [],
		cancerTypes: [],
		excludeAdmin: true,
		platformFilter: '',
		sort: 'created_at-desc',

	};

	constructor(props: IProps) {
		super(props);

		this.state = {
			data: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isLoadingUsers: false,
			filterOpen: false,
			filterValues: JSON.parse(JSON.stringify(this.originalFilterValues)),
			reportGenerating: false,
			isLoadingPermission: false,
			permissions: []
		};

		this.displayUser = this.displayUser.bind(this);
		this.loadData = this.loadData.bind(this);
		this.handleOpenFilter = this.handleOpenFilter.bind(this);
		this.handleCloseFilter = this.handleCloseFilter.bind(this);
		this.handleFilter = this.handleFilter.bind(this);
		this.handleClear = this.handleClear.bind(this);
		this.onGenerateReport = this.onGenerateReport.bind(this);
		this.onUpdateSort = this.onUpdateSort.bind(this);
		this.initialise=this.initialise.bind(this);

	}

	public componentDidMount() {
		const readPermission : boolean=this.props.roleName==="admin"?true: checkPermission(this.props.permissions,EActivitiesPermissions.READ,EActivitiesPermissions.ALL);
		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		const readPermission : boolean=this.props.roleName==="admin"?true: checkPermission(this.props.permissions,EActivitiesPermissions.READ,EActivitiesPermissions.ALL);
		if (readPermission) {
			this.initialise(this.props, prevProps);
		}
	}

	public render() {
		const generateCsvPermission: boolean = this.props.roleName === "admin" || checkPermission(this.props.permissions, EActivitiesPermissions.ALL);

		const tableParams = {
			tableColumns: this.tableColumns,
			data: this.state.data,
			onRowClick: this.displayUser,
			loading: this.state.isLoadingUsers,
			onLoadMore:
				this.state.currentPage < this.state.maxPages
					? this.loadData
					: undefined,
			onGenerateReport:generateCsvPermission? this.onGenerateReport:undefined,
			reportGenerating: this.state.reportGenerating,
			totalMatchedRecords: this.state.total,
			sort: this.state.filterValues.sort,
			onUpdateSort: this.onUpdateSort,
		};
	
		let heroInfo = `Showing ${this.state.data.length} of ${this.state.total
			} activities`;

		if (this.state.isLoadingUsers && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}

		return (
			<div>
				<Head>
					<title>User Activity Log — {process.env.APP_NAME}</title>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent theme-background--gradient">
						<HeroHeading
							title="Activities Log"
							subInfo={heroInfo}
						/>
						<span className="hero-actions">
							<Button
								variant="outlined"
								color="primary"
								onClick={this.handleOpenFilter}
							>
								Filter Activities
							</Button>
						</span>
					</header>
					<div className="admin_content">
						<AdminTable {...tableParams} />
					</div>
					{this.renderFilters()}
				</LayoutDashboard>
			</div>
		);
	}

	protected renderFilters() {
		return (
			<AdminFilter
				isOpen={this.state.filterOpen}
				handleCloseFilter={this.handleCloseFilter}
			>
				<AdminFilterActivities
					showUserFilter={true}
					initialValues={this.state.filterValues}
					handleFilter={this.handleFilter}
					handleClear={this.handleClear}
				/>
			</AdminFilter>
		);
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(props.permissions,EActivitiesPermissions.ALL,EActivitiesPermissions.READ);
		const permissions=prevProps?prevProps.permissions:[];
		const prevReadPermission: boolean =this.props.roleName==="admin"|| checkPermission(permissions,EActivitiesPermissions.ALL,EActivitiesPermissions.READ);
		
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin||(!prevReadPermission&&readPermission))
		) {
			this.loadData(false);
		}
	}

	protected loadData(append: boolean = true) {
		if (
			!this.state.isLoadingUsers &&
			this.state.currentPage < this.state.maxPages
		) {
			let params: any = processQueryParams(
				JSON.parse(JSON.stringify(this.state.filterValues)),
			);

			if (append) {
				params.page = this.state.currentPage + 1;
			}

			const url = buildUrl(null, {
				path: `/api/admin/activities`,
				queryParams: params,
			});
			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'user_activities' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								let data: IApiUserActivity[] =
									response.data.user_activities;

								if (append) {
									data = JSON.parse(
										JSON.stringify(this.state.data),
									);
									(response.data
										.user_activities as IApiUserActivity[]).forEach(
											item => {
												data.push(item);
											},
										);
								}

								this.setState({
									data,
									isLoadingUsers: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
								});
							}
						}).catch(e=>{
							this.props.dispatch(apiError([e.message]))
							this.setState({isLoadingUsers : false})
						});
				},
			);
		}
	}

	protected displayUser(activity: IApiUserActivity) {
		if (activity.user) {
			this.props.dispatch(userPanelDisplay(activity.user));
		}
	}

	protected handleOpenFilter() {
		this.setState({
			filterOpen: true,
		});
	}
	protected handleCloseFilter() {
		this.setState({
			filterOpen: false,
		});
	}

	protected handleFilter(values: any) {
		this.setState(
			{
				filterOpen: false,
				filterValues: values,
				data: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadData(false);
			},
		);
	}

	protected handleClear() {
		this.handleFilter(
			JSON.parse(JSON.stringify(this.originalFilterValues)),
		);
	}

	protected onGenerateReport() {
		this.setState(
			{
				reportGenerating: true,
			},
			() => {
				const data = Object.assign({}, this.state.filterValues);

				let params: any = processQueryParams(data);

				const url = buildUrl(null, {
					path: `/api/admin/activities/export`,
					queryParams: params,
				});

				const fileName =
					`canteen-activities-report-` +
					moment().format('YYYYMMDD-HHmm') +
					'.csv';

				downloadCsvFile(this.props.apiService, url, fileName).then(
					() => {
						this.setState({
							reportGenerating: false,
						});
					},
				);
			},
		);
	}

	protected onUpdateSort(sortable: string, direction: 'asc' | 'desc') {
		this.setState(
			{
				filterValues: Object.assign(this.state.filterValues, {
					sort: `${sortable}-${direction}`,
				}),
				data: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadData(false);
			},
		);
	}
	
}

export default Activities;
