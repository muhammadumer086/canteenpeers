import React from 'react';
import ReactSVG from 'react-svg';
import { IApiEvent } from '../../interfaces';

interface IProps {
	address: string;
	event: IApiEvent;
}

const EventAddress: React.StatelessComponent<IProps> = props => {
	
	return (
		<div className="event_single-time_date">
			<div className="event_single-icon">
				<ReactSVG
					className="event_single-sidebar-icon"
					path={`${process.env.STATIC_PATH}/icons/location.svg`}
				/>
			</div>

			<div className="event_address">
				{props.event.state.slug==="online"?	<span className="font--body theme-title event_single-time_details_text" >online</span> : 
				props.event.location && (
					<span className="font--body theme-title event_single-time_details_text" >
						<a

							data-latitude={
								props.event
									.latitude
							}
							data-longitude={
								props.event
									.longitude
							}
							title="View map"
							target="_blank"
							className=""
							href={`https://google.com/maps/place/${props.event.latitude},${props.event.longitude}`}
						>
							{props.event.location}
						</a>
					</span>
				)}
			</div>
		</div>
	);
};

export default EventAddress;
