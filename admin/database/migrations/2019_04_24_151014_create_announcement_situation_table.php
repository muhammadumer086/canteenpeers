<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementSituationTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('announcement_situation', function (Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('announcement_id');
			$table
				->foreign('announcement_id')
				->references('id')
				->on('announcements')
				->onDelete('cascade');

			$table->unsignedInteger('situation_id');
			$table
				->foreign('situation_id')
				->references('id')
				->on('situations');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('announcement_situation');
	}
}
