import React from 'react';
import { withStyles } from '@material-ui/core/styles';

interface IProps {
	type: string;
	label: string;
	name: string;
	values: any;
	disabled?: boolean;
	multiline?: boolean;
	placeholder?: string;
	setFieldValue?(key: string, validate: any): void;
}

const styles = {
	root: {
		width: '100%',
	},
	input: {
		padding: '0px',
		borderRadius: '50px',
		backgroundColor: 'white',
	},
	multiline: {
		padding: '0',
	},
};

const MessageReplyTextFieldFormik: React.FC<IProps> = (props: IProps) => {
	const autoExpand = field => {
		// Reset field height
		field.style.height = 'inherit';

		// Get the computed styles for the element
		let computed = window.getComputedStyle(field);

		// Calculate the height
		let height =
			parseInt(computed.getPropertyValue('border-top-width'), 10) +
			//+ parseInt(computed.getPropertyValue('padding-top'), 10)
			field.scrollHeight +
			// + parseInt(computed.getPropertyValue('padding-bottom'), 10)
			parseInt(computed.getPropertyValue('border-bottom-width'), 10);

		// field.style.height = ( height / 100 * 70 ) + 'px';
		field.style.height = height + 'px';
	};

	const handleChange = (event: React.ChangeEvent<any>) => {
		const message = event.target.value;

		autoExpand(event.target);
		props.setFieldValue(props.name, message);
	};

	const rows = 1;

	return (
		<div className="form-input--textarea">
			<textarea
				id="form-textarea"
				name={props.name}
				rows={rows}
				onChange={handleChange}
				value={props.values[props.name]}
				placeholder={props.placeholder}
			/>
		</div>
	);
};

export default withStyles(styles)(MessageReplyTextFieldFormik);
