<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use Carbon\Carbon;

use App\User;
use App\UserActivity;
use App\Helpers\CampaignMonitorSubscription;

class UserActivityHelper {
	public function create(User $user, $model, string $type, $content = null) {
		// Create a user activity
		$userActivity = new UserActivity();
		$userActivity->user()->associate($user);
		$userActivity->activity()->associate($model);
		$userActivity->type = $type;
		$userActivity->content = $content;
		$userActivity->platform = $this->getPlatform();
		$userActivity->save();

		if ($user->allowNews()) {
			$subscription = new CampaignMonitorSubscription();
			// Update campaign monitor
			$subscription->subscribeUserToCampaignMonitor(
				$user->email,
				$user->first_name,
				$user->last_name,
				$user->location_data,
				$user->dob,
				$user->situations,
				true,
				$user->verified
			);
		}
	}

	protected function getPlatform() {
		$platform = request('platform', 'web');
		if ($platform !== 'web' && $platform !== 'app') {
			$platform = 'web';
		}
		return $platform;
	}
}
