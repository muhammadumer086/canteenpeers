<?php

namespace App\Observers;

use UserActivityHelper;

use App\Blog;

class BlogObserver {
	/**
	 * Handle the blog "created" event.
	 *
	 * @param  \App\Blog  $blog
	 * @return void
	 */
	public function created(Blog $blog) {
		//
	}

	/**
	 * Handle the blog "updated" event.
	 *
	 * @param  \App\Blog  $blog
	 * @return void
	 */
	public function updated(Blog $blog) {
		//
	}

	/**
	 * Handle the blog "deleted" event.
	 *
	 * @param  \App\Blog  $blog
	 * @return void
	 */
	public function deleted(Blog $blog) {
		// Handled in the "destroy" function of the Blog controller
	}

	/**
	 * Handle the blog "restored" event.
	 *
	 * @param  \App\Blog  $blog
	 * @return void
	 */
	public function restored(Blog $blog) {
		if ($blog->source !== 'prismic' && $blog->user) {
			UserActivityHelper::create(
				$blog->user,
				$blog,
				'Blog Restored',
				null
			);
		}
	}

	/**
	 * Handle the blog "force deleted" event.
	 *
	 * @param  \App\Blog  $blog
	 * @return void
	 */
	public function forceDeleted(Blog $blog) {
		//
	}
}
