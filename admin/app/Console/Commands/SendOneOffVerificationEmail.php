<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\User;

class SendOneOffVerificationEmail extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:sendOneOffVerificationEmail {userId? : The user id to send it to}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send a one-off verification email to the users that haven\'t verified their account';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$usersQuery = User::where('verified', false);
		if ($this->argument('userId')) {
			$usersQuery = User::where('id', $this->argument('userId'));
		}

		$auth = [
			'api_key' => config('canteen.campaign_monitor_key'),
		];
		$smartEmailId = '7457548a-2d23-4f9b-b9b7-221ef54c2240';
		$wrap = new \CS_REST_Transactional_SmartEmail($smartEmailId, $auth);

		$bar = $this->output->createProgressBar($usersQuery->count());
		// Remove all the thread id
		$bar->start();
		$bar->setRedrawFrequency(10);

		$usersQuery->chunk(10, function ($users) use ($wrap, $bar) {
			foreach ($users as $user) {
				$message = [
					'To' => "{$user->first_name} {$user->last_name} <{$user->email}>",
					'Data' => [
						'firstname' => $user->first_name,
						'button' => config('canteen.facade_url') . '/auth/verify?' . http_build_query([
							'token' => $user->verification_token,
						])
					],
				];

				$consentToTrack = 'unchanged';

				$wrap->send($message, $consentToTrack);
				$bar->advance();
			}
		});

		$bar->finish();
		$this->line("\r\nFinished sending verification emails");
	}
}
