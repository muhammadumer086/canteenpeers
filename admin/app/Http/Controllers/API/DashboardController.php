<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use App\Announcement;
use App\AgeRange;
use App\Blog;
use App\Discussion;
use App\Event;
use App\Link;
use App\Resource;
use App\Situation;
use App\State;
use App\Topic;
use App\User;

use App\Http\Resources\Announcement as AnnouncementResource;
use App\Http\Resources\Blog as BlogResource;
use App\Http\Resources\Discussion as DiscussionResource;
use App\Http\Resources\Event as EventResource;
use App\Http\Resources\Link as LinkResource;
use App\Http\Resources\Resources as ResourcesResource;

use Illuminate\Support\Facades\Log;

class DashboardController extends Controller {
	/**
	 * Display the data for the dashboard
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);

		$announcement = null;
		$discussionsQuery = Discussion::latest('last_activity_at')->whereNull(
			'main_discussion_id'
		)->where('for_event', false);
		$suggestedForYouQuery = Link::latest();
		$blogsQuery = Blog::orderBy('last_publication_date', 'DESC');
		$resourcesQuery = Resource::orderBy('last_publication_date', 'DESC');

		if ($authUser && !$authUser->hasRoleName('admin')) {
			// Authenticated user's situation(s)
			$filterBySituation = new \App\Helpers\FilterByUserSituation();
			$situationIds = $filterBySituation->addSituationsMap(
				$authUser
					->situations()
					->pluck('name')
					->toArray()
			);

			// Relevant discussions
			$discussionsQuery->whereIn(
				'id',
				DB::table('discussion_situation')
					->whereIn('situation_id', $situationIds)
					->pluck('discussion_id')
			);

			// Relevant resources
			$resourcesQuery->whereIn(
				'id',
				DB::table('resource_situation')
					->whereIn('situation_id', $situationIds)
					->pluck('resource_id')
			);

			// Relevant blogs
			$blogsQuery->whereIn(
				'id',
				DB::table('blog_situation')
					->whereIn('situation_id', $situationIds)
					->pluck('blog_id')
			);

			$announcement = $this->selectUserAnnouncement(
				$authUser,
				$situationIds
			);
		} elseif ($authUser && $authUser->hasRoleName('admin')) {
			$announcement = Announcement::latest()
				->where('public', false)
				->first();
		} elseif (!$authUser) {
			// Query the latest public announcement
			$announcement = Announcement::latest()
				->where('public', true)
				->first();

			$discussionsQuery->where('private', false);
		}

		$response = [
			'announcement' => $announcement
				? new AnnouncementResource($announcement)
				: null,
			'discussions' => DiscussionResource::collection(
				$discussionsQuery->limit(3)->get()
			),
			'links' => LinkResource::collection(
				$suggestedForYouQuery->limit(5)->get()
			),
			'events' => EventResource::collection(
				$this->selectUserEvents($authUser)
			),
			'blogs' => BlogResource::collection($blogsQuery->limit(3)->get()),
			'resources' => ResourcesResource::collection(
				$resourcesQuery->limit(3)->get()
			),
		];

		return response()->json($response);
	}

	protected function selectUserEvents($authUser) {
		$limitEvents = 2;

		$eventsIds = [];

		if ($authUser) {
			// Query by state if possible
			if ($authUser->state) {
				$eventsQuery = $this->getEventsBaseQuery($limitEvents);
				$eventsQuery->whereHas('state', function ($query) use (
					$authUser
				) {
					$slugs = [strtolower($authUser->state)];
					if ($authUser->state === 'QLD') {
						$slugs[] = 'qld-north';
					}
					$query->whereIn('slug', $slugs);
				});
				$eventsIds = array_merge(
					$eventsIds,
					$eventsQuery->pluck('id')->toArray()
				);
			} elseif (
				$authUser->country_slug &&
				$authUser->country_slug === 'NZ'
			) {
				$eventsQuery = $this->getEventsBaseQuery($limitEvents);
				$eventsQuery->whereHas('state', function ($query) {
					$query->where('slug', 'nz');
				});
				$eventsIds = array_merge(
					$eventsIds,
					$eventsQuery->pluck('id')->toArray()
				);
			}

			// Nation-wide (AU users)
			if (
				sizeof($eventsIds) < $limitEvents &&
				$authUser->country_slug &&
				$authUser->country_slug === 'AU'
			) {
				$eventsQuery = $this->getEventsBaseQuery($limitEvents);
				$eventsQuery->whereHas('state', function ($query) {
					$query->whereIn('slug', [
						'nsw',
						'qld',
						'qld-north',
						'sa',
						'tas',
						'vic',
						'wa',
						'act',
						'nt',
					]);
				});

				$eventsIds = array_merge(
					$eventsIds,
					$eventsQuery->pluck('id')->toArray()
				);
			}

			// Online
			if (sizeof($eventsIds) < $limitEvents) {
				$eventsQuery = $this->getEventsBaseQuery($limitEvents);
				$eventsQuery->whereHas('state', function ($query) {
					$query->where('slug', 'online');
				});

				$eventsIds = array_merge(
					$eventsIds,
					$eventsQuery->pluck('id')->toArray()
				);
			}

			// Anything
			if (sizeof($eventsIds) < $limitEvents) {
				$eventsQuery = $this->getEventsBaseQuery($limitEvents);
				$eventsIds = array_merge(
					$eventsIds,
					$eventsQuery->pluck('id')->toArray()
				);
			}

			return Event::orderBy('start_date', 'ASC')
				->whereIn('id', $eventsIds)
				->limit($limitEvents)
				->get();
		}

		$eventsQuery = $this->getEventsBaseQuery($limitEvents);
		return $eventsQuery->get();
	}

	protected function getEventsBaseQuery($limitEvents) {
		$now = Carbon::today();
		return Event::orderBy('start_date', 'ASC')
			->where('start_date', '>', $now)
			->limit($limitEvents);
	}

	protected function selectUserAnnouncement(User $authUser, $situationIds) {
		$ageRange = null;
		$state = null;

		if ($authUser->dob) {
			$userAge = Carbon::today()->diffInYears($authUser->dob);
			$ageRangeQuery = null;
			if ($userAge < 14) {
				$ageRangeQuery = AgeRange::where('max', 14);
			} elseif ($userAge >= 20) {
				$ageRangeQuery = AgeRange::where('min', 20);
			} else {
				$ageRangeQuery = AgeRange::where('min', '<=', $userAge)->where(
					'max',
					'>=',
					$userAge
				);
			}

			$ageRange = $ageRangeQuery->first();
		}

		if ($authUser->state) {
			$state = State::where('abbreviation', $authUser->state)->first();
		} elseif ($authUser->country_slug && $authUser->country_slug === 'NZ') {
			$state = State::where(
				'abbreviation',
				$authUser->country_slug
			)->first();
		}

		// Try to match all the conditions
		$query = $this->getBaseAnnouncementQuery($authUser);
		$query = $this->addAnnouncementSituationsQuery($query, $situationIds);
		$query = $this->addAnnouncementAgeRangeQuery($query, $ageRange);
		$query = $this->addAnnouncementStateQuery($query, $state);
		$announcement = $query->first();

		if (!$announcement) {
			// Try to match only situation and age range
			$query = $this->getBaseAnnouncementQuery($authUser);
			$query = $this->addAnnouncementSituationsQuery(
				$query,
				$situationIds
			);
			$query = $this->addAnnouncementAgeRangeQuery($query, $ageRange);
			$query = $this->addAnnouncementNoStateQuery($query);
			$announcement = $query->first();
			if (!$announcement) {
				// Try to match situations and state
				$query = $this->getBaseAnnouncementQuery($authUser);
				$query = $this->addAnnouncementSituationsQuery(
					$query,
					$situationIds
				);
				$query = $this->addAnnouncementNoAgeRangeQuery($query);
				$query = $this->addAnnouncementStateQuery($query, $state);
				$announcement = $query->first();
				if (!$announcement) {
					// Try to match only situations
					$query = $this->getBaseAnnouncementQuery($authUser);
					$query = $this->addAnnouncementSituationsQuery(
						$query,
						$situationIds
					);
					$query = $this->addAnnouncementNoAgeRangeQuery($query);
					$query = $this->addAnnouncementNoStateQuery($query);
					$announcement = $query->first();
					if (!$announcement) {
						// Try to match age and state
						$query = $this->getBaseAnnouncementQuery($authUser);
						$query = $this->addAnnouncementNoSituationsQuery(
							$query
						);
						$query = $this->addAnnouncementAgeRangeQuery(
							$query,
							$ageRange
						);
						$query = $this->addAnnouncementStateQuery(
							$query,
							$state
						);
						$announcement = $query->first();
						if (!$announcement) {
							// Try to match age only
							$query = $this->getBaseAnnouncementQuery($authUser);
							$query = $this->addAnnouncementNoSituationsQuery(
								$query
							);
							$query = $this->addAnnouncementAgeRangeQuery(
								$query,
								$ageRange
							);
							$query = $this->addAnnouncementNoStateQuery($query);
							$announcement = $query->first();
							if (!$announcement) {
								// Try to match state only
								$query = $this->getBaseAnnouncementQuery(
									$authUser
								);
								$query = $this->addAnnouncementNoSituationsQuery(
									$query
								);
								$query = $this->addAnnouncementNoAgeRangeQuery(
									$query
								);
								$query = $this->addAnnouncementStateQuery(
									$query,
									$state
								);
								$announcement = $query->first();
								if (!$announcement) {
									// Nothing to match
									$query = $this->getBaseAnnouncementQuery(
										$authUser
									);
									$query = $this->addAnnouncementNoSituationsQuery(
										$query
									);
									$query = $this->addAnnouncementNoAgeRangeQuery(
										$query
									);
									$query = $this->addAnnouncementNoStateQuery(
										$query
									);
									$announcement = $query->first();
								}
							}
						}
					}
				}
			}
		}

		return $announcement;
	}

	protected function getBaseAnnouncementQuery($authUser) {
		return Announcement::latest()
			->where('public', false)
			->whereDoesntHave('users', function ($query) use ($authUser) {
				$query->whereIn('user_id', [$authUser->id]);
			});
	}

	protected function addAnnouncementSituationsQuery($query, $situationIds) {
		return $query->whereIn(
			'id',
			DB::table('announcement_situation')
				->whereIn('situation_id', $situationIds)
				->pluck('announcement_id')
		);
	}

	protected function addAnnouncementNoSituationsQuery($query) {
		return $query->doesntHave('situations');
	}

	protected function addAnnouncementAgeRangeQuery($query, $ageRange) {
		if ($ageRange) {
			return $query->whereIn(
				'id',
				DB::table('age_range_announcement')
					->where('age_range_id', $ageRange->id)
					->pluck('announcement_id')
			);
		}
		return $this->addAnnouncementNoAgeRangeQuery($query);
	}

	protected function addAnnouncementNoAgeRangeQuery($query) {
		return $query->doesntHave('ageRanges');
	}

	protected function addAnnouncementStateQuery($query, $state) {
		if ($state) {
			return $query->whereIn(
				'id',
				DB::table('announcement_state')
					->where('state_id', $state->id)
					->pluck('announcement_id')
			);
		}
		return $this->addAnnouncementNoStateQuery($query);
	}

	protected function addAnnouncementNoStateQuery($query) {
		return $query->doesntHave('states');
	}
}
