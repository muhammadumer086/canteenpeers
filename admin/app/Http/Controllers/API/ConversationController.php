<?php

namespace App\Http\Controllers\API;

use App\Conversation;
use App\Message;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use App\Notifications\MessageReceivedNotification;
use App\NotificationMessageCooldown;

use App\Http\Resources\Conversation as ConversationResource;
use App\Http\Resources\ConversationCollection;
use App\Http\Resources\MessageCollection;

use Carbon\Carbon;

class ConversationController extends Controller {
	const PAGINATION_SIZE = 20;

	/**
	 * Get all conversations a user is in
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);

		$conversations = $authUser
			->conversations()
			->orderBy('updated_at', 'DESC')
			->paginate(self::PAGINATION_SIZE);

		return new ConversationCollection($conversations);
	}

	/**
	 * Create a new discussion.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);

		if ($authUser->underage) {
			return response()->json(
				[
					'error' => 'You need to be above 16 to send a message.',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'users' => 'bail|required|array|min:1|max:100',
			'users.*' => [
				'bail',
				'required',
				'string',
				'distinct',
				Rule::notIn([$authUser->username]),
				'exists:users,username',
			],
			'message' => 'bail|required|string|max:16000000',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Get the user ids for the conversation
		$userIds = [$authUser->id];
		foreach ($request->input('users') as $singleUsername) {
			$tmpUser = User::where('username', $singleUsername)->first();

			if (!$tmpUser) {
				return response()->json(
					[
						'error' => "User is inactive or doesn't exist.",
					],
					400
				);
			}

			$blockedUsers = $authUser->blockedUsers()->get();
			$usersBlocked = $tmpUser->blockedUsers()->get();

			foreach ($blockedUsers as $blockedUser) {
				if ($tmpUser->id === $blockedUser->id) {
					return response()->json(
						[
							'error' => 'User blocked.',
						],
						400
					);
				}
			}

			foreach ($usersBlocked as $userBlocked) {
				if ($authUser->id === $userBlocked->id) {
					return response()->json(
						[
							'error' => 'User blocked.',
						],
						400
					);
				}
			}

			if ($tmpUser->inactive) {
				return response()->json(
					[
						'error' => 'User is inactive.',
					],
					400
				);
			}

			if (!$authUser->hasRoleName('admin') && $tmpUser->underage) {
				return response()->json(
					[
						'error' => "You can't message {$tmpUser->full_name} directly",
					],
					400
				);
			}

			if (
				!$authUser->hasRoleName('admin') &&
				(!User::where('username', $singleUsername)->exists() ||
					!$tmpUser->allowDirectMessage())
			) {
				return response()->json(
					[
						'error' => "{$tmpUser->full_name} doesn't allow to be contacted directly",
					],
					400
				);
			}
			$userIds[] = User::where('username', $singleUsername)->value('id');
		}

		// De-duplicate the user ids
		$userIds = array_unique($userIds);

		// Create the conversation
		if (sizeof($userIds) > 2) {
			$conversation = new Conversation();
			$conversation->group_conversation = true;
			$conversation->save();
		} else {
			// Single conversation, make sure no private conversation already exist
			$conversation = $authUser
				->conversations()
				->where('group_conversation', false)
				->whereHas('users', function ($query) use (
					$userIds,
					$authUser
				) {
					if (
						($key = array_search($authUser->id, $userIds)) !== false
					) {
						unset($userIds[$key]);
					}
					$query->whereIn('user_id', $userIds);
				})
				->first();

			if (!$conversation) {
				$conversation = new Conversation();
				$conversation->group_conversation = false;
				$conversation->save();
			}
		}

		// Sync the users id
		$conversation->users()->sync($userIds);

		// Create a system message to start the conversation
		$this->addSystemMessageToConversation(
			$conversation,
			$authUser,
			'%author% created the conversation'
		);

		// Create the message
		$this->addMessageToConversation(
			$conversation,
			$authUser,
			$request->input('message')
		);

		return response()->json([
			'conversation' => new ConversationResource($conversation),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Conversation  $conversation
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Conversation $conversation) {
		$authUser = $this->getAuthUser($request);

		if (!$this->userInConversation($authUser->id, $conversation->id)) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		$validator = Validator::make($request->all(), [
			'users' => 'bail|nullable|array|min:1|max:100',
			'users.*' => [
				'bail',
				'required',
				'string',
				'distinct',
				'exists:users,username',
			],
			'name' => 'bail|nullable|string|max:191',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Handle the name change
		if ($request->has('name')) {
			$newConversationName = !empty($request->input('name'))
				? $request->input('name')
				: null;

			if ($conversation->name !== $newConversationName) {
				$conversation->name = $newConversationName;
				if ($newConversationName) {
					$this->addSystemMessageToConversation(
						$conversation,
						$authUser,
						"%author% changed the conversation name to \"{$newConversationName}\""
					);
				} else {
					$this->addSystemMessageToConversation(
						$conversation,
						$authUser,
						'%author% removed the conversation name'
					);
				}
				$conversation->save();
			}
		}

		// Handle the users change
		if ($request->has('users') && $conversation->group_conversation) {
			// Get the list of current users
			$currentUserIds = $conversation
				->users()
				->pluck('users.id')
				->toArray();
			sort($currentUserIds);
			$currentUserIds = array_unique($currentUserIds);

			// Get the list of updated users
			$updatedUserIds = User::whereIn(
				'username',
				$request->input('users')
			)
				->pluck('id')
				->toArray();
			$updatedUserIds[] = $authUser->id;
			sort($updatedUserIds);
			$updatedUserIds = array_unique($updatedUserIds);

			// Perform a diff to know which user has been removed
			$removedUserIds = array_values(
				array_diff($currentUserIds, $updatedUserIds)
			);
			if (sizeof($removedUserIds)) {
				// Add the system message
				$this->addSystemMessageToConversation(
					$conversation,
					$authUser,
					'%author% removed %targets% from the conversation',
					$removedUserIds
				);
			}

			// Perform a diff to know which user has been added
			$addedUserIds = array_values(
				array_diff($updatedUserIds, $currentUserIds)
			);
			if (sizeof($addedUserIds)) {
				// Add the system message
				$this->addSystemMessageToConversation(
					$conversation,
					$authUser,
					'%author% added %targets% to the conversation',
					$addedUserIds
				);
			}
			if (sizeof($removedUserIds) || sizeof($addedUserIds)) {
				$conversation->users()->sync($updatedUserIds);
			}
		}

		return new ConversationResource($conversation);
	}

	/**
	 * Get single conversation
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function show($conversationId, Request $request) {
		$authUser = $this->getAuthUser($request);
		$conversation = Conversation::findOrFail($conversationId);

		$valid = $this->userInConversation($authUser->id, $conversationId);
		if (!$valid) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		// Mark the user's notifications for the conversation as read

		return new ConversationResource($conversation);
	}

	public function leaveConversation(Request $request, $conversationId) {
		$authUser = $this->getAuthUser($request);
		$conversation = Conversation::findOrFail($conversationId);

		if (!$this->userInConversation($authUser->id, $conversation->id)) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		// Remove the user from the list of users in conversation
		$conversation->users()->detach($authUser->id);

		$this->addSystemMessageToConversation(
			$conversation,
			$authUser,
			'%author% left the conversation'
		);

		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Get a conversation replies
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function replies($conversationId, Request $request) {
		$authUser = $this->getAuthUser($request);
		$conversation = Conversation::findOrFail($conversationId);

		$valid = $this->userInConversation($authUser->id, $conversationId);
		if (!$valid) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		// Remove the notification for the user for the current conversation
		foreach (
			$authUser
				->unreadNotifications()
				->where('type', 'App\Notifications\MessageReceivedNotification')
				->get()
			as $notification
		) {
			$notificationData = $notification->data;
			if (
				isset(
					$notificationData['conversation'],
					$notificationData['conversation']['id']
				) &&
				(int) $notificationData['conversation']['id'] ===
					$conversation->id
			) {
				$notification->markAsRead();
			}
		}

		$messages = $conversation
			->messages()
			->orderBy('updated_at', 'DESC')
			->orderBy('id', 'DESC')
			->paginate(self::PAGINATION_SIZE);

		return new MessageCollection($messages);
	}

	/**
	 * Get a conversation replies
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function storeReply($conversationId, Request $request) {
		$authUser = $this->getAuthUser($request);
		$conversation = Conversation::findOrFail($conversationId);

		if (
			$this->isConversationWithUnderage($conversation) &&
			!$this->isConversationWithAdmin($conversation)
		) {
			return response()->json(
				[
					'error' => 'No admin is present in this conversation',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'message' => 'bail|required|string|max:16000000',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$valid = $this->userInConversation($authUser->id, $conversationId);
		if (!$valid) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		if (!$this->checkIfNotInactiveUsers($conversation, $authUser)) {
			return response()->json(
				[
					'error' => 'Inactive user(s) in the conversation',
				],
				400
			);
		}

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();
		$messageContent = $richTextFilter->filter($request->input('message'));

		if (empty($messageContent)) {
			return response()->json(
				[
					'error' => "Message can't be empty",
				],
				400
			);
		}

		$this->addMessageToConversation(
			$conversation,
			$authUser,
			$messageContent
		);

		return response()->json([
			'conversation' => new ConversationResource($conversation),
		]);
	}

	protected function isConversationWithUnderage(Conversation $conversation) {
		foreach ($conversation->users as $user) {
			if ($user->underage) {
				return true;
			}
		}
		return false;
	}

	protected function isConversationWithAdmin(Conversation $conversation) {
		foreach ($conversation->users as $user) {
			if ($user->hasRoleName('admin')) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Add a message to a conversation
	 */
	protected function addMessageToConversation(
		Conversation $conversation,
		User $authUser,
		string $messageContent
	) {
		$message = new Message();
		$message->conversation()->associate($conversation);
		$message->user()->associate($authUser);
		$message->message = $messageContent;
		$message->save();

		// Notify the other users
		foreach ($conversation->users as $user) {
			if ($user->id !== $authUser->id) {
				// Create a notification cooldown if necessary
				if (
					!$user
						->notificationMessageCooldowns()
						->where('created_at', '>=', Carbon::now()->subHours(1))
						->exists()
				) {
					// Create the notification meesage cooldown
					$user->notificationMessageCooldowns()->create();
				}

				$user->notify(new MessageReceivedNotification($message));
			}
		}
	}

	/**
	 * Add a system message to a conversation
	 */
	protected function addSystemMessageToConversation(
		Conversation $conversation,
		User $authUser,
		string $messageContent,
		array $userIdTargets = null
	) {
		$message = new Message();
		$message->conversation()->associate($conversation);
		$message->user()->associate($authUser);
		$message->message = $messageContent;
		$message->system_generated = true;
		$message->save();

		if ($userIdTargets && sizeof($userIdTargets)) {
			$message->targetUsers()->sync($userIdTargets);
		}
	}

	/**
	 * Check if a user is in conversation
	 *
	 * @param string $userId
	 * @param string $conversationId
	 * @return boolean $exists
	 */
	protected function userInConversation($userId, $conversationId) {
		$exists = DB::table('conversation_user')
			->where('conversation_id', $conversationId)
			->where('user_id', $userId)
			->exists();

		return $exists;
	}

	/**
	 * Check if the other user(s) in a conversation aren't disabled
	 *
	 * @param Conversation $conversation
	 * @param User $authUser
	 * @return boolean $canMessage
	 */
	protected function checkIfNotInactiveUsers(
		Conversation $conversation,
		User $authUser
	) {
		foreach ($conversation->users as $user) {
			if ($user->id !== $authUser->id && !$user->inactive) {
				return true;
			}
		}

		return false;
	}
}