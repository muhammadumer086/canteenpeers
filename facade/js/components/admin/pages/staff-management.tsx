import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';

import { Button } from '@material-ui/core';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import HeroHeading from '../../../components/general/HeroHeading';
import ModalDialogDeleteConfirm from '../../modals/ModalDialogDeleteConfirm';

import {
	StaffColumnName,
	StaffColumnRole,
	StaffColumnBiography,
	StaffColumnTeam,
} from '../../../components/admin/staff/AdminStaffColumns';

import {
	IFormValues,
	CreateStaffFormik,
	EditStaffFormik,
} from '../../../components/admin/staff/forms';

import { ModalDialog } from '../../modals/local';

import AdminTable, {
	IAdminTableColumn,
} from '../../../components/admin/AdminTable';

import { IAdminProps } from '../../../components/admin/withAdminPage';

import { apiError, apiSuccess } from '../../../redux/store';
import { IApiStaff, IApiTeam } from '../../../interfaces';
import { checkPermission } from "../../../helpers/checkPermission";
export enum EStaffManagementPermissions{
	READ="staff_management_read",
	ADD="staff_management_add",
	EDIT="staff_management_edit",
	DELETE="staff_management_delete",
	ALL="staff_management_all",
}
interface IProps extends IAdminProps {}

interface IState {
	data: IApiStaff[];
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	showingStaff: string;
	isSubmitting: boolean;
	isCreateStaffModalActive: boolean;
	isEditStaffModalActive: boolean;
	staffMemberToDelete: IApiStaff | null;
	staffMemberToEdit: IApiStaff | null;
	isDeleteStaffModalActive: boolean;
	teams: IApiTeam[] | null;
	permissions: string[];
	isLoadingPermission: boolean;
}

class StaffManagement extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			data: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isSubmitting: false,
			isCreateStaffModalActive: false,
			isEditStaffModalActive: false,
			isDeleteStaffModalActive: false,
			showingStaff: '',
			staffMemberToDelete: null,
			staffMemberToEdit: null,
			teams: null,
			isLoadingPermission: false,
			permissions: [],
		};

		this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
		this.setUserToDelete = this.setUserToDelete.bind(this);
		this.toggleCreateStaffModal = this.toggleCreateStaffModal.bind(this);
		this.toggleEditStaffModal = this.toggleEditStaffModal.bind(this);
		this.setUserToEdit = this.setUserToEdit.bind(this);

		this.loadStaffMembers = this.loadStaffMembers.bind(this);
		this.submitDeleteStaff = this.submitDeleteStaff.bind(this);
		this.submitCreateStaff = this.submitCreateStaff.bind(this);
		this.submitEditStaff = this.submitEditStaff.bind(this);
		this.initialise=this.initialise.bind(this);

	}

	protected tableColumns: IAdminTableColumn[] = [
		{
			header: 'Name',
			size: 20,
			renderer: StaffColumnName,
		},
		{
			header: 'Role',
			size: 20,
			renderer: StaffColumnRole,
		},
		{
			header: 'Team',
			size: 15,
			renderer: StaffColumnTeam,
		},
		{
			header: 'Biography',
			size: 20,
			renderer: StaffColumnBiography,
		},
	];

	public componentDidMount() {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.READ);
		
		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.READ);

		if (readPermission) {
			this.initialise(this.props, prevProps);
		}
	}

	public render() {

		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.READ);
		const addPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.ADD);
		const editPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.EDIT);
		const deletePermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.DELETE);
		console.log(this.props.permissions);
		console.log(addPermission,EStaffManagementPermissions.ADD);
		const tableParams = {
			tableColumns: this.tableColumns,
			data: this.state.data,
			onRowClick:editPermission? this.setUserToEdit:undefined,
			totalMatchedRecords: this.state.total,
			loading: this.state.isSubmitting,
			onDelete:deletePermission? this.setUserToDelete:undefined,
			onLoadMore:
				(this.state.currentPage < this.state.maxPages)&&readPermission
					? this.loadStaffMembers
					: undefined,
		};

		const { showingStaff, total } = this.state;

		let heroInfo =
			total !== 0
				? `Showing ${showingStaff} of ${total} staff members`
				: '';

		if (this.state.isSubmitting && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}
		

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading
							title="Staff Management"
							subInfo={heroInfo}
						/>
						{addPermission&&<span className="hero-actions">
							<Button
								variant="outlined"
								color="primary"
								onClick={this.toggleCreateStaffModal}
							>
								Add Staff Member
							</Button>
						</span>}
					</header>
					{readPermission&&<div className="admin_content">
						<AdminTable {...tableParams} />
					</div>}
				</LayoutDashboard>

				{this.state.isDeleteStaffModalActive &&
					!!this.state.staffMemberToDelete && (
						<ModalDialog
							isActive={this.state.isDeleteStaffModalActive}
							modalTitle="Are you sure you want to delete the below member of
						staff?"
							ariaTag="delete-staff-modal"
							handleClose={this.toggleDeleteModal}
						>
							<ModalDialogDeleteConfirm
								name={`${
									this.state.staffMemberToDelete.first_name
								}
								${this.state.staffMemberToDelete.last_name}`}
								loading={this.state.isSubmitting}
								submit={this.submitDeleteStaff}
								cancel={this.toggleDeleteModal}
							/>
						</ModalDialog>
					)}

				{this.state.isCreateStaffModalActive && (
					<ModalDialog
						isActive={this.state.isCreateStaffModalActive}
						modalTitle="Create a new staff member"
						ariaTag="create-staff-modal"
						handleClose={this.toggleCreateStaffModal}
					>
						<CreateStaffFormik
							isSubmitting={this.state.isSubmitting}
							teams={this.state.teams}
							onSubmit={this.submitCreateStaff}
						/>
					</ModalDialog>
				)}

				{this.state.isEditStaffModalActive && (
					<ModalDialog
						isActive={this.state.isEditStaffModalActive}
						modalTitle="Edit staff member"
						ariaTag="edit-staff-modal"
						handleClose={this.toggleEditStaffModal}
					>
						<EditStaffFormik
							isSubmitting={this.state.isSubmitting}
							values={this.state.staffMemberToEdit}
							teams={this.state.teams}
							onSubmit={this.submitEditStaff}
						/>
					</ModalDialog>
				)}
			</div>
		);
	}

	protected toggleCreateStaffModal(_event: React.ChangeEvent<any>) {
		this.setState({
			isCreateStaffModalActive: !this.state.isCreateStaffModalActive,
		});
	}

	protected toggleEditStaffModal() {
		this.setState({
			isEditStaffModalActive: !this.state.isEditStaffModalActive,
		});
	}

	protected setUserToEdit(staffMemberToEdit: IApiStaff | null = null) {
		this.setState(
			{
				staffMemberToEdit,
			},
			() => {
				this.toggleEditStaffModal();
			},
		);
	}

	protected toggleDeleteModal() {
		this.setState({
			isDeleteStaffModalActive: !this.state.isDeleteStaffModalActive,
		});
	}

	protected setUserToDelete(staffMemberToDelete: IApiStaff | null = null) {
		this.setState(
			{
				staffMemberToDelete,
			},
			() => {
				this.toggleDeleteModal();
			},
		);
	}

	protected submitCreateStaff(values: IFormValues) {
		this.setState(
			{
				isSubmitting: true,
			},
			async () => {
				const { staff, error } = await this.props.apiService.queryPOST(
					'/api/staff',
					values,
				);

				if (error) {
					this.props.dispatch(apiError([error]));
				}

				if (staff) {
					this.setState(
						{
							isSubmitting: false,
							isCreateStaffModalActive: false,
						},
						() => {
							this.props.dispatch(
								apiSuccess([
									'Staff member was created successfully',
								]),
							);

							this.loadStaffMembers(false);
						},
					);
				}
			},
		);
	}

	protected submitEditStaff(values: IFormValues) {
		if (this.state.staffMemberToEdit !== null) {
			this.setState(
				{
					isSubmitting: true,
				},
				async () => {
					const {
						data,
						error,
					} = await this.props.apiService.queryPUT(
						`/api/staff/${this.state.staffMemberToEdit.id}`,
						values,
					);

					if (data) {
						this.setState(
							{
								staffMemberToDelete: null,
								isDeleteStaffModalActive: false,
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										'Staff member successfully delete',
									]),
								);

								this.loadStaffMembers(false);
							},
						);
					} else if (error) {
						this.setState(
							{
								staffMemberToDelete: null,
								isDeleteStaffModalActive: false,
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									apiError([
										'There was an error editing the staff member',
									]),
								);
							},
						);
					}
				},
			);
		}
	}

	protected async submitDeleteStaff() {
		if (this.state.staffMemberToDelete !== null) {
			this.setState(
				{
					isSubmitting: true,
				},
				async () => {
					const { success } = await this.props.apiService.queryDELETE(
						`/api/staff/${this.state.staffMemberToDelete.id}`,
					);

					if (success) {
						this.setState(
							{
								staffMemberToDelete: null,
								isDeleteStaffModalActive: false,
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										'Staff member successfully delete',
									]),
								);

								this.loadStaffMembers(false);
							},
						);
					} else {
						this.setState(
							{
								staffMemberToDelete: null,
								isDeleteStaffModalActive: false,
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									apiError([
										'There was an error delete the staff memeber',
									]),
								);
							},
						);
					}
				},
			);
		}
	}

	protected loadStaffMembers(append: boolean = true) {
		if (!this.state.isSubmitting) {
			let params = {};

			if (append) {
				params['page'] = this.state.currentPage + 1;
			}

			params['all'] = 1;

			const url = buildUrl(null, {
				path: '/api/staff',
				queryParams: params,
			});
			this.setState(
				{
					isSubmitting: true,
					isCreateStaffModalActive: false,
					isEditStaffModalActive: false,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta &&
								'from' in response.meta &&
								'to' in response.meta
							) {
								const total = response.meta.total;
								const data: IApiStaff[] = append
									? [
											...this.state.data,
											...response.data.staffs,
									  ]
									: response.data.staffs;
								const showingStaff = `${data.length}`;

								this.setState({
									data,
									isSubmitting: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total,
									showingStaff,
								});
							}
						})
						.catch(error => {
							this.setState({
								isSubmitting: false,
							});

							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		}
	}

	protected async loadStaffTeams() {
		if (!this.state.isSubmitting && this.state.teams === null) {
			const { data } = await this.props.apiService.queryGET('/api/teams');

			if ('teams' in data) {
				this.setState({
					teams: data.teams,
				});
			}
		}
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(props.permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.READ);
		const permissions=prevProps?prevProps.permissions:[];
		const prevReadPermission: boolean =this.props.roleName==="admin"|| checkPermission(permissions,EStaffManagementPermissions.ALL,EStaffManagementPermissions.READ);
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin||(!prevReadPermission&&readPermission))
		) {
			this.loadStaffMembers(false);
			this.loadStaffTeams();
		}
	}
	
}

export default StaffManagement;
