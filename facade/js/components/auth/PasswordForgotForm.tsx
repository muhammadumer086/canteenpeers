import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import { TextFieldFormik } from '../inputs';

import { validationsUser } from '../../helpers/validations';
import Link from 'next/link';

interface IProps {
	submitting: boolean;
	showRegister: boolean;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	email: string;
}

export const PasswordForgotForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		email: validationsUser.email,
	});

	return (
		<div>
			<Formik
				initialValues={{
					email: '',
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form
							className="auth_form form"
							onSubmit={handleSubmit}
						>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="email"
										label="Email"
										name="email"
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							{props.showRegister && (
								<div className="form-row">
									<div className="form-column">
										<span>
											This email is not registered with
											Canteen Connect. Would you like to{' '}
											<Link
												href="/auth/register?step=step-1"
												as="/auth/register/step-1"
											>
												signup
											</Link>{' '}
											instead?{' '}
										</span>
									</div>
								</div>
							)}
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || props.submitting}
									>
										Reset Password
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
};
