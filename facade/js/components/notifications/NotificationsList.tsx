import React from 'react';
import { IApiNotification } from '../../interfaces';

import NotificationsTile from './NotificationsTile';

import { GTMDataLayer } from '../../helpers/dataLayer';
import { throttle } from '../../helpers/throttle';

import ListLoading from '../general/ListLoading';

interface IProps {
	notificationsData: IApiNotification[] | null;
	scrollBottomReached(): void;
	isLoadingNotifications: boolean;
}

interface IState {}

class NotificationsList extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	protected scrollThreshold = !!(process as any).browser
		? window.innerHeight / 2
		: 500;

	protected throttleScroll: any = throttle((target: Element) => {
		const bottomDistance =
			target.scrollHeight - (target.clientHeight + target.scrollTop);

		if (
			this.props.scrollBottomReached &&
			bottomDistance <= this.scrollThreshold
		) {
			this.props.scrollBottomReached();
		}
	}, 200);

	constructor(props: IProps) {
		super(props);

		this.state = {};
		this.handleScroll = this.handleScroll.bind(this);
	}

	public render() {
		return (
			<div
				className="notifications_panel-list theme--main"
				onScroll={this.handleScroll}
			>
				{!!this.props.notificationsData && (
					<React.Fragment>
						{this.props.notificationsData.map(
							(notification, index) => {
								if (notification && 'type' in notification) {
									return (
										<NotificationsTile
											key={index}
											notification={notification}
										/>
									);
								} else {
									return null;
								}
							},
						)}
					</React.Fragment>
				)}
				<ListLoading active={this.props.isLoadingNotifications} />
			</div>
		);
	}

	protected handleScroll(e) {
		this.throttleScroll(e.target);
	}

	protected getWindowHeight() {
		return window.innerHeight;
	}
}

export default NotificationsList;
