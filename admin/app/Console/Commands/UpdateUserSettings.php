<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Discussion;
use App\PersonalSetting;
use App\User;

class UpdateUserSettings extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:userSettings {setting? : The setting slug to enable for everyone}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate the user settings (preferences and roles)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		\App\User::disableSearchSyncing();
		$usersQuery = null;

		$usersQuery = User::whereNull('deleted_at');

		if ($this->argument('setting')) {
			$setting = PersonalSetting::where(
				'slug',
				$this->argument('setting')
			)->firstOrFail();

			$usersQuery->chunk(10, function ($users) use ($setting) {
				foreach ($users as $user) {
					// Associate the user to the setting
					$user
						->personalSettings()
						->syncWithoutDetaching([$setting->id]);
					$user->generatePreferences();
					$user->save(['timestamps' => false]);
				}
			});
		} else {
			$usersQuery->chunk(10, function ($users) {
				foreach ($users as $user) {
					$user->generatePreferences();
					$user->generateRoles();
					$user->save(['timestamps' => false]);
				}
			});
		}
	}
}
