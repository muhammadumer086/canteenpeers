import React from 'react';
import ReactSVG from 'react-svg';
import CircularProgress from '@material-ui/core/CircularProgress';
import MessageDetail from './MessageDetail';
import MessageReply from './MessageReply';
import MessageUserList from './MessageUserList';

import { connect } from 'react-redux';
import { IStoreState } from '../../redux/store';

import { ButtonBase } from '@material-ui/core';
import { IApiUser, IApiMessage, IApiConversationDetail } from 'js/interfaces';

interface IProps {
	userData: IApiUser;
	isUpdating: boolean;
	isLoadingMessages: boolean;
	activeUsers: IApiUser[];
	activeConversationMessages: IApiMessage[];
	conversationInformationActive: boolean;
	conversationDetail: IApiConversationDetail;
	clearMessageData: boolean;
	dispatch(values: any): void;
	loadMoreMessages(): void;
	submitReply(values: any): Promise<boolean>;
	closeConversation(): void;
	toggleConversationDetail(): void;
}

const MessageConversation: React.StatelessComponent<IProps> = props => {
	const arrowIconUrl = `${process.env.STATIC_PATH}/icons/back-arrow.svg`;
	const InformationIconUrl = `${
		process.env.STATIC_PATH
	}/icons/information.svg`;
	const hasUnderageUser = props.activeUsers.find(user => user.underage);
	const hasAdminUser = props.activeUsers.find(
		user => !!user.role_names.find(role => role === 'admin'),
	);

	const imageDimensions = {
		width: 24,
		height: 24,
	};

	return (
		<React.Fragment>
			<div className="user_messages-message_information">
				<div className="user_messages-message_information_wrapper">
					<div className="user_messages-back_to_message">
						{!props.isUpdating && (
							<ButtonBase
								focusRipple={true}
								onClick={props.closeConversation}
							>
								<ReactSVG
									path={arrowIconUrl}
									svgStyle={{ ...imageDimensions }}
								/>
							</ButtonBase>
						)}
					</div>

					<div className="user_messages-message_who">
						{!props.isUpdating &&
							!!props.conversationDetail &&
							props.conversationDetail.name === null && (
								<span className="text-ellipse">
									<MessageUserList
										users={props.activeUsers}
										groupChat={
											props.conversationDetail
												.group_conversation
										}
									/>
								</span>
							)}

						{!props.isUpdating &&
							!!props.conversationDetail &&
							props.conversationDetail.name !== null && (
								<span className="text-ellipse">
									{props.conversationDetail.name}
								</span>
							)}
					</div>

					<div className="user_messages-information">
						{!props.isUpdating &&
							!props.conversationInformationActive &&
							!!props.conversationDetail &&
							props.conversationDetail.group_conversation && (
								<ButtonBase
									onClick={props.toggleConversationDetail}
									disableRipple={true}
								>
									<ReactSVG
										path={InformationIconUrl}
										svgStyle={{ ...imageDimensions }}
									/>
								</ButtonBase>
							)}
					</div>
				</div>

				{props.isUpdating && (
					<div className="user_messages-loading">
						<CircularProgress />
					</div>
				)}
			</div>

			<div className="user_messages-content">
				<div
					className={`user_messages-message_detail ${
						props.isUpdating || props.isLoadingMessages
							? 'user_messages-message_detail--is-disabled'
							: ''
					}`}
				>
					<div className="user_messages-message_detail-content">
						{props.activeConversationMessages && (
							<MessageDetail
								messageItems={props.activeConversationMessages}
								isMultiple={true}
								scrollTopReached={props.loadMoreMessages}
								isLoadingReplies={props.isLoadingMessages}
							/>
						)}
					</div>
					{(!hasUnderageUser || hasAdminUser) && (
						<div className="user_messages-reply">
							<MessageReply
								user={props.userData}
								isReplyOpen={true}
								submitReply={props.submitReply}
								clearMessageData={props.clearMessageData}
							/>
						</div>
					)}
				</div>
			</div>
		</React.Fragment>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(MessageConversation);
