@extends('layouts.email')

@section('plain_text')
	Hi {{$user->first_name}},

	Welcome to {{ config('app.name') }}.<br/>Please verify the account by clicking on the link below.
@endsection

@section('content')
	@component('emails.components.paragraph')
		Hi {{$user->first_name}},
	@endcomponent
	@component('emails.components.paragraph')
		Welcome to {{ config('app.name') }}.<br/>Please verify the account by clicking on the link below.
	@endcomponent
	@php
		$link = config('canteen.facade_url') . '/auth/verify?' . http_build_query([
			'token' => $user->verification_token,
		])
	@endphp
	@component('emails.components.button', ['url' => $link])
		Verify Account
	@endcomponent

	@component('emails.components.paragraph')
		If you did not create an account on {{ config('app.name') }}, no further action is required.
	@endcomponent

	@component('emails.components.paragraph')
		Regards,
	@endcomponent
@endsection
