<?php
namespace App;

use Prismic\LinkResolver;

class PrismicLinkResolver extends LinkResolver {
	public function resolve($link):string {
		if (property_exists($link, 'isBroken') && $link->isBroken === true) {
			return '/404';
		}
		if ($link->type === 'blog') {
			return '/blog/' . $link->uid;
		}
		if ($link->type === 'resource') {
			return '/resource/' . $link->uid;
		}
		return '/';
	}
}

$linkResolver = new PrismicLinkResolver();
