import React from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';
import Router from 'next/router';
import ReactSVG from 'react-svg';

import { checkPage404 } from '../js/helpers/checkPage404';
import { ApiService } from '../js/services';
import { IApiBlog, IApiUser } from '../js/interfaces';

import {
	IStoreState,
	apiError,
	apiSuccess,
	newBlogModalDisplay,
} from '../js/redux/store';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import WYSIWYG from '../js/components/general/WYSIWYG';
import Footer from '../js/components/general/Footer';
import SubTitleDetail from '../js/components/general/SubTitleDetail';
import BlogTile from '../js/components/blogs/BlogTile';
import ArticleActions from '../js/components/general/ArticleActions';
import ArticleActionsAdmin from '../js/components/general/ArticleActionsAdmin';
import TileAuthor from '../js/components/general/TileAuthor';
import Flag from '../js/components/general/Flag';
import FixedBackBar from '../js/components/general/FixedBackBar';
import ModalDialogDeleteConfirm from '../js/components/modals/ModalDialogDeleteConfirm';

import { GTMDataLayer } from '../js/helpers/dataLayer';
import { ButtonBaseLink } from '../js/components/general/ButtonBaseLink';
import { ModalDialog } from '../js/components/modals/local';

 

interface IProps {
	userAdmin: boolean;
	apiService: ApiService;
	userData: IApiUser;
	userAuthenticated: boolean;
	blogData: IApiBlog;
	related: IApiBlog[];
	updateDataCount: number;
	dispatch(values): void;
}

export interface IAdditionalActions {
	label: string;
	action(): void;
}

interface IState {
	isUpdating: boolean;
	deleteConfirmModalActive: boolean;
	additionalActions: IAdditionalActions[] | null;
}

class BlogSingle extends React.Component<IProps, IState> {
	static async getInitialProps({ req, query, apiService }) {
		const result = await checkPage404(
			req,
			query,
			'id',
			async (slug: string) => {
				const data = await (apiService as ApiService).queryGET(
					`/api/blogs/${slug}`,
				);
				const blogData = 'blog' in data ? data.blog : null;
				const related = 'related' in data ? data.related : [];

				return {
					blogData,
					related,
				};
			},
		);

		if (!result) {
			return {
				blogData: null,
			};
		}

		return result;
	}

	protected GTM = new GTMDataLayer();

	constructor(props: IProps) {
		super(props);

		this.editBlog = this.editBlog.bind(this);
		this.deleteBlog = this.deleteBlog.bind(this);

		this.state = {
			isUpdating: false,
			deleteConfirmModalActive: false,
			additionalActions: [],
		};

		this.trackRelatedContent = this.trackRelatedContent.bind(this);
		this.toggleDeleteConfirmModal = this.toggleDeleteConfirmModal.bind(
			this,
		);
		this.generateAdditionalActions = this.generateAdditionalActions.bind(
			this,
		);
	}

	public componentDidMount() {
		if (
			this.props.blogData.private &&
			(this.props.userData == null || !this.props.userAuthenticated)
		) {
			Router.push('/auth/login');
		}

		this.setState({
			additionalActions: this.generateAdditionalActions(),
		});
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAdmin !== prevProps.userAdmin ||
			JSON.stringify(this.props.blogData) !==
				JSON.stringify(prevProps.blogData)
		) {
			this.setState({
				additionalActions: this.generateAdditionalActions(),
			});
		}
	}

	public render() {
		const url = `${process.env.APP_URL}/blogs/${this.props.blogData.slug}`;

		// const removeHTMLandTruncate = (
		// 	content: string,
		// 	stringLength: number,
		// ) => {
		// 	const replaceTags = content.replace(/<(?:.|\n)*?>/gm, '');
		// 	const limitContent = replaceTags.substring(0, stringLength);

		// 	return limitContent;
		// };

		// const seoTitle = !!this.props.blogData.seo.title
		// 	? this.props.blogData.seo.title
		// 	: this.props.blogData.title;

		// const seoDescription = !!this.props.blogData.seo.description
		// 	? this.props.blogData.seo.description
		// 	: removeHTMLandTruncate(this.props.blogData.content, 300);

		const seoFeaturedImage = !!this.props.blogData.seo.image
			? this.props.blogData.seo.image
			: `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;
		const sharingUrl = url;

		return (
			<div className="blog_single">
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:url"
						property="og:url"
						content={sharingUrl}
					/>
					<meta name="og:type" property="og:type" content="article" />
				

					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>

					<meta name="twitter:card" content="summary" />
					<meta name="twitter:site" content="Canteen Connect" />
					<meta
						name="twitter:title"
						content={this.props.blogData.title}
					/>
					<meta
						name="twitter:description"
						content={this.props.blogData.title}
					/>
					<meta name="twitter:image" content={seoFeaturedImage} /> 
				</Head>
				<LayoutDashboard>
					{/* The fixed bar */}
					<FixedBackBar href="/blogs" label="All Blogs" />

					<div className="theme--main">
						<div className="resources_single-content_container">
							<div className="resources_single-wyiwysg">
								<WYSIWYG>
									<div className="resources_single-topic">
										{this.props.blogData.topic && (
											<ButtonBaseLink
												className="general_tile-topic_link"
												href={`/blogs?topic=${
													this.props.blogData.topic
														.slug
												}`}
												title={
													this.props.blogData.topic
														.slug
												}
												disableRipple={true}
											>
												{
													this.props.blogData.topic
														.title
												}
											</ButtonBaseLink>
										)}
									</div>

									<div className="article_single-title hm-r32">
										<h1 className="article_single-wyiwysg_title theme-title">
											{this.props.blogData.title}
										</h1>

										{!!this.state.additionalActions &&
											this.state.additionalActions
												.length && (
												<ArticleActionsAdmin
													menuActions={
														this.state
															.additionalActions
													}
												/>
											)}
									</div>

									<div className="article_single-flags">
										{this.props.blogData.age_sensitive && (
											<Flag>Age sensitive</Flag>
										)}

										{this.props.blogData.private && (
											<Flag>
												<ReactSVG
													path={`${
														process.env.STATIC_PATH
													}/icons/locked.svg`}
												/>
												Members
											</Flag>
										)}
									</div>

									<div className="article_single-meta">
										<div className="article_single-author general_tile-author">
											<TileAuthor
												userData={
													this.props.blogData.user
												}
												blogData={this.props.blogData}
											/>
										</div>
									</div>

									<div
										dangerouslySetInnerHTML={{
											__html: this.props.blogData.content,
										}}
									/>
								</WYSIWYG>
							</div>

							<ArticleActions
								sharingUrl={sharingUrl}
								articleTitle={this.props.blogData.title}
								resourceType="blogs"
								resourceSlug={this.props.blogData.slug}
							/>
						</div>
					</div>

					{this.props.related &&
						this.props.related.length > 0 &&
						this.renderRelatedTiles()}

					<Footer />
				</LayoutDashboard>

				{this.state.deleteConfirmModalActive && (
					<ModalDialog
						isActive={true}
						modalTitle="Are you sure you want to delete the below post?"
						ariaTag="delete-alert-modal"
						handleClose={this.toggleDeleteConfirmModal}
					>
						<ModalDialogDeleteConfirm
							name={this.props.blogData.title}
							loading={this.state.isUpdating}
							submit={this.deleteBlog}
							cancel={this.toggleDeleteConfirmModal}
						/>
					</ModalDialog>
				)}
			</div>
		);
	}

	protected renderRelatedTiles() {
		const subTitleConfig = {
			icon: 'blogs',
			title: 'Other blogs you may be interested in',
			linkLabel: 'All Blogs',
			linkData: {
				prefix: '/blogs',
				href: '/blogs',
				as: '/blogs',
			},
		};

		return (
			<div className="resources_single-related">
				<div className="resources_single-related_container">
					<SubTitleDetail {...subTitleConfig} />
					{this.props.related.map((data, index) => {
						let blogAuthor: IApiUser = data.user;

						if (
							data &&
							'user' in data &&
							data.user !== null &&
							data.prismic_id === null
						) {
							blogAuthor = data.user;
						} else if (data && data.prismic_id !== null) {
							blogAuthor = {
								full_name: data.prismic_author,
								username: data.prismic_author,
								avatar_url: data.prismic_author_image,
								is_banned: false,
								updated_at: null,
							};
						}

						const disableBlogAuthorButton =
							blogAuthor.username === 'Canteen' ||
							data.user === null
								? true
								: false;

						return (
							<BlogTile
								key={index}
								disabled={false}
								disabledAuthorButton={disableBlogAuthorButton}
								blogData={data}
								blogAuthor={blogAuthor}
								gtm={this.trackRelatedContent}
							/>
						);
					})}
				</div>
			</div>
		);
	}

	protected generateAdditionalActions(): IAdditionalActions[] | null {
		const additionalActions: IAdditionalActions[] = [];

		if (
			this.props.blogData.prismic_id === null &&
			(this.props.userAdmin ||
				(this.props.blogData.user &&
					this.props.userData &&
					this.props.blogData.user.username ===
						this.props.userData.username))
		) {
			additionalActions.push({
				label: 'Edit Blog',
				action: this.editBlog,
			});

			additionalActions.push({
				label: 'Delete Blog',
				action: () => {
					this.toggleDeleteConfirmModal();
				},
			});
		}

		return additionalActions.length ? additionalActions : null;
	}

	protected trackRelatedContent(title: string) {
		this.GTM.pushEventToDataLayer({
			event: 'nextContent',
			title,
		});
	}

	protected deleteBlog() {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				this.props.apiService
					.queryDELETE(`/api/blogs/${this.props.blogData.slug}`)
					.then(() => {
						this.setState(
							{
								isUpdating: false,
							},
							() => {
								Router.push('/blogs');
								this.props.dispatch(
									apiSuccess(['Blog successfully deleted']),
								);
							},
						);
					})
					.catch(error => {
						this.setState(
							{
								isUpdating: false,
							},
							() => {
								this.props.dispatch(apiError(error));
							},
						);
					});
			},
		);
	}

	protected editBlog() {
		this.props.dispatch(newBlogModalDisplay(this.props.blogData));
	}

	protected toggleDeleteConfirmModal() {
		this.setState({
			deleteConfirmModalActive: !this.state.deleteConfirmModalActive,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAdmin, userData, userAuthenticated } = state.user;
	const { updateDataCount } = state.updateData;
	return {
		userAuthenticated,
		userAdmin,
		userData,
		updateDataCount,
	};
}

export default connect(mapStateToProps)(BlogSingle);
