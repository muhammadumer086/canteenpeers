<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixRelationshipForEventImage extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('event_images', function (Blueprint $table) {
			$table->unsignedInteger('event_id')->nullable();

			$table
				->foreign('event_id')
				->references('id')
				->on('events')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('event_images', function (Blueprint $table) {
			$table->dropForeign('event_images_event_id_foreign');
			$table->dropColumn('event_id');
		});
	}
}
