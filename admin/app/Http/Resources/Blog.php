<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Situation as SituationResource;
use App\Http\Resources\Topic as TopicResource;
use App\Http\Resources\Hashtag as HashtagResource;

class Blog extends JsonResource {
	use \App\Traits\AuthUser;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'blog',
				$this->id,
				$this->updated_at,
				true,
				true
			)
		) {
			return $apiStore->getApiResource(
				'blog',
				$this->id,
				$this->updated_at,
				true,
				true
			);
		}

		$params = [
			'id',
			'title',
			'slug',
			'content',
			'age_sensitive',
			'private',
			'source',
			'prismic_id',
			'prismic_author',
			'prismic_author_image',
			'first_publication_date',
			'last_publication_date',
			'deleted_at',
			'created_at',
			'updated_at',
			'featured',
			'feature_image',
			'seo',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}


		if (!$result['first_publication_date']) {
			$result['first_publication_date'] = $this->created_at;
		}

		// Add the additional relationships
		$result['topic'] = $this->topic()->exists()
			? new TopicResource($this->topic)
			: null;
		$result['situations'] = $this->situations()->count()
			? SituationResource::collection($this->situations)
			: [];
		$result['hashtags'] = $this->hashtags()->count()
			? HashtagResource::collection($this->hashtags)
			: [];
		$result['user'] = new UserResource($this->user);

		$authUser = $this->getAuthUser($request);
		if ($authUser && $authUser->hasRoleName('admin')) {
			$adminParams = ['deleted_at', 'views_count'];
			// Add the admin values
			foreach ($adminParams as $key) {
				$result[$key] = $this->$key;
			}
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);
		$result['first_publication_date'] = UserPermissionHelper::formatCarbonDate($result['first_publication_date']);
		$result['last_publication_date'] = UserPermissionHelper::formatCarbonDate($result['last_publication_date']);
		$result['deleted_at'] = UserPermissionHelper::formatCarbonDate($result['deleted_at']);

		$apiStore->setApiResource(
			$result,
			'blog',
			$this->id,
			$this->updated_at,
			true,
			true
		);

		return $result;
	}
}
