<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AgeRange extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();

		if (
			$apiStore->hasApiResource(
				'age_range',
				$this->id,
				$this->updated_at,
				true,
				false
			)
		) {
			return $apiStore->getApiResource(
				'age_range',
				$this->id,
				$this->updated_at,
				true,
				false
			);
		}

		$data = [
			'id' => $this->id,
			'name' => $this->name,
			'slug' => $this->slug,
			'min' => $this->min,
			'max' => $this->max,
		];

		$apiStore->setApiResource(
			$data,
			'age_range',
			$this->id,
			$this->updated_at,
			true,
			false
		);

		return $data;
	}
}
