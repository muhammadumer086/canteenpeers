import React from 'react';
import { Transition } from 'react-transition-group';

import { connect } from 'react-redux';

import { IStoreState } from '../../redux/store';

import LinearProgress from '@material-ui/core/LinearProgress';

interface IProps {}

interface IState {
	loaded: boolean;
}

class AppLoader extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			loaded: false,
		};
	}

	public componentDidMount() {
		this.setState({
			loaded: true,
		});
	}

	public render() {
		return (
			<Transition in={!this.state.loaded} timeout={250} unmountOnExit>
				{status => {
					const classNames = [
						`app_loader_wrapper`,
						`theme--secondary`,
						`theme-background--gradient`,
						`animation_loading`,
						`animation_loading--${status}`,
					];
					return (
						<div className={classNames.join(' ')}>
							<div className="app_loader">
								<img
									className="app_loader-logo"
									src={`${
										process.env.STATIC_PATH
									}/images/logo-canteen-loading.png`}
									alt="Canteen"
									width="120"
									height="120"
								/>
								<LinearProgress
									className="app_loader-bar"
									color="secondary"
								/>
							</div>
						</div>
					);
				}}
			</Transition>
		);
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(AppLoader);
