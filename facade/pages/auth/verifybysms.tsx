import React from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';
import Link from 'next/link';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import { apiError, apiSuccess, IStoreState } from '../../js/redux/store';
import { VerifyBySmsForm } from '../../js/components/auth/VerifyBySmsForm';

import { ApiService, AuthService } from '../../js/services';
import { IApiUser } from '../../js/interfaces';
import MyError from '../_error';
import Router from 'next/router';
import { GTMDataLayer } from '../../js/helpers/dataLayer';
import Countdown from 'react-countdown';
import { ButtonLink } from '../../js/components/general/ButtonLink';

interface IProps {
	query: any | null;
	userData: IApiUser | null;
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	resend: boolean;
	disabled: boolean;
	time: number;
	codeSentOnce: boolean;
}
const timeRendrer = ({ minutes, seconds, completed }) => {
	if (completed) {
		return <span>Resend Code</span>;
	}
	return <span>{`Resend Code( ${minutes} Min : ${seconds} Sec )`}</span>;
};
class VerifyBySms extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();
	public constructor(props: IProps) {
		super(props);

		this.state = {
			submitting: false,
			resend: false,
			disabled: true,
			time: Date.now() + 2 * 60 * 1000,
			codeSentOnce: false,
		};

		this.resendVerification = this.resendVerification.bind(this);
	}
	public componentDidMount() {
		Router.push(
			{
				pathname: '/auth/verifybysms',
				query: { step: 'step-1' },
			},
			'/auth/verifybysms/step-1',
		);
	}

	public render() {
		if (
			!this.props.userData ||
			!this.props.userData.hasOwnProperty('first_name') ||
			!this.props.userData.hasOwnProperty('email')
		) {
			return <MyError statusCode={401} />;
		}
		const { time, disabled, submitting } = this.state;
		return (
			<div className="auth_wrapper theme--accent_dark auth_wrapper--with_header">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<div className="auth_page-header">
					<img
						className="auth_page-header-logo"
						src={`${process.env.STATIC_PATH}/images/logo-canteen-horizontal.png`}
					/>
				</div>
				<section className="auth">
					<header className="auth-header">
						<span className="auth_page-header-text">
							Verify account via mobile phone
						</span>
						<p>
							We need to confirm your mobile phone number before
							getting started. After that you're ready to go!
						</p>
					</header>
					<div className="auth-content theme--main">
						{submitting && (
							<LinearProgress
								color="secondary"
								className="auth-content-progress"
							/>
						)}

						<VerifyBySmsForm
							onSubmit={this.onSubmit}
							submitting={submitting}
							number={this.props.userData?.phone}
							sentOnce={this.state.codeSentOnce}
							sendCode={this.resendVerification}
						/>
						{this.state.codeSentOnce && (
							<div className="auth-content-footer font--small">
								<div className="hm-b16">
									Didn’t receive the code?
								</div>

								<div>
									<Button
										variant="contained"
										color="primary"
										disabled={disabled || submitting}
										onClick={() =>
											this.resendVerification(true)
										}
									>
										<Countdown
											onComplete={() =>
												this.setState({
													disabled: false,
												})
											}
											key={Date.now()}
											autoStart={true}
											date={time}
											renderer={timeRendrer}
										/>
									</Button>
								</div>
							</div>
						)}
					</div>
					<footer className="auth-footer font--small">
						<div className="auth-footer-line-container">
							<div className="auth-footer-line"></div>
							<p>Or I want to</p>
							<div className="auth-footer-line"></div>
						</div>
						<div>
							<ButtonLink
								variant="outlined"
								color="inherit"
								className="auth-footer-button"
								href="/auth/verifybyemail"
							>
								verify via email
							</ButtonLink>
						</div>
						Back to{' '}
						<Link href="/">
							<a>{process.env.APP_NAME}</a>
						</Link>
					</footer>
				</section>
			</div>
		);
	}

	protected resendVerification(first?: boolean) {
		this.setState(
			{
				submitting: true,
				resend: false,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/users/smscode', {
						email: this.props.userData.email,
						//	countryCode: '+92',
						// countryCode:
						// 	this.props.userData.country_slug === 'AU'
						// 		? '+61'
						// 		: '+64',
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					})
					.then(res => {
						if (res && res.success) {
							this.props.dispatch(apiSuccess(['Message sent.']));
							this.setState({
								submitting: false,
								resend: true,
								time: Date.now() + 2 * 60 * 1000,
								disabled: true,
								codeSentOnce: first,
							});
							if (first) {
								Router.push(
									{
										pathname: '/auth/verifybysms',
										query: { step: 'step-2' },
									},
									'/auth/verifybysms/step-2',
								);
							}
						} else {
							this.props.dispatch(
								apiError([
									res && res.message
										? res.message
										: 'Something went wrong',
								]),
							);
							this.setState({
								submitting: false,
							});
						}
						// Success
					});
			},
		);
	}

	protected onSubmit = values => {
		//	const url = '/api/users/verifycode';
		const submissionValues = {
			email: this.props.userData.email,
			mobileCode: values.code,
		};

		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST(`/api/users/verifycode`, submissionValues)
					.then(_data => {
						this.props.dispatch(apiSuccess(['Number verified']));
						this.GTM.pushEventToDataLayer({
							event: 'userVerified',
						});
						Router.push(
							'/auth/verify-success?source=mobile',
							'/auth/verify-success/mobile',
						);

						// if (
						// 	'token_type' in data &&
						// 	'expires_in' in data &&
						// 	'access_token' in data &&
						// 	'refresh_token' in data
						// ) {
						// 	// Success
						// 	this.GTM.pushEventToDataLayer({
						// 		event: 'userVerified',
						// 	});
						// 	this.props.authService
						// 		.logIn(data)
						// 		.then(result => {
						// 			if (result) {
						// 				this.props.dispatch(userVerified());
						// 				Router.push('/dashboard');
						// 			}
						// 		})
						// 		.catch(() => {
						// 			this.setState({
						// 				submitting: false,
						// 			});
						// 		});
						// } else {
						// 	this.setState({
						// 		submitting: false,
						// 	});
						// 	throw new Error('Invalid data received');
						// }
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	};
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(VerifyBySms);
