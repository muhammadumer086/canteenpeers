import { actionTypes } from '../actions';
import { IApiSituation } from '../../interfaces';

export interface IStoreState {
	loading: boolean;
	situations: IApiSituation[];
}

export const situations = (state: IStoreState = {
	loading: false,
	situations: [],
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.SITUATIONS_LOAD:
			return Object.assign({}, state, {
				loading: true,
			});
		case actionTypes.SITUATIONS_LOADED:
			return Object.assign({}, state, {
				situations: action.situations,
				loading: false,
			});
		case actionTypes.SITUATIONS_LOAD_ERROR:
			return Object.assign({}, state, {
				loading: false,
			});
		default:
			return state;
	}
}
