import { IApiSituation } from '../interfaces';

export function filterRootSituations(
	situations: IApiSituation[],
): IApiSituation[] {
	// Rebuild situations based on root and parent situations
	const rootSituations = situations.map(situation => {
		if (situation.parent_situation === null) {
			return situation;
		} else {
			return situation.parent_situation;
		}
	});
	// Deduplicate the root situations
	return Array.from(new Set(rootSituations.map(a => a.id))).map(id => {
		return rootSituations.find(a => a.id === id);
	});
}
