<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Http\Resources\User as UserResource;

use App\Message;

class MessageReceivedNotification extends Notification {
	use Queueable;

	protected $message;

	/**
	 * Create a new notification instance.
	 *
	 * @param App\Message $message the message sent
	 *
	 * @return void
	 */
	public function __construct(Message $message) {
		$this->message = $message;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['database'];
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			'user' => new UserResource($this->message->user),
			'conversation' => $this->message->conversation,
			'message' => $this->message->message,
		];
	}
}
