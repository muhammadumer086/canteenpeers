import React          from 'react';
import Link           from 'next/link';
import { ButtonBase } from '@material-ui/core';

interface ILink {
	label: string;
	href: string;
	as: string;
}

interface IProps {
	menuItem: ILink
	action(): void;
}

const ModalMenuItem: React.FC<IProps> = (props: IProps) => {
	return (
		<Link href={props.menuItem.href} as={props.menuItem.as}>
			<ButtonBase focusRipple={true} onClick={props.action} className="modal_menu_item-link">
				<span className="modal_menu_item-link-content">
					<span className="font--h4">{props.menuItem.label}</span>
				</span>
			</ButtonBase>
		</Link>
	)
}

export default ModalMenuItem;
