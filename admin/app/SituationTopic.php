<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituationTopic extends Model {

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'situation_topic';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'situation_id',
		'topic_id',
	];

	/**
	 * The situations related to the situation topic
	 */
	public function situation() {
		return $this->belongsTo('App\Situation');
	}

	/**
	 * The Topic related to the situation topic
	 */
	public function topic() {
		return $this->belongsTo('App\Topic');
	}

	/**
	 * The discussions related to the situation topic
	 */
	public function discussions() {
		return $this->belongsToMany('App\Discussion');
	}

	/**
	 * The blogs related to the situation topic
	 */
	public function blogs() {
		return $this->belongsToMany('App\Blog');
	}

	/**
	 * The resources related to the situation topic
	 */
	public function resources() {
		return $this->belongsToMany('App\Resource');
	}

}
