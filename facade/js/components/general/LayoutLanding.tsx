import React from 'react';
import LandingHeader from '../landing/LandingHeader';
import LandingHero from '../landing/LandingHero';
import Footer from './Footer';
import LandingCallToAction from '../landing/LandingCallToAction';

interface IProps {
	ctaConfig: {
		ctaText: string;
		buttonLabel: string;
	}
	country? : string;
}

const LayoutLanding: React.FC<IProps> = (props) => {
	const {country}=props;
	return (
		<div className="layout_landing theme--main">
			<LandingHeader country={country} />
			<LandingHero country={country} />
				{props.children}
			<LandingCallToAction
				ctaConfig={props.ctaConfig}
			/>
			<Footer
				isLandingPage={true}
			/>
		</div>
	)
}


export default LayoutLanding;
