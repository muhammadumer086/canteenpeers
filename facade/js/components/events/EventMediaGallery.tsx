import React from 'react';
import { ModalDialog } from '../modals/local/ModalDialog';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Fab } from '@material-ui/core';
import { IImage } from '../../interfaces';

interface IProps {
	images: IImage[];
}
interface IState {
	currIndex: number;
	showModal: boolean;
}
class MediaGallery extends React.Component<IProps, IState> {
	constructor(p) {
		super(p);
		this.state = {
			currIndex: 0,
			showModal: false,
		};
		this.setCurrentIndex = this.setCurrentIndex.bind(this);
		this.toggleModel = this.toggleModel.bind(this);
	}
	public render() {
		const { images = [] } = this.props;
		const { currIndex, showModal } = this.state;
		return (
			<div>
				<div className="event_media-container">
					{images.slice(0, 2).map((i, ind) => {
						return (
							<div
								key={ind}
								style={{ backgroundImage: `url(${i.url})` }}
								className="event_media-image"
								onClick={() => this.toggleModel(ind)}
							></div>
						);
					})}
				</div>
				<div className="event_media-container2">
					{images.slice(2, 4).map((i, ind) => {
						return (
							<div
								key={ind}
								style={{ backgroundImage: `url(${i.url})` }}
								className="event_media-image2"
								onClick={() => this.toggleModel(ind + 2)}
							></div>
						);
					})}
					{images.length >= 5 && (
						<div
							style={{ backgroundImage: `url(${images[4].url})` }}
							className="event_media-image2"
							onClick={() => this.toggleModel(4)}
						>
							{images.length>5&& <div className="event_media-count-plus">
								{images.length - 5}+
							</div>}
						</div>
					)}
				</div>
				{showModal && (
					<ModalDialog
						modalTitle={images[currIndex].title}
						isActive={true}
						handleClose={() => this.toggleModel(0)}
					>
						<div className="modal_image_container">
							<img className="modal_image" src={images[currIndex].url}></img>
							<div className="modal_image_icon_container">
								<Fab color="primary" onClick={()=>this.setCurrentIndex(currIndex-1)}> <ChevronLeftIcon /></Fab>
                                <Fab color="primary" onClick={()=>this.setCurrentIndex(currIndex+1)}> <ChevronRightIcon /></Fab>

							</div>
						</div>
					</ModalDialog>
				)}
			</div>
		);
	}
	protected setCurrentIndex = (index: number) => {
        let currIndex=index;
        const {images}=this.props;
        if(currIndex<0)
        {
            currIndex=images.length-1;
        }
        else if(currIndex>=images.length)
        {
            currIndex=0;
        }
		this.setState({ currIndex });
	};
	protected toggleModel = index => {
		this.setCurrentIndex(index);
		this.setState({ showModal: !this.state.showModal });
	};
}
export default MediaGallery;
