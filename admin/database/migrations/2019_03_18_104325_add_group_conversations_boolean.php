<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupConversationsBoolean extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('conversations', function (Blueprint $table) {
			$table->boolean('group_conversation')->default(false);

			$table->dropForeign('conversations_user_id_foreign');
			$table->dropColumn('user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('conversations', function (Blueprint $table) {
			$table->dropColumn('group_conversation');

			$table->unsignedInteger('user_id')->nullable();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});
	}
}
