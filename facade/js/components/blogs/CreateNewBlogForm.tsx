import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';

import Button from '@material-ui/core/Button';

import { ApiService, withServices } from '../../services';
import { IStoreState } from '../../redux/store';

import {
	TextFieldFormik,
	RichTextEditor,
	SelectFieldFormik,
	CheckboxGroupFormik,
	ImageDropzoneFormik,
} from '../inputs';

import {
	IApiSituation,
	ISelectOption,
	IApiBlog,
	IApiUser,
} from '../../interfaces';

import Pill from '../general/Pill';
import { filterRootSituations } from '../../helpers/filterRootSituations';

interface IProps {
	blogData: IApiBlog | null;
	isSubmitting: boolean;
	situations: IApiSituation[];
	userSituations: IApiSituation[];
	topics: ISelectOption[];
	apiService: ApiService;
	userData: IApiUser | null;
	predefinedTopic?: string | null;
	updatedSituations(situations: string[]): void;
	dispatch(action: any): void;
	onSubmit(values): void;
}

interface IState {
	headerImageUploading: boolean;
	situations: IApiSituation[];
	userSituations: IApiSituation[];
	activeUserSituation: string[];
}

class CreateNewBlogForm extends React.Component<IProps, IState> {
	protected setFieldValue: (fieldName: string, value: any) => void = null;

	constructor(props: IProps) {
		super(props);

		this.state = {
			headerImageUploading: false,
			situations: filterRootSituations(props.situations),
			userSituations: props.userSituations,
			activeUserSituation: this.initialiseSituations(props),
		};

		this.handleSituationClick = this.handleSituationClick.bind(this);
	}

	public componentDidUpdate(oldProps: IProps) {
		if (
			JSON.stringify(this.props.userSituations) !==
				JSON.stringify(oldProps.userSituations) ||
			JSON.stringify(this.props.blogData) !==
				JSON.stringify(oldProps.blogData)
		) {
			this.setState({
				userSituations: this.props.userSituations,
				activeUserSituation: this.initialiseSituations(this.props),
			});
		}
		if (
			JSON.stringify(this.props.situations) !==
			JSON.stringify(oldProps.situations)
		) {
			this.setState({
				situations: filterRootSituations(this.props.situations),
				activeUserSituation: this.initialiseSituations(this.props),
			});
		}
	}

	public render() {
		const validationFormat = Yup.object().shape({
			title: Yup.string()
				.required('A title is required')
				.max(191),
			content: Yup.string()
				.required('Blog content is required')
				.max(16000000),
			topic: Yup.string()
				.required('A topic is required')
				.max(191),
			situations: Yup.array()
				.of(
					Yup.mixed().oneOf(
						this.props.situations.map(singleOption => {
							return singleOption.slug;
						}),
					),
				)
				.required('Please select at least one situation'),
		});

		const blogDisplayOptions: ISelectOption[] = [
			{
				label:
					'I’d like to make this blog private to Canteen Connect Members only ',
				value: 'private',
			},
			,
		];

		if (this.props.userData && !this.props.userData.underage) {
			blogDisplayOptions.push({
				label:
					'This discussion contains age-sensitive information (blog won’t be seen by users under the age of 16)',
				value: 'ageSensitive',
			});
		}

		let topic: string;

		if (this.props.predefinedTopic) {
			topic = this.props.predefinedTopic;
		} else if (this.props.blogData) {
			topic = this.props.blogData.topic.slug;
		} else {
			topic = '';
		}

		const displayOptions: string[] = [];

		if (!!this.props.blogData) {
			if (this.props.blogData.age_sensitive) {
				displayOptions.push('ageSensitive');
			}

			if (this.props.blogData.private) {
				displayOptions.push('private');
			}
		}

		return (
			<div>
				<Formik
					initialValues={{
						title: this.props.blogData
							? this.props.blogData.title
							: '',
						content: this.props.blogData
							? this.props.blogData.content
							: '<p></p>',
						topic: topic,
						feature_image: this.props.blogData
							? this.props.blogData.feature_image
							: null,
						situations: this.state.activeUserSituation,
						displayOptions: displayOptions, // Generate previous values
					}}
					validationSchema={validationFormat}
					onSubmit={values => {
						const formValues = JSON.parse(JSON.stringify(values));
						// If check boxes are checked
						if (formValues.displayOptions.length > 0) {
							// If both are checked
							if (
								formValues.displayOptions.indexOf('private') >=
									0 &&
								formValues.displayOptions.indexOf(
									'ageSensitive',
								) >= 0
							) {
								formValues['private'] = true;
								formValues['ageSensitive'] = true;
							} else {
								if (
									formValues.displayOptions.indexOf(
										'private',
									) >= 0
								) {
									formValues['private'] = true;
									formValues['ageSensitive'] = false;
								}

								if (
									formValues.displayOptions.indexOf(
										'ageSensitive',
									) >= 0
								) {
									formValues['private'] = false;
									formValues['ageSensitive'] = true;
								}
							}
						} else {
							formValues['private'] = false;
							formValues['ageSensitive'] = false;
						}

						delete formValues.displayOptions;

						this.props.onSubmit(formValues);
					}}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};

						this.setFieldValue = setFieldValue;

						const dropzoneClasses = ['discussion_modal-dropzone'];

						if ('feature_image' in values && values.feature_image) {
							dropzoneClasses.push(
								'discussion_modal-dropzone--image',
							);
						}

						return (
							<form
								className="create_discussion_form form"
								onSubmit={handleSubmit}
							>
								<div className="form-row">
									<div className="form-column">
										<div className="form-helper_text theme-title hm-b8">
											<span className="font--body">
												This blog relates to the
												following cancer experience(s)
											</span>
										</div>
										{this.state.situations.map(
											situation => {
												return this.renderSituation(
													situation,
													setFieldValue,
												);
											},
										)}
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<SelectFieldFormik
											label="Topics"
											name="topic"
											options={this.props.topics}
											values={inputProps.values.topic}
											disabled={this.props.isSubmitting}
											noEmpty={false}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Blog Title"
											name="title"
											disabled={this.props.isSubmitting}
											{...inputProps}
										/>
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<ImageDropzoneFormik
											values={inputProps.values}
											dropzoneClasses={dropzoneClasses}
											setFieldValue={
												inputProps.setFieldValue
											}
											apiUrl="/api/blog-images"
										/>
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<RichTextEditor
											toolbarId="new-blog-toolbar"
											name="content"
											value={inputProps.values.content}
											profile={[
												'imageUpload',
												'embedVideo',
												'insertGif',
												'hashtag',
											]}
											bounds=".create_discussion_form"
											scrollingContainer=".modal--blog .modal-content_container"
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<CheckboxGroupFormik
											label="Blog post display options"
											name="displayOptions"
											options={blogDisplayOptions}
											disabled={this.props.isSubmitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="contained"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid ||
												this.props.isSubmitting
											}
										>
											Submit Blog
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}

	protected renderSituation(
		userSituation: IApiSituation,
		setFieldValue: (fieldName: string, value: any) => void,
	) {
		return (
			<Pill
				label={userSituation.name}
				active={
					this.state.activeUserSituation.indexOf(
						userSituation.slug,
					) >= 0
				}
				disabled={
					this.state.activeUserSituation.length === 1 &&
					this.state.activeUserSituation.indexOf(
						userSituation.slug,
					) >= 0
				}
				clickable={true}
				onClick={() => {
					this.handleSituationClick(userSituation, setFieldValue);
				}}
				key={userSituation.id}
			/>
		);
	}

	protected initialiseSituations(props: IProps) {
		let activeUserSituation: string[] = [];

		if (props.blogData) {
			activeUserSituation = filterRootSituations(
				props.blogData.situations,
			).map(singleSituation => {
				return singleSituation.slug;
			});
		} else {
			activeUserSituation = filterRootSituations(
				props.userSituations,
			).map(singleSituation => {
				return singleSituation.slug;
			});
		}

		if (this.setFieldValue) {
			this.setFieldValue('situations', activeUserSituation);
		}

		this.props.updatedSituations(activeUserSituation);

		return activeUserSituation;
	}

	protected handleSituationClick(
		situation: IApiSituation,
		setFieldValue: (fieldName: string, value: any) => void,
	) {
		const situationIndex = this.state.activeUserSituation.indexOf(
			situation.slug,
		);
		const activeUserSituation: string[] = JSON.parse(
			JSON.stringify(this.state.activeUserSituation),
		);

		if (situationIndex >= 0) {
			// Remove the topic
			activeUserSituation.splice(situationIndex, 1);
		} else {
			// Add the topic
			activeUserSituation.push(situation.slug);
		}

		setFieldValue('situations', activeUserSituation);

		this.setState(
			{
				activeUserSituation,
			},
			() => {
				this.props.updatedSituations(this.state.activeUserSituation);
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(withServices(CreateNewBlogForm));
