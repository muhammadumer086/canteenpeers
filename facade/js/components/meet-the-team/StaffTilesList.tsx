import React from 'react';
import { IApiStaff } from '../../../js/interfaces';
import StaffTilesListItem from './StaffTilesListItem';
import ListLoading from '../../components/general/ListLoading';

interface IProps {
	isLoading: boolean;
	staffData: IApiStaff[];
	onClick(activeStaffMember: IApiStaff);
}

const StaffTilesList: React.StatelessComponent<IProps> = props => {
	
	return (
		<div className="staff_tiles_list">
			{!props.isLoading && (
				<div className="staff_tiles_list-container">
					{props.staffData.map((staff, index) => {
						return (
							<StaffTilesListItem
								key={index}
								data={staff}
								onClick={props.onClick}
							/>
						);
					})}
				</div>
			)}

			{props.isLoading && (
				<div
					style={{
						display: 'flex',
						justifyContent: 'center',
						marginTop: '2.5rem',
					}}
				>
					<ListLoading active={true} />
				</div>
			)}
		</div>
	);
};

export default StaffTilesList;
