import React from 'react';
import MessageItem from './MessageItem';
import { IApiMessage } from '../../interfaces';
import { throttle } from '../../helpers/throttle';
import ListLoading from '../general/ListLoading';

interface IProps {
	messageItems: IApiMessage[];
	isMultiple: boolean;
	isLoadingReplies: boolean;
	scrollTopReached?: () => void;
	scrollBottomReached?: () => void;
}

const MessageDetail = (props: IProps) => {
	const scrollThreshold = (!!(process as any).browser) ? window.innerHeight / 2 : 500;

	const throttleScroll: any = throttle((target: Element) => {
		const topDistance = target.scrollTop;
		const bottomDistance = target.scrollHeight - (target.clientHeight + target.scrollTop);

		if (
			props.scrollTopReached &&
			topDistance <= scrollThreshold
		) {
			props.scrollTopReached();
		}

		if (
			props.scrollBottomReached &&
			bottomDistance <= scrollThreshold
		) {
			props.scrollBottomReached();
		}
	}, 200);

	const handleScroll = (e) => {
		throttleScroll(e.target);
	}

	return (
		<div id="scroll-container" className="user_messages-conversation" onScroll={handleScroll}>
			<ListLoading active={props.isLoadingReplies} />
			{
				props.messageItems.map((data, index) => {
					return (
						<MessageItem
							key={index}
							message={data}
						/>
					)
				})
			}
		</div>
	)
}

export default MessageDetail;
