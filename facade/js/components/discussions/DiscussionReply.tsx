import React from 'react';
import ReactSVG from 'react-svg';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { Formik } from 'formik';

import Button from '@material-ui/core/Button';

import { IStoreState } from '../../redux/store';
import { IReplyTo, IApiUser } from '../../interfaces';

import DiscussionReplyName from './DiscussionReplyName';
import UserAvatar from '../general/UserAvatar';
import { RichTextEditor } from '../inputs';

interface IProps {
	user: IApiUser;
	userAuthenticated: boolean | null;
	isReplyOpen: boolean | null;
	replyTo: IReplyTo | null;
	dispatch(values: any): void;
	replyToDiscussion(event, replyTo?): void;
	reloadPage(index?: number): void;
	submitReply(values: any, id?: number): Promise<boolean>;
}

interface IState {
	userAuthenticated: boolean | null;
	isReplyOpen: boolean | null;
	submitting: boolean;
}

class DiscussionReply extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			userAuthenticated: props.userAuthenticated,
			isReplyOpen: props.isReplyOpen,
			submitting: false,
		};
	}

	public render() {
		const validationFormat = Yup.object().shape({
			content: Yup.string()
				.required('A reply is required')
				.max(10000),
		});

		const discussionReplyClasses = [
			'discussion_reply',
			'discussion_reply--closed',
		];

		if (this.props.isReplyOpen) {
			const classIndex = discussionReplyClasses.indexOf(
				'discussion_reply--closed',
			);

			if (classIndex !== -1) {
				discussionReplyClasses.splice(classIndex, 1);
			}

			discussionReplyClasses.push('discussion_reply--open');
		}

		return (
			<div className={discussionReplyClasses.join(' ')}>
				<Formik
					initialValues={{
						content: '',
					}}
					validationSchema={validationFormat}
					onSubmit={(values, { resetForm, setValues }) => {
						this.setState(
							{
								submitting: true,
							},
							() => {
								this.props
									.submitReply(
										values,
										this.props.replyTo
											? this.props.replyTo.id
											: undefined,
									)
									.then(result => {
										if (result) {
											resetForm({
												content: '',
											});
											setValues({
												content: '',
											});

											this.props.reloadPage();
										}
										this.setState({
											submitting: false,
										});
									});
							},
						);
					}}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};

						return (
							<form
								className="discussion_reply-form form"
								onSubmit={handleSubmit}
							>
								<div className="discussion_reply-author">
									<div className="discussion_reply-author_avatar">
										<UserAvatar user={this.props.user} />
									</div>
								</div>

								<div className="discussion_reply-form_textarea">
									{this.props.replyTo !== null && (
										<div className="discussion_reply-form_reply">
											<DiscussionReplyName
												deletable={true}
												onClick={() => {
													if (
														!this.state.submitting
													) {
														this.props.replyToDiscussion(
															null,
														);
													}
												}}
												disabled={this.state.submitting}
											>
												{this.props.replyTo.user}
											</DiscussionReplyName>
										</div>
									)}

									{/* <DiscusionReplyTextFieldFormik
										type="textarea"
										label="Content"
										name="content"
										multiline={true}
										placeholder="Write your reply here…"
										disabled={this.state.submitting}
										{...inputProps}
									/> */}

									<RichTextEditor
										toolbarId="discussion-reply-toolbar"
										name="content"
										value={inputProps.values.content}
										profile={[
											'imageUpload',
											'embedVideo',
											'insertGif',
											'mention',
										]}
										bounds=".discussion_reply-form_textarea"
										scrollingContainer=".discussion_reply-form_textarea"
										{...inputProps}
									/>
								</div>

								<div className="discussion_reply-form_submit">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid || this.state.submitting
										}
									>
										<ReactSVG
											style={{ pointerEvents: 'none' }}
											path={`${process.env.STATIC_PATH}/icons/reply.svg`}
										/>
									</Button>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(DiscussionReply);
