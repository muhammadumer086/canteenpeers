import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import { Button } from '@material-ui/core';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import HeroHeading from '../../../components/general/HeroHeading';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import ListLoading from '../../../components/general/ListLoading';
import AdminAlertList from '../../../components/admin/alerts/AdminAlertList';

import { IAdminProps } from '../../../components/admin/withAdminPage';
import {
	IApiAnnouncement,
	IApiSituation,
	IStaticStates,
	IStaticAgeRange,
} from '../../../interfaces';
import { apiSuccess, apiError } from '../../../redux/store';
import ModalDialogDeleteConfirm from '../../modals/ModalDialogDeleteConfirm';
import { ModalDialog } from '../../modals/local';
import EditAlertFormik from '../alerts/EditAlertFormik';
import {
	generateStates,
	generateAgeRanges,
} from '../../../helpers/generateStaticUserData';
import CreateNewAlertFormik from '../alerts/CreateNewAlertFormik';
import { checkPermission } from "../../../helpers/checkPermission";
export enum EAlertManagmentPermissions{
	READ="alert_management_read",
	ADD="alert_management_add",
	EDIT="alert_management_edit",
	DELETE="alert_management_delete",
	ALL="alert_management_all",
}

interface IProps extends IAdminProps { }

interface IState {
	data: IApiAnnouncement[];
	idToDelete: number | null;
	isLoading: boolean;
	alertDeleteData: IApiAnnouncement | null;
	alertEditData: IApiAnnouncement | null;
	isEditModalActive: boolean;
	isCreateModalActive: boolean;
	isDeleteModalActive: boolean;
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;

	situations: IApiSituation[];
	states: IStaticStates[];
	ageRanges: IStaticAgeRange[];
	isLoadingPermission: boolean;
	permissions: string[];
}

class AdminAlertManagement extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			data: null,
			idToDelete: null,
			alertDeleteData: null,
			alertEditData: null,
			isLoading: false,
			isEditModalActive: false,
			isCreateModalActive: false,
			isDeleteModalActive: false,
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			situations: null,
			states: generateStates(),
			ageRanges: generateAgeRanges(),
			isLoadingPermission: false,
			permissions: []
		};

		this.toggleCreateModal = this.toggleCreateModal.bind(this);
		this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
		this.toggleEditModal = this.toggleEditModal.bind(this);

		this.handleDeleteAlert = this.handleDeleteAlert.bind(this);
		this.handleEditAlert = this.handleEditAlert.bind(this);

		this.setAlertToDelete = this.setAlertToDelete.bind(this);
		this.setAlertEditData = this.setAlertEditData.bind(this);

		this.handleCreateAlert = this.handleCreateAlert.bind(this);

		this.initialise = this.initialise.bind(this);
		this.loadAlerts = this.loadAlerts.bind(this);

	}

	public componentDidMount() {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.READ);

		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.READ);

		if (readPermission) {
			this.initialise(this.props, prevProps);
		}
	}

	public render() {
		const addPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.ADD);
		const editPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.EDIT);
		const deletePermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.DELETE);
		console.log("Edit Permission");
		console.log(editPermission);
		console.log(deletePermission);
		return (
			<div>
				<Head>
					<title>Alerts Management — {process.env.APP_NAME}</title>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading title="Alert Managment" />
						{addPermission && <span className="hero-actions">
							<Button
								variant="outlined"
								color="primary"
								onClick={() => {
									this.toggleCreateModal();
								}}
							>
								Add Alert
							</Button>
						</span>}
					</header>

					<div className="admin-alerts">
						{this.state.isLoading && (
							<div className="hm-t64">
								<ListLoading active={true} />
							</div>
						)}

						{!this.state.isLoading &&
							!!this.state.data &&
							this.state.data.length > 0  && (
								<AdminAlertList
									alerts={this.state.data}
									toggleEditModal={editPermission? this.setAlertEditData:undefined}
									toggleDeleteModal={deletePermission? this.setAlertToDelete:undefined}
								/>
							)}
					</div>
				</LayoutDashboard>

				{this.state.isCreateModalActive && (
					<ModalDialog
						isActive={true}
						modalTitle="Create alert"
						ariaTag="create-alert-modal"
						handleClose={this.toggleCreateModal}
					>
						<CreateNewAlertFormik
							isSubmitting={this.state.isLoading}
							situations={this.state.situations}
							ageRanges={this.state.ageRanges}
							states={this.state.states}
							onSubmit={this.handleCreateAlert}
						/>
					</ModalDialog>
				)}

				{this.state.isEditModalActive && !!this.state.alertEditData && (
					<ModalDialog
						isActive={true}
						modalTitle="Edit alert"
						ariaTag="edit-alert-modal"
						handleClose={this.toggleEditModal}
					>
						<EditAlertFormik
							alertData={this.state.alertEditData}
							isSubmitting={this.state.isLoading}
							situations={this.state.situations}
							ageRanges={this.state.ageRanges}
							states={this.state.states}
							onSubmit={this.handleEditAlert}
						/>
					</ModalDialog>
				)}

				{this.state.isDeleteModalActive &&
					!!this.state.alertDeleteData && (
						<ModalDialog
							isActive={true}
							modalTitle="Are you sure you want to delete the below alert?"
							ariaTag="delete-alert-modal"
							handleClose={this.toggleDeleteModal}
						>
							<ModalDialogDeleteConfirm
								name={this.state.alertDeleteData.title}
								loading={this.state.isLoading}
								submit={this.handleDeleteAlert}
								cancel={this.toggleDeleteModal}
							/>
						</ModalDialog>
					)}
			</div>
		);
	}

	protected loadAlerts(append: boolean = true) {
		if (!this.state.isLoading) {
			let params;

			if (append) {
				params.page = this.state.currentPage + 1;
			}

			const url = buildUrl(null, {
				path: '/api/announcements',
				queryParams: params,
			});

			this.setState(
				{
					isLoading: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'announcements' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								let data: IApiAnnouncement[] =
									response.data.announcements;

								this.setState({
									data,
									isLoading: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
								});
							}
						});
				},
			);
		}
	}

	protected toggleCreateModal() {
		this.setState({
			isCreateModalActive: !this.state.isCreateModalActive,
		});
	}

	protected setAlertToDelete(alertDeleteData: IApiAnnouncement) {
		this.setState(
			{
				alertDeleteData,
			},
			() => {
				this.toggleDeleteModal();
			},
		);
	}

	protected toggleDeleteModal() {
		this.setState({
			isDeleteModalActive: !this.state.isDeleteModalActive,
		});
	}

	protected setAlertEditData(alertEditData: IApiAnnouncement) {
		this.setState(
			{
				alertEditData,
			},
			() => {
				this.toggleEditModal();
			},
		);
	}

	protected toggleEditModal() {
		this.setState({
			isEditModalActive: !this.state.isEditModalActive,
		});
	}

	protected handleDeleteAlert() {
		if (this.state.alertDeleteData !== null) {
			this.setState(
				{
					isLoading: true,
				},
				() => {
					this.props.apiService
						.queryDELETE(
							`/api/announcements/${this.state.alertDeleteData.id
							}`,
						)
						.then(() => {
							this.setState(
								{
									isLoading: false,
									isDeleteModalActive: !this.state
										.isDeleteModalActive,
								},
								() => {
									this.loadAlerts(false);
									this.props.dispatch(
										apiSuccess([
											'Alert successfully deleted',
										]),
									);

									this.toggleDeleteModal();
								},
							);
						})
						.catch(() => {
							this.setState(
								{
									isLoading: false,
									isDeleteModalActive: !this.state
										.isDeleteModalActive,
								},
								() => {
									this.loadAlerts(false);
									this.props.dispatch(
										apiError(['Alert was not deleted']),
									);
								},
							);
						});
				},
			);
		}
	}

	protected handleEditAlert(values: any) {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/announcements/${this.state.alertEditData.id}`,
						values,
					)
					.then(() => {
						this.setState(
							{
								isLoading: false,
								isEditModalActive: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess(['Alert successfully edited']),
								);

								this.loadAlerts(false);
							},
						);
					})
					.catch(err => {
						this.setState(
							{
								isLoading: false,
							},
							() => {
								this.props.dispatch(apiError([err.message]));
							},
						);
					});
			},
		);
	}

	protected handleCreateAlert(values) {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/announcements', values)
					.then(() => {
						this.setState({
							isLoading: false,
							isCreateModalActive: false,
						});

						this.props.dispatch(
							apiSuccess([`Alert was created successfully`]),
						);

						this.loadAlerts(false);
					})
					.catch(error => {
						this.setState({
							isLoading: false,
						});

						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(props.permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.READ);
		const permissions=prevProps?prevProps.permissions:[];
		const prevReadPermission: boolean =this.props.roleName==="admin"|| checkPermission(permissions,EAlertManagmentPermissions.ALL,EAlertManagmentPermissions.READ);
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin||(!prevReadPermission&&readPermission))
		) {
			this.loadAlerts(false);
			this.loadSituations();
		}
	}

	protected loadSituations() {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryGET('/api/situations', 'situations')
					.then((situationsList: IApiSituation[]) => {
						const userSituations: IApiSituation[] = [];
						if (situationsList instanceof Array) {
							situationsList.map((situation: IApiSituation) => {
								if (!situation.cancer_type) {
									userSituations.push(situation);
								}
							});
						}

						this.setState({
							situations: userSituations,
							isLoading: false,
						});
					})
					.catch(error => {
						this.props.dispatch(apiError(error));

						this.setState({
							situations: null,
							isLoading: false,
						});
					});
			},
		);
	}
	
}

export default AdminAlertManagement;
