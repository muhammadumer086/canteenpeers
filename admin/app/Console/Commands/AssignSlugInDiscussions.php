<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Discussion;

class AssignSlugInDiscussions extends Command {
	use \App\Traits\CreateSlug;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:assignSlugInDiscussions';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a slug for any main discussion that doesn\'t have a slug';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->info('Setting the slug on all the discussions');
		$bar = $this->output->createProgressBar(
			Discussion::whereNull('main_discussion_id')
				->whereNull('slug')
				->count()
		);
		// Index all the discussions
		Discussion::whereNull('main_discussion_id')
			->whereNull('slug')
			->chunk(10, function ($discussions) use ($bar) {
				foreach ($discussions as $discussion) {
					$discussion->slug = $this->createSlug(
						$discussion->title,
						'App\Discussion'
					);
					$discussion->save(['timestamps' => false]);
					$bar->advance();
				}
			});
		$bar->finish();
		$this->info("\r\nFinished indexing all discussions");
	}
}
