import React from 'react';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';
import * as Yup from 'yup';
import { IApiUser } from '../../interfaces';
import { validationMessage } from '../../helpers/validations';

import { TextFieldFormik } from '../inputs';
import { IFormValues } from './CreateNewMessageModal';

interface IProps {
	userData: IApiUser | null;
	submitting: boolean;
	showUserSelect: IApiUser;
	onSubmit(values: IFormValues);
}

const NewMessageForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		users: Yup.array().required('At least one username is required'),
		message: validationMessage.message,
	});

	const pushUserNameToArray = () => {
		let arrayUserName = [];

		arrayUserName.push(props.showUserSelect.username);

		return arrayUserName;
	};

	return (
		<Formik
			initialValues={{
				users: pushUserNameToArray(),
				message: '',
			}}
			validationSchema={validationFormat}
			onSubmit={props.onSubmit}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				handleSubmit,
				setFieldValue,
				setFieldTouched,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
				};

				return (
					<form className="auth_form form" onSubmit={handleSubmit}>
						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="Message"
									name="message"
									disabled={props.submitting}
									multiline={true}
									{...inputProps}
								/>
							</div>
						</div>

						<div className="form-row form-row--submit">
							<div className="form-column">
								<Button
									variant="contained"
									color="primary"
									className="form-submit"
									type="submit"
									disabled={!isValid || props.submitting}
								>
									Send Message
								</Button>
							</div>
						</div>
					</form>
				);
			}}
		/>
	);
};

export default NewMessageForm;
