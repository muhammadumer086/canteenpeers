<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Situation;

use Carbon\Carbon;

class SituationsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$situations = [
			"I'm a young person with cancer",
			'My parent has cancer',
			'My sibling has cancer',
			'I survived cancer',
			'My parent survived cancer',
			'My sibling survived cancer',
			'My parent died from cancer',
			'My sibling died from cancer',
		];

		$cancers = [
			'Anal cancer',
			'Bladder cancer',
			'Bone cancer',
			'Bowel (colorectal) cancer',
			'Brain or central nervous system cancer',
			'Breast cancer',
			'Breast cancer in men',
			'Cervical cancer',
			'Gallbladder cancer',
			'Head and neck cancer (including mouth and throat)',
			'Kidney cancer',
			'Leukaemia',
			'Liver or bile duct cancer',
			'Lung cancer',
			'Lymphoma (including Hodgkin and non-Hodgkin Lymphoma)',
			'Mesothelioma',
			'Melanoma',
			'Multiple myeloma',
			'Myelodysplastic syndromes',
			'Oesophageal cancer',
			'Ovarian cancer',
			'Pancreatic cancer',
			'Prostate cancer',
			'Soft tissue sarcoma',
			'Skin cancer (other than melanoma)',
			'Stomach cancer',
			'Testicular cancer',
			'Thyroid cancer',
			'Unknown Primary',
			'Uterine cancer',
			'Vulvar or vaginal cancer',
			'Other',
		];

		$now = Carbon::now();

		// Move all the users to the primary situation
		$situationsToMigrate = Situation::whereNotNull('parent_id')->get();
		foreach ($situationsToMigrate as $singleSituation) {
			DB::table('situation_user')
				->where('situation_id', $singleSituation->id)
				->update(['situation_id' => $singleSituation->parent_id]);
		}

		foreach ($situations as $situation) {
			$slug = str_slug($situation, '-');
			$parentSituation = Situation::updateOrCreate(
				[
					'slug' => $slug,
				],
				[
					'name' => $situation,
				]
			);
			$parentSituation->touch();

			foreach ($cancers as $cancer) {
				$slug =
					str_slug($situation, '-') . '-' . str_slug($cancer, '-');

				$childSituation = Situation::updateOrCreate(
					[
						'slug' => $slug,
					],
					[
						'name' => $situation,
						'cancer_type' => $cancer,
						'parent_id' => $parentSituation->id,
					]
				);
				$childSituation->touch();
			}
		}

		// Delete all the topics that haven't been updated
		Situation::where('updated_at', '<', $now)->delete();
	}
}
