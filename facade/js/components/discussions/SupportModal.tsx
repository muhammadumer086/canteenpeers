import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import buildUrl from 'build-url';
import { ModalDialog } from '../../components/modals/local';

import {
	IStoreState,
	discussionSupportHide,
	apiError,
	userPanelDisplay,
	// apiError,
	// apiSuccess,
} from '../../redux/store';

import { IApiDiscussion, IApiSupport } from '../../interfaces';
import { withServices, ApiService } from '../../services';
import { SupportDetails } from './SupportDetails';

interface IProps {
	userAuthenticated: boolean;
	discussionSupportDisplay: boolean;
	discussionData: IApiDiscussion;
	apiService: ApiService;
	dispatch(action: any): void;
}

const SupportModal: React.FC<IProps> = props => {
	const isUnmounted = React.useRef(false);
	// const [currentPage, setCurrentPage] = useState<number>(1);
	// const [maxPage, setMaxPage] = useState<number>(0);
	const [total, setTotal] = useState<number>(0);
	const [loading, setLoading] = useState<boolean>(false);
	const [supports, setSupports] = useState<IApiSupport[]>([]);
	const isActive = !!(
		props.discussionSupportDisplay && props.userAuthenticated
	);

	useEffect(() => {
		console.log('mounted');
		return () => {
			isUnmounted.current = true;
		};
	}, []);

	useEffect(() => {
		// setCurrentPage(1);
		// setMaxPage(0);
		setTotal(0);
		setLoading(false);
		setSupports([]);
		if (props.discussionData && isActive) {
			loadSupport(1);
		}
	}, [props.discussionData, isActive]);

	return (
		<ModalDialog
			isActive={isActive}
			ariaTag="discussion-support"
			variant="small"
			handleClose={handleClose}
		>
			{!!props.discussionData && (
				<SupportDetails
					loading={loading}
					supports={supports}
					totalAll={total}
					totalLike={props.discussionData.likes_count}
					totalHug={props.discussionData.hugs_count}
					totalReply={props.discussionData.replies_count}
					displayUser={displayUser}
				/>
			)}
		</ModalDialog>
	);

	function displayUser(item: IApiSupport) {
		props.dispatch(discussionSupportHide());
		props.dispatch(userPanelDisplay(item.user));
	}

	function handleClose() {
		props.dispatch(discussionSupportHide());
	}

	function loadSupport(page: number) {
		if (!loading && props.discussionData) {
			setLoading(true);
			const url = buildUrl(null, {
				path: `/api/discussions/${props.discussionData.id}/supports`,
				queryParams: {
					page,
				},
			});

			props.apiService
				.queryGET(url)
				.then((response: any) => {
					if (!isUnmounted.current) {
						if (
							'data' in response &&
							'supports_users' in response.data &&
							'meta' in response &&
							'current_page' in response.meta &&
							'last_page' in response.meta &&
							'total' in response.meta
						) {
							// setCurrentPage(response.meta.current_page);
							// setMaxPage(response.meta.last_page);
							setTotal(response.meta.total);
							setSupports([
								...supports,
								...response.data.supports_users,
							]);
						}
						setLoading(false);
					}
				})
				.catch(error => {
					// dispatch the error message
					if (!isUnmounted.current) {
						props.dispatch(apiError([error.message]));
						setLoading(false);
					}
				});
		}
	}
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	const {
		discussionSupportDisplay,
		discussionData,
	} = state.discussionSupport;
	return {
		userAuthenticated,
		discussionSupportDisplay,
		discussionData,
	};
}

export default connect(mapStateToProps)(withServices(SupportModal));
