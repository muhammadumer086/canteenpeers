<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Announcement;
use App\Discussion;

class Notification extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'notification',
				$this->id,
				$this->updated_at,
				true,
				true
			)
		) {
			return $apiStore->getApiResource(
				'notification',
				$this->id,
				$this->updated_at,
				true,
				true
			);
		}

		$params = ['id', 'data', 'read_at', 'created_at', 'updated_at'];

		$result = [];

		// Process the type
		$type = explode('\\', $this->type);
		$result['type'] = array_pop($type);

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);
		$result['read_at'] = UserPermissionHelper::formatCarbonDate($result['read_at']);


		// Hide the notification is the subject is deleted
		if (!$this->shouldDisplayNotification()) {
			return null;
		}

		$apiStore->setApiResource(
			$result,
			'notification',
			$this->id,
			$this->updated_at,
			true,
			true
		);

		return $result;
	}

	public function shouldDisplayNotification() {
		$explodedType = explode('\\', $this->type);
		$type = array_pop($explodedType);

		$functionName = 'shouldDisplay' . $type;

		if (method_exists($this, $functionName)) {
			return $this->$functionName();
		}

		return true;
	}

	public function shouldDisplayNewAnnouncementNotification() {
		if (
			isset(
				$this->data,
				$this->data['announcement'],
				$this->data['announcement']['id']
			)
		) {
			// Try to get the announcement
			return Announcement::where(
				'id',
				$this->data['announcement']['id']
			)->exists();
		}
		return false;
	}

	public function shouldDisplayDiscussionReportNotification() {
		if (
			isset(
				$this->data,
				$this->data['discussion'],
				$this->data['discussion']['id']
			)
		) {
			return Discussion::where(
				'id',
				$this->data['discussion']['id']
			)->exists();
		}
		return false;
	}

	public function shouldDisplayDiscussionReplyNotification() {
		if (
			isset(
				$this->data,
				$this->data['discussion'],
				$this->data['discussion']['id']
			)
		) {
			return Discussion::where(
				'id',
				$this->data['discussion']['id']
			)->exists();
		}
		return true;
	}

	public function shouldDisplayUserMentionedNotification() {
		if (
			isset(
				$this->data,
				$this->data['discussion'],
				$this->data['discussion']['id']
			)
		) {
			return Discussion::where(
				'id',
				$this->data['discussion']['id']
			)->exists();
		}
		return true;
	}

	public function shouldDisplayNewBlogArticleNotification() {
		if (
			isset($this->data, $this->data['blog'], $this->data['blog']['id'])
		) {
			return Discussion::where('id', $this->data['blog']['id'])->exists();
		}
		return true;
	}
}
