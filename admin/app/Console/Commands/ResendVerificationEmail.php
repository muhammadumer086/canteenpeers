<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\User;

class ResendVerificationEmail extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:resendVerificationEmail';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Re-send a verification email to the users that haven\'t verified their account in the past 24h';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		// Get the current time
		$intervalStart = Carbon::now()->subHours(24);
		$intervalEnd = Carbon::now()->subHours(23);

		// Select unverified users
		$usersQuery = User::where('created_at', '>=', $intervalStart)
			->where('created_at', '<', $intervalEnd)
			->where('verified', false);

		$usersQuery->chunk(10, function ($users) {
			foreach ($users as $user) {
				$user->resendVerificationNotification();
			}
		});

		// // Send additonal verification after a week
		// $intervalStart = Carbon::now()->subHours(24 * 7);
		// $intervalEnd = Carbon::now()->subHours(24 * 7 - 1);
		//
		// $auth = [
		// 	'api_key' => config('canteen.campaign_monitor_key'),
		// ];
		// $smartEmailId = '7457548a-2d23-4f9b-b9b7-221ef54c2240';
		// $wrap = new \CS_REST_Transactional_SmartEmail($smartEmailId, $auth);
		//
		// // Select unverified users
		// $usersQuery = User::where('created_at', '>=', $intervalStart)
		// 	->where('created_at', '<', $intervalEnd)
		// 	->where('verified', false);
		//
		// $usersQuery->chunk(10, function ($users) use ($wrap) {
		// 	foreach ($users as $user) {
		// 		$message = [
		// 			'To' => "{$user->first_name} {$user->last_name} <{$user->email}>",
		// 			'Data' => [
		// 				'firstname' => $user->first_name,
		// 				'button' => config('canteen.facade_url') . '/auth/verify?' . http_build_query([
		// 					'token' => $user->verification_token,
		// 				])
		// 			],
		// 		];
		//
		// 		$consentToTrack = 'unchanged';
		//
		// 		$wrap->send($message, $consentToTrack);
		// 	}
		// });
	}
}
