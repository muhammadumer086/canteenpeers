import React from 'react';
import { connect } from 'react-redux';

import Button     from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/ButtonBase';
import Checkbox   from '@material-ui/core/Checkbox';

import {
	IStoreState,
	apiError
} from '../../redux/store';

import {
	IApiSituation,
	IApiTopic,
} from '../../interfaces';

import { ApiService } from '../../services';

interface IProps {
	title: string;
	type: string;
	situations: IApiSituation[];
	topics: IApiTopic[];
	apiService: ApiService;
	editPermission : boolean;
	handleTopicsUpdate(topics: IApiTopic[]): void;
	dispatch(action: any): void;
}

interface IState {
	loading: boolean;
	changed: boolean;
	topics: IApiTopic[];
}

class TopicsGrid extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			changed: false,
			topics: JSON.parse(JSON.stringify(props.topics)),
		};

		this.handleChange = this.handleChange.bind(this);
		this.saveTopics = this.saveTopics.bind(this);
		this.cancelChanges = this.cancelChanges.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.topics !== prevProps.topics) {
			this.setState({
				topics: JSON.parse(JSON.stringify(this.props.topics)),
			});
		}
	}

	public render() {
		return (
			<section className="admin_topics_grid hm-t32">
				<header className="hm-b32">
					<h1 className="font--h3">{this.props.title}</h1>
				</header>
				<div className="admin_topics_grid-content">
					<div>
						<table className="admin_topics_grid-content-table">
							<thead>
								<tr>
									<td></td>
									{
										this.props.situations.map((situation, index) => {
											if (situation.cancer_type) {
												return null;
											}
											return (
												<th key={index}>{situation.name}</th>
											);
										})
									}
								</tr>
							</thead>
							<tbody>
								{
									this.state.topics.map((topic, topicIndex) => {
										if (topic.category !== this.props.type) {
											return null;
										}
										const situationIds = topic.situations.map((situation) => {
											return situation.id;
										});

										return (
											<tr key={topicIndex}>
												<th>{topic.title}</th>
												{
													this.props.situations.map((situation, index) => {
														if (situation.cancer_type) {
															return null;
														}
														const isActive = (situationIds.indexOf(situation.id) >= 0);
														const classNames = [
															'admin_topics_grid-content-table-cell',
														];
														if (isActive) {
															classNames.push('admin_topics_grid-content-table-cell--active');
														}

														return (
															<td key={index} className={classNames.join(' ')}>
																<Checkbox
																	checked={isActive}
																	onChange={() => {this.handleChange(topicIndex, situation)}}
																	disabled={this.state.loading}
																/>
																<ButtonBase
																	focusRipple
																	className='admin_topics_grid-content-table-cell-button'
																	onClick={() => {this.handleChange(topicIndex, situation)}}
																	tabIndex={-1}
																	disabled={this.state.loading}
																>
																</ButtonBase>
															</td>
														);
													})
												}
											</tr>
										);
									})
								}
							</tbody>
						</table>
					</div>
					{
						this.state.changed &&this.props.editPermission&&
						<div className="hm-t32">
							<Button
								variant="contained"
								color="primary"
								onClick={this.saveTopics}
								disabled={this.state.loading}
							>
								Save Changes
							</Button>
							<span className="hm-l16">
								<Button
									onClick={this.cancelChanges}
									disabled={this.state.loading}
								>
									Cancel
								</Button>
							</span>
						</div>
					}
				</div>
			</section>
		)
	}

	protected handleChange(topicIndex, situation: IApiSituation) {
		if(!this.props.editPermission){
			return;
		}
		// Duplicate the topics
		const topics: IApiTopic[] = JSON.parse(JSON.stringify(this.state.topics));

		const situationIds = topics[topicIndex].situations.map((situation) => {
			return situation.id;
		});

		const situationIndex = situationIds.indexOf(situation.id);

		if (situationIndex >= 0) {
			topics[topicIndex].situations.splice(situationIndex, 1);
		} else {
			topics[topicIndex].situations.push(JSON.parse(JSON.stringify(situation)));
		}

		this.setState({
			topics,
			changed: true,
		});
	}

	protected saveTopics() {
		this.setState({
			loading: true,
		}, () => {
			// Process the data
			const data = this.state.topics.map((topic) => {
				return {
					id: topic.id,
					situationIds: topic.situations.map((situation) => {
						return situation.id;
					}),
				};
			});

			this.props.apiService.queryPUT('/api/topics', {
				topics: data
			}, 'topics')
			.then((topics: IApiTopic[]) => {
				this.setState({
					loading: false,
					changed: false,
				});
				this.props.handleTopicsUpdate(topics);
			})
			.catch((error) => {
				// dispatch the error message
				this.props.dispatch(apiError([
					error.message
				]));
				this.setState({
					loading: false,
				});
			});
		});
	}

	protected cancelChanges(){
		this.setState({
			topics: JSON.parse(JSON.stringify(this.props.topics)),
			changed: false,
		})
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(TopicsGrid);
