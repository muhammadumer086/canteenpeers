<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use League\Csv\Writer;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

use Image;

use App\Topic;

abstract class CsvMigration {
	/**
	 * The headers for the raw CSV
	 *
	 * @var array
	 */
	public $rawCsvHeaders = [];
	/**
	 * The headers for the CSV
	 *
	 * @var array
	 */
	public $csvHeaders = [];

	public function __construct() {
	}

	abstract public function processRecord(Writer $writer, $record);

	abstract public function importData($record);

	abstract protected function formatRecord($data, $record);

	protected function processContent($content = '', $allowHeadings = false) {
		// process the Wordpress content and format the HTML accordingly

		// replace strong tags with b tags
		$content = str_replace('<strong', '<b', $content);
		$content = str_replace('</strong', '</b', $content);
		// replace em tags with b tags
		$content = str_replace('<em', '<b', $content);
		$content = str_replace('</em', '</b', $content);
		// Add line break for each new paragraph
		$content = str_replace('<p', "\r\n<p", $content);

		// Remove non-supported tags
		$allowedTags = '<p><b><ul><ol><li><a><img><br>';
		if ($allowHeadings) {
			$allowedTags .= '<h1><h2><h3><h4>';
		}
		$content = strip_tags($content, $allowedTags);

		// Standardise new lines
		$content = preg_replace('/\R/u', "\r\n", $content);

		// Remove tabs
		$content = trim(preg_replace('/\t+/', '', $content));
		$content = trim(preg_replace('/^\s+/m', '', $content));

		// Remove HTML attributes
		$content = preg_replace('/class\=\"+[^\"]+\"+/', '', $content);
		$content = preg_replace('/style\=\"+[^\"]+\"+/', '', $content);

		// Split each line
		$contentParts = explode("\r\n", $content);
		if (is_array($contentParts) && sizeof($contentParts)) {
			$newContentParts = [];
			$match = '/^\<\/?(?:ul|ol|li|p)/';
			if ($allowHeadings) {
				$match = '/^\<\/?(?:ul|ol|li|p|h1|h2|h3|h4)/';
			}
			foreach ($contentParts as $part) {
				// Add the <p> tag to the line only if it doesn't already start with a block tag
				if (!preg_match($match, $part)) {
					$part = '<p>' . $part . '</p>';
				}
				$newContentParts[] = $part;
			}
			$content = implode("\r\n", $newContentParts);
		}
		// Remove empty paragraphs
		$content = str_replace("\r\n<p></p>", '', $content);
		$content = str_replace("\r\n<p> </p>", '', $content);
		$content = str_replace("\r\n<p>&nbsp;</p>", '', $content);

		return $content;
	}

	protected function nullIfEmpty($value) {
		if (empty($value)) {
			return null;
		}
		return $value;
	}

	protected function downloadImage($path, $stripWordpressSize = true) {
		if ($stripWordpressSize) {
			$path = $this->stripWordpressImageSize($path);
		}
		$pathSplit = explode('/', $path);
		$fileName = array_pop($pathSplit);

		$tmpImage = tempnam(sys_get_temp_dir(), $fileName);
		$handle = fopen($tmpImage, 'w');

		$client = new Client([
			'base_uri' => '',
			'verify' => false,
			'sink' => $tmpImage,
			'curl.options' => [
				'CURLOPT_RETURNTRANSFER' => true,
				'CURLOPT_FILE' => $handle,
			],
		]);

		$client->get($path);
		fclose($handle);
		return $tmpImage;
	}

	protected function stripWordpressImageSize($image) {
		return preg_replace('/\-[0-9]+x[0-9]+\./', '.', $image);
	}

	protected function processImportContent($data) {
		$content = $this->nullIfEmpty($data);

		if ($content) {
			$dom = HtmlDomParser::str_get_html($content);
			$this->processImportContentLinks($dom);

			return $dom->save();
		}

		return '';
	}

	protected function processImportContentLinks(&$dom) {
		$dataUsername = 'data-username';
		// Get the links from the content
		foreach ($dom->find('a') as &$link) {
			if ($link->find('img', 0)) {
				// Process the image link by replacing the link by the image directly
				$img = $link->find('img', 0);
				if ($img->src) {
					$newSrc = $this->migrateImage($img->src);
					// Replace the link by the image
					$link->outertext = "<img src=\"$newSrc\" alt=\"$img->alt\"/>";
				}
			} elseif ($link->$dataUsername) {
				// User mention
				$link->outertext = "<span class=\"mention-blot\"><span contenteditable=\"false\"><span>@{$link->$dataUsername}</span></span></span>";
			} else {
				// Regular link, open a new tab
				$link->target = '_blank';
				$link->rel = 'noopener noreferrer';
			}
		}
	}

	protected function migrateImage($src) {
		// Remove the wordpress image size
		$fullSizeSrc = $this->stripWordpressImageSize($src);
		// Download the image
		$tmpImage = $this->downloadImage($fullSizeSrc);
		// Create a unique hash based on the src
		$hash = md5($src);
		// Get the file extension
		$explodedFile = explode('.', $src);
		$extension = array_pop($explodedFile);

		if ($extension !== 'gif') {
			// Process the image to resize it if it's not a gif
			$image = Image::make($tmpImage)->orientate();
			$image->widen(min($image->width(), 960));
			$hashTmp = md5($image->__toString());
			$tmpImage = storage_path("images/{$hashTmp}.{$extension}");
			$image->save($tmpImage, 70);
			$image->destroy();
		}

		// Upload the image
		$path = Storage::disk('s3')->putFileAs(
			'migration',
			new File($tmpImage),
			"{$hash}.{$extension}",
			'public'
		);

		unlink($tmpImage);

		return Storage::disk('s3')->url($path);
	}

	protected function formatTopics($record, $topicsMap) {
		$topics = [];

		foreach ($topicsMap as $key => $value) {
			if ($record[$key] && !empty($record[$key])) {
				$topics[] = $value;
			}
		}

		if (sizeof($topics)) {
			return $topics[0];
		}

		return '';
	}

	protected function processTopics($entity, $data, $category) {
		$rawTopic = $this->nullIfEmpty($data);
		$topic = null;
		if ($rawTopic) {
			$topic = Topic::where('title', $rawTopic)
				->where('category', $category)
				->firstOrFail();
		}

		if ($topic) {
			$entity->topic()->associate($topic);
			$entity->save();
		}
	}
}
