import React from 'react';
import ReactSVG from 'react-svg';

interface IProps {
	title: string;
	iconSlug?: string;
	subInfo?: string | any;
}

const HeroHeading: React.StatelessComponent<IProps> = props => {
	return (
		<div className="hero-heading">
			{props.iconSlug && (
				<span className="hero-heading-icon">
					<ReactSVG
						path={`${process.env.STATIC_PATH}/icons/${
							props.iconSlug
						}.svg`}
					/>
				</span>
			)}

			<div className="hero-heading-text-container">
				<span
					className="hero-heading-text font--h4"
					dangerouslySetInnerHTML={{
						__html: props.title,
					}}
				/>

				{props.subInfo && (
					<div className="hero-secondary_info">{props.subInfo}</div>
				)}
			</div>
		</div>
	);
};

export default HeroHeading;
