import React from 'react';
import { Button } from '@material-ui/core';
import { IApiDiscussion } from '../../interfaces';

interface IProps {
	discussionToDelete?: IApiDiscussion | null;
	name: string;
	loading: boolean;
	submit(): void;
	cancel(): void;
}

const ModalDialogDeleteConfirm: React.StatelessComponent<IProps> = props => {
	return (
		<React.Fragment>
			{props.discussionToDelete ? (
				<span className="delete_staff_modal-staff_title font--bold" />
			) : (
				<span className="delete_staff_modal-staff_title font--bold">
					{props.name}
				</span>
			)}

			<div
				style={{
					display: 'flex',
					justifyContent: 'center',
				}}
			>
				<Button
					onClick={props.submit}
					color="primary"
					variant="contained"
					disabled={props.loading}
				>
					Delete
				</Button>
				&nbsp;
				<Button
					onClick={props.cancel}
					color="secondary"
					variant="contained"
					disabled={props.loading}
				>
					Cancel
				</Button>
			</div>
		</React.Fragment>
	);
};

export default ModalDialogDeleteConfirm;
