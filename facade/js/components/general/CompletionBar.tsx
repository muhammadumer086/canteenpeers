import React from 'react';

import LinearProgress from '@material-ui/core/LinearProgress';

interface IProps {
	value: number;
}

const CompletionBar: React.FC<IProps> = (props: IProps) => {
	const classes = {
		root: 'completion_bar-track',
		bar: 'completion_bar-bar',
	};

	return (
		<div className="completion_bar">
			<LinearProgress
				classes={classes}
				variant="determinate"
				value={props.value}
			/>
		</div>
	)
}

export default CompletionBar;
