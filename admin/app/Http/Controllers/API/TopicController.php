<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Topic;

use App\Http\Resources\Topic as TopicResource;

class TopicController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$topics = Topic::orderBy('slug', 'ASC')->get();

		// foreach ($topics as &$singleTopic) {
		// 	$singleTopic->situations;
		// }

		return response()->json([
			'topics' => TopicResource::collection($topics),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$validator = Validator::make($request->all(), [
			'title'    => 'bail|required|string|max:191',
			'category' => 'bail|required|string|max:191|in:blogs,discussions,resources',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json([
				'error' => $error,
			], 400);
		}

		$originalSlug = Str::slug($request->input('category') . " " . $request->input('title'), '-');
		$slug = $originalSlug;
		$index = 0;
		$topicFound = false;
		// Test if a topic already has the slug
		do {
			if (Topic::where('slug', $slug)->exists()) {
				$topicFound = true;
				++$index;
				$slug = "{$originalSlug}-{$index}";
			} else {
				$topicFound = false;
			}
		} while ($topicFound);

		$topic = new Topic();
		$topic->title    = $request->input('title');
		$topic->slug     = $slug;
		$topic->category = $request->input('category');
		$topic->save();

		return response()->json([
			'topic' => new TopicResource($topic),
		]);
	}

	/**
	 * Batch update the topics associations with situations
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function batchUpdate(Request $request) {
		$validator = Validator::make($request->all(), [
			'topics'                => 'bail|required|array',
			'topics.*.id'           => 'bail|required|exists:topics',
			'topics.*.situationIds' => 'bail|array',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json([
				'error' => $error,
			], 400);
		}

		// Update the connection for all the topics
		$topics = $request->input('topics');
		if (sizeof($topics)) {
			foreach ($topics as $singleTopic) {
				if (sizeof($singleTopic['situationIds'])) {
					$topic = Topic::findOrFail($singleTopic['id']);
					$topic->situations()->sync($singleTopic['situationIds']);
				}
			}
		}

		return $this->index();
	}
	//
	// /**
	//  * Display the specified resource.
	//  *
	//  * @param  int  $id
	//  * @return \Illuminate\Http\Response
	//  */
	// public function show($id)
	// {
	// 	//
	// }
	//
	// /**
	//  * Update the specified resource in storage.
	//  *
	//  * @param  \Illuminate\Http\Request  $request
	//  * @param  int  $id
	//  * @return \Illuminate\Http\Response
	//  */
	// public function update(Request $request, $id)
	// {
	// 	//
	// }
	//
	// /**
	//  * Remove the specified resource from storage.
	//  *
	//  * @param  int  $id
	//  * @return \Illuminate\Http\Response
	//  */
	// public function destroy($id)
	// {
	// 	//
	// }
}
