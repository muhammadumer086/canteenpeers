<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Http\Resources\Blog as BlogResource;
use App\Blog;

class BlogCollection extends ResourceCollection {
	use \App\Traits\AuthUser;

	protected $withFeaturedBlogs = null;

	public function setFeaturedBlogIds($featuredBlogs) {
		$this->withFeaturedBlogs = $featuredBlogs;
	}

	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'featured' => $this->when(!is_null($this->withFeaturedBlogs), function () {
				return BlogResource::collection($this->withFeaturedBlogs);
			}),
			'data' => $this->collection
		];
	}

	protected function noParams(\Illuminate\Http\Request $request) {
		return !(sizeof($request->all()));
	}
}
