import React from 'react';
import GiphySelectionResultListItem from './GiphySelectionResultListItem';

import { IApiGIFObject } from '../../../interfaces';

interface IProps {
	data: IApiGIFObject[];
	onGifSelection(value: any): void;
}

const GiphySelectionResults: React.StatelessComponent<IProps> = props => {
	return (
		<div className="giphy_selection-results_list">
			{props.data.map((gif, index) => {
				return (
					<GiphySelectionResultListItem
						onGifSelection={props.onGifSelection}
						key={index}
						data={gif}
					/>
				);
			})}
		</div>
	);
};

export default GiphySelectionResults;
