import { actionTypes } from '../actions';
import { IApiSettings } from '../../interfaces';

export interface IStoreState {
	loading: boolean;
	settings: IApiSettings|null;
}

export const settings = (state: IStoreState = {
	loading: false,
	settings: null,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.SETTINGS_LOADING:
			return Object.assign({}, state, {
				loading: true,
			});
		case actionTypes.SETTINGS_LOADED:
			return Object.assign({}, state, {
				settings: action.settings,
				loading: false,
			});
		default:
			return state;
	}
}
