import React from 'react';
import Link from 'next/link';
import Button, { ButtonProps } from '@material-ui/core/Button';

interface IProps extends ButtonProps {
	href: string;
	hrefAs?: string;
	children?: any;
	prefetch?: boolean;
}

export const ButtonLink: React.FC<IProps> = (props: IProps) => {
	return (
		<Link href={props.href} as={props.hrefAs}>
			<Button
				variant={props.variant}
				color={props.color}
				className={props.className}
				href={props.hrefAs || props.href}
				disabled={props.disabled}
			>
				{props.children}
			</Button>
		</Link>
	);
};
