import React from 'react';
import { Formik } from 'formik';
import isEqual from 'lodash/isEqual';

import { Button } from '@material-ui/core';

import {
	TextFieldFormik,
	SwitchFieldFormik,
	ImageDropzoneFormik,
	RichTextEditor,
} from '../../inputs';
import AnimatePillListFormik, {
	ISlugLabel,
} from '../alerts/AnimatePillListFormik';
import { dissectUrl } from '../../../helpers/dissectUrl';

import {
	IStaticAgeRange,
	IStaticStates,
	IApiAnnouncement,
	IApiState,
	IApiAgeRange,
	IApiSituation,
} from '../../../interfaces';

interface IProps {
	isSubmitting: boolean;
	alertData: IApiAnnouncement;
	situations: IApiSituation[];
	ageRanges: IStaticAgeRange[];
	states: IStaticStates[];
	onSubmit(values: any): void;
}

interface IState {
	activeSituations: string[];
	activeAgeRange: string[];
	activeStates: string[];

	isSituationActive: boolean;
	isSpecificAgesActive: boolean;
	isSpecificLocationActive: boolean;
}

interface ICheckboxData {
	data: ISlugLabel[];
	activeData: string[];
	sectionLabel: string;
	stateCheckReference: string;
	stateDataReference: string;
	isAreaActive: boolean;
	toggleArea: (area: string) => void;
	togglePill: (slug: string, activeTarget: string) => void;
}

class EditAlertFormik extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			activeSituations: this.generateActiveSituations(
				this.props.alertData.situations,
			),
			activeAgeRange: this.generateActiveAgeRanges(
				this.props.alertData.age_ranges,
			),
			activeStates: this.generateActiveStates(
				this.props.alertData.states,
			),

			isSituationActive: this.props.alertData.situations.length > 0,
			isSpecificAgesActive: this.props.alertData.age_ranges.length > 0,
			isSpecificLocationActive: this.props.alertData.states.length > 0,
		};

		this.toggleArea = this.toggleArea.bind(this);
		this.handlePillToggle = this.handlePillToggle.bind(this);
	}

	public render() {
		const validationFormat = {};

		const strippedSituations = this.props.situations.map(situation => {
			return {
				value: situation.slug,
				label: situation.name,
			};
		});

		const strippedActiveSituations = this.state.activeSituations.map(
			situation => {
				return situation;
			},
		);

		const checkboxData: ICheckboxData[] = [
			{
				data: strippedSituations,
				activeData: strippedActiveSituations,
				sectionLabel: 'This alert only applies to specific situations',
				stateCheckReference: 'isSituationActive',
				stateDataReference: 'activeSituations',
				isAreaActive: this.state.isSituationActive,
				toggleArea: this.toggleArea,
				togglePill: this.handlePillToggle,
			},
			{
				data: this.props.ageRanges,
				activeData: this.state.activeAgeRange,
				sectionLabel: 'This alert only applies to specific ages',
				stateCheckReference: 'isSpecificAgesActive',
				stateDataReference: 'activeAgeRange',
				isAreaActive: this.state.isSpecificAgesActive,
				toggleArea: this.toggleArea,
				togglePill: this.handlePillToggle,
			},
			{
				data: this.props.states,
				activeData: this.state.activeStates,
				sectionLabel: 'This alert only applies to specific states',
				stateCheckReference: 'isSpecificLocationActive',
				stateDataReference: 'activeStates',
				isAreaActive: this.state.isSpecificLocationActive,
				toggleArea: this.toggleArea,
				togglePill: this.handlePillToggle,
			},
		];

		return (
			<Formik
				initialValues={{
					title: this.props.alertData.title,
					message: this.props.alertData.message,
					url: this.props.alertData.url
						? this.props.alertData.url.hrefAs
						: '',
					url_label: this.props.alertData.url_label,
					situations: this.props.alertData.situations,
					states: this.props.alertData.states,
					age_ranges: this.props.alertData.age_ranges,
					feature_image: this.props.alertData.feature_image,
					public: this.props.alertData.public,
				}}
				validationSchema={validationFormat}
				onSubmit={values => {
					const formValues = JSON.parse(JSON.stringify(values));
					formValues['url'] = dissectUrl(formValues.url);

					this.props.onSubmit(formValues);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
						isValid,
					};

					if (
						!isEqual(
							this.state.activeSituations.sort(),
							inputProps.values.situations.sort(),
						)
					) {
						inputProps.setFieldValue(
							'situations',
							this.state.activeSituations,
						);
					}

					if (
						!isEqual(
							this.state.activeAgeRange.sort(),
							inputProps.values.age_ranges.sort(),
						)
					) {
						inputProps.setFieldValue(
							'age_ranges',
							this.state.activeAgeRange,
						);
					}

					if (
						!isEqual(
							this.state.activeStates.sort(),
							inputProps.values.states.sort(),
						)
					) {
						inputProps.setFieldValue(
							'states',
							this.state.activeStates,
						);
					}

					const dropzoneClasses = ['discussion_modal-dropzone'];

					if ('feature_image' in values && values.feature_image) {
						dropzoneClasses.push(
							'discussion_modal-dropzone--image',
						);
					}

					return (
						<form
							className="create_new_alert_form form"
							onSubmit={handleSubmit}
						>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Alert Title"
										name="title"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<RichTextEditor
										toolbarId="event-edit-toolbar"
										name="message"
										value={inputProps.values.message}
										profile={[]}
										bounds=".modal--blog .modal-content_container"
										scrollingContainer=".modal--blog .modal-content_container"
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<ImageDropzoneFormik
										values={inputProps.values}
										dropzoneClasses={dropzoneClasses}
										setFieldValue={inputProps.setFieldValue}
										apiUrl="/api/announcement-images"
										label="Set the alert image (optional)"
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Alert URL"
										name="url"
										multiline={false}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Alert URL Label"
										name="url_label"
										multiline={false}
										disabled={this.props.isSubmitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<SwitchFieldFormik
										label="Public announcement"
										name="public"
										{...inputProps}
									/>
								</div>
							</div>

							{!values['public'] &&
								checkboxData.map((checkboxFields, index) => {
									return (
										<AnimatePillListFormik
											key={index}
											{...checkboxFields}
										/>
									);
								})}

							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid || this.props.isSubmitting
										}
									>
										Edit Alert
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		);
	}

	protected toggleArea(area: string) {
		const currentValues = !this.state[area];
		const updatedState: any = {};
		updatedState[area] = currentValues;

		this.setState(updatedState);
	}

	protected handlePillToggle(slug: string, activeTarget: string) {
		const activeStateTarget = [...this.state[activeTarget]];
		const indexOf = activeStateTarget.indexOf(slug);

		if (indexOf === -1) {
			const updatedState: any = {};
			updatedState[activeTarget] = [...activeStateTarget, slug];

			this.setState(updatedState);
		} else if (indexOf >= 0) {
			const filterActiveStateTarget = activeStateTarget.filter(
				filteredSituationSlug => {
					if (filteredSituationSlug !== slug) {
						return filteredSituationSlug;
					}
				},
			);

			const updatedState: any = {};
			updatedState[activeTarget] = filterActiveStateTarget;

			this.setState(updatedState);
		}
	}

	protected generateActiveSituations(situations: IApiSituation[]) {
		const filterData = situations.map(situation => {
			return situation.slug;
		});

		return filterData;
	}

	protected generateActiveAgeRanges(ages: IApiAgeRange[]) {
		const filterData = ages.map(age => {
			return age.slug;
		});

		return filterData;
	}

	protected generateActiveStates(states: IApiState[]) {
		const filterData = states.map(state => {
			return state.slug;
		});

		return filterData;
	}
}
export default EditAlertFormik;
