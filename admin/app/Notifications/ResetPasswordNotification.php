<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification {
	use Queueable;

	public $token;
	public $user;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($token, $user) {
		$this->token = $token;
		$this->user = $user;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$facadeUrl = config('canteen.facade_url') . '/auth/password-reset' . '?token=' . urlencode($this->token);

		return (new MailMessage)->view(
			'emails.reset-password', [
				'user' => $this->user,
				'facadeUrl' => $facadeUrl,
			]
		);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			//
		];
	}
}
