<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_state', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('announcement_id');
            $table->foreign('announcement_id')
                  ->references('id')->on('announcements')
                  ->onDelete('cascade');

            $table->unsignedInteger('state_id');
            $table->foreign('state_id')
                  ->references('id')->on('states');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_state');
    }
}
