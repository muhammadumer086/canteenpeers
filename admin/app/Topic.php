<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model {

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'created_at',
		'updated_at',
	];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'topics';

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = [
		'is_user_topic',
	];

	public function getIsUserTopicAttribute() {
		$request = request();
		if ($request->headers->has('Authorization')) {
			$authUser = Auth::guard('api')->authenticate($request);
			if ($authUser) {
				if ($authUser->hasRoleName('admin')) {
					return true;
				} else {
					// Get all the user's parent situations
					$userSituationIds = [];
					foreach ($authUser->situations as $situation) {
						if ($situation->parent_id) {
							$userSituationIds[] = $situation->parent_id;
						} else {
							$userSituationIds[] = $situation->id;
						}
					}
					if ($this->situations()->whereIn('situations.id', $userSituationIds)->exists()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * The blogs topics
	 */
	public function blogs() {
		return $this->hasMany('App\Blog');
	}

	/**
	 * The resource topics
	 */
	public function resources() {
		return $this->hasMany('App\Resource');
	}

	/**
	 * The situation topics
	 */
	public function discussions() {
		return $this->hasMany('App\Discussion');
	}

	/**
	 * The situations related to the topic
	 */
	public function situations() {
		return $this->belongsToMany('App\Situation');
	}

	/**
	 * The situation topics
	 */
	public function situationTopics() {
		return $this->hasMany('App\SituationTopic');
	}

}
