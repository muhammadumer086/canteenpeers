<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderToPersonalSettings extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('personal_settings', function (Blueprint $table) {
			$table->string('category_slug')->nullable();
			$table
				->integer('order')
				->unsigned()
				->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('personal_settings', function (Blueprint $table) {
			$table->dropColumn('category_slug');
			$table->dropColumn('order');
		});
	}
}
