<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Team extends JsonResource
{
    use \App\Traits\AuthUser;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('team', $this->id, $this->updated_at, false)) {
			return $apiStore->getApiResource('team', $this->id, $this->updated_at, false);
		}

		$params = [
			'id',
			'name',
			'slug',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
        }

        // Add the additional values
        $authUser = $this->getAuthUser($request);
        
        if (
            $authUser &&
            $authUser->hasRoleName('admin')
        ) {
            $adminParams = [
                'order',
            ];

            foreach ($adminParams as $key) {
                $result[$key] = $this->$key;
            }
        }
        
		$apiStore->setApiResource($result, 'team', $this->id, $this->updated_at, false);

		return $result;
    }
}
