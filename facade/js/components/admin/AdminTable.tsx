import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/ButtonBase';
import Checkbox from '@material-ui/core/Checkbox';
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Pill from '../general/Pill';

export interface IAdminTableColumn {
	/** The table header */
	header: string;
	/** The size of the column in REM */
	size: number;
	/** The rendered for the column */
	renderer: any;
	/** The sorting parameter */
	sortable?: string;
	/** If the sortable should be reversed */
	sortableReverse?: boolean;
}

interface IProps {
	/** The table columns */
	tableColumns: IAdminTableColumn[];
	/** The data passed to the table */
	data: any[];
	/** Action to click on a row */
	onRowClick?(data: any): void;
	/** loading status */
	loading?: boolean;
	/** Action to load more */
	onLoadMore?(): void;
	/** The selected rows */
	selectedData?: any[];
	/** Action when user select/unselect rows */
	onRowSelectionUpdate?(items: any[]): void;
	/** Generate report button label */
	generateReportLabel?: string;
	/** When a report is being generated */
	reportGenerating?: boolean;
	/** Action when generating report */
	onGenerateReport?(): void;
	/** The total number of records currently matched */
	totalMatchedRecords: number;
	/** The current sorting order */
	sort?: string;
	/** The sort update function */
	onUpdateSort?(sortable: string, direction: 'asc' | 'desc'): void;
	/** automatcially have delete at the end of the row */
	onDelete?(value: any): void;
	// change status of graduated user
	onChangeGraduate?(value: any): void;

	//resend verfication email
	onReminderEmail?(values: any): void;

	//
	isUpdating?: boolean;
	updatingUserId?: number;
}

interface IState {}

class AdminTable extends React.Component<IProps, IState> {
	protected checkboxColumnWidth = 5.5;

	constructor(props: IProps) {
		super(props);

		this.state = {};

		this.isChecked = this.isChecked.bind(this);
		this.handleFullSelect = this.handleFullSelect.bind(this);
		this.handleRowSelect = this.handleRowSelect.bind(this);
	}

	public render() {
		let generateReportLabel = 'Generate CSV Report';
		const tableContentClasses = ['admin_table-content'];

		if (!this.props.data.length && !this.props.loading) {
			tableContentClasses.push('admin_table-content--empty');
		}

		if (this.props.onGenerateReport) {
			if (this.props.generateReportLabel) {
				generateReportLabel = this.props.generateReportLabel;
			}
			if (this.props.selectedData && this.props.selectedData.length) {
				generateReportLabel += ` (${this.props.selectedData.length})`;
			} else {
				generateReportLabel += ` (${this.props.totalMatchedRecords})`;
			}
			tableContentClasses.push('admin_table-content--with_report');
		}
		const { isUpdating, updatingUserId } = this.props;
		return (
			<section className="admin_table">
				<header className="admin_table-header">
					{!!this.props.onRowSelectionUpdate && (
						<div
							className="admin_table-column admin_table-header-column"
							style={{
								width: `${this.checkboxColumnWidth}rem`,
								minWidth: `${this.checkboxColumnWidth}rem`,
								maxWidth: `${this.checkboxColumnWidth}rem`,
							}}
						>
							<Checkbox
								className="admin_table-clickable"
								onChange={this.handleFullSelect}
								checked={
									this.props.data.length ===
									this.props.selectedData.length
								}
								indeterminate={
									!!this.props.data.length &&
									!!this.props.selectedData.length &&
									this.props.data.length !==
										this.props.selectedData.length
								}
							/>
						</div>
					)}
					{!!this.props.onChangeGraduate && (
						<div
							className="admin_table-column admin_table-header-column"
							style={{
								width: `10rem`,
								minWidth: `10rem`,
								maxWidth: `10rem`,
							}}
						></div>
					)}
					{this.props.tableColumns.map((column, index) => (
						<div
							className="admin_table-column admin_table-header-column"
							style={{
								width: `${column.size}rem`,
								minWidth: `${column.size}rem`,
								maxWidth: `${column.size}rem`,
							}}
							key={index}
						>
							{column.header}
							{this.renderColumnSort(column)}
						</div>
					))}
					{!!this.props.onReminderEmail && (
						<div
							className="admin_table-column admin_table-header-column"
							style={{
								width: `14rem`,
								minWidth: `12rem`,
								maxWidth: `14rem`,
							}}
						></div>
					)}
					{!!this.props.onDelete && (
						<div
							className="admin_table-column admin_table-header-column"
							style={{
								width: `10rem`,
								minWidth: `10rem`,
								maxWidth: `10rem`,
							}}
						>
							Delete
						</div>
					)}
				</header>
				<div className={tableContentClasses.join(' ')}>
					{this.props.data.map((item, index) => (
						<div key={index} className="admin_table-row">
							{!!this.props.onRowSelectionUpdate && (
								<div
									className="admin_table-column admin_table-content-column"
									style={{
										width: `${this.checkboxColumnWidth}rem`,
										minWidth: `${this.checkboxColumnWidth}rem`,
										maxWidth: `${this.checkboxColumnWidth}rem`,
									}}
								>
									<Checkbox
										className="admin_table-clickable"
										checked={this.isChecked(item)}
										onChange={() => {
											this.handleRowSelect(item);
										}}
									/>
								</div>
							)}

							{!!this.props.onChangeGraduate && (
								<div
									className="admin_table-column admin_table-content-column"
									style={{
										width: `10rem`,
										minWidth: `10rem`,
										maxWidth: `10rem`,
										zIndex: 1,
									}}
								>
									{isUpdating &&
									item.id === updatingUserId ? (
										<CircularProgress size={15} />
									) : (
										<Pill
											active={true}
											clickable={true}
											label={
												item.is_graduate === 1
													? 'Enable'
													: 'Disable'
											}
											disabled={false}
											onClick={() => {
												this.props.onChangeGraduate(
													item,
												);
											}}
										/>
									)}
								</div>
							)}

							{this.props.tableColumns.map((column, index) => {
								const Renderer = column.renderer;
								return (
									<div
										className="admin_table-column admin_table-content-column"
										style={{
											width: `${column.size}rem`,
											minWidth: `${column.size}rem`,
											maxWidth: `${column.size}rem`,
										}}
										key={index}
									>
										<Renderer data={item} />
									</div>
								);
							})}
							{!!this.props.onReminderEmail && (
								<div
									className="admin_table-column admin_table-content-column"
									style={{
										width: `14rem`,
										minWidth: `10rem`,
										maxWidth: `14rem`,
										zIndex: 1,
									}}
								>
									{!item.verified &&
										!item.is_mobile_verified && (
											<div>
												{' '}
												{isUpdating &&
												item.id === updatingUserId ? (
													<CircularProgress
														size={15}
													/>
												) : (
													<Pill
														active={true}
														clickable={true}
														label={
															'Send Verification Reminder'
														}
														disabled={false}
														onClick={() => {
															this.props.onReminderEmail(
																item,
															);
														}}
													/>
												)}
											</div>
										)}
								</div>
							)}
							{!!this.props.onRowClick && (
								<ButtonBase
									onClick={() => {
										this.props.onRowClick(item);
									}}
									focusRipple={true}
									className="admin_table-row-action"
								/>
							)}

							{!!this.props.onDelete && (
								<div
									className="admin_table-column admin_table-content-column"
									style={{
										width: `10rem`,
										minWidth: `10rem`,
										maxWidth: `10rem`,
									}}
								>
									<Pill
										active={true}
										clickable={true}
										label="Delete"
										disabled={false}
										onClick={() => {
											this.props.onDelete(item);
										}}
									/>
								</div>
							)}
						</div>
					))}

					{!this.props.data.length && !this.props.loading && (
						<div className="admin_table-row">
							<div className="admin_table-column admin_table-content-column">
								<div
									className="font--h5"
									style={{
										padding: '1rem 0',
									}}
								>
									No content available.
								</div>
							</div>
						</div>
					)}
				</div>
				{(!!this.props.onLoadMore ||
					(!!this.props.onGenerateReport &&
						!!this.props.data.length)) && (
					<footer className="admin_table-footer">
						{!!this.props.onLoadMore && (
							<React.Fragment>
								{this.props.loading && (
									<LinearProgress
										color="secondary"
										className="admin_table-footer-loader"
									/>
								)}
								<Button
									className="admin_table-footer-load_more"
									onClick={this.props.onLoadMore}
									disabled={this.props.loading}
									variant="contained"
								>
									Load More Results
								</Button>
							</React.Fragment>
						)}
						{!!this.props.onGenerateReport &&
							!!this.props.data.length && (
								<div className="admin_table-generate_report">
									<Button
										className="admin_table-generate_report-button"
										onClick={this.props.onGenerateReport}
										disabled={
											this.props.loading ||
											this.props.reportGenerating
										}
										variant="contained"
										color="primary"
									>
										{this.props.reportGenerating
											? 'Generating...'
											: generateReportLabel}
									</Button>
								</div>
							)}
					</footer>
				)}
			</section>
		);
	}

	protected renderColumnSort(column: IAdminTableColumn) {
		if (column.sortable && this.props.onUpdateSort) {
			let icon = 'filter_list';
			let sortingOrder: 'asc' | 'desc' = 'asc';
			if (this.props.sort) {
				const sorting = this.props.sort.split('-');
				if (sorting.length === 2 && sorting[0] === column.sortable) {
					if (sorting[1] === 'desc') {
						icon = column.sortableReverse
							? 'arrow_downward'
							: 'arrow_upward';
					} else {
						icon = column.sortableReverse
							? 'arrow_upward'
							: 'arrow_downward';
						sortingOrder = 'desc';
					}
				}
			}
			return (
				<IconButton
					onClick={() => {
						this.props.onUpdateSort(column.sortable, sortingOrder);
					}}
				>
					<Icon>{icon}</Icon>
				</IconButton>
			);
		}
		return null;
	}

	protected getItemIndex(item: any): number {
		const itemJSON = JSON.stringify(item);
		return this.props.selectedData.findIndex(selected => {
			return JSON.stringify(selected) === itemJSON;
		});
	}

	protected isChecked(item: any): boolean {
		if (this.props.selectedData) {
			return this.getItemIndex(item) >= 0;
		}

		return false;
	}

	protected handleFullSelect() {
		let selectedData: any[] = [];
		if (!this.props.selectedData.length) {
			selectedData = JSON.parse(JSON.stringify(this.props.data));
		}
		if (this.props.onRowSelectionUpdate) {
			this.props.onRowSelectionUpdate(selectedData);
		}
	}

	protected handleRowSelect(item: any) {
		const index = this.getItemIndex(item);
		const selectedData: any[] = JSON.parse(
			JSON.stringify(this.props.selectedData),
		);
		if (index >= 0) {
			// Remove the item from the selected items
			selectedData.splice(index, 1);
		} else {
			// Add the item to the selected items
			selectedData.push(JSON.parse(JSON.stringify(item)));
		}
		if (this.props.onRowSelectionUpdate) {
			this.props.onRowSelectionUpdate(selectedData);
		}
	}
}

export default AdminTable;
