<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use App\Http\Resources\Staff as StaffResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StaffImage extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'staff_image',
				$this->id,
				$this->updated_at,
				true,
				true
			)
		) {
			return $apiStore->getApiResource(
				'staff_image',
				$this->id,
				$this->updated_at,
				true,
				true
			);
		}

		$params = [
			'id',
			'image_url',
			'width',
			'height',
			'created_at',
			'updated_at',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);


		$apiStore->setApiResource(
			$result,
			'staff_image',
			$this->id,
			$this->updated_at,
			true,
			true
		);

		return $result;
	}
}
