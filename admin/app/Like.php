<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model {
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'likes';

	/**
	 * The user that created this like
	 */
	public function user() {
		return $this->belongsTo('App\User')->withTrashed();
	}

	/**
	 * The discussion associated with this like
	 */
	public function discussion() {
		return $this->belongsTo('App\Discussion')->withTrashed();
	}

	public function userActivities() {
		return $this->morphMany('App\UserActivity', 'activity');
	}
}
