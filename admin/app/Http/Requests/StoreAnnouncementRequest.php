<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAnnouncementRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'title' => 'bail|required|string',
			'message' => 'bail|required|string',
			'url' => 'bail|nullable',
			'url.href' => 'bail|string',
			'url.hrefAs' => 'bail|string',
			'url.type' => 'bail|string',
			'url_label' => 'bail|nullable|string',
			'image_id' => 'bail|string',
			'situations' => 'bail|array',
			'states' => 'bail|array',
			'age_ranges' => 'bail|array',
			'public' => 'bail|boolean',
		];
	}
}
