<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Resource</title>
</head>
<body>
	Single Resource
	<?php //TODO: Var_dump the data ?>
	<?php var_dump($data); ?>
	<div id="app" class="flex-center position-ref full-height"></div>
</body>
</html>
