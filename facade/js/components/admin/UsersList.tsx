import React from 'react';
import { connect } from 'react-redux';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import {
	IStoreState,
	userPanelDisplay
} from '../../redux/store';

import { IApiUser } from '../../interfaces';

import UserAvatar from '../general/UserAvatar';

interface IProps {
	users: IApiUser[];
	userPanelDisplay: boolean;
	userPanelData: IApiUser|null;
	dispatch(action: any): void;
}

interface IState {
	users: IApiUser[];
	selectedUserIndex: number|null;
}

class UsersList extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			users: props.users,
			selectedUserIndex: null,
		};

		this.displayUser = this.displayUser.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.users !== prevProps.users) {
			this.setState({
				users: JSON.parse(JSON.stringify(this.props.users)),
			});
		}
		if (
			this.props.userPanelDisplay !== prevProps.userPanelDisplay &&
			!this.props.userPanelDisplay &&
			this.state.selectedUserIndex >= 0
		) {
			const users = JSON.parse(JSON.stringify(this.state.users));
			users[this.state.selectedUserIndex] = JSON.parse(JSON.stringify(this.props.userPanelData));
			this.setState({
				users,
			});
		}
	}

	public render() {
		return (
			<section className="admin_users_list hm-t32">
				<div className="admin_users_list-content">
					<List className="admin_users_list-list">
						{
							this.state.users.map((user, index) => {
								const userName = [
									user.first_name,
									user.last_name,
									`(${user.email})`,
								];
								if (user.is_banned) {
									userName.unshift('[BANNED]');
								}

								return (
									<ListItem key={index} button onClick={() => {
										this.displayUser(user, index);
									}}>
										<div className="admin_users_list-avatar">
											<UserAvatar user={user} />
										</div>
										<ListItemText primary={userName.join(' ')} secondary={user.role_names.join(', ')} />
									</ListItem>
								);
							})
						}
					</List>
				</div>
			</section>
		);
	}

	protected displayUser(user: IApiUser, selectedUserIndex: number) {
		this.setState({
			selectedUserIndex
		}, () => {
			this.props.dispatch(userPanelDisplay(user));
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const {
		userPanelDisplay,
		userPanelData,
	} = state.userPanel;
	return {
		userPanelDisplay,
		userPanelData,
	};
}

export default connect(mapStateToProps)(UsersList);
