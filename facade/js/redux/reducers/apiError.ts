import { actionTypes } from '../actions';

export interface IStoreState {
	apiError: string[],
	apiErrorShowing: boolean;
}

export const apiError = (
	state: IStoreState = {
		apiError: [],
		apiErrorShowing: false,
	},
	action
): IStoreState => {
	switch (action.type) {
		case actionTypes.API_ERROR:
			return Object.assign({}, state, {
				apiError: action.apiError,
				apiErrorShowing: true
			});
		case actionTypes.API_ERROR_DISMISS:
			return Object.assign({}, state, {
				apiErrorShowing: false
			});
		default:
			return state;
	}
}
