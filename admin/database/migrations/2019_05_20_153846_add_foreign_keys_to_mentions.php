<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToMentions extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('mentions', function (Blueprint $table) {
			$table
				->integer('mentioned_user_id')
				->unsigned()
				->nullable()
				->change();
			$table
				->integer('sender_user_id')
				->unsigned()
				->nullable()
				->change();

			$table->dropColumn('discussion_id');

			$table
				->foreign('mentioned_user_id')
				->references('id')
				->on('users')
				->onDelete('cascade');
			$table
				->foreign('sender_user_id')
				->references('id')
				->on('users')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('mentions', function (Blueprint $table) {
			$table->dropForeign('mentions_mentioned_user_id_foreign');
			$table->dropForeign('mentions_sender_user_id_foreign');

			$table
				->bigInteger('discussion_id')
				->unsigned()
				->nullable()
				->change();
		});
	}
}
