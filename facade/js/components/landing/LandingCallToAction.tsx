import React from 'react';
import { ButtonLink } from '../general/ButtonLink';

interface IProps {
	ctaConfig: {
		ctaText: string;
		buttonLabel: string;
	};
}

const LandingCallToAction: React.FC<IProps> = (props: IProps) => {
	return (
		<div className="landing_call_to_action">
			<div className="landing_call_to_action-container">
				<h4 className="font--h4">{props.ctaConfig.ctaText}</h4>
				<ButtonLink
					variant="contained"
					color="primary"
					href="/auth/register?step=step-1"
					hrefAs="/auth/register/step-1"
					className="landing_call_to_action-join-button"
				>
					{props.ctaConfig.buttonLabel}
				</ButtonLink>
			</div>
		</div>
	);
};

export default LandingCallToAction;
