import React from 'react';
import ReactSVG from 'react-svg';
import Head from 'next/head';
import { connect } from 'react-redux';
import buildUrl from 'build-url';

import { withRouter } from 'next/router';

import Button from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import {
	IStoreState,
	apiError,
	discussionModalDisplay,
} from '../js/redux/store';

import {
	IApiDiscussion,
	IApiUser,
	IApiPagination,
	IApiTopic,
	IApiSituation,
	FilterOrder,
} from '../js/interfaces';

import { ApiService, AuthService } from '../js/services';

import {
	IFilters,
	IFiltersSelected,
	IFiltersGroup,
	FilterResultsHelpers,
} from '../js/components/general/FilterResults';
import FilterResults from '../js/components/general/FilterResults';
import DiscussionTile from '../js/components/discussions/DiscussionTile';
import LayoutDashboard from '../js/components/general/LayoutDashboard';
import ListLoading from '../js/components/general/ListLoading';
import HeroHeading from '../js/components/general/HeroHeading';
import Footer from '../js/components/general/Footer';
import { ButtonLink } from '../js/components/general/ButtonLink';
import isAdmin from '../js/helpers/isAdmin';

import { GTMDataLayer } from '../js/helpers/dataLayer';

enum EDiscussionTypes {
	all = 'all',
	active = 'active',
	followed = 'followed',
}

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	discussionsData: {
		data: {
			discussions: IApiDiscussion[];
		};
		meta: IApiPagination;
	};
	topicsLoading: boolean;
	topics: IApiTopic[];
	situationsLoading: boolean;
	situations: IApiSituation[];
	queryType: EDiscussionTypes | null;
	newModal: boolean;
	apiService: ApiService;
	authService: AuthService;
	updateDataCount: number;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	discussionsType: EDiscussionTypes;
	discussions: IApiDiscussion[];
	discussionsTotal: number;
	isLoadingDiscussions: boolean;
	currentPage: number;
	maxPages: number;
	topicFromUrl: IApiTopic | null;
	filters: IFilters;
	order: FilterOrder;
	selectedFilters: IFiltersSelected;
}

class Discussions extends React.Component<IProps, IState> {
	protected filterHelpers: FilterResultsHelpers = new FilterResultsHelpers();
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ query, apiService }) {
		let newModal = false;
		let queryType: EDiscussionTypes | null = null;
		const params: any = {};
		if (
			'type' in query &&
			query.type in EDiscussionTypes &&
			query.type !== EDiscussionTypes.all
		) {
			queryType = query.type;
			params.type = query.type;
		}

		if ('new' in query && query.new) {
			newModal = true;
		}

		if ('topic' in query) {
			params[`topics[0]`] = query.topic;
		}

		const url = buildUrl(null, {
			path: '/api/discussions',
			queryParams: params,
		});

		const discussionsData = await (apiService as ApiService).queryGET(url);

		return {
			queryType,
			discussionsData,
			newModal,
		};
	}

	constructor(props: IProps) {
		super(props);

		const selectedFilters: IFiltersSelected = {
			filters: [],
		};

		this.state = {
			isUpdating: false,
			discussionsType: props.queryType
				? props.queryType
				: EDiscussionTypes.all,
			discussions: props.discussionsData.data.discussions,
			discussionsTotal: props.discussionsData.meta.total,
			isLoadingDiscussions: false,
			currentPage: props.discussionsData.meta.current_page,
			maxPages: props.discussionsData.meta.last_page,
			filters: this.generateFilters(),
			topicFromUrl: null,
			order: 'DESC',
			selectedFilters,
		};

		this.handleDiscussionsTypeChange = this.handleDiscussionsTypeChange.bind(
			this,
		);
		this.handleDiscussionModalDisplay = this.handleDiscussionModalDisplay.bind(
			this,
		);
		this.loadDiscussions = this.loadDiscussions.bind(this);
		this.handleOrderChange = this.handleOrderChange.bind(this);
		this.handleFiltersToggle = this.handleFiltersToggle.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
		this.handleFiltersReset = this.handleFiltersReset.bind(this);
		this.handleFiltersSubmit = this.handleFiltersSubmit.bind(this);
		this.trackFilterUpdate = this.trackFilterUpdate.bind(this);
	}

	public componentDidMount() {
		if (this.props.newModal) {
			this.props.dispatch(discussionModalDisplay());
		}

		this.updateFiltersBasedOnUrl();
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.situations !== prevProps.situations ||
			this.props.topics !== prevProps.topics ||
			this.props.updateDataCount !== prevProps.updateDataCount
		) {
			this.setState(
				{
					filters: this.generateFilters(),
					selectedFilters: {
						filters: [],
					},
				},
				() => {
					this.updateData();
				},
			);
		}

		if (
			JSON.stringify(this.props.router.query) !==
			JSON.stringify(prevProps.router.query)
		) {
			this.updateFiltersBasedOnUrl();
		}
		if (
			prevProps.userData &&
			prevProps.userData.following_count > 0 &&
			this.props.userData.following_count <= 0
		) {
			this.setState({ discussionsType: EDiscussionTypes.all });
		}
	}

	public render() {
		// const pageTitle =
		// 	'Cancer Forums & Discussions For young adult & Teens | Canteen';

		const url = buildUrl(process.env.APP_URL, {
			path: `/discussions/`,
		});

		const featuredImageUrl = `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;

		// const seoDescription =
		// 	'Talk with our community of Australian teenagers whose lives have been affected by cancer. Register to connect with other parents &amp; get support.';

		return (
			<div>
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:image"
						property="og:image"
						content={featuredImageUrl}
					/>
					<meta name="og:url" property="og:url" content={url} />
					<meta name="og:type" property="og:type" content="article" />

					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadDiscussions}>
					<header className="hero theme--accent">
						<HeroHeading
							iconSlug="discussions"
							title="Discussions"
						/>
						<span className="hero-actions">
							{this.props.userAuthenticated && (
								<Button
									variant="contained"
									color="primary"
									onClick={this.handleDiscussionModalDisplay}
								>
									Start New Discussion{' '}
									<ReactSVG
										path={`${process.env.STATIC_PATH}/icons/plus.svg`}
									/>
								</Button>
							)}
							{!this.props.userAuthenticated && (
								<ButtonLink
									variant="contained"
									color="primary"
									href="/auth/login"
								>
									Log In to Start New Discussion
								</ButtonLink>
							)}
						</span>
					</header>

					{/* The fixed bar */}
					{this.renderFixedBar()}

					{this.state.discussionsType === EDiscussionTypes.all && (
						<FilterResults
							filters={this.state.filters}
							selectedFilters={this.state.selectedFilters}
							suggestTopic={true}
							onToggle={this.handleFiltersToggle}
							onUpdateFilters={this.handleFiltersUpdate}
							onReset={this.handleFiltersReset}
							onSubmit={this.handleFiltersSubmit}
							total={this.state.discussionsTotal}
							totalString={{
								noResults: 'No discussions',
								singular: '1 discussion',
								plural: 'discussions',
							}}
							gtmDataLayer={this.trackFilterUpdate}
							order={this.state.order}
							onOrderChange={this.handleOrderChange}
						/>
					)}
					<div className="discussions-tiles">
						{this.state.discussions.map(discussion => {
							return (
								<div
									className="discussions-tiles-item"
									key={discussion.id}
								>
									<DiscussionTile
										discussion={discussion}
										disabled={this.state.isUpdating}
										useLastActivity={true}
										withRepliedTo={
											this.state.discussionsType ===
											EDiscussionTypes.active
												? this.props.userData.full_name
												: null
										}
										noActions={
											!this.props.userAuthenticated
										}
									/>
								</div>
							);
						})}
					</div>
					<ListLoading active={this.state.isLoadingDiscussions} />

					<Footer />
				</LayoutDashboard>
			</div>
		);
	}

	protected renderFixedBar() {
		if (
			!this.props.userAuthenticated ||
			!(
				(this.props.userData &&
					this.props.userData.active_discussions_count &&
						this.props.userData.active_discussions_count > 0) ||
				(this.props.userData.following_count &&
					this.props.userData.following_count > 0)
			)
		) {
			return null;
		}

		const classNamesAll = ['fixed_bar-button'];
		const classNamesActive = ['fixed_bar-button'];
		const classNamesFollowed = ['fixed_bar-button'];

		switch (this.state.discussionsType) {
			case EDiscussionTypes.all:
				classNamesAll.push('fixed_bar-button--active');
				classNamesActive.push('fixed_bar-button--inactive');
				classNamesFollowed.push('fixed_bar-button--inactive');
				break;
			case EDiscussionTypes.active:
				classNamesAll.push('fixed_bar-button--inactive');
				classNamesActive.push('fixed_bar-button--active');
				classNamesFollowed.push('fixed_bar-button--inactive');
				break;
			case EDiscussionTypes.followed:
				classNamesAll.push('fixed_bar-button--inactive');
				classNamesActive.push('fixed_bar-button--inactive');
				classNamesFollowed.push('fixed_bar-button--active');
				break;
		}

		return (
			<div className="fixed_bar fixed_bar--with_tabs theme--accent">
				<Tabs
					className="fixed_bar-tabs"
					value={this.state.discussionsType}
					onChange={this.handleDiscussionsTypeChange}
					indicatorColor="secondary"
					textColor="inherit"
					variant="scrollable"
				>
					<Tab label="All Discussions" value={EDiscussionTypes.all} />
					{!!this.props.userData &&
						!!this.props.userData.active_discussions_count &&
						this.props.userData.active_discussions_count > 0 && (
							<Tab
								label={`My Discussions (${this.props.userData.active_discussions_count})`}
								value={EDiscussionTypes.active}
							/>
						)}
					{!!this.props.userData &&
						!!this.props.userData.following_count &&
						this.props.userData.following_count > 0 && (
							<Tab
								label={`Followed Discussions (${this.props.userData.following_count})`}
								value={EDiscussionTypes.followed}
							/>
						)}
				</Tabs>
			</div>
		);
	}

	protected handleDiscussionsTypeChange(_event, type: EDiscussionTypes) {
		this.setState(
			{
				discussionsType: type,
			},
			() => {
				this.updateData();
			},
		);
	}

	protected handleDiscussionModalDisplay() {
		this.props.dispatch(discussionModalDisplay());
	}

	protected updateData() {
		// Scroll to the top (browser only)
		if (typeof window !== 'undefined') {
			const dashboard = window.document.querySelector(
				'.layout_dashboard-content_container',
			);
			dashboard.scroll({
				top: 0,
				left: 0,
			});
		}

		this.loadDiscussions(false);
	}

	protected handleOrderChange(order: FilterOrder) {
		this.setState(
			{
				order,
			},
			() => {
				this.updateData();
			},
		);
	}

	protected buildQueryParams(withPage: boolean = false) {
		const params: any = {};

		if (this.state.discussionsType !== EDiscussionTypes.all) {
			params.type = this.state.discussionsType;
		} else {
			this.state.selectedFilters.filters.forEach(singleFilter => {
				if (singleFilter.filterValues instanceof Array) {
					singleFilter.filterValues.forEach((filterValue, index) => {
						params[`${singleFilter.paramName}[${index}]`] =
							filterValue.value;
					});
				} else {
					params[singleFilter.paramName] =
						singleFilter.filterValues.value;
				}
			});
		}

		if (withPage) {
			params.page = this.state.currentPage + 1;
		}

		if (!this.state.filters.filterToggle) {
			params.anySituation = true;
		}

		if (this.state.order) {
			params.order = this.state.order;
		}

		return buildUrl(null, {
			path: '/api/discussions',
			queryParams: params,
		});
	}

	protected loadDiscussions(append: boolean = true) {
		if (
			!this.state.isLoadingDiscussions &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			this.setState(
				{
					isUpdating: !append,
					isLoadingDiscussions: true,
				},
				() => {
					const url = this.buildQueryParams(append);

					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'discussions' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let discussions: IApiDiscussion[] =
									response.data.discussions;

								if (append) {
									discussions = JSON.parse(
										JSON.stringify(this.state.discussions),
									);
									(response.data
										.discussions as IApiDiscussion[]).forEach(
										singleDiscussion => {
											discussions.push(singleDiscussion);
										},
									);
								}

								this.setState({
									isUpdating: false,
									isLoadingDiscussions: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									discussionsTotal: response.meta.total,
									discussions,
								});
							} else {
								this.props.dispatch(
									apiError(['Invalid data returned by API']),
								);
							}
						})
						.catch(error => {
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isUpdating: false,
								isLoadingDiscussions: false,
							});
						});
				},
			);
		}
	}

	protected handleFiltersUpdate(filters: IFilters) {
		this.setState({
			filters,
		});
	}

	protected handleFiltersReset() {
		this.setState({
			selectedFilters: {
				filters: [],
			},
			filters: this.generateFilters(),
		});
	}

	protected handleFiltersSubmit(selectedFilters: IFiltersSelected) {
		this.setState(
			{
				selectedFilters,
				filters: this.filterHelpers.updateFiltersBasedOnSelectedFilters(
					selectedFilters,
					this.state.filters,
				),
			},
			() => {
				this.updateData();
			},
		);
	}

	protected handleFiltersToggle() {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.state.filters),
		);

		filters.filterToggle = !filters.filterToggle;

		if (filters.filterToggle) {
			if (this.props.userAuthenticated) {
				const cancerTypesIndex = filters.filterGroups.findIndex(
					value => {
						return value.paramName === 'situations';
					},
				);
				if (cancerTypesIndex >= 0) {
					filters.filterGroups.splice(cancerTypesIndex, 1);
				}

				// Remove the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					filters.filterGroups[
						topicsIndex
					].filters = filters.filterGroups[
						topicsIndex
					].filters.filter(filterValue => {
						const result = this.props.topics.find(topic => {
							return (
								topic.category === 'discussions' &&
								topic.slug === filterValue.value &&
								(topic.is_user_topic ||
									isAdmin(this.props.userData))
							);
						});
						return !!result;
					});
				}
			}
		} else {
			if (this.props.userAuthenticated) {
				// Add the situations at the beginning
				const situationsGroup: IFiltersGroup = {
					label: null,
					noBorder: true,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: !!this.props.userData.situations.find(
									userSituation => {
										return (
											userSituation.name ===
											situation.name
										);
									},
								),
							};
						}),
				};
				filters.filterGroups.unshift(situationsGroup);

				// add the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					this.props.topics.forEach(topic => {
						if (
							!topic.is_user_topic &&
							!isAdmin(this.props.userData) &&
							topic.category === 'discussions'
						) {
							filters.filterGroups[topicsIndex].filters.push({
								label: topic.title,
								value: topic.slug,
								active: false,
							});
						}
					});
				}
			}
		}

		this.setState({
			filters,
		});
	}

	protected updateFiltersBasedOnUrl() {
		if ('topic' in this.props.router.query) {
			const selectedFilters: IFiltersSelected = {
				filters: [],
			};

			// find the topic
			const topicFromUrl: IApiTopic | null = this.props.topics.find(
				singleTopic => {
					return (
						singleTopic.slug ===
							(this.props.router.query.topic as string) &&
						singleTopic.category === 'discussions'
					);
				},
			);

			if (topicFromUrl) {
				selectedFilters.filters.push({
					paramName: 'topics',
					filterValues: [
						{
							label: topicFromUrl.title,
							value: topicFromUrl.slug,
							active: true,
						},
					],
				});
			}

			this.setState(
				{
					selectedFilters,
					filters: this.generateFilters(topicFromUrl),
				},
				() => {
					this.updateData();
				},
			);
		}
	}

	protected generateFilters(topicFromUrl?: IApiTopic | null): IFilters {
		const filters: IFilters = {
			filtersTitle: 'Filter',
			withToggle: this.props.userAuthenticated,
			filterToggleLabel:
				'Only show discussions directly related to my cancer experience',
			filterToggle: true,
			filterGroups: [],
		};

		if (!this.props.situationsLoading && !this.props.topicsLoading) {
			// Add the situations for the un-authenticated users
			if (!this.props.userAuthenticated) {
				filters.filterGroups.push({
					label: null,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: false,
							};
						}),
				});
			}

			// Add the topics
			filters.filterGroups.push({
				label: null,
				paramName: 'topics',
				filters: this.props.topics
					.filter(topic => {
						return (
							topic.category === 'discussions' &&
							(!this.props.userAuthenticated ||
								isAdmin(this.props.userData) ||
								topic.is_user_topic)
						);
					})
					.map(topic => {
						let active = false;
						if (topicFromUrl) {
							active = topic.slug === topicFromUrl.slug;
						}
						return {
							label: topic.title,
							value: topic.slug,
							active,
						};
					}),
			});
		}

		return filters;
	}

	protected trackFilterUpdate(value: string) {
		this.GTM.pushEventToDataLayer({
			event: 'filterDiscussions',
			filter: value,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { situations } = state.situations;
	const { topics } = state.topics;
	const { updateDataCount } = state.updateData;

	return {
		userAuthenticated,
		userData,
		situations,
		situationsLoading: state.situations.loading,
		topics,
		topicsLoading: state.topics.loading,
		updateDataCount,
	};
}

export default connect(mapStateToProps)(withRouter(Discussions as any));
