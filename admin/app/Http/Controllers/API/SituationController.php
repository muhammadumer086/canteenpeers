<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Situation;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Resources\Situation as SituationResource;

class SituationController extends Controller {
	const PAGINATION_SIZE = 20;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$situations = Situation::all();

		return response()->json([
			'situations' => SituationResource::collection($situations),
		]);
	}
}
