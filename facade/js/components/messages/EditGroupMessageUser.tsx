import React from 'react';
import { connect } from 'react-redux';

import { ButtonBase } from '@material-ui/core';
import UserAvatar from '../general/UserAvatar';

import { IApiUser } from 'js/interfaces';
import { userPanelDisplay } from '../../redux/store';

import Pill from '../general/Pill';

interface IProps {
	user: IApiUser;
	disabled?: boolean;
	removeUser(user: IApiUser): void;
	dispatch(values: any): void;
}

const EditGroupMessageUser: React.StatelessComponent<IProps> = props => {
	const openUserPanel = () => {
		props.dispatch(userPanelDisplay(props.user));
	};

	return (
		<div className="edit_group_message_user theme--main">
			<div className="edit_group_message_user-avatar">
				<ButtonBase
					className="edit_group_message_user-open_user_panel"
					onClick={openUserPanel}
					disableRipple={true}
				/>

				<div className="edit_group_message_user-avatar_container">
					<UserAvatar user={props.user} />
				</div>
			</div>

			<div className="edit_group_message_user-username">
				<span className="theme-title">{props.user.full_name}</span>
			</div>

			<div className="edit_group_message_user-remove">
				<Pill
					label="Remove"
					onClick={() => {
						props.removeUser(props.user);
					}}
					active={true}
					disabled={false}
					clickable={true}
					warning={true}
				/>
			</div>

			<div className="edit_group_message_user-divider" />
		</div>
	);
};

function mapStateToProps() {
	return {};
}

export default connect(mapStateToProps)(EditGroupMessageUser);
