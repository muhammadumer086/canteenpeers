<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiscussionnThread extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->unsignedInteger('thread_id')->nullable();

			$table->foreign('thread_id')
			->references('id')->on('discussions')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->dropForeign('discussions_thread_id_foreign');
			$table->dropColumn('thread_id');
		});
	}
}
