<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Discussion;
use App\Resource;
use App\Blog;

class AlgoliaIndex extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:algoliaIndex {--private : Index the private index} {--flush : Flush the index}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the algolia index.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$command = 'scout:import';
		if ($this->option('flush')) {
			$command = 'scout:flush';
		}

		$models = [
			'App\Discussion',
			'App\Resource',
			'App\Blog',
			'App\Event',
			'App\Hashtag',
		];

		if (!$this->option('private')) {
			define('ALGOLIA_PUBLIC', true);
		} else {
			// Add user only in private
			$models[] = 'App\User';
		}

		foreach ($models as $model) {
			$this->call($command, [
				'model' => $model,
			]);
		}
	}
}
