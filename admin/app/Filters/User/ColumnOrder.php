<?php

namespace App\Filters\User;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

class ColumnOrder extends FilterAbstract {
	/**
	 * Mappings for database values.
	 *
	 * @return array
	 */
	public function mappings() {
		return [];
	}

	/**
	 * Filter with builder queries.
	 *
	 * @param  string $access
	 * @return Illuminate\Database\Eloquent\Builder
	 */
	public function filter(Builder $builder, $value) {
		$valueSplit = explode('-', $value);
		$order = 'ASC';
		if (
			sizeof($valueSplit) === 2 &&
			(strtoupper($valueSplit[1]) === 'DESC' ||
				strtoupper($valueSplit[1]) === 'ASC')
		) {
			$order = strtoupper($valueSplit[1]);
		}
		if (sizeof($valueSplit) && Schema::hasColumn('users', $valueSplit[0])) {
			return $builder->orderBy($valueSplit[0], $order);
		}

		return $builder;
	}
}
