<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Resources\Event as EventResource;
use App\Http\Resources\EventCollection;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;

use App\Notifications\UserRegisteredEvent;

use App\Situation;
use App\State;
use App\User;
use App\Event;
use App\EventImage;
use App\EventAdmin;
use App\Discussion;
use App\Topic;

use App\Traits\CreateSlug;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;

use phpDocumentor\Reflection\Types\Null_;
use Validator;
use UserActivityHelper;
use UserPermissionHelper;

class EventController extends Controller {
	use CreateSlug;

	const PAGINATION_SIZE = 30;
	const RELATED_SIZE = 2;

	/**
	 * Display a listing of paginated events.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_read","events_management_all"]);
		$now = Carbon::today();

		// $events = Event::orderBy('start_date', 'ASC')
		// 	->where('start_date', '>', $now)
		// 	->filter($request);

		$sortOrder = $request->input('order')?$request->input('order'):'ASC';

		// Base of the Query
		$eventsQuery = Event::where('start_date', '>=', $now)->orderBy(
			'start_date',
			'ASC'
		);
		if ($request->has('my_events')) {
			$eventsQuery = Event::join('event_user', 'events.id', 'event_user.event_id')
				->select('events.*')
				->where('start_date', '>=', $now)
				->where('event_user.user_id', $authUser->id)
				->orderBy(
					'start_date',
					$sortOrder
				);
		}

		if ($request->has('past_events')) {
			$eventsQuery = Event::join('event_user', 'events.id', 'event_user.event_id')
				->select('events.*')
				->where('start_date', '<', $now)
				->where('event_user.user_id', $authUser->id)
				->orderBy(
					'start_date',
					$sortOrder
				);
		}

		//dd($eventsQuery);

		if (
		($authUser &&
			($authUser->hasRoleName('admin')) || $userHasPermission)
		) {
			if($request->input('include_past'))
			{
				$eventsQuery = Event::orderBy('start_date', $sortOrder);
			}
			elseif($request->has('past_or_upcoming')) {
				if ($request->input("past_or_upcoming") == "admin_upcoming_events") {
					$eventsQuery = Event::select('events.*')
						->where('start_date', '>=', $now)
						->orderBy(
							'start_date',
							$sortOrder
						);
				}
				else{
					$eventsQuery = Event::select('events.*')
						->where('start_date', '<', $now)
						->orderBy(
							'start_date',
							$sortOrder
						);
				}
			}


		}

		if($request->has('event_type') && !empty($request->input('event_type')))
		{
			$eventsQuery->whereIn('events.event_type', $request->input('event_type'));
		}

		$filterBySituation = new \App\Helpers\FilterByUserSituation();
		//return new EventCollection($eventsQuery->paginate(self::PAGINATION_SIZE));
		if ($request->has('situations') && !$userHasPermission) {
			// $situationIds = Situation::whereIn(
			// 	'name',
			// 	$request->input('situations')
			// )
			// 	->whereNull('parent_id')
			// 	->pluck('situations.id');
			$situationIds = $filterBySituation->addSituationsMap(
				$request->input('situations')
			);

			$eventsQuery->whereIn(
				'events.id',
				DB::table('event_situation')
					->whereIn('situation_id', $situationIds)
					->pluck('event_id')
			);
		} elseif (
			$authUser &&
			(!$authUser->hasRoleName('admin') && !$userHasPermission) &&
			!$request->has('anySituation')
		) {
			// No situation filters selected, use situation of current user
			$eventsQuery = $filterBySituation->filterByUserSituation(
				$authUser,
				$eventsQuery,
				'events',
				'event_situation',
				'event_id'
			);
		}



		if ($request->has('state')) {
			$states = $request->input('state');
			if (in_array('all-of-australia', $states)) {
				$states[] = 'nsw';
				$states[] = 'vic';
				$states[] = 'qld';
				$states[] = 'qld-north';
				$states[] = 'tas';
				$states[] = 'act';
				$states[] = 'sa';
				$states[] = 'nt';
				$states[] = 'wa';
			}
			$eventsQuery->whereIn(
				'state_id',
				DB::table('states')
					->whereIn(
						'id',
						State::whereIn('slug', $states)->pluck('id')
					)
					->pluck('id')
			);
		}

		if ($request->has('duration')) {
			$duration = $request->input('duration');
			if (
				in_array('overnight', $duration) &&
				in_array('weekend', $duration)
			) {
				$eventsQuery
					->where('overnight', true)
					->orWhere('weekend', true);
			} elseif (in_array('overnight', $duration)) {
				$eventsQuery->where('overnight', true);
			} elseif (in_array('weekend', $duration)) {
				$eventsQuery->where('weekend', true);
			}
		}

		if ($request->has('time')) {
			$time = $request->input('time');
			if (in_array('daytime', $time) && in_array('evening', $time)) {
				$eventsQuery->where('daytime', true)->orWhere('evening', true);
			} elseif (in_array('daytime', $time)) {
				$eventsQuery->where('daytime', true);
			} elseif (in_array('evening', $time)) {
				$eventsQuery->where('evening', true);
			}
		}

		$events = $eventsQuery->paginate(self::PAGINATION_SIZE);

		return new EventCollection($events);
	}

	/**
	 * Store a newly created event in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function storeEventMedia(Request $request)
	{
		$authUser = $this->getAuthUser($request);
		//$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_management_add","events_management_edit","events_management_all"]);

		if (!$authUser) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
		}

		if (!$request->has('image_id')) {
			return response()->json(
				[
					'error' => 'Image id require',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			//'title' => 'bail|required|string|max:191',
			'image_id' => 'bail|string|exists:event_images,id',
		]);


		if ($request->has('image_id')) {
			$eventImage = EventImage::where(
				'id',
				$request->input('image_id')
			)->first();
			$eventImage->user_id = $authUser->id;
			$eventImage->is_featured = true;
			$eventImage->title = $request->input('title');
			//$eventImage->path = $request->input('path');
			//$eventImage->width = $request->input('width');
			//$eventImage->height = $request->input('height');
			$eventImage->event()->associate($request->input('event_id'));
			$eventImage->save();
		}

		return response()->json(
			[
				'succcess' => 'Image has been updated',
			],
			200
		);
	}

	/**
	 * Store a newly created event in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_management_add","events_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$validator = Validator::make($request->all(), [
			'title' => 'bail|required|string|max:191',
			'description' => 'bail|required|string|max:50000',
			'situations' => 'bail|array|exists:situations,slug',
			'start_date' => 'bail|required|date',
			'end_date' => 'bail|required|date|after:start_date',
			'latitude' => 'bail|nullable|string|max:191',
			'longitude' => 'bail|nullable|string|max:191',
			'location' => 'bail|nullable|string|max:191',
			'location_data' => 'bail|nullable',
			//'state' => 'bail|required|string|exists:states,slug',
			'email_address' => 'bail|required|string|max:191',
			'image_id' => 'bail|string|exists:event_images,id',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$situationIds = Situation::whereIn(
			'slug',
			$request->input('situations')
		)->pluck('id');

		$state = State::where('slug', $request->input('state'))->firstOrFail();

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();

		//$content = $richTextFilter->filter($request->input('description'));
		$content = $request->input('description');

		if (empty($content)) {
			return response()->json(
				[
					'error' => "Content can't be empty",
				],
				400
			);
		}

		$event = new Event();

		$event->title = $request->title;
		$event->slug = $this->createSlug($request->title, 'App\Event');
		$event->description = $content;
		$event->start_date = $request->start_date;
		$event->end_date = $request->end_date;
		$event->last_joining_date = $request->last_joining_date;
		$event->latitude = $request->latitude ? $request->latitude : null;
		$event->longitude = $request->longitude ? $request->longitude : null;
		$event->location = $request->location ? $request->location : null;
		$event->event_time_zone = $request->event_time_zone ? $request->event_time_zone : 'AEST';
		$event->created_by = auth()->user()->id;

		$event->location_data = $request->location_data
			? json_encode($request->location_data)
			: null;
		$event->email_address = $request->email_address;

		$startDate = Carbon::instance($event->start_date);
		$endDate = Carbon::instance($event->end_date);
		$lastDateToJoin = Carbon::instance(new \DateTime($request->last_joining_date));
		$event->event_type= $request->event_type;
		$event->last_joining_date = $lastDateToJoin;

		$event->max_participents = $request->max_participents;
		$event->min_age = $request->min_age;
		$event->max_age = $request->max_age;
		$event->promo_video_url = $request->promo_video_url;
		$event->is_online = $request->is_online;
		$event->is_travel_cover = $request->is_travel_cover;
		$event->show_rsvp = $request->show_rsvp;
		$event->event_admin = $request->event_admin;



		//Create discussion for event
		$discusion = $this->createEventDiscussion($request->title,
					auth()->user()->id,
			"Follow this discussion to stay uptoDate with ".$request->title." event",
					$request->input('topic_id'));
		$event->discussion_id = $discusion->slug;


		$event->overnight = $endDate->day > $startDate->day ? true : false;
		$event->weekend =
			$startDate->isWeekend() || $endDate->isWeekend() ? true : false;
		$event->daytime =
			$startDate->hour >= 9 && $startDate->hour < 17 ? true : false;
		$event->evening =
			$startDate->hour >= 17 && $startDate->hour <= 24 ? true : false;

		$event->state()->associate($state->id);
		$event->save();

		if ($request->has('image_id')) {
			$eventImage = EventImage::where(
				'id',
				$request->input('image_id')
			)->first();
			$eventImage->user_id = $authUser->id;
			$eventImage->event()->associate($event->id);
			$eventImage->save();
		}

		$event->situations()->sync($situationIds);

		$event->save();
		$event->discussion = $discusion;

		return response()->json([
			'event' => new EventResource($event),
		]);
	}

	private function createEventDiscussion($title, $userId, $content='', $topic_id='')
	{
		$discussion = new Discussion();
		$discussion->user_id = $userId;
		$discussion->title = $title;
		$discussion->for_event = true;
		$discussion->slug = $this->createSlug(
			$title,
			'App\Discussion'
		);
		$discussion->content = $content;
		if($topic_id && $topic_id != '') {
			$discussion->topic_id = $topic_id;
		}else {
			$discussion->topic_id = 9;
		}
		$discussion->last_activity_at = Carbon::now();

		$discussion->save();
		return $discussion;
	}

	/**
	 * Display the specified event.
	 *
	 * @param \App\Event $event
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug) {
		$event = Event::where('slug', $slug)->firstOrFail();
		$now = Carbon::today();
		$relatedEvents = Event::where([['id', '<>', $event->id]])
			->inRandomOrder()
			->where('start_date', '>', $now)
			->limit(self::RELATED_SIZE)
			->get();

		return response()->json([
			'event' => new EventResource($event),
			'related' => EventResource::collection($relatedEvents),
		]);
	}

	/**
	 * Update the specified event in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Event $event
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Event $event) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_management_edit","events_management_all"]);
		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$validator = Validator::make($request->all(), [
			'title' => 'bail|string|max:191',
			'description' => 'bail|string|max:50000',
			'situations' => 'bail|array|exists:situations,slug',
			'start_date' => 'bail|date',
			'end_date' => 'bail|date|after:start_date',
			'latitude' => 'bail|nullable|string|max:191',
			'longitude' => 'bail|nullable|string|max:191',
			'location' => 'bail|nullable|string|max:191',
			'location_data' => 'bail|nullable',
			'state' => 'bail|required|string|exists:states,slug',
			'email_address' => 'bail|string|max:191',
			'image_id' => 'bail|string|exists:event_images,id',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();
		//$content = $richTextFilter->filter($request->input('description'));
		$content = $request->input('description');

		if (empty($content)) {
			return response()->json(
				[
					'error' => "Content can't be empty",
				],
				400
			);
		}

		$situationIds = Situation::whereIn(
			'slug',
			$request->input('situations')
		)->pluck('id');

		$state = State::where('slug', $request->input('state'))->firstOrFail();

		$event->title = $request->title;
		$event->description = $content;
		$event->start_date = $request->start_date;
		$event->end_date = $request->end_date;
		$event->latitude = $request->latitude ? $request->latitude : null;
		$event->longitude = $request->longitude ? $request->longitude : null;
		$event->location = $request->location ? $request->location : null;
		$event->event_time_zone = $request->event_time_zone ? $request->event_time_zone : 'AEST';
		$event->location_data = $request->location_data
			? json_encode($request->location_data)
			: null;
		$event->email_address = $request->email_address;

		$startDate = Carbon::instance($event->start_date);
		$endDate = Carbon::instance($event->end_date);

		$lastDateToJoin = isset($request->last_joining_date)?Carbon::instance(new \DateTime($request->last_joining_date)):Carbon::instance(new \DateTime($request->start_date));
		$event->event_type= $request->event_type;
		$event->last_joining_date = $lastDateToJoin;
		$event->max_participents = $request->max_participents;
		$event->min_age = $request->min_age;
		$event->max_age = $request->max_age;
		$event->promo_video_url = $request->promo_video_url;
		$event->is_online = $request->is_online;
		$event->show_rsvp = $request->show_rsvp;
		$event->is_travel_cover = $request->is_travel_cover;
		$event->event_admin = $request->event_admin;


		$event->overnight = $endDate->day > $startDate->day ? true : false;
		$event->weekend =
			$startDate->isWeekend() || $endDate->isWeekend() ? true : false;
		$event->daytime =
			$startDate->hour >= 9 && $startDate->hour < 17 ? true : false;
		$event->evening =
			$startDate->hour >= 17 && $startDate->hour <= 24 ? true : false;

		$event->state()->associate($state->id);
		//dd($event);
		if ($request->has('image_id')) {

			if ($event->image) {
				$oldImageArr = $event->image;
				foreach ($oldImageArr as $oldImage)
				{
					if($oldImage->is_featured == 0)
					{
						$oldImage->event()->dissociate();
						$oldImage->save();
						$oldImage->delete();
						break;
					}
				}
			}
			$eventImage = EventImage::where(
				'id',
				$request->input('image_id')
			)->where('is_featured', 0)->first();
			$eventImage->user_id = $authUser->id;
			$eventImage->event()->associate($event->id);
			$eventImage->save();
		}

		$event->situations()->sync($situationIds);
		$event->save();
		//$event->admin = $adminUser;
		return response()->json([
			'event' => new EventResource($event),
		]);
	}

	/**
	 * Duplicate the specified event in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Event $event
	 * @return \Illuminate\Http\Response
	 */
	public function duplicate(Request $request, Event $event) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_management_add","events_management_edit","events_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$newEvent = $event->replicate();
		$newEvent->users()->sync([]);
		$newEvent->slug = $this->createSlug($newEvent->title, 'App\Event');

		//Create discussion for event
		$discusion = $this->createEventDiscussion($newEvent->title, auth()->user()->id, "Follow this discussion to stay uptoDate with ".$newEvent->title." event");
		$newEvent->discussion_id = $discusion->slug;
		$newEvent->save();

		// Copy the event image if any
		if (isset($event->image) && !empty($event->image)) {
			foreach ($event->image as $image) {
				if ($image->is_featured == 0 && isset($image->event_id)) {
					$newImage = $image->replicate();
					$newImage->event_id = $newEvent->id;
					$newImage->save();
					break;
				}
			}
		}

		return $this->update($request, $newEvent);
	}

	/**
	 * Remove the specified event from storage.
	 *
	 * @param \App\Event $event
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, Event $event) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_management_delete","events_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$event->delete();

		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Register for an event as an authenticated user.
	 */
	public function register(Request $request, Event $event) {
		$validator = Validator::make($request->all(), [
			'phone' => 'bail|required|string',
			'content' => 'bail|required|string|max:1600',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$authUser = $this->getAuthUser($request);

		if (!$authUser || !$authUser->id) {
			return response()->json(
				[
					'error' => 'Not authorised.',
				],
				400
			);
		}

		if ($event->users->contains($authUser)) {
			return response()->json([
				'error' => 'Already registered.',
			]);
		}

		$event->users()->attach($authUser);
		$event->save();

		$phone = $request->input('phone');
		$content = $request->input('content');

		UserActivityHelper::create(
			$authUser,
			$authUser,
			'User Event Registration',
			[
				'event' => $event->title,
			]
		);

		if ($event->sos_id !== null) {
			$genders = ['male' => 'M', 'female' => 'F', 'non_binary' => 'N'];
			try {
				$userSituations =
					implode(
						', ',
						$authUser
							->situations()
							->pluck('name')
							->toArray()
					) | 'No cancer situation';
				$locationData = $authUser->location_data;
				$postcode = '';

				if (
					isset($locationData, $locationData['address_components']) &&
					is_array($locationData['address_components']) &&
					sizeof($locationData['address_components'])
				) {
					foreach (
						$locationData['address_components']
						as $addressComponent
					) {
						if (
							isset(
								$addressComponent['types'],
								$addressComponent['long_name'],
								$addressComponent['short_name']
							) &&
							is_array($addressComponent['types']) &&
							sizeof($addressComponent['types'])
						) {
							// Test if postcode
							if (
								$this->addressHasTypes(
									$addressComponent['types'],
									['postal_code']
								)
							) {
								$postcode = $addressComponent['short_name'];
							}
						}
					}
				}

				$parameters = [
					'EventId' => $event->sos_id,
					'CancerAffectedYou' => $userSituations,
					'SupportedByCanteen' => 'Yes',
					'Answer' => $content,
					'Firstname' => $authUser->first_name,
					'Lastname' => $authUser->last_name,
					'DateOfBirth' => $authUser->dob->format('Y-m-d'),
					'Gender' =>
						$genders[$authUser->gender] ?? $genders['non_binary'],
					'Email' => $authUser->email,
					'Mobile' => $phone,
					'PostCode' => $postcode,
					'Terms' => true,
				];

				$client = new Client();

				$response = $client->request(
					'POST',
					config('side-of-stage.api') . '/entry',
					[
						'json' => ($parameters),
					]
				);
			} catch (\Exception $e) {
				Log::error($e);
			}
		} else {
			Notification::route('mail', $event->email_address)->notify(
				new UserRegisteredEvent($authUser, $event, $phone, $content)
			);
		}

		return response()->json([
			'success' => true,
		]);
	}

	protected function addressHasTypes($addressComponentTypes, $types) {
		foreach ($types as $type) {
			if (array_search($type, $addressComponentTypes) === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Show all the registered users for an event.
	 */
	public function listUsers(Request $request, Event $event) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["events_management_delete","events_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$usersQuery = $event->users();
		if ($request->has('origin') && $request->input('origin')) {
			$usersQuery->where(
				'event_user.registration_origin',
				$request->input('origin')
			);
		}

		return new UserCollection($usersQuery->paginate(self::PAGINATION_SIZE));
	}
}
