import React from 'react';

interface IProps {
	src: string;
	webp: string;
	className?: string;
	alt?: string;
}

const Image: React.FC<IProps> = props => {
	// Get the extension of the defaul image
	const splitPath = props.src.split('.');
	const extension = splitPath.pop();
	let type = null;
	if (extension === 'jpg' || extension === 'jpeg') {
		type = 'image/jpeg';
	} else if (extension === 'png') {
		type = 'image/png';
	}
	return (
		<picture className={props.className}>
			<source srcSet={props.webp} type="image/webp" />
			{!!type && <source srcSet={props.src} type={extension} />}
			<img src={props.src} alt={props.alt} />
		</picture>
	);
};

export default Image;
