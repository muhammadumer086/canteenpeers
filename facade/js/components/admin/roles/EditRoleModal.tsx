import React from 'react';
import { ModalDialog } from '../../modals/local/ModalDialog';
import { TextFieldFormik } from '../../inputs';
import { Formik } from 'formik';
import { Button } from '@material-ui/core';
import { CircularProgress } from '@material-ui/core';
import * as Yup from 'yup';
import { IApiRole } from 'js/interfaces';

interface IProps {
    loading : boolean;
    role : IApiRole;
	handleEdit(id : number,name : string):void;
	handleClose(): void;
}

interface IState {}

class EditRoleModal extends React.Component<IProps, IState> {
	constructor(props : IProps){
		super(props);

	}
	public render() {
		const {loading}=this.props;
		const validationSchema=Yup.object().shape({
			title : Yup.string().required('title is required'),
		})

		return (
			<div>
				<ModalDialog
					isActive={true}
					modalTitle="Edit Role"
					ariaTag="event-delete-modal"
					handleClose={this.props.handleClose}
				>
					<Formik
					 initialValues={{
						 title : this.props.role.name,
						
					 }}
					 validationSchema={validationSchema}
					 onSubmit={value=>{
						 this.props.handleEdit(this.props.role.id,value.title);
					 }}
					 render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						//isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};
	
						return (
							<form
								className="create_event_form form"
								onSubmit={e => {
									e.preventDefault();
									handleSubmit(e);
								}}
							>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Role Title *"
											name="title"
											disabled={loading}
											{...inputProps}
										/>
									</div>
								</div>
							
							
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="contained"
											color="secondary"
											className="form-submit"
											type='submit'
										>
										{loading? <CircularProgress size={16} /> : "Update Role"}
										</Button>
									</div>
								</div>
							</form>
						);
					}}
					/>
				</ModalDialog>
			</div>
		);
	}
	
}

export default EditRoleModal;
