@extends('layouts.email')

@section('plain_text')
	@component('emails.components.paragraph')
		Community member {{$blocker->username}} has blocked {{$blocked->username}}.
	@endcomponent
@endsection

@section('content')
	@component('emails.components.paragraph')
		Community member {{$blocker->username}} has blocked {{$blocked->username}}.
	@endcomponent

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
