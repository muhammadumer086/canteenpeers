import React from 'react';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
// import FormLabel from '@material-ui/core/FormLabel';

interface IProps {
	label: string;
	value: string;
	options: Array<{
		label: string;
		value: string;
	}>;
	disabled?: boolean;
    handleChange(any):void;
	
}

const MyRadioGroup: React.FC<IProps> = (props: IProps) => {
	const optionsMapped = props.options.map(option => {
		return {
			label: option.label,
			value: option.value,
		};
	});

	

	return (
		<FormControl
			// error={props.touched[props.name] && !!props.errors[props.name]}
			className="form-input form-input-radio_wrapper"
			disabled={props.disabled}
			style={{marginLeft : "16px"}}
           // component="fieldset"   
		>
			{/* <FormLabel  component="legend" >{props.label}</FormLabel> */}
			<RadioGroup
				 className="form-input-radio_group"
				aria-label={props.label}
				value={props.value}
                onChange={props.handleChange}
                row
			>
				{optionsMapped.map((singleOption, index) => {
					return (
						<FormControlLabel
							className="form-input-radio"
							style={{alignItems : 'center'}}
							value={singleOption.value}
							control={
								<Radio
									className="form-input-radio-tick"
									color="primary"
								/>
							}
							label={singleOption.label}
                            key={index}
                            labelPlacement='end'
						/>
					);
				})}
			</RadioGroup>
			{/* {props.touched[props.name] && !!props.errors[props.name] && (
				<FormHelperText>{props.errors[props.name]}</FormHelperText>
			)} */}
		</FormControl>
	);

	
};
export default MyRadioGroup;
