import Router from 'next/router';
import Cookies from 'universal-cookie';

import { IApiToken, IApiUser } from '../interfaces';
import { ApiService } from '../services';
import isAdmin from '../helpers/isAdmin';
import {
	userAuthenticating,
	userIsAuthenticated,
	userLogout,
	apiError,
} from '../redux/actions';

export class AuthService {
	public static readonly UserToken = 'userToken';
	public static readonly UserTokenExpiry = 'userTokenExpiry';
	// private static _instance: AuthService;

	protected _verificationInterval: any = null;
	protected _isLoggedIn: boolean | null = null;
	protected _userToken: IApiToken | null = null;
	protected _userTokenExpiry: number = 0;
	protected _userData: IApiUser | null = null;
	protected _reduxStore = null;
	protected _apiService: ApiService | null = null;
	protected _cookies;
	protected _hasAuthenticationCookie: boolean;
	protected _serverUrl: string = null;
	protected _protectedRoutes: RegExp[] = [
		/^\/admin/,
		/^\/messages/,
		/^\/people-like-me/,
		/^\/discussions\/user/,
	];
	public initialising: boolean = true;

	public constructor(reduxStore, request: any = null) {
		this._reduxStore = reduxStore;
		if (request) {
			this._cookies = new Cookies(request.headers.cookie);
			this._serverUrl = request.url;
		} else {
			this._cookies = new Cookies();
		}

		this._apiService = new ApiService(reduxStore, request);
		// Set the user's token based on the cookie
		this.userToken = this._cookies.get(AuthService.UserToken);
		this._userTokenExpiry = this._cookies.get(AuthService.UserTokenExpiry)
			? parseInt(this._cookies.get(AuthService.UserTokenExpiry))
			: 0;
		this._hasAuthenticationCookie =
			this._cookies.get(AuthService.UserToken) &&
			this._cookies.get(AuthService.UserTokenExpiry);
	}

	get isLoggedIn(): boolean | null {
		return this._isLoggedIn;
	}

	get isAdmin(): boolean {
		const userData = this.userData;
		return isAdmin(userData);
	}

	get userData(): IApiUser | null {
		return JSON.parse(JSON.stringify(this._userData));
	}

	get userToken(): IApiToken | null {
		return JSON.parse(JSON.stringify(this._userToken));
	}

	get hasAuthenticationCookie(): boolean {
		return this._hasAuthenticationCookie;
	}

	set userToken(token: IApiToken | null) {
		if (token) {
			const expiry = new Date();
			expiry.setTime(expiry.getTime() + token.expires_in * 1000);
			this._cookies.set(AuthService.UserToken, token, {
				path: '/',
				expires: expiry,
			});
			this._cookies.set(AuthService.UserTokenExpiry, expiry.getTime(), {
				path: '/',
				expires: expiry,
			});
			this._userToken = token;
			this._userTokenExpiry = expiry.getTime();
			this._hasAuthenticationCookie = true;
		} else {
			this.logOut();
		}
	}

	/**
	 * Initialise the service by loading the user token if available
	 *
	 */
	public initialise(verifyAuth: boolean = false) {
		this.initialising = true;
		return new Promise<boolean>(resolve => {
			if (this.userData && !verifyAuth) {
				this.initialising = false;
				resolve(true);
				return;
			}
			if (this.hasAuthenticationCookie) {
				if (!verifyAuth) {
					this.dispatch(userAuthenticating());
				}

				const now = new Date();
				if (now.getTime() >= this._userTokenExpiry) {
					// Token refresh
					this.refreshToken(this.userToken).then(result => {
						if (result) {
							this.initialising = false;
							resolve(true);
						} else {
							this.initialising = false;
							resolve(false);
						}
					});
				} else {
					// Initialise the user data
					this.initialiseUserData(this.userToken).then(result => {
						if (result) {
							this.initialising = false;
							resolve(true);
						} else {
							this.initialising = false;
							resolve(false);
						}
					});
				}
			} else {
				this.logOut();
				this.initialising = false;
				resolve(false);
			}
		});
	}

	/**
	 * Log a user in
	 */
	public logIn(token: IApiToken) {
		this.userToken = token;
		return this.initialiseUserData(token);
	}

	public clearAuthCookies() {
		this._cookies.remove(AuthService.UserToken);
		this._cookies.remove(AuthService.UserTokenExpiry);
	}

	/**
	 * Log out the user
	 */
	public logOut() {
		this._isLoggedIn = false;
		this._userToken = null;
		this._userTokenExpiry = null;
		this._hasAuthenticationCookie = false;
		this.clearAuthCookies();
		this.redirectIfProtectedRoute();
		this.setIntercomUserData(null, true);
		this.dispatch(userLogout());
	}

	public shouldRedirectToAuth(url: string) {
		if (this.isProtectedRoute(url) && !this._isLoggedIn) {
			Router.push('/auth/login');
			return false;
		}
		return true;
	}

	protected dispatch(action: any): void {
		if (this._reduxStore) {
			this._reduxStore.dispatch(action);
		}
	}

	protected initialiseUserData(userToken: IApiToken | null) {
		this.dispatch(userAuthenticating());

		return new Promise<IApiUser | null>(resolve => {
			let accessToken = null;
			if (userToken && 'access_token' in userToken) {
				accessToken = userToken.access_token;
			}
			this._apiService
				.queryAuthGET('/api/users/me', accessToken)
				.then((data: any) => {
					if ('user' in data) {
						this._isLoggedIn = true;

						const needsShutdown = this._userData === null;

						this._userData = data.user;
						this.setIntercomUserData(this.userData, needsShutdown);
						this.dispatch(
							userIsAuthenticated(
								this.userToken,
								this.userData,
								this.isAdmin,
							),
						);
						resolve(JSON.parse(JSON.stringify(this.userData)));
					} else {
						this.logOut();
						this.dispatch(apiError([`You've been logged-out`]));
						resolve(null);
					}
				})
				.catch(() => {
					this.logOut();
					resolve(null);
				});
		});
	}

	protected refreshToken(userToken: IApiToken) {
		return new Promise<boolean>(resolve => {
			const data = {
				refresh_token: userToken.refresh_token,
			};

			this._apiService
				.queryPOST('/api/users/refresh', data)
				.then((token: IApiToken) => {
					this.userToken = token;
					this.initialiseUserData(token).then(result => {
						if (result) {
							resolve(true);
						} else {
							resolve(false);
						}
					});
				})
				.catch(() => {
					this.logOut();
					this.dispatch(apiError([`You've been logged-out`]));
					resolve(false);
				});
		});
	}

	protected setIntercomUserData(
		userData: IApiUser | null,
		needsShutdown: boolean = true,
	) {
		if (typeof window !== 'undefined' && typeof Intercom !== 'undefined') {
			const intercomParams = {
				custom_launcher_selector: '.consellor_chat',
				hide_default_launcher: true,
				alignment: 'right',
				horizontal_padding: 30,
				vertical_padding: 16,
				platform: process.env.APP_NAME,
			};
			let intercomAction = 'update';
			if (needsShutdown) {
				Intercom('shutdown');
				intercomAction = 'boot';
			}
			if (userData) {
				Intercom(
					intercomAction,
					Object.assign({}, intercomParams, {
						email: userData.email,
						name: userData.full_name,
						user_id: userData.id,
						user_hash: userData.intercom_hash,
					}),
				);
			} else {
				Intercom(intercomAction, Object.assign({}, intercomParams));
			}
		}
	}

	protected redirectIfProtectedRoute() {
		if (
			typeof window === 'undefined' &&
			this._serverUrl &&
			this.isProtectedRoute(this._serverUrl)
		) {
			// On server, will defer to error catching happening higher up
			throw new Error('Not authenticated');
		}
	}

	protected isProtectedRoute(url: string): boolean {
		let result: boolean = false;

		this._protectedRoutes.forEach(route => {
			if (url.match(route)) {
				result = true;
			}
		});

		return result;
	}
}
