import React from 'react';
import { connect } from 'react-redux';

import Pill from '../general/Pill';

import { IStoreState } from '../../redux/store';
import { withServices } from '../../services';

interface IProps {
	count: number;
	userAuthenticated: boolean | null;
	scrollToReplies(): void;
}
const DiscussionReplyCount: React.FC<IProps> = (props: IProps) => {
	let classes = ['discussion_tile-content-replies'];
	{ !props.userAuthenticated &&
		classes.push('discussion_tile-content-replies--not-logged');
	}
	return (
		<div className={classes.join(' ')}>
			<Pill
				label="Replies"
				active={false}
				disabled={false}
				clickable={true}
				statusCount={props.count}
				isPost={true}
				isReply={true}
				onClick={props.scrollToReplies}
			/>
		</div>
	)
};

function mapStateToProps(state: IStoreState) {
	const {
		userAuthenticated,
	} = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(withServices(DiscussionReplyCount));
