<?php

namespace App;

use App\AgeRange;
use App\AgeRangeAnnouncement;
use App\AnnouncementImage;
use App\AnnouncementSituation;
use App\AnnouncementState;
use App\Situation;
use App\State;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model {
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];

	/**
	 * The attributes that should be mass assigned.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'message',
		'url_href',
		'url_href_as',
		'url_type',
		'url_label',
		'image_id',
		'public',
	];

	/**
	 * The attributes that are hidden in arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['user_id'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'announcements';

	/**
	 * The image associated with this announcement
	 */
	public function image() {
		return $this->hasOne(AnnouncementImage::class, 'id', 'image_id');
	}

	/**
	 * The user that created this announcement
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

	/**
	 * The user(s) that have received the announcement
	 */
	public function users() {
		return $this->belongsToMany(
			'App\User',
			'announcement_user',
			'announcement_id',
			'user_id'
		);
	}

	/**
	 * The situations targeted with this announcement
	 */
	public function situations() {
		return $this->belongsToMany(Situation::class)->using(
			AnnouncementSituation::class
		);
	}

	/**
	 * The age ranges targeted with this announcement
	 */
	public function ageRanges() {
		return $this->belongsToMany(AgeRange::class)->using(
			AgeRangeAnnouncement::class
		);
	}

	/**
	 * The states targeted with this announcement
	 */
	public function states() {
		return $this->belongsToMany(State::class)->using(
			AnnouncementState::class
		);
	}
}
