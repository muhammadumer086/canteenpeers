import React from 'react';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import {
	IStoreState,
	newMessageDisplay,
	reportModalDisplay,
} from '../../redux/store';

import { ApiService, withServices } from '../../services';

import { IApiDiscussion, IApiUser } from '../../interfaces';
import { EReportModalType } from '../../redux/reducers/reportModal';

interface IProps {
	userAuthenticated: boolean;
	userAdmin: boolean;
	userData: IApiUser | null;
	discussion: IApiDiscussion;
	apiService: ApiService;
	withAgeSensitive?: boolean;
	canEdit: boolean;
	editDiscussion(): void;
	deleteDiscussion?(discussionId?: number | null): void;
	handleDelete?(): void;
	dispatch(action: any): void;
}

interface IState {
	anchorEl: any | null;
}

class DiscussionReport extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			anchorEl: null,
		};

		this.handleMenuOpen = this.handleMenuOpen.bind(this);
		this.handleMenuClose = this.handleMenuClose.bind(this);
		this.openNewConversationModal = this.openNewConversationModal.bind(
			this,
		);
		this.openReportDiscussionModal = this.openReportDiscussionModal.bind(
			this,
		);
		this.openAgeSensitiveDiscussionModal = this.openAgeSensitiveDiscussionModal.bind(
			this,
		);
		this.deletePost = this.deletePost.bind(this);
		this.editPost = this.editPost.bind(this);
	}

	get canReport(): boolean {
		return (
			this.props.userAuthenticated &&
			this.props.discussion.user.id !== this.props.userData.id
		);
	}

	get canReportAgeSensitive(): boolean {
		return (
			this.props.userAuthenticated &&
			this.props.discussion.user.id !== this.props.userData.id &&
			!this.props.discussion.main_discussion_id
		);
	}

	get hasAgeSensitive(): boolean {
		return (
			this.props.withAgeSensitive && this.props.discussion.age_sensitive
		);
	}

	get canDelete(): boolean {
		return (
			((this.props.userAuthenticated && this.props.userAdmin) ||
				(this.props.userData.username ===
					this.props.discussion.user.username &&
					!!this.props.deleteDiscussion)) &&
			!!this.props.deleteDiscussion
		);
	}

	get canEdit(): boolean {
		return (
			((this.props.userAuthenticated && this.props.userAdmin) ||
				this.props.userData.username ===
					this.props.discussion.user.username) &&
			!!this.props.canEdit
		);
	}

	public render() {
		if (!this.canReport && !this.canDelete) {
			return null;
		}

		return (
			<div className="discussion_report">
				<IconButton
					className="discussion_report-button"
					onClick={this.handleMenuOpen}
				>
					<Icon>more_vert</Icon>
				</IconButton>
				<Menu
					anchorEl={this.state.anchorEl}
					open={Boolean(this.state.anchorEl)}
					onClose={this.handleMenuClose}
				>
					{this.canReport && (
						<React.Fragment>
							<MenuItem onClick={this.openNewConversationModal}>
								Message User
							</MenuItem>
							{!this.hasAgeSensitive &&
								this.canReportAgeSensitive && (
									<MenuItem
										onClick={
											this.openAgeSensitiveDiscussionModal
										}
									>
										Age Sensitive Post
									</MenuItem>
								)}
							<MenuItem onClick={this.openReportDiscussionModal}>
								Report Post
							</MenuItem>
						</React.Fragment>
					)}

					{this.canDelete && (
						<MenuItem onClick={this.deletePost}>
							Delete Post
						</MenuItem>
					)}
					{this.canEdit && (
						<MenuItem onClick={this.editPost}>Edit Post</MenuItem>
					)}
				</Menu>
			</div>
		);
	}

	protected handleMenuOpen(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}

	protected handleMenuClose() {
		this.setState({
			anchorEl: null,
		});
	}

	protected openNewConversationModal() {
		this.handleMenuClose();
		this.props.dispatch(newMessageDisplay(this.props.discussion.user));
	}

	protected openReportDiscussionModal() {
		this.handleMenuClose();
		this.props.dispatch(
			reportModalDisplay(this.props.discussion, EReportModalType.HARMFUL),
		);
	}

	protected openAgeSensitiveDiscussionModal() {
		this.handleMenuClose();
		this.props.dispatch(
			reportModalDisplay(
				this.props.discussion,
				EReportModalType.AGE_SENSITIVE,
			),
		);
	}

	protected deletePost() {
		this.handleMenuClose();
		this.props.deleteDiscussion(this.props.discussion.id);
	}

	protected editPost() {
		this.handleMenuClose();
		this.props.editDiscussion();
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userAdmin, userData } = state.user;
	return {
		userAuthenticated,
		userAdmin,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(DiscussionReport));
