import React from 'react';

import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';

interface IProps {
	label: string;
	name: string;
	values: any;
	touched: any;
	disabled?: boolean;
	handleChange(e: React.ChangeEvent<any>): void;
	handleBlur(e: any): void;
}

export const SwitchFieldFormik: React.FC<IProps> = (props: IProps) => {
	return (
		<FormControl className="form-input">
			<FormGroup>
				<FormControlLabel
					control={
						<Switch
							name={props.name}
							onChange={props.handleChange}
							onBlur={props.handleBlur}
							checked={props.values[props.name]}
							value={props.values[props.name]}
						/>
					}
					label={props.label}
					className="form-input-switch"
				/>
			</FormGroup>
		</FormControl>
	);
};
