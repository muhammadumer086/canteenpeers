import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import moment from 'moment';
import Button from '@material-ui/core/Button';

import {
	IApiUser,
	IApiSituation,
	IApiRole,
	ISelectOption,
} from '../../../interfaces';

import { userPanelDisplay, apiSuccess, apiError } from '../../../redux/store';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import HeroHeading from '../../../components/general/HeroHeading';
import CreateNewUserModal from '../users/modals/CreateNewUserModal';
import AdminTable, {
	IAdminTableColumn,
} from '../../../components/admin/AdminTable';
import {
	IAdminProps,
	IAdminState,
} from '../../../components/admin/withAdminPage';
import {
	ColumnNameSituation,
	ColumnAge,
	ColumnState,
	ColumnGender,
	ColumnSituation,
	ColumnLastLogin,
	ColumnTotalLogins,
	ColumnActivityLog,
	ColumnTotalBlogs,
	ColumnTotalDiscussions,
	ColumnTotalLikes,
	ColumnTotalHugs,
	ColumnTotalFollowedDiscussions,
	ColumnTotalSavedResources,
	ColumnRole,
} from '../../../components/admin/AdminTableColumns';
import AdminFilter from '../../../components/admin/AdminFilter';
import AdminFilterUsers from '../../../components/admin/AdminFilterUsers';
import { processQueryParams } from '../../../helpers/processQueryParams';
import { downloadCsvFile } from '../../../helpers/downloadFile';

import { ICreateUserFormik } from '../users/forms/CreateNewUserFormik';
import { checkPermission } from '../../../helpers/checkPermission';
export enum EUsersManagementPermissions {
	READ = 'users_management_read',
	ADD = 'users_management_add',
	EDIT = 'users_management_edit',
	DELETE = 'users_management_delete',
	ALL = 'users_management_all',
}
interface IProps extends IAdminProps {}

interface IState extends IAdminState {
	data: IApiUser[];
	selectedData: IApiUser[];
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	isLoadingUsers: boolean;
	filterOpen: boolean;
	filterValues: any;
	reportGenerating: boolean;
	isCreatUserModalActive: boolean;
	situations: IApiSituation[] | null;
	permissions: string[];
	isLoadingPermission: boolean;
	rolesOptions: ISelectOption[];
	tableColumns: IAdminTableColumn[];
	isUpdating: boolean;
	updatingUserId: number;
}

class Users extends React.Component<IProps, IState> {
	protected originalFilterValues = {
		sort: 'username-asc',
		user_id: '',
		name: '',
		username: '',
		email: '',
		age: [],
		state: [],
		gender: [],
		situations: [],
		cancerTypes: [],
		lastLoginFrom: null,
		lastLoginTo: null,
		userCreatedFrom: null,
		userCreatedTo: null,
		userVerifiedFrom: null,
		userVerifiedTo: null,
		//excludeNotVerified: true,
		verified: '1',
		role: ['user'],
	};

	constructor(props: IProps) {
		super(props);

		this.state = {
			data: [],
			selectedData: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isLoadingUsers: false,
			filterOpen: false,
			filterValues: JSON.parse(JSON.stringify(this.originalFilterValues)),
			reportGenerating: false,
			isCreatUserModalActive: false,
			situations: null,
			isLoadingPermission: false,
			permissions: [],
			rolesOptions: [{ label: 'user', value: 'user' }],
			tableColumns: this.getTableColumns(),
			isUpdating: false,
			updatingUserId: 0,
		};

		this.displayUser = this.displayUser.bind(this);
		this.loadUsers = this.loadUsers.bind(this);
		this.loadSituations = this.loadSituations.bind(this);
		this.onRowSelectionUpdate = this.onRowSelectionUpdate.bind(this);
		this.handleOpenFilter = this.handleOpenFilter.bind(this);
		this.handleCloseFilter = this.handleCloseFilter.bind(this);
		this.handleFilter = this.handleFilter.bind(this);
		this.handleClear = this.handleClear.bind(this);
		this.onGenerateReport = this.onGenerateReport.bind(this);
		this.onUpdateSort = this.onUpdateSort.bind(this);
		this.handleNewUserModal = this.handleNewUserModal.bind(this);
		this.submitNewUserPayload = this.submitNewUserPayload.bind(this);
		this.loadRoles = this.loadRoles.bind(this);
		this.initialise = this.initialise.bind(this);
		this.onReminderEmail = this.onReminderEmail.bind(this);
		this.generateTableColumns = this.generateTableColumns.bind(this);
	}

	public componentDidMount() {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EUsersManagementPermissions.ALL,
				EUsersManagementPermissions.READ,
			);
		this.loadRoles();

		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		this.initialise(this.props, prevProps);
	}

	public render() {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EUsersManagementPermissions.ALL,
				EUsersManagementPermissions.READ,
			);
		const addPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EUsersManagementPermissions.ALL,
				EUsersManagementPermissions.ADD,
			);
		//const editPermission: boolean = this.props.roleName === "admin" || checkPermission(this.props.permissions, EUsersManagementPermissions.ALL, EUsersManagementPermissions.EDIT);
		const generateCsvPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EUsersManagementPermissions.ALL,
			);

		const tableParams = {
			tableColumns: this.state.tableColumns,
			data: this.state.data,
			onRowClick: this.displayUser,
			loading: this.state.isLoadingUsers,
			onLoadMore:
				this.state.currentPage < this.state.maxPages && readPermission
					? this.loadUsers
					: undefined,
			selectedData: this.state.selectedData,
			onRowSelectionUpdate: this.onRowSelectionUpdate,
			onGenerateReport: generateCsvPermission
				? this.onGenerateReport
				: undefined,
			reportGenerating: this.state.reportGenerating,
			totalMatchedRecords: this.state.total,
			sort: this.state.filterValues.sort,
			onUpdateSort: this.onUpdateSort,
			onReminderEmail: this.onReminderEmail,
		};

		let heroInfo = `Showing ${this.state.data.length} of ${this.state.total} users`;
		if (this.state.selectedData.length) {
			heroInfo += `, selected ${this.state.selectedData.length}`;
		}
		if (this.state.isLoadingUsers && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}

		return (
			<div>
				<Head>
					<title>Users Management — {process.env.APP_NAME}</title>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading
							title="User Management"
							subInfo={heroInfo}
						/>
						<span className="hero-actions">
							{addPermission && (
								<Button
									variant="contained"
									color="primary"
									onClick={this.handleNewUserModal}
								>
									Add new user
								</Button>
							)}

							<Button
								variant="outlined"
								color="primary"
								onClick={this.handleOpenFilter}
							>
								Filter Users
							</Button>
						</span>
					</header>
					{readPermission && (
						<div className="admin_content">
							<AdminTable {...tableParams} />
						</div>
					)}
					{this.renderFilters()}
				</LayoutDashboard>

				{this.state.isCreatUserModalActive && (
					<CreateNewUserModal
						isActive={this.state.isCreatUserModalActive}
						isSubmitting={this.state.isLoadingUsers}
						roles={this.state.rolesOptions}
						situations={this.state.situations}
						handleClose={this.handleNewUserModal}
						onSubmit={this.submitNewUserPayload}
					/>
				)}
			</div>
		);
	}

	protected renderFilters() {
		return (
			<AdminFilter
				isOpen={this.state.filterOpen}
				handleCloseFilter={this.handleCloseFilter}
			>
				<AdminFilterUsers
					initialValues={this.state.filterValues}
					roleOptions={this.state.rolesOptions}
					handleFilter={this.handleFilter}
					handleClear={this.handleClear}
				/>
			</AdminFilter>
		);
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				props.permissions,
				EUsersManagementPermissions.ALL,
				EUsersManagementPermissions.READ,
			);
		const permissions = prevProps ? prevProps.permissions : [];
		const prevReadPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				permissions,
				EUsersManagementPermissions.ALL,
				EUsersManagementPermissions.READ,
			);
		const all_permissions: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(props.permissions, EUsersManagementPermissions.ALL);

		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin) ||
			(!prevReadPermission && readPermission)
		) {
			this.loadUsers(false);
			this.loadSituations();
			this.generateTableColumns(all_permissions);
		}
	}

	protected loadUsers(append: boolean = true) {
		if (
			!this.state.isLoadingUsers &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			let params: any = processQueryParams(
				JSON.parse(JSON.stringify(this.state.filterValues)),
			);

			if (append) {
				params.page = this.state.currentPage + 1;
			}
			if (params.verified) {
				//verified in query params mean verified by email

				if (params.verified === '2') {
					params.is_mobile_verified = '1';
					delete params.verified;
				} else if (params.verified === '3') {
					params.is_email_verified = '1';
					delete params.verified;
				} else if (params.verified === 'any') {
					delete params.verified;
				}
			}

			const url = buildUrl(null, {
				path: '/api/admin/users',
				queryParams: params,
			});
			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'users' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								let data: IApiUser[] = response.data.users;

								if (append) {
									data = JSON.parse(
										JSON.stringify(this.state.data),
									);
									(response.data.users as IApiUser[]).forEach(
										item => {
											data.push(item);
										},
									);
								}

								this.setState({
									data,
									isLoadingUsers: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
									// Reset the selected user if reseting the list of users
									selectedData: append
										? this.state.selectedData
										: [],
								});
							}
						});
				},
			);
		}
	}

	protected loadSituations() {
		this.setState(
			{
				isLoadingUsers: true,
			},
			() => {
				this.props.apiService
					.queryGET('/api/situations', 'situations')
					.then((situationsList: IApiSituation[]) => {
						const userSituations: IApiSituation[] = [];
						if (situationsList instanceof Array) {
							situationsList.map((situation: IApiSituation) => {
								if (!situation.cancer_type) {
									userSituations.push(situation);
								}
							});
						}

						this.setState({
							situations: userSituations,
							isLoadingUsers: false,
						});
					})
					.catch(error => {
						this.setState({
							situations: null,
							isLoadingUsers: false,
						});

						this.props.dispatch(apiError([error]));
					});
			},
		);
	}

	protected displayUser(user: IApiUser) {
		this.props.dispatch(userPanelDisplay(user));
	}

	protected onRowSelectionUpdate(selectedData: IApiUser[]) {
		this.setState({
			selectedData,
		});
	}

	protected handleOpenFilter() {
		this.setState({
			filterOpen: true,
		});
	}
	protected handleCloseFilter() {
		this.setState({
			filterOpen: false,
		});
	}

	protected handleFilter(values: any) {
		this.setState(
			{
				filterOpen: false,
				filterValues: values,
				data: [],
				selectedData: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadUsers(false);
			},
		);
	}

	protected handleClear() {
		this.handleFilter(
			JSON.parse(JSON.stringify(this.originalFilterValues)),
		);
	}

	protected handleNewUserModal() {
		this.setState({
			isCreatUserModalActive: !this.state.isCreatUserModalActive,
		});
	}

	protected submitNewUserPayload(values: ICreateUserFormik) {
		if (values) {
			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					const url = buildUrl(null, {
						path: '/api/admin/users',
					});

					this.props.apiService
						.queryPOST(url, values)
						.then(() => {
							// if (this.props.roleName === 'admin') {
							// 	if (
							// 		values.user_type !== 'admin' &&
							// 		values.user_type !== 'user'
							// 	) {
							// 		this.updateUserRole(
							// 			values.user_type,
							// 			values.username,
							// 		);
							// 	}
							// } else {
							this.setState({
								isLoadingUsers: false,
								isCreatUserModalActive: false,
							});
							this.props.dispatch(
								apiSuccess([
									'User has successfuly been created',
								]),
							);

							this.loadUsers(false);
							// }
						})
						.catch(error => {
							this.setState({
								isLoadingUsers: false,
							});

							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		}
	}

	protected onGenerateReport() {
		this.setState(
			{
				reportGenerating: true,
			},
			() => {
				const data = Object.assign({}, this.state.filterValues, {
					id: this.state.selectedData.map(user => user.id),
				});

				let params: any = processQueryParams(data);
				if (params.verified) {
					//verified in query params mean verified by email
					if (params.verified === '2') {
						params.is_mobile_verified = '1';
						delete params.verified;
					} else if (params.verified === '3') {
						params.is_email_verified = '1';
						delete params.verified;
					} else if (params.verified === 'any') {
						delete params.verified;
					}
				}

				const url = buildUrl(null, {
					path: '/api/admin/users/export',
					queryParams: params,
				});

				const fileName =
					'canteen-users-report-' +
					moment().format('YYYYMMDD-HHmm') +
					'.csv';

				downloadCsvFile(this.props.apiService, url, fileName).then(
					() => {
						this.setState({
							reportGenerating: false,
						});
					},
				);
			},
		);
	}

	protected onUpdateSort(sortable: string, direction: 'asc' | 'desc') {
		this.setState(
			{
				filterValues: Object.assign(this.state.filterValues, {
					sort: `${sortable}-${direction}`,
				}),
				data: [],
				selectedData: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadUsers(false);
			},
		);
	}

	protected loadRoles = () => {
		if (this.props.roleName !== 'admin') {
			return;
		}
		const url = '/api/roles?page_size=1000';
		this.props.apiService
			.queryGET(url)
			.then((response: any) => {
				if ('roles' in response && response.roles.data) {
					const data = response.roles.data as Array<IApiRole>;
					const rolesOptions: ISelectOption[] = data.map(r => {
						return {
							value: r.name,
							label: r.name,
							tooltip: r.id.toString(), //saving ids to toolpit for later use
						};
					});
					this.setState({
						rolesOptions: rolesOptions,
						isLoadingPermission: false,
					});
				}
			})
			.catch(_error => {
				this.setState({ isLoadingPermission: false });
			});
	};
	protected updateUserRole(role: string, username: string) {
		const roleIndex = this.state.rolesOptions.findIndex(
			r => r.value === role,
		);
		const roleId =
			roleIndex > -1 ? this.state.rolesOptions[roleIndex].tooltip : -1; //saved ids in toolpit
		this.props.apiService
			.queryPUT(`api/roles/${username}/${roleId}/assignrole`, {})
			.then((_userData: IApiUser) => {
				this.setState({
					isLoadingUsers: false,
					isCreatUserModalActive: false,
				});
				this.props.dispatch(
					apiSuccess(['User has successfuly been created']),
				);

				this.loadUsers(false);
			})
			.catch(error => {
				this.setState({
					isLoadingUsers: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}
	protected getTableColumns = () => {
		const tableColumns = [
			{
				header: 'Name',
				size: 20,
				renderer: ColumnNameSituation,
				sortable: 'username',
			},
			{
				header: 'Age',
				size: 7,
				renderer: ColumnAge,
				sortable: 'dob',
				sortableReverse: true,
			},
			{
				header: 'State',
				size: 7,
				renderer: ColumnState,
				sortable: 'state',
			},
			{
				header: 'Gender',
				size: 10,
				renderer: ColumnGender,
				sortable: 'gender',
			},
			{
				header: 'Situation',
				size: 20,
				renderer: ColumnSituation,
			},
			{
				header: 'Role',
				size: 10,
				renderer: ColumnRole,
			},
			{
				header: 'Last Login',
				size: 10,
				renderer: ColumnLastLogin,
				sortable: 'last_login',
			},
			{
				header: 'Total Logins',
				size: 10,
				renderer: ColumnTotalLogins,
				sortable: 'count_logins',
			},
			{
				header: 'Active Discussions',
				size: 12,
				renderer: ColumnTotalDiscussions,
				sortable: 'count_active_discussions',
			},
			{
				header: 'I Get It',
				size: 7,
				renderer: ColumnTotalLikes,
				sortable: 'count_likes',
			},
			{
				header: 'Hugs',
				size: 7,
				renderer: ColumnTotalHugs,
				sortable: 'count_hugs',
			},
			{
				header: 'Followed Discussions',
				size: 14,
				renderer: ColumnTotalFollowedDiscussions,
				sortable: 'count_followed_discussions',
			},
			{
				header: 'Total Blogs',
				size: 10,
				renderer: ColumnTotalBlogs,
				sortable: 'count_blogs',
			},
			{
				header: 'Saved Resources',
				size: 12,
				renderer: ColumnTotalSavedResources,
				sortable: 'count_saved_resources',
			},
		];

		return tableColumns;
	};
	protected generateTableColumns = (permission: boolean) => {
		const { tableColumns } = this.state;
		if (permission) {
			const newTableColums = [...tableColumns];
			const foundIndex = newTableColums.findIndex(
				c => c.header === 'User Activity Log',
			);
			if (foundIndex < 0) {
				newTableColums.push({
					header: 'User Activity Log',
					size: 12,
					renderer: ColumnActivityLog,
				});
				this.setState({ tableColumns: newTableColums });
			}
		}
	};

	protected onReminderEmail = (user: IApiUser) => {
		this.setState(
			{
				isUpdating: true,
				updatingUserId: user.id,
			},
			() => {
				const url = buildUrl(null, {
					path: `/api/users/${user.username}/reSendVerificationEmail`,
				});

				const values = { email: user.email };
				this.props.apiService
					.queryPOST(url, values)
					.then(_response => {
						this.setState({
							isUpdating: false,
							updatingUserId: 0,
						});

						this.props.dispatch(
							apiSuccess(['Email has successfuly been sent']),
						);

						this.loadUsers(false);
					})
					.catch(error => {
						this.setState({
							isUpdating: false,
							updatingUserId: 0,
						});

						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	};
}

export default Users;
