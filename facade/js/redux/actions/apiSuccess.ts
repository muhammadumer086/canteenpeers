import { IStoreState } from '../store';
import { actionTypes } from '../actions';

let dismissTimeout = null;

export const apiSuccess = (apiSuccess: string[]) => (dispatch, _getState: () => IStoreState) => {
	// Auto-dismiss message when in browser
	if ((process as any).browser) {
		if (dismissTimeout) {
			window.clearTimeout(dismissTimeout);
		}
		dismissTimeout = window.setTimeout(() => {
			dispatch({
				type: actionTypes.API_SUCCESS_DISMISS,
			});
		}, 3000);
	}

	if (!(apiSuccess instanceof Array)) {
		apiSuccess = [apiSuccess];
	}

	return dispatch({
		type: actionTypes.API_SUCCESS,
		apiSuccess: apiSuccess,
	});
}

export const apiSuccessDismiss = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().apiSuccess.apiSuccessShowing) {
		return;
	}
	return dispatch({
		type: actionTypes.API_SUCCESS_DISMISS,
	});
}
