import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import buildUrl from 'build-url';
import { withRouter } from 'next/router';
import moment from 'moment';
import 'moment-timezone';

import { IStoreState, apiError } from '../js/redux/store';
import {
	IApiEvent,
	IApiUser,
	IApiPagination,
	IApiTopic,
	IApiSituation,
} from '../js/interfaces';
import { ApiService, AuthService } from '../js/services';

import {
	IFilters,
	IFiltersSelected,
	IFiltersGroup,
	IFilterValue,
	FilterResultsHelpers,
} from '../js/components/general/FilterResults';

import EventTile from '../js/components/events/EventTile';
import LayoutDashboard from '../js/components/general/LayoutDashboard';
import Footer from '../js/components/general/Footer';
import isAdmin from '../js/helpers/isAdmin';
import HeroHeading from '../js/components/general/HeroHeading';
import FilterResults from '../js/components/general/FilterResults';
import ListLoading from '../js/components/general/ListLoading';

import { GTMDataLayer } from '../js/helpers/dataLayer';

export interface IFormValues {
	cancerTypes: string[];
	state: string[];
	duration: string[];
	time: string[];
}

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	eventsData: {
		data: {
			events: IApiEvent[];
		};
		meta: IApiPagination;
		filters: {
			cancerTypes: IFilterValue[];
		};
	};
	topicsLoading: boolean;
	topics: IApiTopic[];
	situationsLoading: boolean;
	situations: IApiSituation[];
	apiService: ApiService;
	authService: AuthService;
	updateDataCount: number;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	eventsData: IApiEvent[];
	eventsTotal: number;
	isLoadingEvents: boolean;
	currentPage: number;
	maxPages: number;
	filters: IFilters;
	selectedFilters: IFiltersSelected;
	noEventMessage: String;
}
const ausZoneArr = moment.tz.zonesForCountry('AU');
const nzZoneArr = moment.tz.zonesForCountry('NZ');
class Events extends React.Component<IProps, IState> {
	protected filterHelpers: FilterResultsHelpers = new FilterResultsHelpers();
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ apiService }) {
		const eventsData = await (apiService as ApiService).queryGET(
			'/api/events',
		);

		return {
			eventsData,
		};
	}

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			eventsData: props.eventsData.data.events,
			currentPage: props.eventsData.meta.current_page,
			maxPages: props.eventsData.meta.last_page,
			isLoadingEvents: false,
			eventsTotal: props.eventsData.meta.total,
			filters: this.generateFilters(),
			noEventMessage: "No upcoming events as of now.",
			selectedFilters: {
				filters: [],
			},
		};

		this.loadEvents = this.loadEvents.bind(this);
		this.handleFiltersToggle = this.handleFiltersToggle.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
		this.handleFiltersReset = this.handleFiltersReset.bind(this);
		this.handleFiltersSubmit = this.handleFiltersSubmit.bind(this);
		this.trackFilterUpdate = this.trackFilterUpdate.bind(this);
		this.trackEventTileOpen = this.trackEventTileOpen.bind(this);
		this.loadMyEvents = this.loadMyEvents.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.situations !== prevProps.situations ||
			this.props.topics !== prevProps.topics ||
			this.props.updateDataCount !== prevProps.updateDataCount
		) {
			this.setState(
				{
					filters: this.generateFilters(),
					selectedFilters: {
						filters: [],
					},
				},
				() => {
					this.updateData();
				},
			);
		}
	}

	public render() {
		const pageTitle =
			'Cancer Events In Australia For young adult & Teens | Canteen';

		const url = buildUrl(process.env.APP_URL, {
			path: `/events/`,
		});

		const featuredImageUrl = `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;

		const seoDescription =
			'Check out our range of resources aimed at helping young people overcome challenges with cancer. Register to connect with other teens &amp; get support.';
		return (
			<div>
				<Head>
					<title>{pageTitle}</title>
					<meta name="description" content={seoDescription} />

					<meta
						name="og:description"
						property="og:description"
						content={seoDescription}
					/>

					<meta
						name="og:image"
						property="og:image"
						content={featuredImageUrl}
					/>
					<meta name="og:url" property="og:url" content={url} />
					<meta
						name="og:title"
						property="og:title"
						content={pageTitle}
					/>
					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>
					<meta
						name="viewport"
						content="width=device-width, initial-scale=1.0"
					/>
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadEvents}>
					<header className="hero theme--accent">
						<HeroHeading iconSlug="events" title="Events" />
					</header>

					<FilterResults
						filters={this.state.filters}
						selectedFilters={this.state.selectedFilters}
						onToggle={this.handleFiltersToggle}
						onUpdateFilters={this.handleFiltersUpdate}
						onReset={this.handleFiltersReset}
						onSubmit={this.handleFiltersSubmit}
						total={this.state.eventsTotal}
						totalString={{
							noResults: 'No events',
							singular: '1 event',
							plural: 'events',
						}}
						gtmDataLayer={this.trackFilterUpdate}
						loadMyEvents={this.loadMyEvents}
					/>

					<div className="events-tiles">

						{this.state.eventsData.length > 0 ? this.state.eventsData.map((event, ind) => {
							return (

								<EventTile
									key={ind}
									event={event}
									index={ind}
									gtmDataLayer={this.trackEventTileOpen}
									ausZoneArr={ausZoneArr}
									nzZoneArr={nzZoneArr}
								/>

							);
						}) : <div>{this.state.noEventMessage}</div>}
					</div>
					<ListLoading active={this.state.isLoadingEvents} />

					<Footer />
				</LayoutDashboard>
			</div>
		);
	}

	protected updateData() {
		// Scroll to the top (browser only)
		if (typeof window !== 'undefined') {
			const dashboard = window.document.querySelector(
				'.layout_dashboard-content_container',
			);
			dashboard.scroll({
				top: 0,
				left: 0,
			});
		}

		this.loadEvents(false);
	}

	protected loadEvents(append: boolean = true) {
		if (
			!this.state.isLoadingEvents &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			this.setState(
				{
					isUpdating: !append,
					isLoadingEvents: true,
				},
				() => {
					const url = this.buildQueryParams(append);

					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'events' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let events: IApiEvent[] = response.data.events;

								if (append) {
									events = JSON.parse(
										JSON.stringify(this.state.eventsData),
									);
									(response.data
										.events as IApiEvent[]).forEach(
											event => {
												events.push(event);
											},
										);
								}

								this.setState({
									isUpdating: false,
									isLoadingEvents: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									eventsTotal: response.meta.total,
									eventsData: events,
								});
							} else {
								this.props.dispatch(
									apiError(['Invalid data returned by API']),
								);
							}
						})
						.catch(error => {
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isUpdating: false,
								isLoadingEvents: false,
							});
						});
				},
			);
		}
	}
	protected loadMyEvents = (
		append: boolean = true,
		past_events: string,
	) => {
		this.setState(
			{
				isUpdating: !append,
				isLoadingEvents: true,
			},
			() => {
				//const url = this.buildQueryParams(append);
				let noEventMessage = "";
				const params = {};
				if (past_events === "past_events") {
					params['past_events'] = 1;
					noEventMessage = "Looks like haven't attended any event yet. Check the upcoming events to see something you like.";
				} else if (past_events === "my_events") {
					noEventMessage = "Looks like you have not registered for any event yet. Check the upcoming events to see something you like.";
					params['my_events'] = 1;
				}
				else {
					noEventMessage = "No upcoming events as of now.";
				}
				const url = buildUrl(null, {
					path: '/api/events',
					queryParams: params,
				});
				this.props.apiService
					.queryGET(url)
					.then((response: any) => {
						if (
							'data' in response &&
							'events' in response.data &&
							'meta' in response &&
							'current_page' in response.meta &&
							'last_page' in response.meta
						) {
							let events: IApiEvent[] = response.data.events;

							if (append) {
								events = JSON.parse(
									JSON.stringify(this.state.eventsData),
								);
								(response.data.events as IApiEvent[]).forEach(
									event => {
										events.push(event);
									},
								);
							}

							this.setState({
								isUpdating: false,
								isLoadingEvents: false,
								currentPage: response.meta.current_page,
								maxPages: response.meta.last_page,
								eventsTotal: response.meta.total,
								eventsData: events,
								noEventMessage
							});
						} else {
							this.props.dispatch(
								apiError(['Invalid data returned by API']),
							);
						}
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isUpdating: false,
							isLoadingEvents: false,
						});
					});
			},
		);
	};

	protected buildQueryParams(withPage: boolean = false) {
		const params: any = {};

		this.state.selectedFilters.filters.forEach(singleFilter => {
			console.log(singleFilter);
			if (singleFilter.filterValues instanceof Array) {
				singleFilter.filterValues.forEach((filterValue, index) => {
					// params[`${singleFilter.paramName}[${index}]`] =
					// 	filterValue.value;
					params[`${singleFilter.paramName}[${index}]`] =
						filterValue.value;
				});
			} else {
				params[`${singleFilter.paramName}`] =
					singleFilter.filterValues.value;
			}
		});

		if (withPage) {
			params.page = this.state.currentPage + 1;
		}

		if (!this.state.filters.filterToggle) {
			params.anySituation = true;
		}

		return buildUrl(null, {
			path: '/api/events',
			queryParams: params,
		});
	}

	protected handleFiltersSubmit(selectedFilters: IFiltersSelected) {
		this.setState(
			{
				selectedFilters,
				filters: this.filterHelpers.updateFiltersBasedOnSelectedFilters(
					selectedFilters,
					this.state.filters,
				),
			},
			() => {
				this.updateData();
			},
		);
	}

	protected handleFiltersUpdate(filters: IFilters) {
		this.setState({
			filters,
		});
	}

	protected handleFiltersReset() {
		this.setState({
			selectedFilters: {
				filters: [],
			},
			filters: this.generateFilters(),
		});
	}

	protected handleFiltersToggle() {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.state.filters),
		);

		filters.filterToggle = !filters.filterToggle;

		if (filters.filterToggle) {
			if (this.props.userAuthenticated) {
				const cancerTypesIndex = filters.filterGroups.findIndex(
					value => {
						return value.paramName === 'situations';
					},
				);
				if (cancerTypesIndex >= 0) {
					filters.filterGroups.splice(cancerTypesIndex, 1);
				}

				// Remove the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					filters.filterGroups[
						topicsIndex
					].filters = filters.filterGroups[
						topicsIndex
					].filters.filter(filterValue => {
						const result = this.props.topics.find(topic => {
							return (
								topic.category === 'discussions' &&
								topic.slug === filterValue.value &&
								(topic.is_user_topic ||
									isAdmin(this.props.userData))
							);
						});
						return !!result;
					});
				}
			}
		} else {
			if (this.props.userAuthenticated) {
				// Add the situations at the beginning
				const situationsGroup: IFiltersGroup = {
					label: null,
					noBorder: true,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: !!this.props.userData.situations.find(
									userSituation => {
										return (
											userSituation.name ===
											situation.name
										);
									},
								),
							};
						}),
				};
				filters.filterGroups.unshift(situationsGroup);

				// add the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					this.props.topics.forEach(topic => {
						if (
							!topic.is_user_topic &&
							!isAdmin(this.props.userData) &&
							topic.category === 'discussions'
						) {
							filters.filterGroups[topicsIndex].filters.push({
								label: topic.title,
								value: topic.slug,
								active: false,
							});
						}
					});
				}
			}
		}

		this.setState({
			filters,
		});
	}

	protected generateFilters(): IFilters {
		const filters: IFilters = {
			filtersTitle: 'Filter',
			withToggle: this.props.userAuthenticated,
			filterToggleLabel:
				'Only show events directly related to my cancer experience',
			filterToggle: true,
			filterGroups: [],
		};

		if (!this.props.situationsLoading && !this.props.topicsLoading) {
			// Add the location filter
			filters.filterGroups.push({
				label: 'In the following locations',
				paramName: 'state',
				singleValue: false,
				filters: [
					{
						label: 'All of Australia',
						value: 'all-of-australia',
						active: false,
					},
					{
						label: 'NSW',
						value: 'nsw',
						active: false,
					},
					{
						label: 'VIC',
						value: 'vic',
						active: false,
					},
					{
						label: 'QLD',
						value: 'qld',
						active: false,
					},
					{
						label: 'North QLD',
						value: 'qld-north',
						active: false,
					},
					{
						label: 'ACT',
						value: 'act',
						active: false,
					},
					{
						label: 'TAS',
						value: 'tas',
						active: false,
					},
					{
						label: 'SA',
						value: 'sa',
						active: false,
					},
					{
						label: 'NT',
						value: 'nt',
						active: false,
					},
					{
						label: 'WA',
						value: 'wa',
						active: false,
					},
					{
						label: 'New Zealand',
						value: 'nz',
						active: false,
					},
					{
						label: 'Online',
						value: 'online',
						active: false,
					},
				],
			});

			// Add the duration filter
			filters.filterGroups.push({
				label: 'Duration',
				paramName: 'duration',
				singleValue: false,
				filters: [
					{
						label: 'Overnight',
						value: 'overnight',
						active: false,
					},
					{
						label: 'Weekend',
						value: 'weekend',
						active: false,
					},
				],
			});

			// Add the time filter
			filters.filterGroups.push({
				label: 'By time',
				paramName: 'time',
				singleValue: false,
				filters: [
					{
						label: 'Daytime',
						value: 'daytime',
						active: false,
					},
					{
						label: 'Evening',
						value: 'evening',
						active: false,
					},
				],
			});
		}

		return filters;
	}

	protected trackEventTileOpen() {
		this.GTM.pushEventToDataLayer({
			event: 'viewEvent',
		});
	}

	protected trackFilterUpdate(value: string) {
		this.GTM.pushEventToDataLayer({
			event: 'filterEvents',
			filter: value,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { situations } = state.situations;
	const { topics } = state.topics;
	const { updateDataCount } = state.updateData;

	return {
		userAuthenticated,
		userData,
		situations,
		situationsLoading: state.situations.loading,
		topics,
		topicsLoading: state.topics.loading,
		updateDataCount,
	};
}

export default connect(mapStateToProps)(withRouter(Events as any));
