import React, { MouseEvent } from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import 'moment-timezone';
import { connect } from 'react-redux';

import {
	IStoreState,
	registerEventInterestModalDisplay,
} from '../../redux/store';
import { IApiEvent, IApiUser } from '../../interfaces';

import { withServices } from '../../services';

import { getExcerpt } from '../../helpers/getExcerpt';
import { formatEventDate } from '../../helpers/formatEventDate';

import PlayArrowIcon from '@material-ui/icons/PlayArrow';

import Router from 'next/router';
import EventErrorModal from './EventErrorModal';
import EventPromoModal from './EventPromoModal';

const typeArr: string[] = ['workshop', 'activity', 'group-activity', 'drop-in'];

interface IProps {
	event: IApiEvent;
	onRowClick?(data: any): void;
	userAuthenticated: boolean | null;
	index: number;
	userData: IApiUser;
	ausZoneArr: string[];
	nzZoneArr: string[];
	dispatch(action: any): void;
}
interface IState {
	showPromo: boolean;
	showPeopleModal: boolean;
	value: 'interested' | 'going';
	showErrorModal: boolean;
	errorMessage: string;
}

class EventTile extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);
		this.state = {
			showPromo: false,
			showPeopleModal: false,
			value: 'interested',
			showErrorModal: false,
			errorMessage: '',
		};
		this.toggleModel = this.toggleModel.bind(this);
		this.setValue = this.setValue.bind(this);
		this.togglePeopleModel = this.togglePeopleModel.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.isRegisterDisabled = this.isRegisterDisabled.bind(this);
		this.toggleErrorModal = this.toggleErrorModal.bind(this);
	}

	protected toggleModel = (e: MouseEvent) => {
		e.stopPropagation();
		this.setState({ showPromo: !this.state.showPromo });
	};

	protected togglePeopleModel = (e: any) => {
		e.stopPropagation();
		const id = e.target.id;
		if (id === 'going' || id === 'interested') {
			this.setState({
				showPeopleModal: !this.state.showPeopleModal,
				value: id,
			});
			return;
		}
		this.setState({ showPeopleModal: !this.state.showPeopleModal });
	};

	protected setValue = value => {
		this.setState({ value });
	};

	public render() {
		const { event, onRowClick, index } = this.props;
		let { event_type = 'workshop' } = event;
		if (event_type === '' || event_type === null) {
			event_type = 'workshop';
		}
		const eventType = typeArr[index % 4];
		const { ausZoneArr, nzZoneArr } = this.props;
		const titleExcerpt = getExcerpt(this.props.event.title, 6);

		const foundInAus = ausZoneArr.findIndex(
			t => t === event.event_time_zone,
		);
		const foundInNz = nzZoneArr.findIndex(t => t === event.event_time_zone);
		let timeZone = event.event_time_zone;
		if (foundInAus < 0 && foundInNz < 0) {
			timeZone = 'Australia/Sydney';
		}
		const offSet = moment.tz(timeZone).utcOffset();
		const currentTimeZone = moment.tz.guess(true);
		const myOffset = moment.tz(currentTimeZone).utcOffset();
		const diff = myOffset - offSet;

		let startDate = moment(this.props.event.start_date.date).add(
			diff,
			'minute',
		);
		let endDate = moment(this.props.event.end_date.date).add(
			diff,
			'minute',
		);

		const startTime = moment(startDate).format('hh:mm a');
		const endTime = moment(endDate).format('hh:mm a');
		const eventDateStartDay = moment(startDate).format('D');
		const eventDateStartMonth = moment(startDate).format('MMM');
		const formattedDate = formatEventDate(startDate, endDate);
		const eventFeaturedImageUrl = this.props.event.feature_image
			? this.props.event.feature_image.url
			: '';
		const registerButtonClass = 'event_tile-join-button';
		const { showErrorModal, errorMessage } = this.state;
		return (
			<div
				className="event_tile theme--main"
				onClick={() => {
					if (onRowClick) {
						onRowClick(event);
					} else {
						const id = event.slug.trim();
						Router.push(
							{
								pathname: '/events-single',
								query: { id, eventType },
							},
							`/events/${id}`,
						);
					}
				}}
			>
				<div
					className="event_tile-thumbnail"
					style={{ backgroundImage: `url(${eventFeaturedImageUrl})` }}
				>
					{event.promo_video_url && (
						<div
							className="event_tile-buttons"
							onClick={this.toggleModel}
						>
							<PlayArrowIcon className="event_single-sidebar-promo-icon" />
							<span className=""> Promo</span>
						</div>
					)}
				</div>

				<div className="event_tile-content">
					<div className="event_tile-content_wrapper">
						<div className="event_tile-content_information">
							<div className="event_tile-content-info">
								<div className="event_tile-content-header">
									<div
										className={`event_tile-thumbnail-date type-${eventType}`}
									>
										<div>
											<span className="event_tile-thumbnail-date-month">
												{' '}
												{eventDateStartMonth}
											</span>
										</div>
										<div>
											<span className="event_tile-thumbnail-date-day">
												{eventDateStartDay}
											</span>
										</div>
									</div>
									<div
										style={{ padding: '1rem 0.5rem 0rem' }}
									>
										<h1 className="event_tile-content-title theme-title font--h5">
											{titleExcerpt}
										</h1>

										<div className="event_tile-going-container">
											<div>
												<span
													className={`span-${eventType}`}
												>
													{event_type}
												</span>
											</div>
											<div className="verticle_line"></div>
											<div className="dot-conatiner">
												<div className="dark-dot" />
												<div className="dark-dot-medium" />
												<div className="dark-dot-light" />
											</div>
											<div>
												<span className="going-span">
													+
													{
														event.registered_users_count
													}{' '}
													Going
												</span>
											</div>
										</div>
									</div>
								</div>

								<div
									className={`event_tile-date-and-time icon-${eventType}`}
								>
									<div className="event_tile-date-and-time-icon">
										<ReactSVG
											className="event_tile-content-info-icon"
											path={`${process.env.STATIC_PATH}/icons/events.svg`}
										/>
									</div>
									<div className="event_tile-date-and-time-detail">
										<div>
											<p className="event_tile-content-info-detail">
												{formattedDate}
											</p>
											<p className="event_tile-content-info-detail">
												{startTime} {' - '} {endTime}
											</p>
										</div>
									</div>
								</div>

								<div
									className={`event_tile-date-and-time icon-${eventType}`}
								>
									<div className="event_tile-date-and-time-icon">
										<ReactSVG
											className={`event_tile-content-info-icon`}
											path={`${process.env.STATIC_PATH}/icons/location.svg`}
										/>
									</div>
									<div className="event_tile-date-and-time-detail">
										{this.props.event.state.slug ===
										'online' ? (
											<span className="event_tile-content-info-detail event_tile-location-span">
												Online
											</span>
										) : (
											this.props.event.location && (
												<span className="event_tile-content-info-detail event_tile-location-span event_register-button">
													<a
														onClick={e =>
															e.stopPropagation()
														}
														data-latitude={
															this.props.event
																.latitude
														}
														data-longitude={
															this.props.event
																.longitude
														}
														title="View map"
														target="_blank"
														className=""
														href={`https://google.com/maps/place/${this.props.event.latitude},${this.props.event.longitude}`}
													>
														{
															this.props.event
																.location
														}
													</a>
												</span>
											)
										)}
									</div>
									{!onRowClick && (
										<div
											onClick={this.handleClick}
											className={registerButtonClass}
										>
											<div>+</div>
										</div>
									)}
								</div>

								<br />
							</div>
						</div>
					</div>
				</div>

				{this.state.showPromo && (
					<EventPromoModal
						toggleModel={this.toggleModel}
						event={event}
					/>
				)}
				{showErrorModal && (
					<EventErrorModal
						message={errorMessage}
						handleClose={this.toggleErrorModal}
						dispatch={this.props.dispatch}
					/>
				)}
			</div>
		);
	}
	protected isRegisterDisabled = () => {
		const { userData } = this.props;
		let { age, dob } = userData;
		if (!age) {
			age = moment().diff(dob, 'years', false);
		}
		const { event } = this.props;
		let {
			max_age,
			min_age,
			max_participents,
			registered,
			registered_users_count,
			last_joining_date,
			start_date,
			event_time_zone = 'Australia/Sydney',
		} = event;
		if (registered) {
			this.setState({
				showErrorModal: true,
				errorMessage: 'You have already registered for this event',
			});
			// this.props.dispatch(
			// 	apiError(['You have already registered for this event']),
			// );
			return true;
		}
		if (!last_joining_date) {
			last_joining_date = start_date;
		}
		const offSet = moment.tz(event_time_zone).utcOffset();
		const currentTimeZone = moment.tz.guess(true);
		const myOffset = moment.tz(currentTimeZone).utcOffset();
		const diff = myOffset - offSet;

		const now = moment();
		let lastDate = moment(last_joining_date.date).tz(event.event_time_zone);
		lastDate = moment(lastDate).add(diff, 'minute');
		if (now > lastDate) {
			this.setState({
				showErrorModal: true,
				errorMessage: 'Registration date over',
			});
			// sthis.props.dispatch(apiError(['Registration date over']));
			return true;
		}
		if (!max_participents) {
			max_participents = 9999999999;
		}
		if (!max_age) {
			max_age = 999;
		}
		if (!min_age) {
			min_age = -1;
		}
		if (age > max_age) {
			this.setState({
				showErrorModal: true,
				errorMessage: `maximum age limit for this event is ${max_age}`,
			});

			// this.props.dispatch(
			// 	apiError([`maximum age limit for this event is ${max_age}`]),
			// );
			return true;
		}
		if (age < min_age) {
			this.setState({
				showErrorModal: true,
				errorMessage: `minimum age limit for this event is ${min_age}`,
			});

			// this.props.dispatch(
			// 	apiError([`minimum age limit for this event is ${min_age}`]),
			// );
			return true;
		}

		if (max_participents === registered_users_count) {
			this.setState({
				showErrorModal: true,
				errorMessage:
					'Maximum participant limit reached ' + max_participents,
			});

			// this.props.dispatch(
			// 	apiError([
			// 		'Maximum participant limit reached ' + max_participents,
			// 	]),
			// );
			return true;
		}

		return false;
	};
	protected handleClick(e) {
		e.stopPropagation();

		if (this.props.userAuthenticated) {
			const disableButtonResult = this.isRegisterDisabled();
			if (disableButtonResult) {
				return;
			}
			const eventSlug = this.props.event.slug;
			const formattedDate = formatEventDate(
				this.props.event.start_date.date,
				this.props.event.end_date.date,
			);
			const eventDate = formattedDate;
			const eventLocation = this.props.event.location;
			this.props.dispatch(
				registerEventInterestModalDisplay(
					eventSlug,
					eventDate,
					eventLocation,
				),
			);
		} else {
			const href = `/auth/login?redirect=/events/${this.props.event.slug}`;
			Router.push(href);
		}
	}
	protected toggleErrorModal = e => {
		e.stopPropagation();
		this.setState({ showErrorModal: false, errorMessage: '' });
	};
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(EventTile));
