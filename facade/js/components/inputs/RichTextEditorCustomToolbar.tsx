import React from 'react';
import ReactSVG from 'react-svg';

import { GiphySelection } from '../../components/general/Giphy';

export interface IToolbarConfig {
	formatHeading: boolean;
	formatText: boolean;
	formatList: boolean;
	formatVideo: boolean;
	formatImage: boolean;
	formatGif: boolean;
	formatClean: boolean;
}

interface IProps {
	toolbarId: string;
	config: IToolbarConfig;
	isGifActive: boolean;
	onGifSelection(value: any): void; //TODO: update param interface
}

const RichTextEditorCustomToolbar: React.StatelessComponent<IProps> = props => {
	return (
		<React.Fragment>
			<div id={props.toolbarId}>
				{props.config.formatHeading && (
					<span className="ql-formats">
						<select
							className="ql-header"
							defaultValue={''}
							onChange={e => e.persist()}
						>
							<option value="1" />
							<option value="2" />
						</select>
					</span>
				)}

				{props.config.formatText && (
					<span className="ql-formats">
						<button className="ql-bold" />
						<button className="ql-italic" />
						<button className="ql-strike" />
					</span>
				)}

				{props.config.formatList && (
					<span className="ql-formats">
						<button className="ql-list" value="ordered" />
						<button className="ql-list" value="bullet" />
					</span>
				)}

				<span className="ql-formats">
					<button className="ql-link" />
					{props.config.formatImage && (
						<button className="ql-image" />
					)}

					{props.config.formatVideo && (
						<button className="ql-video" />
					)}

					{props.config.formatGif && (
						<div className="ql-gifContainer">
							{props.isGifActive && (
								<GiphySelection
									onGifSelection={props.onGifSelection}
								/>
							)}

							<button
								id="ql-gifSearch"
								className={`ql-gifSearch ${
									props.isGifActive
										? 'ql-gifSearch--active'
										: ''
								}`}
							>
								<ReactSVG
									path={`${
										process.env.STATIC_PATH
									}/icons/gif.svg`}
								/>
							</button>
						</div>
					)}
				</span>

				{props.config.formatClean && (
					<span className="ql-formats">
						<button className="ql-clean" />
					</span>
				)}
			</div>
		</React.Fragment>
	);
};

export { RichTextEditorCustomToolbar };
