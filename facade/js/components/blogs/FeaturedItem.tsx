import React from 'react';

import { IApiBlog, IApiUser, IApiResource, IApiTopic } from '../../interfaces';

import { ButtonBaseLink } from '../general/ButtonBaseLink';
import TileAuthor, { IResourceAuthor } from '../general/TileAuthor';

interface IProps {
	blogData?: IApiBlog;
	blogAuthorDetails?: IApiUser;
	resourceData?: IApiResource;
	resourceAuthorDetails?: IResourceAuthor;
}

interface IFeaturedItemButton {
	area: 'blogs' | 'resources';
	slug: string;
	title: string;
}

const FeaturedItemButton: React.StatelessComponent<
	IFeaturedItemButton
> = props => {
	return (
		<ButtonBaseLink
			className="featured_item-button"
			hrefAs={`${props.area}/${props.slug}`}
			href={`${props.area}-single?id=${props.slug}`}
			title={props.title}
		/>
	);
};

interface IFeaturedItemImage {
	imageUrl: string;
}

const FeaturedItemImage: React.StatelessComponent<
	IFeaturedItemImage
> = props => {
	return (
		<div
			className="featured_item-image"
			style={{
				background: `url(${props.imageUrl}) no-repeat center center`,
				backgroundSize: 'cover',
			}}
		/>
	);
};

interface IFeaturedItemMeta {
	topic: IApiTopic;
	title: string;
	hrefPrefix: string;
}

const FeaturedItemMeta: React.StatelessComponent<IFeaturedItemMeta> = props => {
	return (
		<div className="featured_item-meta">
			{props.topic && (
				<div className="general_tile-topic hm-b8">
					<ButtonBaseLink
						className="general_tile-topic_link"
						href={`/${props.hrefPrefix}?topic=${props.topic.slug}`}
						title={props.topic.title}
						disableRipple={true}
					>
						{props.topic.title}
					</ButtonBaseLink>
				</div>
			)}

			<h3 className="featured_item-title">{props.title}</h3>
		</div>
	);
};

const FeaturedItem: React.StatelessComponent<IProps> = (props: IProps) => {
	let featuredItemButton: React.ReactElement;
	let featuredItemImage: React.ReactElement;
	let featuredItemMeta: React.ReactElement;

	if (!!props.blogData) {
		featuredItemButton = (
			<FeaturedItemButton
				area="blogs"
				slug={props.blogData.slug}
				title={props.blogData.slug}
			/>
		);

		featuredItemImage = props.blogData.feature_image ? (
			<FeaturedItemImage imageUrl={props.blogData.feature_image.url} />
		) : null;

		featuredItemMeta = (
			<FeaturedItemMeta
				title={props.blogData.title}
				topic={props.blogData.topic}
				hrefPrefix="blogs"
			/>
		);
	} else if (!!props.resourceData) {
		featuredItemButton = (
			<FeaturedItemButton
				area="resources"
				slug={props.resourceData.slug}
				title={props.resourceData.slug}
			/>
		);

		featuredItemImage = props.resourceData.feature_image ? (
			<FeaturedItemImage
				imageUrl={props.resourceData.feature_image.url}
			/>
		) : null;

		featuredItemMeta = (
			<FeaturedItemMeta
				title={props.resourceData.title}
				topic={props.resourceData.topic}
				hrefPrefix="resources"
			/>
		);
	}

	return (
		<div className="featured_item">
			{featuredItemButton}

			{featuredItemImage}

			{featuredItemMeta}

			{!!props.blogData && (
				<div className="featured_item-author">
					<TileAuthor
						blogData={props.blogData}
						userData={props.blogData.user}
					/>
				</div>
			)}

			{!!props.resourceData && (
				<div className="featured_item-author">
					<TileAuthor resourceData={props.resourceAuthorDetails} />
				</div>
			)}
		</div>
	);
};

export default FeaturedItem;
