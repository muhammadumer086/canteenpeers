@extends('layouts.email')

@section('plain_text')
	You are receiving this email because we received a password reset request for your account on {{ config('app.name') }}.
@endsection

@section('content')
	@component('emails.components.paragraph')
		You are receiving this email because we received a password reset request for your account on {{ config('app.name') }}.
	@endcomponent

	@component('emails.components.button', ['url' => $facadeUrl])
		Reset Password
	@endcomponent

	@component('emails.components.paragraph')
		If you did not request a password reset, no further action is required.
	@endcomponent
@endsection
