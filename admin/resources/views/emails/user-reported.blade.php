@extends('layouts.email')

@section('plain_text')
	Hi,

	Community member {{ $author->full_name }} ({{$author->email}}) reported <b>{{$reportedUser->full_name}} ({{$reportedUser->email}})</b>.
@endsection

@section('content')
	Hi,

	@component('emails.components.paragraph')
		Community member {{ $author->full_name }} ({{$author->email}}) reported <b>{{$reportedUser->full_name}} ({{$reportedUser->email}})</b> for the following reason:
	@endcomponent

	@component('emails.components.paragraph')
		<b>{{$reportReason}}</b>
	@endcomponent

	@if ($reportInformation && !empty($reportInformation))
		@component('emails.components.paragraph')
			The following additional information has been provided:
		@endcomponent

		@component('emails.components.paragraph')
			<b>{{ $reportInformation }}</b>
		@endcomponent
	@endif

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
