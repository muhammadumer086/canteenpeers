import React from 'react';

import { ApiService, Error400, Error403, Error404 } from './ApiService';
import { AuthService } from './AuthService';

const isServer = typeof window === 'undefined';
const __NEXT_AUTH_SERVICE__ = '__NEXT_AUTH_SERVICE__';
const __NEXT_API_SERVICE__ = '__NEXT_API_SERVICE__';

interface IState {
	apiService: ApiService;
	authService: AuthService;
}

function getOrCreateAuthService(reduxStore, request: any = null): AuthService {
	// Always make a new store if server, otherwise state is shared between requests
	if (isServer) {
		return new AuthService(reduxStore, request);
	}

	// Create store if unavailable on the client and set it on the window object
	if (!window[__NEXT_AUTH_SERVICE__]) {
		window[__NEXT_AUTH_SERVICE__] = new AuthService(reduxStore, request);
	}
	return window[__NEXT_AUTH_SERVICE__];
}

function getOrCreateApiService(reduxStore, request: any = null): ApiService {
	// Always make a new store if server, otherwise state is shared between requests
	if (isServer) {
		return new ApiService(reduxStore, request);
	}

	// Create store if unavailable on the client and set it on the window object
	if (!window[__NEXT_API_SERVICE__]) {
		window[__NEXT_API_SERVICE__] = new ApiService(reduxStore);
	}
	return window[__NEXT_API_SERVICE__];
}

export const ServicesContext = React.createContext({
	apiService: null,
	authService: null,
});

export const withAppServices = App => {
	return class AppWithServices extends React.Component<any, IState> {
		protected apiService: ApiService;
		protected authService: AuthService;

		static async getInitialProps(appContext) {
			try {
				if (!('reduxStore' in appContext.ctx)) {
					throw new Error('reduxStore required in AppWithServices');
				}

				let request = null;
				if ('req' in appContext.ctx) {
					request = appContext.ctx.req;
				}

				const authService = getOrCreateAuthService(
					appContext.ctx.reduxStore,
					request,
				);
				const apiService = getOrCreateApiService(
					appContext.ctx.reduxStore,
					request,
				);

				appContext.ctx.apiService = apiService;
				appContext.ctx.authService = authService;

				const hasAuthenticationCookie =
					authService.hasAuthenticationCookie;

				const authenticated = await authService.initialise(true);
				if (hasAuthenticationCookie && !authenticated) {
					// Logout
					if ('res' in appContext.ctx && appContext.ctx.res) {
						// Redirect to the login page
						appContext.ctx.res.writeHead(302, {
							Location: '/auth/login',
							'Set-Cookie': [
								`${AuthService.UserToken}=""; Max-Age=0`,
								`${AuthService.UserTokenExpiry}=""; Max-Age=0`,
							],
						});
						appContext.ctx.res.end();
						appContext.ctx.res.finished = true;
						return {};
					}
				}

				let appProps = {};

				if (typeof App.getInitialProps === 'function') {
					appProps = await App.getInitialProps.call(App, appContext);
				}

				(appProps as any).authService = authService;
				(appProps as any).apiService = apiService;

				return {
					...appProps,
				};
			} catch (error) {
				if (
					!(error instanceof Error400) &&
					!(error instanceof Error403) &&
					!(error instanceof Error404) &&
					'res' in appContext.ctx
				) {
					console.log(
						'Caught error in withAppServices.getInitialProps:',
						error,
					);
					appContext.ctx.res.writeHead(302, {
						Location: '/auth/login',
						'Set-Cookie': [
							`${AuthService.UserToken}=""; Max-Age=0`,
							`${AuthService.UserTokenExpiry}=""; Max-Age=0`,
						],
					});
					appContext.ctx.res.end();
					appContext.ctx.res.finished = true;
				} else {
					throw error;
				}
			}

			return {};
		}

		constructor(props) {
			super(props);

			let authService = props.authService;
			let apiService = props.apiService;
			if (!isServer) {
				authService = getOrCreateAuthService(props.reduxStore);
				apiService = getOrCreateApiService(
					props.reduxStore,
					authService,
				);
				apiService.authService = authService;
			}

			this.state = {
				authService,
				apiService,
			};
		}

		render() {
			return (
				<App
					{...this.props}
					apiService={this.state.apiService}
					authService={this.state.authService}
				/>
			);
		}
	};
};

export const withServices = Component => {
	const WithServicesComponent: React.FC<any> = props => {
		return (
			<ServicesContext.Consumer>
				{({ apiService, authService }) => (
					<Component
						{...props}
						apiService={apiService}
						authService={authService}
					/>
				)}
			</ServicesContext.Consumer>
		);
	};
	return WithServicesComponent;
};
