<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoOverwrites extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->string('seo_title', 191)->nullable();
			$table->text('seo_description')->nullable();
			$table->text('seo_image')->nullable();
		});

		Schema::table('resources', function (Blueprint $table) {
			$table->string('seo_title', 191)->nullable();
			$table->text('seo_description')->nullable();
			$table->text('seo_image')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->dropColumn(['seo_title', 'seo_description', 'seo_image']);
		});

		Schema::table('resources', function (Blueprint $table) {
			$table->dropColumn(['seo_title', 'seo_description', 'seo_image']);
		});
	}
}
