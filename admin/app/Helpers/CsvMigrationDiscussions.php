<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use League\Csv\Writer;

use App\Helpers\CsvMigration;
use App\Discussion;
use App\User;

class CsvMigrationDiscussions extends CsvMigration {
	protected $likesHugsWriter = null;
	protected $skippedDiscussions = [];

	/**
	 * The headers for the raw CSV
	 *
	 * @var array
	 */
	public $rawCsvHeaders = [
		'DISCUSSION TOPICS & TITLES',
		'Post/Topic ID',
		'Cancer Stuff',
		'My Story',
		'Canteen Research',
		'Family',
		'Fertility',
		'Friends',
		'Grief & Bereavement',
		'Life after Treatment',
		'My Cancer',
		'My Diagnosis',
		"My Parent's Cancer",
		"My Sibling's cancer",
		'My Treatment',
		'Practical Tips & Recipes',
		'Random Chat',
		'Rare Cancers',
		'Relationships',
		'Study, and Work',
		'Wellbeing, Creativity and Self Care',
	];
	/**
	 * The headers for the CSV
	 *
	 * @var array
	 */
	public $csvHeaders = [
		'id',
		'slug',
		'title',
		'content',
		'author',
		'main_discussion_id',
		'created_at',
		'topics',
	];

	public function processRecord(Writer $writer, $record) {
		$id = $record['Post/Topic ID'];
		$title = $record['DISCUSSION TOPICS & TITLES'];

		$discussionData = DB::connection('wordpress')
			->table('wp_posts')
			->where('ID', $id)
			->where('post_status', 'publish')
			->first();

		if (!$discussionData) {
			$discussionData = DB::connection('wordpress')
				->table('wp_posts')
				->where('post_title', htmlentities($title, ENT_QUOTES))
				->where('post_status', 'publish')
				->first();
		}

		if ($discussionData) {
			$result = $this->formatRecord($discussionData, $record);
			$writer->insertOne($result);

			// Add the replies
			$this->processReplies($writer, $discussionData->ID);
		} else {
			echo "\r\n\r\nSkipped {$title}\r\n\r\n";
		}
	}

	protected function processReplies(Writer $writer, $id) {
		$discussionsData = DB::connection('wordpress')
			->table('wp_posts')
			->where('post_parent', $id)
			->where('post_status', 'publish')
			->get();

		foreach ($discussionsData as $discussion) {
			$result = $this->formatRecord($discussion, null, $id);
			$writer->insertOne($result);

			$this->processReplies($writer, $discussion->ID);
		}
	}

	protected function formatRecord($data, $record, $parentId = '') {
		$this->processLikesHugs($data->ID);

		return [
			$data->ID,
			$data->post_name,
			html_entity_decode($data->post_title, ENT_QUOTES),
			$this->processContent($data->post_content),
			$data->post_author,
			$parentId,
			$data->post_date,
			$this->formatTopics($record, [
				'Cancer Stuff' => 'Cancer Stuff',
				'My Story' => 'My Story',
				'Canteen Research' => 'Canteen Research',
				'Family' => 'Family',
				'Fertility' => 'Fertility',
				'Friends' => 'Friends',
				'Grief & Bereavement' => 'Grief & Bereavement',
				'Life after Treatment' => 'Life after Treatment',
				'My Cancer' => 'My Cancer',
				'My Diagnosis' => 'My Diagnosis',
				"My Parent's Cancer" => "My Parent's Cancer",
				"My Sibling's cancer" => "My Sibling's Cancer",
				'My Treatment' => 'My Treatment',
				'Practical Tips & Recipes' => 'Practical Tips',
				'Random Chat' => 'Random Chat',
				'Rare Cancers' => 'Rare Cancers',
				'Relationships' => 'Relationships',
				'Study, and Work' => 'Study, and Work',
				'Wellbeing, Creativity and Self Care' =>
					'Wellbeing and Self Care',
				'Aotearoa New Zealand' => 'Aotearoa New Zealand',
				'LGBTQI+' => 'LGBTQI+',
			]),
		];
	}

	protected function processLikesHugs($id) {
		if (!$this->likesHugsWriter) {
			$csvPath = storage_path('migration/hugs_likes.csv');
			$this->likesHugsWriter = Writer::createFromPath($csvPath, 'w');
			$headers = ['user_id', 'discussion_id', 'type'];
			$this->likesHugsWriter->insertOne($headers);
		}

		$likesHugs = DB::connection('wordpress')
			->table('wp_like')
			->where('post_id', $id)
			->get();

		foreach ($likesHugs as $singleLikeHug) {
			$this->likesHugsWriter->insertOne([
				$singleLikeHug->user_id,
				$singleLikeHug->post_id,
				strtoupper($singleLikeHug->like_text),
			]);
		}
	}

	public function importData($record) {
		// Automatically skip some discussions
		if (
			!empty($record['main_discussion_id']) &&
			in_array($record['main_discussion_id'], $this->skippedDiscussions)
		) {
			return;
		}

		$title = $this->nullIfEmpty($record['title']);
		$slug = null;
		$parentDiscussionMigrationId = $this->nullIfEmpty(
			$record['main_discussion_id']
		);
		$parentDiscussionId = null;

		try {
			$user = User::where(
				'migration_id',
				$record['author']
			)->firstOrFail();
		} catch (\Exception $e) {
			echo "\r\n\r\nError creating discussion {$record['id']}: User not found {$record['author']}\r\n\r\n";
			$this->skippedDiscussions[] = $record['id'];
			return;
		}

		if ($parentDiscussionMigrationId) {
			try {
				$parentDiscussion = Discussion::where(
					'migration_id',
					$parentDiscussionMigrationId
				)->firstOrFail();
				$parentDiscussionId = $parentDiscussion->id;
			} catch (\Exception $e) {
				echo "\r\n\r\nError creating discussion {$record['id']}: Discussion not found {$parentDiscussionMigrationId}\r\n\r\n";
				$this->skippedDiscussions[] = $record['id'];
				return;
			}
		} else {
			$slug = $record['slug'];
		}

		try {
			$discussion = Discussion::updateOrCreate(
				[
					'migration_id' => $record['id'],
				],
				[
					'user_id' => $user->id,
					'title' => $title,
					'slug' => $slug,
					'content' => $this->processImportContent(
						$record['content']
					),
					'main_discussion_id' => $parentDiscussionId,
					'reply_to_id' => $parentDiscussionId,
					'created_at' => $record['created_at'],
				]
			);

			if (!$parentDiscussionId) {
				$this->processTopics(
					$discussion,
					$record['topics'],
					'discussions'
				);
				$this->processSituation($discussion, $user);
			}
		} catch (\Exception $e) {
			echo "\r\n\r\nError creating discussion {$record['id']}: {$e->getMessage()}\r\n\r\n";
			$this->skippedDiscussions[] = $record['id'];
			return;
		}
	}

	protected function processSituation($discussion, $user) {
		// Grab the user's situations ID
		$situationsId = [];
		foreach ($user->situations as $situation) {
			// Always get the root situation
			if ($situation->parentSituation) {
				$situationsId[] = $situation->parentSituation->id;
			} else {
				$situationsId[] = $situation->id;
			}
		}

		$discussion->situations()->sync($situationsId);
	}
}
