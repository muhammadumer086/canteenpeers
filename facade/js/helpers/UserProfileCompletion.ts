import { IApiUser } from '../interfaces';

export class UserProfileCompletion {
	protected userData: IApiUser;

	constructor(userData: IApiUser) {
		this.userData = userData;
	}

	public getUserProfileCompletion(): number {
		let total = 0;
		total += this.getUserProfileCompletionPublic();
		total += this.getUserProfileCompletionPrivate();
		total += this.getUserProfileCompletionPassword();
		total += this.getUserProfileCompletionOptional();

		return total / 4;
	}

	public getUserProfileCompletionPublic(): number {
		const fields = ['first_name', 'last_name', 'username', 'about', 'dob'];
		let partialRatio = fields.length;
		let partialTotal =
			this.getUserProfileCompletionForFields(fields) * partialRatio;

		// Additional total based on the details provided with each situation
		this.userData.situations.forEach(situation => {
			++partialRatio;
			if (situation.cancer_type) {
				++partialTotal;
			}
			++partialRatio;
			if (situation.date) {
				++partialTotal;
			}
		});

		return partialTotal / partialRatio;
	}

	public getUserProfileCompletionPrivate(): number {
		return this.getUserProfileCompletionForFields([
			'email',
			'phone',
			'gender',
			'location',
		]);
	}

	public getUserProfileCompletionPassword(): number {
		return 1;
	}

	public getUserProfileCompletionOptional(): number {
		return this.getUserProfileCompletionForFields([
			'country_of_birth',
			'indigenous_australian',
			'languages',
			'heard_abouts',
		]);
	}

	public getUserProfileCompletionForFields(fields: string[]) {
		let total = 0;
		for (const fieldName of fields) {
			if (
				fieldName in this.userData &&
				((this.userData[fieldName] instanceof Array &&
					this.userData[fieldName].length > 0) ||
					(!(this.userData[fieldName] instanceof Array) &&
						this.userData[fieldName] !== null &&
						this.userData[fieldName] !== ''))
			) {
				++total;
			}
		}

		return Math.min(1, total / fields.length);
	}
}
