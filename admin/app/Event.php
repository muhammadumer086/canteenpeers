<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

use App\User;
use App\State;
use App\Situation;
use App\EventImage;
use App\Filters\Event\EventFilters;
use App\Traits\AlgoliaIndex;
use function Functional\false;

class Event extends Model {
	use Searchable;
	use AlgoliaIndex;
	use SoftDeletes;

	protected $table = 'events';
	protected $dates = ['start_date', 'end_date', 'last_joining_date'];

	protected $casts = [
		'feature_image' => 'array',
		'overnight' => 'boolean',
		'weekend' => 'boolean',
		'daytime' => 'boolean',
		'evening' => 'boolean',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['registered_users_count'];

	/**
	 * Get the index name for the model.
	 *
	 * @return string
	 */
	public function searchableAs() {
		return $this->algoliaIndex();
	}

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		$id = $this->slug;
		$image = null;
		$title = $this->title;
		$excerpt = str_limit(strip_tags($this->description), 250);
		$data = str_limit(strip_tags($this->description), 5000);

		if (isset($this->image) && isset($this->image->image_url)) {
			$image = $this->image->image_url;
		}

		// Result structure
		$result = [
			'type' => $type,
			'id' => $id,
			'image' => $image,
			'title' => $title,
			'excerpt' => $excerpt,
			'data' => $data,
		];

		$result['location_data'] = json_decode($this->location_data);

		return $result;
	}

	/**
	 * Get the value used to index the model.
	 *
	 * @return mixed
	 */
	public function getScoutKey() {
		return $this->algolia_id;
	}

	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName() {
		return 'slug';
	}

	public function situations() {
		return $this->belongsToMany(Situation::class);
	}

	public function state() {
		return $this->belongsTo(State::class);
	}

	public function users() {
		return $this->belongsToMany(User::class);
	}

	public function hasUser($user) {
		return $this->users->contains($user);
	}

	public function image() {
		return $this->hasMany(EventImage::class);
	}

	public function scopeFilter(
		Builder $builder,
		Request $request,
		array $filters = []
	) {
		return (new EventFilters($request))->add($filters)->filter($builder);
	}

	public function getRegisteredUsersCountAttribute() {
		return $this->users()->count();
	}

	public function getRegisteredUsersCountByOrigin($origin) {
		return $this->users()
			->where('event_user.registration_origin', $origin)
			->count();
	}

	public function getAlgoliaIdAttribute() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		return "$type/{$this->slug}";
	}
}
