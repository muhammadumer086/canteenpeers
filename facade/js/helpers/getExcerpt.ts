import * as remark from 'remark';
import * as strip from 'strip-markdown';
import * as he from 'he';

import stripHtmlTags from './stripHtmlTags';

export function getExcerpt(str: string, wordsLimit: number = 50) {
	const averageEnglishWordLength = 5.1;
	const sizeLimit = Math.floor(wordsLimit * averageEnglishWordLength);

	// Remove any HTML markup
	str = stripHtmlTags(str);
	// Replace any HTML entity
	str = he.decode(str);
	// Remove any markdown
	remark()
		.use(strip)
		.process(str, (err, data) => {
			if (err) {
				console.warn(err);
			} else {
				str = String(data);
			}
		});

	if (str.length < sizeLimit) {
		return str;
	}

	const explodedStr = str.split(' ').slice(0, wordsLimit);
	let excerptSize = 0;
	const explodedExcerpt = [];
	for (const subStr of explodedStr) {
		if (excerptSize === 0) {
			explodedExcerpt.push(subStr);
			excerptSize += subStr.length;
		} else {
			if (excerptSize + subStr.length + 1 <= sizeLimit) {
				explodedExcerpt.push(subStr);
				excerptSize += subStr.length + 1;
			}
		}
	}

	const excerpt = explodedExcerpt.join(' ') + '...';
	return excerpt;
}
