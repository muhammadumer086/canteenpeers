import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { IApiUser } from '../../interfaces';

export const newMessageDisplay = (userMessageData: IApiUser) => (dispatch, getState: () => IStoreState) => {
	if (getState().newMessageModal.newMessageModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.NEW_MESSAGE_MODAL_DISPLAY,
		userMessageData
	});
}

export const newMessageHide = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().newMessageModal.newMessageModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.NEW_MESSAGE_MODAL_HIDE,
	});
}
