import React from 'react';

interface IProps {
	count: number;
}

const Count: React.FC<IProps> = (props: IProps) => {
	let countString: string = getCountToString(props.count);

	if (props.count >= 1000) {
		countString = getCountToString(props.count / 1000, 'k');
	}

	if (props.count >= 1000000) {
		countString = getCountToString(props.count / 1000000, 'm');
	}

	return (
		<span className="count">{countString}</span>
	);

	function getCountToString(count: number, append: string = '') {
		return Math.floor(count).toString() + append;
	}
}

export default Count;
