@extends('layouts.email')

@section('plain_text')
	@component('emails.components.paragraph')
		Type of Submission: {{$type}}
	@endcomponent
	@component('emails.components.paragraph')
		From: {{$email}}
	@endcomponent
	@component('emails.components.paragraph')
		Message: {{$content}}
	@endcomponent
@endsection

@section('content')
	@component('emails.components.paragraph')
		<b>Type of Submission:</b><br> {{$type}}
	@endcomponent
	@component('emails.components.paragraph')
		<b>From:</b><br>  <a href="mailto:{{$email}}">{{$email}}</a>
	@endcomponent
	@component('emails.components.paragraph')
		<b>Message:</b><br>  {{$content}}
	@endcomponent
@endsection
