declare const google: any;
declare const FB: any;
declare const Intercom: any;
declare const dataLayer: any;
declare module 'isomorphic-unfetch';
declare module 'react-jss';
declare module 'react-jss/lib/JssProvider';
declare module 'build-url';
declare module 'es6-object-assign';
declare module 'smoothscroll-polyfill';
declare module 'quill-image-drop-module';
declare module 'quill-plugin-image-upload';
