<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Situation as SituationResource;

class PersonalSetting extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'personal_settings',
				$this->id,
				$this->updated_at,
				true,
				false
			)
		) {
			return $apiStore->getApiResource(
				'personal_settings',
				$this->id,
				$this->updated_at,
				true,
				false
			);
		}

		$params = ['slug', 'name', 'category_slug', 'category'];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$apiStore->setApiResource(
			$result,
			'personal_settings',
			$this->id,
			$this->updated_at,
			true,
			false
		);

		return $result;
	}
}
