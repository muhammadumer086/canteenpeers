import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import { ApiService, withServices } from '../../services';

import { IApiDiscussion } from '../../interfaces';

import { IStoreState } from '../../redux/store';

import DiscussionTile from './DiscussionTile';
import VirtualisedList, { TOrder } from '../general/VirtualisedList';
import { Select, MenuItem } from '@material-ui/core';
import { CircularProgress } from "@material-ui/core";
interface IProps {
	mainDiscussion: IApiDiscussion | null;
	apiService: ApiService;
	scrollingElement: HTMLElement;
	scrollToIndex: number;
	showIsReply: boolean;
	order: TOrder;
	openReply?(event, replyTo?): void;
	editDiscussion(discussion: IApiDiscussion): void;
	deleteDiscussion(discussion: IApiDiscussion | null): void;
	handleOrderChange(order: TOrder): void;
	dispatch(action: any): void;
}

interface IState { }

class DiscussionsList extends React.Component<IProps, IState> {
	protected scrollElement: HTMLElement = null;

	constructor(props: IProps) {
		super(props);

		this.handleRenderCell = this.handleRenderCell.bind(this);
	}

	public render() {
		if (typeof window === 'undefined') {
			return null;
		}
		return (
			<Fragment>
				<div className="discussion_single-order_container">
					<Select
						value={this.props.order}
						onChange={evt => {
							this.props.handleOrderChange(evt.target
								.value as any);
						}}
						className="discussion_single-order"
					>
						<MenuItem value="DESC">Newest to Oldest</MenuItem>
						<MenuItem value="ASC">Oldest to Newest</MenuItem>
					</Select>
				</div>
				<VirtualisedList
					endpoint={`/api/discussions/${this.props.mainDiscussion.id
						}/replies`}
					endpointExpectedParam="discussions"
					defaultCellHeight={200}
					totalItems={this.props.mainDiscussion.direct_replies_count}
					scrollingElement={this.props.scrollingElement}
					renderCell={this.handleRenderCell}
					scrollToIndex={this.props.scrollToIndex}
					order={this.props.order}
				/>
			</Fragment>
		);
	}

	protected handleRenderCell(data: IApiDiscussion | null) {
		return (
			<div className="discussions_list-item">
				<div className="discussion_single-reply">
					{data && (
						<DiscussionTile
							noActions={true}
							noLink={true}
							hideReplyCount={true}
							discussion={data}
							withReplyAction={true}
							multiline={true}
							order={this.props.order}
							openReply={this.props.openReply}
							editDiscussion={this.props.editDiscussion}
							deleteDiscussion={this.props.deleteDiscussion}
						/>
					)}
					{!data && <div className="discussions_list-item-empty" ><CircularProgress /></div>}
				</div>
			</div>
		);
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(withServices(DiscussionsList));
