<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('topics', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->string('category')->default('discussions');
			$table->timestamps();

			$table->index('slug');
			$table->index('category');
		});

		Schema::create('situation_topic', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('situation_id');
			$table->unsignedInteger('topic_id');

			$table->foreign('situation_id')
			->references('id')->on('situations')
			->onDelete('cascade');

			$table->foreign('topic_id')
			->references('id')->on('topics')
			->onDelete('cascade');
		});

		Schema::create('discussions', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->string('title')->nullable();
			$table->mediumText('content');
			$table->unsignedInteger('main_discussion_id')->nullable();
			$table->unsignedInteger('reply_to_id')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');

			$table->foreign('main_discussion_id')
			->references('id')->on('discussions')
			->onDelete('cascade');

			$table->foreign('reply_to_id')
			->references('id')->on('discussions')
			->onDelete('cascade');
		});

		Schema::create('discussion_situation_topic', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('discussion_id');
			$table->unsignedInteger('situation_topic_id');

			$table->foreign('discussion_id')
			->references('id')->on('discussions')
			->onDelete('cascade');

			$table->foreign('situation_topic_id')
			->references('id')->on('situation_topic')
			->onDelete('cascade');
		});

		Schema::create('likes', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('discussion_id');
			$table->timestamps();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');

			$table->foreign('discussion_id')
			->references('id')->on('discussions')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('likes');
		Schema::dropIfExists('discussion_situation_topic');
		Schema::dropIfExists('discussions');
		Schema::dropIfExists('situation_topic');
		Schema::dropIfExists('topics');
	}
}
