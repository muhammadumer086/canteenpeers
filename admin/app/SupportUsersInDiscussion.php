<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Discussion;
use App\User;

class SupportUsersInDiscussion extends Model {
	public $table = 'support_users_in_discussion';

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'reply' => 'boolean',
		'hug' => 'boolean',
		'like' => 'boolean',
	];

	public function discussion() {
		return $this->belongsTo(Discussion::class)->withTrashed();
	}

	public function user() {
		return $this->belongsTo(User::class)->withTrashed();
	}
}
