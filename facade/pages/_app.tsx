import App from 'next/app';
// import { Container } from 'next/app';
import Head from 'next/head';
import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import { Provider } from 'react-redux';

// Polyfills
import '@babel/polyfill';
import * as assignPolyfill from 'es6-object-assign';
import smoothscroll from 'smoothscroll-polyfill';

import withReduxStore from '../js/redux/withReduxStore';
import getPageContext from '../js/helpers/getPageContext';
//import { frankMark } from '../js/helpers/frankMark';

import { withAppServices, ServicesContext } from '../js/services';

import { ApiErrorBanner, ApiSuccessBanner } from '../js/components/api';

import AppLoader from '../js/components/general/AppLoader';
import DataLoader from '../js/components/general/DataLoader';
import PageLoader from '../js/components/general/PageLoader';
import DynamicComponents from '../js/components/general/DynamicComponents';
import { GTMDataLayer } from '../js/helpers/dataLayer';
import '../styles/styles.scss';
class MyApp extends App {
	protected pageContext = null;
	public props: any;

	static async getInitialProps({ Component, ctx }) {
		let pageProps = {};

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx);
		}

		return {
			pageProps,
		};
	}

	constructor(props) {
		super(props);

		if (typeof window !== 'undefined') {
			// Polyfills for IE11
			assignPolyfill.polyfill();
			smoothscroll.polyfill();
		}

		this.pageContext = getPageContext();

		//frankMark();
		this.initFacebook();
	}

	componentDidMount() {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side');
		if (jssStyles && jssStyles.parentNode) {
			jssStyles.parentNode.removeChild(jssStyles);
		}
		this.initLoginStatus();
	}

	public componentDidCatch(error) {
		console.log(`componentDidCatch`, error);
	}

	render() {
		const {
			Component,
			pageProps,
			reduxStore,
			apiService,
			authService,
		} = this.props as any;

		const services = {
			apiService,
			authService,
		};

		return (
			<div>
				<Head>
					<meta
						name="viewport"
						content={
							'user-scalable=0, initial-scale=1, ' +
							'minimum-scale=1, width=device-width, height=device-height'
						}
					/>
				</Head>
				<Provider store={reduxStore}>
					<ServicesContext.Provider value={services}>
						<JssProvider
							registry={
								this.pageContext
									? this.pageContext.sheetsRegistry
									: undefined
							}
							generateClassName={
								this.pageContext.generateClassName
							}
						>
							{/* MuiThemeProvider makes the theme available down the React
							tree thanks to React context. */}
							<MuiThemeProvider
								theme={this.pageContext.theme}
								sheetsManager={this.pageContext.sheetsManager}
							>
								{/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
								<CssBaseline />
								{/* Handles the pre-loading of the app */}
								<AppLoader />
								{/* Handles any API error */}
								<ApiErrorBanner />
								{/* Handles API success messages */}
								<ApiSuccessBanner />
								{/* Display a page loader */}
								<PageLoader authService={authService} />
								{/* Load data in the background */}
								<DataLoader />
								{/* Pass pageContext to the _document though the renderPage enhancer
								to render collected styles on server side. */}
								<Component
									pageContext={this.pageContext}
									apiService={apiService}
									authService={authService}
									{...pageProps}
								/>
								<DynamicComponents />
							</MuiThemeProvider>
						</JssProvider>
					</ServicesContext.Provider>
				</Provider>
			</div>
		);
	}

	protected initFacebook() {
		if (typeof window !== 'undefined') {
			(window as any).fbAsyncInit = function() {
				FB.init({
					appId: process.env.FB_APP_ID,
					cookie: true,
					status: true,
					xfbml: true,
					version: 'v2.5',
				});
			};
			(function(d, s, id) {
				var js,
					fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {
					return;
				}
				js = d.createElement(s);
				js.id = id;
				js.src = '//connect.facebook.net/en_US/sdk.js';
				fjs.parentNode.insertBefore(js, fjs);
			})(document, 'script', 'facebook-jssdk');
		}
	}

	protected initLoginStatus() {
		if (typeof window !== 'undefined') {
			if (!this.props.authService.initialising) {
				// Trigger GTM event
				const GTM = new GTMDataLayer();
				if (this.props.authService.userData) {
					GTM.pushEventToDataLayer({
						event: 'userLogin',
						login: 'Yes',
						userId: this.props.authService.userData.id,
					});
				} else {
					GTM.pushEventToDataLayer({
						event: 'userLogout',
						login: 'No',
					});
				}
			} else {
				const self = this;
				setTimeout(() => {
					self.initLoginStatus();
				}, 200);
			}
		}
	}
}

export default withReduxStore(withAppServices(MyApp));
