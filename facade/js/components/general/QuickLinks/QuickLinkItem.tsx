import React from 'react';
import { connect } from 'react-redux';
import { Avatar, ButtonBase } from '@material-ui/core';

import { dissectUrl } from './../../../helpers/dissectUrl';
import { withServices } from '../../../../js/services';

import { IApiLink, EStaticLinkType } from '../../../../js/interfaces';
import { ButtonBaseLink } from './../../general/ButtonBaseLink';
import {
	newBlogModalDisplay,
	IStoreState,
	discussionModalDisplay,
} from '../../../../js/redux/store';

interface IProps {
	staticLink: IApiLink;
	dispatch(value: any): void;
}

const QuickLinkItem: React.StatelessComponent<IProps> = props => {
	let actionComponent: React.ReactElement;

	if (props.staticLink.url.type === 'custom') {
		// Open Chatt to counsellor modal
		if (
			props.staticLink.url.data === EStaticLinkType.OPEN_COUNSELLOR_CHAT
		) {
			actionComponent = (
				<ButtonBase
					className="consellor_chat quick_links_item-button"
					href={`mailto:${
						process.env.INTERCOM_APP_ID
					}@intercom-mail.com`}
				/>
			);
		} else if (
			props.staticLink.url.data === EStaticLinkType.OPEN_NEW_BLOG_MODAL
		) {
			const predefinedTopic = 'my-story';

			actionComponent = (
				<ButtonBase
					className="quick_links_item-button"
					onClick={() => {
						props.dispatch(
							newBlogModalDisplay(null, predefinedTopic),
						);
					}}
				/>
			);
		} else if (
			props.staticLink.url.data ===
			EStaticLinkType.OPEN_NEW_DISCUSSION_MODAL
		) {
			const predefinedTopic = 'my-story';
			const predefinedSituations = [
				'my-parent-has-cancer',
				'my-sibling-has-cancer',
				'my-parent-died-from-cancer',
				'my-sibling-died-from-cancer',
				'im-a-young-person-with-cancer',
				'i-survived-cancer',
				'my-parent-survived-cancer',
				'my-sibling-survived-cancer',
			];

			actionComponent = (
				<ButtonBase
					className="quick_links_item-button"
					onClick={() => {
						props.dispatch(
							discussionModalDisplay(
								null,
								predefinedTopic,
								predefinedSituations,
							),
						);
					}}
				/>
			);
		}

		return (
			<div className="quick_links_item">
				{actionComponent}

				<div className="quick_links_item-image_container">
					<Avatar
						className="quick_links_item-image"
						src={props.staticLink.feature_image.url}
					/>
				</div>

				<div className="quick_links_item-content">
					<span className="quick_links_item-type">
						{props.staticLink.title}
					</span>
					<span className="quick_links_item-title">
						{props.staticLink.subtitle}
					</span>
				</div>
			</div>
		);
	} else if (props.staticLink.url.type === 'link') {
		const formattedUrl = dissectUrl(props.staticLink.url.data);

		return (
			<div className="quick_links_item">
				<ButtonBaseLink
					className="quick_links_item-button"
					href={formattedUrl.href}
					hrefAs={formattedUrl.hrefAs}
				/>

				<div className="quick_links_item-image_container">
					<Avatar
						className="quick_links_item-image"
						src={props.staticLink.feature_image.url}
					/>
				</div>

				<div className="quick_links_item-content">
					<span className="quick_links_item-type">
						{props.staticLink.subtitle}
					</span>
					<span className="quick_links_item-title">
						{props.staticLink.title}
					</span>
				</div>
			</div>
		);
	} else {
		return null;
	}
};

const mapStateToProps = (_state: IStoreState) => {
	return {};
};

export default connect(mapStateToProps)(withServices(QuickLinkItem));
