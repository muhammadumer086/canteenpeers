<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class State extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
        if ($apiStore->hasApiResource('state', $this->id, $this->updated_at, true, false)) {
            return $apiStore->getApiResource('state', $this->id, $this->updated_at, true, false);
        }

        $params = [
            'id',
            'slug',
            'name',
            'abbreviation',
        ];

        $result = [];

        foreach ($params as $key) {
            $result[$key] = $this->$key;
        }
       
        $apiStore->setApiResource($result, 'state', $this->id, $this->updated_at, true, false);

        return $result;return parent::toArray($request);
    }
}
