<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

use App\Topic;
use App\Traits\HasHashtags;
use App\Traits\AlgoliaIndex;
use App\Traits\HasSeoAttribute;

class Blog extends Model {
	use Searchable;
	use SoftDeletes;
	use AlgoliaIndex;
	use HasHashtags;
	use HasSeoAttribute;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'prismic_id',
		'title',
		'slug',
		'content',
		'age_sensitive',
		'source',
		'prismic_author',
		'prismic_author_image',
		'feature_image',
		'featured',
		'first_publication_date',
		'last_publication_date',
		'topic_id',
		'user_id',
		'migration_id',
		'seo_title',
		'seo_description',
		'seo_image',
		'deleted_at',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'feature_image' => 'array',
		'seo_image' => 'array',
		'age_sensitive' => 'boolean',
		'private' => 'boolean',
		'featured' => 'boolean',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
		'first_publication_date',
		'last_publication_date',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['seo'];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'blogs';

	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName() {
		return 'slug';
	}

	/**
	 * Get the index name for the model.
	 *
	 * @return string
	 */
	public function searchableAs() {
		return $this->algoliaIndex();
	}

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {
		if (!$this->private) {
			$type = str_slug((new \ReflectionClass($this))->getShortName());
			$id = $this->slug;
			$image = null;
			$title = $this->title;
			$excerpt = str_limit(strip_tags($this->content), 250);
			$data = str_limit(strip_tags($this->content), 5000);
			$hashtags = $this->hashtags()
				->pluck('name')
				->toArray();

			if (
				$this->feature_image &&
				is_array($this->feature_image) &&
				isset($this->feature_image['url'])
			) {
				$image = $this->feature_image['url'];
			}

			// Result structure
			$result = [
				'type' => $type,
				'id' => $id,
				'image' => $image,
				'title' => $title,
				'excerpt' => $excerpt,
				'data' => $data,
				'hashtags' => $hashtags,
			];

			return $result;
		}

		return [];
	}

	/**
	 * Get the value used to index the model.
	 *
	 * @return mixed
	 */
	public function getScoutKey() {
		return $this->algolia_id;
	}

	/**
	 * The user that created this blog
	 */
	public function user() {
		return $this->belongsTo('App\User')->withTrashed();
	}

	/**
	 * Topic relating to this blog
	 */
	public function topic() {
		return $this->belongsTo('App\Topic');
	}

	public function userActivities() {
		return $this->morphMany('App\UserActivity', 'activity');
	}

	/**
	 * The situation topics related to this blog
	 */
	public function situations() {
		return $this->belongsToMany('App\Situation');
	}

	/**
	 * The blog images related to this blog
	 */
	public function blogImages() {
		return $this->belongsToMany('App\BlogImage');
	}

	public function getAlgoliaIdAttribute() {
		$type = str_slug((new \ReflectionClass($this))->getShortName());
		return "$type/{$this->slug}";
	}

	/**
	 * Get the number of views
	 *
	 * @return bool
	 */
	public function getViewsCountAttribute() {
		return $this->userActivities()
			->where('type', 'Blog Read')
			->count();
	}
}
