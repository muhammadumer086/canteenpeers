import React from 'react';
import AnimateHeight from 'react-animate-height';

import ConditionalCheckBoxFieldFormik from '../../inputs/ConditionalChecklBoxFieldFormik';
import ListLoading from '../../../../js/components/general/ListLoading';
import Pill from '../../../../js/components/general/Pill';

interface IProps {
	data: ISlugLabel[];
	activeData: any;
	sectionLabel: string;
	stateCheckReference: string;
	stateDataReference: string;
	isAreaActive: boolean;
	toggleArea(area: string): void;
	togglePill(slug: string, targetArea: string): void;
}

export interface ISlugLabel {
	value: string;
	label: string;
}

const AnimatePillListFormik: React.StatelessComponent<IProps> = props => {
	if (!!props.data) {
		return (
			<React.Fragment>
				<div className="form-row form-row--space_above">
					<div className="form-column">
						<ConditionalCheckBoxFieldFormik
							label={props.sectionLabel}
							name={props.stateCheckReference}
							isChecked={props.isAreaActive}
							conditionAction={props.toggleArea}
						/>
					</div>
				</div>

				<AnimateHeight
					duration={400}
					height={props.isAreaActive ? 'auto' : 0}
				>
					<div className="form-row">
						<div className="form-column">
							{props.data.length > 0 &&
								props.data.map((item, index) => {
									const isActive =
										props.activeData.indexOf(item.value) >=
										0;

									return (
										<Pill
											key={index}
											label={item.label}
											active={isActive}
											disabled={false}
											clickable={true}
											onClick={() => {
												props.togglePill(
													item.value,
													props.stateDataReference,
												);
											}}
										/>
									);
								})}
						</div>
					</div>
				</AnimateHeight>
			</React.Fragment>
		);
	} else {
		return <ListLoading active={true} />;
	}
};

export default AnimatePillListFormik;
