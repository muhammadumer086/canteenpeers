import React from 'react';
import Document from 'next/document';
import { Html, Head, Main, NextScript } from 'next/document';
import PropTypes from 'prop-types';
import flush from 'styled-jsx/server';

import { Error400, Error403, Error404 } from '../js/services/ApiService';
import { AuthService } from '../js/services';

export default class MyDocument extends Document {
	protected theme: any;
	public props: any;

	static async getInitialProps(context) {
		try {
			// Render app and page and get the context of the page with collected side effects.
			let pageContext;
			const page = context.renderPage(Component => {
				const WrappedComponent: any = props => {
					pageContext = props.pageContext;
					return <Component {...props} />;
				};

				WrappedComponent.propTypes = {
					pageContext: PropTypes.object.isRequired,
				};

				return WrappedComponent;
			});

			// Styles fragment is rendered after the app and page rendering finish.
			let styles = <React.Fragment>{flush() || null}</React.Fragment>;

			styles = (
				<React.Fragment>
					<style
						id="jss-server-side"
						// eslint-disable-next-line react/no-danger
						dangerouslySetInnerHTML={{
							__html: pageContext
								? pageContext.sheetsRegistry.toString()
								: '',
						}}
					/>
					{flush() || null}
				</React.Fragment>
			);
			return {
				...page,
				pageContext,
				// Styles fragment is rendered after the app and page rendering finish.
				styles,
			};
		} catch (error) {
			console.log(error);
			if (
				'res' in context &&
				'req' in context &&
				context.req.url !== '/auth/login' &&
				context.req.url !== '/' &&
				!(error instanceof Error400) &&
				!(error instanceof Error403) &&
				!(error instanceof Error404)
			) {
				if ('authService' in context && context.authService) {
					(context.authService as AuthService).clearAuthCookies();
				}
				context.res.writeHead(302, {
					Location: '/auth/login',
				});
				context.res.end();
			} else {
				throw error;
			}
		}

		return {};
	}

	public render() {
		const { pageContext } = this.props;

		const googleMaps = `https://maps.googleapis.com/maps/api/js?key=${process.env.GOOGLE_MAPS_API_KEY}&v=3.exp&libraries=geolocation,places`;
		const gooleTagManager = process.env.GOOGLE_TAG_MANAGER_ID
			? `https://www.googletagmanager.com/ns.html?id=${process.env.GOOGLE_TAG_MANAGER_ID}`
			: null;

		const appBannerId = 'app-id=1484899856';

		return (
			<Html className="no-js">
				<Head>
					<link
						rel="icon"
						type="image/png"
						href={`${process.env.STATIC_PATH}/images/favicon.png`}
					/>
					<meta name="apple-itunes-app" content={appBannerId} />
					<meta
						name="theme-color"
						content={
							pageContext
								? pageContext.theme.palette.primary.main
								: '#000'
						}
					/>
					<meta
						name="google-site-verification"
						content="lZTSMqJY1V2YaE6_mAxgcXNZw3Nc98EfMzknROnJl-k"
					/>
					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>
					<link
						href="https://fonts.googleapis.com/css?family=Muli:400,400i,700,700i"
						rel="stylesheet"
					/>
					<link
						rel="stylesheet"
						href="https://fonts.googleapis.com/icon?family=Material+Icons"
					/>
					<link
						rel="stylesheet"
						href={`${process.env.STATIC_PATH}/fonts.css`}
					/>
					<link
						rel="stylesheet"
						href="https://cdn.quilljs.com/1.3.6/quill.snow.css"
					/>

					<link rel="manifest" href={`/manifest.json`} />

					<script
						src={`${process.env.STATIC_PATH}/js/modernizr.custom.js`}
					/>
					<script>dataLayer = [];</script>
					{!!gooleTagManager && (
						<script
							dangerouslySetInnerHTML={{
								__html: `
							(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
							new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
							j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
							'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
							})(window,document,'script','dataLayer','${process.env.GOOGLE_TAG_MANAGER_ID}');
						`,
							}}
						/>
					)}

					<script
						dangerouslySetInnerHTML={{
							__html: `let deferredPrompt;

							window.addEventListener('beforeinstallprompt', (e) => {
							// Prevent Chrome 67 and earlier from automatically showing the prompt
							e.preventDefault();
							// Stash the event so it can be triggered later.
							console.log('beforeinstallprompt', e);
							deferredPrompt = e;
							});`,
						}}
					/>

					<script
						dangerouslySetInnerHTML={{
							__html: `
						let iosDevice = !!navigator.platform.match(/iPhone|iPod/);
						let root = document.getElementsByTagName( 'html' )[0];

						if (
							iosDevice
						) {
							root.setAttribute( 'class', 'ios-device' );
						}
					`,
						}}
					/>

					<script
						dangerouslySetInnerHTML={{
							__html: `
						window.intercomSettings = {
							app_id: "${process.env.INTERCOM_APP_ID}",
							custom_launcher_selector: '.consellor_chat',
							hide_default_launcher: true,
							alignment: 'right',
							horizontal_padding: 30,
							vertical_padding: 16,
							platform: "${process.env.APP_NAME}"
						};
					`,
						}}
					/>
					<script
						dangerouslySetInnerHTML={{
							__html: `(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/${process.env.INTERCOM_APP_ID}';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()`,
						}}
					/>

					<script src="https://www.googleoptimize.com/optimize.js?id=OPT-NGJ7DNV"></script>
				</Head>
				<body>
					{!!gooleTagManager && (
						<noscript
							dangerouslySetInnerHTML={{
								__html: `
							<iframe
								url="${gooleTagManager}"
								height="0"
								width="0"
								style="position: absolute; width: 0; height: 0;"
							></iframe>
						`,
							}}
						/>
					)}

					<Main />
					<script src={googleMaps} />
					<NextScript />
					<script
						dangerouslySetInnerHTML={{
							__html: `
						document.documentElement.classList.remove('no-js');
						document.documentElement.classList.remove('js');
					`,
						}}
					/>
					<script
						src="https://37f12d309b7c4ce0ab4c67d358543c2a.js.ubembed.com"
						async
					/>
				</body>
			</Html>
		);
	}
}
