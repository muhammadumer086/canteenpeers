import React from "react";
import Head from "next/head";
import { withRouter, SingletonRouter } from "next/router";

import Paper from "@material-ui/core/Paper";


import withAdminPage, {
  IAdminProps,
  IAdminState,
} from "../js/components/admin/withAdminPage";
import LayoutDashboard from "../js/components/general/LayoutDashboard";
import { ButtonBaseLink } from "../js/components/general/ButtonBaseLink";
import HeroHeading from "../js/components/general/HeroHeading";

import PageActivities from "../js/components/admin/pages/activities";
import { EActivitiesPermissions } from "../js/components/admin/pages/activities";
import PageAlerts from "../js/components/admin/pages/alert-management";
import { EAlertManagmentPermissions } from "../js/components/admin/pages/alert-management";
import PageEvents from "../js/components/admin/pages/events-management";
import { EEventManagementPermissions } from "../js/components/admin/pages/events-management";
import PageReports from "../js/components/admin/pages/reports";
import { EReportedDiscussionPermissions } from "../js/components/admin/pages/reports";
import PageStaff from "../js/components/admin/pages/staff-management";
import { EStaffManagementPermissions } from "../js/components/admin/pages/staff-management";
import PageTopics from "../js/components/admin/pages/topics";
import { ETopicsManagementPermissions } from "../js/components/admin/pages/topics";
import PageUsers from "../js/components/admin/pages/users";
import { EUsersManagementPermissions } from "../js/components/admin/pages/users";
import PageGraduatedUser from "../js/components/admin/pages/graduated";
import { EGraduateUserManagementPermissions } from "../js/components/admin/pages/graduated";
import PageRoles from "../js/components/admin/pages/roles";
import {
  apiError,
  //apiSuccess,
} from '../js/redux/store';
import { IApiUser, IApiPermission } from '../js/interfaces';
import { checkPermission } from "../js/helpers/checkPermission";



export enum EAdminPages {
  ALL = "all",
  ACTIVITIES = "activities",
  ALERTS = "alerts",
  EVENTS = "events",
  GRADUATED = "graduated",
  REPORTS = "reports",
  ROLES = "roles",
  STAFF = "staff",
  TOPICS = "topics",
  USERS = "users",
}
interface IAdminAction {
  hrefAs: string;
  href: string;
  title: string;
  description: string;
}
interface IProps extends IAdminProps {
  router: SingletonRouter;
  userData: IApiUser;
}

interface IState extends IAdminState {
  isLoading: boolean;
  adminActions: IAdminAction[];
  type: EAdminPages;
  permissions: IApiPermission[];

}

class Admin extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isLoading: false,
      adminActions: this.getAdminAction(props.roleName),
      type: this.getTypeFromRouter(props.router),
      permissions: [],
    };
    this.getPermissions = this.getPermissions.bind(this);
    this.renderModules = this.renderModules.bind(this);
  }

  public componentDidUpdate(prevProps: IProps) {
    if (
      JSON.stringify(this.props.router.query) !==
      JSON.stringify(prevProps.router.query)
    ) {
      this.setState({
        type: this.getTypeFromRouter(this.props.router),
      },()=>this.getPermissions());

    }
    
  }
  public componentDidMount() {
    this.getPermissions();
  }

  public render() {
    const pageProps = {
      apiService: this.props.apiService,
      authService: this.props.authService,
      query: this.props.query,
      userAuthenticated: this.props.userAuthenticated,
      userAdmin: this.props.userAdmin,
      roleName:this.props.roleName,
      permissions : this.state.permissions,
      dispatch: this.props.dispatch,

    };

    switch (this.state.type) {
      case EAdminPages.ACTIVITIES:
        return <PageActivities {...pageProps} />;
      case EAdminPages.ALERTS:
        return <PageAlerts {...pageProps} />;
      case EAdminPages.EVENTS:
        return <PageEvents {...pageProps} />;
      case EAdminPages.GRADUATED:
        return <PageGraduatedUser {...pageProps} />;
      case EAdminPages.REPORTS:
        return <PageReports {...pageProps} />;
      case EAdminPages.STAFF:
        return <PageStaff {...pageProps} />;
      case EAdminPages.TOPICS:
        return <PageTopics {...pageProps} />;
      case EAdminPages.USERS:
        return <PageUsers {...pageProps} />;
      case EAdminPages.ROLES: {
        if (this.props.roleName==="admin") {//only for admin
          return <PageRoles {...pageProps} />;
        }
      }
    }
    return (
      <div>
        <Head>
          <title>Admin — {process.env.APP_NAME}</title>
        </Head>
        <LayoutDashboard>
          <header className="hero theme--accent">
            <HeroHeading title="Admin Area" />
          </header>
          {this.renderModules()}
        </LayoutDashboard>
      </div>
    );
  }

  protected getTypeFromRouter(router: SingletonRouter): EAdminPages {
    if ("type" in router.query) {
      switch (router.query.type) {
        case "activities":
          return EAdminPages.ACTIVITIES;
        case "alerts":
          return EAdminPages.ALERTS;
        case "events":
          return EAdminPages.EVENTS;
        case "graduated":
          return EAdminPages.GRADUATED;
        case "reports":
          return EAdminPages.REPORTS;
        case "staff":
          return EAdminPages.STAFF;
        case "topics":
          return EAdminPages.TOPICS;
        case "users":
          return EAdminPages.USERS;
        case "roles":
          return EAdminPages.ROLES;
        default:
          return EAdminPages.ALL;
      }
    }

    return EAdminPages.ALL;
  }
  protected getAdminAction(roleName:string) {
   

    const adminActions = [
      {
        hrefAs: `/admin?type=${EAdminPages.USERS}`,
        href: `/admin?type=${EAdminPages.USERS}`,
        title: "Users Management",
        description: "Block, remove, promote & review users",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.ACTIVITIES}`,
        href: `/admin?type=${EAdminPages.ACTIVITIES}`,
        title: "All Users Activities",
        description: "Review all the users actions",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.TOPICS}`,
        href: `/admin?type=${EAdminPages.TOPICS}`,
        title: "Topics Management",
        description: "Add, remove, modify topics",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.REPORTS}`,
        href: `/admin?type=${EAdminPages.REPORTS}`,
        title: "Reported Discussions",
        description: "Review reported discussions",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.EVENTS}`,
        href: `/admin?type=${EAdminPages.EVENTS}`,
        title: "Events Management",
        description: "Create, delete and update events",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.ALERTS}`,
        href: `/admin?type=${EAdminPages.ALERTS}`,
        title: "Alert Management",
        description: "Create, delete and update alerts",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.STAFF}`,
        href: `/admin?type=${EAdminPages.STAFF}`,
        title: "Staff Management",
        description: "Create, delete and update staff members",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.GRADUATED}`,
        href: `/admin?type=${EAdminPages.GRADUATED}`,
        title: "Graduated User Management",
        description: "Manage Graduated Users",
      },
      {
        hrefAs: `/admin?type=${EAdminPages.ROLES}`,
        href: `/admin?type=${EAdminPages.ROLES}`,
        title: "Role Management",
        description: "Manage Roles And Permissions of Users",
      }
    ];
    if ( roleName === "admin") {
      return adminActions;
    }
    else {
      return [];
    }
  }
  protected getPermissions() {
    this.setState({ isLoading: true }, () => {
      const role=this.props.roleName;
      const url = `/api/roles/getRolePermissions?name=${role}`;
      //const url = `/api/roles/getRolePermissions?name=COSS`;
      this.props.apiService.queryGET(url)
        .then(res => {
          if ('role' in res && 'permissions' in res.role) {
            const permissions: IApiPermission[] = res.role.permissions;
            this.setState({ isLoading: false, permissions }, () => {
              if (role === "admin") {
                return;
              }
              const adminActions: IAdminAction[] = [];
              const userManagementRead = checkPermission(permissions, EUsersManagementPermissions.READ, EUsersManagementPermissions.ALL);
              if (userManagementRead) {
                adminActions.push(
                  {
                    hrefAs: `/admin?type=${EAdminPages.USERS}`,
                    href: `/admin?type=${EAdminPages.USERS}`,
                    title: "Users Management",
                    description: "Block, remove, promote & review users",
                  });
              }

              const userActivitiesRead = checkPermission(permissions, EActivitiesPermissions.READ, EActivitiesPermissions.ALL);
              if (userActivitiesRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.ACTIVITIES}`,
                  href: `/admin?type=${EAdminPages.ACTIVITIES}`,
                  title: "All Users Activities",
                  description: "Review all the users actions",
                });
              }

              const topicManagementRead = checkPermission(permissions, ETopicsManagementPermissions.READ, ETopicsManagementPermissions.ALL);
              if (topicManagementRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.TOPICS}`,
                  href: `/admin?type=${EAdminPages.TOPICS}`,
                  title: "Topics Management",
                  description: "Add, remove, modify topics",
                })
              }

              const reportedDiscussionsRead = checkPermission(permissions, EReportedDiscussionPermissions.READ, EReportedDiscussionPermissions.ALL);
              if (reportedDiscussionsRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.REPORTS}`,
                  href: `/admin?type=${EAdminPages.REPORTS}`,
                  title: "Reported Discussions",
                  description: "Review reported discussions",
                })
              }

              const eventsManagementRead = checkPermission(permissions, EEventManagementPermissions.READ, EEventManagementPermissions.ALL);
              if (eventsManagementRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.EVENTS}`,
                  href: `/admin?type=${EAdminPages.EVENTS}`,
                  title: "Events Management",
                  description: "Create, delete and update events",
                })
              }

              const alertsManagementRead = checkPermission(permissions, EAlertManagmentPermissions.READ, EAlertManagmentPermissions.ALL);
              if (alertsManagementRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.ALERTS}`,
                  href: `/admin?type=${EAdminPages.ALERTS}`,
                  title: "Alert Management",
                  description: "Create, delete and update alerts",
                })
              }

              const staffManagementRead = checkPermission(permissions, EStaffManagementPermissions.READ, EStaffManagementPermissions.ALL);
              if (staffManagementRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.STAFF}`,
                  href: `/admin?type=${EAdminPages.STAFF}`,
                  title: "Staff Management",
                  description: "Create, delete and update staff members",
                })
              }

              const graduateUsersManagementRead = checkPermission(permissions, EGraduateUserManagementPermissions.READ, EGraduateUserManagementPermissions.ALL);
              if (graduateUsersManagementRead) {
                adminActions.push({
                  hrefAs: `/admin?type=${EAdminPages.GRADUATED}`,
                  href: `/admin?type=${EAdminPages.GRADUATED}`,
                  title: "Graduated User Management",
                  description: "Manage Graduated Users",
                })
              }


              this.setState({ adminActions },()=>console.log("admin action ===> ",this.state.adminActions))
            });
          }
        }).catch(_e => {
          this.props.dispatch(apiError(["Something went wrong"]))
          this.setState({ isLoading: false });
        })
    })
  }
  protected renderModules() {
    return (<div className="admin_content">
      <div className="admin_content-grid">
        {this.props.userAuthenticated&& this.state.adminActions.map((adminAction) => {
          return (
            <ButtonBaseLink
              focusRipple={true}
              className="admin_content-link"
              href={adminAction.href}
              hrefAs={adminAction.hrefAs}
              key={adminAction.href}
            >
              <Paper className="admin_content-link-content" square={true}>
                <div className="hp-16">
                  <h2 className="font--h5">{adminAction.title}</h2>
                  <div className="hm-t16">
                    <p className="hm-0">{adminAction.description}</p>
                  </div>
                </div>
              </Paper>
            </ButtonBaseLink>
          );
        })}
      </div>
    </div>);
  }
}

export default withRouter(withAdminPage(Admin) as any);
