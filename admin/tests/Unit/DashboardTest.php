<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testDashboard()
	{
		$response = $this->json('GET', '/api/dashboard');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'announcement',
                'discussions',
                'resources',
                'blogs',
                'events',
            ]);
	}
}
