import { actionTypes } from '../actions';

export interface IStoreState {
	registerEventInterestModalDisplay: boolean;
	registerEventInterestModalSlug: string;
	registerEventInterestModalDate: string;
	registerEventInterestModalLocation: string;
}

export const eventRegisterInterestModal = (
	state: IStoreState = {
		registerEventInterestModalDisplay: false,
		registerEventInterestModalSlug: '',
		registerEventInterestModalDate: '',
		registerEventInterestModalLocation: '',
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.REGISTER_EVENT_INTEREST_MODAL_DISPLAY:
			return Object.assign({}, state, {
				registerEventInterestModalDisplay: true,
				registerEventInterestModalSlug: action.eventData.slug,
				registerEventInterestModalDate: action.eventData.date,
				registerEventInterestModalLocation: action.eventData.location,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.REGISTER_EVENT_INTEREST_MODAL_HIDE:
			return Object.assign({}, state, {
				registerEventInterestModalDisplay: false,
				registerEventInterestModalSlug: '',
				registerEventInterestModalDate: '',
				registerEventInterestModalLocation: '',
			});
		default:
			return state;
	}
};
