import { actionTypes } from '../actions';
import { IApiUser } from '../../interfaces';
import { ETabs } from '../../components/user-panel/UserPanelAccount';

export interface IStoreState {
	userPanelDisplay: boolean;
	userPanelData: IApiUser|null;
	userPanelTab: ETabs;
	onUserUpdated: () => void|null;
}

export const userPanel = (state: IStoreState = {
	userPanelDisplay: false,
	userPanelData: null,
	userPanelTab: ETabs.MY_PROFILE,
	onUserUpdated: null,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.USER_PANEL_DISPLAY:
			return Object.assign({}, state, {
				userPanelDisplay: true,
				userPanelData: action.userPanelData,
				userPanelTab: action.userPanelTab,
				onUserUpdated: action.onUserUpdated,
			});
		case actionTypes.USER_PANEL_CHANGE_TAB:
			return Object.assign({}, state, {
				userPanelTab: action.userPanelTab,
			});
		case actionTypes.USER_LOGOUT: // hide panel on logout
		case actionTypes.USER_PANEL_HIDE:
			return Object.assign({}, state, {
				userPanelDisplay: false,
				onUserUpdated: null,
			});
		case actionTypes.USER_AVATAR_UPLOADED:
			return Object.assign({}, state, {
				userPanelData: action.userPanelData,
			});
		case actionTypes.USER_PANEL_UPDATED:
			return Object.assign({}, state, {
				userPanelData: action.userPanelData,
			});
		default:
			return state;
	}
}
