import React from 'react';
import { connect } from 'react-redux';
import { ApiService, withServices } from '../../../services';
import {
	IApiSituation,
	ISituationOption,
	IApiUser,
	ISelectOption,
} from 'js/interfaces';
import { apiError, IStoreState } from '../../../redux/store';
import { SelectFieldFormik } from './../../inputs';

interface IProps {
	userData: IApiUser;
	apiService: ApiService;
	inputProps: any;
	dispatch(value: any): void;
}

interface IState {
	submitting: boolean;
	situations: ISituationOption[];
	situationsList: IApiSituation[];
}

class WelcomeModal extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
			situations: [],
			situationsList: [],
		};
	}

	public componentDidMount() {
		// Load the situations
		this.props.apiService
			.queryGET('/api/situations', 'situations')
			.then((situationsList: IApiSituation[]) => {
				const situations: ISituationOption[] = [];

				if (situationsList instanceof Array) {
					situationsList.map((situation: IApiSituation) => {
						if (!situation.cancer_type) {
							situations.push({
								name: situation.name,
								slug: situation.main_slug,
								cancer_type: situation.cancer_type,
								date: situation.date ? situation.date : null,
							});
						}
					});
				}

				this.setState({
					situations,
					situationsList,
				});
			})
			.catch(error => {
				this.props.dispatch(apiError(error));
			});
	}

	public render() {
		return (
			<div className="modal_introduction-item">
				<div className="modal_introduction-item_image modal_introduction-item_image--mobile modal_introduction-item_image--step_two" />

				<div className="modal_introduction-item_content theme--main">
					<h4 className="font--h4 theme-title">
						Tailored just for you
					</h4>
					<p>
						Providing the type of cancer that’s affecting you or
						your loved one will help us connect you with people and
						content that are most relevant to you.
					</p>

					<div className="modal_introduction-select">
						{this.props.inputProps.values.situations.map(
							(singleSituation: ISituationOption) => {
								return this.renderSituationOptions(
									this.props.inputProps,
									singleSituation,
								);
							},
						)}
					</div>

					<small className="font--small font--italic">
						You can do this later in your profile too
					</small>
				</div>

				<div className="modal_introduction-item_image modal_introduction-item_image--step_two" />
			</div>
		);
	}

	protected renderSituationOptions(
		inputProps: any,
		singleSituation: ISituationOption,
	) {
		// Get the list of cancer types associated with this situation
		const cancerTypes: ISelectOption[] = [];
		this.state.situationsList.forEach(situation => {
			if (
				situation.main_slug === singleSituation.slug &&
				situation.cancer_type
			) {
				cancerTypes.push({
					label: situation.cancer_type,
					value: situation.cancer_type,
				});
			}
		});

		if (!cancerTypes.length) {
			return null;
		}

		return (
			<React.Fragment key={singleSituation.slug}>
				<div className="form-row">
					<div className="form-column">
						<SelectFieldFormik
							label="Cancer Type"
							name={`cancer_type_${singleSituation.slug}`}
							options={cancerTypes}
							values={singleSituation.cancer_type}
							disabled={false}
							emptyLabel="Please select"
							{...inputProps}
						/>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state: IStoreState) => {
	const { userData } = state.user;

	return {
		userData,
	};
};

export default connect(mapStateToProps)(withServices(WelcomeModal));
