@extends('layouts.email')

@section('plain_text')
	Hi,

	@if ($ageSensitive)
		Community member {{ $author->full_name }} flagged <b>{{$mainDiscussion->title}}</b> as age-sensitive.
	@elseif ($isReply)
		Community member {{ $author->full_name }} reported a reply in <b>{{$mainDiscussion->title}}</b>.
	@else
		Community member {{ $author->full_name }} reported <b>{{$mainDiscussion->title}}</b>.
	@endif
@endsection

@section('content')
	@component('emails.components.paragraph')
		Hi,
	@endcomponent

	@component('emails.components.paragraph')
		@if ($ageSensitive)
			Community member {{ $author->full_name }} flagged <b>{{$mainDiscussion->title}}</b> as age-sensitive.
		@elseif ($isReply)
			Community member {{ $author->full_name }} reported a reply in <b>{{$mainDiscussion->title}}</b>.
		@else
			Community member {{ $author->full_name }} reported <b>{{$mainDiscussion->title}}</b>.
		@endif
	@endcomponent

	@php
		$link = config('canteen.facade_url') . '/admin/reports';
	@endphp
	@component('emails.components.button', ['url' => $link])
		Moderate discussions
	@endcomponent

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
