import React from 'react';
import Link from 'next/link';
import ButtonBase from '@material-ui/core/ButtonBase';
import Count from './../general/Count';
import { ILinkData } from '../../interfaces';

export interface IStat {
	count: number;
	label: string;
	color?: string;
	inheritBorderColor?: boolean;
}

interface IProps {
	stats: IStat;
	isDisabled?: boolean;
	link?: ILinkData;
	action?(): void;
}

const UserStat: React.FC<IProps> = (props: IProps) => {
	const statClasses = ['user_stat-counter-number'];

	if (props.stats.color === 'green') {
		statClasses.push('user_stat-counter-number--color_green');
	}

	if (props.stats.color === 'yellow') {
		statClasses.push('user_stat-counter-number--color_yellow');
	}

	if (props.stats.color === 'red') {
		statClasses.push('user_stat-counter-number--color_red');
	}

	if (props.stats.inheritBorderColor) {
		statClasses.push('user_stat-counter-number--inherit');
	}

	const RenderStatWithLink = () => {
		return (
			<Link href={props.link.href} as={props.link.as}>
				<ButtonBase
					focusRipple={true}
					disabled={props.isDisabled}
					className="user_stat-button"
					onClick={props.action}
				>
					<div className="user_stat-container">
						<div className={statClasses.join(' ')}>
							<Count count={props.stats.count} />
						</div>
						<div className="user_stat-counter-label">
							{props.stats.label}
						</div>
					</div>
				</ButtonBase>
			</Link>
		);
	};

	const RenderStatWithoutLink = () => {
		return (
			<ButtonBase
				focusRipple={true}
				disabled={props.isDisabled}
				className="user_stat-button"
				onClick={props.action}
			>
				<div className="user_stat-container">
					<div className={statClasses.join(' ')}>
						<Count count={props.stats.count} />
					</div>
					<div className="user_stat-counter-label">
						{props.stats.label}
					</div>
				</div>
			</ButtonBase>
		);
	};

	const hasLink = props.link;
	let component;

	if (hasLink) {
		component = <RenderStatWithLink />;
	} else {
		component = <RenderStatWithoutLink />;
	}

	return <React.Fragment>{component}</React.Fragment>;
};

export default UserStat;
