<?php

namespace App\Filters\Activity;

use App\Filters\FiltersAbstract;
use App\User;

use App\Filters\Activity\ColumnOrder;

class UserActivityFilters extends FiltersAbstract {
	/**
	 * Default course filters.
	 *
	 * @var array
	 */
	protected $filters = [
		'sort' => ColumnOrder::class,
	];

	public static function mappings() {
		return $map;
	}
}
