<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use League\Csv\Writer;

use App\Helpers\CsvMigration;
use App\Blog;
use App\User;

class CsvMigrationBlogs extends CsvMigration {
	/**
	 * The headers for the raw CSV
	 *
	 * @var array
	 */
	public $rawCsvHeaders = [
		'BLOG Titles',
		'Spreadsheet ID',
		'My Diagnosis',
		'My Treatment',
		'Life After Treatment',
		"My Parent's Cancer",
		"My Sibling's Cancer",
		'Grief & Bereavement',
		'Cancer Stuff',
	];
	/**
	 * The headers for the CSV
	 *
	 * @var array
	 */
	public $csvHeaders = [
		'id',
		'title',
		'content',
		'author',
		'created_at',
		'topics',
	];

	public function processRecord(Writer $writer, $record) {
		$id = $record['Spreadsheet ID'];
		$title = $record['BLOG Titles'];

		$blogData = DB::connection('wordpress')
			->table('wp_posts')
			->where('ID', $id)
			->where('post_status', 'publish')
			->first();

		if (!$blogData) {
			$blogData = DB::connection('wordpress')
				->table('wp_posts')
				->where('post_title', htmlentities($title, ENT_QUOTES))
				->where('post_status', 'publish')
				->first();
		}

		if ($blogData) {
			$result = $this->formatRecord($blogData, $record);
			$writer->insertOne($result);
		} else {
			echo "\r\n\r\nSkipped {$title}\r\n\r\n";
		}
	}

	protected function formatRecord($data, $record) {
		return [
			$data->ID,
			html_entity_decode($data->post_title, ENT_QUOTES),
			$this->processContent($data->post_content, true),
			$data->post_author,
			$data->post_date,
			$this->formatTopics($record, [
				'My Diagnosis' => 'My Diagnosis',
				'My Treatment' => 'My Treatment',
				'Life After Treatment' => 'Life after Treatment',
				"My Parent's Cancer" => "My Parent's Cancer",
				"My Sibling's Cancer" => "My Sibling's Cancer",
				'Grief & Bereavement' => 'Grief & Bereavement',
				'Cancer Stuff' => 'Cancer Stuff',
			]),
		];
	}

	public function importData($record) {
		$title = $this->nullIfEmpty($record['title']);
		if ($title) {
			$slug = Str::slug($title, '-');

			try {
				$user = User::where(
					'migration_id',
					$record['author']
				)->firstOrFail();
			} catch (\Exception $e) {
				echo "\r\n\r\nError creating blog {$record['id']}: User not found {$record['author']}\r\n\r\n";
				return;
			}

			try {
				$blog = Blog::updateOrCreate(
					[
						'migration_id' => $record['id'],
					],
					[
						'user_id' => $user->id,
						'title' => $title,
						'slug' => $slug,
						'content' => $this->processImportContent(
							$record['content']
						),
						'created_at' => $record['created_at'],
						'first_publication_date' => $record['created_at'],
						'last_publication_date' => $record['created_at'],
					]
				);

				$this->processSituation($blog, $user);
				$this->processTopics($blog, $record['topics'], 'blogs');
			} catch (\Exception $e) {
				echo "\r\n\r\nError creating discussion {$record['id']}: {$e->getMessage()}\r\n\r\n";
				$this->skippedDiscussions[] = $record['id'];
				return;
			}
		}
	}

	protected function processSituation($blog, $user) {
		// Grab the user's situations ID
		$situationsId = [];
		foreach ($user->situations as $situation) {
			// Always get the root situation
			if ($situation->parentSituation) {
				$situationsId[] = $situation->parentSituation->id;
			} else {
				$situationsId[] = $situation->id;
			}
		}

		$blog->situations()->sync($situationsId);
	}
}
