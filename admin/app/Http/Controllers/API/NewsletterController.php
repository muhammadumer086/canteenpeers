<?php
namespace App\Http\Controllers\API;

use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\CampaignMonitorSubscription;

class NewsletterController extends Controller {
	public function subscribe(Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'bail|required|email',
			'name' => 'bail|required|string',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		try {
			$subscription = new CampaignMonitorSubscription();
			$subscription->subscribeUserToCampaignMonitor(
				$request->input('email'),
				$request->input('name'),
				'',
				null,
				null,
				[],
				false,
				false
			);
			return response()->json([
				'success' => true,
			]);
		} catch (\Exception $e) {
			return response()->json(
				[
					'success' => false,
					'error' => $e,
				],
				400
			);
		}
	}
}

?>
