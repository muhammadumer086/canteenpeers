<?php

namespace App\Filters\Event;

use App\Filters\FiltersAbstract;
use App\Event;

use App\Filters\Event\StateFilter;
use App\Filters\Event\SituationFilter;
use App\Filters\Event\DurationFilter;
use App\Filters\Event\TimeFilter;

class EventFilters extends FiltersAbstract {
	/**
	 * Default course filters.
	 *
	 * @var array
	 */
	protected $filters = [
		'state' => StateFilter::class,
		'situations' => SituationFilter::class,
		'time' => TimeFilter::class,
		'duration' => DurationFilter::class,
	];

	public static function mappings() {
		return $map;
	}
}
