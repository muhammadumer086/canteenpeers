import { actionTypes } from '../actions';
import { IApiUser, IApiToken, IApiNotification } from '../../interfaces';

export interface IStoreState {
	userData: IApiUser | null;
	userAdmin: boolean | null;
	userVerified: boolean;
	userAuthenticated: boolean | null;
	userToken: IApiToken | null;
	userNotificationLoading: boolean;
	userNotificationCount: number;
	userNotifications: IApiNotification[] | null;
	userAvatarUploading: boolean;
	registerUserData: IApiUser | null;
}

export const user = (
	state: IStoreState = {
		userData: null,
		userAdmin: null,
		userVerified: false,
		userAuthenticated: false,
		userToken: null,
		userNotificationLoading: false,
		userNotificationCount: 0,
		userNotifications: null,
		userAvatarUploading: false,
		registerUserData: null,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.USER_REGISTERED:
			return Object.assign({}, state, {
				userData: action.userData,
			});
		case actionTypes.USER_VERIFIED:
			return Object.assign({}, state, {
				userVerified: true,
			});
		case actionTypes.USER_VERIFIED_DISPLAYED:
			return Object.assign({}, state, {
				userVerified: false,
			});
		case actionTypes.USER_AUTHENTICATING:
			return Object.assign({}, state, {
				userAuthenticated: null,
			});
		case actionTypes.USER_AUTHENTICATED:
			return Object.assign({}, state, {
				userToken: action.userToken,
				userData: action.userData,
				userAdmin: action.userAdmin,
				userAuthenticated: true,
			});
		case actionTypes.USER_LOGOUT:
			return Object.assign({}, state, {
				userToken: null,
				userAuthenticated: false,
				userData: null,
				userAdmin: null,
				userNotificationLoading: false,
				userNotificationCount: 0,
				userNotifications: null,
			});
		case actionTypes.USER_UPDATED:
			return Object.assign({}, state, {
				userData: action.userData,
			});
		// case actionTypes.USER_NOTIFICATION_LOADING:
		// 	return Object.assign({}, state, {
		// 		userNotificationLoading: true,
		// 	});
		// case actionTypes.USER_NOTIFICATION_LOADED:
		// 	return Object.assign({}, state, {
		// 		userNotificationLoading: false,
		// 		userNotificationCount: action.userNotificationCount,
		// 		userNotifications: action.userNotifications,
		// 	});
		case actionTypes.USER_AVATAR_UPLOADING:
			return Object.assign({}, state, {
				userAvatarUploading: true,
			});
		case actionTypes.USER_AVATAR_UPLOADED:
			return Object.assign({}, state, {
				userAvatarUploading: false,
				userData:
					action.userPanelData.id === state.userData.id
						? action.userPanelData
						: state.userData,
			});
		case actionTypes.USER_AVATAR_UPLOAD_ERROR:
			return Object.assign({}, state, {
				userAvatarUploading: false,
			});
		case actionTypes.USER_PANEL_UPDATED:
			return Object.assign({}, state, {
				userData: action.userData,
			});
		case actionTypes.USER_REGISTER_UPDATE_DATA:
			return Object.assign({}, state, {
				registerUserData: {
					...state.registerUserData,
					...action.userData,
				},
			});
		default:
			return state;
	}
};
