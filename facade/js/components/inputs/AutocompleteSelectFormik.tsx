import React from 'react';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Select from 'react-select';
import { ISelectOption } from '../../interfaces';

import CancelIcon from '@material-ui/icons/Cancel';

interface IProps {
	label: string;
	name: string;
	values: any;
	options: ISelectOption[];
	touched: any;
	errors: any;
	disabled?: boolean;
	multi?: boolean;
	emptyLabel?: string;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
	customHandleChange?(value: any, setFieldValue: (value: any) => void): void;
}

interface IState {
	disabled: boolean;
	selectValue: string | number | boolean | (string | number | boolean)[];
}

class Option extends React.Component<any, {}> {
	constructor(props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	public render() {
		const { children, isFocused, onFocus } = this.props;

		return (
			<MenuItem
				onFocus={onFocus}
				selected={isFocused}
				onClick={this.handleClick}
				component="div"
			>
				{children}
			</MenuItem>
		);
	}

	protected handleClick(event) {
		this.props.onSelect(this.props.option, event);
	}
}

const SelectWrapped: React.FC<any> = (props: any) => {
	const { classes, value, ...other } = props;

	return (
		<Select
			optionComponent={Option}
			noResultsText={<Typography>{'No results found'}</Typography>}
			arrowRenderer={arrowProps => {
				return arrowProps.isOpen ? (
					<ArrowDropUpIcon className="input-select-icon" />
				) : (
					<ArrowDropDownIcon className="input-select-icon" />
				);
			}}
			cacheOptions
			clearable={false}
			value={value}
			valueComponent={valueProps => {
				const { value, children, onRemove } = valueProps;

				const onDelete = event => {
					event.preventDefault();
					event.stopPropagation();
					onRemove(value);
				};

				if (onRemove) {
					return (
						<Chip
							tabIndex={-1}
							label={children}
							deleteIcon={<CancelIcon onTouchEnd={onDelete} />}
							onDelete={onDelete}
						/>
					);
				}

				return <div className="Select-value">{children}</div>;
			}}
			{...other}
		/>
	);
};

export class AutocompleteSelectFormik extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			disabled: false,
			selectValue: this.getSelectedOption(props),
		};

		this.handleChange = this.handleChange.bind(this);
	}

	public render() {
		return (
			<TextField
				className="form-input form-input--autocomplete"
				value={this.props.values[this.props.name]}
				onChange={this.handleChange}
				name={this.props.name}
				label={this.props.label}
				error={
					this.props.touched[this.props.name] &&
					!!this.props.errors[this.props.name]
				}
				helperText={
					this.props.touched[this.props.name]
						? this.props.errors[this.props.name]
						: null
				}
				InputLabelProps={{
					shrink: true,
				}}
				autoComplete="off"
				InputProps={{
					inputComponent: SelectWrapped,
					inputProps: {
						instanceId: 'react-select-chip-label',
						id: 'react-select-chip-label',
						options: this.props.options,
						disabled: this.state.disabled,
						value: this.state.selectValue,
						placeholder: 'Please select',
						multi: 'multi' in this.props ? this.props.multi : false,
					},
				}}
				disabled={this.state.disabled}
			/>
		);
	}

	public handleChange(option) {
		if (this.props.customHandleChange) {
			this.props.customHandleChange(option, value => {
				this.setState(
					{
						selectValue: value,
					},
					() => {
						this.props.setFieldValue(this.props.name, value);
					},
				);
			});
			this.props.setFieldTouched(this.props.name, true, true);
		} else {
			if (this.props.multi) {
				this.setState(
					{
						selectValue: option,
					},
					() => {
						this.props.setFieldValue(
							this.props.name,
							option.map(singleOption => {
								return singleOption.value;
							}),
						);
						this.props.setFieldTouched(this.props.name, true, true);
					},
				);
			} else {
				if (option && option.hasOwnProperty('value')) {
					this.setState(
						{
							selectValue: option,
						},
						() => {
							this.props.setFieldValue(
								this.props.name,
								option.value,
							);
							this.props.setFieldTouched(
								this.props.name,
								true,
								true,
							);
						},
					);
				} else {
					this.setState(
						{
							selectValue: null,
						},
						() => {
							this.props.setFieldValue(this.props.name, null);
							this.props.setFieldTouched(
								this.props.name,
								true,
								true,
							);
						},
					);
				}
			}
		}
	}

	protected getSelectedOption(
		props: IProps,
	): string | number | boolean | (string | number | boolean)[] {
		if (props.name in props.values) {
			if (props.values[props.name] instanceof Array) {
				// Multiple values
				const result = [];
				(props.values[props.name] as string[]).forEach(value => {
					const selectedOption = props.options.find(option => {
						return option.value === value;
					});
					if (selectedOption) {
						result.push(selectedOption.value);
					}
				});
				return result;
			} else {
				// Single value
				const selectedOption = props.options.find(option => {
					return option.value === props.values[props.name];
				});
				if (selectedOption) {
					return selectedOption.value;
				}
			}
		}

		return null;
	}
}
