# Canteen Peers Community Admin
## Local Setup

### Install [Lando](https://docs.devwithlando.io) via Hyperdrive

```sh
curl -Ls https://github.com/lando/hyperdrive/releases/download/v0.5.4/hyperdrive > /tmp/hyperdrive \
  && chmod +x /tmp/hyperdrive \
  && /tmp/hyperdrive
```

### Start Lando

```sh
( cd admin ; lando start)
```

### Development

[Artisan commands can either be run through the container.](https://docs.devwithlando.io/tutorials/laravel.html#tooling)
e.g.

```sh
lando artisan make:migration ...
lando artisan make:model ...
```

Alternatively, ssh into the app server to call these commands directly.

```sh
lando ssh
php artisan make:migration ...
php artisan make:model ...
```

## ssh access

To gain access to staging and production elastic beanstalk ssh, run these commands

```
ssh canteen@bastion.access.canteen.anchor.net.au
```

Password: view7oldest\$cite

Get a list of available instances

```
ec2-list-instances
```

Replace the \$IP with the Ip address of the selected environment above

```
ssh -i ~/.ssh/beanstalk.pem ec2-user@$IP
```

The application is stored at:

```
cd /var/www/html/application
```

## tailing errors in the terminal

Navigate to the errors folder

```
cd /var/log
```

run the tail on the aws errors file

```
tail -f awslogs.log
```

## Connecting to the RDS from elastic beanstalk ssh

```
mysql -h $RDS_HOSTNAME -u $RDS_USERNAME --port=$RDS_PORT --password=$RDS_PASSWORD $RDS_DB_NAME
```

Some DB commands: https://gist.github.com/hofmannsven/9164408

## Deployments

selected 'deploy' or 'master' branch then ./deploy
