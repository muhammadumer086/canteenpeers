<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Situation extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('situation', $this->id, $this->updated_at, true, false)) {
			return $apiStore->getApiResource('situation', $this->id, $this->updated_at, true, false);
		}

		$params = [
			'id',
			'slug',
			'name',
			'cancer_type',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['main_slug'] = $this->slug;
		$result['parent_situation'] = null;
		if ($this->parentSituation) {
			$result['main_slug'] = $this->parentSituation->slug;
			$result['parent_situation'] = new Situation($this->parentSituation);
		}

		$apiStore->setApiResource($result, 'situation', $this->id, $this->updated_at, true, false);

		return $result;
	}
}
