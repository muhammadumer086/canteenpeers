import React from 'react';
import { connect } from 'react-redux';

import { IStoreState, userPanelDisplay } from '../../redux/store';
import { IApiUser } from '../../interfaces';
import { ButtonBase } from '@material-ui/core';

interface IProps {
	users: IApiUser[];
	userData: IApiUser | null;
	groupChat: boolean;
	dispatch(value: any): void;
}

interface IPropsItem {
	user: IApiUser;
	dispatch(value: any): void;
}

const MessageUserListItem: React.StatelessComponent<IPropsItem> = props => {
	const handleUserClick = () => {
		props.dispatch(userPanelDisplay(props.user));
	};

	return (
		<span className="message_user_list-conversation_user">
			<ButtonBase
				className="message_user_list-conversation_user_button"
				onClick={handleUserClick}
			/>
			<span>{props.user.full_name}</span>
		</span>
	);
};

const MessageUserList: React.FC<IProps> = props => {
	const users: IApiUser[] = (JSON.parse(
		JSON.stringify(props.users),
	) as IApiUser[]).filter(user => {
		if (props.userData && user.id !== props.userData.id) {
			return true;
		}
		return false;
	});

	if (!users || users.length === 0) {
		return null;
	}

	const lastUser: IApiUser = users.pop();
	let penultimateUser: IApiUser | null = null;

	// Case for one user
	if (users.length !== 0) {
		penultimateUser = users.pop();
	}

	let conversationName = 'Private chat';
	if (props.groupChat) {
		conversationName = 'Group chat';
	}

	return (
		<React.Fragment>
			<span className="message_user_list-conversation_names">
				{conversationName} with&nbsp;
			</span>
			{users.map(user => {
				return (
					<React.Fragment key={user.username}>
						<MessageUserListItem
							dispatch={props.dispatch}
							user={user}
						/>
						,&nbsp;
					</React.Fragment>
				);
			})}
			{!!penultimateUser && (
				<React.Fragment>
					<MessageUserListItem
						dispatch={props.dispatch}
						user={penultimateUser}
					/>
					<span className="message_user_list-conversation_names">
						&nbsp;and&nbsp;
					</span>
				</React.Fragment>
			)}
			<MessageUserListItem dispatch={props.dispatch} user={lastUser} />
		</React.Fragment>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;

	return {
		userData,
	};
}

export default connect(mapStateToProps)(MessageUserList);
