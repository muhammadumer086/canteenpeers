import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import { TextFieldFormik, SelectFieldFormik } from '../inputs';

import { IStoreState } from '../../redux/store';

import { IApiDiscussion, ISelectOption } from '../../interfaces';
import { EReportModalType } from '../../redux/reducers/reportModal';

interface IProps {
	discussion: IApiDiscussion;
	submitting: boolean;
	userAuthenticated: boolean | null;
	modalType: EReportModalType;
	onSubmit(values: IFormValues): void;
}

interface IState {
	reasons: ISelectOption[];
}

export interface IFormValues {
	reason: string;
	comment: string;
}

class ReportDiscussionForm extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			reasons: this.getReasons(),
		};
	}

	public render() {
		const validationFormat = Yup.object().shape({
			reason: Yup.string()
				.required('A title is required')
				.max(191),
			comment: Yup.string().max(1000),
		});

		return (
			<div>
				<Formik
					initialValues={{
						reason: '',
						comment: '',
					}}
					validationSchema={validationFormat}
					onSubmit={this.props.onSubmit}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};

						return (
							<form className="form" onSubmit={handleSubmit}>
								<div className="form-row">
									<div className="form-column">
										<SelectFieldFormik
											label={this.getTextSelectLabel()}
											name="reason"
											options={this.state.reasons}
											values={inputProps.values.reason}
											noEmpty={true}
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Would you like to add more information? (optional)"
											name="comment"
											multiline={true}
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid ||
												this.props.submitting
											}
										>
											{this.getTextSubmit()}
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}

	protected getReasons(): ISelectOption[] {
		let options: string[] = [];
		switch (this.props.modalType) {
			case EReportModalType.AGE_SENSITIVE: {
				options = [
					'This content is about consensual sexual activity and/or adult sexuality',
					'This content is about age-restricted or illicit activity (eg drinking, smoking, drug use etc)',
					'This content is about age-restricted content (eg MA+ movies or websites)',
				];
				break;
			}
			default: {
				options = [
					'Inappropriate/offensive content',
					'I am concerned this person will harm themselves or someone else',
					'Abusive behaviour',
				];
				break;
			}
		}

		return options.map(value => {
			return {
				label: value,
				value,
			};
		});
	}

	protected getTextSelectLabel() {
		switch (this.props.modalType) {
			case EReportModalType.AGE_SENSITIVE:
				return 'Reason for flagging this post';
			default:
				return 'Reason for reporting this post';
		}
	}

	protected getTextSubmit() {
		switch (this.props.modalType) {
			case EReportModalType.AGE_SENSITIVE:
				return 'Flag Post';
			default:
				return 'Report Post';
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	const { modalType } = state.reportModal;
	return {
		userAuthenticated,
		modalType,
	};
}

export default connect(mapStateToProps)(ReportDiscussionForm);
