import React from 'react';
import { connect } from 'react-redux';

import {
	IStoreState,
	settingsLoad,
	notificationsLoad,
	situationsLoad,
	topicsLoad,
	permissionsLoad
} from '../../redux/store';

import { IApiUser, IApiToken } from '../../interfaces';
import { withServices, AuthService, ApiService } from '../../services';

enum ETimeouts {
	USER = 'USER',
	NOTIFICATIONS = 'NOTIFICATIONS',
	MESSAGES = 'MESSAGES',
}

interface ITimeouts {
	frequency: number;
	timeout: number | null;
	callback(): void;
}

interface IProps {
	userData: IApiUser | null;
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {}

class DataLoader extends React.Component<IProps, IState> {
	protected timeouts = new Map<ETimeouts, ITimeouts>();

	public componentDidMount() {
		const self = this;
		this.timeouts.set(ETimeouts.USER, {
			frequency: 30000,
			timeout: null,
			callback: () => {
				self.props.authService.initialise(true);
			},
		});

		this.timeouts.set(ETimeouts.NOTIFICATIONS, {
			frequency: 20000,
			timeout: null,
			callback: () => {
				self.updateNotifications();
			},
		});

		this.timeouts.set(ETimeouts.MESSAGES, {
			frequency: 10000,
			timeout: null,
			callback: () => {
				// TODO dispatch messages loading
				// TODO if a conversation is active, load the conversation content
			},
		});

		if (typeof window !== 'undefined') {
			window.setTimeout(() => {
				// Call the various timeouts
				if (this.props.userData) {
					self.startAllTimeouts();
				}
				self.updateSettings();
			}, 500);

			document.addEventListener(
				'visibilitychange',
				() => {
					if (document.hidden) {
						this.cancelAllTimeouts();
					} else {
						if (this.props.userData) {
							this.startAllTimeouts();
						}
					}
				},
				false,
			);
		}
	}

	public componentDidUpdate(oldProps: IProps) {
		if (oldProps.userData !== this.props.userData) {
			if (this.props.userData) {
				this.startAllTimeouts();
			} else {
				this.cancelAllTimeouts();
			}
			this.updateSettings();
		}
	}

	public componentWillUnmount() {
		this.cancelAllTimeouts();
	}

	public render() {
		return <React.Fragment />;
	}

	protected startTimeout(timeoutId: ETimeouts) {
		if (
			typeof window !== 'undefined' &&
			this.timeouts.has(timeoutId) &&
			!this.timeouts.get(timeoutId).timeout
		) {
			this.timeouts.get(timeoutId).callback();

			this.timeouts.get(timeoutId).timeout = window.setTimeout(() => {
				this.timeouts.get(timeoutId).timeout = null;
				this.startTimeout(timeoutId);
			}, this.timeouts.get(timeoutId).frequency);
		}
	}

	protected cancelTimeout(timeoutId: ETimeouts) {
		if (typeof window !== 'undefined' && this.timeouts.has(timeoutId)) {
			if (this.timeouts.get(timeoutId).timeout) {
				window.clearTimeout(this.timeouts.get(timeoutId).timeout);
				this.timeouts.get(timeoutId).timeout = null;
			}
		}
	}

	protected startAllTimeouts() {
		this.timeouts.forEach((_timeoutData, timeout) => {
			this.startTimeout(timeout);
		});
	}

	protected cancelAllTimeouts() {
		this.timeouts.forEach((_timeoutData, timeout) => {
			this.cancelTimeout(timeout);
		});
	}

	protected updateSettings() {
		const {userData}=this.props;
		const token: IApiToken | null = this.props.authService.userToken;
		this.props.dispatch(
			settingsLoad(
				this.props.apiService,
				token ? token.access_token : null,
			),
		);
		this.props.dispatch(
			situationsLoad(
				this.props.apiService,
				token ? token.access_token : null,
			),
		);
		this.props.dispatch(
			topicsLoad(
				this.props.apiService,
				token ? token.access_token : null,
			),
		);
		
		if(userData&&userData.role_names&&userData.role_names[0]){
			const role=userData.role_names[0];
			if(role!=="admin"&&role!=="user"){
				this.props.dispatch(
					permissionsLoad(this.props.apiService,role)
				);
			}
			
		}
	}

	protected updateNotifications() {
		const token: IApiToken | null = this.props.authService.userToken;
		this.props.dispatch(
			notificationsLoad(
				this.props.apiService,
				token ? token.access_token : null,
			),
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(withServices(DataLoader));
