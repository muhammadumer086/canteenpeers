import React from 'react';
import { connect } from 'react-redux';

import { ModalDialog } from '../../components/modals/local';

import {
	IStoreState,
	apiError,
	apiSuccess,
	newMessageHide,
} from '../../redux/store';

import { IApiUser } from '../../interfaces';

import { ApiService, withServices } from '../../services';
import NewMessageForm from './NewMessageForm';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';

interface IProps {
	userData: IApiUser | null;
	userMessageData: IApiUser;
	newMessageModalDisplay: boolean;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	isSubmitting: boolean;
}

export interface IFormValues {
	users: string[];
	message: string;
}

class CreateNewMessageModal extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isSubmitting: false,
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleSubmitData = this.handleSubmitData.bind(this);
	}

	public render() {
		const title = `Send a message ${
			this.props.userMessageData
				? `to ${this.props.userMessageData.full_name}`
				: ''
		}`;

		return (
			<ModalDialog
				isActive={this.props.newMessageModalDisplay}
				modalTitle={title}
				ariaTag="new-message-modal"
				handleClose={this.handleClose}
			>
				<NewMessageForm
					onSubmit={this.handleSubmitData}
					submitting={this.state.isSubmitting}
					userData={this.props.userData}
					showUserSelect={this.props.userMessageData}
				/>
			</ModalDialog>
		);
	}

	protected handleClose() {
		this.props.dispatch(newMessageHide());
	}

	protected handleSubmitData(values: IFormValues) {
		this.props.apiService
			.queryPOST(`/api/conversations`, values, 'conversation')
			.then(() => {
				this.setState({
					isSubmitting: false,
				});

				this.GTM.pushEventToDataLayer({
					event: 'messageCTA',
				});

				this.props.dispatch(apiSuccess(['Message has been sent']));
				this.handleClose();
			})
			.catch(error => {
				this.setState({
					isSubmitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;

	const { newMessageModalDisplay, userMessageData } = state.newMessageModal;
	return {
		userData,
		userMessageData,
		newMessageModalDisplay,
	};
}

export default connect(mapStateToProps)(withServices(CreateNewMessageModal));
