<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

use App\AgeRange;

class AgeRangeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$ageRanges = [
			[
				'slug' => '12-14',
				'name' => '12 - 14',
				'min'  => 12,
				'max'  => 14
			],
			[
				'slug' => '15-17',
				'name' => '15 - 17',
				'min'  => 15,
				'max'  => 17
			],
			[
				'slug' => '18-20',
				'name' => '18 - 20',
				'min'  => 18,
				'max'  => 20
			],
			[
				'slug' => '21-25',
				'name' => '21 - 25',
				'min'  => 21,
				'max'  => 25
			],
		];

		$now = Carbon::now();

		foreach ($ageRanges as $ageRange) {
			$ageRange = AgeRange::updateOrCreate([
				'slug' => $ageRange['slug'],
			], [
				'name' => $ageRange['name'],
				'min'  => $ageRange['min'],
				'max'  => $ageRange['max']
			]);
			$ageRange->touch();
		}

		// Delete all the age ranges that haven't been updated
		AgeRange::where('updated_at', '<', $now)->orWhereNull('updated_at')->delete();
	}
}
