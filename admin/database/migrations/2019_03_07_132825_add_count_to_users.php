<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountToUsers extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('users', function (Blueprint $table) {
			$table->unsignedInteger('count_logins')->default(0);
			$table->unsignedInteger('count_messages')->default(0);
			$table->unsignedInteger('count_active_discussions')->default(0);
			$table->unsignedInteger('count_saved_resources')->default(0);
			$table->unsignedInteger('count_followed_discussions')->default(0);
			$table->unsignedInteger('count_likes')->default(0);
			$table->unsignedInteger('count_blogs')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('count_logins');
			$table->dropColumn('count_messages');
			$table->dropColumn('count_active_discussions');
			$table->dropColumn('count_saved_resources');
			$table->dropColumn('count_followed_discussions');
			$table->dropColumn('count_likes');
			$table->dropColumn('count_blogs');
		});
	}
}
