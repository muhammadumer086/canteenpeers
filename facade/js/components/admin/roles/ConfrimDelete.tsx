import React from "react";
import { ModalDialog } from '../../modals/local/ModalDialog';
import ModalDialogDeleteConfirm from '../../modals/ModalDialogDeleteConfirm';
import {IApiRole} from "../../../interfaces";

interface IProps {
    isLoading: boolean,
    role : IApiRole,
    toggleDeleteModal(): void;
    submitDeleteRole(): void;
};
interface IState {

};
class ConfirmDelete extends React.Component<IProps, IState>{
    constructor(p: IProps) {
        super(p);
    }
    public render() {
        return (
            <ModalDialog
                isActive={true}
                modalTitle="Are you sure you want to delete this Role?"
                ariaTag="event-delete-modal"
                handleClose={this.props.toggleDeleteModal}
            >
                <ModalDialogDeleteConfirm
                    name={this.props.role.name}
                    loading={this.props.isLoading}
                    submit={this.props.submitDeleteRole}
                    cancel={this.props.toggleDeleteModal}
                />
            </ModalDialog>
        );
    }
}
export default ConfirmDelete;