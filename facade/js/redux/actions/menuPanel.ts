import { IStoreState } from '../store';
import { actionTypes } from '../actions';

export const menuPanelDisplay = () => (dispatch, getState: () => IStoreState) => {
	if (getState().menuPanel.menuPanelDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.MENU_PANEL_DISPLAY,
	});
}

export const menuPanelHide = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().menuPanel.menuPanelDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.MENU_PANEL_HIDE,
	});
}
