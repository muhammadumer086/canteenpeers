import React from 'react';
import { connect } from 'react-redux';
import Dotdotdot from 'react-dotdotdot';

import { IApiBlog, IApiUser } from '../../interfaces';
import TileTitle from '../general/TileTitle';
import TileAuthor from '../general/TileAuthor';

import { ButtonBaseLink } from '../general/ButtonBaseLink';
import { getExcerpt } from '../../helpers/getExcerpt';
import { IStoreState } from '../../redux/store';

interface IProps {
	blogData: IApiBlog;
	blogAuthor: IApiUser;
	disabled: boolean;
	disabledAuthorButton?: boolean;
	gtm?(title: string): void;
	dispatch(action: any): void;
}

export const defaultBlogImagesMap = {
	'aotearoa-new-zealand': 'aotearoa-new-zealand.jpg',
	'cancer-stuff': 'cancer-stuff.jpg',
	family: 'family.jpg',
	fertility: 'fertility.jpg',
	friends: 'friends.jpg',
	'grief-bereavement': 'grief-bereavement.jpg',
	lgbtqi: 'lgbtqi.jpg',
	'life-after-treatment': 'life-after-treatment.jpg',
	'my-cancer': 'my-cancer.jpg',
	'my-diagnosis': 'my-diagnosis.jpg',
	'my-parents-cancer': 'my-parents-cancer.jpg',
	'my-siblings-cancer': 'my-siblings-cancer.jpg',
	'my-treatment': 'my-treatment.jpg',
	'practical-tips': 'practical-tips.jpg',
	'rare-cancers': 'rare-cancers.jpg',
	'study-and-work': 'study-and-work.jpg',
	'wellbeing-and-self-care': 'wellbeing-and-self-care.jpg',
};

const BlogTile: React.StatelessComponent<IProps> = (props: IProps) => {
	const classNames = ['general_tile', 'theme--main'];

	const excerpt = getExcerpt(props.blogData.content);

	if (props.disabled) {
		classNames.push('general_tile--disabled');
	}

	let imageStyles = {};

	if (props.blogData.feature_image) {
		imageStyles = {
			backgroundImage: `url(${props.blogData.feature_image.url})`,
		};
	} else if (
		props.blogData.topic &&
		props.blogData.topic.slug in defaultBlogImagesMap
	) {
		imageStyles = {
			backgroundImage: `url(${
				process.env.STATIC_PATH
			}/images/blog-default/${
				defaultBlogImagesMap[props.blogData.topic.slug]
			})`,
		};
	}

	return (
		<div className={classNames.join(' ')}>
			{props.gtm ? (
				<ButtonBaseLink
					onClick={() => {
						props.gtm(props.blogData.title);
					}}
					className="general_tile-button"
					hrefAs={`/blogs/${props.blogData.slug}`}
					href={`/blogs-single?id=${props.blogData.slug}`}
					title={props.blogData.title}
				/>
			) : (
				<ButtonBaseLink
					className="general_tile-button"
					hrefAs={`/blogs/${props.blogData.slug}`}
					href={`/blogs-single?id=${props.blogData.slug}`}
					title={props.blogData.title}
				/>
			)}

			<div className="general_tile-image" style={imageStyles} />
			<div className="general_tile-content theme--main">
				{props.blogData.topic && (
					<div className="general_tile-topic hm-b8">
						<ButtonBaseLink
							className="general_tile-topic_link"
							href={`/blogs?topic=${props.blogData.topic.slug}`}
							title={props.blogData.topic.title}
							disableRipple={true}
						>
							{props.blogData.topic.title}
						</ButtonBaseLink>
					</div>
				)}

				<div className="general_tile-title">
					<TileTitle content={props.blogData.title} clamp={false} />
				</div>
				<span className="general_tile-message">
					<Dotdotdot clamp={1}>
						<span className="general_tile-text">{excerpt}</span>
					</Dotdotdot>
				</span>

				<div className="general_tile-author">
					<TileAuthor
						userData={props.blogAuthor}
						blogData={props.blogData}
					/>
				</div>
			</div>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(BlogTile);
