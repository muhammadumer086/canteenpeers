<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NotificationCollection extends ResourceCollection {
	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		// Filter all the empty notifications
		return ['notifications' => $this->filterEmptyNotifications()];
	}

	protected function filterEmptyNotifications() {
		$filtered = $this->collection->filter(function ($element) {
			return $element->shouldDisplayNotification();
		});
		return collect(array_values($filtered->all()));
	}
}
