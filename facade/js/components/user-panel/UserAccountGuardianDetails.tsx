import React from 'react';

import { Formik } from 'formik';

import ButtonBase from '@material-ui/core/ButtonBase';

import Icon from '@material-ui/core/Icon';

import { IApiUser } from '../../interfaces';
import { EAccountScreens, ETabs } from './UserPanelAccount';

import {
	TextFieldFormik,
} from '../inputs';

interface IProps {
	userData: IApiUser | null;
	submitting: boolean;
	changeScreen(tab: ETabs, screen: EAccountScreens);
	onSubmit(values: any, resetForm?: () => void);
}

const UserAccountGuardianDetails: React.FC<IProps> = (props: IProps) => {
	return (
		<div className="user_account_panel user_account_panel--account_guardian_details">
			<div className="user_panel-back">
				<ButtonBase
					focusRipple={true}
					className="user_panel-back-button"
					onClick={() => {
						props.changeScreen(
							ETabs.MY_PROFILE,
							EAccountScreens.OVERVIEW,
						);
					}}
					disabled={props.submitting}
				>
					<Icon className="user_panel-back-back_icon">
						arrow_back
					</Icon>
					Profile Overview
				</ButtonBase>
			</div>
			<div className="user_panel-heading">
				<h4 className="font--h4 theme-title">Guardian Details</h4>
			</div>
			<Formik
				initialValues={{
					email: props.userData.guardian_details.email,
					name: props.userData.guardian_details.name
				}}
				onSubmit={() => {return false}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
						isValid,
					};

					return (
						<form className="form">

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="email"
										label="Email"
										name="email"
										disabled={true}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="name"
										label="Name"
										name="name"
										disabled={true}
										{...inputProps}
									/>
								</div>
							</div>

						</form>
					);
				}}
			/>
		</div>
	);
};

export default UserAccountGuardianDetails;
