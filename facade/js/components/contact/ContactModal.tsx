import React       from 'react';
import { connect } from 'react-redux';

import Modal        from '@material-ui/core/Modal';
import Paper        from '@material-ui/core/Paper';

import ClearIcon    from '@material-ui/icons/Clear';

import {
	IStoreState,
	contactModalHide,
	apiError,
	apiSuccess,
} from '../../redux/store';

import ContactForm from './ContactForm';
import { IApiUser } from '../../interfaces';
import { ApiService, withServices } from '../../services';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';

interface IProps {
	userData: IApiUser|null;
	contactModalDisplay: boolean;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	isSubmitting: boolean;
}

class ContactModal extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isSubmitting: false,
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleSubmitData = this.handleSubmitData.bind(this);
	}

	public render() {
		return (
			<Modal
				aria-labelledby="contact-modal"
				open={this.props.contactModalDisplay}
				onClose={this.handleClose}
			>
				<Paper className="modal theme--main" square={true}>
					<button onClick={this.handleClose} className="close_button close_button--primary" title="Dismiss">
						<ClearIcon className="close_button-icon"/>
					</button>
					<div className="modal-content_container">
						<div className="modal-content discussion_modal">
							<h2 className="modal-content_title font--h4 theme-title hm-t16">How Can We Help</h2>

							<ContactForm
								onSubmit={this.handleSubmitData}
								submitting={this.state.isSubmitting}
							/>
						</div>
					</div>
				</Paper>
			</Modal>
		);
	}

	protected handleClose() {
		this.props.dispatch(contactModalHide());
	}

	protected handleSubmitData(values) {
		this.setState({
			isSubmitting: true,
		}, () => {
			this.props.apiService.queryPOST('/api/contact', values)
			.then(() => {
				this.setState({
					isSubmitting: false,
				});

				this.GTM.pushEventToDataLayer({
					event: 'contactFormSubmission',
				})

				this.props.dispatch(contactModalHide());
				this.props.dispatch(apiSuccess([
					'Message successfully sent',
				]));
			})
			.catch((error) => {
				this.setState({
					isSubmitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([
					error.message
				]));
			});
		})
	}

}

function mapStateToProps(state: IStoreState) {
	const {
		userData,
	} = state.user;

	const {
		contactModalDisplay,
	} = state.contactModal;
	return {
		userData,
		contactModalDisplay,
	};
}

export default connect(mapStateToProps)(withServices(ContactModal));
