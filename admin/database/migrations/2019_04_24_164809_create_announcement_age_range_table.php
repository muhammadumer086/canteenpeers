<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementAgeRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('age_range_announcement', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('age_range_id');
            $table->foreign('age_range_id')
                  ->references('id')->on('age_ranges');

            $table->unsignedInteger('announcement_id');
            $table->foreign('announcement_id')
                  ->references('id')->on('announcements')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('age_range_announcement');
    }
}
