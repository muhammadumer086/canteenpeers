import { actionTypes } from '../actions';

export interface IStoreState {
	searchModalDisplay: boolean;
}

export const searchModal = (state: IStoreState = {
	searchModalDisplay: false,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.SEARCH_MODAL_DISPLAY:
			return Object.assign({}, state, {
				searchModalDisplay: true
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.SEARCH_MODAL_HIDE:
			return Object.assign({}, state, {
				searchModalDisplay: false
			});
		default:
			return state;
	}
}
