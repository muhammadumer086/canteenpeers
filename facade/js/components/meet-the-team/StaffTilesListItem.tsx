import React from 'react';

import ButtonBase from '@material-ui/core/ButtonBase';
import { Avatar } from '@material-ui/core';

import { IApiStaff } from '../../../js/interfaces';

interface IProps {
	data: IApiStaff;
	onClick(activeStaffMember: IApiStaff): void;
}

const StaffTilesListItem: React.StatelessComponent<IProps> = props => {
	return (
		<div className="staff_tile">
			<ButtonBase
				focusRipple={true}
				className="staff_tile-action"
				onClick={() => {
					props.onClick(props.data);
				}}
			/>

			<div className="staff_tile-avatar_container">
				<Avatar
					className="staff_tile-avatar"
					src={props.data.feature_image&&props.data.feature_image.url}
				/>
			</div>

			<div className="staff_tile-info">
				<span className="staff_tile-name">
					{props.data.first_name} {props.data.last_name}
				</span>

				<span className="staff_tile-role">{props.data.role}</span>
			</div>
		</div>
	);
};

export default StaffTilesListItem;
