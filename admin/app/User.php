<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Http\File;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;

use Spatie\Permission\Traits\HasRoles;

use App\Filters\User\UserFilters;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\DiscussionReplyNotification;
use App\Notifications\DiscussionReportNotification;
use App\Notifications\NewAnnouncementNotification;
use App\Notifications\UserReportNotification;
use App\Notifications\WelcomeNotification;
use App\Notifications\MigrationNotification;
use App\Notifications\UserResendVerificationdNotification;
use App\Notifications\MessagesNotification;

use App\Http\Controllers\API\SettingsController;

use App\Traits\AlgoliaIndex;

use App\Event;
use App\PersonalSetting;

use Image;

class User extends Authenticatable {
	use HasApiTokens;
	use HasRoles;
	use Notifiable;
	use Searchable;
	use SoftDeletes;
	use AlgoliaIndex;

	protected static function boot() {
		parent::boot();

		static::deleting(function ($user) {
			$user->inactive = true;
			$user->save();
		});
	}

	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName() {
		return 'username';
	}

	public function findForPassport($identifier) {
		return $this->orWhere('email', $identifier)
			->orWhere('phone', $identifier)
			->orWhere('username', $identifier)
			->first();
	}

	public function scopeFilter(
		Builder $builder,
		$request,
		array $filters = []
	) {
		return (new UserFilters($request))->add($filters)->filter($builder);
	}

	/**
	 * Get the index name for the model.
	 *
	 * @return string
	 */
	public function searchableAs() {
		return $this->algoliaIndex();
	}

	/**
	 * Get the indexable data array for the model.
	 *
	 * @return array
	 */
	public function toSearchableArray() {
		if (!defined('ALGOLIA_PUBLIC')) {
			$type = Str::slug((new \ReflectionClass($this))->getShortName());
			$id = $this->username;
			$image = $this->avatar_url;
			$title = $this->full_name;
			$excerpt = $this->description;
			$groups = [];
			$data = "{$this->username} | {$this->full_name}";

			if ($this->is_banned) {
				$image = null;
				$title = null;
				$excerpt = null;
				$data = null;
			}

			// Check if under 16
			if ($this->underage) {
				$groups[] = 'underage';
			} else {
				$groups[] = 'ofage';
			}
			if ($this->hasRoleName('admin')) {
				$groups[] = 'admin';
			}

			// Result structure
			$result = [
				'type' => $type,
				'id' => $id,
				'image' => $image,
				'title' => $title,
				'excerpt' => $excerpt,
				'data' => $data,
				'groups' => $groups,
			];

			return $result;
		}

		return [];
	}

	/**
	 * Get the value used to index the model.
	 *
	 * @return mixed
	 */
	public function getScoutKey() {
		return $this->algolia_id;
	}

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'dob','email_verified_at', 'mobile_verified_at'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'indigenous_australian' => 'boolean',
		'location' => 'string',
		'location_data' => 'array',
		'dob' => 'datetime:Y-m-d',
		'verified' => 'boolean',
		'is_mobile_verified' => 'boolean',
		'gender' => 'string',
		'phone' => 'string',
		'about' => 'string',
		'inactive' => 'boolean',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username',
		'first_name',
		'last_name',
		'email',
		'password',
		'inactive',
		'created_at',
		'migration_id',
		'gender',
		'dob',
		'phone',
		'location',
		'location_data',
		'latitude',
		'longitude',
		'indigenous_australian',
		'state',
		'verified',
		'verification_token',
		'about',
		'migrated',
		'preferences',
		'role_names',
		'country_of_birth',
		'registration_origin',
		'is_graduate',
		'mobile_verification_code',
		'is_mobile_verified',
		'mobile_verified_at',
		'email_verified_at',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'id',
		'password',
		'remember_token',
		'created_at',
		'updated_at',
		'first_name',
		'last_name',
		'full_name',
		'location',
		'location_data',
		'phone',
		'dob',
		'last_login',
		'last_login_ip',
		'deleted_at',
		'verified',
		'verification_token',
		'country_of_birth',
		'indigenous_australian',
		'role_names',
		'roles',
		'ban',
		'is_banned',
		'blogs_count',
		'active_discussions_count',
		'likes_count',
		'hugs_count',
		'following_count',
		'saved_resources_count',
		'messages_count',
		'is_deleted',
		'avatar',
		'is_graduate',
		'mobile_verification_code',
		'is_mobile_verified',
		'mobile_verified_at',
		'email_verified_at',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = [
		'full_name',
		'avatar_url',
		'role_names',
		'is_banned',
		'blogs_count',
		'active_discussions_count',
		'likes_count',
		'hugs_count',
		'following_count',
		'saved_resources_count',
		'messages_count',
		'is_deleted',
		'intercom_hash',
		'guardian_details',
		'age',
		'underage',
		'is_graduate',
		'is_mobile_verified',
		'mobile_verified_at',
		'email_verified_at',
	];

	/**
	 * Specifies the user's FCM token
	 *
	 * @return string
	 */
	public function routeNotificationForFcm() {
		return $this->fcm_token;
	}

	public function getEmailVerifiedAtAttribute()
	{
		return isset($this->attributes['email_verified_at'])?$this->attributes['email_verified_at']:'';
	}

	public function getUpdatedAtAttribute()
	{
		$updatedATF = $this->attributes['updated_at'];
		if(isset($updatedATF) && is_string($updatedATF))
		{
			return Carbon::parse(date($updatedATF));
		}
		return '';
	}

	public function reportNumbers() {
		$this->count_active_discussions = $this->activeDiscussions()->count();

		$this->count_blogs = $this->blogs()->count();

		$this->count_likes = $this->likes()->count();
		$this->count_hugs = $this->hugs()->count();

		$this->count_followed_discussions = $this->followedDiscussions()->count();

		$this->count_messages = $this->messages()->count();

		$this->count_saved_resources = $this->savedResources()->count();

		$this->count_logins = $this->userActivities()
			->where('type', 'User Login')
			->count();
	}

	public function generatePreferences() {
		$this->preferences = implode(
			',',
			$this->personalSettings()
				->pluck('slug')
				->toArray()
		);
	}

	public function generateRoles() {
		$this->role_names = implode(',', $this->getRoleNames()->toArray());
	}

	public function getAge() {
		if ($this->dob && !$this->hasRoleName('admin')) {
			$age = Carbon::now()->diffInYears($this->dob);

			return $age;
		}
		return null;
	}

	public function isUnderage() {
		$age = $this->getAge();
		if ($age && $age < 16) {
			return true;
		} else {
			return false;
		}
	}

	public function hasConversations() {
		return count($this->conversations) ? true : false;
	}

	public function canMessage($authUser = null) {
		if ($authUser && $authUser->id !== $this->id) {
			if (
				$authUser->hasRoleName('admin') ||
				(!$this->isUnderage() &&
					$this->allowDirectMessage() &&
					!$authUser->isUnderage() &&
					!$this->isBlocked() &&
					!$this->blockedUsers()
						->where('blocked_user_id', $authUser->id)
						->exists())
			) {
				return true;
			}
		}

		return false;
	}

	public function isBlocked($authUser = null) {
		if (
			$authUser &&
			$authUser->id !== $this->id &&
			$authUser
				->blockedUsers()
				->where('blocked_user_id', $this->id)
				->exists()
		) {
			return true;
		}

		return false;
	}

	public function getFullNameAttribute() {
		return $this->username;
	}

	public function getAvatarUrlAttribute() {
		if ($this->avatar) {
			return Storage::disk('s3')->url($this->avatar);
		}
		$params = [
			'd' =>
				'https://s3-ap-southeast-2.amazonaws.com/assets.parents.canteen.org.au/avatars/default_avatar.png',
			's' => 200,
		];
		return 'https://www.gravatar.com/avatar/' .
			md5(strtolower(trim($this->email))) .
			'?' .
			http_build_query($params);
	}

	public function getIsDeletedAttribute() {
		return $this->trashed();
	}

	public function getIsBannedAttribute() {
		return !is_null($this->ban);
	}

	public function getActiveDiscussionsCountAttribute() {
		return $this->count_active_discussions;
	}

	public function getBlogsCountAttribute() {
		return $this->count_blogs;
	}

	public function getLikesCountAttribute() {
		return $this->count_likes;
	}

	public function getHugsCountAttribute() {
		return $this->count_hugs;
	}

	public function getFollowingCountAttribute() {
		return $this->count_followed_discussions;
	}

	public function getMessagesCountAttribute() {
		return $this->count_messages;
	}

	public function getSavedResourcesCountAttribute() {
		return $this->count_saved_resources;
	}

	public function getTotalLoginsAttribute() {
		return $this->count_logins;
	}

	public function getAlgoliaIdAttribute() {
		$type = Str::slug((new \ReflectionClass($this))->getShortName());
		return "$type/{$this->id}";
	}

	public function getIntercomHashAttribute() {
		return hash_hmac('sha256', $this->id, config('canteen.intercom_hash'));
	}

	public function getPreferencesAttribute($value) {
		return explode(',', $value);
	}

	public function getRoleNamesAttribute($value) {
		return explode(',', $value);
	}

	public function getAgeAttribute() {
		return $this->getAge();
	}

	public function getUnderageAttribute() {
		return $this->isUnderage();
	}

	public function getGuardianDetailsAttribute() {
		if ($this->parent_name && $this->parent_email && $this->isUnderage()) {
			return [
				'name' => $this->parent_name,
				'email' => $this->parent_email,
			];
		}

		return null;
	}

	/**
	 * The discussions the user participated to
	 */
	public function activeDiscussions() {
		return $this->belongsToMany('App\Discussion', 'users_in_discussion');
	}

	/**
	 * The situations the user belongs to
	 */
	public function situations() {
		return $this->belongsToMany(
			'App\Situation',
			'situation_user',
			'user_id',
			'situation_id'
		);
	}

	/**
	 * The discussions the user authored
	 */
	public function discussions() {
		return $this->hasMany('App\Discussion');
	}

	/**
	 * The discussions reported by the user
	 */
	public function reportDiscussions() {
		return $this->hasMany('App\ReportDiscussion');
	}

	/**
	 * The reported discussions reviewed by the user
	 */
	public function reviewedReportDiscussions() {
		return $this->hasMany('App\ReportDiscussion', 'reviewer_id');
	}

	/**
	 * The discussions the user authored
	 */
	public function userActivities() {
		return $this->hasMany('App\UserActivity');
	}

	/**
	 * The likes associated with the user
	 */
	public function likes() {
		return $this->hasMany('App\Like');
	}

	/**
	 * The hugs associated with the user
	 */
	public function hugs() {
		return $this->hasMany('App\Hug');
	}

	public function discussionLikes() {
		return $this->belongsToMany('App\Discussion', 'likes');
	}

	public function discussionHugs() {
		return $this->belongsToMany('App\Discussion', 'hugs');
	}

	/**
	 * The notification cooldowns
	 */
	public function notificationMessageCooldowns() {
		return $this->hasMany('App\NotificationMessageCooldown');
	}

	/**
	 * The following associated with the user/discusions
	 */
	public function followedDiscussions() {
		return $this->belongsToMany(
			'App\Discussion',
			'followings',
			'user_id',
			'discussion_id'
		);
	}

	/**
	 * The like associated with the user/discusions
	 */
	public function likedDiscussions() {
		return $this->belongsToMany(
			'App\Discussion',
			'likes',
			'user_id',
			'discussion_id'
		);
	}

	/**
	 * The hugs associated with the user/discusions
	 */
	public function huggedDiscussions() {
		return $this->belongsToMany(
			'App\Discussion',
			'hugs',
			'user_id',
			'discussion_id'
		);
	}

	/**
	 * The saved resources associated with the user/resources
	 */
	public function savedResources() {
		return $this->belongsToMany(
			'App\Resource',
			'saved_resources',
			'user_id',
			'resource_id'
		);
	}

	/**
	 * The settings associated with the user
	 */
	public function personalSettings() {
		return $this->belongsToMany('App\PersonalSetting');
	}

	/**
	 * The reasons for leaving the community
	 */
	public function leavingReasons() {
		return $this->hasMany('App\LeavingReason');
	}

	/**
	 * The languages associated with the user
	 */
	public function messages() {
		return $this->hasMany('App\Message');
	}

	/**
	 * The users part of the conversation
	 */
	public function conversations() {
		return $this->belongsToMany('App\Conversation');
	}

	/**
	 * The blogs the user authored
	 */
	public function blogs() {
		return $this->hasMany('App\Blog');
	}

	/**
	 * The Resources the users have saved
	 */
	public function saveResources() {
		return $this->belongsToMany('App\Resource', 'saved_resources');
	}

	/**
	 * The blogs the user authored
	 */
	public function contacts() {
		return $this->hasMany('App\Contact');
	}

	/**
	 * The languages associated with the user
	 */
	public function cancers() {
		return $this->hasMany('App\Cancer');
	}

	public function announcements() {
		return $this->belongsToMany(
			'App\Announcement',
			'announcement_user',
			'user_id',
			'announcement_id'
		);
	}

	/**
	 * The country of birth with the user
	 */
	public function countryOfBirth() {
		return $this->belongsTo('App\Country', 'country_of_birth');
	}

	/**
	 * The languages associated with the user
	 */
	public function languages() {
		return $this->belongsToMany('App\Language');
	}

	public function heardAbouts() {
		return $this->belongsToMany('App\HeardAbout');
	}

	public function events() {
		return $this->belongsToMany(Event::class);
	}

	public function blockedUsers() {
		return $this->belongsToMany(
			$this,
			'users_blocked',
			'user_id',
			'blocked_user_id'
		)->withTimestamps();
	}

	/**
	 * Temporarily display the user data for serialisation
	 */
	public function makeVisibleUserData() {
		$visible = [
			'email',
			'first_name',
			'last_name',
			'location',
			'phone',
			'dob',
			'verified',
			'is_mobile_verified',
			'country_of_birth',
			'indigenous_australian',
			'role_names',
			'ban',
			'blogs_count',
			'active_discussions_count',
			'likes_count',
			'hugs_count',
			'following_count',
			'saved_resources_count',
			'messages_count',
			'is_deleted',
		];
		foreach ($visible as $attribute) {
			$this->makeVisible($attribute);
		}
	}

	/**
	 * Send the welcome notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendWelcomeNotification() {
		$this->notify(new WelcomeNotification($this));
	}

	/**
	 * Send the migration notification
	 */
	public function sendMigrationNotification($token) {
		$this->notify(new MigrationNotification($token, $this));
	}

	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token) {
		$this->notify(new ResetPasswordNotification($token, $this));
	}

	/**
	 * Send the reported discussion notification.
	 *
	 * @return void
	 */
	public function sendDiscussionReportNotification(
		$discussion,
		$author,
		$ageSensitive
	) {
		$this->notify(
			new DiscussionReportNotification(
				$discussion,
				$this,
				$author,
				$ageSensitive
			)
		);
	}

	/**
	 * Send the reported user notification.
	 *
	 * @return void
	 */
	public function sendUserReportNotification(
		$reportedUser,
		$author,
		$reportReason,
		$reportInformation
	) {
		$this->notify(
			new UserReportNotification(
				$this,
				$reportedUser,
				$author,
				$reportReason,
				$reportInformation
			)
		);
	}

	/**
	 * Send the discussion reply notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendDiscussionReplyNotification(
		$reply,
		$discussion,
		$mainDiscussion,
		$author
	) {
		if ($this->id !== $author->id) {
			$followed = $discussion->user_id !== $this->id;
			$this->notify(
				new DiscussionReplyNotification(
					$reply,
					$discussion,
					$mainDiscussion,
					$this,
					$author,
					$followed
				)
			);
		}
	}

	/**
	 * Send the announcement notification
	 */
	public function sendAnnouncementNotification($announcement) {
		$this->notify(new NewAnnouncementNotification($announcement, $this));
	}

	/**
	 * Re-send the activation email
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function resendVerificationNotification() {
		$this->notify(new UserResendVerificationdNotification($this));
	}

	/**
	 * Send the messages notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendMessagesNotification() {
		$this->notify(new MessagesNotification($this));
	}

	public function allowPublicBlogPosts() {
		return $this->hasPersonalSettingEnabled(PersonalSetting::$BLOG_PUBLIC);
	}

	public function allowDisplayCancerType() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_DISPLAY_CANCER_TYPE
		);
	}

	public function allowDirectMessage() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_DIRECT_MESSAGE
		);
	}

	public function allowDisplayGender() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_DISPLAY_GENDER
		);
	}

	public function allowNotificationDiscussionReply() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_NOTIF_DISCUSSION_REPLIES
		);
	}

	public function allowPushNotificationDiscussionReply() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_PUSH_NOTIF_DISCUSSION_REPLIES
		);
	}

	public function allowEmailDiscussionReply() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_EMAIL_DISCUSSION_REPLIES
		);
	}

	public function allowNotificationDiscussionFollowed() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_NOTIF_DISCUSSION_FOLLOWED
		);
	}

	public function allowPushNotificationDiscussionFollowed() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_PUSH_NOTIF_DISCUSSION_FOLLOWED
		);
	}

	public function allowEmailDiscussionFollowed() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_EMAIL_DISCUSSION_FOLLOWED
		);
	}

	public function allowNotificationMentioned() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_NOTIF_DISCUSSION_MENTION
		);
	}

	public function allowPushNotificationMentioned() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_PUSH_NOTIF_DISCUSSION_MENTION
		);
	}

	public function allowEmailMentioned() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_EMAIL_DISCUSSION_MENTION
		);
	}

	public function allowNotificationNewBlog() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_NOTIF_BLOGS
		);
	}

	public function allowEmailMessages() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_EMAIL_MESSAGES
		);
	}

	public function allowPushNotificationMessages() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_PUSH_NOTIF_MESSAGES
		);
	}

	public function allowNotificationNewAnnouncement() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_NOTIF_ALERTS
		);
	}

	public function allowPushNotificationNewAnnouncement() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_PUSH_NOTIF_ALERTS
		);
	}

	public function allowEmailNewAnnouncement() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_EMAIL_ALERTS
		);
	}

	public function allowResearchContact() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_CONTACTED_RESEARCH
		);
	}

	public function allowNews() {
		return $this->hasPersonalSettingEnabled(
			PersonalSetting::$ALLOW_NEWSLETTER
		);
	}

	public function hasRoleName(string $roleName) {
		if (is_array($this->role_names)) {
			return in_array($roleName, $this->role_names);
		}
		return false;
	}

	/**
	 * Check if the user has a specific personal setting enabled
	 *
	 * @param string the setting name
	 * @return boolean
	 */
	protected function hasPersonalSettingEnabled(string $settingName) {
		if (is_array($this->preferences)) {
			return in_array($settingName, $this->preferences);
		}
		return false;
	}

	public function processUserAvatar($file) {
		$oldPath = $this->avatar;

		$image = Image::make($file)
			->orientate()
			->fit(240, 240);
		$hash = md5($image->__toString());
		$tmpPath = storage_path("images/{$hash}.jpg");
		$image->save($tmpPath, 70);
		$image->destroy();

		// Upload the file to S3
		$path = Storage::disk('s3')->putFile(
			'avatars',
			new File($tmpPath),
			'public'
		);

		// Save the user's avatar
		$this->avatar = $path;
		$this->save();

		// Handle delete of old file
		if ($oldPath) {
			Storage::disk('s3')->delete($oldPath);
		}
		// Delete the uploaded image
		unlink($tmpPath);
	}
}
