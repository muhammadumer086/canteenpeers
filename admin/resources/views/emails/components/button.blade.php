<tr>
	<td style="padding: 20px 0;">
		<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
			<tbody>
				<tr>
					<td class="button-td button-td-primary" style="border-radius: 30px; background: #FF5F33;">
						<a class="button-a button-a-primary" href="{{ $url }}" style="background: #FF5F33; border: 1px solid #FF5F33; font-family: sans-serif; font-size: 14px; line-height: 14px; text-decoration: none; padding: 22px 30px; color: #ffffff; display: block; border-radius: 30px; text-transform: uppercase;">
							{{ $slot }}
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>
