import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { IStoreState } from '../../../js/redux/store';

import { TextFieldFormik } from '../inputs';
import { IApiUser } from '../../../js/interfaces';
import { validationsUser } from '../../../js/helpers/validations';

interface IProps {
	userData: IApiUser | null;
	isSubmitting: boolean;
	onSubmit(value: IFormValues): void;
}

export interface IFormValues {
	phone: string;
	content: string;
}

const EventRegisterInterestFormik: React.StatelessComponent<IProps> = props => {
	const validationFormat = Yup.object().shape({
		phone: validationsUser.phone,
		content: Yup.string()
			.required()
			.max(1600),
	});

	return (
		<Formik
			initialValues={{
				phone: props.userData.phone || '',
				content: '',
			}}
			validationSchema={validationFormat}
			onSubmit={props.onSubmit}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				handleSubmit,
				setFieldValue,
				setFieldTouched,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
				};

				return (
					<form
						className="create_discussion_form form"
						onSubmit={handleSubmit}
					>
						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="tel"
									label="Phone"
									name="phone"
									disabled={props.isSubmitting}
									{...inputProps}
								/>
							</div>
						</div>
						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="Why I want to attend this event"
									name="content"
									disabled={props.isSubmitting}
									multiline={true}
									{...inputProps}
								/>
							</div>
						</div>

						<div className="form-row form-row--submit">
							<div className="form-column">
								<Button
									variant="outlined"
									color="primary"
									className="form-submit"
									type="submit"
									disabled={!isValid || props.isSubmitting}
								>
									Get in Touch
								</Button>
							</div>
						</div>
					</form>
				);
			}}
		/>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(EventRegisterInterestFormik);
