import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {IApiRole} from "../../../interfaces";

interface IProps{
	handleEdit(role?:IApiRole):void;
	handleDelete(role?:IApiRole):void;
	handleManagePermissions(roleName?:IApiRole):void;
    role : IApiRole;
}

interface IState {
	anchorEl: any | null;
}
class RoleOptions extends React.Component<IProps,IState>{
    constructor(props : IProps){
        super(props);
        this.state={
            anchorEl : null,
        }
        this.editRole=this.editRole.bind(this);
        this.deleteRole=this.deleteRole.bind(this);
        this.manageRole=this.manageRole.bind(this);
        this.handleMenuOpen=this.handleMenuOpen.bind(this);
        this.handleMenuClose=this.handleMenuClose.bind(this);
    }
    public render(){
        return(
            <div>
                	<IconButton
					className="discussion_report-button"
					disabled={this.props.role.name==="admin"||this.props.role.name==="user"}
					onClick={this.handleMenuOpen}
				>
					<Icon>more_vert</Icon>
				</IconButton>
				<Menu
					anchorEl={this.state.anchorEl}
					open={Boolean(this.state.anchorEl)}
					onClose={this.handleMenuClose}
				>
                    	<MenuItem onClick={this.editRole}>
							Edit Role
						</MenuItem>
                        <MenuItem onClick={this.manageRole}>
							Manage Permissions
						</MenuItem>
                        <MenuItem onClick={this.deleteRole}>
							Delete Role
						</MenuItem>
				</Menu>
            </div>
        );
    }
    protected handleMenuOpen(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}

	protected handleMenuClose() {
		this.setState({
			anchorEl: null,
		});
    }
    protected deleteRole() {
		this.handleMenuClose();
        this.props.handleDelete(this.props.role);
	//	delete role
    }
    protected manageRole() {
		this.handleMenuClose();
		this.props.handleManagePermissions(this.props.role);
	//	manage role
    }
    protected editRole() {
        this.handleMenuClose();
        this.props.handleEdit(this.props.role);
	//	edit role
	}

}
export default RoleOptions;
