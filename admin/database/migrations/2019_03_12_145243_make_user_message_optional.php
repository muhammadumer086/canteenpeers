<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeUserMessageOptional extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('messages', function (Blueprint $table) {
			$table->unsignedInteger('user_id')->nullable()->change();
			$table->boolean('system_generated')->default(false);
		});

		Schema::create('message_user', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('message_id');
			$table->unsignedInteger('user_id');

			$table->foreign('message_id')
			->references('id')->on('messages')
			->onDelete('cascade');

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});

		Schema::table('conversations', function (Blueprint $table) {
			$table->string('name')->nullable();
			$table->dropColumn('users_hash');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('messages', function (Blueprint $table) {
			$table->unsignedInteger('user_id')->change();
			$table->dropColumn('system_generated');
		});

		Schema::dropIfExists('message_user');

		Schema::table('conversations', function (Blueprint $table) {
			$table->dropColumn('name');
			$table->string('users_hash')->unique();
		});
	}
}
