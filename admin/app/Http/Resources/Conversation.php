<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Message as MessageResource;
use App\Http\Resources\User as UserResource;

class Conversation extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('conversation', $this->id, $this->updated_at, false, true)) {
			return $apiStore->getApiResource('conversation', $this->id, $this->updated_at, false, true);
		}

		$params = [
			'id',
			'name',
			'group_conversation',
			'created_at',
			'updated_at',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		// Add the additional relationships
		$result['users'] = (count($this->users)) ? UserResource::collection($this->users) : [];

		// Add the last message
		$result['last_message'] = new MessageResource($this->messages()->where('system_generated', false)->orderBy('updated_at', 'DESC')->orderBy('id', 'DESC')->first());

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);

		$apiStore->setApiResource($result, 'conversation', $this->id, $this->updated_at, false, true);

		return $result;
	}
}
