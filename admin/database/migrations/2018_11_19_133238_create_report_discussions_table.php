<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportDiscussionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('report_discussions', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();

			$table->string('title');
			$table->text('comment')->nullable();
			$table->unsignedInteger('user_id');
			$table->unsignedInteger('discussion_id');
			$table->unsignedInteger('reviewer_id')->nullable();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');

			$table->foreign('discussion_id')
			->references('id')->on('discussions')
			->onDelete('cascade');

			$table->foreign('reviewer_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('report_discussions');
	}
}
