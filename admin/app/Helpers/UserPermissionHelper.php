<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use Carbon\Carbon;

use App\User;


class UserPermissionHelper {


	/**
	 * Call this method to get singleton
	 *
	 * @return AuthUserStoreHelper
	 */
	public static function Instance() {
		static $inst = null;
		if ($inst === null) {
			$inst = new UserPermissionHelper();
		}
		return $inst;
	}

	public static function userHasPermission(User $user=null, $permissionArr=[]) {
		if($user && isset($user->roles[0]) && isset($user->roles[0]->permissions)) {
			$permissions = $user->roles[0]->permissions;
			foreach ($permissions as $permissionObj) {
				if (in_array($permissionObj->name, $permissionArr)) {
					return true;
				}
			}
		}
		return false;
	}

	public static function formatCarbonDate($date)
	{
		if($date)
		{
			//dd($date->toDateTimeString());
			$carbonFormat = array();
			$carbonFormat['date'] = $date->toDateTimeString();
			$carbonFormat['timezone_type'] = 3;
			$carbonFormat['timezone'] = "Australia/Sydney";
			return $carbonFormat;
		}
		return $date;
	}

}
