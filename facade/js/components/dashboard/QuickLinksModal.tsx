import React        from 'react';
import { connect }  from 'react-redux';

import Modal        from '@material-ui/core/Modal';
import Paper        from '@material-ui/core/Paper';

import ModalMenuItem from './../general/ModalMenuItem';

import {
	IStoreState,
	quickLinksModalHide,
} from '../../redux/store';

interface ILinks {
	label: string;
	href: string;
	as: string;
}

interface IProps {
	userAuthenticated: boolean|null;
	quickLinksModalDisplay: boolean|null;
	dispatch(action: any): void;
}

interface IState {
	links: ILinks[];
}

export class QuickLinksModal extends React.Component<IProps, IState> {

	constructor(props: IProps) {
		super(props);

		this.state = {
			links: this.generateQuickLinksList(),
		};

		this.handleClose = this.handleClose.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated
		) {
			this.setState({
				links: this.generateQuickLinksList()
			});
		}
	}

	public render() {
		return (
			<Modal
				aria-labelledby="quick-links-modal"
				open={this.props.quickLinksModalDisplay}
				onClose={this.handleClose}
			>
				<Paper className="modal quick_links_modal theme--accent" square={true}>
					<div className="quick_links_modal-title">
						<h6 className="font--h6">I'd Like to</h6>
					</div>
					<div className="quick_links_modal-menu">
						<ul>
							{
								this.state.links.map((link, index) => {
									return (
										<li key={index}>
											<ModalMenuItem action={this.handleClose} menuItem={link} />
										</li>
									)
								})
							}
						</ul>
					</div>
				</Paper>
			</Modal>
		);
	}

	protected handleClose() {
		this.props.dispatch(quickLinksModalHide());
	}

	protected generateQuickLinksList() {
		const result: ILinks[] = [];

		result.push({
			label: 'Start a new discussion',
			href: '/discussions?new=true',
			as: '/discussions?new=true'
		});

		result.push({
			label: 'Share my story',
			href: '/blogs',
			as: '/blogs'
		})

		result.push({
			label: 'Find helpful resources',
			href: '/resources',
			as: '/resources'
		})

		result.push({
			label: 'Connect with parents like me',
			href: '/people-like-me',
			as: '/people-like-me'
		})

		return result;
	}
}

function mapStateToProps(state: IStoreState) {
	const {
		userAuthenticated,

		userData
	} = state.user;
	const {
		quickLinksModalDisplay,
	} = state.quickLinksModal;
	return {
		userAuthenticated,
		quickLinksModalDisplay,
		userData
	};
}

export default connect(mapStateToProps)(QuickLinksModal);
