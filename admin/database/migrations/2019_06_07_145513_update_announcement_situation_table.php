<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAnnouncementSituationTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('announcement_situation', function (Blueprint $table) {
			$table->dropForeign('announcement_situation_situation_id_foreign');
			$table
				->foreign('situation_id')
				->references('id')
				->on('situations')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('announcement_situation', function (Blueprint $table) {
			$table->dropForeign('announcement_situation_situation_id_foreign');
			$table
				->foreign('situation_id')
				->references('id')
				->on('situations');
		});
	}
}
