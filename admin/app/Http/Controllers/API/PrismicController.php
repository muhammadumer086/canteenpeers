<?php

namespace App\Http\Controllers\API;

use App\Blog;
use App\PrismicLinkResolver;
use App\Situation;
use App\SituationUser;
use App\Topic;
use App\User;
use App\Resource;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;

use Prismic\Dom\RichText;
use Prismic\Api;
use Prismic\LinkResolver;
use Prismic\Predicates;

use Carbon\Carbon;

class PrismicController extends Controller {
	use \App\Traits\CreateSlug;

	const PAGINATION_SIZE = 20;

	protected function getBlogQueryPredicates() {
		return [
			Predicates::at('document.type', 'blog'),
			Predicates::at('my.blog.peers_visible_on_platform', 'Yes'),
		];
	}

	protected function getResourceQueryPredicates() {
		return [
			Predicates::at('document.type', 'resource'),
			Predicates::at('my.resource.peers_visible_on_platform', 'Yes'),
		];
	}

	/**
	 * Create a connection to the prismic API
	 *
	 * @return api
	 */
	protected function prismicApiCall() {
		$url = env('PRISMIC_API_URL', '');
		$token = env('PRISMIC_TOKEN', '');
		$api = Api::get($url, $token);
		return $api;
	}

	/**
	 * Call all prismic webhook related functions
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function prismicWebhook(Request $request) {
		// try {
		$api = $this->prismicApiCall();
		$this->syncPrismicEntries(
			$api,
			$this->getBlogQueryPredicates(),
			'syncSingleBlog',
			'finishSyncBlogs'
		);
		$this->syncPrismicEntries(
			$api,
			$this->getResourceQueryPredicates(),
			'syncSingleResource',
			'finishSyncResources'
		);

		return response()->json([
			'success' => true,
		]);
	}

	protected function syncPrismicEntries(
		API $api,
		$queryPredicates,
		$syncEntry,
		$finishSync
	) {
		// Pagination handling
		$currentPage = 0;
		$totalPages = 0;
		$timestamp = Carbon::now();

		do {
			++$currentPage;

			$response = $api->query($queryPredicates, [
				'pageSize' => self::PAGINATION_SIZE,
				'page' => $currentPage,
			]);


			if (isset($response->total_pages)) {
				$totalPages = $response->total_pages;
			}

			if (
				isset($response->results) &&
				is_array($response->results) &&
				sizeof($response->results)
			) {
				foreach ($response->results as $single) {
					$this->$syncEntry($api, $single);
				}
			}
		} while ($currentPage <= $totalPages);

		$this->$finishSync($timestamp);
	}

	protected function syncSingleBlog(API $api, $blogData) {

		$rawSituations = $this->filterPrismicSituations(
			isset($blogData->data->peers_situations)?
			$blogData->data->peers_situations:
			null
		);

		if (
			isset(
				$blogData->id,
				$blogData->uid,
				$blogData->data,
				$blogData->data->author,
				$blogData->data->author->id,
				$blogData->data->title[0],
				$blogData->data->body,
				$blogData->data->peers_featured,
				$blogData->data->peers_topic,
				$blogData->data->peers_topic->id,
				$blogData->first_publication_date,
				$blogData->last_publication_date
			)
		) {
			$author = $this->getPrismicEntry($api, $blogData->data->author->id);
			$topicData = $this->getPrismicEntry(
				$api,
				$blogData->data->peers_topic->id
			);

			$topic = null;
			if (isset($topicData->uid)) {
				$topic = $this->getTopicFromSlug($topicData->uid, 'blogs');
			}

			$contentHtml = RichText::asHtml(
				$blogData->data->body,
				new PrismicLinkResolver()
			);

			if (
				$topic &&
				isset(
					$author,
					$author->data,
					$author->data->author_full_name[0],
					$author->data->author_image->url
				)
			) {
				$originalBlog = Blog::where(
					'prismic_id',
					$blogData->id
				)->first();

				$slug = $this->createSlug(
					$blogData->uid,
					'App\Blog',
					$originalBlog ? $originalBlog->id : null
				);

				$blog = Blog::withTrashed()->updateOrCreate(
					[
						'prismic_id' => $blogData->id,
					],
					[
						'title' => $blogData->data->title[0]->text,
						'slug' => $slug,
						'content' => $contentHtml,
						'source' => 'prismic',
						'prismic_author' =>
							$author->data->author_full_name[0]->text,
						'prismic_author_image' =>
							$author->data->author_image->url,
						'feature_image' => isset($blogData->data->feature_image)
							? $blogData->data->feature_image
							: null,
						'featured' => $blogData->data->peers_featured === 'Yes',
						'first_publication_date' => date(
							'Y-m-d H:i:s',
							strtotime($blogData->first_publication_date)
						),
						'last_publication_date' => date(
							'Y-m-d H:i:s',
							strtotime($blogData->last_publication_date)
						),
						'seo_title' => isset($blogData->data->seo_title[0])
							? $blogData->data->seo_title[0]->text
							: null,
						'seo_description' => isset(
							$blogData->data->seo_description[0]
						)
							? $blogData->data->seo_description[0]->text
							: null,
						'seo_image' => isset($blogData->data->seo_image)
							? $blogData->data->seo_image
							: null,
						'deleted_at' => null,
					]
				);

				$blog->topic()->associate($topic);

				// Sync the situations
				if (
					is_array($rawSituations) &&
					sizeof($rawSituations)
				) {
					$blog
						->situations()
						->sync(
							$this->processPrismicSituations(
								$api,
								$rawSituations
							)
						);
				} else {
					// No situations attached to the entry means entry should be linked to all situations
					$blog
						->situations()
						->sync(Situation::whereNull('parent_id')->pluck('id'));
				}

				$blog->touch();
				$blog->save();
			}
		}
	}

	protected function finishSyncBlogs($timestamp) {
		// Remove all the prismic blogs that haven't been synchronised (deleted from prismic)
		Blog::whereNotNull('prismic_id')
			->where('updated_at', '<', $timestamp)
			->forceDelete();
	}

	protected function syncSingleResource(API $api, $resourceData) {
		$rawSituations = $this->filterPrismicSituations(
			isset($resourceData->data->peers_situations)?
			$resourceData->data->peers_situations:
			null
		);

		if (
			isset(
				$resourceData->id,
				$resourceData->uid,
				$resourceData->data,
				$resourceData->data->title[0],
				$resourceData->data->body,
				$resourceData->data->peers_featured,
				$resourceData->data->peers_ycs,
				$resourceData->data->peers_topic,
				$resourceData->data->peers_topic->id,
				$resourceData->first_publication_date,
				$resourceData->last_publication_date
			)
		) {
			$topicData = $this->getPrismicEntry(
				$api,
				$resourceData->data->peers_topic->id
			);

			$topic = null;
			if (isset($topicData->uid)) {
				$topic = $this->getTopicFromSlug($topicData->uid, 'resources');
			}

			$contentHtml = RichText::asHtml(
				$resourceData->data->body,
				new PrismicLinkResolver()
			);

			if ($topic) {
				$originalResource = Resource::where(
					'prismic_id',
					$resourceData->id
				)->first();

				$slug = $this->createSlug(
					$resourceData->uid,
					'App\Resource',
					$originalResource ? $originalResource->id : null
				);

				$resource = Resource::withTrashed()->updateOrCreate(
					[
						'prismic_id' => $resourceData->id,
					],
					[
						'title' => $resourceData->data->title[0]->text,
						'slug' => $slug,
						'content' => $contentHtml,
						'feature_image' => isset(
							$resourceData->data->feature_image
						)
							? $resourceData->data->feature_image
							: null,
						'featured' =>
							$resourceData->data->peers_featured === 'Yes',
						'is_ycs' => $resourceData->data->peers_ycs === 'Yes',
						'first_publication_date' => date(
							'Y-m-d H:i:s',
							strtotime($resourceData->first_publication_date)
						),
						'last_publication_date' => date(
							'Y-m-d H:i:s',
							strtotime($resourceData->last_publication_date)
						),
						'seo_title' => isset($resourceData->data->seo_title[0])
							? $resourceData->data->seo_title[0]->text
							: null,
						'seo_description' => isset(
							$resourceData->data->seo_description[0]
						)
							? $resourceData->data->seo_description[0]->text
							: null,
						'seo_image' => isset($resourceData->data->seo_image)
							? $resourceData->data->seo_image
							: null,
						'deleted_at' => null,
					]
				);

				$resource->topic()->associate($topic);

				// Sync the situations
				if (
					is_array($rawSituations) &&
					sizeof($rawSituations)
				) {
					$resource
						->situations()
						->sync(
							$this->processPrismicSituations(
								$api,
								$rawSituations
							)
						);
				} else {
					// No situations attached to the entry means entry should be linked to all situations
					$resource
						->situations()
						->sync(Situation::whereNull('parent_id')->pluck('id'));
				}
				$resource->touch();
				$resource->save();

			}
		}
	}

	protected function finishSyncResources($timestamp) {
		// Remove all the prismic resources that haven't been synchronised (deleted from prismic)
		Resource::whereNotNull('prismic_id')
			->where('updated_at', '<', $timestamp)
			->forceDelete();
	}

	protected function getTopicFromSlug($slug, $category) {
		return Topic::where('slug', $slug)
			->where('category', $category)
			->first();
	}

	protected function filterPrismicSituations(array $situations) {
		$filteredSituations = [];
		if (is_array($situations) && sizeof($situations)) {
			foreach ($situations as $situationData) {
				if (
					isset(
						$situationData->situation,
						$situationData->situation->uid
					)
				) {
					$filteredSituations[] = $situationData;
				}
			}
		}
		return $filteredSituations;
	}

	protected function processPrismicSituations(API $api, array $situations) {
		$situationIds = [];
		foreach ($situations as $situationData) {
			if (
				isset($situationData->situation, $situationData->situation->uid)
			) {
				$situation = $this->getSituationFromSlug(
					$situationData->situation->uid
				);
				if ($situation) {
					$situationIds[] = $situation->id;
				}
			}
		}
		return $situationIds;
	}

	protected function getSituationFromSlug($slug) {
		return Situation::where('slug', $slug)->first();
	}

	public function getPrismicEntry(API $api, $id) {
		$response = $api->getByID($id);

		return $response;
	}
}
