<?php

namespace App\Filters\User;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class UsernameFilter extends FilterAbstract {
	/**
	 * Mappings for database values.
	 *
	 * @return array
	 */
	public function mappings() {
		return [];
	}

	/**
	 * Filter with builder queries.
	 *
	 * @param  string $access
	 * @return Illuminate\Database\Eloquent\Builder
	 */
	public function filter(Builder $builder, $value) {
		return $builder->where('username', 'LIKE', '%' . $value . '%');
	}
}
