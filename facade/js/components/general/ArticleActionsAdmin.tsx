import React from 'react';
import { IconButton, Icon, Menu, MenuItem } from '@material-ui/core';

interface IMenuActions {
	label: string;
	action(): void;
}

interface IProps {
	menuActions: IMenuActions[] | null;
}

interface IState {
	anchorEl: any | null;
}

class ArticleActionsAdmin extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
		};

		this.handleMenuClose = this.handleMenuClose.bind(this);
		this.handleMenuOpen = this.handleMenuOpen.bind(this);
	}

	public render() {
		return (
			<React.Fragment>
				<div className="article_single-contextual_menu">
					<IconButton onClick={this.handleMenuOpen}>
						<Icon>more_vert</Icon>
					</IconButton>
				</div>

				<Menu
					anchorEl={this.state.anchorEl}
					open={Boolean(this.state.anchorEl)}
					onClose={this.handleMenuClose}
				>
					{this.props.menuActions.map((item, index) => {
						return (
							<MenuItem
								onClick={() => {
									this.handleMenuClose();
									item.action();
								}}
								key={index}
							>
								{item.label}
							</MenuItem>
						);
					})}
				</Menu>
			</React.Fragment>
		);
	}

	protected handleMenuOpen(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}

	protected handleMenuClose() {
		this.setState({
			anchorEl: null,
		});
	}
}

export default ArticleActionsAdmin;
