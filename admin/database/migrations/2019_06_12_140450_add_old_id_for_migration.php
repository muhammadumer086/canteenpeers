<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldIdForMigration extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('users', function (Blueprint $table) {
			$table->unsignedInteger('migration_id')->nullable();
		});
		Schema::table('blogs', function (Blueprint $table) {
			$table->unsignedInteger('migration_id')->nullable();
		});
		Schema::table('discussions', function (Blueprint $table) {
			$table->unsignedInteger('migration_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->dropColumn('migration_id');
		});
		Schema::table('blogs', function (Blueprint $table) {
			$table->dropColumn('migration_id');
		});
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('migration_id');
		});
	}
}
