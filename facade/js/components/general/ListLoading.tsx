import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

interface IProps {
	active: boolean;
	size?: number;
}

const ListLoading: React.FC<IProps> = (props: IProps) => {
	if (props.active) {
		return (
			<div className="list_loading">
				<CircularProgress size={props.size ? props.size : 40} />
			</div>
		);
	}

	return null;
};

export default ListLoading;
