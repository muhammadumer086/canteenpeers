import React from 'react';
import { connect } from 'react-redux';
import { ApiService, withServices } from '../../../services';
import { IApiUser } from 'js/interfaces';
import { IStoreState } from '../../../redux/store';

interface IProps {
	userData: IApiUser;
	apiService: ApiService;
	inputProps: any;
	dispatch(value: any): void;
}

interface IState {
	submitting: boolean;
}

class WelcomeModalSupportResearch extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
		};
	}

	public render() {
		return (
			<div className="modal_introduction-item">
				<div className="modal_introduction-item_image modal_introduction-item_image--mobile modal_introduction-item_image--step_four" />

				<div className="modal_introduction-item_content theme--main">
					<h4 className="font--h4 theme-title">
						Support our research
					</h4>
					<p>
						Are you happy to share your views and experiences to
						help Canteen provide the best possible support for young
						people affected by cancer?
					</p>

					<p>It’s ok to contact me</p>
				</div>

				<div className="modal_introduction-item_image modal_introduction-item_image--step_four" />
			</div>
		);
	}
}

const mapStateToProps = (state: IStoreState) => {
	const { userData } = state.user;

	return {
		userData,
	};
};

export default connect(mapStateToProps)(
	withServices(WelcomeModalSupportResearch),
);
