<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class UserBlocked extends Mailable {
	use Queueable, SerializesModels;

	protected $blocker;
	protected $blocked;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($blocker, $blocked) {
		$this->blocker = User::where(
			'username',
			$blocker->username
		)->firstOrFail();
		$this->blocked = User::where(
			'username',
			$blocked->username
		)->firstOrFail();
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		$title = "{$this->blocker->username} has blocked {$this->blocked->username}";

		return $this->view('emails.user-blocked')
			->subject($title)
			->with([
				'blocker' => $this->blocker,
				'blocked' => $this->blocked,
			]);
	}
}
