<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Validator;


class RoleController extends Controller
{
	const PAGINATION_SIZE = 3000;
	const RELATED_SIZE = 2;

	/**
	 * Display a listing of paginated roles.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		if (
			$authUser &&
			$authUser->hasRoleName('admin')
		) {
			$roles = Role::paginate(self::PAGINATION_SIZE);
			return response()->json(
				[
					'roles' => $roles,
				],
				200
			);
		}
	}

	/**
	 * Store a newly created role in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'name' => 'bail|required|string|max:191'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}
		$role = Role::create(['name' => $request->input('name'),'guard_name' => 'web']);

		return response()->json(
			[
				'role' => $role,
				'succcess' => 'Role has been added',
			],
			200
		);
	}

	/**
	 * Update the specified role from storage.
	 *
	 * @param \App\Role $role
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Role $role)
	{
		$authUser = $this->getAuthUser($request);

		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'name' => 'bail|required|string|max:191'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$role = Role::findOrCreate(['name' => $request->name,'guard_name' => 'web']);

		return response()->json(
			[
				'role' => $role,
				'permissions' => $role->permissions,
				'success' => "Successfully Updated",
			],
			200
		);

	}

	/**
	 * Update the specified role from storage.
	 *
	 * @param \App\Role $role
	 * @return \Illuminate\Http\Response
	 */
	public function getRolePermissions(Role $role)
	{
		/*if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}*/

		$permissions = $role->permissions;

		return response()->json(
			[
				'role' => $role
			],
			200
		);

	}/**
	 * Update the specified role from storage.
	 *
	 * @param \App\Role $role
	 * @return \Illuminate\Http\Response
	 */
	public function getRolePermissionsByName(Request $request)
	{
		/*if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}*/
		$validator = Validator::make($request->all(), [
			'name' => 'bail|required|string|max:191'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$role = Role::findByName($request->input('name'), "web");

		$permissions = $role->permissions;

		return response()->json(
			[
				'role' => $role
			],
			200
		);

	}


	/**
	 * Remove the specified role from storage.
	 *
	 * @param \App\Role $role
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, Role $role) {
		$authUser = $this->getAuthUser($request);

		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$role->delete();

		return response()->json([
			'success' => true,
		]);
	}



	/**
	 * Assign Permission to the specified role.
	 *
	 * @param \App\Role $role
	 * @return \Illuminate\Http\Response
	 */
	public function assignPermissionToRole(Request $request, Role $role) {
		$authUser = $this->getAuthUser($request);
		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'permissions' => 'required|array|min:1',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$result = $role->syncPermissions($request->input('permissions'));
		if($result->error)
		{
			return response()->json([
				'message' => $result->error,
				'success' => false,
			]);

		}
		return response()->json([
			'success' => true,
		]);
	}
	 /**
	 * Remove Permission to the specified role.
	 *
	 * @param \App\Role $role
	 * @return \Illuminate\Http\Response
	 */
	public function removePermissionToRole(Request $request, Role $role) {
		$authUser = $this->getAuthUser($request);
		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'permissions' => 'required|array|min:1',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$role->revokePermissionTo($request->input('permissions'));
		$role->fresh();

		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Assign the specified role in user.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $username
	 * @return \Illuminate\Http\Response
	 */
	public function assignRoleToUser(Request $request, User $user, Role $role) {
		// If user has admin role
		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}
		// Remove Current Roles
		$user->roles()->detach();
		$user->forgetCachedPermissions();

		//Assign new Role
		$user->assignRole($role);
		$user->generateRoles();
		$user->save();

		return response()->json([
			'success' => true,
		]);
	}
}
