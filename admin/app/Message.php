<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model {

	use SoftDeletes;

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'system_generated' => 'boolean',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'user_id',
	];

	/**
	 * All of the relationships to be touched.
	 *
	 * @var array
	 */
	protected $touches = [
		'conversation',
	];

	/**
	 * The conversation associated with the message
	 */
	public function conversation() {
		return $this->belongsTo('App\Conversation');
	}

	/**
	 * The user associated with the message
	 */
	public function user() {
		return $this->belongsTo('App\User')->withTrashed();
	}

	/**
	 * The target users associated with the message
	 */
	public function targetUsers() {
		return $this->belongsToMany('App\User', 'message_user')->withTrashed();
	}

	public function userActivities() {
		return $this->morphMany('App\UserActivity', 'activity');
	}
}
