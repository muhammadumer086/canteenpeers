<?php

namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\File;

use App\DiscussionImage;
use App\Helpers\ResizeImage;
use App\Http\Controllers\Controller;
use App\Http\Resources\DiscussionImage as DiscussionImageResource;

class DiscussionImageController extends Controller {
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);

		$messages = [
			'image' => 'An image is required',
			'max' => 'The image maximum size is 6MB',
			'dimensions' =>
				'The image must be below 5000 pixels width and height',
		];

		/*$validator = Validator::make(
			$request->all(),
			[
				'image' =>
					'required|image|max:6144|dimensions:max_width=5000,max_height=5000',
			],
			$messages
		);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}*/

		if(!$request->has('image'))
		{
			return response()->json(
				[
					'error' => "An image is required",
				],
				400
			);

		}

		$imageResize = new ResizeImage();

		$pathPartial =
			'discussion-images/' .
			md5($authUser->email . date('Y-m-d-h:i:s')) .
			'/' .
			md5(date('Y-m-d-h:i:s') . rand());

		// Upload the file to S3
		$tmpPath = $imageResize->maxSize($request->file('image'), 1024);
		$tmpFile = new File($tmpPath);
		$path = Storage::disk('s3')->putFile('discussion-images', $tmpFile, 'public');
		$imageSize = getimagesize($tmpFile);

		// Create the discussion image
		$discussionImage = new DiscussionImage();
		$discussionImage->path = $path;
		$discussionImage->width = $imageSize[0];
		$discussionImage->height = $imageSize[1];
		$discussionImage->user()->associate($authUser);
		$discussionImage->save();

		// Return the image's path and ID
		return response()->json([
			'image' => new DiscussionImageResource($discussionImage),
		]);
	}
}
