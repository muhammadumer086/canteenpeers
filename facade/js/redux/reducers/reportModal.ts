import { actionTypes } from '../actions';

import { IApiDiscussion } from '../../interfaces';

export enum EReportModalType {
	HARMFUL,
	AGE_SENSITIVE,
}

export interface IStoreState {
	modalDisplay: boolean;
	discussion: IApiDiscussion | null;
	modalType: EReportModalType;
}

export const reportModal = (
	state: IStoreState = {
		modalDisplay: false,
		discussion: null,
		modalType: EReportModalType.HARMFUL,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.REPORT_MODAL_DISPLAY:
			return Object.assign({}, state, {
				modalDisplay: true,
				discussion: action.discussion,
				modalType: action.modalType,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.REPORT_MODAL_HIDE:
			return Object.assign({}, state, {
				modalDisplay: false,
			});
		default:
			return state;
	}
};
