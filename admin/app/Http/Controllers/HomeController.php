<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		// $this->middleware('auth');
	}

	/**
	* Show the application dashboard.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index() {
		// return view('home');
		//
		return view('home', [
			'test' => "Test Value",
			'welcomeActions' => $this->welcomeActions(),
		]);
	}

	/**
	* Get the links for the welcome hero
	*
	* @return \Illuminate\Http\Response
	*/
	public function welcomeActions() {
		$actions = [];
		$actions[] = [
			'label' => 'Start a new discusion',
			'url' => '/discussions',
		];

		$actions[] = [
			'label' => 'Share my story',
			'url' => '/',
		];

		$actions[] = [
			'label' => 'Find helpful resources',
			'url' => '/',
		];

		$actions[] = [
			'label' => 'Connect with parents like me',
			'url' => '/',
		];

		$actions[] = [
			'label' => 'Browse topics people are talking about',
			'url' => '/',
		];

		return $actions;
	}
}
