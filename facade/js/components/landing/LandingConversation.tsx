import React from 'react';
import ReactMarkdown from 'react-markdown';
import ReactSVG from 'react-svg';

interface IConversationDetail {
	isUserReply: boolean;
	content: string;
	timestamp: string;
	username: string;
	avatar?: string | null;
}

const LandingConversation = () => {
	const landingPageConversation: IConversationDetail[] = [
		{
			isUserReply: false,
			content: 'Haha yep totally',
			timestamp: '15 mins ago',
			username: 'Krissy1',
			avatar: `${
				process.env.STATIC_PATH
			}/images/landing-page/landing-avatar.png`,
		},
		{
			isUserReply: false,
			content:
				'I was going to suggest we get coffee first and talk about your week?',
			timestamp: '15 mins ago',
			username: 'Krissy1',
			avatar: null,
		},
		{
			isUserReply: true,
			content:
				'Defs would be good to catch up before we go into the event!',
			timestamp: '14 mins ago',
			username: 'bella111',
			avatar: null,
		},
		{
			isUserReply: false,
			content: 'Exactly',
			timestamp: '12 mins ago',
			username: 'Krissy1',
			avatar: `${
				process.env.STATIC_PATH
			}/images/landing-page/landing-avatar.png`,
		},
		{
			isUserReply: false,
			content: 'Have you shared the event with Joel?',
			timestamp: '12 mins ago',
			username: 'alex',
			avatar: null,
		},
	];

	return (
		<div className="landing_conversation">
			{landingPageConversation.map((message, index) => {
				const classes = [
					'message_item-container',
					'animate-when-visible-loop',
				];

				if (message.isUserReply) {
					classes.push('message_item-container--is_user_message');
				}

				return (
					<div key={index} className={classes.join(' ')}>
						<div className="message_avatar">
							{message.avatar && (
								<img
									src={message.avatar}
									alt={process.env.APP_NAME}
								/>
							)}
						</div>

						<div className="message_item">
							<div className="message_item-content_container">
								<span className="message_item-name">
									{message.username}
								</span>
								<div className="message_item-content">
									<ReactMarkdown
										className="markdown"
										source={message.content}
										linkTarget="_blank"
									/>
									<ReactSVG
										className="message_item-accent"
										path={`${
											process.env.STATIC_PATH
										}/icons/message-accent.svg`}
									/>
								</div>
								<span className="message_item-timeago">
									{message.timestamp}
								</span>
							</div>
						</div>
					</div>
				);
			})}
		</div>
	);
};

export default LandingConversation;
