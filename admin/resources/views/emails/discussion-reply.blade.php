@extends('layouts.email')

@section('plain_text')
	@if (isset($user->username))
		Hi {{$user->username}},
	@else
		Hi,
	@endif

	@if ($followed)
		Community member {{ $author->full_name }} just replied to your followed discussion <b>{{$mainDiscussion->title}}</b>.
	@else
		Community member {{ $author->full_name }} just replied to you in the discussion <b>{{$mainDiscussion->title}}</b>.
	@endif
@endsection

@section('content')
	@component('emails.components.paragraph')
		@if (isset($user->username))
			Hi {{$user->username}},
		@else
			Hi,
		@endif
	@endcomponent

	@component('emails.components.paragraph')
		@if ($followed)
			Community member {{ $author->full_name }} just replied to your followed discussion <b>{{$mainDiscussion->title}}</b>.
		@else
			Community member {{ $author->full_name }} just replied to you in the discussion <b>{{$mainDiscussion->title}}</b>.
		@endif
	@endcomponent

	@php
		$link = config('canteen.facade_url') . '/discussions/' . $mainDiscussion->slug;

		if (
			!empty($reply->discussion_index)
		) {
			$link = config('canteen.facade_url') . '/discussions/' . $mainDiscussion->slug . '?replyId=' . $reply->discussion_index;
		}

	@endphp
	@component('emails.components.button', ['url' => $link])
		View Reply
	@endcomponent

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
