import React       from 'react';
import { connect } from 'react-redux';

import Modal        from '@material-ui/core/Modal';
import Paper        from '@material-ui/core/Paper';

import {
	IStoreState,
	landingVideoModalHide
} from '../../redux/store';

import { IApiUser } from '../../interfaces';
import { ApiService, withServices } from '../../services';
import { Icon, ButtonBase } from '@material-ui/core';

interface IProps {
	userData: IApiUser|null;
	landingVideoModalDisplay: boolean;
	apiService: ApiService;
	dispatch(action: any): void;
}

const LandingVideoModal: React.FC<IProps> = (props: IProps) => {

	const closeModal = () => {
		props.dispatch(landingVideoModalHide());
	}

	return (
		<Modal
			aria-labelledby="landing-video-modal"
			open={props.landingVideoModalDisplay}
			onClose={closeModal}
		>
			<React.Fragment>
				<ButtonBase focusRipple={true} onClick={closeModal}>
					<div className="landing_modal-close">
						<Icon>close</Icon>
					</div>
				</ButtonBase>

				<Paper className="modal modal--landing-modal-video theme--main" square={true}>
					<video loop autoPlay className="landing_modal-video" plays-inline="true" poster={`${process.env.STATIC_PATH}/images/desktop-hero-bg.jpg`}>
						<source src={`${process.env.STATIC_PATH}/videos/landing_hero_loop.mp4`} type="video/mp4" />
					</video>
				</Paper>
			</React.Fragment>
		</Modal>
	);
}

function mapStateToProps(state: IStoreState) {
	const {
		userData,
	} = state.user;

	const {
		landingVideoModalDisplay,
	} = state.landingVideoModal;
	return {
		userData,
		landingVideoModalDisplay,
	};
}

export default connect(mapStateToProps)(withServices(LandingVideoModal))
