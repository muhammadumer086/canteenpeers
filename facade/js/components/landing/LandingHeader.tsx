import React from 'react';
import Link from 'next/link';
import ReactSVG from 'react-svg';
import Router from 'next/router';
import { connect } from 'react-redux';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ButtonBase from '@material-ui/core/ButtonBase';

import { IStoreState } from '../../redux/store';
import { ButtonLink } from '../general/ButtonLink';

interface IProps {
	userAuthenticated: boolean | null;
	country: string;
}

interface IState {
	anchorEl: any | null;
	menuOpen: boolean;
	headerOpacity: number | null;
	countryLogo: string;
	landinHeadeClasses: string;
}

class LandingHeader extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
			menuOpen: false,
			headerOpacity: 0,
			landinHeadeClasses:
				'landing_header-logo landing_header-logo--desktop',
			countryLogo: 'canteen-connect-logo-new.svg',
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleLink = this.handleLink.bind(this);
		this.handleMenuClick = this.handleMenuClick.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
	}

	public componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
		const { country } = this.props;

		let landinHeadeClasses =
			'landing_header-logo landing_header-logo--desktop';
		const countryLogo =
			country === 'AU'
				? 'canteen-connect-logo-new.svg'
				: 'nz/landing_logo_nz.svg';
		if (country !== 'AU') {
			landinHeadeClasses = landinHeadeClasses + ' landing_header-logo-nz';
		}
		this.setState({ countryLogo, landinHeadeClasses });
	}

	public componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

	public render() {
		const { anchorEl, countryLogo, landinHeadeClasses } = this.state;

		return (
			<header className="landing_header">
				<div
					className="landing_header-background"
					style={{ opacity: this.state.headerOpacity }}
				/>
				<div className="landing_header-container">
					<div className="landing_header-logo_container">
						<Link href="/">
							<a>
								<img
									className={landinHeadeClasses}
									src={`${process.env.STATIC_PATH}/images/${countryLogo}`}
									alt={process.env.APP_NAME}
								/>
							</a>
						</Link>
						<Link href="/">
							<a>
								<img
									className="landing_header-logo landing_header-logo--mobile"
									src={`${process.env.STATIC_PATH}/images/logo-canteen-mobile-white.png`}
									alt={process.env.APP_NAME}
									width="40"
									height="40"
								/>
							</a>
						</Link>
					</div>

					<div className="landing_header-action landing_header-action--mobile">
						<ButtonBase
							focusRipple={true}
							className="landing_header-mobile_login_button"
							onClick={this.handleMenuClick}
						>
							<div className="landing_header-side-profile-avatar">
								<ReactSVG
									path={`${process.env.STATIC_PATH}/icons/avatar.svg`}
								/>
							</div>
						</ButtonBase>
					</div>

					<div className="landing_header-action landing_header-action--desktop">
						{this.props.userAuthenticated && (
							<ButtonLink
								variant="contained"
								color="secondary"
								href="/dashboard"
								className="landing_header-button landing_header-button--login"
							>
								Dashboard
							</ButtonLink>
						)}
						{!this.props.userAuthenticated && (
							<React.Fragment>
								<ButtonLink
									variant="contained"
									color="secondary"
									className="landing_header-button landing_header-button--login"
									href="/auth/login"
								>
									Log In
								</ButtonLink>

								<ButtonLink
									variant="contained"
									color="primary"
									className="landing_header-button"
									href="/auth/register?step=step-1"
									hrefAs="/auth/register/step-1"
								>
									Join
								</ButtonLink>
							</React.Fragment>
						)}
					</div>
				</div>

				<Menu
					id="account-menu"
					anchorEl={anchorEl}
					open={Boolean(anchorEl)}
					onClose={this.handleClose}
				>
					<MenuItem
						onClick={() => {
							this.handleLink('/auth/login');
						}}
					>
						Login
					</MenuItem>
					<MenuItem
						onClick={() => {
							this.handleLink('/auth/register/step-1');
						}}
					>
						Register
					</MenuItem>
				</Menu>
			</header>
		);
	}

	protected handleMenuClick(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}

	protected handleLink(url: string) {
		Router.push(url);
	}

	protected handleClose() {
		this.setState({
			anchorEl: null,
		});
	}

	protected handleScroll(_event: Event) {
		if (window.scrollY > 0) {
			this.setState({
				headerOpacity: 1,
			});
		} else {
			this.setState({
				headerOpacity: 0,
			});
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(LandingHeader);
