<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationToUsersAndEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('location')->after('postcode');
            $table->renameColumn('postcode_data', 'location_data');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('address', 'location');
            $table->mediumText('location_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('location');
            $table->renameColumn('location_data', 'postcode_data');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->renameColumn('location','address');
            $table->dropColumn('location_data');
        });
    }
}
