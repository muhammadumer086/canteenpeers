<?php

namespace App\Notifications;

use App\Blog;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewBlogArticleNotification extends Notification {
	use Queueable;

	protected $blog;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Blog $blog) {
		$this->blog = $blog;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['database'];
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			'blog' => $this->blog,
		];
	}
}
