import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';

import Button from '@material-ui/core/Button';

import { validationMessage } from '../../helpers/validations';
import { TextFieldFormik } from '../inputs';

interface IProps {
	isSubmitting: boolean;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	email: string;
	name: string;
}

const NewsletterSignupForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		email: validationMessage.email,
		name: Yup.string()
			.required('A name is required')
			.max(191),
	});

	return (
		<React.Fragment>
			<Formik
				initialValues={{
					email: '',
					name: '',
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Your name"
										name="name"
										disabled={props.isSubmitting}
										{...inputProps}
									/>
								</div>

								<div className="form-column">
									<TextFieldFormik
										type="email"
										label="Email Address"
										name="email"
										disabled={props.isSubmitting}
										{...inputProps}
									/>
								</div>

								<div className="form-column form-column--submit">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid || props.isSubmitting
										}
									>
										Subscribe
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</React.Fragment>
	);
};

export default NewsletterSignupForm;
