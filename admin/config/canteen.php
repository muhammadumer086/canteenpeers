<?php

return [

	/*
	|--------------------------------------------------------------------------
	| The facade URL
	|--------------------------------------------------------------------------
	|
	*/
	'facade_url' => env('CANTEEN_FACADE_URL', (isset($_SERVER['CANTEEN_FACADE_URL'])) ? $_SERVER['CANTEEN_FACADE_URL'] : 'http://localhost:3000'),

	/*
	|--------------------------------------------------------------------------
	| The auth API credentials
	|--------------------------------------------------------------------------
	|
	*/
	'auth_client_id' => env('CANTEEN_AUTH_CLIENT_ID', (isset($_SERVER['CANTEEN_AUTH_CLIENT_ID'])) ? $_SERVER['CANTEEN_AUTH_CLIENT_ID'] : null),
	'auth_client_secret' => env('CANTEEN_AUTH_CLIENT_SECRET', (isset($_SERVER['CANTEEN_AUTH_CLIENT_SECRET'])) ? $_SERVER['CANTEEN_AUTH_CLIENT_SECRET'] : null),

	/*
	|--------------------------------------------------------------------------
	| The email assets path
	|--------------------------------------------------------------------------
	|
	*/
	'email_assets' => env('CANTEEN_EMAIL_ASSETS', (isset($_SERVER['CANTEEN_EMAIL_ASSETS'])) ? $_SERVER['CANTEEN_EMAIL_ASSETS'] : ''),

	/*
	|--------------------------------------------------------------------------
	| The algolia custom config
	|--------------------------------------------------------------------------
	|
	*/
	'algolia_private_prefix' => env('CANTEEN_ALGOLIA_PRIVATE_INDEX', (isset($_SERVER['CANTEEN_ALGOLIA_PRIVATE_INDEX'])) ? $_SERVER['CANTEEN_ALGOLIA_PRIVATE_INDEX'] : 'wvYpa0KwbNM5p_'),
	'algolia_public_prefix' => env('CANTEEN_ALGOLIA_PUBLIC_INDEX', (isset($_SERVER['CANTEEN_ALGOLIA_PUBLIC_INDEX'])) ? $_SERVER['CANTEEN_ALGOLIA_PUBLIC_INDEX'] : 'public_'),

	'algolia_app_id' => env('ALGOLIA_APP_ID', (isset($_SERVER['ALGOLIA_APP_ID'])) ? $_SERVER['ALGOLIA_APP_ID'] : ''),
	'algolia_search_key' => env('ALGOLIA_SEARCH', (isset($_SERVER['ALGOLIA_SEARCH'])) ? $_SERVER['ALGOLIA_SEARCH'] : ''),

	'maps_key' => env('GOOGLE_MAPS_KEY', (isset($_SERVER['GOOGLE_MAPS_KEY'])) ? $_SERVER['GOOGLE_MAPS_KEY'] : ''),

	/*
	|--------------------------------------------------------------------------
	| The campaign monitor config
	|--------------------------------------------------------------------------
	|
	*/
	'campaign_monitor_key' => env('CAMPAIGN_MONITOR_KEY', (isset($_SERVER['CAMPAIGN_MONITOR_KEY'])) ? $_SERVER['CAMPAIGN_MONITOR_KEY'] : ''),
	'campaign_monitor_list_id' => env('CAMPAIGN_MONITOR_LIST_ID', (isset($_SERVER['CAMPAIGN_MONITOR_LIST_ID'])) ? $_SERVER['CAMPAIGN_MONITOR_LIST_ID'] : ''),

	/*
	|--------------------------------------------------------------------------
	| The Intercom config
	|--------------------------------------------------------------------------
	|
	*/
	'intercom_hash' => env('INTERCOM_HASH', (isset($_SERVER['INTERCOM_HASH'])) ? $_SERVER['INTERCOM_HASH'] : ''),

	/*
	|--------------------------------------------------------------------------
	| The qualtrics config
	|--------------------------------------------------------------------------
	|
	*/
	'qualtrics_dic' => env('QUALTRICS_DIC', (isset($_SERVER['QUALTRICS_DIC'])) ? $_SERVER['QUALTRICS_DIC'] : ''),
	'data_center_id' => env('DATA_CENTER_ID', (isset($_SERVER['DATA_CENTER_ID'])) ? $_SERVER['DATA_CENTER_ID'] : ''),
	'qualtrics_token' => env('QUALTRICS_TOKEN', (isset($_SERVER['QUALTRICS_TOKEN'])) ? $_SERVER['QUALTRICS_TOKEN'] : ''),
	'mailing_list' => env('MAILING_LIST', (isset($_SERVER['MAILING_LIST'])) ? $_SERVER['MAILING_LIST'] : ''),


	/*
	|--------------------------------------------------------------------------
	| The twilio config
	|--------------------------------------------------------------------------
	|
	*/
	'twillio_id' => env('TWILIO_SID', (isset($_SERVER['TWILIO_SID'])) ? $_SERVER['TWILIO_SID'] : ''),
	'twilio_auth_token' => env('TWILIO_AUTH_TOKEN', (isset($_SERVER['TWILIO_AUTH_TOKEN'])) ? $_SERVER['TWILIO_AUTH_TOKEN'] : ''),
	'twilio_number' => env('TWILIO_NUMBER', (isset($_SERVER['TWILIO_NUMBER'])) ? $_SERVER['TWILIO_NUMBER'] : ''),

	/*
	|--------------------------------------------------------------------------
	| The APP config
	|--------------------------------------------------------------------------
	|
	*/

	'app_sign' => env('APP_SIGNATURE', (isset($_SERVER['APP_SIGNATURE'])) ? $_SERVER['APP_SIGNATURE'] : ''),
	'app_name' => env('APP_NAME', (isset($_SERVER['APP_NAME'])) ? $_SERVER['APP_NAME'] : ''),



];
