<?php

namespace App\Notifications;

use App\Event;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MigrationNotification extends Notification {
	use Queueable;

	public $token;
	protected $user;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($token, $user) {
		$this->token = $token;
		$this->user = $user;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "{$this->user->first_name}, join the convo";

		$facadeUrl =
			config('canteen.facade_url') .
			'/auth/migrated' .
			'?token=' .
			urlencode($this->token);

		return (new MailMessage())
			->subject($title)
			->view('emails.user-migrated', [
				'user' => $this->user,
				'facadeUrl' => $facadeUrl,
			]);
	}
}
