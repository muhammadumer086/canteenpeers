import React from 'react';
import ReactSVG from 'react-svg';
import { connect } from 'react-redux';

// import Button from '@material-ui/core/Button';

// import CheckIcon from '@material-ui/icons/Check';

import { IStoreState, apiError, userUpdated } from '../../redux/store';

import { IApiUser } from '../../interfaces';

import { ApiService } from '../../services';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';
import { Button } from '@material-ui/core';

interface IProps {
	className?: string;
	discussionId: number;
	discussionTitle: string;
	followed: boolean;
	disabled?: boolean;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	followed: boolean;
}

class DiscussionFollow extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isUpdating: false,
			followed: props.followed,
		};

		this.handleClick = this.handleClick.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.followed !== prevProps.followed) {
			this.setState({
				followed: this.props.followed,
			});
		}
	}

	public render() {
		const classNames = ['discussion_follow'];

		if (this.props.className) {
			classNames.push(this.props.className);
		}

		if (this.state.followed) {
			classNames.push('discussion_follow--followed');
		}

		return (
			<div className={classNames.join(' ')}>
				<Button
					className={`discussion_follow-button ${
						this.state.followed
							? 'discussion_follow-button--followed'
							: ''
					}`}
					color="primary"
					variant="outlined"
					onClick={this.handleClick}
				>
					<div className="discussion_follow-label">
						{!this.state.followed && (
							<React.Fragment>
								<span className="discussion_follow-label-text">
									Follow
								</span>
								<ReactSVG
									className="discussion_follow-label-icon"
									path={`${
										process.env.STATIC_PATH
									}/icons/plus.svg`}
								/>
							</React.Fragment>
						)}

						{this.state.followed && (
							<React.Fragment>
								<ReactSVG
									className="discussion_follow-label-icon"
									path={`${
										process.env.STATIC_PATH
									}/icons/users.svg`}
								/>
								<ReactSVG
									className="discussion_follow-label-icon"
									path={`${
										process.env.STATIC_PATH
									}/icons/check.svg`}
								/>
							</React.Fragment>
						)}
					</div>
				</Button>
			</div>
		);
	}

	protected handleClick() {
		this.setState(
			{
				followed: !this.state.followed,
			},
			() => {
				this.updateFollowCount(this.state.followed);
				this.props.apiService
					.queryPOST(
						`/api/discussions/${
							this.props.discussionId
						}/toggle-follow`,
						{},
					)
					.then(data => {
						if (data.followed) {
							this.GTM.pushEventToDataLayer({
								event: 'followDiscussion',
								title: this.props.discussionTitle,
							});
						}
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState(
							{
								followed: !this.state.followed,
							},
							() => {
								this.updateFollowCount(this.state.followed);
							},
						);
					});
			},
		);
	}

	protected updateFollowCount(add: boolean = true) {
		const user: IApiUser = JSON.parse(JSON.stringify(this.props.userData));
		if (
			user &&
			'following_count' in user &&
			typeof user.following_count === 'number'
		) {
			if (add) {
				user.following_count += 1;
			} else if (user.following_count >= 1) {
				user.following_count -= 1;
			}
			this.props.dispatch(userUpdated(user));
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(DiscussionFollow);
