import React from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { apiError, IStoreState } from '../../../redux/store';

import { IApiSituation, IApiTopic } from '../../../interfaces';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import ListLoading from '../../../components/general/ListLoading';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import TopicsGrid from '../../../components/admin/TopicsGrid';
import HeroHeading from '../../../components/general/HeroHeading';
import {
	IAdminProps,
	IAdminState,
} from '../../../components/admin/withAdminPage';

import { checkPermission } from "../../../helpers/checkPermission";
export enum ETopicsManagementPermissions{
	READ="topic_management_read",
	ADD="topic_management_add",
	EDIT="topic_management_edit",
	DELETE="topic_management_delete",
	ALL="topic_management_all",
}
interface IProps extends IAdminProps {
	situations: IApiSituation[];
	topics: IApiTopic[];
}

interface IState extends IAdminState {
	value: number;
	situations: IApiSituation[];
	topics: IApiTopic[];
	isLoadingPermission: boolean;
	permissions: string[];
}

class Topics extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			value: 0,
			topics: [],
			situations: [],
			isLoadingPermission: false,
			permissions: []
		};

		this.handleTabChange = this.handleTabChange.bind(this);
		this.handleTopicsUpdate = this.handleTopicsUpdate.bind(this);
		this.initialise=this.initialise.bind(this);

	}

	public componentDidMount() {
		if (this.props.userAdmin) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.userAdmin) {
			this.initialise(this.props, prevProps);
		}
	}

	public render() {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,ETopicsManagementPermissions.ALL,ETopicsManagementPermissions.READ);
		const editPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,ETopicsManagementPermissions.ALL,ETopicsManagementPermissions.EDIT);

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading title="Topic Management" />
					</header>
					{this.state.topics.length === 0 ||
					this.state.situations.length === 0 ? (
						<div className="admin_content">
							<ListLoading active={true} />
						</div>
					) : readPermission?(
						<div className="admin_content">
							<div className="hm-b16">
								<Paper square={true}>
									<Tabs
										value={this.state.value}
										onChange={this.handleTabChange}
										indicatorColor="primary"
										textColor="primary"
									>
										<Tab label="Discussions" />
										<Tab label="Blogs" />
										<Tab label="Resources" />
									</Tabs>
								</Paper>
							</div>
							{this.state.value === 0 && (
								<TopicsGrid
									title="Discussions Topics"
									type="discussions"
									topics={this.state.topics}
									editPermission={editPermission}
									situations={this.state.situations}
									apiService={this.props.apiService}
									handleTopicsUpdate={this.handleTopicsUpdate}
								/>
							)}
							{this.state.value === 1 && (
								<TopicsGrid
									title="Blogs Topics"
									type="blogs"
									topics={this.state.topics}
									editPermission={editPermission}
									situations={this.state.situations}
									apiService={this.props.apiService}
									handleTopicsUpdate={this.handleTopicsUpdate}
								/>
							)}
							{this.state.value === 2 && (
								<TopicsGrid
									title="Resources Topics"
									type="resources"
									topics={this.state.topics}
									editPermission={editPermission}
									situations={this.state.situations}
									apiService={this.props.apiService}
									handleTopicsUpdate={this.handleTopicsUpdate}
								/>
							)}
						</div>
					):<div><h2>Forbidden</h2></div>}
				</LayoutDashboard>
			</div>
		);
	}

	protected initialise(props?: IProps, prevProps?: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(props.permissions,ETopicsManagementPermissions.ALL,ETopicsManagementPermissions.READ);
		const permissions=prevProps?prevProps.permissions:[];
		const prevReadPermission: boolean =this.props.roleName==="admin"|| checkPermission(permissions,ETopicsManagementPermissions.ALL,ETopicsManagementPermissions.READ);
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin||(!prevReadPermission&&readPermission))
		) {
			this.props.apiService
				.queryGET('/api/topics', 'topics')
				.then(topics => {
					this.setState({
						topics,
					});
				})
				.catch(error => {
					// dispatch the error message
					this.props.dispatch(apiError([error.message]));
				});

			this.props.apiService
				.queryGET('/api/situations', 'situations')
				.then(situations => {
					this.setState({
						situations,
					});
				})
				.catch(error => {
					// dispatch the error message
					this.props.dispatch(apiError([error.message]));
				});
		}
	}

	protected handleTabChange(_event, value) {
		this.setState({
			value,
		});
	}

	protected handleTopicsUpdate(topics: IApiTopic[]) {
		this.setState({
			topics,
		});
	}
	
}

function mapStateToProps(state: IStoreState) {
	const { topics } = state.topics;
	const { situations } = state.situations;
	return {
		topics,
		situations,
	};
}

export default connect(mapStateToProps)(Topics);
