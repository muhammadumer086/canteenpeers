<?php

namespace App\Observers;

use UserActivityHelper;

use App\Discussion;

class DiscussionObserver {
	/**
	 * Handle the discussion "created" event.
	 *
	 * @param  \App\Discussion  $discussion
	 * @return void
	 */
	public function created(Discussion $discussion) {
		UserActivityHelper::create(
			$discussion->user,
			$discussion,
			($discussion->main_discussion_id) ? 'Discussion Reply' : 'Discussion Created',
			null
		);
	}

	/**
	 * Handle the discussion "updated" event.
	 *
	 * @param  \App\Discussion  $discussion
	 * @return void
	 */
	public function updated(Discussion $discussion) {
		//
	}

	/**
	 * Handle the discussion "deleted" event.
	 *
	 * @param  \App\Discussion  $discussion
	 * @return void
	 */
	public function deleted(Discussion $discussion) {
		// Handled in the "destroy" function of the Discussion controller
	}

	/**
	 * Handle the discussion "restored" event.
	 *
	 * @param  \App\Discussion  $discussion
	 * @return void
	 */
	public function restored(Discussion $discussion) {
		UserActivityHelper::create(
			$discussion->user,
			$discussion,
			'Discussion Restored',
			null
		);
	}

	/**
	 * Handle the discussion "force deleted" event.
	 *
	 * @param  \App\Discussion  $discussion
	 * @return void
	 */
	public function forceDeleted(Discussion $discussion) {
		//
	}
}
