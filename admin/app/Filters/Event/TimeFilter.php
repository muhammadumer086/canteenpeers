<?php

namespace App\Filters\Event;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class TimeFilter extends FilterAbstract {
	/**
	 * Mappings for database values.
	 *
	 * @return array
	 */
	public function mappings() {
		return [];
	}

	/**
	 * Filter with builder queries.
	 *
	 * @param  string $access
	 * @return Illuminate\Database\Eloquent\Builder
	 */
	public function filter(Builder $builder, $value) {
		foreach ($value as $key) {
			$builder->orWhere($key, true);
		}
		
		return $builder;
	}
}
