<?php

use Illuminate\Database\Seeder;

use App\Language;

class LanguagesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$languages = [
			'None',
			'Mandarin Chinese',
			// 'English',
			'Spanish',
			'Hindi',
			'French',
			'Portuguese',
			'Bengali',
			'Standard Arabic',
			'Russian',
			'Indonesian',
			'Urdu',
			'Standard German',
			'Japanese',
			'Swahili',
			'Western Punjabi',
			'Javanese',
			'Wu Chinese',
			'Telugu',
			'Korean',
			'Tamil',
			'Marathi',
			'Turkish',
			'Vietnamese',
			'Italian',
			'Yue Chinese',
			'Thai',
			'Egyptian Spoken Arabic',
			'Iranian Persian',
			'Huizhou Chinese',
			'Min Nan Chinese',
			'Gujarati',
			'Kannada',
			'Jinyu Chinese',
			'Filipino',
			'Burmese',
			'Hausa',
			'Polish',
			'Bhojpuri',
			'Xiang Chinese',
			'Ukrainian',
			'Malayalam',
			'Sunda',
			'Maithili',
			'Odia',
			'Algerian Spoken Arabic',
			'Hakka Chinese',
			'Nigerian Pidgin',
			'Eastern Punjabi',
			'Moroccan Spoken Arabic',
			'Zulu',
			'North Levantine Spoken Arabic',
			'Amharic',
			'Tagalog',
			'Northern Uzbek',
			'Sindhi',
			'Romanian',
			'Dutch',
			'Gan Chinese',
			'Northern Pashto',
			'Yoruba',
			'Sa’idi Spoken Arabic',
			'Saraiki',
			'Xhosa',
			'Malay',
			'Igbo',
			'Afrikaans',
			'Sudanese Spoken Arabic',
			'Sinhala',
			'Cebuano',
			'Mesopotamian Spoken Arabic',
			'Nepali',
			'Rangpuri',
			'Central Khmer',
			'Northern Kurdish',
			'Northeastern Thai',
			'South Azerbaijani',
			'Somali',
			'Bamanankan',
			'Bavarian',
			'Magahi',
			'Northern Sotho',
			'Southern Sotho',
			'Chhattisgarhi',
			'Tswana',
			'Czech',
			'Greek',
			'Chittagonian',
			'Assamese',
			'Deccan',
			'Kazakh',
			'Hungarian',
			'Shona',
			'Jula',
			'Swedish',
			'Sylheti',
			'Najdi Spoken Arabic',
			'Nigerian Fulfulde',
			'Tunisian Spoken Arabic',
			'Kinyarwanda',
			'Min Bei Chinese',
			'Maori'
		];

		foreach ($languages as $language) {
			$slug = str_slug($language, '-');
			if (!Language::where('slug', '=', $slug)->exists()) {
				Language::create([
					'slug' => $slug,
					'name' => $language,
				]);
			}
		}
	}
}
