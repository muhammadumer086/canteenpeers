import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';

import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';

import ClearIcon from '@material-ui/icons/Clear';

import { ApiService, withServices } from '../../services';
import { IStoreState, apiError, apiSuccess } from '../../redux/store';

import { AutocompleteUserFormik } from '../inputs';

interface IFormProps {
	submitting: boolean;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	users: string[];
}

const ShareForm: React.FC<IFormProps> = (props: IFormProps) => {
	const validationFormat = Yup.object().shape({
		users: Yup.array().of(Yup.string().max(191)),
	});

	return (
		<div>
			<Formik
				initialValues={{
					users: [],
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>
							<div className="form-row">
								<div className="form-column">
									<AutocompleteUserFormik
										label="Users"
										name="users"
										placeholder="Please search for a user"
										disabled={!props.submitting}
										multi={true}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || props.submitting}
									>
										Share
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
};

interface IProps {
	apiService: ApiService;
	modalOpen: boolean;
	url: string;
	title: string;
	handleClose(): void;
	onCommunityShare(): void;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
}

class ResourceCommunityShare extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
		};

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	public render() {
		return (
			<Modal
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				open={this.props.modalOpen}
				onClose={this.props.handleClose}
			>
				<Paper className="modal theme--main" square={true}>
					<button
						onClick={this.props.handleClose}
						className="close_button close_button--primary"
						title="Dismiss"
					>
						<ClearIcon className="close_button-icon" />
					</button>
					<div className="modal-content_container">
						<div className="modal-content discussion_modal">
							<h2 className="modal-content_title font--h4 theme-title hm-t16">
								Share in the community
							</h2>
							<ShareForm
								onSubmit={this.handleSubmit}
								submitting={this.state.submitting}
							/>
						</div>
					</div>
				</Paper>
			</Modal>
		);
	}

	protected handleSubmit(data: IFormValues) {
		const values = {
			users: data.users,
			message: `Check out this resource: <a href="${this.props.url}">${
				this.props.title
			}</a>`,
		};
		this.props.apiService
			.queryPOST(`/api/conversations`, values, 'conversation')
			.then(() => {
				this.setState({
					submitting: false,
				});
				this.props.dispatch(apiSuccess(['Article shared']));
				this.props.onCommunityShare();
				this.props.handleClose();
			})
			.catch(error => {
				this.setState({
					submitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(withServices(ResourceCommunityShare));
