<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserData extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('situations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::create('situation_user', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('situation_id');
			$table->unsignedInteger('user_id');

			$table->foreign('situation_id')
			->references('id')->on('situations')
			->onDelete('cascade');

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});

		Schema::create('countries', function (Blueprint $table) {
			$table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::create('languages', function (Blueprint $table) {
			$table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::create('language_user', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('language_id');
			$table->unsignedInteger('user_id');

			$table->foreign('language_id')
			->references('id')->on('languages')
			->onDelete('cascade');

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});

		Schema::create('heard_abouts', function (Blueprint $table) {
			$table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::create('heard_about_user', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('heard_about_id');
			$table->unsignedInteger('user_id');

			$table->foreign('heard_about_id')
			->references('id')->on('heard_abouts')
			->onDelete('cascade');

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});

		Schema::create('personal_settings', function (Blueprint $table) {
			$table->increments('id');
			$table->string('slug')->unique();
			$table->string('name');
			$table->timestamps();
		});

		Schema::create('personal_setting_user', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('personal_setting_id');
			$table->unsignedInteger('user_id');

			$table->foreign('personal_setting_id')
			->references('id')->on('personal_settings')
			->onDelete('cascade');

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});

		Schema::create('leaving_reasons', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->mediumText('leaving_reasons_data');
			$table->timestamps();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});

		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('name');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('username')->unique();
			$table->char('postcode', 4);
			$table->mediumText('postcode_data');
			$table->enum('gender', ['male', 'female', 'undefined']);

			$table->char('phone', 20)->nullable();
			$table->date('dob')->nullable();
			$table->unsignedInteger('country_of_birth')->nullable();
			$table->boolean('indigenous_australian')->nullable();

			$table->dateTime('last_login')->nullable();
			$table->ipAddress('last_login_ip')->nullable();

			$table->softDeletes();

			$table->foreign('country_of_birth')
			->references('id')->on('countries')
			->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('users', function (Blueprint $table) {
			$table->dropForeign('users_country_of_birth_foreign');
			$table->dropSoftDeletes();
			$table->dropColumn([
				'name',
				'first_name',
				'last_name',
				'username',
				'postcode',
				'postcode_data',
				'gender',
				'phone',
				'dob',
				'country_of_birth',
				'indigenous_australian',
				'last_login',
				'last_login_ip',
			]);
			$table->string('name');
		});

		Schema::dropIfExists('leaving_reasons');
		Schema::dropIfExists('personal_setting_user');
		Schema::dropIfExists('personal_settings');
		Schema::dropIfExists('heard_about_user');
		Schema::dropIfExists('heard_abouts');
		Schema::dropIfExists('language_user');
		Schema::dropIfExists('languages');
		Schema::dropIfExists('countries');
		Schema::dropIfExists('situation_user');
		Schema::dropIfExists('situations');
	}
}
