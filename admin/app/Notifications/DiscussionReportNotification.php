<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Discussion as DiscussionResource;
use App\Notifications\CanteenNotificationChannel;

class DiscussionReportNotification extends Notification {
	use Queueable;

	public $discussion;
	public $user;
	public $author;
	public $ageSensitive;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($discussion, $user, $author, $ageSensitive) {
		$this->discussion = $discussion;
		$this->user = $user;
		$this->author = $author;
		$this->ageSensitive = $ageSensitive;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [
			// 'mail',
			'database',
		];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$isReply = $this->discussion->main_discussion_id !== null;

		$mainDiscussion = $isReply
			? $this->discussion->mainDiscussion
			: $this->discussion;

		$title = "Someone reported {$mainDiscussion->title}";
		if ($this->ageSensitive) {
			$title = "Someone flagged {$mainDiscussion->title} as age-sensitive";
		} elseif ($isReply) {
			$title = "Someone reported a reply in {$mainDiscussion->title}";
		}

		return (new MailMessage())
			->subject($title)
			->view('emails.discussion-reported', [
				'user' => $this->user,
				'discussion' => $this->discussion,
				'mainDiscussion' => $mainDiscussion,
				'author' => $this->author,
				'ageSensitive' => $this->ageSensitive,
				'isReply' => $isReply,
			]);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		$mainDiscussion = $this->discussion->main_discussion_id
			? $this->discussion->mainDiscussion
			: $this->discussion;
		return [
			'user' => new UserResource($this->user),
			'discussion' => new DiscussionResource($this->discussion),
			'mainDiscussion' => new DiscussionResource($mainDiscussion),
			'author' => new UserResource($this->author),
			'ageSensitive' => $this->ageSensitive,
		];
	}
}
