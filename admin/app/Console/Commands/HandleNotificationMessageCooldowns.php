<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\NotificationMessageCooldown;
use Carbon\Carbon;

class HandleNotificationMessageCooldowns extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:handleNotificationMessageCooldowns';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send a message to all the users that have pending messages';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$intervalMin = Carbon::now()
			->subHours(1)
			->subMinutes(1);
		$intervalMax = Carbon::now()->subHours(1);

		$cooldownsQuery = NotificationMessageCooldown::where(
			'created_at',
			'>=',
			$intervalMin
		)->where('created_at', '<=', $intervalMax);

		$cooldownsQuery->chunk(10, function ($cooldowns) {
			foreach ($cooldowns as $singleCooldown) {
				// Send the user notification
				if ($singleCooldown->user) {
					$singleCooldown->user->sendMessagesNotification();
				}
			}
		});

		// Delete the cooldowns
		$cooldownsQuery->delete();
	}
}
