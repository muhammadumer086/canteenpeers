<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('conversations', function (Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
		});

		Schema::create('messages', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('conversation_id');
			$table->unsignedInteger('user_id');
			$table->mediumText('message');
			$table->timestamps();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');

			$table->foreign('conversation_id')
			->references('id')->on('conversations')
			->onDelete('cascade');
		});

		Schema::create('conversation_user', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('conversation_id');
			$table->unsignedInteger('user_id');
			$table->timestamps();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');

			$table->foreign('conversation_id')
			->references('id')->on('conversations')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('messages');
		Schema::dropIfExists('conversations');
		Schema::dropIfExists('conversation_user');
	}
}
