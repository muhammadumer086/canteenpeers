import React from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import { IStoreState, quickLinksModalDisplay } from '../../redux/store';
import { IApiUser } from '../../interfaces';
import UserStat from '../general/UserStat';

interface IProps {
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	dispatch(action: any): void;
}

interface IState {
	anchorEl: any;
}

class HomeHero extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
		};

		this.handleQuickLinks = this.handleQuickLinks.bind(this);
	}

	public render() {
		if (!this.props.userData) {
			return;
		}

		const activeDiscussionData = {
			meta: {
				count: this.props.userData.active_discussions_count,
				label: 'Active Discussions',
				color: 'green',
			},
			link: {
				href: `/discussions?type=active`,
				as: `/discussions?type=active`,
			},
		};

		const followedDiscussionData = {
			meta: {
				count: this.props.userData.following_count,
				label: 'Followed Discussions',
				color: 'yellow',
			},
			link: {
				href: `/discussions?type=followed`,
				as: `/discussions?type=followed`,
			},
		};

		const savedResourcesData = {
			meta: {
				count: this.props.userData.saved_resources_count,
				label: 'Saved Resources',
				color: 'red',
			},
			link: {
				href: `/resources?type=saved`,
				as: `/resources?type=saved`,
			},
		};

		return (
			<section className="hero home_hero theme--accent">
				<div className="hero-heading font--h4">Welcome</div>
				<header className="home_hero-header">
					<h2 className="home_hero-title font--h2 hm-b8">
						Hi{' '}
						<span className="theme-text--accent">
							{this.props.userData.full_name}
						</span>
						,
					</h2>
					<p className="font--h4">
						Welcome to Canteen Connect for Parents.
					</p>
				</header>
				<footer className="home_hero-footer">
					<div className="home_hero-footer-action">
						<Button
							variant="contained"
							color="secondary"
							className="home_hero-footer-quick_links"
							onClick={this.handleQuickLinks}
						>
							<span className="home_hero-footer-quick_links-icon">
								<Icon>add</Icon>
							</span>
							<span className="home_hero-footer-quick_links-label">
								I'd like to
							</span>
						</Button>
					</div>

					<div className="home_hero-footer-stats">
						{this.props.userData.active_discussions_count > 0 && (
							<UserStat
								link={activeDiscussionData.link}
								stats={activeDiscussionData.meta}
							/>
						)}

						{this.props.userData.following_count > 0 && (
							<UserStat
								link={followedDiscussionData.link}
								stats={followedDiscussionData.meta}
							/>
						)}

						{this.props.userData.saved_resources_count > 0 && (
							<UserStat
								link={savedResourcesData.link}
								stats={savedResourcesData.meta}
							/>
						)}
					</div>
				</footer>
			</section>
		);
	}

	protected handleQuickLinks() {
		this.props.dispatch(quickLinksModalDisplay());
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { quickLinksModalDisplay } = state.quickLinksModal;
	return {
		userAuthenticated,
		userData,
		quickLinksModalDisplay,
	};
}

export default connect(mapStateToProps)(HomeHero);
