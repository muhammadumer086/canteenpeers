import React from 'react';
// import ReplyIcon from '@material-ui/icons/Reply';
import Pill from '../general/Pill';

interface IReplyTo {
	id: number;
	user: string;
}

interface IProps {
	action(event, replyTo?): void;
	replyTo: IReplyTo;
	count: number;
}

const DiscussionReplyAction: React.FC<IProps> = (props: IProps) => {
	const handleClick = (event, replyTo: IReplyTo) => {
		props.action(event, replyTo);
	};

	return (
		<React.Fragment>
			<Pill
				label="Reply"
				active={false}
				onClick={() => {
					handleClick(event, props.replyTo);
				}}
				disabled={false}
				clickable={true}
				isPost={true}
				isReply={true}
				showIfZero={true}
				statusCount={props.count}
			/>
		</React.Fragment>
	);
};

export default DiscussionReplyAction;
