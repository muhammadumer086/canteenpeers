<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use League\Csv\Writer;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

use App\Helpers\CsvMigration;
use App\Country;
use App\PersonalSetting;
use App\Situation;
use App\User;

use UserVerification;

class CsvMigrationUsers extends CsvMigration {
	/**
	 * The headers for the raw CSV
	 *
	 * @var array
	 */
	public $rawCsvHeaders = ['Is_YP', 'Email', 'ID'];
	/**
	 * The headers for the CSV
	 *
	 * @var array
	 */
	public $csvHeaders = [
		'id',
		'email',
		'created_at',
		'first_name',
		'last_name',
		'username',
		'situation',
		'active_treatment',
		'gender',
		'dob',
		'phone',
		'postcode',
		'state',
		'lat',
		'lng',
		'country_of_birth',
		'indigenous_australian',
		'avatar',
		'about',
	];

	protected $personalSettingIds = [];
	protected $preferences = null;
	protected $mapboxApiKey = null;
	protected $mapboxQueryCount = 0;

	public function __construct() {
		parent::__construct();

		$defaultSettings = PersonalSetting::getDefaultSettings();

		$settingsQuery = PersonalSetting::whereIn('slug', $defaultSettings);

		$this->personalSettingIds = $settingsQuery->pluck('id');

		$this->preferences = implode(',', $defaultSettings);
		$this->mapboxApiKey = env('MAPBOX_KEY', null);
	}

	public function processRecord(Writer $writer, $record) {
		if ($record['ID'] !== '#N/A' && intval($record['ID']) !== 0) {
			$id = $record['ID'];
			$userData = DB::connection('wordpress')
				->table('wp_users')
				->where('ID', $id)
				->first();

			if ($userData) {
				$result = $this->formatRecord($userData, $record);
				$writer->insertOne($result);
			}
		}
	}

	protected function formatRecord($data, $record) {
		$id = $data->ID;
		$email = strtolower($data->user_email);
		$createdAt = $data->user_registered;
		$username = $data->display_name;

		$firstname = $this->fetchUserMeta($id, 'first_name');
		$lastname = $this->fetchUserMeta($id, 'last_name');
		$situation = $this->fetchUserMeta($id, 'your_situation');
		$activeTreatment = $this->fetchUserMeta($id, 'in_active_treatment');
		$gender = strtolower($this->fetchUserMeta($id, 'gender'));
		$dob = $this->formatDob($this->fetchUserMeta($id, 'date_of_birth'));
		$phone = $this->fetchUserMeta($id, 'mobile_number');
		$postcode = $this->fetchUserMeta($id, 'autocomplete_address');
		$state = $this->fetchUserMeta($id, 'state');
		$latLng = $this->formatLatLng($postcode);
		$countryOfBirth = strtolower(
			$this->fetchUserMeta($id, 'cultural_background')
		);
		$indigenousAustralian = strtolower(
			$this->fetchUserMeta($id, 'aboriginal_torres_strait')
		);
		$avatar = $this->fetchUserMeta($id, 'avatar');
		$about = $this->fetchUserMeta($id, 'bio');

		return [
			$id,
			$email,
			$createdAt,
			$firstname,
			$lastname,
			$username,
			$situation,
			$activeTreatment,
			$gender,
			$dob,
			$phone,
			$postcode,
			$state,
			$latLng['lat'],
			$latLng['lng'],
			$countryOfBirth,
			$indigenousAustralian,
			$avatar,
			$about,
		];
	}

	protected function fetchUserMeta($userId, $meta, $default = '') {
		$value = DB::connection('wordpress')
			->table('wp_usermeta')
			->where('user_id', $userId)
			->where('meta_key', $meta)
			->value('meta_value');

		if ($value) {
			return $value;
		}

		return $default;
	}

	protected function formatLatLng($query) {
		$result = [
			'lat' => null,
			'lng' => null,
		];
		if (!empty($query)) {
			$url = "https://api.mapbox.com/geocoding/v5/mapbox.places/{$query}.json";

			if (Cache::has($url)) {
				$result = Cache::get($url);
			} else {
				// Perform query to mapbox
				$client = new Client();
				try {
					$response = $client->request('GET', $url, [
						'query' => [
							'access_token' => $this->mapboxApiKey,
							'country' => 'au,nz',
							'types' =>
								'region,district,postcode,locality,neighborhood',
							'limit' => 1,
						],
					]);

					if ($response->getStatusCode() === 200) {
						$body = $response->getBody();
						$rawResult = json_decode($body);
						if (
							$rawResult &&
							isset($rawResult->features) &&
							is_array($rawResult->features) &&
							sizeof($rawResult->features) &&
							isset($rawResult->features[0]->center) &&
							is_array($rawResult->features[0]->center) &&
							sizeof($rawResult->features[0]->center) >= 2
						) {
							$result = [
								'lat' => $rawResult->features[0]->center[0],
								'lng' => $rawResult->features[0]->center[1],
							];
							Cache::forever($url, $result);
						}
					}
				} catch (\Exception $e) {
					echo "\r\n\r\n" . $e->getMessage() . "\r\n";
				}

				++$this->mapboxQueryCount;
				// Logic to reduce the number of queries to mapbox per minutes
				if ($this->mapboxQueryCount % 10 === 0) {
					sleep(1);
				}
			}
		}

		return $result;
	}

	protected function formatDob($dob = '') {
		if (!empty($dob)) {
			try {
				return \Carbon\Carbon::createFromFormat('d/m/Y', $dob)->format(
					'Y-m-d'
				);
			} catch (\Exception $e) {
				echo "\r\n\r\nError parsing DoB: {$dob}\r\n\r\n";
			}
		}
		return '';
	}

	public function importData($record) {
		try {
			$user = User::updateOrCreate(
				[
					'email' => $record['email'],
				],
				[
					'migration_id' => $record['id'],
					'created_at' => $this->nullIfEmpty($record['created_at']),
					'first_name' => $record['first_name'],
					'last_name' => $record['last_name'],
					'username' => $record['username'],
					'gender' => $this->nullIfEmpty($record['gender']),
					'dob' => $this->nullIfEmpty($record['dob']),
					'phone' => $this->formatPhoneNumber($record['phone']),
					'location' => $record['postcode'],
					'location_data' => [],
					'latitude' => $this->nullIfEmpty($record['lat']),
					'longitude' => $this->nullIfEmpty($record['lng']),
					'state' => $record['state'],
					'verified' => false,
					'verification_token' => null,
					'inactive' => true,
					'password' => Hash::make(md5(rand() . '')),
					'indigenous_australian' => $this->formatIndigenousAustralian(
						$record['indigenous_australian']
					),
					'about' => $this->nullIfEmpty($record['about']),
					'migrated' => true,
					'preferences' => $this->preferences,
					'role_names' => 'user',
					'country_of_birth' => $this->formatCountryOfBirth(
						$record['country_of_birth']
					),
				]
			);

			$user->assignRole('user');

			// Associate the user with the personal settings
			$user->personalSettings()->sync($this->personalSettingIds);

			$user->save();

			$this->processSituation(
				$user,
				$this->nullIfEmpty($record['situation']),
				$this->nullIfEmpty($record['active_treatment'])
			);

			$this->processAvatar($user, $record['avatar']);
		} catch (\Exception $e) {
			echo "\r\n\r\nError creating user {$record['email']}: {$e->getMessage()}\r\n\r\n";
			return;
		}
	}

	protected function formatPhoneNumber($data) {
		$result = $this->nullIfEmpty($data);

		if ($result) {
			return substr($result, 20);
		}
		return null;
	}

	protected function formatIndigenousAustralian($data) {
		$result = $this->nullIfEmpty($data);

		if ($result) {
			if ($result === 'yes') {
				return true;
			} elseif ($result === 'no') {
				return false;
			}
		}

		return null;
	}

	protected function formatCountryOfBirth($data) {
		$result = $this->nullIfEmpty($data);

		if ($result) {
			$country = Country::where('name', 'like', "%$data%")->first();
			if ($country) {
				return $country->id;
			}
		}

		return null;
	}

	protected function processAvatar($user, $data) {
		$avatarPath = $this->nullIfEmpty($data);
		if ($avatarPath) {
			// Download the avatar
			$tmpImage = $this->downloadImage($avatarPath, false);
			if ($tmpImage) {
				try {
					$user->processUserAvatar($tmpImage);
					unlink($tmpImage);
				} catch (\Exception $e) {
					var_dump($e);
				}
			}
		}
	}

	protected function processSituation($user, $situationRaw, $treatment) {
		if ($situationRaw) {
			$searchSituation = null;
			if ($situationRaw === 'My parent has/had cancer') {
				$searchSituation = 'My parent has cancer';
			} elseif ($situationRaw === 'My sibling has/had cancer') {
				$searchSituation = 'My sibling has cancer';
			} elseif ($situationRaw === 'My parent died of cancer') {
				$searchSituation = 'My parent died from cancer';
			} elseif ($situationRaw === 'My sibling died of cancer') {
				$searchSituation = 'My sibling died from cancer';
			} elseif ($situationRaw === 'I have/had cancer') {
				$searchSituation = 'I\'m a young person with cancer';
				if ($treatment && $treatment === 'Finished treatment') {
					$searchSituation = 'I survived cancer';
				}
			}

			if ($searchSituation) {
				$situation = Situation::whereNull('parent_id')
					->where('name', $searchSituation)
					->firstOrFail();

				$user->situations()->sync([$situation->id]);
			}
		}
	}
}
