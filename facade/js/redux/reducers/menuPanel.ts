import { actionTypes } from '../actions';

export interface IStoreState {
	menuPanelDisplay: boolean;
}

export const menuPanel = (state: IStoreState = {
	menuPanelDisplay: false,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.MENU_PANEL_DISPLAY:
			return Object.assign({}, state, {
				menuPanelDisplay: true,
			});
		case actionTypes.USER_LOGOUT: // hide panel on logout
		case actionTypes.MENU_PANEL_HIDE:
			return Object.assign({}, state, {
				menuPanelDisplay: false,
			});
		default:
			return state;
	}
}
