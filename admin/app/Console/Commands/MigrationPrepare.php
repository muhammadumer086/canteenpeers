<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Writer;

use App\Helpers\CsvMigration;
use App\Helpers\CsvMigrationBlogs;
use App\Helpers\CsvMigrationDiscussions;
use App\Helpers\CsvMigrationUsers;

class MigrationPrepare extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:migration:prepare';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Prepare Canteen\'s content migration';

	protected $rawCsvData = [];

	protected $fileMigration = null;

	protected function init() {
		$migrationUsers = new CsvMigrationUsers();
		$migrationDiscussions = new CsvMigrationDiscussions();
		$migrationBlogs = new CsvMigrationBlogs();

		$this->rawCsvData = [
			'users' => [
				'file' => 'migration_users.csv',
				'columns' => $migrationUsers->rawCsvHeaders,
				'reader' => null,
				'migrator' => $migrationUsers,
			],
			'discussions' => [
				'file' => 'migration_discussions.csv',
				'columns' => $migrationDiscussions->rawCsvHeaders,
				'reader' => null,
				'migrator' => $migrationDiscussions,
			],
			'blogs' => [
				'file' => 'migration_blogs.csv',
				'columns' => $migrationBlogs->rawCsvHeaders,
				'reader' => null,
				'migrator' => $migrationBlogs,
			],
		];
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->init();
		if ($this->performPreTests()) {
			// Handle the migration CSVs
			$this->processCSV('users');
			$this->processCSV('discussions');
			$this->processCSV('blogs');
		}
	}

	/**
	 * Execute the pre-test making sure everything is ready
	 *
	 * @return boolean
	 */
	protected function performPreTests() {
		$this->info('Starting pre tests');
		$tests = ['testWordpressDB', 'testCSVs', 'testMapbox'];

		foreach ($tests as $testName) {
			if (!$this->$testName()) {
				$this->comment('Stopped pre tests');
				return false;
			}
		}

		$this->info('Finished pre tests');
		return true;
	}

	/**
	 * Test if the wordpress database is available
	 *
	 * @return boolean
	 */
	protected function testWordpressDB() {
		$message = 'Testing Wordpress DB';

		try {
			DB::connection('wordpress')->getPdo();
			$this->displayStatus($message, true);
		} catch (\Exception $e) {
			$this->displayStatus($message, false);
			$this->error($e->getMessage());
			return false;
		}

		return true;
	}

	/**
	 * Test if the CSVs are available and have the required columns
	 *
	 * @return boolean
	 */
	protected function testCSVs() {
		$this->line('Testing CSV files presence:');

		foreach ($this->rawCsvData as &$fileData) {
			$fileName = $fileData['file'];
			$columns = $fileData['columns'];
			$message = "* Testing {$fileName}";
			$fileExists = Storage::disk('migration_raw')->exists($fileName);

			if (!$fileExists) {
				$this->displayStatus($message, false);
				$this->error('File missing');
				return false;
			}

			// Initialise the CSV reader
			$reader = Reader::createFromPath(
				Storage::disk('migration_raw')->path($fileName),
				'r'
			);
			$reader->setHeaderOffset(0);
			$headers = $reader->getHeader();
			// Test the columns
			foreach ($columns as $columnName) {
				if (!in_array($columnName, $headers)) {
					$this->displayStatus($message, false);
					$this->error("Missing column '{$columnName}'");
					return false;
				}
			}

			$fileData['reader'] = $reader;

			$this->displayStatus($message, true);
		}

		return true;
	}

	protected function testMapbox() {
		$message = 'Testing Mapbox key presence';

		if (env('MAPBOX_KEY', null)) {
			$this->displayStatus($message, true);
			return true;
		} else {
			$this->displayStatus($message, false);
			$this->error(
				'Please make sure MAPBOX_KEY is present in your environment variables'
			);
			return false;
		}
	}

	/**
	 * Display a test status
	 *
	 * @param string $message the name of the test
	 * @param boolean $success set to true if the test failed
	 *
	 * @return void
	 */
	protected function displayStatus(string $message, $success) {
		$statusLength = 50;

		$fullMessage = str_pad($message, $statusLength);
		if ($success) {
			$fullMessage .= '[<fg=green>OK</>]';
		} else {
			$fullMessage .= '[<fg=red>FAIL</>]';
		}

		$this->line($fullMessage);
	}

	protected function processCSV($type) {
		$this->info("Processing {$type}");
		$migrator = $this->rawCsvData[$type]['migrator'];
		$reader = $this->rawCsvData[$type]['reader'];
		$bar = $this->output->createProgressBar(count($reader));
		$csvPath = storage_path("migration/{$type}.csv");

		$writer = Writer::createFromPath($csvPath, 'w');

		$writer->insertOne($migrator->csvHeaders);

		$records = $reader->getRecords();

		$bar->start();
		foreach ($records as $record) {
			$migrator->processRecord($writer, $record);
			$bar->advance();
		}

		$bar->finish();
		$this->line('');
		$this->info("Finished processing {$type}");
	}
}
