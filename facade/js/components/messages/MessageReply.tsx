import React from 'react';
import ReactSVG from 'react-svg';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { Formik } from 'formik';

import Button from '@material-ui/core/Button';

import { IStoreState } from '../../redux/store';

import { IApiUser } from '../../interfaces';

import { RichTextEditor } from '../inputs';
import UserAvatar from '../general/UserAvatar';
import { GTMDataLayer } from './../../../js/helpers/dataLayer';

interface IProps {
	user: IApiUser;
	userAuthenticated: boolean | null;
	isReplyOpen: boolean | null;
	clearMessageData: boolean;
	submitReply(values: any): Promise<boolean>;
}

interface IState {
	userAuthenticated: boolean | null;
	submitting: boolean;
}

class MessageReply extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			userAuthenticated: props.userAuthenticated,
			submitting: false,
		};
	}

	public render() {
		const GTM = new GTMDataLayer();

		const validationFormat = Yup.object().shape({
			message: Yup.string()
				.required('A valid reply is required')
				.max(5000),
		});

		const userReplyClasses = ['user_reply', 'user_reply--open'];

		return (
			<div className={userReplyClasses.join(' ')}>
				<Formik
					initialValues={{
						message: '',
					}}
					validationSchema={validationFormat}
					onSubmit={(values, { resetForm, setValues }) => {
						this.setState(
							{
								submitting: true,
							},
							() => {
								this.props.submitReply(values).then(result => {
									if (result) {
										resetForm({
											message: '',
										});
										setValues({
											message: '',
										});

										// Push Event to GTM
										GTM.pushEventToDataLayer({
											event: 'sendMessage',
										});
									}
									this.setState({
										submitting: false,
									});
								});
							},
						);
					}}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};

						if (
							this.props.clearMessageData &&
							(inputProps.values.message !== '' ||
								!!inputProps.values.message)
						) {
							inputProps.setFieldValue('message', '');
						}

						return (
							<form
								className="user_reply-form form"
								onSubmit={handleSubmit}
							>
								<div className="user_reply-author">
									<div className="user_reply-author_avatar">
										<UserAvatar user={this.props.user} />
									</div>
								</div>

								<div className="user_reply-form_textarea">
									<RichTextEditor
										toolbarId="message-toolbar"
										name="message"
										value={inputProps.values.message}
										profile={[
											'imageUpload',
											'embedVideo',
											'insertGif',
										]}
										bounds=".user_reply-form_textarea"
										scrollingContainer=".user_reply-form_textarea"
										{...inputProps}
									/>
								</div>

								<div className="user_reply-form_submit">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid || this.state.submitting
										}
									>
										<ReactSVG
											path={`${
												process.env.STATIC_PATH
											}/icons/reply.svg`}
										/>
									</Button>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(MessageReply);
