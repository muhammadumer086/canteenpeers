import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import { Switch } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Icon from '@material-ui/core/Icon';

import { IApiUser } from '../../interfaces';
import { EAccountScreens, ETabs } from './UserPanelAccount';
import { validationsUser } from '../../helpers/validations';
import { TextFieldFormik, AutocompletePostcodeFormik } from '../inputs';
import isAdmin from '../../../js/helpers/isAdmin';
import { connect } from 'react-redux';
import { ApiService } from '../../services/ApiService';
import { IStoreState, apiError, apiSuccess } from '../../redux/store';
interface IProps {
	userData: IApiUser | null;
	apiService: ApiService;
	loggedInUserData: IApiUser;
	submitting: boolean;
	allPermission: boolean;
	changeScreen(tab: ETabs, screen: EAccountScreens);
	onSubmit(values: any, resetForm?: () => void);
	dispatch(action: any): void;
}

interface IState {
	userVerified: boolean;
	isUpdating: boolean;
}

class UserAccountPrivateDetails extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);
		this.state = {
			userVerified:
				props.userData.verified || props.userData.is_mobile_verified,
			isUpdating: false,
		};

		this.handleVerifiedChange = this.handleVerifiedChange.bind(this);
	}
	public render() {
		const validationFormat = Yup.object().shape({
			email: validationsUser.email,
			phone: validationsUser.phone,
			location: validationsUser.location,
			location_data: validationsUser.location_data,
		});
		const { userVerified, isUpdating } = this.state;
		return (
			<div className="user_account_panel user_account_panel--account_private_details">
				<div className="user_panel-back">
					<ButtonBase
						focusRipple={true}
						className="user_panel-back-button"
						onClick={() => {
							this.props.changeScreen(
								ETabs.MY_PROFILE,
								EAccountScreens.OVERVIEW,
							);
						}}
						disabled={this.props.submitting}
					>
						<Icon className="user_panel-back-back_icon">
							arrow_back
						</Icon>
						Profile Overview
					</ButtonBase>
				</div>
				<div className="user_panel-heading">
					<h4 className="font--h4 theme-title">Private Details</h4>
				</div>
				<Formik
					initialValues={{
						first_name: this.props.userData.first_name,
						last_name: this.props.userData.last_name,
						email: this.props.userData.email,
						phone: this.props.userData.phone || '',
						location: this.props.userData.location || '',
						location_data: this.props.userData.location_data,
						user_type: this.props.userData.role_names[0],
					}}
					validationSchema={validationFormat}
					onSubmit={values => {
						this.props.onSubmit(values);
					}}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							isValid,
							setFieldValue,
							setFieldTouched,
						};

						return (
							<form className="form" onSubmit={handleSubmit}>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="First Name"
											name="first_name"
											disabled={
												this.props.submitting ||
												!isAdmin(
													this.props.loggedInUserData,
												)
											}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Last Name"
											name="last_name"
											disabled={
												this.props.submitting ||
												!isAdmin(
													this.props.loggedInUserData,
												)
											}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="email"
											label="Email"
											name="email"
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="tel"
											label="Phone"
											name="phone"
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<AutocompletePostcodeFormik
											type="postcode"
											label="Postcode"
											name="location"
											name_data="location_data"
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								{this.props.userData.id !==
									this.props.loggedInUserData.id &&
									this.props.allPermission && (
										<div className="form-row">
											<div className="form-column">
												<FormControl
													className="form-input"
													disabled={isUpdating}
												>
													<FormGroup>
														<FormControlLabel
															control={
																<Switch
																	name={
																		'verified'
																	}
																	onChange={
																		this
																			.handleVerifiedChange
																	}
																	checked={
																		userVerified
																	}
																	value={
																		userVerified
																	}
																/>
															}
															label={
																'Account Verified'
															}
															labelPlacement="top"
															className="form-input-switch"
														/>
													</FormGroup>
												</FormControl>
											</div>
										</div>
									)}
								<div className="form-row form-row--submit_left">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid ||
												!touched ||
												this.props.submitting
											}
										>
											Update
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}

	protected handleVerifiedChange = e => {
		if (this.state.userVerified) {
			return;
		}
		this.setState(
			{ isUpdating: true, userVerified: e.target.checked },
			() => {
				const url = e.target.checked
					? `/api/users/${this.props.userData.username}/verified`
					: `/api/users/${this.props.userData.username}/unverified`;
				this.props.apiService
					.queryPUT(url, {})
					.then(_res => {
						this.setState({ isUpdating: false });
						this.props.dispatch(
							apiSuccess(['User account updated.']),
						);
					})
					.catch(_e => {
						this.setState({
							isUpdating: false,
							userVerified: !this.state.userVerified,
						});
						this.props.dispatch(
							apiError(['User account not updated.']),
						);
					});
			},
		);
	};
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		loggedInUserData: userData,
	};
}

export default connect(mapStateToProps)(UserAccountPrivateDetails);
