<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DiscussionReported extends Mailable {
	use Queueable, SerializesModels;

	public $discussion;
	public $author;
	public $ageSensitive;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($discussion, $author, $ageSensitive) {
		$this->discussion = $discussion;
		$this->author = $author;
		$this->ageSensitive = $ageSensitive;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		$isReply = $this->discussion->main_discussion_id !== null;

		$mainDiscussion = $isReply
			? $this->discussion->mainDiscussion
			: $this->discussion;

		$title = "Someone reported {$mainDiscussion->title}";
		if ($this->ageSensitive) {
			$title = "Someone flagged {$mainDiscussion->title} as age-sensitive";
		} elseif ($isReply) {
			$title = "Someone reported a reply in {$mainDiscussion->title}";
		}

		return $this->view('emails.discussion-reported')
			->subject($title)
			->with([
				'discussion' => $this->discussion,
				'mainDiscussion' => $mainDiscussion,
				'author' => $this->author,
				'ageSensitive' => $this->ageSensitive,
				'isReply' => $isReply,
			]);
	}
}
