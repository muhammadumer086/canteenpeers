<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

use League\Csv\Writer;
use Carbon\Carbon;
use Password;

class MigrationNotifyCSV extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:migration:notify-csv {user? : The user ID to notify}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a CSV of notification links';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		// Select all the migrated users that are inactive
		$query = User::where('migrated', true)->where('verified', false);

		if ($this->argument('user')) {
			$userId = intval($this->argument('user'));
			$query = User::where('id', $userId);
		}

		$this->line('');
		$this->info('Creating the CSV');
		$date = Carbon::now();
		$csvPath = storage_path("migration_notify/users-" . $date->format('Y-m-d_Hi') . ".csv");

		$writer = Writer::createFromPath($csvPath, 'w');
		$writer->insertOne([
			'id',
			'old id',
			'first name',
			'last name',
			'username',
			'email',
			'link',
		]);

		$bar = $this->output->createProgressBar($query->count());
		$bar->start();
		$bar->setRedrawFrequency(10);

		$query->chunk(10, function ($users) use ($bar, $writer) {
			foreach ($users as $user) {
				// Refresh the password reset token
				$token = Password::getRepository()->create($user);

				$link = config('canteen.facade_url') .
				'/auth/migrated?' .
				http_build_query([
					'token' => $token,
					'email' =>$user->email,
				]);

				$writer->insertOne([
					$user->id,
					$user->migration_id,
					$user->first_name,
					$user->last_name,
					$user->username,
					$user->email,
					$link,
				]);

				$bar->advance();
			}
		});
		$bar->finish();

		$this->info("\r\nFinished creating the CSV");
		$this->line('');
	}
}
