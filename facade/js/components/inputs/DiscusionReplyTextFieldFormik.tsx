import React from 'react';

import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles';

interface IProps {
	type: string;
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	disabled?: boolean;
	multiline?: boolean;
	classes?: any;
	placeholder?: string;
	handleChange(e: React.ChangeEvent<any>): void;
	handleBlur(e: any): void;
}

const styles = theme => ({
	root: {
		backgroundColor: '#fff',
		borderRadius: '0',
		height: 'auto',
		padding: '16px 10px',
		minHeight: '72px',
		border: '0',
		fontSize: '14px',
		lineHeight: '24px',
		[theme.breakpoints.down('sm')]: {
			lineHeight: '16px',
			padding: '0 10px',
			minHeight: '40px',
		},
	},
	input: {
		height: '100%',
		padding: '17px 0',
		color: '#4C4D4F',
		lineHeight: '26px',
		boxsizing: 'border-box',
		fontsize: '14px',
	},
	inputMultiline: {
		[theme.breakpoints.down('sm')]: {
			height: '50px',
			minHeight: '26px',
		},
	},
});

const DiscusionReplyTextFieldFormik: React.FC<IProps> = (props: IProps) => {
	return (
		<Input
			className="form-input"
			type={props.type}
			name={props.name}
			onChange={props.handleChange}
			onBlur={props.handleBlur}
			multiline={props.multiline}
			rowsMax={15}
			value={props.values[props.name]}
			disableUnderline={true}
			autoFocus={true}
			placeholder={props.placeholder}
			error={props.touched[props.name] && !!props.errors[props.name]}
			classes={{
				root: props.classes.root,
				multiline: props.classes.multiline,
				inputMultiline: props.classes.inputMultiline,
			}}
		/>
	);
};

export default withStyles(styles)(DiscusionReplyTextFieldFormik);
