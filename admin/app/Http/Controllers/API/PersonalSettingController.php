<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\PersonalSetting;
use App\Http\Resources\PersonalSetting as PersonalSettingResource;

class PersonalSettingController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$settingsQuery = PersonalSetting::orderBy('order', 'ASC');
		if (!$request->has('type') || $request->input('type') !== 'all') {
			$settingsQuery->whereNull('type');
		}
		// Use the default ID order
		$settings = $settingsQuery->get();

		return response()->json([
			'personal-settings' => PersonalSettingResource::collection(
				$settings
			),
		]);
	}
}
