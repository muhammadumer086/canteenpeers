<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Validator;
use UserActivityHelper;
use UserPermissionHelper;
use App\User;
use App\Discussion;
use App\ReportDiscussion;
use App\Http\Resources\ReportDiscussion as ReportDiscussionResource;
use App\Http\Resources\ReportDiscussionCollection;

class ReportDiscussionController extends Controller {
	const PAGINATION_SIZE = 20;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$request = request();
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["reported_discussions_read","reported_discussions_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$reportDiscussions = ReportDiscussion::orderBy('created_at', 'ASC')
			->whereIn(
				'id',
				ReportDiscussion::selectRaw('min(id) as group_id')
					->groupBy('discussion_id')
					->pluck('group_id')
			)
			->paginate(self::PAGINATION_SIZE);

		return new ReportDiscussionCollection($reportDiscussions);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'title' => 'bail|required|string|max:191',
			'comment' => 'bail|nullable|string|max:2000',
			'age_sensitive' => 'bail|required|boolean',
			'discussion' => 'bail|required|exists:discussions,id',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($request->input('discussion'));

		// Test if the users doesn't report his own discussion
		if ($authUser->id === $discussion->user->id) {
			return response()->json(
				[
					'error' => "You can't report your own posts",
				],
				400
			);
		}

		// Test if the user hasn't already reported the discussion
		if (
			ReportDiscussion::withTrashed()
				->where('user_id', $authUser->id)
				->where('discussion_id', $discussion->id)
				->exists()
		) {
			return response()->json(
				[
					'error' => "You've already reported this post",
				],
				400
			);
		}

		$reportDiscussion = new ReportDiscussion();

		$reportDiscussion->title = $request->input('title');
		$reportDiscussion->comment = $request->input('comment', null);
		$reportDiscussion->age_sensitive = $request->input('age_sensitive');
		$reportDiscussion->user()->associate($authUser->id);

		$discussion->reportDiscussions()->save($reportDiscussion);

		UserActivityHelper::create(
			$discussion->user,
			$discussion,
			'Discussion Reported',
			null
		);
		UserActivityHelper::create(
			$authUser,
			$discussion,
			'Reported Discussion',
			null
		);

		// Notify all the admins
		$ageSensitive = $request->input('age_sensitive');
		$adminEmail = new \App\Helpers\AdminEmail();
		$adminEmail->sendDiscussionReported(
			$discussion,
			$authUser,
			$ageSensitive
		);
		User::role('admin')->chunk(100, function ($admins) use (
			$discussion,
			$ageSensitive,
			$authUser
		) {
			foreach ($admins as $adminUser) {
				if ($adminUser->id !== $authUser->id) {
					$adminUser->sendDiscussionReportNotification(
						$discussion,
						$authUser,
						$ageSensitive
					);
				}
			}
		});

		return response()->json([
			'report_discussion' => new ReportDiscussionResource(
				$reportDiscussion
			),
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\ReportDiscussion  $reportDiscussion
	 * @return \Illuminate\Http\Response
	 */
	public function show(ReportDiscussion $reportDiscussion) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\ReportDiscussion  $reportDiscussion
	 * @return \Illuminate\Http\Response
	 */
	public function update(
		Request $request,
		ReportDiscussion $reportDiscussion
	) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["reported_discussions_edit","reported_discussions_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$validator = Validator::make($request->all(), [
			'reviewed' => 'bail|boolean',
		]);

		if ($request->has('reviewed')) {
			if ($request->input('reviewed')) {
				if ($reportDiscussion->reviewer) {
					return response()->json(
						[
							'error' => "Already reviewed by $reportDiscussion->reviewer->full_name",
						],
						400
					);
				}
				$reportDiscussion->reviewer()->associate($authUser->id);
			} else {
				// Remove the reviewer
				$reportDiscussion->reviewer()->dissociate();
			}
		}

		$reportDiscussion->save();

		return response()->json([
			'report_discussion' => new ReportDiscussionResource(
				$reportDiscussion
			),
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\ReportDiscussion  $reportDiscussion
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(ReportDiscussion $reportDiscussion) {
		$request = request();
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["reported_discussions_delete","reported_discussions_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$reportDiscussion->delete();

		return response()->json([
			'success' => true,
		]);
	}
}
