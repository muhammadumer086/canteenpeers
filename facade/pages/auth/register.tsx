import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { connect } from 'react-redux';

import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';

import RegisterForm from '../../js/components/auth/RegisterForm';
import { ApiService, AuthService } from '../../js/services';
import { IApiUser, IApiSituation } from '../../js/interfaces';
import {
	apiError,
	userRegistered,
	IStoreState,
	//  apiSuccess,
} from '../../js/redux/store';
import { GTMDataLayer } from '../../js/helpers/dataLayer';

interface IProps {
	query: any | null;
	situations: IApiSituation[];
	situationsLoading: boolean;
	apiService: ApiService;
	authService: AuthService;
	userData: IApiUser | null;
	userAuthenticated: boolean;
	dispatch(action: any): void;
}

interface IState {
	loading: boolean;
	submitting: boolean;
	registered: boolean;
	userRawData: any | null;
	userNameTaken: boolean;
	emailTaken: boolean;
}

class Register extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();
	static getInitialProps({ query }) {
		return {
			query,
		};
	}

	constructor(props) {
		super(props);

		this.state = {
			loading: true,
			submitting: false,
			userRawData: null,
			userNameTaken: false,
			emailTaken: false,
			registered: false,
		};

		this.onSubmit = this.onSubmit.bind(this);
		this.verifyByEmail = this.verifyByEmail.bind(this);
		this.verifyByPhone = this.verifyByPhone.bind(this);
		this.loadUserData = this.loadUserData.bind(this);
		this.updateRoute = this.updateRoute.bind(this);
		this.handleLogout = this.handleLogout.bind(this);
		this.dispatchError = this.dispatchError.bind(this);
	}

	get registrationToken(): string | null {
		if (this.hasRegistrationToken) {
			return this.props.query.token + '';
		}
		return null;
	}

	get hasRegistrationToken(): boolean {
		return this.props.query && 'token' in this.props.query;
	}

	public componentDidMount() {
		this.setState(
			{
				loading: this.hasRegistrationToken,
			},
			() => {
				if (this.hasRegistrationToken) {
					// load user data
					this.loadUserData();
				}
			},
		);
		if (this.props.userAuthenticated) {
			this.handleLogout();
		}
	}
	// public componentDidUpdate(prevP: IProps) {
	//  if (prevP.query !== this.props.query) {
	//    console.log(this.props.query);
	//  }
	// }
	public render() {
		let options = [];
		const { userNameTaken, emailTaken, registered } = this.state;
		const { userData } = this.props;
		if (this.props.situations && this.props.situations.length) {
			options = this.props.situations
				.filter(situation => {
					return situation.parent_situation === null;
				})
				.map(situation => {
					return {
						value: situation.slug,
						label: situation.name,
					};
				});
		}

		return (
			<div className="auth_wrapper theme--accent_dark">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<section className="auth">
					{registered && (
						<img
							className="register_verify-logo"
							src={`${process.env.STATIC_PATH}/images/logo-canteen-horizontal.png`}
						/>
					)}
					<header className="auth-header">
						{registered ? (
							<h1 className="auth-title font--h4 register_verify-main-heading">
								Hi {userData && userData.username}, thanks for
								registering to Canteen Connect!
							</h1>
						) : (
							<h1 className="auth-title font--h4">
								Join Canteen Connect
							</h1>
						)}
					</header>
					<div className="auth-content theme--main">
						{this.state.submitting ||
							(this.state.loading && (
								<LinearProgress
									color="secondary"
									className="auth-content-progress"
								/>
							))}
						{this.props.situationsLoading ||
						!this.props.situations.length ? (
							<div className="auth-content-loading">
								<CircularProgress />
							</div>
						) : (
							<RegisterForm
								userRawData={this.state.userRawData}
								situations={options}
								onSubmit={this.onSubmit}
								verifyByPhone={this.verifyByPhone}
								verifyByEmail={this.verifyByEmail}
								emailTaken={emailTaken}
								userNameTaken={userNameTaken}
								submitting={
									this.state.submitting || this.state.loading
								}
								dispatchError={this.dispatchError}
								apiService={this.props.apiService}
								registered={registered}
								updateRoute={this.updateRoute}
							/>
						)}

						{!registered && (
							<div className="auth-content-footer font--small">
								Already have an account?{' '}
								<Link href="/auth/login">
									<a>Log In</a>
								</Link>
							</div>
						)}
					</div>
					{!registered && (
						<footer className="auth-footer font--small">
							Back to{' '}
							<Link href="/">
								<a>{process.env.APP_NAME}</a>
							</Link>
						</footer>
					)}
				</section>
			</div>
		);
	}

	protected onSubmit(values) {
		this.setState(
			{
				submitting: true,
				userNameTaken: false,
				emailTaken: false,
			},
			() => {
				if (this.state.userRawData) {
					values.sos_registration = true;
				}
				this.props.apiService
					.queryPOST('/api/users', values)
					.then((data: any) => {
						if ('user' in data) {
							// Success
							this.setState({
								registered: true,
								submitting: false,
							});
							this.props.dispatch(
								userRegistered(data.user as IApiUser),
							);
							Router.push(
								{
									pathname: '/auth/register',
									query: { step: 'choice' },
								},
								'/auth/register/verificationchoice',
							);
						} else {
							this.setState({ submitting: false });
							throw new Error('Invalid data received');
						}
					})
					.catch(error => {
						if (
							error.message ===
							'The email has already been taken.'
						) {
							this.setState({
								submitting: false,
								emailTaken: true,
							});
						} else if (
							error.message ===
							'The username has already been taken.'
						) {
							this.setState({
								submitting: false,
								userNameTaken: true,
							});
						} else {
							this.setState({
								submitting: false,
							});
							// dispatch the error message
						}
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected loadUserData() {
		this.props.apiService
			.queryGET(`/api/sideofstage/register/${this.registrationToken}`)
			.catch(error => {
				this.setState({
					loading: false,
				});
				console.log('API error when fetching side of stage user');
				console.warn(error);
			})
			.then(response => {
				// Success
				this.setState({
					userRawData: response,
					loading: false,
				});
			});
	}

	protected verifyByPhone = () => {
		Router.push(
			'/auth/verifybysms?step=step-1',
			'/auth/verifybysms?step-1',
		);

		// this.setState({ submitting: true }, () => {
		//  const url = `/api/users/smscode`;
		//  const body = {
		//    email: this.props.userData.email,

		//  };
		//  this.props.apiService
		//    .queryPOST(url, body)
		//    .then(_res => {
		//      this.setState({ submitting: false });
		//      this.props.dispatch(apiSuccess(['Message sent.']));
		//      Router.push('/auth/verifybysms');
		//    })
		//    .catch(e => {
		//      this.setState({ submitting: false });
		//      this.props.dispatch(apiError([e.message]));
		//    });
		// });
	};

	protected verifyByEmail = () => {
		Router.push('/auth/verifybyemail');

		// this.setState({ submitting: true }, () => {
		// 	this.props.apiService
		// 		.queryPOST('/api/users/resend-verification', {
		// 			email: this.props.userData.email,
		// 		})
		// 		.catch(error => {
		// 			this.setState({
		// 				submitting: false,
		// 			});
		// 			// dispatch the error message
		// 			this.props.dispatch(apiError([error.message]));
		// 		})
		// 		.then(() => {
		// 			// Success
		// 			this.setState({
		// 				submitting: false,
		// 			});
		// 			this.props.dispatch(apiSuccess(['Email sent.']));
		// 			Router.push('/auth/verifybyemail');
		// 		});
		// });
	};

	protected updateRoute = (url: any, as?: string) => {
		Router.push(url, as);
	};
	protected handleLogout = () => {
		this.GTM.pushEventToDataLayer({
			event: 'userLogout',
			login: 'No',
		});
		this.props.authService.logOut();
	};
	protected dispatchError = (
		message: string,
		email?: string,
		age?: number,
	) => {
		this.props.dispatch(apiError([message]));
		if (email) {
			this.GTM.pushEventToDataLayer({
				event: 'userRegisterationFailed',
				email,
				age,
			});
		}
	};
}

function mapStateToProps(state: IStoreState) {
	const { situations, loading } = state.situations;
	const { userData, userAuthenticated } = state.user;
	return {
		situations,
		situationsLoading: loading,
		userData,
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(Register);
