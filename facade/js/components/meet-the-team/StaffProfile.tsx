import React from 'react';
import { Avatar } from '@material-ui/core';
import { IApiStaff } from '../../../js/interfaces';

interface IProps {
	staffData: IApiStaff | null;
}

const StaffProfile: React.StatelessComponent<IProps> = props => {
	
	return (
		<div className="staff_profile">
			<div className="staff_profile-details">
				<div className="staff_profile-image_container">
					{!!props.staffData.feature_image && (
						<Avatar
							className="staff_profile-image"
							src={props.staffData.feature_image.url}
						/>
					)}
				</div>

				<span className="staff_profile-name">
					{props.staffData.first_name} {props.staffData.last_name}
				</span>

				<span className="staff_profile-role">
					{props.staffData.role}
				</span>
			</div>

			<div className="staff_profile-biography">
				<p>{props.staffData.biography}</p>
			</div>
		</div>
	);
};

export default StaffProfile;
