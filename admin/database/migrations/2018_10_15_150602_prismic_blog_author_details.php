<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrismicBlogAuthorDetails extends Migration {
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->string('prismic_author')->nullable();
			$table->text('prismic_author_image')->nullable();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->dropColumn([
				'prismic_author',
				'prismic_author_image',
			]);
		});
	}
}
