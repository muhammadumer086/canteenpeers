<?php
namespace App\Helpers;

use Log;
use GuzzleHttp\Client;
use Carbon\Carbon;

/**
 * Manage the subscriptions to campaign monitor
 */
class CampaignMonitorSubscription {
	protected $australianStates = [
		'ACT',
		'NSW',
		'QLD',
		'VIC',
		'NT',
		'WA',
		'SA',
		'TAS',
	];

	/**
	 * Subscribe a user to Campaign Monitor
	 *
	 * @param string $email
	 * @param string $firstName
	 * @param string $lastName
	 * @param boolean $registered (default true)
	 * @return void
	 */
	public function subscribeUserToCampaignMonitor(
		$email,
		$firstName,
		$lastName,
		$locationData,
		$dob,
		$situations=[],
		$registered = true,
		$verified = false,
		$fieldsArr = []
	) {
		$customFields = [];
		/*My parent has cancer = Offspring
		My sibling has cancer = Sibling
		I’m a young person with cancer = Patient
		My parent died from cancer = Bereaved
		My sibling died from cancer = Bereaved
		My parent survived cancer = Offspring
		My sibling survived cancer = Sibling
		I survived cancer = Survivor*/
		//dd("testing");
		$customFields[] = [
			'Key' => 'CanteenConnectCancerConnection',
			'Value' => ''
		];
		$canteenConnectCancerConnection = [];
		$arrKey = [];
		foreach ($situations as $situation)
		{
			$sit = str_replace("'", '', $situation['name']);
			switch ($sit){
				case 'My parent has cancer':
					if(!in_array('Offspring', $arrKey)) {
						$customFields[] = [
							'Key' => 'CanteenConnectCancerConnection',
							'Value' => 'Offspring'
						];
						$arrKey[] = 'Offspring';
					}
					break;
				case 'My sibling has cancer':
					if(!in_array('Sibling', $arrKey)) {
						$customFields[] = [
							'Key' => 'CanteenConnectCancerConnection',
							'Value' => 'Sibling'
						];
						$arrKey[] = 'Sibling';
					}
					break;//""
				case 'Im a young person with cancer':
					if(!in_array('Patient', $arrKey)) {
						$customFields[] = [
							'Key' => 'CanteenConnectCancerConnection',
							'Value' => 'Patient'
						];
						$arrKey[] = 'Patient';


					}
					break;

				case 'My parent died from cancer':
					if(!in_array('Bereaved', $arrKey)) {
					$customFields[] = [
						'Key' => 'CanteenConnectCancerConnection',
						'Value' => 'Bereaved'
					];
					$arrKey[] =  'Bereaved';
					}
					break;
				case 'My sibling died from cancer':
					if(!in_array('Bereaved', $arrKey)) {
						$customFields[] = [
							'Key' => 'CanteenConnectCancerConnection',
							'Value' => 'Bereaved'
						];
						$arrKey[] = 'Bereaved';
					}
					break;
				case 'My parent survived cancer':
					if(!in_array('Offspring', $arrKey)) {
						$customFields[] = [
							'Key' => 'CanteenConnectCancerConnection',
							'Value' => 'Offspring'
						];
						$arrKey[] = 'Offspring';
					}
					break;
				case 'My sibling survived cancer':
					if(!in_array('Sibling', $arrKey)) {
						$customFields[] = [
							'Key' => 'CanteenConnectCancerConnection',
							'Value' => 'Sibling'
						];
						$arrKey[] = 'Sibling';
					}
					break;
				case 'I survived cancer':
					if(!in_array('Survivor', $arrKey)) {
					$customFields[] = [
						'Key' => 'CanteenConnectCancerConnection',
						'Value' => 'Survivor'
					];
					$arrKey[] =  'Survivor';
					}
					break;
			}
		}

		// Process the address component
		if (
			isset($locationData, $locationData['address_components']) &&
			is_array($locationData['address_components']) &&
			sizeof($locationData['address_components'])
		) {
			foreach ($locationData['address_components'] as $addressComponent) {
				if (
					isset(
						$addressComponent['types'],
						$addressComponent['long_name'],
						$addressComponent['short_name']
					) &&
					is_array($addressComponent['types']) &&
					sizeof($addressComponent['types'])
				) {
					// Test if suburb
					if (
						$this->addressHasTypes($addressComponent['types'], [
							'locality',
							'political',
						])
					) {
						$customFields[] = [
							'Key' => 'Suburb',
							'Value' => $addressComponent['long_name'],
						];
					}
					// Test if postcode
					if (
						$this->addressHasTypes($addressComponent['types'], [
							'postal_code',
						])
					) {
						$customFields[] = [
							'Key' => 'Postcode',
							'Value' => $addressComponent['short_name'],
						];
					}

					// Test if state
					if (
						$this->addressHasTypes($addressComponent['types'], [
							'administrative_area_level_1',
							'political',
						]) &&
						array_search(
							$addressComponent['short_name'],
							$this->australianStates
						)
					) {
						$customFields[] = [
							'Key' => 'State',
							'Value' => $addressComponent['short_name'],
						];
					}

					// Test if country
					if (
						$this->addressHasTypes($addressComponent['types'], [
							'country',
							'political',
						])
					) {
						$customFields[] = [
							'Key' => 'CanteenConnectCountry',
							'Value' => $addressComponent['short_name'],
						];
					}
				}
			}
		}

		if ($dob) {
			$customFields[] = [
				'Key' => 'Dateofbirth',
				'Value' => $dob->format('Y-m-d'),
			];
		}

		if ($registered) {
			$customFields[] = [
				'Key' => 'CanteenConnectSignup',
				'Value' => 'YES',
			];

			$customFields[] = [
				'Key' => 'CanteenConnectVerified',
				'Value' => $verified ? 'YES': 'NO',
			];
		} else {
			$customFields[] = [
				'Key' => 'Unregistered',
				'Value' => 'YES',
			];
		}

		// Add the last engagement time
		$customFields[] = [
			'Key' => 'CanteenConnectLastEngagement',
			'Value' => Carbon::now()->format('Y-m-d'),
		];


		// if email change
		if(!empty($fieldsArr))
		{
			$customFields[] = $fieldsArr;
		}

		$params = [
			'EmailAddress' => $email,
			'Name' => "$firstName $lastName",
			'CustomFields' => $customFields,
			'Resubscribe' => true,
			'ConsentToTrack' => 'Yes',
		];
		$listId = config('canteen.campaign_monitor_list_id');
		$path = "/subscribers/{$listId}.json";
		$this->campaignMonitorQuery($path, $params, 'POST');
	}

	public function unsubscribeUserFromCampaignMonitor($email) {
		$params = [
			'EmailAddress' => $email,
		];
		$listId = config('canteen.campaign_monitor_list_id');
		$path = "/subscribers/{$listId}/unsubscribe.json";
		$this->campaignMonitorQuery($path, $params, 'POST');
	}

	protected function campaignMonitorQuery($path, $params, $verb) {
		$apiKey = config('canteen.campaign_monitor_key');
		$credentials = base64_encode($apiKey);
		$client = new Client([
			'base_uri' => 'https://api.createsend.com',
		]);

		try {
			  $response = $client->request($verb, "/api/v3.2{$path}", [
				'headers' => [
					'Authorization' => 'Basic ' . $credentials,
				],
				'json' => $params
			]);
			//echo($response->getBody()->getContents());
		} catch (\Exception $e) {
			//dd(Log::error($e));
			Log::error($e);
		}
	}

	protected function addressHasTypes($addressComponentTypes, $types) {
		foreach ($types as $type) {
			if (array_search($type, $addressComponentTypes) === false) {
				return false;
			}
		}
		return true;
	}
}

?>
