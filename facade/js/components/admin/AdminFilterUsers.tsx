import React, { useState } from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import { Fab } from '@material-ui/core';
import { ISelectOption, IApiSituation } from '../../interfaces';
import { IStoreState } from '../../redux/store';

import { SelectFieldFormik, InlineDatePickerFormik } from '../inputs';
import AdminFilterUsersPartial from './AdminFilterUsersPartial';
import { SwitchFieldFormik } from '../inputs/SwitchFieldFormik';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
interface IProps {
	initialValues: any;
	roleOptions?: ISelectOption[];
	situations: IApiSituation[] | null;
	handleFilter(filterValues: any): void;
	handleClear(): void;
}

const AdminFilterUsers: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		lastLoginFrom: Yup.string()
			.nullable()
			.test('test', 'Last login from is required.', function(from) {
				const to = this.parent.lastLoginTo;
				if (!from && to) {
					return false;
				} else {
					return true;
				}
			}),
		lastLoginTo: Yup.string()
			.nullable()
			.test('test', 'Last login to is required.', function(to) {
				const from = this.parent.lastLoginFrom;
				if (from && !to) {
					return false;
				} else {
					return true;
				}
			}),
		userCreatedFrom: Yup.string()
			.nullable()
			.test('test', 'User created from is required.', function(from) {
				const to = this.parent.userCreatedTo;
				if (!from && to) {
					return false;
				} else {
					return true;
				}
			}),
		userCreatedTo: Yup.string()
			.nullable()
			.test('test', 'User created to is required.', function(to) {
				const from = this.parent.userCreatedFrom;
				if (from && !to) {
					return false;
				} else {
					return true;
				}
			}),
		userVerifiedFrom: Yup.string()
			.nullable()
			.test('test', 'User verified from is required.', function(from) {
				const to = this.parent.userVerifiedTo;
				if (!from && to) {
					return false;
				} else {
					return true;
				}
			}),
		userVerifiedTo: Yup.string()
			.nullable()
			.test('test', 'User verified to is required.', function(to) {
				const from = this.parent.userVerifiedFrom;
				if (from && !to) {
					return false;
				} else {
					return true;
				}
			}),
	});
	const [showDate, setShowDate] = useState(false);
	const toggleDate = () => {
		setShowDate(!showDate);
	};
	const sortOptions: ISelectOption[] = [
		{
			label: 'Username Asc.',
			value: 'username-asc',
		},
		{
			label: 'Username Desc.',
			value: 'username-desc',
		},
		{
			label: 'Email Asc.',
			value: 'email-asc',
		},
		{
			label: 'Email Desc.',
			value: 'email-desc',
		},
		{
			label: 'Age Asc.',
			value: 'dob-desc',
		},
		{
			label: 'Age Desc.',
			value: 'dob-asc',
		},
		{
			label: 'Last Login Asc.',
			value: 'last_login-desc',
		},
		{
			label: 'Last Login Desc.',
			value: 'last_login-asc',
		},
	];

	const situationOptions: ISelectOption[] = [];
	const cancerOptions: ISelectOption[] = [];

	if (props.situations) {
		let cancers: string[] = [];
		props.situations.forEach(situation => {
			if (situation.parent_situation === null) {
				situationOptions.push({
					label: situation.name,
					value: situation.name,
				});
			}
			if (situation.cancer_type) {
				if (cancers.indexOf(situation.cancer_type) < 0) {
					cancers.push(situation.cancer_type);
				}
			}
		});
		cancers = cancers.sort();

		cancers.forEach(cancer => {
			cancerOptions.push({
				label: cancer,
				value: cancer,
			});
		});
	}

	const initialValues = JSON.parse(JSON.stringify(props.initialValues));
	return (
		<Formik
			initialValues={initialValues}
			validationSchema={validationFormat}
			onSubmit={values => {
				const valuesModified = { ...values };
				console.log(valuesModified);
				if (
					'lastLoginFrom' in valuesModified &&
					valuesModified.lastLoginFrom
				) {
					valuesModified.lastLoginFrom = moment(
						valuesModified.lastLoginFrom,
					).format('YYYY-MM-DD');
				}

				if (
					'lastLoginTo' in valuesModified &&
					valuesModified.lastLoginTo
				) {
					valuesModified.lastLoginTo = moment(
						valuesModified.lastLoginTo,
					).format('YYYY-MM-DD');
				}

				if (
					'userVerifiedFrom' in valuesModified &&
					valuesModified.userVerifiedFrom
				) {
					valuesModified.userVerifiedFrom = moment(
						valuesModified.userVerifiedFrom,
					).format('YYYY-MM-DD');
				}

				if (
					'userVerifiedTo' in valuesModified &&
					valuesModified.userVerifiedTo
				) {
					valuesModified.userVerifiedTo = moment(
						valuesModified.userVerifiedTo,
					).format('YYYY-MM-DD');
				}

				if (
					'userCreatedFrom' in valuesModified &&
					valuesModified.userCreatedFrom
				) {
					valuesModified.userCreatedFrom = moment(
						valuesModified.userCreatedFrom,
					).format('YYYY-MM-DD');
				}

				if (
					'userCreatedTo' in valuesModified &&
					valuesModified.userCreatedTo
				) {
					valuesModified.userCreatedTo = moment(
						valuesModified.userCreatedTo,
					).format('YYYY-MM-DD');
				}

				console.log(valuesModified);
				props.handleFilter(valuesModified);
			}}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				handleSubmit,
				setFieldValue,
				setFieldTouched,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
				};

				return (
					<form className="form" onSubmit={handleSubmit}>
						<div className="form-row">
							<div className="form-column">
								<SelectFieldFormik
									label="Sort users by"
									name="sort"
									options={sortOptions}
									values={props.initialValues.sort}
									noEmpty
									{...inputProps}
								/>
							</div>
						</div>
						<div className="form-row form-row--space_above">
							<div className="form-column">
								<h5 className="font--h6 theme-title">
									Filter Users By
								</h5>
							</div>
						</div>
						<div className="form-row-filter" onClick={toggleDate}>
							<div className="form-column">
								<h5 className="font--h6 theme-title">Dates</h5>
							</div>
							<div className="form-column">
								<Fab size="small">
									{showDate ? (
										<ArrowDropUpIcon />
									) : (
										<ArrowDropDownIcon />
									)}
								</Fab>
							</div>
						</div>
						{showDate && (
							<div>
								<div className="form-row hm-t16">
									<h5 className="font--h6 theme-title">
										<div className="form-column">
											Last login
										</div>
									</h5>
								</div>

								<div className="form-row">
									<div className="form-column">
										<InlineDatePickerFormik
											label="from"
											name="lastLoginFrom"
											format="DD/MM/YYYY"
											customError
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<InlineDatePickerFormik
											label="to"
											name="lastLoginTo"
											format="DD/MM/YYYY"
											customError
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row hm-t24">
									<h5 className="font--h6 theme-title">
										<div className="form-column">
											User created
										</div>
									</h5>
								</div>
								<div className="form-row">
									<div className="form-column">
										<InlineDatePickerFormik
											label="from"
											name="userCreatedFrom"
											format="DD/MM/YYYY"
											customError
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<InlineDatePickerFormik
											label="to"
											name="userCreatedTo"
											format="DD/MM/YYYY"
											customError
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row hm-t24">
									<h5 className="font--h6 theme-title">
										<div className="form-column">
											User verified
										</div>
									</h5>
								</div>
								<div className="form-row">
									<div className="form-column">
										<InlineDatePickerFormik
											label="from"
											name="userVerifiedFrom"
											format="DD/MM/YYYY"
											customError
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<InlineDatePickerFormik
											label="to"
											name="userVerifiedTo"
											format="DD/MM/YYYY"
											customError
											{...inputProps}
										/>
									</div>
								</div>
							</div>
						)}

						<AdminFilterUsersPartial
							initialValues={props.initialValues}
							inputProps={inputProps}
						/>
						{props.roleOptions && (
							<div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="Role"
										name="role"
										values={props.initialValues.role}
										options={props.roleOptions}
										emptyLabel="Select"
										{...inputProps}
									/>
								</div>
							</div>
						)}
						<div className="form-row">
							<div className="form-column">
								<SelectFieldFormik
									label="Verified"
									name="verified"
									noEmpty
									options={[
										{
											value: 'any',
											label: 'Any',
										},
										{
											value: '1',
											label: 'Verified',
										},
										{
											value: '3',
											label: 'Email verified',
										},
										{
											value: '2',
											label: 'Mobile verified',
										},
										{
											value: '0',
											label: 'Not Verified',
										},
									]}
									values={props.initialValues.verified}
									{...inputProps}
								/>
							</div>
						</div>
						<div className="form-row">
							<div className="form-column">
								<SwitchFieldFormik
									label="Exclude not verified users"
									name="excludeNotVerified"
									values={
										props.initialValues.excludeNotVerified
									}
									{...inputProps}
								/>
							</div>
								</div> 
						<div className="form-row">
							<div className="form-column">
								<SwitchFieldFormik
									label="Include inactive"
									name="includeInactive"
									values={props.initialValues.includeInactive}
									{...inputProps}
								/>
							</div>
						</div>
						<div className="form-row form-row--submit_left">
							<div className="form-column">
								<Button
									variant="contained"
									color="primary"
									className="form-submit"
									type="submit"
									disabled={!isValid || !touched}
								>
									Filter
								</Button>
							</div>
						</div>
						<div className="form-row form-row--submit_left">
							<div className="form-column">
								<Button
									variant="contained"
									color="secondary"
									className="form-clear"
									type="button"
									onClick={props.handleClear}
								>
									Clear
								</Button>
							</div>
						</div>
					</form>
				);
			}}
		/>
	);
};

function mapStateToProps(state: IStoreState) {
	const { situations } = state.situations;
	return {
		situations,
	};
}

export default connect(mapStateToProps)(AdminFilterUsers);
