import { actionTypes } from '../actions';

export interface IStoreState {
	quickLinksModalDisplay: boolean;
}

export const quickLinksModal = (state: IStoreState = {
	quickLinksModalDisplay: false,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.QUICK_LINKS_MODAL_DISPLAY:
			return Object.assign({}, state, {
				quickLinksModalDisplay: true
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.QUICK_LINKS_MODAL_HIDE:
			return Object.assign({}, state, {
				quickLinksModalDisplay: false
			});
		default:
			return state;
	}
}
