import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import { IStoreState } from '../js/redux/store';
import Link from 'next/link';


import { dissectUrl } from '../js/helpers/dissectUrl';
import {
	IconFacebook,
	IconTwitter,
	IconYouTube,
	IconInstagram,
} from '../js/components/general/Icons';

const list = {
	links: [
		{
			url: dissectUrl('https://www.facebook.com/CanteenAus/'),
			icon: IconFacebook,
		},
		{
			url: dissectUrl('https://www.youtube.com/user/CanteenAustralia'),
			icon: IconYouTube,
		},
		{
			url: dissectUrl('https://twitter.com/canteenaus'),
			icon: IconTwitter,
		},
		{
			url: dissectUrl('https://www.instagram.com/canteen_aus/'),
			icon: IconInstagram,
		},
	],
};
const imageArr = [
	'ankit-sinha-qKgRJ9_vajY-unsplash.jpg',
	'artem-beliaikin-WGCY-Wuh_Ho-unsplash.jpg',
	'clay-banks-POzx_amnWJw-unsplash.jpg',
	'tyler-nix-sh3LSNbyj7k-unsplash.jpg',
];
const mobileImages = [
	'Bg-Img-1.jpg',
	'Bg-Img-3.jpg',
	'Bg-Img-4.jpg',
	'Bg-Img-6.jpg',
	'Bg-Img-7.jpg',

];
interface IProps {
	userAuthenticated: boolean | null;
}
interface IState {
	picIndex: number;
}
class GraduatedUser extends React.Component<IProps, IState> {
	constructor(props: any) {
		super(props);

		let picIndex = Math.random() * 100;
		picIndex = Math.floor(picIndex) % 4;

		this.state = {
			picIndex,
		};
		this.renderInformation = this.renderInformation.bind(this);
	}
	public componentDidMount() {
		//this.counter();
	}
	public componentWillUnmount() {
		clearInterval(this.counter());
	}
	public render() {
		const { picIndex } = this.state;
		return (
			<div className="graduate_user-container">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="viewport"
						content="width=device-width,initial-scale=1.0"
					></meta>
				</Head>
				<div className="graduate_user-content">
					<div
						style={{
							backgroundImage: `url("${process.env.STATIC_PATH}/images/${imageArr[picIndex]}")`,
						}}
						className="graduate_user-image-container"
					>
						<div className="graduate_user-image-thanks-container">
							<img
								src={`${process.env.STATIC_PATH}/images/Thank-You.png`}
								className="graduate_user-image-thanks"
							></img>
						</div>
						<div className="graduate_user-Clogo" >
							<div> <img
								src={`${process.env.STATIC_PATH}/images/logo-Mark-Blue.png`}
								className="graduate_user-Clogo-image"
							></img> </div>
						</div>
					</div>
					<div
						style={{
							backgroundImage: `url("${process.env.STATIC_PATH}/images/graduated-user-mobile/${mobileImages[picIndex]}")`,
						}}
						className="graduate_user-image-container-mobile"
					>
						<div className="graduate_user-image-thanks-container">
							<img
								src={`${process.env.STATIC_PATH}/images/Thank-You.png`}
								className="graduate_user-image-thanks"
							></img>
						</div>
						{this.renderInformation(true)}
					</div>
					{this.renderInformation()}
				</div>
			</div>
		);
	}
	protected renderInformation(isMobile?: boolean) {
		return (
			<div className={`graduate_user-information${isMobile ? "-mobile" : ""}`}>
				<div className="graduate_user-information-heading">
					<h2>
						Now that you're 26, you've graduated from <span style={{ display: "inline-block" }}> Canteen
						Connect</span>. We want to thank you for being a part of our
						community and helping us create a vibrant online space.
					</h2>
				</div>
				<div className="graduate_user-information-text">
					If you need extra support, please email us on
					<span className="graduate_user-information-email">
						{' '}
						<a
							href="mailto:online@canteen.org.au "
							className="link"
						>
							{' '}
							(online@canteen.org.au){' '}
						</a>
					</span>{' '}
					or contact Cancer Council on{' '}
					<span className="graduate_user-information-phone">
						{' '}
						13 11 20{' '}
					</span>
				</div>
				<div className="graduate_user-information-line"></div>
				<div className="graduate_user-information-footer">
					<div>
						<p className="graduate_user-information-footer-heading">
							Stay up to date
						</p>
						<div>
							<ul className="graduate_user-inforamation-footer-links">
								{list.links.map((link, index) => {
									return (
										<li
											className="graduate_user-inforamation-footer-link"
											key={index}
										>
											<a
												className="footer-content-link font--body theme-text--accent_dark"
												href={link.url.hrefAs}
												target="_blank"
												rel="noopener"
											>
												{!!link.icon && (
													<span className="footer-content-link-icon_container">
														<link.icon className="footer-content-link-icon" />
													</span>
												)}
											</a>
										</li>
									);
								})}
							</ul>
						</div>
					</div>
					<div>
						<Link href="/dashboard">
							<img
								className="graduate_user-information-footer-logo"
								src={`${process.env.STATIC_PATH}/images/CC-Logo-White.png`}
							></img>
						</Link>
					</div>
				</div>
			</div>
		);
	}
	protected counter = () =>
		setInterval(() => {
			let { picIndex } = this.state;
			picIndex = picIndex + 1;
			if (picIndex >= 4) {
				picIndex = 0;
			}
			this.setState({ picIndex });
		}, 3000);
}
function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(GraduatedUser);
