<?php

namespace App;

use App\Staff;
use Illuminate\Database\Eloquent\Model;

class Team extends Model {
	protected $guarded = [
		'slug'
	];

	protected $hidden = [
		'created_at',
		'updated_at',
		'pivot'
	];

	public function getRouteKeyName() {
		return 'slug';
	}

	public function staffs() {
		return $this->hasMany(Staff::class);
	}
}
