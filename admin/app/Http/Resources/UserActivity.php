<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Blog as BlogResource;
use App\Http\Resources\Discussion as DiscussionResource;
use App\Http\Resources\Resources as ResourcesResource;
use App\Http\Resources\User as UserResource;

class UserActivity extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'user_activity',
				$this->id,
				$this->updated_at,
				true,
				true
			)
		) {
			return $apiStore->getApiResource(
				'user_activity',
				$this->id,
				$this->updated_at,
				true,
				true
			);
		}

		$params = [
			'id',
			'type',
			'content',
			'user_id',
			'platform',
			'activity_model_name',
			'created_at',
			'updated_at',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}


		// Load the related model
		$activity = null;
		if (
			$this->activity()
				->withTrashed()
				->exists()
		) {
			switch ($this->activity_model_name) {
				case 'blog':
					$activity = new BlogResource(
						$this->activity()
							->withTrashed()
							->first()
					);
					break;
				case 'discussion':
					$activity = new DiscussionResource(
						$this->activity()
							->withTrashed()
							->first()
					);
					break;
				case 'resource':
					$activity = new ResourcesResource(
						$this->activity()
							->withTrashed()
							->first()
					);
					break;
				case 'user':
					$activity = new UserResource(
						$this->activity()
							->withTrashed()
							->first()
					);
					break;
			}
		}
		$result['activity'] = $activity;

		$result['user'] = $this->user()
			->withTrashed()
			->exists()
			? new UserResource(
				$this->user()
					->withTrashed()
					->first()
			)
			: null;

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);

		$apiStore->setApiResource(
			$result,
			'user_activity',
			$this->id,
			$this->updated_at,
			true,
			true
		);

		return $result;
	}
}
