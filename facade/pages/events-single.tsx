import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import Router from 'next/router';
import moment from 'moment';
import 'moment-timezone';

import { IStoreState, apiError, apiSuccess } from '../js/redux/store';
import {
	IApiUser,
	IApiEvent,
	IApiStaff,
	IApiDiscussion,
} from '../js/interfaces';
import { checkPage404 } from '../js/helpers/checkPage404';
import StaffTilesListItem from '../js/components/meet-the-team/StaffTilesListItem';
import { ApiService, AuthService } from '../js/services';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import WYSIWYG from '../js/components/general/WYSIWYG';
import SubTitleDetail from '../js/components/general/SubTitleDetail';
import FixedBackBar from '../js/components/general/FixedBackBar';
import Footer from '../js/components/general/Footer';
import EventTile from '../js/components/events/EventTile';
import EventSidebar from '../js/components/events/EventSidebar';
import EventAddress from '../js/components/events/EventAddress';
import EventTimeDate from '../js/components/events/EventTimeDate';
import EventSidebarActions from '../js/components/events/EventSidebarActions';
import UserTile from '../js/components/general/UserTile';
import { GTMDataLayer } from '../js/helpers/dataLayer';
import MediaGallery from '../js/components/events/EventMediaGallery';
import DiscussionTile from '../js/components/discussions/DiscussionTile';
import EventPromoModal from '../js/components/events/EventPromoModal';
import EventPostMedia from '../js/components/events/EventPostMedia';

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	event: IApiEvent;
	related: IApiEvent[];
	apiService: ApiService;
	authService: AuthService;
	updateDataCount: number;
	userData: IApiUser;
	eventType: string;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	event: IApiEvent;
	related: IApiEvent[];
	resourceSaved: boolean;
	peopleLikeMeData: IApiUser[];
	currentPage: number;
	maxPages: number;
	total: number;
	isLoadingUsers: boolean;
	staffData: IApiStaff;
	showPromo: boolean;
	discussion: IApiDiscussion | null;
	showPostModal: boolean;
	register_error_message: string;
	isButtonDisable: boolean;
	currentUserCount: number;
}
const ausZoneArr = moment.tz.zonesForCountry('AU');
const nzZoneArr = moment.tz.zonesForCountry('NZ');
class EventSingle extends React.Component<IProps, IState> {
	static async getInitialProps({ req, query, apiService }) {
		const result = await checkPage404(
			req,
			query,
			'id',
			async (slug: string) => {
				const data = await (apiService as ApiService).queryGET(
					`/api/events/${slug}`,
				);
				const event = 'event' in data ? data.event : null;
				const related = 'related' in data ? data.related : [];

				return {
					event,
					related,
					eventType: query.eventType,
				};
			},
		);

		if (!result) {
			return {
				event: null,
				related: null,
			};
		}

		return result;
	}

	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isUpdating: false,
			resourceSaved: false,
			event: this.props.event,
			related: this.props.related,
			peopleLikeMeData: [],
			currentPage: 0,
			maxPages: 0,
			isLoadingUsers: false,
			total: 0,
			staffData: null,
			showPromo: false,
			discussion: null,
			showPostModal: false,
			register_error_message: '',
			isButtonDisable: false,
			currentUserCount: 9,
		};

		this.trackRelatedContent = this.trackRelatedContent.bind(this);
		this.loadUsers = this.loadUsers.bind(this);
		this.loadStaffData = this.loadStaffData.bind(this);
		this.togglePromoModal = this.togglePromoModal.bind(this);
		this.togglePostModal = this.togglePostModal.bind(this);
		this.loadDiscussion = this.loadDiscussion.bind(this);
		this.saveMedia = this.saveMedia.bind(this);
		this.updateData = this.updateData.bind(this);
		this.setLocalTime = this.setLocalTime.bind(this);
		this.isRegisterDisabled = this.isRegisterDisabled.bind(this);
		this.handleUserCountIncrement = this.handleUserCountIncrement.bind(
			this,
		);
	}

	public componentDidMount() {
		if (typeof window !== 'undefined') {
			Router.events.on('routeChangeComplete', url => {
				if (url) {
					const urlSplit = url.split('/');
					if (urlSplit.length >= 3) {
						this.updateData(urlSplit[2]);
					}
				}
			});

			this.isRegisterDisabled();
			this.loadStaffData();
			this.loadDiscussion();
			this.setLocalTime();
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.updateDataCount !== prevProps.updateDataCount) {
			this.updateData();
		}
		if (
			JSON.stringify(this.props.event) !== JSON.stringify(prevProps.event)
		) {
			this.setState(
				{
					event: this.props.event,
				},
				() => this.isRegisterDisabled(),
			);
		}
	}

	public render() {
		let title = `Event`;
		let real_event_type = 'workshop';
		const {
			event,
			showPostModal,
			register_error_message,
			isButtonDisable,
			currentUserCount,
		} = this.state;

		let {
			event_type = 'workshop',
			promo_video_url,
			registered_users,
			user_media = [],
		} = event;
		if (event_type === '' || event_type === null) {
			event_type = 'workshop';
		} else {
			real_event_type = event_type;
		}
		event_type = this.props.eventType ? this.props.eventType : 'workshop';
		const { users = [] } = registered_users;
		if (!promo_video_url) {
			promo_video_url = `${process.env.STATIC_PATH}/videos/landing_hero_loop.mp4`;
		}

		if (this.state.event) {
			title = this.state.event.title;
		}
		const url = `${process.env.APP_URL}/events/${this.props.event.slug}`;

		// const removeHTMLandTruncate = (
		// 	content: string,
		// 	stringLength: number,
		// ) => {
		// 	const replaceTags = content.replace(/<(?:.|\n)*?>/gm, '');
		// 	const limitContent = replaceTags.substring(0, stringLength);

		// 	return limitContent;
		// };

		const featuredImageUrl = this.props.event.feature_image.url
			? this.props.event.feature_image.url
			: `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;
		// const seoDescription = removeHTMLandTruncate(
		// 	this.props.event.description,
		// 	300,
		// );

		const sharingUrl = url;
		const eventDateStartDay = moment(
			this.state.event.start_date.date,
		).format('D');
		const eventDateStartMonth = moment(
			this.state.event.start_date.date,
		).format('MMM');
		const role_names =
			this.props.userData && this.props.userData.role_names;
		const role = role_names && role_names.length > 0 && role_names[0];
		const { min_age = 0, max_age = 0 } = this.props.event;
		return (
			<div className="events_single">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:url"
						property="og:url"
						content={sharingUrl}
					/>
					<meta name="og:type" property="og:type" content="article" />

					<meta
						name="og:image"
						property="og:image"
						content={featuredImageUrl}
					/>
					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>

					<meta name="twitter:card" content="summary" />
					<meta name="twitter:site" content="Canteen Connect" />
					<meta name="twitter:title" content={title} />
					<meta name="twitter:description" content={title} />
					<meta name="twitter:image" content={featuredImageUrl} />
				</Head>
				<LayoutDashboard>
					<FixedBackBar href="/events" label="All Events" />

					<div
						className="event_single-hero"
						style={{ backgroundImage: `url(${featuredImageUrl})` }}
					/>

					<div className="event_single-content">
						<div className="event_single-content_container">
							<div className="event-single-title-and-information">
								<div
									className={`event_tile-thumbnail-date type-${event_type}`}
								>
									<div>
										<span className="event_tile-thumbnail-date-month">
											{' '}
											{eventDateStartMonth}
										</span>
									</div>
									<div>
										<span className="event_tile-thumbnail-date-day">
											{eventDateStartDay}
										</span>
									</div>
								</div>
								<div className="event_single-header">
									<h1 className="event_tile-content-title theme-title font--h5">
										{this.props.event.title}
									</h1>
									<div className="event_tile-going-container">
										<div>
											<span
												className={`span-${event_type}`}
											>
												{real_event_type}
											</span>
										</div>
										<div className="verticle_line"></div>
										<div className="dot-conatiner">
											<div className="dark-dot" />
											<div className="dark-dot-medium" />
											<div className="dark-dot-light" />
										</div>
										<div>
											<span className="going-span">
												+{' '}
												{
													this.state.event
														.registered_users_count
												}{' '}
												going
											</span>
										</div>
									</div>
								</div>
							</div>

							<div className="event_single-wysiwyg">
								<WYSIWYG>
									{this.state.event.feature_image && (
										<img
											className="event_single-wysiwyg_image"
											src={`${featuredImageUrl}`}
											alt={` `}
										/>
									)}

									<div
										className={`event_single-event_details type-${event_type}`}
									>
										<EventAddress
											address={this.state.event.location}
											event={this.state.event}
										/>

										<EventTimeDate
											dateStart={
												this.state.event.start_date
											}
											dateEnd={this.state.event.end_date}
										/>
									</div>

									<div
										className="event_single-wysiwyg-content"
										dangerouslySetInnerHTML={{
											__html: this.state.event
												.description,
										}}
									/>
								</WYSIWYG>
								{max_age !== 0 && min_age !== 0 ? (
									<div>
										<h3 className="event_single-user-heading-container">
											Eligibility
										</h3>
										<p>
											This event is suitable for Canteen
											young people between {min_age} and{' '}
											{max_age} years.
										</p>
									</div>
								) : (
									''
								)}
								{max_age !== 0 && min_age === 0 ? (
									<div>
										<h3 className="event_single-user-heading-container">
											Eligibility
										</h3>
										<p>
											This event is suitable for Canteen
											young people below the age of{' '}
											{max_age} years.
										</p>
									</div>
								) : (
									''
								)}
								{max_age === 0 && min_age !== 0 ? (
									<div>
										<h3 className="event_single-user-heading-container">
											Eligibility
										</h3>
										<p>
											TThis event is suitable for Canteen
											young people over the age {min_age}{' '}
											years.
										</p>
									</div>
								) : (
									''
								)}
								{this.state.event.is_travel_cover ? (
									<div>
										<h3 className="event_single-user-heading-container">
											Travel cost
										</h3>
										<p>The event covers travel cost</p>
									</div>
								) : (
									''
								)}
								{this.props.userAuthenticated &&
									(role === 'admin' ||
										this.state.event.show_rsvp === 1) &&
									users &&
									users.length > 0 && (
										<div>
											<div className="event_single-user-heading-container">
												<h3>Attendes Going</h3>
											</div>
											<div className="event_single-going-user-container">
												{users
													.slice(0, currentUserCount)
													.map((data, index) => {
														return (
															<UserTile
																key={index}
																userData={data}
															/>
														);
													})}
												{users.length >
													currentUserCount && (
													<div
														onClick={
															this
																.handleUserCountIncrement
														}
														className="event_single-show-more-users"
													>
														show more
													</div>
												)}
											</div>
										</div>
									)}
								{this.props.userAuthenticated && (
									<div>
										<div className="event_single-user-heading-container">
											<h3>Event Media</h3>
											{event.registered && (
												<h3
													className="event_single-post-media-button"
													onClick={
														this.togglePostModal
													}
												>
													Post Event Media
												</h3>
											)}
										</div>
										<div>
											{user_media &&
											user_media.length > 0 ? (
												<MediaGallery
													images={user_media}
												/>
											) : (
												<h5 className="hm-0">
													No Event Media.{' '}
													{!event.registered
														? 'You can post media after registering for this event'
														: 'Post event media'}
												</h5>
											)}
										</div>
									</div>
								)}
								{this.props.userAuthenticated &&
									this.state.staffData && (
										<div>
											<div className="event_single-user-heading-container">
												<h3>Event Staff</h3>
											</div>
											<div className="event_single-going-user-container">
												{this.state.staffData && (
													<StaffTilesListItem
														data={
															this.state.staffData
														}
														onClick={() => {}}
													/>
												)}
											</div>
										</div>
									)}
								{this.state.discussion && (
									<div className="event_single-discussion">
										<div className="discussions-tiles-item">
											<h3>Event Discussion</h3>

											<DiscussionTile
												discussion={
													this.state.discussion
												}
												disabled={false}
												useLastActivity={true}
												withRepliedTo={null}
												noActions={
													!this.props
														.userAuthenticated
												}
											/>
										</div>
									</div>
								)}
								<div
									className={`event_single-sidebar_action type-${event_type}`}
								>
									<EventSidebarActions
										event={this.state.event}
										eventId={this.state.event.id}
										isRegistered={
											this.state.event.registered
										}
										eventSlug={this.state.event.slug}
										classes={`type-${
											event_type
												? event_type
												: 'group-activity'
										}`}
										disableRegister={isButtonDisable}
										resgisterMessage={
											register_error_message
										}
									/>
								</div>
							</div>

							<EventSidebar
								event={this.state.event}
								address={this.state.event.location}
								dateStart={this.state.event.start_date}
								dateEnd={this.state.event.end_date}
								latitude={this.state.event.latitude}
								longitude={this.state.event.longitude}
								eventId={this.state.event.id}
								eventSlug={this.state.event.slug}
								isRegistered={this.state.event.registered}
								classes={`type-${
									event_type ? event_type : 'group-activity'
								}`}
								togglePromoModal={this.togglePromoModal}
								disableRegister={isButtonDisable}
								resgisterMessage={register_error_message}
							/>
						</div>
					</div>

					{this.props.related &&
						this.props.related.length > 0 &&
						this.renderRelatedTiles()}

					<Footer />
				</LayoutDashboard>
				{this.state.showPromo && (
					<EventPromoModal
						toggleModel={this.togglePromoModal}
						event={event}
					/>
				)}

				{showPostModal && (
					<EventPostMedia
						togglePostModal={this.togglePostModal}
						event={event}
						saveMedia={this.saveMedia}
					/>
				)}
			</div>
		);
	}

	protected trackRelatedContent(title: string) {
		this.GTM.pushEventToDataLayer({
			event: 'nextContent',
			title,
		});
	}

	protected renderRelatedTiles() {
		const subTitleConfig = {
			icon: 'events',
			title: 'Other events you may like',
			linkLabel: 'All events',
			linkData: {
				prefix: '/events',
				href: '/events',
				as: '/events',
			},
		};

		return (
			<div className="events_single-related">
				<div className="events_single-related_container">
					<SubTitleDetail {...subTitleConfig} />

					<div className="events-tiles">
						{this.state.related.map((relatedTile, index) => {
							return (
								<div className="events-tiles-item" key={index}>
									<EventTile
										eventLinkPrefix={
											subTitleConfig.linkData.prefix
										}
										event={relatedTile}
										gtm={this.trackRelatedContent}
										index={index}
										ausZoneArr={ausZoneArr}
										nzZoneArr={nzZoneArr}
									/>
								</div>
							);
						})}
					</div>
				</div>
			</div>
		);
	}
	protected saveMedia = values => {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				const slugValue = this.props.router.query.id;

				this.props.apiService
					.queryPUT(`/api/events/${slugValue}/uploadmedia`, values)
					.then(_res => {
						console.log('res -===?> ', _res);
						this.setState({
							isUpdating: false,
							showPostModal: false,
						});

						this.props.dispatch(
							apiSuccess([`Edited successfully`]),
						);

						this.updateData();
					})
					.catch(error => {
						this.setState({
							isUpdating: false,
						});

						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	};
	protected updateData(slug?) {
		this.setState(
			{
				isUpdating: true,
			},
			async () => {
				const slugValue = slug ? slug : this.props.router.query.id;

				const data = await this.props.apiService.queryGET(
					`/api/events/${slugValue}`,
				);
				const event = 'event' in data ? data.event : null;
				const related = 'related' in data ? data.related : [];
				const timeZone =
					event && event.event_time_zone
						? event.event_time_zone
						: 'Australia/Sydney';
				const offSet = moment.tz(timeZone).utcOffset();
				const currentTimeZone = moment.tz.guess(true);
				const myOffset = moment.tz(currentTimeZone).utcOffset();
				const diff = myOffset - offSet;

				if (event && event.start_date) {
					let newDate = moment(event.start_date.date).add(
						diff,
						'minute',
					);
					event.start_date['date'] = newDate;
				}
				if (event && event.end_date) {
					let newDate = moment(event.end_date.date).add(
						diff,
						'minute',
					);
					event.end_date['date'] = newDate;
				}
				if (event && event.last_joining_date) {
					let newDate = moment(event.last_joining_date.date).add(
						diff,
						'minute',
					);
					event.last_joining_date['date'] = newDate;
				}
				this.setState(
					{
						event,
						related,
					},
					() => {
						this.loadStaffData();
						this.loadDiscussion();
					},
				);
			},
		);
	}

	protected loadUsers() {
		const url = buildUrl(null, {
			path: `/api/users/me/similar-users`,
		});

		this.setState(
			{
				isLoadingUsers: true,
			},
			() => {
				this.props.apiService
					.queryGET(url)
					.then((response: any) => {
						let users: IApiUser[] = response.data.users;
						this.setState({
							isUpdating: false,
							isLoadingUsers: false,
							peopleLikeMeData: users,
							currentPage: response.meta.current_page,
							maxPages: response.meta.last_page,
							total: response.meta.total,
						});
					})
					.catch(error => {
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isLoadingUsers: false,
						});
					});
			},
		);
	}
	protected loadStaffData() {
		const { event } = this.state;
		if (!event.event_admin) {
			return;
		}
		this.setState(
			{
				isLoadingUsers: true,
			},
			async () => {
				const url = buildUrl(null, {
					path: `/api/staff/${event.event_admin}`,
				});

				const { data, error } = await this.props.apiService.queryGET(
					url,
				);
				if (error) {
					this.props.dispatch(apiError([error]));

					this.setState({
						isLoadingUsers: false,
					});
				} else {
					this.setState({
						isLoadingUsers: false,
						staffData: data,
					});
				}
			},
		);
	}

	protected loadDiscussion() {
		const { event } = this.state;
		if (!event.discussion_id) {
			return;
		}
		this.setState(
			{
				isLoadingUsers: true,
			},
			async () => {
				const url = buildUrl(null, {
					path: `/api/discussions/${event.discussion_id}`,
				});

				const {
					discussion,
					error,
				} = await this.props.apiService.queryGET(url);
				if (error) {
					this.props.dispatch(apiError([error]));

					this.setState({
						isLoadingUsers: false,
					});
				} else {
					this.setState({
						isLoadingUsers: false,
						discussion: discussion,
					});
				}
			},
		);
	}

	protected togglePromoModal = () => {
		this.setState({ showPromo: !this.state.showPromo });
	};
	protected togglePostModal = () => {
		this.setState({ showPostModal: !this.state.showPostModal });
	};
	protected isRegisterDisabled = () => {
		const { userData, userAuthenticated } = this.props;
		if (!userAuthenticated) {
			return;
		}
		let { age, dob } = userData;
		if (!age) {
			age = moment().diff(dob, 'years', false);
		}
		const { event } = this.state;
		let {
			max_age,
			min_age,
			max_participents,
			registered,
			registered_users_count,
			last_joining_date,
			start_date,
			event_time_zone = 'Australia/Sydney',
		} = event;
		if (registered) {
			this.setState({
				isButtonDisable: true,
				register_error_message:
					'You have already registered for this event',
			});
			return true;
		}
		if (!last_joining_date) {
			last_joining_date = start_date;
		}
		const now = moment();
		const offSet = moment.tz(event_time_zone).utcOffset();
		const currentTimeZone = moment.tz.guess(true);
		const myOffset = moment.tz(currentTimeZone).utcOffset();
		const diff = myOffset - offSet;

		let lastDate = moment(last_joining_date.date).tz(event.event_time_zone);
		lastDate = moment(lastDate).add(diff, 'minute');

		if (now > lastDate) {
			this.setState({
				isButtonDisable: true,
				register_error_message: 'Registration date over',
			});
			return true;
		}
		if (!max_participents) {
			max_participents = 9999999999;
		}
		if (!max_age) {
			max_age = 999;
		}
		if (!min_age) {
			min_age = -1;
		}
		if (age > max_age) {
			this.setState({
				isButtonDisable: true,
				register_error_message: `maximum age limit for this event is ${max_age}`,
			});
			return true;
		}
		if (age < min_age) {
			this.setState({
				isButtonDisable: true,
				register_error_message: `minimum age limit for this event is ${min_age}`,
			});
			return true;
		}

		if (max_participents === registered_users_count) {
			this.setState({
				isButtonDisable: true,
				register_error_message: 'Maximum participant limit reached',
			});
			return true;
		}

		return false;
	};
	protected setLocalTime = () => {
		const { event } = this.state;
		const newEvent = { ...event };
		const timeZone =
			event && event.event_time_zone
				? event.event_time_zone
				: 'Australia/Sydney';
		const offSet = moment.tz(timeZone).utcOffset();
		const currentTimeZone = moment.tz.guess(true);
		const myOffset = moment.tz(currentTimeZone).utcOffset();
		const diff = myOffset - offSet;

		if (event && event.start_date) {
			let newDate = moment(event.start_date.date)
				.add(diff, 'minute')
				.format('YYYY-MM-DD HH:mm');
			newEvent.start_date['date'] = newDate;
		}
		if (event && event.end_date) {
			let newDate = moment(event.end_date.date)
				.add(diff, 'minute')
				.format('YYYY-MM-DD HH:mm');
			newEvent.end_date['date'] = newDate;
		}
		if (event && event.last_joining_date) {
			let newDate = moment(event.last_joining_date.date)
				.add(diff, 'minute')
				.format('YYYY-MM-DD HH:mm');
			newEvent.last_joining_date['date'] = newDate;
		}
		this.setState({ event: newEvent });
	};
	protected handleUserCountIncrement = () => {
		this.setState({ currentUserCount: this.state.currentUserCount + 9 });
	};
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { updateDataCount } = state.updateData;

	return {
		userAuthenticated,
		updateDataCount,
		userData,
	};
}

export default connect(mapStateToProps)(withRouter(EventSingle));
