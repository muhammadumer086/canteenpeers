<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

use UserVerification;
use App\Helpers\CampaignMonitorSubscription;

class UpdateUsersBans extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:updateUsersBans';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Remove one day for each of the banned users';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		User::whereNotNull('ban')
			->where('ban', '<', 999)
			->chunk(100, function ($users) {
				foreach ($users as $user) {
					$user->ban = $user->ban - 1;
					if ($user->ban <= 0) {
						$user->ban = null;
					}
					$user->save(['timestamps' => false]);
				}
			});
	}
}
