import React from 'react';
import { Modal } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

interface IProps {
	isActive: boolean;
	modalTitle?: string;
	ariaTag?: string;
	variant?: 'small';
	handleClose(event: React.ChangeEvent<any>): void;
}

const ModalDialog: React.StatelessComponent<IProps> = props => {
	const classes: string[] = ['modal', 'modal--default', 'theme--main'];
	if (props.variant) {
		classes.push(`modal--${props.variant}`);
	}
	return (
		<Modal
			aria-labelledby={!!props.ariaTag ? props.ariaTag : 'modal-dialog'}
			open={props.isActive}
			onClose={props.handleClose}
		>
			<Paper className={classes.join(' ')}>
				<button
					onClick={props.handleClose}
					className="close_button close_button--primary"
					title="Dismiss"
				>
					<ClearIcon className="close_button-icon" />
				</button>

				<div className="modal-content_container">
					<div className="modal-content modal-content--default">
						{!!props.modalTitle && (
							<h2 className="modal-content_title font--h4 theme-title hm-t16">
								{props.modalTitle}
							</h2>
						)}

						{props.children}
					</div>
				</div>
			</Paper>
		</Modal>
	);
};

export { ModalDialog };
