import React from 'react';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import { ISelectOption } from '../../interfaces';

interface IProps {
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	noEmpty?: boolean;
	multiple?: boolean;
	emptyLabel?: string;
	disabled?: boolean;
	options: ISelectOption[];
	helperText?: string;
	id?:string;
	handleChange?(e: React.ChangeEvent<any>): void;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
	handleBlur(e: any): void;
}

export const SelectFieldFormik: React.SFC<IProps> = (props: IProps) => {
	let selectValue;

	if (props.name in props.values && props.values[props.name] !== null) {
		selectValue = props.values[props.name];
	} else {
		selectValue = '';
	}

	return (
		<FormControl className="form-input" id={props.id} disabled={props.disabled}>
			<InputLabel htmlFor={props.name}>{props.label}</InputLabel>
			<Select
				value={selectValue}
				onChange={handleChange}
				name={props.name}
				multiple={props.multiple}
				error={props.touched[props.name] && !!props.errors[props.name]}
			
				// helperText={
				// 	props.touched[props.name] && props.errors[props.name]
				// 		? props.errors[props.name]
				// 		: props.helperText
				// }
			>
				{!props.noEmpty && (
					<MenuItem value="">
						{props.emptyLabel && <em>{props.emptyLabel}</em>}
						{!props.emptyLabel && <em>None</em>}
					</MenuItem>
				)}

				{props.options.map((option, index) => {
					return (
						<MenuItem key={index} value={option.value}>
							{option.label}
						</MenuItem>
					);
				})}
			</Select>
			<p className='event-form-helper-text'>
			{
					props.touched[props.name] && props.errors[props.name]
						? props.errors[props.name]
					 		: props.helperText
			}	
			</p>
		</FormControl>
	);

	function handleChange(event) {
		props.setFieldValue(props.name, event.target.value);
		props.setFieldTouched(props.name, true, true);
		if (props.handleChange) {
			props.handleChange(event);
		}
	}
};
