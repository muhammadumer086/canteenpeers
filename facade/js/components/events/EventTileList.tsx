import React from 'react';
import { IApiEvent } from '../../../js/interfaces';
import EventTile from './EventTile';
import moment from 'moment';
import 'moment-timezone';
const ausZoneArr = moment.tz.zonesForCountry('AU');
const nzZoneArr = moment.tz.zonesForCountry('NZ');
interface IProps {
	events: IApiEvent[];
}

const EventTileList: React.StatelessComponent<IProps> = props => {
	return (
		<div className="event_tile_list">
			{props.events.map((event, index) => {
				return <EventTile key={index} index={index} event={event} ausZoneArr={ausZoneArr}
				nzZoneArr={nzZoneArr} />;
			})}
		</div>
	);
};

export default EventTileList;
