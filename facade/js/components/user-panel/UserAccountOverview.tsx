import React from 'react';
import { connect } from 'react-redux';
import Dropzone from 'react-dropzone';

import ButtonBase from '@material-ui/core/ButtonBase';
import CircularProgress from '@material-ui/core/CircularProgress';

import CheckIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';

import { IApiUser } from '../../interfaces';
import { EAccountScreens, ETabs } from './UserPanelAccount';
import Pill from '../general/Pill';
import { UserProfileCompletion } from '../../helpers/UserProfileCompletion';
import UserCompletion from '../general/UserCompletion';
import UserAvatar from '../general/UserAvatar';
import { ApiService } from '../../services';

import { IStoreState, userAvatarUpload } from '../../redux/store';

interface IProps {
	userData: IApiUser | null;
	loggedInUser: IApiUser | null;	
	userAvatarUploading: boolean;
	submitting: boolean;
	apiService: ApiService;
	changeScreen(tab: ETabs, screen: EAccountScreens): void;
	onSubmit(values: any, resetForm?: () => void): void;
	dispatch(action: any): void;
}

interface ISection {
	label: string;
	complete: boolean;
	order: number;
	action(): void;
}

const UserAccountOverview: React.FC<IProps> = (props: IProps) => {
	const userProfileCompletion = new UserProfileCompletion(props.userData);

	const sections: ISection[] = [
		{
			label: 'Public Details',
			order: 0,
			complete:
				userProfileCompletion.getUserProfileCompletionPublic() === 1,
			action: () =>
				props.changeScreen(
					ETabs.MY_PROFILE,
					EAccountScreens.PUBLIC_DETAILS,
				),
		},
		{
			label: 'Private Details',
			order: 1,
			complete:
				userProfileCompletion.getUserProfileCompletionPrivate() === 1,
			action: () =>
				props.changeScreen(
					ETabs.MY_PROFILE,
					EAccountScreens.PRIVATE_DETAILS,
				),
		},
		{
			label: 'Password',
			order: 3,
			complete:
				userProfileCompletion.getUserProfileCompletionPassword() === 1,
			action: () =>
				props.changeScreen(ETabs.MY_PROFILE, EAccountScreens.PASSWORD),
		},
		{
			label: 'Optional Details',
			order: 4,
			complete:
				userProfileCompletion.getUserProfileCompletionOptional() === 1,
			action: () =>
				props.changeScreen(ETabs.MY_PROFILE, EAccountScreens.OPTIONAL),
		},
		
	];
	if(props.loggedInUser.role_names[0]==="admin"){
		sections.push({
			label: 'Role Details',
			order: 5,
			complete:true,
			action: () =>
				props.changeScreen(ETabs.MY_PROFILE, EAccountScreens.ROLE),
		});
	}
	if (
		props.userData.guardian_details
	) {
		sections.push(
			{
				order: 2,
				label: 'Guardian Details',
				complete:
					userProfileCompletion.getUserProfileCompletionPrivate() === 1,
				action: () =>
					props.changeScreen(
						ETabs.MY_PROFILE,
						EAccountScreens.GUARDIAN_DETAILS,
					),
			},
		)
		sections.sort((a, b) => a.order - b.order);
	}

	return (
		<div className="user_account_panel user_account_panel--account_overview">

			<section className="user_account_panel-profile">

				<div className="user_account_panel-profile-name-wrapper">
					<h4 className="font--h3 theme-title user_account_panel-profile-name">
						{props.userData.full_name}
					</h4>
				</div>

				<div className="user_account_panel-profile-avatar">
					<div className="user_account_panel-profile-avatar-image">
						<UserAvatar user={props.userData} />
					</div>
				</div>

				<div className="user_account_panel-profile-change-avatar theme-text--accent">
					<Dropzone
						onDrop={files => {
							if (files.length) {
								props.dispatch(
									userAvatarUpload(
										props.userData.username,
										props.apiService,
										files[0],
									),
								);
							}
						}}
						multiple={false}
						accept="image/jpeg, image/png, image/gif"
						className="user_account_panel-profile-change-avatar-dropzone"
						activeClassName="user_account_panel-profile-change-avatar-dropzone--active"
						rejectClassName="user_account_panel-profile-change-avatar-dropzone--reject"
						disabled={props.userAvatarUploading}
					>
						<div className="user_account_panel-profile-change-avatar-content">

							<div className="user_account_panel-profile-change-avatar-content-text">
								Change profile picture
							</div>

							<div className="user_account_panel-profile-change-avatar-content-progress-bar">
								{props.userAvatarUploading && (
									<CircularProgress size={15} />
								)}
							</div>

						</div>
					</Dropzone>
				</div>

			</section>

			<section className="user_account_panel-completion">
				<UserCompletion
					showOptionalText={false}
					userData={props.userData}
				/>
			</section>

			<section className="user_account_panel-sections">
				{sections.map((section, index) => {
					return (
						<ButtonBase
							focusRipple={true}
							className="user_account_panel-sections-item"
							onClick={section.action}
							key={index}
							disabled={props.submitting}
						>
							<div className="user_account_panel-sections-item-content">
								<div className="user_account_panel-sections-primary">
									{section.label}
								</div>
								<div className="user_account_panel-sections-secondary">
									{section.complete && (
										<React.Fragment>
											<Pill
												label="Edit"
												active={false}
												disabled={true}
												clickable={false}
											/>
											<CheckIcon className="user_account_panel-sections-secondary-icon" />
										</React.Fragment>
									)}
									{!section.complete && (
										<React.Fragment>
											<Pill
												label="Provide"
												active={true}
												disabled={true}
												clickable={false}
											/>
											<CancelIcon className="user_account_panel-sections-secondary-icon user_account_panel-sections-secondary-icon--gray" />
										</React.Fragment>
									)}
								</div>
							</div>
						</ButtonBase>
					);
				})}
			</section>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAvatarUploading ,userData} = state.user;
	const { userPanelDisplay, userPanelData } = state.userPanel;
	return {
		userPanelDisplay,
		userPanelData,
		userAvatarUploading,
		loggedInUser : userData
	};
}

export default connect(mapStateToProps)(UserAccountOverview);
