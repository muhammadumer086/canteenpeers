<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserReported extends Mailable {
	use Queueable, SerializesModels;

	public $reportedUser;
	public $author;
	public $reportReason;
	public $reportInformation;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(
		$reportedUser,
		$author,
		$reportReason,
		$reportInformation
	) {
		$this->reportedUser = $reportedUser;
		$this->author = $author;
		$this->reportReason = $reportReason;
		$this->reportInformation = $reportInformation;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		$title = "User {$this->author->full_name} reported user {$this->reportedUser->full_name}";

		return $this->view('emails.user-reported')
			->subject($title)
			->with([
				'reportedUser' => $this->reportedUser,
				'author' => $this->author,
				'reportReason' => $this->reportReason,
				'reportInformation' => $this->reportInformation,
			]);
	}
}
