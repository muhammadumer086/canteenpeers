import React from 'react';
import ReactSVG from 'react-svg';
import { connect } from 'react-redux';

import { IApiUser, IApiNotification } from '../../interfaces';

import ButtonBase from '@material-ui/core/ButtonBase';

import Drawer from '@material-ui/core/Drawer';

import ClearIcon from '@material-ui/icons/Clear';

import {
	IStoreState,
	notificationsPanelHide,
	userPanelDisplay,
} from '../../redux/store';

import NotificationsList from './NotificationsList';
import { ApiService, withServices } from '../../services';
import { apiError } from '../../redux/actions/apiError';
import { ETabs } from '../user-panel/UserPanelAccount';

interface IProps {
	userData: IApiUser;
	userAuthenticated: boolean | null;
	notificationsPanelDisplay: boolean;
	notificationsMaxPage: number;
	notificationsData: IApiNotification[];
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	notificationsData: IApiNotification[];
	additionalNotificationsData: IApiNotification[];
	isLoadingNotifications: boolean;
	currentPage: number;
}

class Notifications extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			// Do a copy of the notifications data
			notificationsData: JSON.parse(
				JSON.stringify(props.notificationsData),
			),
			additionalNotificationsData: [],
			isLoadingNotifications: false,
			currentPage: 1,
		};

		this.closePanel = this.closePanel.bind(this);
		this.loadMoreNotifications = this.loadMoreNotifications.bind(this);
	}

	componentDidUpdate(oldProps: IProps) {
		if (
			JSON.stringify(this.props.notificationsData) !==
				JSON.stringify(oldProps.notificationsData) ||
			this.props.notificationsMaxPage !== oldProps.notificationsMaxPage
		) {
			this.setState({
				currentPage: 1,
				additionalNotificationsData: [],
				notificationsData: JSON.parse(
					JSON.stringify(this.props.notificationsData),
				),
				isLoadingNotifications: false,
			});
		}
	}

	public render() {
		if (!this.props.userAuthenticated) {
			return null;
		}
		return (
			<Drawer
				anchor="right"
				open={this.props.notificationsPanelDisplay}
				onClose={this.closePanel}
			>
				<div className="notifications_panel">
					<div className="notifications_panel-header">
						<ButtonBase
							focusRipple={false}
							className="notifications_panel-header-settings"
							onClick={() => {
								this.openSettings();
							}}
						>
							<ReactSVG
								className="notifications_panel-header-settings-icon"
								path={`${
									process.env.STATIC_PATH
								}/icons/settings.svg`}
							/>
						</ButtonBase>
						<div className="notifications_panel-header-title">
							Notifications
						</div>
					</div>
					<NotificationsList
						scrollBottomReached={this.loadMoreNotifications}
						notificationsData={this.state.notificationsData}
						isLoadingNotifications={
							this.state.isLoadingNotifications
						}
					/>
				</div>
				<button
					onClick={this.closePanel}
					className="close_button close_button--primary"
					title="Dismiss"
				>
					<ClearIcon className="close_button-icon" />
				</button>
			</Drawer>
		);
	}

	protected openSettings() {
		this.closePanel();
		this.props.dispatch(
			userPanelDisplay(this.props.userData, ETabs.SETTINGS),
		);
	}

	protected closePanel() {
		this.props.dispatch(notificationsPanelHide());
	}

	protected loadMoreNotifications() {
		if (
			!this.state.isLoadingNotifications &&
			this.state.currentPage < this.props.notificationsMaxPage
		) {
			this.setState(
				{
					isLoadingNotifications: true,
					currentPage: this.state.currentPage + 1,
				},
				() => {
					// Query next page of notifications
					this.props.apiService
						.queryGET(
							`/api/users/me/notifications?page=${
								this.state.currentPage
							}`,
							'data',
						)
						.then(data => {
							if (
								'notifications' in data &&
								data.notifications instanceof Array
							) {
								// Append the notifications to the list of notifications
								const additionalNotificationsData = this.state.additionalNotificationsData.concat(
									data.notifications,
								);
								const notificationsData = this.state.notificationsData.concat(
									additionalNotificationsData,
								);
								this.setState({
									additionalNotificationsData,
									notificationsData,
									isLoadingNotifications: false,
								});
							}
						})
						.catch(error => {
							this.setState({
								isLoadingNotifications: false,
							});
							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData, userAuthenticated } = state.user;
	const {
		notificationsData,
		notificationsPanelDisplay,
		notificationsMaxPage,
	} = state.notifications;
	return {
		notificationsData,
		notificationsPanelDisplay,
		notificationsMaxPage,
		userData,
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(withServices(Notifications));
