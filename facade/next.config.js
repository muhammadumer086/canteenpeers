const inLambda = !!process.env.LAMBDA_TASK_ROOT;
const withSass = !inLambda && require('@zeit/next-sass');
const sassUtils = !inLambda && require('node-sass-utils')(require('node-sass'));
const config = !inLambda && require('dotenv').config();

let assetsPrefix = null;
if (!inLambda && config && config.hasOwnProperty('parsed') && config.parsed) {
	for (const key in config.parsed) {
		if (config.parsed.hasOwnProperty(key)) {
			process.env[key] = config.parsed[key];

			if (key === 'ASSETS_PREFIX_FOLDER') {
				assetsPrefix = config.parsed[key];
			}
		}
	}
}

const configuration = {
	sassLoaderOptions: {
		includePaths: ['styles'],
		functions: {
			'get($keys)': function(keys) {
				keys = keys.getValue().split('.');
				let result = process.env;
				let i;
				for (i = 0; i < keys.length; i++) {
					result = result[keys[i]];
				}
				result = sassUtils.castToSass(result);
				return result;
			},
		},
	},
	webpack: (config, _options) => {
	
		return config;
	},
};

if (assetsPrefix) {
	configuration.assetPrefix = assetsPrefix;
}

try {
	if (!inLambda) {
		module.exports = withSass(configuration);
	} else {
		module.exports = configuration;
	}
} catch (e) {
	console.log('next.config.js error configuration');
	console.log(e);
}
