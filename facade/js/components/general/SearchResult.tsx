import React from 'react';
import ReactSVG from 'react-svg';
import Link from 'next/link';
import { ButtonBase } from '@material-ui/core';
import { ILinkData } from '../../interfaces';
import { ISearchResult } from './SearchModal';

interface IProps {
	resultData: ISearchResult;
	resultLink?: ILinkData;
	handleUserClick?(userType?: ISearchResult): void;
}

const SearchResult: React.FC<IProps> = (props: IProps) => {
	let iconType: string;
	let linkData: ILinkData = null;

	if (props.resultData.type === 'discussion') {
		iconType = 'discussions';
		linkData = {
			href: `/discussions-single?id=${props.resultData.id}`,
			as: `/discussions/${props.resultData.id}`,
		};
	} else if (props.resultData.type === 'resource') {
		iconType = 'resources';
		linkData = {
			href: `/resources-single?id=${props.resultData.id}`,
			as: `/resources/${props.resultData.id}`,
		};
	} else if (props.resultData.type === 'blog') {
		iconType = 'blogs';
		linkData = {
			href: `/blogs-single?id=${props.resultData.id}`,
			as: `/blogs/${props.resultData.id}`,
		};
	} else if (props.resultData.type === 'event') {
		iconType = 'events';
		linkData = {
			href: `/events-single?id=${props.resultData.id}`,
			as: `/events/${props.resultData.id}`,
		};
	} else if (props.resultData.type === 'user') {
		iconType = 'users';
	} else if (props.resultData.type === 'hashtag') {
		iconType = 'hashtags';
	}

	if (props.resultLink) {
		return renderResult(props.resultLink);
	} else if (linkData) {
		return renderResult(linkData);
	} else {
		return renderResult();
	}

	function renderResult(linkData: ILinkData | null = null) {
		return (
			<div className="search_result">
				{!!linkData ? (
					<Link {...linkData}>{renderButtonContent()}</Link>
				) : (
					renderButtonContent()
				)}

				{!!props.resultData.hashtags &&
					!!props.resultData.hashtags.length && (
						<div className="search_result-hashtags">
							{props.resultData.hashtags.map(hashtag => (
								<ButtonBase
									focusRipple={true}
									className="search_result-single_hashtag"
									onClick={() => {
										handleHashtagClick(hashtag);
									}}
									key={hashtag}
								>
									#{hashtag}
								</ButtonBase>
							))}
						</div>
					)}
			</div>
		);
	}

	function renderButtonContent() {
		return (
			<div className="search_result-content">
				<div className="search_result-icon">
					<ReactSVG
						path={`${
							process.env.STATIC_PATH
						}/icons/${iconType}.svg`}
					/>
				</div>

				<div className="search_result-result_name">
					<span>{props.resultData.title}</span>
				</div>

				<ButtonBase
					focusRipple={true}
					className="search_result-button"
					onClick={handleClick}
				/>
			</div>
		);
	}

	function handleClick() {
		props.handleUserClick(props.resultData);
	}

	function handleHashtagClick(hashtag: string) {
		const hashtagObject: ISearchResult = {
			data: null,
			id: hashtag,
			title: hashtag,
			type: 'hashtag',
			objectID: `hashtag/${hashtag}`,
		};
		props.handleUserClick(hashtagObject);
	}
};

export default SearchResult;
