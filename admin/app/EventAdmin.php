<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;
use App\User;
class EventAdmin extends Model
{
	protected $table = 'event_admins';
	/**
	 * The users that created this image
	 */
	public function user() {
		return $this->belongsTo(User::class);
	}

	/**
	 * The event associated with this image
	 */
	public function event() {
		return $this->belongsTo(Event::class);
	}
}
