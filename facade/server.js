const path = require('path');

const express = require('express');
const next = require('next');
// const proxy = require('http-proxy-middleware');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const noIndex = 'NO_INDEX' in process.env && process.env.NO_INDEX;

function createServer() {
	console.log('createServer');
	const server = express();
	// server.get('/service-worker.js', async (req, res) => {
	// 	const filePath = path.join(__dirname, '.next', '/service-worker.js');
	// 	await app.serveStatic(req, res, filePath);
	// });

	// server.use('/api', proxy({
	// 	target: process.env.ADMIN_URL,
	// 	changeOrigin: true
	// }));

	server.use('/_next', express.static(path.join(__dirname, '.next')));
	server.get('/', (req, res) => app.render(req, res, '/'));

	server.get('/discussions/user/:id', (req, res) => {
		const actualPage = '/discussions-user';
		const queryParams = {
			id: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});

	server.get('/discussions/:id', (req, res) => {
		const actualPage = '/discussions-single';
		const queryParams = {
			id: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});

	server.get('/resources/:id', (req, res) => {
		const actualPage = '/resources-single';
		const queryParams = {
			id: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});

	server.get('/events/:id', (req, res) => {
		const actualPage = '/events-single';
		const queryParams = {
			id: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});

	server.get('/blogs/:id', (req, res) => {
		const actualPage = '/blogs-single';
		const queryParams = {
			id: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});

	server.get('/admin/event/:id/registered-users', (req, res) => {
		const actualPage = '/admin-registered-users';
		const queryParams = {
			id: req.params.id,
		};

		app.render(req, res, actualPage, queryParams);
	});

	server.get('/admin/users/:id/activities', (req, res) => {
		const actualPage = '/admin-user-activities';
		const queryParams = {
			id: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});
	server.get('/auth/verify-success/:id', (req, res) => {
		const actualPage = '/auth/verify-success';
		const queryParams = {
			source: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});
	server.get('/auth/verifybysms/:id', (req, res) => {
		const actualPage = '/auth/verifybysms';
		const queryParams = {
			step: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});

	server.get('/auth/register/:id', (req, res) => {
		const actualPage = '/auth/register';
		const queryParams = {
			step: req.params.id,
		};
		app.render(req, res, actualPage, queryParams);
	});
	server.get('*', (req, res) => handle(req, res));
	return server;
}

const server = createServer();

const prepareP = app.prepare().then(() => {
	console.log('App prepared');
	if (process.env.IN_LAMBDA !== 'true') {
		console.log('Starting server on: ' + port);
		server.listen(port);
	}
});

module.exports = { appServer: server, prepareP };
