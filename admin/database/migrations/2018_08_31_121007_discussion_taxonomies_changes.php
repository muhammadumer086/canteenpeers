<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiscussionTaxonomiesChanges extends Migration{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('discussions', function (Blueprint $table) {
			$table->unsignedInteger('topic_id')->nullable();

			$table->foreign('topic_id')
			->references('id')->on('topics')
			->onDelete('cascade');
		});

		Schema::table('situations', function (Blueprint $table) {
			$table->unsignedInteger('parent_id')->nullable();
			$table->string('cancer_type')->nullable();

			$table->foreign('parent_id')
			->references('id')->on('situations')
			->onDelete('cascade');
		});

		Schema::create('discussion_situation', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('discussion_id');
			$table->unsignedInteger('situation_id');

			$table->foreign('discussion_id')
			->references('id')->on('discussions')
			->onDelete('cascade');

			$table->foreign('situation_id')
			->references('id')->on('situations')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('situations', function (Blueprint $table) {
			$table->dropForeign('situations_parent_id_foreign');
			$table->dropColumn('parent_id');
			$table->dropColumn('cancer_type');
		});

		Schema::dropIfExists('user_cancer_types');
		Schema::dropIfExists('cancer_types');
		Schema::dropIfExists('discussion_situation');

		Schema::table('discussions', function (Blueprint $table) {
			$table->dropForeign('discussions_topic_id_foreign');
			$table->dropColumn('topic_id');
		});
	}
}
