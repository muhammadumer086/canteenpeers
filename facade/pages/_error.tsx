import React from 'react';
import { connect } from 'react-redux';

import { IStoreState } from '../js/redux/store';

 

class MyError extends React.Component<any, any> {
	static getInitialProps({ res, err }) {
		const statusCode = res ? res.statusCode : err ? err.statusCode : null;
		return {
			statusCode
		};
	}

	public render() {
		let message: string = 'An unknown error happened';
		if (this.props.statusCode === 404) {
			message = 'Page not found';
		} else if (this.props.statusCode === 403) {
			message = 'You need to be above 16';
		} else if (this.props.statusCode === 401) {
			message = 'Access denied';
		}

		return (
			<div className="page_error_wrapper theme--accent theme-background--gradient">
				<div className="page_error">
					{
						!!this.props.statusCode &&
						<h1 className="font--h1">
							{this.props.statusCode}
						</h1>
					}
					<p className="font--h4">
						{message}
					</p>
				</div>
			</div>
		);
	}
}

function mapStateToProps(_state: IStoreState) {
	return {
	};
}

export default connect(mapStateToProps)(MyError);
