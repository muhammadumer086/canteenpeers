import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { ETabs } from '../../components/user-panel/UserPanelAccount';

import {
	IApiUser
} from '../../interfaces';

export const userPanelDisplay = (
	userData: IApiUser, userPanelTab: ETabs = ETabs.MY_PROFILE, onUserUpdated: () => void = null
) => (dispatch, getState: () => IStoreState) => {
	if (getState().userPanel.userPanelDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.USER_PANEL_DISPLAY,
		userPanelData: userData,
		userPanelTab: userPanelTab,
		onUserUpdated,
	});
}

export const userPanelChangeTab = (userPanelTab: ETabs) => (dispatch, getState: () => IStoreState) => {
	if (userPanelTab === getState().userPanel.userPanelTab) {
		return;
	}
	return dispatch({
		type: actionTypes.USER_PANEL_CHANGE_TAB,
		userPanelTab,
	});
}

export const userPanelHide = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().userPanel.userPanelDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.USER_PANEL_HIDE,
	});
}

export const userPanelUpdated = (userPanelData: IApiUser) => (dispatch, getState: () => IStoreState) => {
	let userData = getState().user.userData;

	if (
		userData &&
		userData.id === userPanelData.id
	) {
		userData = userPanelData;
	}

	return dispatch({
		type: actionTypes.USER_PANEL_UPDATED,
		userPanelData,
		userData,
	});
}
