import React from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';

import { IStoreState } from '../../js/redux/store';
import Router from 'next/router';

import { ApiService } from '../../js/services';
import { IApiUser } from '../../js/interfaces';
import MyError from '../_error';

interface IProps {
	userData: IApiUser | null;
	apiService: ApiService;
	query: any;
	dispatch(action: any): void;
}

interface IState {
	redirect: boolean;
	count: number;
}

class VerifySuccess extends React.Component<IProps, IState> {
	protected timer;
	static getInitialProps({ query }) {
		return { query };
	}
	public constructor(props: IProps) {
		super(props);

		this.state = {
			redirect: false,
			count: 5,
		};
	}

	public componentDidMount() {
		if (typeof window !== 'undefined') {
			setTimeout(() => {
				this.setState({ redirect: true });
			}, 5000);
			this.timer = setInterval(() => {
				if (this.state.count !== 0) {
					this.setState({ count: this.state.count - 1 });
				}
			}, 1000);
		}
	}
	public componentWillUnmount() {
		clearInterval(this.timer);
	}
	public componentDidUpdate(_prevP: IProps, prevS: IState) {
		if (!prevS.redirect && this.state.redirect) {
			if (this.props.query.source === 'email') {
				Router.push('/dashboard');
			} else {
				Router.push('/auth/login');
			}
		}
	}
	public render() {
		if (
			!this.props.userData ||
			!this.props.userData.hasOwnProperty('first_name') ||
			!this.props.userData.hasOwnProperty('email')
		) {
			return <MyError statusCode={401} />;
		}
		return (
			<div className="auth_wrapper theme--accent_dark auth_wrapper--with_header">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<div className="auth_page-header">
					<img
						className="auth_page-header-logo"
						src={`${process.env.STATIC_PATH}/images/logo-canteen-horizontal.png`}
					/>
				</div>
				<section className="auth">
					<header className="auth-header">
						<h1 className="auth-title font--h4">
							Hi{' '}
							{this.props.userData &&
								this.props.userData.username}
							, we have successfully verified your account. You
							will be redirected in {this.state.count}
						</h1>
					</header>
				</section>
			</div>
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(VerifySuccess);
