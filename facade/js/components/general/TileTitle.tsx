import React from 'react';
import Dotdotdot from 'react-dotdotdot';

interface IProps {
	content: string;
	clamp: boolean;
	clampLines?: number;
}

const TileTitle: React.StatelessComponent<IProps> = props => {
	return (
		<React.Fragment>
			{props.clamp && props.clampLines ? (
				<Dotdotdot clamp={props.clampLines}>
					<h5 className="tile_title-title theme-title font--h5">
						{props.content}
					</h5>
				</Dotdotdot>
			) : (
				<h5 className="tile_title-title theme-title font--h5">
					{props.content}
				</h5>
			)}
		</React.Fragment>
	);
};

export default TileTitle;
