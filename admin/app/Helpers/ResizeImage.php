<?php

namespace App\Helpers;

use Image;

class ResizeImage {
	public function maxSize($file, $maxWidth = 1024, $maxHeight = null) {
		$image = Image::make($file)
			->orientate()
			->widen($maxWidth, function ($constraint) {
				$constraint->upsize();
			});
		if ($maxHeight) {
			$image->heighten($maxHeight, function ($constraint) {
				$constraint->upsize();
			});
		}
		$hash = md5($image->__toString());
		$tmpPath = storage_path("images/{$hash}.jpg");
		$image->save($tmpPath, 70);
		$image->destroy();

		return $tmpPath;
	}
}

?>
