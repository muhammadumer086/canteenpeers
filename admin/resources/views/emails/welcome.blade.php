@extends('layouts.email')

@section('plain_text')
	@if (isset($user->first_name))
		Hi {{$user->first_name}},
	@else
		Hi,
	@endif

	A warm welcome to you from the Canteen Online Support Service. We just wanted to send you a quick message to say hi and welcome you to Canteen Connect, our online community for parents dealing with cancer in their family.
@endsection

@section('content')
	@component('emails.components.paragraph')
		@if (isset($user->first_name))
			Hi {{$user->first_name}},
		@else
			Hi,
		@endif
	@endcomponent

	@component('emails.components.paragraph')
		A warm welcome to you from the Canteen Online Support Service. We just wanted to send you a quick message to say hi and welcome you to Canteen Connect, our online community for parents dealing with cancer in their family.
	@endcomponent

	@component('emails.components.paragraph')
		People sign up to our community for lots of different reasons. Whether you're looking to connect with others in the same boat, or to chat to a counsellor and get support, there are a range of ways Canteen Connect can help you navigate your cancer experience.
	@endcomponent

	@component('emails.components.paragraph')
		Canteen Connect offers a range of platforms to help you feel supported and, if you choose, to share your own experiences and feelings. We also offer a wide range of evidence-based resources to help you navigate your way through this difficult period.
	@endcomponent

	@component('emails.components.paragraph')
		With Canteen offices located around Australia, we are able to offer face-to-face support to families dealing with cancer. <a href="https://canteen.org.au/contact/division-offices/">Click to learn more</a>.
	@endcomponent

	@component('emails.components.paragraph')
		There's a lot to discover, so if you're looking for a place to start, we recommend visiting our discussions and introducing yourself, to start meeting others like you!
	@endcomponent

	@component('emails.components.paragraph')
		We're always looking to improve our services, so if you have any feedback or questions about Canteen Connect or the other work we do, <a href="https://www.canteen.org.au/contact/">please get in touch</a>!
	@endcomponent

	@component('emails.components.paragraph')
		Thanks for being part of our community. We look forward to catching you online again soon.
	@endcomponent

@endsection
