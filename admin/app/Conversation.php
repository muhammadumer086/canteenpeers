<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends Model {

	use SoftDeletes;

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'users_hash',
		'updated_at',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'group_conversation' => 'boolean',
	];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = [
		'users_list',
	];

	/*public function getUpdatedAtAttribute()
	{
		$updatedATF = isset($this->attributes['updated_at'])?$this->attributes['updated_at']:null;
		if(isset($updatedATF) && is_string($updatedATF))
		{
			return Carbon::parse(date($updatedATF));
		}
		return '';
	}*/

	/**
	 * The messages associated with the conversation
	 */
	public function messages() {
		return $this->hasMany('App\Message');
	}

	/**
	 * The users part of the conversation
	 */
	public function users() {
		return $this->belongsToMany('App\User')->withTrashed();
	}

	/**
	 * Return a string containing the list of all the users in the conversation
	 */
	public function getUsersListAttribute() {
		return $this->users()->pluck('users.id')->toArray();
	}

}
