import { connect } from 'react-redux';
import ButtonBase from '@material-ui/core/ButtonBase';
import { IApiDiscussion } from '../../interfaces';
import { discussionSupportDisplay } from '../../redux/actions';
import { IStoreState } from '../../redux/store';

interface IProps {
	userAuthenticated: boolean;
	discussion: IApiDiscussion;
	dispatch(action: any): void;
}

export const DiscussionSupportMetaWrapped: React.FC<IProps> = props => {
	if (!props.discussion.support_count) {
		return null;
	}

	const supportString = getStringCountSupport(props.discussion);

	return (
		<div className="discussion_support_meta">
			<ButtonBase
				className="discussion_support_meta-button"
				focusRipple={true}
				onClick={() => {
					props.dispatch(discussionSupportDisplay(props.discussion));
				}}
				disabled={!props.userAuthenticated}
			>
				{supportString}
			</ButtonBase>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export const DiscussionSupportMeta = connect(mapStateToProps)(
	DiscussionSupportMetaWrapped,
);

function getStringCountSupport(discussion: IApiDiscussion): string | null {
	let resultParts: string[] = [];
	let numberUsersListed: number = 0;
	if (discussion.liked || discussion.hugged) {
		resultParts.push('You');
		++numberUsersListed;
	}

	// Get the latest of the hugged and likes
	if (discussion.user_likes && discussion.user_likes.length) {
		resultParts.push(discussion.user_likes[0].full_name);
		++numberUsersListed;
	} else if (discussion.user_hugs && discussion.user_hugs.length) {
		resultParts.push(discussion.user_hugs[0].full_name);
		++numberUsersListed;
	} else if (discussion.user_replies && discussion.user_replies.length) {
		resultParts.push(discussion.user_replies[0].full_name);
		++numberUsersListed;
	}

	// Get the total additional support
	const additionalSupportCount = discussion.support_count - numberUsersListed;
	if (additionalSupportCount === 1) {
		resultParts.push('1 other');
	} else if (additionalSupportCount > 1) {
		resultParts.push(`${additionalSupportCount} others`);
	}

	return resultParts.length
		? concatenateListWithAnd(resultParts) + ' showed support'
		: null;
}
/**
 * Takes an array of string and concatenate it with "and" for the last item
 */
function concatenateListWithAnd(stringParts: string[]): string {
	if (stringParts.length >= 2) {
		const lastItem = stringParts.pop();
		return [stringParts.join(', '), lastItem].join(' and ');
	}
	return stringParts.join('');
}
