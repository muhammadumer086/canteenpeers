import React from 'react';
import { connect } from 'react-redux';
import { ButtonBase } from '@material-ui/core';

import UserAvatar from '../general/UserAvatar';
import { userPanelDisplay } from '../../redux/store';
import TileTitle from '../general/TileTitle';
import Pill from '../general/Pill';

import { IContentTileData, ILinkData } from '../../interfaces';
import { ButtonBaseLink } from '../general/ButtonBaseLink';
import { defaultBlogImagesMap } from '../blogs/BlogTile';

interface IProps {
	contentTileData: IContentTileData;
	hasAction: boolean;
	dispatch(values: any): void;
}

const ContentTile: React.FC<IProps> = (props: IProps) => {
	const classes = ['content_tile', 'theme--main'];
	let imageUrl: string | null = null;

	if (props.contentTileData.imageUrl) {
		classes.push('content_tile--with-thumbnail');
		imageUrl = props.contentTileData.imageUrl;
	} else if (
		props.contentTileData.type &&
		props.contentTileData.type === 'blog' &&
		props.contentTileData.topic &&
		props.contentTileData.topic.slug in defaultBlogImagesMap
	) {
		imageUrl = `${process.env.STATIC_PATH}/images/blog-default/${
			defaultBlogImagesMap[props.contentTileData.topic.slug]
		}`;
	}

	const openUserPanel = () => {
		props.dispatch(userPanelDisplay(props.contentTileData.user));
	};

	const linkData: ILinkData = {
		href: `${props.contentTileData.linkPrefix}-single?id=${
			props.contentTileData.slug
		}`,
		as: `${props.contentTileData.linkPrefix}/${props.contentTileData.slug}`,
	};

	return (
		<React.Fragment>
			<div className={classes.join(' ')}>
				<div className="content_tile-grid_wrapper">
					{imageUrl === null && props.contentTileData.user ? (
						<div className="content_tile-image content_tile-image--user">
							<ButtonBase
								className="content_tile-button"
								onClick={openUserPanel}
							/>
							<UserAvatar user={props.contentTileData.user} />
							<div className="general_tile-user_name hm-t8">
								by{' '}
								<span className="theme-text--accent">
									{props.contentTileData.user.full_name}
								</span>
							</div>
						</div>
					) : (
						<div
							className="content_tile-image"
							style={{
								backgroundImage: `url(${imageUrl})`,
							}}
						/>
					)}

					<div className="content_tile-content">
						{props.hasAction && (
							<ButtonBaseLink
								focusRipple={true}
								className="content_tile-button"
								hrefAs={linkData.as}
								href={linkData.href}
							/>
						)}
						{!!props.contentTileData.topic && (
							<div className="hm-b8">
								<Pill
									withoutBorder={true}
									label={props.contentTileData.topic.title}
									active={true}
									disabled={false}
									clickable={true}
									href={`${
										props.contentTileData.linkPrefix
									}?topic=${
										props.contentTileData.topic.slug
									}`}
								/>
							</div>
						)}
						<TileTitle
							content={props.contentTileData.title}
							clamp={true}
							clampLines={2}
						/>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

function mapStateToProps() {
	return {};
}

export default connect(mapStateToProps)(ContentTile);
