import React          from 'react';
import { ButtonBase } from '@material-ui/core';

interface IProps {
	navigationData: any;
}

const FixedNavigationBar: React.FC<IProps> = (props: IProps) => {
	const classNamesAll = ['fixed_bar-button'];

	return (
		<div className="fixed_bar theme--accent">
			{
				props.navigationData.map((buttonData, index) => {
					let count;
					if (buttonData.count) {
						count = `(${buttonData.count})`;
					} else {
						count = ``;
					}

					return (
						<ButtonBase
							key={index}
							className={classNamesAll.join(' ')}
							onClick={buttonData.action}
							focusRipple={true}
						>
								{buttonData.label} {count}
							<span className="fixed_bar-button-border"></span>
						</ButtonBase>
					);
				})
			}
		</div>
	)
}

export default FixedNavigationBar;
