import React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import { ISelectOption } from '../../interfaces';

interface IProps {
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	options: ISelectOption[];
	disabledOptions?: string[], 
	disabled?: boolean;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
}

export const CheckboxGroupFormik: React.FC<IProps> = (props: IProps) => {
	function findOptionIndex(singleOption: ISelectOption, values: string[]) {
		return values.indexOf(singleOption.value);
	}

	function handleChange(singleOption: ISelectOption, event) {
		let isChanged = false;
		const values = JSON.parse(JSON.stringify(props.values[props.name]));

		const optionIndex = findOptionIndex(singleOption, values);
		if (event.target.checked) {
			// Add option while preventing duplicates
			if (optionIndex < 0) {
				values.push(singleOption.value);
				isChanged = true;
			}
		} else {
			// remove option
			if (optionIndex >= 0) {
				values.splice(optionIndex, 1);
				isChanged = true;
			}
		}

		if (isChanged) {
			props.setFieldValue(props.name, values);
			props.setFieldTouched(props.name, true, true);
		}
	}

	function isChecked(singleOption: ISelectOption): boolean {
		return findOptionIndex(singleOption, props.values[props.name]) >= 0;
	}

	function isDisabled(singleOption: string): boolean {
		return props.disabledOptions.findIndex(option=>option===singleOption)>-1;
	}

	return (
		<FormControl
			error={props.touched[props.name] && !!props.errors[props.name]}
			className="form-input form-input-checkbox_wrapper"
			disabled={props.disabled}
		>
			<FormLabel className="form-input-label">{props.label}</FormLabel>
			<FormGroup className="form-input-checkbox_group">
				{props.options.map(renderOption)}
			</FormGroup>
			{props.touched[props.name] && !!props.errors[props.name] && (
				<FormHelperText className="color-error">{props.errors[props.name]}</FormHelperText>
			)}
		</FormControl>
	);

	function renderOption(option: ISelectOption, key?: number) {
		const checked = isChecked(option);
		const disabled=props.disabledOptions&&isDisabled(option.value);
		return (
			<FormControlLabel
				className="form-input-checkbox"
				control={
					<Checkbox
						className="form-input-checkbox-tick"
						name={props.name}
						checked={checked}
						disabled={disabled}
						onChange={event => {
							handleChange(option, event);
						}}
						value={option.label}
						color="primary"
					/>
				}
				label={
					option.tooltip ? (
						<span>
							{option.label}
							<br />
							<em className="form-input-checkbox-tooltip">
								{option.tooltip}
							</em>
						</span>
					) : (
						option.label
					)
				}
				key={key}
			/>
		);
	}
};
