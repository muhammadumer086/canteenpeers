import moment from 'moment';

const getLocadate=(rDate)=>{
    var offset = moment().utcOffset();
    const res= moment.utc(rDate).utcOffset(offset).format('YYYY-MM-DD HH:mm');
    return res;
}
export default getLocadate;