<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToAgeRanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('age_ranges', function (Blueprint $table) {
            $table->timestamps();
        });

        Schema::table('age_range_announcement', function (Blueprint $table) {
            $table->dropForeign(['age_range_id']);

            $table->foreign('age_range_id')->references('id')->on('age_ranges')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('age_range_announcement', function (Blueprint $table) {
            $table->dropForeign(['age_range_id']);

            $table->foreign('age_range_id')->references('id')->on('age_ranges')->onDelete('cascade');
        });

        Schema::table('age_ranges', function (Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at']);
        });
    }
}
