import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import buildUrl from 'build-url';
import { withRouter } from 'next/router';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';



import { ApiService, AuthService } from '../js/services';

import {
	IApiUser,
	IApiResource,
	IApiPagination,
	IApiTopic,
	IApiSituation,
} from '../js/interfaces';

import isAdmin from '../js/helpers/isAdmin';

import { IStoreState, apiError } from '../js/redux/store';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import ResourceTile from '../js/components/resources/ResourceTile';
import ListLoading from '../js/components/general/ListLoading';
import FilterResults from '../js/components/general/FilterResults';
import SectionTitle from '../js/components/general/SectionTitle';
import FeaturedItemsList from '../js/components/blogs/FeaturedItemsList';
import Footer from '../js/components/general/Footer';

import {
	IFilters,
	IFiltersSelected,
	IFiltersGroup,
	FilterResultsHelpers,
} from '../js/components/general/FilterResults';

import { GTMDataLayer } from '../js/helpers/dataLayer';
import ReactSVG from 'react-svg';

enum EResourceTypes {
	all = 'all',
	saved = 'saved',
}

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	apiService: ApiService;
	authService: AuthService;
	queryType: EResourceTypes | null;
	resources: {
		featured?: IApiResource[];
		data: {
			resources: IApiResource[];
		};
		meta: IApiPagination;
	};
	topicsLoading: boolean;
	topics: IApiTopic[];
	situationsLoading: boolean;
	situations: IApiSituation[];
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	activeTopics: string[];
	isLoadingResource: boolean;
	currentPage: number;
	maxPages: number;
	featuredResources: IApiResource[];
	resources: IApiResource[];
	resourcesTotal: number;
	resourceType: EResourceTypes;
	filters: IFilters;
	selectedFilters: IFiltersSelected;
}

const NoMatches = props => {
	return (
		<div className="no_matches">
			<div className="no_matches-container">
				<span className="font--h3">
					There are no results matching your filter criteria, please
					update the filter and try again
				</span>

				<Button
					className="no_matches-button"
					variant="contained"
					color="primary"
					onClick={props.handleReset}
				>
					Reset Filter
				</Button>
			</div>
		</div>
	);
};

class Resources extends React.Component<IProps, IState> {
	protected filterHelpers: FilterResultsHelpers = new FilterResultsHelpers();
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ query, apiService }) {
		let queryType: EResourceTypes | null = null;
		const params: any = {};
		if (
			'type' in query &&
			query.type in EResourceTypes &&
			query.type !== EResourceTypes.all
		) {
			queryType = query.type;
			params.type = query.type;
		}

		if ('topic' in query) {
			params[`topics[0]`] = query.topic;
		}

		const url = buildUrl(null, {
			path: '/api/resources',
			queryParams: params,
		});

		const resources = await (apiService as ApiService).queryGET(url);

		return {
			queryType,
			resources,
		};
	}

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			activeTopics: [],
			isLoadingResource: false,
			currentPage: props.resources.meta.current_page,
			maxPages: props.resources.meta.last_page,
			featuredResources:
				'featured' in props.resources ? props.resources.featured : [],
			resources: props.resources.data.resources,
			resourcesTotal: props.resources.meta.total,
			resourceType: props.queryType
				? props.queryType
				: EResourceTypes.all,
			filters: this.generateFilters(),
			selectedFilters: {
				filters: [],
			},
		};

		this.handleResourcesTypeChange = this.handleResourcesTypeChange.bind(
			this,
		);
		this.loadResources = this.loadResources.bind(this);
		this.handleFiltersToggle = this.handleFiltersToggle.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
		this.handleFiltersReset = this.handleFiltersReset.bind(this);
		this.handleFiltersSubmit = this.handleFiltersSubmit.bind(this);
		this.trackFilterUpdate = this.trackFilterUpdate.bind(this);
		this.trackRelatedContent = this.trackRelatedContent.bind(this);
	}

	public componentDidMount() {
		this.updateFiltersBasedOnUrl();
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.situations !== prevProps.situations ||
			this.props.topics !== prevProps.topics
		) {
			this.setState(
				{
					filters: this.generateFilters(),
					selectedFilters: {
						filters: [],
					},
				},
				() => {
					this.updateData();
				},
			);
		}

		if (
			JSON.stringify(this.props.router.query) !==
			JSON.stringify(prevProps.router.query)
		) {
			this.updateFiltersBasedOnUrl();
		}
	}

	public render() {
		const url = buildUrl(process.env.APP_URL, {
			path: `/resources/`,
		});

		const seoFeaturedImage = `${process.env.STATIC_PATH
			}/images/og-facebook-default.jpg`;

		const resourceIconUrl = `${process.env.STATIC_PATH
			}/icons/resources.svg`;

		// const seoDescription =
		// 	'Check out our variety of resources to help Australian teenagers whose lives have been affected by cancer. Register to connect with other teens &amp; get help.';

		// const pageTitle =
		// 	'Cancer Resources For young adult & Teens | Canteen Connect';

		return (
			<div>
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
					<meta name="og:url" property="og:url" content={url} />
					<meta name="og:type" property="og:type" content="article" />
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadResources}>
					<header className="hero theme--accent">
						<div className="hero-heading">
							<span className="hero-heading-icon">
								<ReactSVG path={resourceIconUrl} />
							</span>
							<span className="hero-heading-text font--h4">
								Resources
							</span>
						</div>
					</header>
					{this.renderFixedBar()}
					{this.state.resourceType === EResourceTypes.all && (
						<FilterResults
							filters={this.state.filters}
							selectedFilters={this.state.selectedFilters}
							suggestTopic={true}
							onToggle={this.handleFiltersToggle}
							onUpdateFilters={this.handleFiltersUpdate}
							onReset={this.handleFiltersReset}
							onSubmit={this.handleFiltersSubmit}
							total={this.state.resourcesTotal}
							totalString={{
								noResults: 'No resources',
								singular: '1 resource',
								plural: 'resources',
							}}
							gtmDataLayer={this.trackFilterUpdate}
						/>
					)}

					{this.renderResourceList()}
					<ListLoading active={this.state.isLoadingResource} />
					<Footer />
				</LayoutDashboard>
			</div>
		);
	}

	protected handleFiltersReset() {
		this.setState({
			filters: this.generateFilters(),
			selectedFilters: {
				filters: [],
			},
		});
	}

	protected updateData() {
		// Scroll to the top (browser only)
		if (!!(process as any).browser) {
			const dashboard = window.document.querySelector(
				'.layout_dashboard-content_container',
			);
			dashboard.scroll({
				top: 0,
				left: 0,
			});
		}

		this.loadResources(false);
	}

	protected buildQueryParams(withPage: boolean = false) {
		const params: any = {};

		if (this.state.resourceType !== EResourceTypes.all) {
			params.type = this.state.resourceType;
		} else {
			this.state.selectedFilters.filters.forEach(singleFilter => {
				if (singleFilter.filterValues instanceof Array) {
					singleFilter.filterValues.forEach((filterValue, index) => {
						params[`${singleFilter.paramName}[${index}]`] =
							filterValue.value;
					});
				} else {
					params[singleFilter.paramName] =
						singleFilter.filterValues.value;
				}
			});
		}

		if (this.state.activeTopics.length) {
			this.state.activeTopics.map((topicSlug, index) => {
				params[`topics[${index}]`] = topicSlug;
			});
		}

		if (!this.state.filters.filterToggle) {
			params.anySituation = true;
		}

		if (withPage) {
			params.page = this.state.currentPage + 1;
		}

		return buildUrl(null, {
			path: '/api/resources',
			queryParams: params,
		});
	}

	protected loadResources(append: boolean = true) {
		if (
			!this.state.isLoadingResource &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			this.setState(
				{
					isUpdating: !append,
					isLoadingResource: true,
				},
				() => {
					const url = this.buildQueryParams(append);

					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'resources' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let resources: IApiResource[] =
									response.data.resources;
								let featuredResources: IApiResource[] = this
									.state.featuredResources;

								if (append) {
									resources = JSON.parse(
										JSON.stringify(this.state.resources),
									);
									(response.data
										.resources as IApiResource[]).forEach(
											singleResource => {
												resources.push(singleResource);
											},
										);
								}

								if (!append) {
									featuredResources =
										'featured' in response
											? response.featured
											: [];
								}

								this.setState({
									isUpdating: false,
									isLoadingResource: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									resourcesTotal: response.meta.total,
									featuredResources,
									resources,
								});
							} else {
								this.props.dispatch(
									apiError(['Invalid data returned by API']),
								);
							}
						})
						.catch(error => {
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isUpdating: false,
								isLoadingResource: false,
							});
						});
				},
			);
		}
	}

	protected renderFixedBar() {
		if (
			!this.props.userAuthenticated ||
			!(
				this.props.userData &&
				(this.props.userData.saved_resources_count &&
					this.props.userData.saved_resources_count > 0)
			)
		) {
			return null;
		}

		const classNamesAll = ['fixed_bar-button'];
		const classNamesActive = ['fixed_bar-button'];
		const classNamesFollowed = ['fixed_bar-button'];

		switch (this.state.resourceType) {
			case EResourceTypes.all:
				classNamesAll.push('fixed_bar-button--active');
				classNamesActive.push('fixed_bar-button--inactive');
				classNamesFollowed.push('fixed_bar-button--inactive');
				break;
			case EResourceTypes.saved:
				classNamesAll.push('fixed_bar-button--inactive');
				classNamesActive.push('fixed_bar-button--active');
				classNamesFollowed.push('fixed_bar-button--inactive');
				break;
		}

		return (
			<div className="fixed_bar fixed_bar--with_tabs theme--accent">
				<Tabs
					className="fixed_bar-tabs"
					value={this.state.resourceType}
					onChange={this.handleResourcesTypeChange}
					indicatorColor="secondary"
					textColor="inherit"
					variant="scrollable"
				>
					<Tab label="All Resources" value={EResourceTypes.all} />
					{!!this.props.userData &&
						!!this.props.userData.saved_resources_count &&
						this.props.userData.saved_resources_count > 0 && (
							<Tab
								label={`Saved Resources (${this.props.userData.saved_resources_count
									})`}
								value={EResourceTypes.saved}
							/>
						)}
				</Tabs>
			</div>
		);
	}

	protected renderResourceList() {
		return (
			<React.Fragment>
				{/* Render fetaured images */}
				{this.state.featuredResources &&
					!!this.state.featuredResources.length &&
					this.renderFeaturedResources()}

				{this.state.resources &&
					!!this.state.resources.length &&
					this.renderResources()}

				{this.state.resources.length === 0 &&
					!!this.state.activeTopics.length && (
						<NoMatches handleReset={this.handleFiltersReset} />
					)}
			</React.Fragment>
		);
	}

	protected renderFeaturedResources() {
		return (
			<div className="featured_items-wrapper">
				<SectionTitle
					label="Featured Resources"
					iconSlug="star-outline"
				/>

				<FeaturedItemsList
					featuredResources={this.state.featuredResources}
					disabled={this.state.isLoadingResource}
				/>
			</div>
		);
	}

	protected renderResources() {
		return (
			<div className="resources_listings">
				<div className="resources_listings-container">
					<SectionTitle label="All Resources" iconSlug="resources" />
					{this.state.resources.map((data, index) => {
						return (
							<ResourceTile
								key={index}
								disabled={this.state.isUpdating}
								resourceLinkPrexfix="resources"
								resourceData={data}
								gtm={this.trackRelatedContent}
							/>
						);
					})}
				</div>
			</div>
		);
	}

	protected handleResourcesTypeChange(_event, type: EResourceTypes) {
		this.setState(
			{
				resourceType: type,
			},
			() => {
				this.updateData();
			},
		);
	}

	protected handleFiltersUpdate(filters: IFilters) {
		this.setState({
			filters,
		});
	}

	protected handleFiltersSubmit(selectedFilters: IFiltersSelected) {
		this.setState(
			{
				selectedFilters,
				filters: this.filterHelpers.updateFiltersBasedOnSelectedFilters(
					selectedFilters,
					this.state.filters,
				),
			},
			() => {
				this.updateData();
			},
		);
	}

	protected handleFiltersToggle() {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.state.filters),
		);

		filters.filterToggle = !filters.filterToggle;

		if (filters.filterToggle) {
			if (this.props.userAuthenticated) {
				const cancerTypesIndex = filters.filterGroups.findIndex(
					value => {
						return value.paramName === 'situations';
					},
				);
				if (cancerTypesIndex >= 0) {
					filters.filterGroups.splice(cancerTypesIndex, 1);
				}

				// Remove the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					filters.filterGroups[
						topicsIndex
					].filters = filters.filterGroups[
						topicsIndex
					].filters.filter(filterValue => {
						return !!this.props.topics.find(topic => {
							return (
								topic.category === 'resources' &&
								topic.slug === filterValue.value &&
								(topic.is_user_topic ||
									isAdmin(this.props.userData))
							);
						});
					});
				}
			}
		} else {
			if (this.props.userAuthenticated) {
				// Add the situations at the beginning
				const situationsGroup: IFiltersGroup = {
					label: null,
					noBorder: true,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: !!this.props.userData.situations.find(
									userSituation => {
										return (
											userSituation.name ===
											situation.name
										);
									},
								),
							};
						}),
				};
				filters.filterGroups.unshift(situationsGroup);

				// add the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					this.props.topics.forEach(topic => {
						if (
							!topic.is_user_topic &&
							!isAdmin(this.props.userData) &&
							topic.category === 'resources'
						) {
							filters.filterGroups[topicsIndex].filters.push({
								label: topic.title,
								value: topic.slug,
								active: false,
							});
						}
					});
				}
			}
		}

		this.setState({
			filters,
		});
	}

	protected updateFiltersBasedOnUrl() {
		if ('topic' in this.props.router.query) {
			const selectedFilters: IFiltersSelected = {
				filters: [],
			};

			// find the topic
			const topicFromUrl: IApiTopic | null = this.props.topics.find(
				singleTopic => {
					return (
						singleTopic.slug ===
						(this.props.router.query.topic as string) &&
						singleTopic.category === 'discussions'
					);
				},
			);

			if (topicFromUrl) {
				selectedFilters.filters.push({
					paramName: 'topics',
					filterValues: [
						{
							label: topicFromUrl.title,
							value: topicFromUrl.slug,
							active: true,
						},
					],
				});
			}

			this.setState(
				{
					selectedFilters,
					filters: this.generateFilters(topicFromUrl),
				},
				() => {
					this.updateData();
				},
			);
		}
	}

	protected generateFilters(topicFromUrl?: IApiTopic | null): IFilters {
		const filters: IFilters = {
			filtersTitle: 'Filter Resources',
			withToggle: this.props.userAuthenticated,
			filterToggleLabel:
				'Only show resources directly related to my cancer experience',
			filterToggle: true,
			filterGroups: [],
		};

		if (!this.props.situationsLoading && !this.props.topicsLoading) {
			// Add the situations for the un-authenticated users
			if (!this.props.userAuthenticated) {
				filters.filterGroups.push({
					label: null,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: false,
							};
						}),
				});
			}

			// Add the topics
			filters.filterGroups.push({
				label: null,
				paramName: 'topics',
				filters: this.props.topics
					.filter(topic => {
						return (
							topic.category === 'resources' &&
							(!this.props.userAuthenticated ||
								isAdmin(this.props.userData) ||
								topic.is_user_topic)
						);
					})
					.map(topic => {
						let active = false;
						if (topicFromUrl) {
							active = topic.slug === topicFromUrl.slug;
						}

						return {
							label: topic.title,
							value: topic.slug,
							active,
						};
					}),
			});
		}

		return filters;
	}

	protected trackFilterUpdate(value: string) {
		this.GTM.pushEventToDataLayer({
			event: 'filterResources',
			filter: value,
		});
	}

	protected trackRelatedContent(title: string) {
		this.GTM.pushEventToDataLayer({
			event: 'nextContent',
			title,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { situations } = state.situations;
	const { topics } = state.topics;
	return {
		userAuthenticated,
		userData,
		situations,
		situationsLoading: state.situations.loading,
		topics,
		topicsLoading: state.topics.loading,
	};
}

export default connect(mapStateToProps)(withRouter(Resources as any));
