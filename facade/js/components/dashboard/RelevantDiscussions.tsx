import React       from 'react';
import { connect } from 'react-redux';

import DiscussionTile from '../discussions/DiscussionTile';
import SubTitleDetail from '../general/SubTitleDetail';

import { IApiDiscussion } from '../../interfaces';
import { ApiService, withServices } from '../../services';

import { IStoreState } from '../../redux/store';

interface IProps {
	title: string;
	discussions: IApiDiscussion[];
	apiService: ApiService;
	userAuthenticated: boolean|null;
}

const RelevantDiscussions: React.FC<IProps> = (props: IProps) => {
	const discussionLimit = 3;

	const linkData = {
		href: '/discussions',
		as: '/discussions'
	};

	return (
		<section className="relevant_discussions">
			<div className="relevant_discussions-container">
				<SubTitleDetail
					icon="discussions"
					title={props.title}
					linkData={linkData}
					linkLabel="All discussions"
				/>

				<div className="relevant_discussions-tiles">
					{
						props.discussions.slice(0, discussionLimit).map((discussion, index) => {
							return (
								<DiscussionTile
									key={index}
									isNarrow={true}
									useLastActivity={true}
									discussion={discussion}
									apiService={props.apiService}
								/>
							)
						})
					}
				</div>
			</div>
		</section>
	)
}

function mapStateToProps(state: IStoreState) {
	const {
		userAuthenticated,
	} = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(withServices(RelevantDiscussions));
