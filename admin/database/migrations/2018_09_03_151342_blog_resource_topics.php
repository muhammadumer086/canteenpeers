<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlogResourceTopics extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('blog_situation', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('blog_id');
			$table->unsignedInteger('situation_id');

			$table->foreign('blog_id')
			->references('id')->on('blogs')
			->onDelete('cascade');

			$table->foreign('situation_id')
			->references('id')->on('situations')
			->onDelete('cascade');
		});

		Schema::create('resource_situation', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resource_id');
			$table->unsignedInteger('situation_id');

			$table->foreign('resource_id')
			->references('id')->on('resources')
			->onDelete('cascade');

			$table->foreign('situation_id')
			->references('id')->on('situations')
			->onDelete('cascade');
		});

		Schema::table('blogs', function (Blueprint $table) {
			$table->unsignedInteger('topic_id')->nullable();

			$table->foreign('topic_id')
			->references('id')->on('topics')
			->onDelete('cascade');
		});

		Schema::table('resources', function (Blueprint $table) {
			$table->unsignedInteger('topic_id')->nullable();

			$table->foreign('topic_id')
			->references('id')->on('topics')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('blog_situation');
		Schema::dropIfExists('resource_situation');

		Schema::table('blogs', function (Blueprint $table) {
			$table->dropForeign('blogs_topic_id_foreign');
			$table->dropColumn('topic_id');
		});

		Schema::table('resources', function (Blueprint $table) {
			$table->dropForeign('resources_topic_id_foreign');
			$table->dropColumn('topic_id');
		});
	}
}
