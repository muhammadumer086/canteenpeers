import { actionTypes } from '../actions';

export interface IStoreState {
	apiSuccess: string[],
	apiSuccessShowing: boolean;
}

export const apiSuccess = (
	state: IStoreState = {
		apiSuccess: [],
		apiSuccessShowing: false,
	},
	action
): IStoreState => {
	switch (action.type) {
		case actionTypes.API_SUCCESS:
			return Object.assign({}, state, {
				apiSuccess: action.apiSuccess,
				apiSuccessShowing: true
			});
		case actionTypes.API_SUCCESS_DISMISS:
			return Object.assign({}, state, {
				apiSuccessShowing: false
			});
		default:
			return state;
	}
}
