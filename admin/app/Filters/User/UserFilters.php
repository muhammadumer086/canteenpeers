<?php

namespace App\Filters\User;

use App\Filters\FiltersAbstract;
use App\User;

use App\Filters\User\{ColumnOrder, NameFilter, UsernameFilter, EmailFilter};

class UserFilters extends FiltersAbstract {
	/**
	 * Default course filters.
	 *
	 * @var array
	 */
	protected $filters = [
		'name' => NameFilter::class,
		'username' => UsernameFilter::class,
		'email' => EmailFilter::class,
		'sort' => ColumnOrder::class,
	];

	public static function mappings() {
		return $map;
	}
}
