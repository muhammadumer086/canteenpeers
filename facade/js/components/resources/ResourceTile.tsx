import React from 'react';
import Dotdotdot from 'react-dotdotdot';

import { ButtonBaseLink } from '../general/ButtonBaseLink';

import TileTitle from '../general/TileTitle';
import TileAuthor from '../general/TileAuthor';

import { getExcerpt } from '../../helpers/getExcerpt';
import { IApiResource } from '../../interfaces';

//TODO: update interafce when structure in API has changed
//TODO: abstract into seperate component
interface IProps {
	disabled: boolean;
	resourceData: IApiResource;
	resourceLinkPrexfix: string;
	gtm?(title: string): void;
}

const ResourceTile: React.FC<IProps> = (props: IProps) => {
	const classNames = ['general_tile', 'theme--main'];
	const leftAreaClassName = ['general_tile-image'];
	const excerpt = getExcerpt(props.resourceData.content);

	const resourceTileAuthor = {
		authorAvatarUrl: `${process.env.STATIC_PATH}/icons/canteen-icon.svg`,
		authorUsername: 'Canteen',
		timeAgo: props.resourceData.first_publication_date.date,
		timezone: 'Australia/Sydney',
	};

	let imageStyles = {};
	if (props.resourceData.feature_image) {
		imageStyles = {
			backgroundImage: `url(${props.resourceData.feature_image.url})`,
		};
	}

	if (props.disabled) {
		classNames.push('general_tile--disabled');
	}

	return (
		<div className={classNames.join(' ')}>
			{props.gtm ? (
				<ButtonBaseLink
					onClick={() => {
						props.gtm(props.resourceData.title);
					}}
					className="general_tile-button"
					hrefAs={`${props.resourceLinkPrexfix}/${props.resourceData.slug}`}
					href={`${props.resourceLinkPrexfix}-single?id=${props.resourceData.slug}`}
					title={props.resourceData.title}
				/>
			) : (
				<ButtonBaseLink
					className="general_tile-button"
					hrefAs={`${props.resourceLinkPrexfix}/${props.resourceData.slug}`}
					href={`${props.resourceLinkPrexfix}-single?id=${props.resourceData.slug}`}
					title={props.resourceData.title}
				/>
			)}
			<div className={leftAreaClassName.join(' ')} style={imageStyles} />
			<div className="general_tile-content theme--main">
				{props.resourceData.topic && (
					<div className="general_tile-topic hm-b8">
						<ButtonBaseLink
							className="general_tile-topic_link"
							href={`/resources?topic=${props.resourceData.topic.slug}`}
							title={props.resourceData.topic.title}
							disableRipple={true}
						>
							{props.resourceData.topic.title}
						</ButtonBaseLink>
					</div>
				)}

				<div className="general_tile-title">
					<TileTitle
						content={props.resourceData.title}
						clamp={false}
					/>
				</div>

				<span className="general_tile-message">
					<Dotdotdot clamp={1}>
						<span className="general_tile-text">{excerpt}</span>
					</Dotdotdot>
				</span>

				<div className="general_tile-author">
					<TileAuthor resourceData={resourceTileAuthor} />
				</div>
			</div>
		</div>
	);
};

export default ResourceTile;
