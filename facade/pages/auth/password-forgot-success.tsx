import React from 'react';
import Head from 'next/head';
import Link from 'next/link';

const PasswordForgotSuccess: React.FC<{}> = () => {
	return (
		<div className="auth_wrapper theme--accent_dark">
			<Head>
			<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
			</Head>
			<section className="auth">
				<header className="auth-header">
					<h1 className="auth-title font--h4">
						Success
					</h1>
				</header>
				<div className="auth-content theme--main">
					<div className="auth-content-text">
						<div className="font--h5">
							We've sent you an email to reset your password.
						</div>
					</div>
				</div>
				<footer className="auth-footer font--small">
					Back to <Link href="/"><a>{ process.env.APP_NAME }</a></Link>
				</footer>
			</section>
		</div>
	);
}

export default PasswordForgotSuccess;
