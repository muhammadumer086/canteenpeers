import { IStoreState } from '../store';
import { actionTypes } from '../actions';

import { ApiService } from '../../services';

import { apiError } from './apiError';

export const situationsLoad = (apiService: ApiService, token: string|null) => (dispatch, getState: () => IStoreState) => {
	if (!getState().situations.loading) {
		dispatch({
			type: actionTypes.SITUATIONS_LOAD,
		});

		apiService.queryAuthGET('/api/situations', token, 'situations')
		.then((data) => {
			dispatch({
				type: actionTypes.SITUATIONS_LOADED,
				situations: data,
			});
		})
		.catch((error) => {
			dispatch({
				type: actionTypes.SITUATIONS_LOAD_ERROR,
			});
			dispatch(apiError([
				error.message
			]));
		});
	}
}
