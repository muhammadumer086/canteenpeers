import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import { IStoreState, apiError, apiSuccess } from '../../redux/store';
import { IApiUser } from '../../interfaces';
import { EAccountScreens, ETabs } from './UserPanelAccount';
import { validationsUser } from '../../helpers/validations';
import { TextFieldFormik } from '../inputs';
import { ApiService } from '../../services';

interface IProps {
	userData: IApiUser | null;
	submitting: boolean;
	isAdminEdit: boolean;
	apiService: ApiService;
	changeScreen(tab: ETabs, screen: EAccountScreens);
	onSubmit(values: any, resetForm?: () => void);
	dispatch(action: any): void;
}

interface IState {
	resetting: boolean;
}

class UserAccountPassword extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			resetting: false,
		};

		this.resetPassword = this.resetPassword.bind(this);
	}

	public render() {
		const validationFormat = Yup.object().shape({
			password: validationsUser.password,
			new_password: validationsUser.password,
			new_password_confirmation: Yup.string()
				.oneOf(
					[Yup.ref('new_password'), null],
					'Please match your new password',
				)
				.required('Please confirm your new password'),
		});

		return (
			<div className="user_account_panel user_account_panel--account_password">
				<div className="user_panel-back">
					<ButtonBase
						focusRipple={true}
						className="user_panel-back-button"
						onClick={() => {
							this.props.changeScreen(
								ETabs.MY_PROFILE,
								EAccountScreens.OVERVIEW,
							);
						}}
						disabled={this.props.submitting}
					>
						<Icon className="user_panel-back-back_icon">
							arrow_back
						</Icon>
						Profile Overview
					</ButtonBase>
				</div>
				<div className="user_panel-heading">
					<h4 className="font--h4 theme-title">Password</h4>
				</div>
				{this.props.isAdminEdit ? (
					<div className="form">
						<div className="form-row">
							<div className="form-column">
								<Button
									variant="contained"
									color="primary"
									onClick={this.resetPassword}
									disabled={this.state.resetting}
								>
									Reset Password
								</Button>
							</div>
						</div>
					</div>
				) : (
					<Formik
						initialValues={{
							password: '',
							new_password: '',
							new_password_confirmation: '',
						}}
						validationSchema={validationFormat}
						onSubmit={values => {
							this.props.onSubmit(values);
						}}
						render={({
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							handleSubmit,
							setFieldValue,
							setFieldTouched,
							isValid,
						}) => {
							const inputProps = {
								values,
								touched,
								errors,
								handleChange,
								handleBlur,
								setFieldValue,
								setFieldTouched,
							};

							return (
								<form className="form" onSubmit={handleSubmit}>
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="password"
												label="Current Password"
												name="password"
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="password"
												label="New Password"
												name="new_password"
												autoComplete="new-password"
												disabled={this.props.submitting}
												helperText="Password must be at least 8 characters, must contain a number and a special character (ex: @,!,#)"
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="password"
												label="Confirm New Password"
												name="new_password_confirmation"
												autoComplete="new-password-confirm"
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row form-row--submit_left">
										<div className="form-column">
											<Button
												variant="outlined"
												color="primary"
												className="form-submit"
												type="submit"
												disabled={
													!isValid ||
													!touched ||
													this.props.submitting
												}
											>
												Update
											</Button>
										</div>
									</div>
								</form>
							);
						}}
					/>
				)}
			</div>
		);
	}

	protected resetPassword() {
		this.setState(
			{
				resetting: true,
			},
			() => {
				const values = {
					email: this.props.userData.email,
				};

				this.props.apiService
					.queryPOST('/api/users/password-forgot', values)
					.then(() => {
						this.setState(
							{
								resetting: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										`Password successfully reset for ${
											this.props.userData.full_name
										}`,
									]),
								);
								this.props.changeScreen(
									ETabs.MY_PROFILE,
									EAccountScreens.OVERVIEW,
								);
							},
						);
					})
					.catch(error => {
						this.setState(
							{
								resetting: false,
							},
							() => {
								// dispatch the error message
								this.props.dispatch(apiError([error.message]));
							},
						);
					});
			},
		);
	}
}
function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(UserAccountPassword);
