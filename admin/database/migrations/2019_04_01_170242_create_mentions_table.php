<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMentionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('mentions', function (Blueprint $table) {
			$table->increments('id');
			$table->string('mentioned_username');
			$table->unsignedBigInteger('mentioned_user_id');
			$table->unsignedBigInteger('sender_user_id');
			$table->unsignedBigInteger('discussion_id');
			$table->timestamps();
		});

		Schema::create('mentionables', function (Blueprint $table) {
			$table->unsignedBigInteger('mention_id');
			$table->unsignedBigInteger('mentionable_id');
			$table->string('mentionable_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('mentions');
		Schema::dropIfExists('mentionables');
	}
}
