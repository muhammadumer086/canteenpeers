<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactUsNotification extends Notification {
	use Queueable;

	protected $type;
	protected $message;
	protected $email;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($type, $message, $email) {
		$this->type = $type;
		$this->message = $message;
		$this->email = $email;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = 'Contact form submission';

		return (new MailMessage())
			->subject($title)
			->view('emails.user-contact-us', [
				'type' => $this->type,
				'content' => $this->message,
				'email' => $this->email,
			]);
	}
}
