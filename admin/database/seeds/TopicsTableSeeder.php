<?php

use Illuminate\Database\Seeder;
use App\Topic;

use Carbon\Carbon;

class TopicsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$discussionTopics = [
			'relationships' => 'Relationships',
			'grief-bereavement' => 'Grief & Bereavement',
			'friends' => 'Friends',
			'wellbeing-and-self-care' => 'Wellbeing and Self Care',
			'study-and-work' => 'Study, and Work',
			'family' => 'Family',
			'practical-tips' => 'Practical Tips',
			'random-chat' => 'Random Chat',
			'rare-cancers' => 'Rare Cancers',
			'my-diagnosis' => 'My Diagnosis',
			'my-treatment' => 'My Treatment',
			'life-after-treatment' => 'Life after Treatment',
			'fertility' => 'Fertility',
			'cancer-stuff' => 'Cancer Stuff',
			'my-parents-cancer' => "My Parent's Cancer",
			'my-siblings-cancer' => "My Sibling's Cancer",
			'my-cancer' => 'My Cancer',
			'canteen-research' => 'Canteen Research',
			'lgbtqi' => 'LGBTQI+',
			'aotearoa-new-zealand' => 'Aotearoa New Zealand',
			'my-story' => 'My Story',
			'canteen-news' => 'Canteen News',
		];

		$blogTopics = [
			'lgbtqi' => 'LGBTQI+',
			'aotearoa-new-zealand' => 'Aotearoa New Zealand',
			'grief-bereavement' => 'Grief & Bereavement',
			'friends' => 'Friends',
			'wellbeing-and-self-care' => 'Wellbeing and Self Care',
			'study-and-work' => 'Study, and Work',
			'family' => 'Family',
			'practical-tips' => 'Practical Tips',
			'rare-cancers' => 'Rare Cancers',
			'my-diagnosis' => 'My Diagnosis',
			'my-treatment' => 'My Treatment',
			'life-after-treatment' => 'Life after Treatment',
			'fertility' => 'Fertility',
			'cancer-stuff' => 'Cancer Stuff',
			'my-parents-cancer' => "My Parent's Cancer",
			'my-siblings-cancer' => "My Sibling's Cancer",
			'my-cancer' => 'My Cancer',
			'canteen-news' => 'Canteen News',
		];

		$resourceTopics = [
			'useful-links' => 'Useful Links',
			'grief-bereavement' => 'Grief & Bereavement',
			'friends' => 'Friends',
			'wellbeing-and-self-care' => 'Wellbeing and Self Care',
			'study-and-work' => 'Study, and Work',
			'family' => 'Family',
			'practical-tips' => 'Practical Tips',
			'rare-cancers' => 'Rare Cancers',
			'my-diagnosis' => 'My Diagnosis',
			'my-treatment' => 'My Treatment',
			'life-after-treatment' => 'Life after Treatment',
			'fertility' => 'Fertility',
			'cancer-stuff' => 'Cancer Stuff',
			'canteen-news' => 'Canteen',
		];

		$now = Carbon::now();

		$migrations = [
			'discussions' => $discussionTopics,
			'blogs' => $blogTopics,
			'resources' => $resourceTopics,
		];

		foreach ($migrations as $category => $topics) {
			foreach ($topics as $slug => $title) {
				$topic = Topic::updateOrCreate(
					[
						'slug' => $slug,
						'category' => $category,
					],
					[
						'title' => $title,
					]
				);
				$topic->touch();
			}
		}

		// Delete all the topics that haven't been updated
		Topic::where('updated_at', '<', $now)->delete();
	}
}
