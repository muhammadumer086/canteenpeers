/**
 * Replace the closing tags with space and remove the opening tags
 */
export default function stripHtmlTags(content: string): string {
	return content
		.replace(/<\/("[^"]*"|'[^']*'|[^>])*(>|$)/g, ' ')
		.replace(/<("[^"]*"|'[^']*'|[^>])*(>|$)/g, ' ')
		.replace(/\s+/g, ' ');
}
