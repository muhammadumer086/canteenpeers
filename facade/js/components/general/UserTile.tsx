import React from 'react';
import { connect } from 'react-redux';

import ButtonBase from '@material-ui/core/ButtonBase';
import CheckIcon from '@material-ui/icons/CheckCircle';

import { IStoreState, userPanelDisplay } from '../../redux/store';
import { IApiUser } from '../../interfaces';

import UserAvatar from '../general/UserAvatar';
import { ETabs } from '../user-panel/UserPanelAccount';

interface IProps {
	userData: IApiUser;
	gtmDataLayer?(): void;
	dispatch(action: any): void;
	onUserUpdated?(): void;
}

const UserTile = (props: IProps) => {
	const classNames = ['user_tile'];
	if (props.userData.is_blocked === true) {
		classNames.push('user_tile--blocked');
	}
	return (
		<div className={classNames.join(' ')}>
			<ButtonBase
				focusRipple={true}
				className="user_tile-action"
				onClick={() => {
					props.gtmDataLayer&&props.gtmDataLayer();
					props.dispatch(
						userPanelDisplay(
							props.userData,
							ETabs.MY_PROFILE,
							props.onUserUpdated,
						),
					);
				}}
			/>

			<div className="user_tile-avatar_container">
				<div className="user_tile-avatar">
					<UserAvatar user={props.userData} />
				</div>
			</div>

			<div className="user_tile-info">
				<p className="user_tile-name">
					{props.userData.full_name}
					{!!props.userData.is_blocked === true && (
						<React.Fragment>&nbsp;(blocked)</React.Fragment>
					)}
				</p>
				{!!props.userData.situations.length && (
					<p className="user_tile-description">
						{props.userData.situations.map((situation, index) => (
							<span
								key={index}
								className="user_tile-description-element"
							>
								<CheckIcon className="user_tile-description-check" />
								<span>{situation.name}</span>
							</span>
						))}
					</p>
				)}
			</div>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(UserTile);
