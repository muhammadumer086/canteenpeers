<?php

namespace App\Helpers;

//use Sunra\PhpSimple\HtmlDomParser;
use KubAT\PhpSimple\HtmlDomParser;
use Log;

class RichTextFilterHelper {
	public function filter($content = '') {
		$content = iconv('UTF-8', 'UTF-8//IGNORE', $content);
		$dom = HtmlDomParser::str_get_html($content);
		// Remove empty paragraphs
		$pars = $dom->find('p');
		foreach ($pars as $par) {
			if (
				trim($par->plaintext) == '' &&
				!$par->getElementByTagName('img')
			) {
				$par->outertext = '';
			}
		}
		$divs = $dom->find('div');
		foreach ($divs as $div) {
			if (
				trim($div->plaintext) == '' &&
				!$div->getElementByTagName('img')
			) {
				$div->outertext = '';
			}
		}

		return $dom->save();
	}
}
