import React from 'react';
import Head from 'next/head';
import Router, { withRouter } from 'next/router';
import { connect } from 'react-redux';
import buildUrl from 'build-url';

 

import { IStoreState, userPanelDisplay, apiError } from '../js/redux/store';

import { IApiDiscussion, IApiUser, IApiPagination } from '../js/interfaces';

import { checkPage404 } from '../js/helpers/checkPage404';

import { ApiService, AuthService } from '../js/services';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import ListLoading from '../js/components/general/ListLoading';
import HeroHeading from '../js/components/general/HeroHeading';
import DiscussionTile from '../js/components/discussions/DiscussionTile';
import FixedBackBar from '../js/components/general/FixedBackBar';

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	user: IApiUser | null;
	discussionsData: {
		data: {
			discussions: IApiDiscussion[];
		};
		meta: IApiPagination;
	};
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	isLoadingDiscussions: boolean;
	user: IApiUser | null;
	discussions: IApiDiscussion[];
	currentPage: number;
	maxPages: number;
}

class DiscussionUser extends React.Component<IProps, IState> {
	static async getInitialProps({ req, query, apiService }) {
		const result = await checkPage404(
			req,
			query,
			'id',
			async (userId: string) => {
				const user = await (apiService as ApiService).queryGET(
					`/api/users/${userId}`,
					'user',
				);
				const discussionsData = await (apiService as ApiService).queryGET(
					`/api/users/${userId}/discussions`,
				);

				return {
					user,
					discussionsData,
				};
			},
		);
		if (!result) {
			return {
				user: null,
				discussionsData: null,
			};
		}

		return result;
	}

	constructor(props) {
		super(props);

		this.state = {
			isUpdating: false,
			isLoadingDiscussions: false,
			user: props.user,
			discussions: props.discussionsData
				? props.discussionsData.data.discussions
				: [],
			currentPage: props.discussionsData
				? props.discussionsData.meta.current_page
				: 0,
			maxPages: props.discussionsData
				? props.discussionsData.meta.last_page
				: 0,
		};

		this.loadDiscussions = this.loadDiscussions.bind(this);
		this.openUserPanel = this.openUserPanel.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.userAuthenticated !== prevProps.userAuthenticated) {
			if (!this.props.userAuthenticated) {
				Router.push('/auth/login');
			} else {
				this.updateData();
			}
		}
	}

	public render() {
		let fullName = `User`;
		if (this.state.user) {
			fullName = this.state.user.full_name;
		}

		return (
			<div className="discussion_user">
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadDiscussions}>
					<header className="hero theme--accent">
						<HeroHeading
							iconSlug="discussions"
							title={`${fullName}'s discussion activity`}
						/>
					</header>

					<FixedBackBar href="/discussions" label="All Discussions" />

					{/* The discussions have loaded */}
					<div className="discussions-tiles">
						{this.state.discussions.map(discussion => {
							let discussionData = discussion;
							let repliedTo = this.state.user.full_name;

							if (
								discussion.main_discussion_id !== null &&
								discussion.main_discussion !== null
							) {
								discussionData = discussion.main_discussion;
							}
							if (discussion.main_discussion === null) {
								repliedTo = undefined;
							}

							return (
								<div
									className="discussions-tiles-item"
									key={discussion.id}
								>
									<DiscussionTile
										discussion={discussionData}
										discussionReplyData={discussion}
										withRepliedTo={repliedTo}
										disabled={this.state.isUpdating}
									/>
								</div>
							);
						})}
					</div>
					<ListLoading active={this.state.isLoadingDiscussions} />
				</LayoutDashboard>
			</div>
		);
	}

	protected updateData() {
		// API call to get the data
		this.loadDiscussions(false);
		this.updateUserData();
	}

	protected updateUserData() {
		this.props.apiService
			.queryGET(`/api/users/${this.props.router.query.id}`, 'user')
			.then((user: IApiUser) => {
				this.setState({
					isUpdating: false,
					user,
				});
			})
			.catch(error => {
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
				this.setState({
					isUpdating: false,
				});
			});
	}

	protected loadDiscussions(append: boolean = true) {
		if (
			!this.state.isLoadingDiscussions &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			this.setState(
				{
					isLoadingDiscussions: true,
				},
				() => {
					let params: any = null;

					if (append) {
						params = {
							page: this.state.currentPage + 1,
						};
					}

					const url = buildUrl(null, {
						path: `/api/users/${
							this.props.router.query.id
						}/discussions`,
						queryParams: params,
					});

					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'discussions' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let discussions: IApiDiscussion[] =
									response.data.discussions;

								if (append) {
									discussions = JSON.parse(
										JSON.stringify(this.state.discussions),
									);
									(response.data
										.discussions as IApiDiscussion[]).forEach(
										singleDiscussion => {
											discussions.push(singleDiscussion);
										},
									);
								}

								this.setState({
									isLoadingDiscussions: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									discussions,
								});
							} else {
								this.props.dispatch(
									apiError(['Invalid data returned by API']),
								);
							}
						})
						.catch(error => {
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isLoadingDiscussions: false,
							});
						});
				},
			);
		}
	}

	protected openUserPanel() {
		this.props.dispatch(userPanelDisplay(this.state.user));
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(withRouter(DiscussionUser));
