import { actionTypes } from '../actions';
import { IApiBlog } from '../../interfaces';

export interface IStoreState {
	newBlogModalDisplay: boolean;
	blogData: IApiBlog | null;
	predefinedTopic: string | null;
}

export const newBlogModal = (
	state: IStoreState = {
		newBlogModalDisplay: false,
		blogData: null,
		predefinedTopic: null,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.NEW_BLOG_MODAL_DISPLAY:
			return Object.assign({}, state, {
				newBlogModalDisplay: true,
				blogData: action.blogData,
				predefinedTopic: action.predefinedTopic,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.NEW_BLOG_MODAL_HIDE:
			return Object.assign({}, state, {
				newBlogModalDisplay: false,
			});
		default:
			return state;
	}
};
