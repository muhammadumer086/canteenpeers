import React from 'react';
import ReactSVG from 'react-svg';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

import Button from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

import { IStoreState } from '../../redux/store';

import { IApiUser, IApiMessage } from 'js/interfaces';

import { validationMessage } from '../../helpers/validations';

import { AutocompleteUserFormik, RichTextEditor } from '../inputs';
import MessageDetail from './MessageDetail';
import UserAvatar from '../general/UserAvatar';

interface IFormValues {
	users: string[];
	message: string;
}

interface IProps {
	messagesActive: boolean;
	userData: IApiUser;
	isUpdating: boolean;
	isLoadingMessages: boolean;
	activeUsers: IApiUser[];
	activeConversation: IApiMessage[];
	clearMessageData: boolean;
	createConversation(values: IFormValues): void;
	closeConversation(): void;
}

const MessageViewNew = (props: IProps) => {
	const arrowIconUrl = `${process.env.STATIC_PATH}/icons/back-arrow.svg`;
	const classes = ['user_messages-view'];

	const imageDimensions = {
		width: 24,
		height: 24,
	};

	const validationFormat = Yup.object().shape({
		users: Yup.array().required('At least one username is required'),
		message: validationMessage.message,
	});

	if (props.messagesActive) {
		classes.push('user_messages-view--isActive');
	}

	return (
		<Formik
			initialValues={{
				users: [],
				message: '',
			}}
			validationSchema={validationFormat}
			onSubmit={props.createConversation}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				handleSubmit,
				setFieldValue,
				setFieldTouched,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
				};

				if (
					props.clearMessageData &&
					(inputProps.values.message !== '' ||
						!!inputProps.values.message)
				) {
					inputProps.setFieldValue('message', '');
				}

				return (
					<form className={classes.join(' ')} onSubmit={handleSubmit}>
						<div className="user_messages-message_information">
							{!props.isUpdating && (
								<div className="user_messages-back_to_message">
									<ButtonBase
										focusRipple={true}
										onClick={props.closeConversation}
									>
										<ReactSVG
											path={arrowIconUrl}
											svgStyle={{ ...imageDimensions }}
										/>
									</ButtonBase>
								</div>
							)}

							{!props.isUpdating && (
								<div className="user_messages-select_users">
									<AutocompleteUserFormik
										label="To:"
										name="users"
										placeholder="Please search for a user(s)"
										disabled={!props.isUpdating}
										multi={true}
										{...inputProps}
									/>
								</div>
							)}

							{props.isUpdating && (
								<div className="user_messages-loading">
									<CircularProgress />
								</div>
							)}
						</div>

						<div className="user_messages-content">
							<div
								className={`user_messages-message_detail ${
									props.isUpdating || props.isLoadingMessages
										? 'user_messages-message_detail--is-disabled'
										: ''
								}`}
							>
								<div className="user_messages-message_detail-content">
									{props.activeConversation && (
										<MessageDetail
											messageItems={
												props.activeConversation
											}
											isMultiple={true}
											isLoadingReplies={
												props.isLoadingMessages
											}
										/>
									)}
								</div>
								<div className="user_messages-reply">
									<div className="user_reply">
										<div className="user_reply-form form">
											<div className="user_reply-author">
												<div className="user_reply-author_avatar">
													<UserAvatar
														user={props.userData}
													/>
												</div>
											</div>

											<div className="user_reply-form_textarea">
												<RichTextEditor
													toolbarId="message-toolbar"
													name="message"
													value={
														inputProps.values
															.message
													}
													profile={[
														'imageUpload',
														'embedVideo',
														'insertGif',
													]}
													bounds=".user_reply-form_textarea"
													scrollingContainer=".user_reply-form_textarea"
													{...inputProps}
												/>
											</div>

											<div className="user_reply-form_submit">
												<Button
													variant="contained"
													color="primary"
													className="form-submit"
													type="submit"
													disabled={!isValid}
												>
													<ReactSVG
														path={`${
															process.env
																.STATIC_PATH
														}/icons/reply.svg`}
													/>
												</Button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				);
			}}
		/>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(MessageViewNew);
