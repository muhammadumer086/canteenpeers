import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Button } from '@material-ui/core';

import { SelectFieldFormik, TextFieldFormik } from '../inputs';
import { EUserPanelState } from './UserPanelReportUserMenu';

interface IProps {
	submitting: boolean;
	handleTabChange(tab: EUserPanelState): void;
	onSubmit(value: IUserPanelReportUserFormikValues): void;
}

export interface IUserPanelReportUserFormikValues {
	report_reason: string;
	information: string;
}

const UserPanelReportUserFormik: React.StatelessComponent<IProps> = props => {
	const validationSchema = Yup.object().shape({
		report_reason: Yup.string()
			.required('A reason is required')
			.max(191),
		information: Yup.string(),
	});

	const handleCancel = () => {
		props.handleTabChange(EUserPanelState.OVERVIEW);
	};

	return (
		<Formik
			initialValues={{
				report_reason: '',
				information: '',
			}}
			validationSchema={validationSchema}
			onSubmit={props.onSubmit}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				handleSubmit,
				setFieldValue,
				setFieldTouched,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					setFieldTouched,
				};

				const report_reason_options = [
					{
						label: 'Bullying/harassment',
						value: 'bullying-harassment',
					},
					{
						label: 'Racism',
						value: 'racism',
					}
				];

				return (
					<form
						className="form form--no_padding"
						onSubmit={handleSubmit}
						style={{ textAlign: 'left' }}
					>
						<div className="form-row">
							<div className="form-column">
								<SelectFieldFormik
									label="Reason for reporting user"
									name="report_reason"
									options={report_reason_options}
									values={inputProps.values.report_reason}
									disabled={props.submitting}
									noEmpty={true}
									{...inputProps}
								/>
							</div>
						</div>
						<div className="form-row">
							<div className="form-column">
								<TextFieldFormik
									type="text"
									label="Would you like to add anymore information"
									name="information"
									multiline={true}
									disabled={props.submitting}
									{...inputProps}
								/>
							</div>
						</div>

						<div className="form-row form-row--submit">
							<div className="form-column">
								<Button
									variant="contained"
									color="primary"
									className="form-submit"
									type="submit"
									disabled={!isValid || props.submitting}
								>
									Report User
								</Button>
							</div>
						</div>

						<div
							className="form-row"
							style={{ textAlign: 'center' }}
						>
							<div className="form-column">
								<Button
									variant="contained"
									color="secondary"
									type="button"
									disabled={props.submitting}
									onClick={handleCancel}
								>
									Cancel
								</Button>
							</div>
						</div>
					</form>
				);
			}}
		/>
	);
};

export default UserPanelReportUserFormik;
