@extends('layouts.email')

@section('plain_text')
	@component('emails.components.paragraph')
		@if ($user->first_name)
			Hi {{$user->first_name}},
		@else
			Hi,
		@endif
	@endcomponent
	@component('emails.components.paragraph')
		Community member {{$sender->full_name}} mentioned you in the following discussion {{$mainDiscussion->title}}.
	@endcomponent
@endsection

@section('content')
	@component('emails.components.paragraph')
		@if ($user->first_name)
			Hi {{$user->first_name}},
		@else
			Hi,
		@endif
	@endcomponent

	@component('emails.components.paragraph')
		Community member {{$sender->full_name}} mentioned you in the following discussion <b>{{$mainDiscussion->title}}</b>.
	@endcomponent

	@php
		$link = config('canteen.facade_url') . '/discussions/' . $mainDiscussion->slug;
	@endphp
	@component('emails.components.button', ['url' => $link])
		View Reply
	@endcomponent

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
