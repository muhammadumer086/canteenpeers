import React from 'react';
import TimeAgo from 'react-timeago';

import buildUrl from 'build-url';
import Router from 'next/router';
import { connect } from 'react-redux';

import {
	IStoreState,
	apiError,
	notificationsPanelHide,
} from '../../redux/store';

import { IApiNotification } from '../../interfaces';

import { withServices, ApiService } from '../../services';

import UserAvatar from '../general/UserAvatar';
import { ButtonBaseLink } from '../general/ButtonBaseLink';
import { generateAndFormatTimezoneDate } from '../../../js/helpers/generateTimezoneDate';

interface IProps {
	notification: IApiNotification;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	isActiveDiscussion: boolean;
}

interface IDiscussionErrorState {
	link: string;
	error: string;
}

class NotificationsTile extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			isActiveDiscussion: true,
		};
		this.redirectToNotification = this.redirectToNotification.bind(this);
		this.setNotificationAsRead = this.setNotificationAsRead.bind(this);
		this.toggleClick = this.toggleClick.bind(this);
	}

	public render() {
		const classNames = ['notification_tile'];

		if (this.props.notification.read_at === null) {
			classNames.push('notification_tile--unread');
		}

		const formatTimezoneDate = generateAndFormatTimezoneDate(
			this.props.notification.created_at.date,
		);

		const notificationType = this.props.notification.type;
		const notificationDate = <TimeAgo date={formatTimezoneDate} />;
		const linkData = this.redirectToNotification();

		return (
			this.state.isActiveDiscussion && (
				<div className={classNames.join(' ')}>
					{!!linkData && (
						<ButtonBaseLink
							href={linkData.href}
							hrefAs={linkData.hrefAs}
							className="notification_tile-action"
							onClick={this.toggleClick}
						/>
					)}

					<div className="notification_tile-wrapper">
						<div className="notification_tile-avatar_container">
							<div className="notification_tile-avatar">
								{[
									'UserMentionedNotification',
									'DiscussionReplyNotification',
									'UserMentionedNotification',
								].indexOf(notificationType) >= 0 && (
									<UserAvatar
										user={
											this.props.notification.data.author
										}
									/>
								)}
							</div>
						</div>

						<div className="notification_tile-description">
							{this.notificationDescription(
								notificationType,
								notificationDate,
							)}
						</div>
					</div>
				</div>
			)
		);
	}

	protected notificationDescription(
		notificationType: string,
		notificationDate,
	) {
		switch (notificationType) {
			case 'DiscussionReplyNotification':
				return (
					<React.Fragment>
						<p className="notification_tile-description-text">
							<strong>
								{this.props.notification.data.author.full_name}
							</strong>{' '}
							replied to your discussion.
						</p>
						<p className="notification_tile-description-text">
							<strong>
								{this.props.notification.data.discussion.title}
							</strong>
						</p>
						<p className="notification_tile-description-date">
							{notificationDate}
						</p>
					</React.Fragment>
				);

			case 'MessageReceivedNotification':
				return (
					<React.Fragment>
						<p className="notification_tile-description-text">
							<strong>
								{this.props.notification.data.user.full_name}
							</strong>{' '}
							sent you a message.
						</p>
						<p className="notification_tile-description-date">
							{notificationDate}
						</p>
					</React.Fragment>
				);

			case 'NewAnnouncementNotification':
				return (
					<React.Fragment>
						<p className="notification_tile-description-text">
							<strong>Canteen</strong> posted on announcement.
						</p>
						<p className="notification_tile-description-text">
							<strong>
								{
									this.props.notification.data.announcement
										.message
								}
							</strong>
						</p>
						<p className="notification_tile-description-date">
							{notificationDate}
						</p>
					</React.Fragment>
				);

			case 'NewBlogArticleNotification':
				return (
					<React.Fragment>
						<p className="notification_tile-description-text">
							<strong>
								{
									this.props.notification.data.blog.user
										.username
								}
							</strong>{' '}
							posted a new blog article.
						</p>
						<p className="notification_tile-description-text">
							<strong>
								{this.props.notification.data.blog.title}
							</strong>
						</p>
						<p className="notification_tile-description-date">
							{notificationDate}
						</p>
					</React.Fragment>
				);

			case 'UserMentionedNotification':
				return (
					<React.Fragment>
						<p className="notification_tile-description-text">
							<strong>
								{this.props.notification.data.author.full_name}
							</strong>{' '}
							mentioned you in a discussion.
						</p>
						<p className="notification_tile-description-date">
							{notificationDate}
						</p>
					</React.Fragment>
				);

			case 'DiscussionReportNotification':
				return (
					<React.Fragment>
						<p className="notification_tile-description-text">
							<strong>
								{this.props.notification.data.author.full_name}
							</strong>{' '}
							{this.props.notification.data.ageSensitive
								? 'flagged discussion as age sensitive'
								: 'reported discussion'}
						</p>
						<p className="notification_tile-description-date">
							{notificationDate}
						</p>
					</React.Fragment>
				);
		}
	}

	protected redirectToNotification(): {
		href: string;
		hrefAs: string;
	} | null {
		const notificationType = this.props.notification.type;
		switch (notificationType) {
			case 'DiscussionReplyNotification':
				let replyId: number | null = null;

				if ('reply' in this.props.notification.data) {
					replyId = this.props.notification.data.reply
						.discussion_index;
				}

				const href = buildUrl(null, {
					path: '/discussions-single',
					queryParams: {
						id: this.props.notification.data.discussion.slug,
					},
				});

				const hrefAs = buildUrl(null, {
					path: `/discussions/${this.props.notification.data.discussion.slug}`,
					queryParams: {
						replyId,
					},
				});

				return {
					href,
					hrefAs,
				};

			case 'MessageReceivedNotification':
				return {
					href: `/messages`,
					hrefAs: `/messages`,
				};
			case 'NewAnnouncementNotification':
				return {
					href: `/dashboard`,
					hrefAs: `/dashboard`,
				};
			case 'NewBlogArticleNotification':
				return {
					href: `/blogs-single?id=${this.props.notification.data.blog.slug}`,
					hrefAs: `/blogs/${this.props.notification.data.blog.slug}`,
				};
			case 'UserMentionedNotification':
				if (this.props.notification.data.discussion.main_discussion) {
					return {
						href: `/discussions-single?id=${this.props.notification.data.discussion.main_discussion.slug}`,
						hrefAs: `/discussions/${this.props.notification.data.discussion.main_discussion.slug}`,
					};
				} else if (this.props.notification.data.discussion.slug) {
					return {
						href: `/discussions-single?id=${this.props.notification.data.discussion.slug}`,
						hrefAs: `/discussions/${this.props.notification.data.discussion.slug}`,
					};
				}

			case 'WelcomeNotification':
				return {
					href: `/dashboard`,
					hrefAs: `/dashboard`,
				};
			case 'DiscussionReportNotification':
				return {
					href: '/admin?type=reports',
					hrefAs: '/admin?type=reports',
				};
		}
		return null;
	}

	protected async toggleClick(event) {
		event.preventDefault();
		const notificationType = this.props.notification.type;
		const discussionReplyLink = this.redirectToNotification().hrefAs;
		if (notificationType === 'DiscussionReplyNotification') {
			const resourceError = await this.getRemovedResourceErrorAndLink();
			if (resourceError) {
				this.setState({ isActiveDiscussion: false });
				Router.push(resourceError.link).then(_ => {
					this.props.dispatch(apiError([resourceError.error]));
				});
				this.closePanel();
				return;
			}
		}
		Router.push(discussionReplyLink);
		if (
			// set notification as 'read' only if still isn't 'read'
			!(this.props.notification.read_at !== null) &&
			!(this.props.notification.read_at instanceof Object)
		) {
			this.setNotificationAsRead();
		}
		// this.redirectToNotification()
		this.closePanel();
	}

	protected async getRemovedResourceErrorAndLink(): Promise<IDiscussionErrorState | null> {
		try {
			await this.requestDiscussion();
		} catch (e) {
			return {
				error: 'Discussion has been removed',
				link: '/discussions',
			};
		}
		try {
			await this.requestDiscussionReply();
		} catch (e) {
			const discussionLink = `/discussions/${this.props.notification.data.discussion.slug}`;
			return {
				error: 'Reply has been removed',
				link: discussionLink,
			};
		}
		return null;
	}

	protected requestDiscussion() {
		const discussionUrl = `/api/discussions/${this.props.notification.data.discussion.slug}`;
		return this.props.apiService.queryGET(discussionUrl);
	}

	protected async requestDiscussionReply() {
		const replyId = this.props.notification.data.reply.discussion_index;
		const replyUrl = `/api/discussions/${this.props.notification.data.discussion.id}/replies?index=${replyId}&total=1`;
		const { discussions } = await this.props.apiService.queryGET(replyUrl);
		if (discussions.length === 0) {
			throw new Error('Reply has been removed');
		}
	}

	protected closePanel() {
		this.props.dispatch(notificationsPanelHide());
	}

	protected setNotificationAsRead() {
		const url = `/api/users/me/notifications/${this.props.notification.id}`;
		this.props.apiService
			.queryGET(url)
			.then(() => {
				// nothing
			})
			.catch(error => {
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}

	// protected displayError(notificationType: string) {
	// 	let message = 'This discussion has been removed';

	// 	if (notificationType === 'DiscussionReplyNotification') {
	// 		message = 'This discussion has been removed';
	// 	}

	// 	this.props.dispatch(apiError([message]));
	// }
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(NotificationsTile));
