import React from 'react';
import { ModalDialog } from '../modals/local/ModalDialog';
import { IApiUser } from '../../interfaces';
// import UserAvatar from '../general/UserAvatar';
interface IProps {
	toggleModel(any): void;
	selectUser(user: IApiUser): void;
	allUsers: IApiUser[];
}
class EventPromoModal extends React.Component<IProps> {
	public render() {
		return (
			<ModalDialog
				modalTitle={'Select Your Account'}
				isActive={true}
				handleClose={this.props.toggleModel}
			>
				<div>
					{this.props.allUsers.map(user => {
						return (
							<div
								key={user.id}
								className="user_select_modal-tile"
								onClick={() => {
									this.props.selectUser(user);
								}}
							>
								<div className="user_select_modal-tile-image">
									{user.username.charAt(0)}
								</div>

								<div>{user.username}</div>
							</div>
						);
					})}
				</div>
			</ModalDialog>
		);
	}
	protected getHiddenName = (name: string) => {
		if (name.length > 3) {
			let newName = name.substr(0, name.length - 2);
			newName = newName + '**';
			return newName;
		} else {
			let newName = name.substr(0, 1);
			newName = newName + '*';
			return newName;
		}
	};
}
export default EventPromoModal;
