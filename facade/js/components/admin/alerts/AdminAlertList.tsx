import React from 'react';
import { IApiAnnouncement } from '../../../../js/interfaces';
import AdminAlertItem from './AdminAlertItem';

interface IProps {
	alerts: IApiAnnouncement[];
	toggleEditModal?(data: IApiAnnouncement): void;
	toggleDeleteModal?(data: IApiAnnouncement): void;
}

const AdminAlertList: React.StatelessComponent<IProps> = props => {
	return (
		<React.Fragment>
			{props.alerts.map((alert, index) => {
				return (
					<AdminAlertItem
						key={index}
						data={alert}
						toggleEditAlertModal={props.toggleEditModal}
						toggleDeleteAlertModal={props.toggleDeleteModal}
					/>
				);
			})}
		</React.Fragment>
	);
};

export default AdminAlertList;
