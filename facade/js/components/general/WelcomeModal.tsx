import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';

import { ApiService, withServices } from '../../services';
import {
	apiError,
	userVerifiedDisplayed,
	IStoreState,
	apiSuccess,
	userUpdated,
} from '../../redux/store';
import { IApiUser, IApiSituation, ISituationOption } from '../../interfaces';

import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import ClearIcon from '@material-ui/icons/Clear';
import WelcomeModalScreen from './WelcomeModal/WelcomeModalScreen';
import WelcomeModalCancerType from './WelcomeModal/WelcomeModalCancerType';
import WelcomeModalChat from './WelcomeModal/WelcomeModalChat';
import { Icon, ButtonBase, Button } from '@material-ui/core';
import WelcomeModalResearch from './WelcomeModal/WelcomeModalResearch';

interface IProps {
	userAuthenticated: boolean | null;
	userVerified: boolean;
	userData: IApiUser;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	modalActive: boolean;
	slides: any[] | null;
	currentIndex: number;
	translateValue: number;
	situations: ISituationOption[];
}

class WelcomeModal extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			modalActive: this.props.userVerified,
			submitting: false,
			slides: null,
			currentIndex: 0,
			translateValue: 0,
			situations: [],
		};

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.saveResultsToState = this.saveResultsToState.bind(this);
		this.renderNavigation = this.renderNavigation.bind(this);
		this.renderSlider = this.renderSlider.bind(this);
		this.getInitialValues = this.getInitialValues.bind(this);
		this.getUserSituations = this.getUserSituations.bind(this);
		this.prevSlide = this.prevSlide.bind(this);
		this.nextSlide = this.nextSlide.bind(this);
	}

	public componentDidMount() {
		this.setState({
			slides: [
				WelcomeModalScreen,
				WelcomeModalCancerType,
				WelcomeModalChat,
				WelcomeModalResearch,
			],
		});
	}

	public componentDidUpdate(oldProps: IProps) {
		if (this.props.userVerified !== oldProps.userVerified) {
			this.setState({
				modalActive: this.props.userVerified,
			});
		}
	}

	protected nextSlide() {
		this.setState({
			currentIndex: this.state.currentIndex + 1,
			translateValue: this.state.translateValue + -this.slideWidth(),
		});
	}

	protected prevSlide() {
		this.setState({
			currentIndex: this.state.currentIndex - 1,
			translateValue: this.state.translateValue + this.slideWidth(),
		});
	}

	protected slideWidth() {
		return document.querySelector('.modal_introduction-item').clientWidth;
	}

	protected saveResultsToState(situations?: any) {
		this.setState({
			situations,
		});
	}

	public render() {
		if (!this.props.userData) {
			return null;
		}
		const initialValues = this.getInitialValues();

		const validationFormat = Yup.object().shape({
			situations: Yup.array().of(
				Yup.object().shape({
					slug: Yup.string().required(),
					name: Yup.string().required(),
					cancer_type: Yup.string().nullable(true),
					personal_settings: Yup.array(Yup.string()),
				}),
			),
		});

		return (
			<Modal
				aria-labelledby="welcome-modal"
				open={this.state.modalActive && !!this.props.userData}
				onClose={this.handleClose}
			>
				<Paper
					className="modal modal--welcome theme--main"
					square={true}
				>
					<button
						onClick={this.handleClose}
						className="close_button close_button--primary"
						title="Dismiss"
					>
						<ClearIcon className="close_button-icon" />
					</button>

					<div className="modal_introduction">
						<Formik
							initialValues={initialValues}
							validationSchema={validationFormat}
							onSubmit={values => {
								const reducedValues = JSON.parse(
									JSON.stringify(values),
								);
								for (const key in reducedValues) {
									if (key.startsWith('cancer_type_')) {
										const keySlug = key.replace(
											'cancer_type_',
											'',
										);
										(reducedValues.situations as ISituationOption[]).forEach(
											singleSituation => {
												if (
													singleSituation.slug ===
													keySlug
												) {
													singleSituation.cancer_type =
														reducedValues[key];
												}
											},
										);
										delete reducedValues[key];
									}
								}
								this.handleSubmit(reducedValues);
							}}
							render={({
								values,
								touched,
								errors,
								handleChange,
								handleBlur,
								handleSubmit,
								setFieldValue,
								setFieldTouched,
							}) => {
								const inputProps = {
									values,
									touched,
									errors,
									handleChange,
									handleBlur,
									setFieldValue,
									setFieldTouched,
								};

								return (
									<React.Fragment>
										<form onSubmit={handleSubmit}>
											<div className="modal_introduction-slider_container">
												{this.renderSlider(inputProps)}
											</div>
											<div className="modal_introduction-footer">
												{this.renderNavigation()}
											</div>
										</form>
									</React.Fragment>
								);
							}}
						/>
					</div>
				</Paper>
			</Modal>
		);
	}

	protected handleClose() {
		this.props.dispatch(userVerifiedDisplayed());
	}

	public handleSubmit(values) {
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/users/${this.props.userData.username}`,
						values,
						'user',
					)
					.then(user => {
						this.setState({
							submitting: false,
						});
						this.props.dispatch(
							apiSuccess(['Your details have been updated']),
						);
						this.props.dispatch(userUpdated(user));

						this.handleClose();
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							submitting: false,
						});
					});
			},
		);
	}

	protected renderSlider(inputProps) {
		return (
			<div
				className="modal_introduction-slider"
				style={{
					transform: `translateX(${this.state.translateValue}px)`,
					transition: 'transform ease-out 0.45s',
				}}
			>
				{this.state.slides.map((Slide, index) => (
					<div
						className="modal_introduction-item_content_wrapper"
						key={index}
					>
						<Slide inputProps={inputProps} />
					</div>
				))}
			</div>
		);
	}

	protected renderNavigation() {
		const { currentIndex } = this.state;
		const backArrowClasses = `modal_introduction-previous ${currentIndex ===
			0 && 'modal_introduction-previous--hidden'}`;
		const forwardArrowClasses = `modal_introduction-next ${currentIndex ===
			this.state.slides.length - 1 && 'modal_introduction-next--hidden'}`;

		return (
			<React.Fragment>
				<div className="modal_introduction-navigation">
					<ButtonBase
						onClick={this.prevSlide}
						disabled={this.state.currentIndex === 0}
					>
						<Icon className={backArrowClasses}>
							arrow_right_alt
						</Icon>
					</ButtonBase>
					{this.state.slides !== null &&
						this.state.slides.length &&
						this.state.slides.map((_dot, index) => {
							return (
								<Icon
									key={index}
									className={`modal_introduction-current_slide ${
										this.state.currentIndex === index
											? 'modal_introduction-current_slide--active'
											: ''
									}`}
								>
									lens
								</Icon>
							);
						})}
					<ButtonBase
						onClick={this.nextSlide}
						disabled={
							this.state.currentIndex ===
							this.state.slides.length - 1
						}
					>
						<Icon className={forwardArrowClasses}>
							arrow_right_alt
						</Icon>
					</ButtonBase>
				</div>

				{this.state.currentIndex === this.state.slides.length - 1 && (
					<div className="modal_introduction-started">
						<Button
							color="primary"
							variant="outlined"
							type="submit"
						>
							Get Started
						</Button>
					</div>
				)}
			</React.Fragment>
		);
	}

	protected getInitialValues() {
		return {
			situations: this.getUserSituations(this.props.userData.situations),
			...this.props.userData.situations.reduce(
				(accum, current) =>
					Object.assign(accum, {
						[`cancer_type_${
							current.main_slug
						}`]: current.cancer_type,
						[`date_cancer_type_${current.main_slug}`]: current.date,
					}),
				{},
			),
			personal_settings: this.props.userData.personal_settings.map(
				setting => setting.slug,
			),
		};
	}

	protected getUserSituations(
		situations: IApiSituation[],
	): ISituationOption[] {
		const result = [];
		situations.forEach((singleSituation: IApiSituation) => {
			if (result.indexOf(singleSituation.main_slug) === -1) {
				result.push({
					slug: singleSituation.main_slug,
					name: singleSituation.name,
					cancer_type: singleSituation.cancer_type,
				});
			}
		});

		return result;
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData, userVerified } = state.user;

	return {
		userAuthenticated,
		userVerified,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(WelcomeModal));
