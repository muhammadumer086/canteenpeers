import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import { IStoreState } from '../../redux/store';

import { TextFieldFormik } from '../inputs';

import { validationMessage } from '../../helpers/validations';
import { IApiUser } from '../../interfaces';

interface IProps {
	userData: IApiUser | null;
	submitting: boolean;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	email: string;
	message: string;
}

export const ReportForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		email: validationMessage.email,
		message: validationMessage.message,
	});

	return (
		<div>
			<Formik
				initialValues={{
					email:
						props.userData && 'email' in props.userData
							? props.userData.email
							: '',
					message: '',
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form
							className="auth_form form"
							onSubmit={handleSubmit}
						>
							{(!props.userData ||
								!('email' in props.userData)) && (
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="email"
											label="Email"
											name="email"
											disabled={props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
							)}

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Describe the problem in detail"
										name="message"
										disabled={props.submitting}
										multiline={true}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || props.submitting}
									>
										Report
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;

	return {
		userData,
	};
}

export default connect(mapStateToProps)(ReportForm);
