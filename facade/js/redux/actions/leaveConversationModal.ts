import { IStoreState } from '../store';
import { actionTypes } from '../actions';

export const leaveConversationModalDisplay = (conversationId: number) => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (getState().leaveConversationModal.leaveConversationModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.LEAVE_CONVERSATION_DISPLAY,
		conversationId,
	});
};

export const leaveConversationModalHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().leaveConversationModal.leaveConversationModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.LEAVE_CONVERSATION_HIDE,
	});
};
