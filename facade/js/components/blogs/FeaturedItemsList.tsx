import React from 'react';
import FeaturedItem from './FeaturedItem';
import { IApiUser, IApiBlog, IApiResource } from 'js/interfaces';

interface IProps {
	featuredBlogs?: IApiBlog[];
	featuredResources?: IApiResource[];
	disabled: boolean;
}

interface IFeaturedItemsBlogs {
	featuredBlogs: IApiBlog[];
}

interface IFeaturedItemsResources {
	featuredresources: IApiResource[];
}

const FeaturedItemsBlogs: React.StatelessComponent<IFeaturedItemsBlogs> = props => {
	return (
		<React.Fragment>
			{props.featuredBlogs.map((data, index) => {
				let blogAuthor: IApiUser = data.user;

				if (data && 'user' in data && data.user !== null) {
					blogAuthor = {
						full_name: data.user.full_name,
						username: data.user.username,
						avatar_url: data.user.avatar_url,
						is_banned: false,
						updated_at: data.user.updated_at,
					};

					return (
						<FeaturedItem
							key={index}
							blogData={data}
							blogAuthorDetails={data.user}
						/>
					);
				} else {
					blogAuthor = {
						full_name: data.prismic_author,
						username: data.prismic_author,
						avatar_url: data.prismic_author_image,
						is_banned: false,
						updated_at: null,
					};

					return (
						<FeaturedItem
							key={index}
							blogData={data}
							blogAuthorDetails={blogAuthor}
						/>
					);
				}
			})}
		</React.Fragment>
	);
};

const FeaturedItemsResources: React.StatelessComponent<IFeaturedItemsResources> = props => {
	return (
		<React.Fragment>
			{props.featuredresources.map((data, index) => {
				const resourceTileAuthor = {
					authorAvatarUrl: `${process.env.STATIC_PATH}/icons/canteen-icon.svg`,
					authorUsername: 'Canteen',
					timeAgo: data.first_publication_date.date,
					timezone: 'Australia/Sydney',
				};

				return (
					<FeaturedItem
						key={index}
						resourceData={data}
						resourceAuthorDetails={resourceTileAuthor}
					/>
				);
			})}
		</React.Fragment>
	);
};

const FeaturedItemsList: React.StatelessComponent<IProps> = props => {
	const classNames = ['featured_items-container'];

	if (props.disabled) {
		classNames.push('featured_items-container--disabled');
	}

	return (
		<React.Fragment>
			<div className={classNames.join(' ')}>
				{!!props.featuredBlogs && (
					<FeaturedItemsBlogs featuredBlogs={props.featuredBlogs} />
				)}

				{!!props.featuredResources && (
					<FeaturedItemsResources
						featuredresources={props.featuredResources}
					/>
				)}
			</div>
		</React.Fragment>
	);
};

export default FeaturedItemsList;
