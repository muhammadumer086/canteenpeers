import Quill from 'quill';

const Embed = Quill.import('blots/embed');

export default class QuillMentionBlot extends Embed {
	static create(data) {
		let node = super.create();
		node.innerHTML = `<span>@${data.username}</span>`;
		return node;
	}
}

QuillMentionBlot.className = 'mention-blot';
QuillMentionBlot.tagName = 'span';
QuillMentionBlot.blotName = 'mention';

Quill.register(
	{
		'formats/mention': QuillMentionBlot,
	},
	false,
);
