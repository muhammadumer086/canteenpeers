<?php

use Illuminate\Database\Seeder;

use App\PersonalSetting;
use Carbon\Carbon;

class PersonalSettingsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$settings = [
			'Permissions' => [
				PersonalSetting::$BLOG_PUBLIC =>
					'Allow non community members to read my blog posts',
				PersonalSetting::$ALLOW_DISPLAY_CANCER_TYPE =>
					'Show the cancer type/s I\'m impacted by in my profile',
				PersonalSetting::$ALLOW_DIRECT_MESSAGE =>
					'Allow community members to direct message me',
				PersonalSetting::$ALLOW_DISPLAY_GENDER =>
					'Show my gender in my profile',
			],
			'Research' => [
				PersonalSetting::$ALLOW_CONTACTED_RESEARCH =>
					'I am willing to be contacted about new research opportunities',
			],
			'Newsletter' => [
				PersonalSetting::$ALLOW_NEWSLETTER =>
					'Receive Canteen news to my email',
			],
			'@ mentions' => [
				PersonalSetting::$ALLOW_NOTIF_DISCUSSION_MENTION =>
					'Display in notification panel',
				PersonalSetting::$ALLOW_PUSH_NOTIF_DISCUSSION_MENTION => [
					'name' => 'Notify me on my device',
					'type' => 'native-app',
				],
				PersonalSetting::$ALLOW_EMAIL_DISCUSSION_MENTION =>
					'Email me when I receive a mention',
			],
			'Discussion Replies' => [
				PersonalSetting::$ALLOW_NOTIF_DISCUSSION_REPLIES =>
					'Display in notification panel',
				PersonalSetting::$ALLOW_PUSH_NOTIF_DISCUSSION_REPLIES => [
					'name' => 'Notify me on my device',
					'type' => 'native-app',
				],
				PersonalSetting::$ALLOW_EMAIL_DISCUSSION_REPLIES =>
					'Email me when someone replies to my post',
			],
			'Followed Discussions' => [
				PersonalSetting::$ALLOW_NOTIF_DISCUSSION_FOLLOWED =>
					'Display in notification panel',
				PersonalSetting::$ALLOW_PUSH_NOTIF_DISCUSSION_FOLLOWED => [
					'name' => 'Notify me on my device',
					'type' => 'native-app',
				],
				PersonalSetting::$ALLOW_EMAIL_DISCUSSION_FOLLOWED =>
					'Email me when my ‘Followed Discussions’ have new replies',
			],
			'Messages' => [
				PersonalSetting::$ALLOW_EMAIL_MESSAGES =>
					'Email me when someone messages me',
				PersonalSetting::$ALLOW_PUSH_NOTIF_MESSAGES => [
					'name' => 'Notify me on my device when someone messages me',
					'type' => 'native-app',
				],
			],
			'Canteen Alerts' => [
				PersonalSetting::$ALLOW_NOTIF_ALERTS =>
					'Display in notification panel',
				PersonalSetting::$ALLOW_PUSH_NOTIF_ALERTS => [
					'name' => 'Notify me on my device',
					'type' => 'native-app',
				],
				PersonalSetting::$ALLOW_EMAIL_ALERTS =>
					'Email me Canteen activity',
			],
			'New Blogs' => [
				PersonalSetting::$ALLOW_NOTIF_BLOGS =>
					'Notify me when a new blog is published',
			],
		];

		$categoryIndex = 0;
		$preUpdateTimestamp = Carbon::now();

		foreach ($settings as $categoryName => $settingCategory) {
			if (is_array($settingCategory) && sizeof($settingCategory)) {
				$categorySlug = str_slug($categoryName, '-');
				$settingIndex = 0;
				foreach ($settingCategory as $slug => $singleSetting) {
					$settingName = $singleSetting;
					$settingType = null;
					if (is_array($singleSetting)) {
						$settingName = $singleSetting['name'];
						$settingType = $singleSetting['type'];
					}
					$setting = PersonalSetting::updateOrCreate(
						[
							'slug' => $slug,
						],
						[
							'name' => $settingName,
							'category' => $categoryName,
							'category_slug' => $categorySlug,
							'order' => $categoryIndex * 100 + $settingIndex,
							'type' => $settingType,
						]
					);
					$setting->touch();
					++$settingIndex;
				}
			}
			++$categoryIndex;
		}

		// Delete past settings
		PersonalSetting::where(
			'updated_at',
			'<',
			$preUpdateTimestamp
		)->delete();
	}
}
