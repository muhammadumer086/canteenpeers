<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDataColumnSizeNotification extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('notifications', function (Blueprint $table) {
			$table
				->mediumText('data')
				->comment('increased size')
				->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('notifications', function (Blueprint $table) {
			$table
				->mediumText('data')
				->comment('')
				->change();
		});
	}
}
