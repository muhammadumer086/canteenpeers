<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\User;
use App\UserActivity;

class UpdateUserVerification extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:userVerifications';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate the user settings (preferences and roles)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		\App\User::disableSearchSyncing();
		$usersQuery = null;

		$usersQuery = User::where('verified', true)->withTrashed();

		$this->line('');
		$this->info('Updating user verifications');
		$bar = $this->output->createProgressBar($usersQuery->count());
		$bar->start();
		$bar->setRedrawFrequency(10);

		$usersQuery->chunk(10, function ($users) use ($bar) {
			foreach ($users as $user) {
				// List the 2 first activities of the user
				$activity1 = UserActivity::where('user_id', $user->id)
					->orderBy('created_at', 'asc')
					->first();
				$activity2 = UserActivity::where('user_id', $user->id)
					->orderBy('created_at', 'asc')
					->offset(1)
					->first();
				$activity3 = UserActivity::where('user_id', $user->id)
					->orderBy('created_at', 'asc')
					->offset(2)
					->first();
				$isActivated = UserActivity::where('user_id', $user->id)
					->whereIn('type', [
						'User Verified',
						'Migrated User Activation',
						'User Verified With Password Reset',
					])
					->orderBy('created_at', 'asc')
					->first();

				if (
					!$isActivated &&
					$activity1 &&
					$activity2 &&
					$activity1->type === 'User Created' &&
					($activity2->type === 'User Password Reset' ||
						($activity2->type !==
							'User Verified With Password Reset' &&
							$activity3 &&
							$activity3->type === 'User Password Reset'))
				) {
					$activityToCopy = $activity2;
					if (
						$activity3 &&
						$activity3->type === 'User Password Reset'
					) {
						$activityToCopy = $activity3;
					}
					// Create a new activity for the user
					$userActivation = $activityToCopy->replicate();
					$userActivation->type = 'User Verified With Password Reset';
					$userActivation->created_at = $activityToCopy->created_at->subSecond();
					$userActivation->updated_at = $activityToCopy->created_at->subSecond();
					$userActivation->save();
				}

				$bar->advance();
			}
		});

		$bar->finish();
		$this->info("\r\nTrack remaining users");

		// Track the remaining users that don't have an activation
		$remainingActivations = User::whereNotIn(
			'id',
			UserActivity::whereIn('type', [
				'User Verified',
				'Migrated User Activation',
				'User Verified With Password Reset',
			])->pluck('user_id')
		)
			->where('verified', true)
			->withTrashed();

		$this->info("\r\nRemaining users: " . $remainingActivations->count());

		$bar = $this->output->createProgressBar($usersQuery->count());
		$bar->start();
		$bar->setRedrawFrequency(10);

		$remainingActivations->chunk(10, function ($users) use ($bar) {
			foreach ($users as $user) {
				$activityToCopy = UserActivity::where('user_id', $user->id)
					->where('type', 'User Password Reset')
					->orderBy('created_at', 'asc')
					->first();
				if ($activityToCopy) {
					$userActivation = $activityToCopy->replicate();
					$userActivation->type = 'User Verified With Password Reset';
					$userActivation->created_at = $activityToCopy->created_at->subSecond();
					$userActivation->updated_at = $activityToCopy->created_at->subSecond();
					$userActivation->save();
				}

				$bar->advance();
			}
		});

		$bar->finish();

		$this->info("\r\nFinished updating user verifications");
		$this->line('');
	}
}
