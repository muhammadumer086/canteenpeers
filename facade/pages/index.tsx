import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import Router from 'next/router';
import buildUrl from 'build-url';

import { ApiService, AuthService } from '../js/services';

import Image from '../js/components/general/Image';
import LayoutLanding from '../js/components/general/LayoutLanding';
import LandingBoxTile from '../js/components/landing/LandingBoxTiles';
import LandingConversation from '../js/components/landing/LandingConversation';
import { ButtonLink } from '../js/components/general/ButtonLink';

import { IStoreState } from '../js/redux/store';
import { IApiUser } from 'js/interfaces';
import { getCountry } from '../js/helpers/getCountry';
import CircularProgress from '@material-ui/core/CircularProgress';

interface IProps {
	userAuthenticated: boolean | null;
	userData: IApiUser;
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	ctaConfig: {
		ctaText: string;
		buttonLabel: string;
		buttonAction(): void;
	};
	isMobile: boolean;
	isChecked: boolean;
}

class Index extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			ctaConfig: {
				ctaText: 'Join the community to connect with people like you.',
				buttonLabel: 'Get Support',
				buttonAction: this.handleRegister,
			},
			isMobile: this.isMobile(),
			isChecked: false,
		};

		this.handleRegister = this.handleRegister.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
		this.handleDashboard = this.handleDashboard.bind(this);
		this.handleScrollEvent = this.handleScrollEvent.bind(this);
		this.checkAndRedirect = this.checkAndRedirect.bind(this);
	}

	public componentDidMount() {
		if (typeof window !== 'undefined') {
			this.checkAndRedirect();
			setTimeout(() => {
				this.handleScrollEvent();
			}, 1500);

			window.addEventListener('scroll', this.handleScrollEvent);
			window.addEventListener('resize', this.handleScrollEvent);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.userAuthenticated !== prevProps.userAuthenticated) {
			this.updateData();
		}
	}

	public render() {
		// const pageTitle =
		// 	'Canteen Connect - Young Adult & Teens Cancer Support Community';
		// const seoDescription =
		// 	'Canteen is a community for young adult & teens in Australia whose lives have been affected by cancer. Register to connect with other teens &amp; get support.';
		const seoFeaturedImage = `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;

		const url = buildUrl(process.env.APP_URL, {
			path: `/`,
		});
		const { userAuthenticated, userData } = this.props;
		let currentCountry: string = getCountry(userAuthenticated, userData);
		const { isChecked } = this.state;
		if (!isChecked) {
			return (
				<div
					style={{
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						height: '100vh',
						width: '100%',
					}}
				>
					<CircularProgress />
				</div>
			);
		}
		return (
			<div>
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
					<meta name="og:url" property="og:url" content={url} />
					<meta name="og:type" property="og:type" content="website" />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>
				</Head>
				<LayoutLanding
					ctaConfig={this.state.ctaConfig}
					country={currentCountry}
				>
					<div className="landing_page-content landing_page-content--introduction">
						<div className="landing_page-container animate-on-load">
							<p>
								{currentCountry === 'AU'
									? `Canteen Connect is a community for young people
								dealing with their own or a close family
								member’s cancer.`
									: `Canteen Connect is a community for rangatahi dealing with
								 cancer in their whānau or their own diagnosis.`}
							</p>
							<div className="landing_page-store-container">
								<a
									target="_blank"
									href="https://apps.apple.com/us/app/id1484899856?utm_medium=referral&utm_source=website&utm_campaign=connect+app&utm_content=ios"
								>
									<img
										src={`${process.env.STATIC_PATH}/images/applestore.png`}
									></img>
								</a>
								<a
									href="https://play.google.com/store/apps/details?id=canteen.connect.android.app&utm_source=website&utm_campaign=connect%2Bapp&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"
									target="_blank"
								>
									<img
										src={`${process.env.STATIC_PATH}/images/GooglePlayStore.png`}
									></img>
								</a>
							</div>
							<Image
								src={`${process.env.STATIC_PATH}/images/landing-page/landing-intro-hand.png`}
								webp={`${process.env.STATIC_PATH}/images/landing-page/landing-intro-hand.webp`}
								className="landing_page-supporting_image"
								alt={process.env.APP_NAME}
							/>
						</div>
					</div>

					<div className="landing_page-content landing_page-content--messages">
						<div className="landing_page-container">
							<div className="landing_page-row">
								<div className="landing_page-content_container">
									<LandingBoxTile>
										<h3>Personalised to you</h3>
										<p className="font--bold">
											Find discussions and people who have
											a similar cancer experience to you.
										</p>
										{/* <ButtonLink
											variant="outlined"
											color="primary"
											href="/discussions"
											className="button_take-a-look"
										>
											Take a look
										</ButtonLink> */}
									</LandingBoxTile>

									<div className="landing_page-image_container">
										<Image
											src={`${process.env.STATIC_PATH}/images/landing-page/landing-connect-iphone-one.jpg`}
											webp={`${process.env.STATIC_PATH}/images/landing-page/landing-connect-iphone-one.webp`}
											className="landing_page-supporting_image"
											alt={process.env.APP_NAME}
										/>

										<Image
											src={`${process.env.STATIC_PATH}/images/landing-page/landing-connect-iphone-two-updated.jpg`}
											webp={`${process.env.STATIC_PATH}/images/landing-page/landing-connect-iphone-two-updated.webp`}
											className="landing_page-supporting_image"
											alt={process.env.APP_NAME}
										/>
									</div>
								</div>

								<div className="landing_page-image_container">
									<div className="landing_page-conversation">
										<LandingConversation />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div className="landing_page-content landing_page-content--events">
						<div
							style={{
								backgroundImage: `url(${process.env.STATIC_PATH}/images/landing-page/landing-background.jpg)`,
							}}
							className="landing_page-full_container landing_page-full_container--image"
						>
							<Image
								src={`${process.env.STATIC_PATH}/images/landing-page/landing-events-iphone.png`}
								webp={`${process.env.STATIC_PATH}/images/landing-page/landing-events-iphone.webp`}
								className="landing_page-supporting_image animate-when-visible"
								alt={process.env.APP_NAME}
							/>
						</div>
						<div className="landing_page-full_container landing_page-full_container--content">
							<div className="landing_page-content_container">
								<LandingBoxTile>
									<h3>Check out our events</h3>
									<p className="font--bold">
										Canteen events give you time out from
										the daily pressures of living with
										cancer. You can meet other young people
										who really understand what you’re going
										through and learn new ways to cope with
										cancer.
									</p>

									<ButtonLink
										variant="outlined"
										color="primary"
										className="landing_hero-cta landing_hero-cta--outlined"
										href="/events"
									>
										Find events near you
									</ButtonLink>
								</LandingBoxTile>
							</div>
						</div>
					</div>

					<div className="landing_page-content landing_page-content--counselling">
						<div className="landing_page-container">
							<div className="landing_page-row">
								<div className="landing_page-content_container">
									<LandingBoxTile>
										<h3>
											Extra support whenever you need it
										</h3>
										<p className="font--bold">
											Canteen counselling is confidential
											and completely free. Our counsellors
											give you a safe space to talk about
											whatever is on your mind – and just
											listen.
										</p>

										<br />

										<ButtonLink
											variant="outlined"
											color="primary"
											href="/auth/register?step=step-1"
											hrefAs="/auth/register/step-1"
										>
											Get Support
										</ButtonLink>
									</LandingBoxTile>
								</div>

								<div className="landing_page-image_container">
									<Image
										src={`${process.env.STATIC_PATH}/images/landing-page/landing-counselling-iphone-updated.png`}
										webp={`${process.env.STATIC_PATH}/images/landing-page/landing-counselling-iphone-updated.webp`}
										className="landing_page-supporting_image"
										alt={process.env.APP_NAME}
									/>
								</div>
							</div>
						</div>
					</div>
				</LayoutLanding>
			</div>
		);
	}

	protected handleRegister() {
		Router.push('/auth/register?step=step-1', '/auth/register/step-1');
	}

	protected handleLogin() {
		Router.push('/auth/login');
	}

	protected handleDashboard() {
		Router.push('/dashboard');
	}

	protected updateData() {}

	protected isMobile() {
		if (
			typeof window !== 'undefined' &&
			typeof window.orientation !== 'undefined' &&
			window.innerWidth <= 600
		) {
			return true;
		}

		return false;
	}

	protected isVisibleInWindow(element, offset = 250) {
		let bounding = element.getBoundingClientRect();

		return (
			typeof window !== 'undefined' &&
			bounding.top >= 0 &&
			bounding.left >= 0 &&
			bounding.bottom <=
				(window.innerHeight || document.documentElement.clientHeight) +
					offset &&
			bounding.right <=
				(window.innerWidth || document.documentElement.clientWidth)
		);
	}
	protected checkAndRedirect = () => {
		if (this.props.userAuthenticated) {
			Router.replace('/dashboard');
		} else {
			this.setState({ isChecked: true });
		}
	};

	protected handleScrollEvent() {
		const elementsToFaceIn = [].slice.call(
			document.querySelectorAll('.animate-when-visible'),
		);
		const elementsToFadeInLoop = [].slice.call(
			document.querySelectorAll('.animate-when-visible-loop'),
		);
		let elementResourcesAnimate = [].slice.call(
			document.querySelectorAll('.resource-animate'),
		);
		const resourcesTrigger = [].slice.call(
			document.querySelectorAll('.animation-trigger'),
		);
		const animateOnLoad = [].slice.call(
			document.querySelectorAll('.animate-on-load'),
		);

		// Reorder resource tile because HTML structure is not correct for the animations
		let tmp = elementResourcesAnimate[0];
		elementResourcesAnimate[0] = elementResourcesAnimate[1];
		elementResourcesAnimate[1] = tmp;

		resourcesTrigger.forEach(el => {
			const visible = this.isVisibleInWindow(el, 0);

			if (visible && !this.isMobile()) {
				for (
					let childIndex = 0;
					childIndex < elementResourcesAnimate.length;
					childIndex++
				) {
					const element = elementResourcesAnimate[childIndex];

					setTimeout(() => {
						element.classList.add('animate');
					}, 800 * childIndex);
				}
			}

			elementResourcesAnimate;
		});

		animateOnLoad.forEach(element => {
			if (element && !this.isMobile()) {
				setTimeout(() => {
					element.classList.add('animate');
				}, 850);
			}
		});

		elementsToFaceIn.forEach(element => {
			const visible = this.isVisibleInWindow(element);

			if (visible && !this.isMobile()) {
				element.classList.add('animate');
			}
		});

		elementsToFadeInLoop.forEach((element, index) => {
			const visible = this.isVisibleInWindow(element);

			if (visible && !this.isMobile()) {
				setTimeout(() => {
					element.classList.add('animate');
				}, 400 * index);
			}
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(Index);
