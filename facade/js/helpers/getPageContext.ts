import { SheetsRegistry } from 'jss';
import {
	createMuiTheme,
	createGenerateClassName,
} from '@material-ui/core/styles';

// A custom theme for material UI
const theme = createMuiTheme({
	palette: {
		primary: {
			main: '#0F2542',
		},
		secondary: {
			main: '#0F2542',
		},
		error: {
			main: '#0F2542',
		},
	},
	typography: {
		useNextVariants: true,
		fontFamily: ['Muli', 'sans-serif'].join(', '),
		fontSize: 14,
	},
	overrides: {
		MuiLinearProgress: {
			barColorSecondary: {
				backgroundColor: '#09172a',
			},
		},
		MuiBackdrop: {
			root: {
				backgroundColor: 'rgba(0, 0, 0, 0.25)',
				WebkitTapHighlightColor: 'transparent',
				touchAction: 'none',
			},
		},
		MuiFormLabel: {
			root: {
				color: '#0F2542',
				fontWeight: 'normal',
				fontSize: 14,
				position: 'relative',
			},
		},
		MuiFormControlLabel: {
			root: {
				position: 'relative',
				alignItems: 'flex-start',
			},
		},
		MuiInput: {
			root: {
				color: '#e7e7e7',
				borderColor: '#e7e7e7',
				borderStyle: 'solid',
				borderWidth: '2px',
				fontSize: '12px',
				borderRadius: '2px',
				backgroundColor: '#fff',
				outline: '0',
				'&:before': {
					content: 'none',
				},
				'&:after': {
					content: 'none',
				},
				'&$focused': {
					borderColor: '#c4c5c4',
				},
				'&$error': {
					borderColor: '#ffae99',
				},
			},
			inputMultiline: {
				minHeight: 90,
				lineHeight: '1.5',
			},
			formControl: {
				margin: '0',
				'label + &': {
					marginTop: '0',
				},
			},
			underline: {
				'&:after': {
					content: 'none',
				},
				'&:before': {
					content: 'none',
				},
				'&:hover:not($disabled):not($focused):not($error):before': {
					content: 'none',
				},
			},
			input: {
				color: '#0F2542',
				margin: '0',
				lineHeight: '1',
				padding: '7px 16.5px',
			},
		},
		MuiInputLabel: {
			root: {
				color: '#0F2542',
				position: 'relative',
				lineHeight: '1.25',
				fontSize: '12px',
				fontWeight: 'bold',
				transform: 'none',
				marginBottom: '7.5px',
				'&$shrink': {
					transform: 'none',
				},
			},
			formControl: {
				transform: 'none',
				position: 'relative',
				top: 'auto',
				left: 'auto',
			},
		},
		MuiFormHelperText: {
			root: {
				color: '#757575',
				marginLeft: '0',
				marginTop: '7.5px',
				fontSize: '12px',
				lineHeight: '1.33',
			},
			error: {
				color: '#0F2542',
				marginLeft: '16px',
				fontSize: '10px',
			},
		},
		MuiButton: {
			root: {
				minHeight: '1px',
				minWidth: '1px',
				boxSizing: 'border-box',
				borderRadius: '2px',
				padding: '7.5px 16px',
				fontFamily: ['Muli', 'sans-serif'].join(', '),
				fontWeight: 600,
				fontSize: '14px',
				lineHeight: '1',
				borderWidth: '2px',
				borderStyle: 'solid',
				borderColor: '#ffaf99',
				transition: '0.3s all',
				'@media all and (-ms-high-contrast:none)': {
					padding: '7px 16px',
				},
				'&$disabled': {
					opacity: 0.5,
				},
			},
			label: {
				position: 'relative',
			},
			contained: {
				borderWidth: '2px',
				borderStyle: 'solid',
				boxShadow: 'none',
				'&:active': {
					boxShadow: 'none',
				},
			},
			containedPrimary: {
				boxShadow: 'none',
				backgroundColor: '#92C83E',
				borderColor: '#92C83E',
				color: '#FFFFFF',
				fill: '#FFFFFF',
				'&:hover:not($disabled)': {
					boxShadow: 'none',
					backgroundColor: 'transparent',
					borderColor: '#92C83E',
					color: '#92C83E',
					fill: '#92C83E',
				},
				'&$disabled': {
					opacity: 0.4,
					boxShadow: 'none',
					backgroundColor: '#92C83E',
					borderColor: '#92C83E',
					color: '#FFFFFF',
					fill: '#FFFFFF',
				},
				'&:focus': {
					opacity: 1,
					boxShadow: 'none',
					backgroundColor: '#92C83E',
					borderColor: '#92C83E',
					color: '#FFFFFF',
					fill: '#FFFFFF',
				},
				'&:active': {
					opacity: 1,
					boxShadow: 'none',
					backgroundColor: '#92C83E',
					borderColor: '#92C83E',
					color: '#FFFFFF',
					fill: '#FFFFFF',
				},
			},
			containedSecondary: {
				backgroundColor: '#FFFFFF',
				borderColor: '#8d95a3',
				color: '#0F2542',
				boxShadow: 'none',
				'&:hover:not($disabled)': {
					backgroundColor: '#0F2542',
					borderColor: '#0F2542',
					color: '#FFFFFF',
				},
				'&:active:not($disabled)': {
					boxShadow: 'none',
					backgroundColor: 'rgba(15, 37, 66, 0.8)',
					borderColor: 'rgba(15, 37, 66, 0.8)',
					color: '#FFFFFF',
				},
				'&$disabled': {
					backgroundColor: '#FFFFFF',
					color: '#2E3091',
				},
			},
			outlined: {
				padding: null,
				color: '#00ADEF',
				borderWidth: '2px',
				borderStyle: 'solid',
				borderColor: ' #00ADEF',
				'&:hover:not($disabled)': {
					color: '#2E3091',
					border: '2px solid #2E3091',
					background: 'transparent',
				},
				'&$disabled': {
					color: '#00ADEF',
					border: '2px solid #00ADEF',
				},
			},
			outlinedPrimary: {
				boxShadow: 'none',
				backgroundColor: 'transparent',
				border: '2px solid #92C83E',
				color: '#92C83E',
				fill: '#92C83E',
				'&:hover:not($disabled)': {
					boxShadow: 'none',
					backgroundColor: '#92C83E',
					borderColor: '#92C83E',
					color: '#FFFFFF',
					fill: '#FFFFFF',
				},
				'&$disabled': {
					opacity: 0.5,
					boxShadow: 'none',
					backgroundColor: 'transparent',
					borderColor: '#92C83E',
					color: '#92C83E',
					fill: '#92C83E',
				},
				'&:focus': {
					opacity: 1,
					boxShadow: 'none',
					backgroundColor: '#92C83E',
					borderColor: '#92C83E',
					color: '#FFFFFF',
					fill: '#FFFFFF',
				},
				'&:active': {
					opacity: 1,
					boxShadow: 'none',
					backgroundColor: '#92C83E',
					borderColor: '#92C83E',
					color: '#FFFFFF',
					fill: '#FFFFFF',
				},
			},
			outlinedSecondary: {
				boxShadow: 'none',
				backgroundColor: 'transparent',
				borderWidth: '2px',
				borderStyle: 'solid',
				borderColor: 'rgba(15, 37, 66, 0.5)',
				color: '#0F2542',
				'&:hover:not($disabled)': {
					boxShadow: 'none',
					backgroundColor: 'transparent',
					borderColor: '#92C83E',
					color: '#92C83E',
				},
				'&$disabled': {
					opacity: 0.5,
					boxShadow: 'none',
					backgroundColor: 'transparent',
					borderColor: 'rgba(15, 37, 66, 0.5)',
					color: '#0F2542',
				},
				'&:focus': {
					opacity: 1,
					boxShadow: 'none',
					backgroundColor: '#0F2542',
					borderColor: '#0F2542',
					color: '#FFFFFF',
				},
				'&:active': {
					opacity: 1,
					boxShadow: 'none',
					backgroundColor: '#0F2542',
					borderColor: '#0F2542',
					color: '#FFFFFF',
				},
			},
		},
		MuiSelect: {
			root: {
				backgroundColor: 'white',
				'&$focused': {
					backgroundColor: 'white',
				},
			},
			select: {
				'&:focus': {
					backgroundColor: 'transparent',
				},
			},
		},
		MuiSwitch: {
			root: {
				display: 'inline-flex',
				width: 62,
				position: 'relative',
				flexShrink: 0,
				zIndex: 0, // Reset the stacking context.
				verticalAlign: 'middle',
				'&$checked': {
					'& + $bar': {
						opacity: 1,
					},
				},
			},
			colorSecondary: {
				transform: 'translateX(-2px)',
				color: '#898989',
				'&$checked': {
					color: '#92C83E',
					transform: 'translateX(16px)',
					'& + $bar': {
						backgroundColor: 'transparent',
						opacity: 1,
					},
				},
			},
			icon: {
				boxShadow: 'none',
				backgroundColor: 'currentColor',
				width: 16,
				height: 16,
				borderRadius: '50%',
			},
			switchBase: {
				padding: 0,
				height: 48,
				width: 48,
				color: 'red',
			},
			bar: {
				borderRadius: 12.5 / 2,
				display: 'block',
				position: 'absolute',
				zIndex: -1,
				width: 32.5,
				height: 12.5,
				top: '50%',
				left: '50%',
				marginTop: 0,
				marginLeft: 0,
				transform: 'translate(-50%, -50%)',
				backgroundColor: 'transparent',
				opacity: 1,
				border: '1px solid rgba(151,151,151,0.6)',
			},
		},
		MuiMenuItem: {
			root: {
				fontSize: 14,
				fontWeight: 600,
				whiteSpace: 'normal',
				paddingTop: 0,
				paddingBottom: 0,
				lineHeight: 1,
				height: '40px',
				color: '#0F2542',
				'&:hover': {
					backgroundColor: '#f0f0f0',
				},
			},
			gutters: {
				paddingLeft: 24,
				paddingRight: 24,
			},
		},
		MuiPaper: {
			elevation2: {
				boxShadow: '0 0 6px 3px rgba(0,0,0,0.07)',
			},
		},
		MuiTab: {
			root: {
				minWidth: '1px',
				padding: '0 28px',
				fontWeight: 600,
				textTransform: 'none',
				opacity: 0.5,
			},
			wrapper: {
				fontSize: '12px',
			},
			labelContainer: {
				padding: '0',
				fontSize: '12px',
				'@media (min-width: 960px)': {
					padding: '0',
					fontSize: '12px',
				},
			},
			textColorPrimary: {
				color: '#2E3091',
				'&$selected': {
					fontWeight: 600,
					opacity: 1,
				},
			},
			textColorInherit: {
				opacity: 1,
				color: 'rgba(255, 255, 255, 0.5)',
				position: 'relative',
				// '&:after': {
				// 	content: '" "',
				// 	position: 'absolute',
				// 	bottom: 0,
				// 	height: 4,
				// 	width: 48,
				// 	backgroundColor: '#92C83E',
				// 	transition: '0.3s all',
				// 	opacity: 0,
				// },
				'&$selected': {
					opacity: 1,
					color: 'rgba(255, 255, 255, 1)',
				},
				// '&$selected:after': {
				// 	opacity: 1,
				// },
				'&$disabled': {
					opacity: 1,
					color: 'rgba(255, 255, 255, 1)',
				},
			},
		},
		MuiTabs: {
			scrollButtonsAuto: {
				display: 'none',
			},
			indicator: {
				backgroundColor: 'transparent',
				'&:before': {
					content: '""',
					display: 'block',
					position: 'absolute',
					left: '50%',
					bottom: 0,
					transform: 'translateX(-50%)',
					height: '4px',
					width: '40%',
					backgroundColor: '#92C83E',
				},
			},
		},
		MuiChip: {
			root: {
				height: '24px',
				margin: '2px',
				lineHeight: '1',
				borderRadius: '0',
			},
			label: {
				fontSize: '12px',
				lineHeight: '15px',
				color: '#0F2542',
				padding: '0',
			},
			deleteIcon: {
				margin: 0,
			},
		},
		MuiRadio: {
			colorPrimary: {
				color: '#102542',
				'&$checked': {
					color: '#102542',
				},
			},
		},
		MuiCheckbox: {
			colorPrimary: {
				color: '#102542',
				'&$checked': {
					color: '#102542',
				},
			},
		},
	},
});

function createPageContext() {
	return {
		theme,
		// This is needed in order to deduplicate the injection of CSS in the page.
		sheetsManager: new Map(),
		// This is needed in order to inject the critical CSS.
		sheetsRegistry: new SheetsRegistry(),
		// The standard class name generator.
		generateClassName: createGenerateClassName(),
	};
}

export default function getPageContext() {
	// Make sure to create a new context for every server-side request so that data
	// isn't shared between connections (which would be bad).
	if (!(process as any).browser) {
		return createPageContext();
	}

	// Reuse context on the client-side.
	if (!(global as any).__INIT_MATERIAL_UI__) {
		(global as any).__INIT_MATERIAL_UI__ = createPageContext();
	}

	return (global as any).__INIT_MATERIAL_UI__;
}
