import React from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import { ModalDialog } from '../../components/modals/local';

import StartDiscussionForm from './StartDiscussionForm';

import {
	IStoreState,
	discussionModalHide,
	apiError,
	apiSuccess,
	updateData,
} from '../../redux/store';

import {
	IApiTopic,
	IApiUser,
	IApiSituation,
	ISelectOption,
	IApiDiscussion,
} from '../../interfaces';
import { ApiService, withServices } from '../../services';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';
import EditDiscussionFormik from './EditDiscussionFormik';

interface IProps {
	discussionModalDisplay: boolean;
	discussionData: IApiDiscussion | null;
	predefinedTopic: string | null;
	predefinedSituations: string[] | null;
	userData: IApiUser;
	userAuthenticated: boolean | null;
	situations: IApiSituation[];
	apiService: ApiService;
	topics: IApiTopic[];
	dispatch(action: any): void;
}

interface IState {
	isSubmitting: boolean;
	isUpdating: boolean;
	userSituations: IApiSituation[];
	topicOptions: ISelectOption[];
}

class DiscussionModal extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props: IProps) {
		super(props);

		this.state = {
			isSubmitting: false,
			isUpdating: false,
			topicOptions: [],
			userSituations: [],
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleSubmitData = this.handleSubmitData.bind(this);
		this.submitEditData = this.submitEditData.bind(this);
		this.updatedSituations = this.updatedSituations.bind(this);
	}

	public componentWillMount() {
		this.updateData();
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.userData !== prevProps.userData
		) {
			if (this.props.userAuthenticated) {
				this.updateData();
			} else {
				this.handleClose();
			}
		}
	}

	public render() {
		let modalTitle: string = 'Start a New Discussion';

		if (this.props.discussionData !== null) {
			modalTitle = 'Edit Post';
		}

		return (
			<ModalDialog
				isActive={
					!!(
						this.props.discussionModalDisplay &&
						this.props.userAuthenticated
					)
				}
				modalTitle={modalTitle}
				ariaTag="new-discussion-modal"
				handleClose={this.handleClose}
			>
				{this.props.discussionData !== null ? (
					<EditDiscussionFormik
						discussionData={this.props.discussionData}
						onSubmit={this.submitEditData}
						submitting={this.state.isSubmitting}
						situations={this.props.situations}
						userSituations={this.state.userSituations}
						topics={this.state.topicOptions}
						updatedSituations={this.updatedSituations}
					/>
				) : (
					<StartDiscussionForm
						onSubmit={this.handleSubmitData}
						submitting={this.state.isSubmitting}
						situations={this.props.situations}
						userSituations={this.state.userSituations}
						topics={this.state.topicOptions}
						updatedSituations={this.updatedSituations}
						predefinedTopic={this.props.predefinedTopic}
						predefinedSituations={this.props.predefinedSituations}
					/>
				)}
			</ModalDialog>
		);
	}

	protected handleClose() {
		this.props.dispatch(discussionModalHide());
	}

	protected handleSubmitData(values) {
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/discussions', values, 'discussion')
					.then((newDiscussion: IApiDiscussion) => {
						const newDiscussionId = newDiscussion.slug;
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.GTM.pushEventToDataLayer({
									event: 'postDiscussion',
									id: newDiscussionId,
								});

								this.handleClose();
								this.props.dispatch(
									apiSuccess(['Discussion created']),
								);
								Router.push(
									`/discussions-single?id=${newDiscussionId}`,
									`/discussions/${newDiscussionId}`,
								);
							},
						);
					})
					.catch(error => {
						this.setState({
							isSubmitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	//TODO: Update after fixed API put methid
	protected submitEditData(values) {
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/discussions/${this.props.discussionData.id}`,
						values,
						'discussion',
					)
					.then((newDiscussion: IApiDiscussion) => {
						const newDiscussionId = newDiscussion.slug;
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.GTM.pushEventToDataLayer({
									event: 'editDiscussion',
									id: newDiscussionId,
								});

								this.handleClose();
								this.props.dispatch(
									apiSuccess(['Discussion edited']),
								);

								this.props.dispatch(updateData());

								// Reload the page if editing a child discussion
								if (
									!!newDiscussion.main_discussion_id &&
									typeof window !== 'undefined'
								) {
									window.location.href = `/discussions/${
										newDiscussion.main_discussion.slug
									}?replyId=${
										newDiscussion.discussion_index
									}`;
								}
							},
						);
					})
					.catch(error => {
						this.setState({
							isSubmitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected updateData() {
		// Fetch the user situation and topics
		if (this.props.userData) {
			const userSituations = this.props.userData.situations;

			if (userSituations.length) {
				this.setState({
					userSituations,
				});
			} else {
				// Use all situations from API
				this.setState({
					userSituations: JSON.parse(
						JSON.stringify(this.props.situations),
					),
				});
			}
		}

		this.updateTopics();
	}

	protected updateTopics(situations?: string[]) {
		const topicKey = 'discussions';

		const topics: IApiTopic[] = this.props.topics.filter(singleTopic => {
			if (singleTopic.category === topicKey) {
				// If situations are provided, make sure the topic match all the situations
				if (situations) {
					for (const situationSlug of situations) {
						if (
							!singleTopic.situations.find(topicSituation => {
								return topicSituation.slug === situationSlug;
							})
						) {
							return false;
						}
					}
				}
				return singleTopic;
			}
		});

		const topicOptions: ISelectOption[] = topics.map(singleTopic => {
			return {
				label: singleTopic.title,
				value: singleTopic.slug,
			};
		});

		this.setState({
			isUpdating: false,
			topicOptions,
		});
	}

	protected updatedSituations(situations: string[]) {
		this.updateTopics(situations);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const {
		discussionModalDisplay,
		discussionData,
		predefinedTopic,
		predefinedSituations,
	} = state.discussionModal;
	const { situations } = state.situations;
	const { topics } = state.topics;
	return {
		userAuthenticated,
		discussionModalDisplay,
		discussionData,
		predefinedTopic,
		predefinedSituations,
		userData,
		situations,
		topics,
	};
}

export default connect(mapStateToProps)(withServices(DiscussionModal));
