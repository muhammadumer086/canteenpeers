import React from 'react';

import ButtonBase from '@material-ui/core/ButtonBase';
import Icon from '@material-ui/core/Icon';

import Count from './Count';
import { ButtonBaseLink } from './ButtonBaseLink';

interface IProps {
	label: string;
	active: boolean;
	disabled: boolean;
	clickable: boolean;
	statusCount?: number;
	deletable?: boolean;
	icon?: string;
	isGreyedOut?: boolean;
	withoutBorder?: boolean;
	href?: string;
	hrefAs?: string;
	warning?: boolean;
	onClick?(): void;
	isPost?: boolean;
	isReply?: boolean;
	showIfZero?: boolean;
}

const Pill: React.FC<IProps> = props => {
	const classNames = ['pill'];

	if (!props.active) {
		classNames.push('pill--inactive');
	} else {
		classNames.push('pill--active');
	}

	if (props.clickable) {
		classNames.push('pill--clickable');
	}

	if (props.withoutBorder) {
		classNames.push('pill--withoutBorder');
	}

	if (props.icon) {
		classNames.push('pill--withIcon');
	}

	if (props.isGreyedOut) {
		classNames.push('pill--greyedOut');
	}

	if (props.warning) {
		classNames.push('pill--warning');
	}

	if (props.isPost) {
		classNames.push('pill--post');
	}

	if (props.isReply) {
		classNames.push('pill--post-reply');
	}

	if (!props.clickable) {
		return (
			<span className={classNames.join(' ')}>
				{renderIcon()}
				<span className="pill-label">{props.label}</span>
				{renderCount()}
				{renderDeletable()}
			</span>
		);
	} else {
		if (props.href) {
			return (
				<ButtonBaseLink
					focusRipple={true}
					className={classNames.join(' ')}
					disabled={props.disabled}
					onClick={props.onClick}
					href={props.href}
					hrefAs={props.hrefAs}
				>
					{renderIcon()}
					<span className="pill-label">{props.label}</span>
					{renderCount()}
					{renderDeletable()}
				</ButtonBaseLink>
			);
		}
		return (
			<ButtonBase
				focusRipple={true}
				className={classNames.join(' ')}
				disabled={props.disabled}
				onClick={props.onClick}
			>
				{renderIcon()}
				<span className="pill-label">{props.label}</span>
				{renderCount()}
				{renderDeletable()}
			</ButtonBase>
		);
	}

	function renderIcon() {
		if ('icon' in props && !!props.icon) {
			return (
				<span className="pill-icon">
					<Icon>{props.icon}</Icon>
				</span>
			);
		}
		return null;
	}

	function renderCount() {
		if (
			('statusCount' in props && !!props.statusCount) ||
			('showIfZero' in props && !!props.showIfZero)
		) {
			return (
				<span className="status">
					<Count count={props.statusCount} />
				</span>
			);
		}
		return null;
	}

	function renderDeletable() {
		if ('deletable' in props && !!props.deletable) {
			return <span className="status">x</span>;
		}
		return null;
	}
};

export default Pill;
