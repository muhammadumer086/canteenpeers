import { IStoreState } from '../store';
import { actionTypes } from '../actions';

import { ApiService } from '../../services';

import { IApiNotification } from '../../interfaces';

export const notificationsPanelDisplay = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (getState().notifications.notificationsPanelDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.NOTIFICATIONS_PANEL_DISPLAY,
	});
};

export const notificationsPanelHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().notifications.notificationsPanelDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.NOTIFICATIONS_PANEL_HIDE,
	});
};

export const notificationsLoad = (
	apiService: ApiService,
	token: string | null,
) => (dispatch, getState: () => IStoreState) => {
	if (!getState().notifications.notificationLoading && token) {
		dispatch({
			type: actionTypes.NOTIFICATION_LOADING,
		});
		apiService
			.queryAuthGET('/api/users/me/notifications', token)
			.then(data => {
				// get the number of unread notifications and the number of unread messages
				let notificationUnreadCount = 0;
				let notificationMessagesCount = 0;
				let notificationsData: IApiNotification[] = [];
				let notificationsMaxPage = 0;
				if (
					'data' in data &&
					data.data &&
					'notifications' in data.data &&
					data.data.notifications instanceof Array &&
					'meta' in data &&
					data.meta &&
					'last_page' in data.meta
				) {
					notificationUnreadCount = data.data.notifications.filter(
						notification => notification.read_at === null,
					).length;

					notificationsData = data.data.notifications;
					notificationsMaxPage = data.meta.last_page;
				}

				// Get the notifications for direct messages
				apiService
					.queryAuthGET(
						'/api/users/me/notifications?filter=MessageReceivedNotification',
						token,
					)
					.then(data => {
						notificationMessagesCount = data.data.notifications.filter(
							notification => notification.read_at === null,
						).length;

						dispatch({
							type: actionTypes.NOTIFICATION_LOADED,
							notificationUnreadCount,
							notificationMessagesCount,
							notificationsData,
							notificationsMaxPage,
						});
					})
					.catch(() => {
						dispatch({
							type: actionTypes.NOTIFICATION_LOADED,
							notificationUnreadCount: 0,
							notificationMessagesCount: 0,
							notificationsData: [],
							notificationsMaxPage: 0,
						});
					});
			})
			.catch(() => {
				dispatch({
					type: actionTypes.NOTIFICATION_LOADED,
					notificationUnreadCount: 0,
					notificationMessagesCount: 0,
					notificationsData: [],
					notificationsMaxPage: 0,
				});
			});
	}
};
