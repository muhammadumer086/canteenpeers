<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_blocked', function (Blueprint $table) {
            $table->increments('id');

			$table->unsignedInteger('user_id');
			$table->unsignedInteger('blocked_user_id');

			$table->foreign('user_id')
			->references('id')->on('users');

			$table->foreign('blocked_user_id')
			->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_blocked');
    }
}
