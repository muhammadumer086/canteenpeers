<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Discussion;
use App\PersonalSetting;
use App\User;

class UpdateUserState extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:userState';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update the users states when needed';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		User::disableSearchSyncing();
		$usersQuery = User::whereNull('country_slug')->whereNotNull('state');

		$this->line('');
		$this->info('Updating country slugs');
		$bar = $this->output->createProgressBar($usersQuery->count());
		$bar->start();
		$bar->setRedrawFrequency(10);

		$usersQuery->chunk(10, function ($users) use ($bar) {
			foreach ($users as $user) {
				$countrySlug = 'AU';
				if ($user->state === 'NZ') {
					$countrySlug = 'NZ';
				}
				$user->country_slug = $countrySlug;
				$user->save(['timestamps' => false]);
				$bar->advance();
			}
		});

		$bar->finish();

		$usersQuery = User::whereNotNull('country_slug')->whereNull('state');

		$this->line('');
		$this->info('Updating states');
		$bar = $this->output->createProgressBar($usersQuery->count());
		$bar->start();
		$bar->setRedrawFrequency(10);

		$usersQuery->chunk(10, function ($users) use ($bar) {
			foreach ($users as $user) {
				$state = null;
				if ($user->country_slug === 'NZ') {
					$state = 'NZ';
				}
				$user->state = $state;
				$user->save(['timestamps' => false]);
				$bar->advance();
			}
		});

		$bar->finish();
	}
}
