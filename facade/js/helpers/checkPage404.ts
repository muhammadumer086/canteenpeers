
export async function checkPage404(
	request: Request|null,
	query: any|null,
	paramName: string,
	callback: (paramValue: string) => any
) {
	if (
		query &&
		paramName in query &&
		query[paramName]
	) {
		try {
			return callback(query[paramName]);
		} catch(error) {
			(request as any).res.writeHead(302, {
				Location: '/404'
			});
			(request as any).res.end();
		}
	}

	return null;
}
