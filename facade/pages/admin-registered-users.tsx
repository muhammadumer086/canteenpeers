import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';

 

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import AdminFixedBar from '../js/components/admin/AdminFixedBar';
import HeroHeading from '../js/components/general/HeroHeading';
import { IApiEvent, IApiUser } from '../js/interfaces';

import AdminTable, {
	IAdminTableColumn,
} from '../js/components/admin/AdminTable';
import {
	ColumnNameSituation,
	ColumnAge,
	ColumnState,
	ColumnGender,
	ColumnSituation,
	ColumnActivityLog,
} from '../js/components/admin/AdminTableColumns';
import { userPanelDisplay } from '../js/redux/store';
import withAdminPage, {
	IAdminProps,
} from '../js/components/admin/withAdminPage';
import { EAdminPages } from './admin';

interface IProps extends IAdminProps {
	eventRegisteredUsers: {
		event: {
			users: IApiUser[];
		};
	};
}

interface IState {
	eventSlug: string | null;
	origin: string | null;
	event: IApiEvent | null;
	data: IApiUser[] | null;
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	isLoadingUsers: boolean;
}

class EventRegisteredUsers extends React.Component<IProps, IState> {
	protected tableColumns: IAdminTableColumn[] = [
		{
			header: 'Name',
			size: 20,
			renderer: ColumnNameSituation,
		},
		{
			header: 'Age',
			size: 5,
			renderer: ColumnAge,
		},
		{
			header: 'State',
			size: 5,
			renderer: ColumnState,
		},
		{
			header: 'Gender',
			size: 10,
			renderer: ColumnGender,
		},
		{
			header: 'Situation',
			size: 20,
			renderer: ColumnSituation,
		},
		{
			header: 'User Activity Log',
			size: 12,
			renderer: ColumnActivityLog,
		},
	];

	constructor(props: IProps) {
		super(props);

		let eventSlug = null;
		let origin = null;

		if (props.query && 'id' in props.query && props.query['id']) {
			eventSlug = props.query['id'];
		}

		console.log(props.query);

		if (props.query && 'origin' in props.query && props.query['origin']) {
			origin = props.query['origin'];
		}

		this.state = {
			eventSlug,
			origin,
			event: null,
			data: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isLoadingUsers: false,
		};

		this.loadEvent = this.loadEvent.bind(this);
		this.loadData = this.loadData.bind(this);
		this.displayUser = this.displayUser.bind(this);
	}

	public componentDidMount() {
		if (this.props.userAdmin) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps, prevState: IState) {
		if (this.props.userAdmin) {
			this.initialise(this.props, prevProps, prevState);
		}
	}

	public render() {
		const tableParams = {
			tableColumns: this.tableColumns,
			onRowClick: this.displayUser,
			data: this.state.data,
			loading: this.state.isLoadingUsers,
			totalMatchedRecords: this.state.total,
			onLoadMore:
				this.state.currentPage < this.state.maxPages
					? this.loadData
					: undefined,
		};

		let heroInfo = `Showing ${this.state.data.length} of ${
			this.state.total
		} registered users`;

		if (this.state.isLoadingUsers && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}

		let sectionTitle: string = '';

		if (this.state.event && !this.state.isLoadingUsers) {
			sectionTitle = `Registered Users for ${this.state.event.title}`;
			if (this.state.origin) {
				// Un-slugify the origin
				const originSplit = this.state.origin
					.replace(/-/g, ' ')
					.toLowerCase()
					.split(' ');
				for (var i = 0; i < originSplit.length; i++) {
					originSplit[i] =
						originSplit[i][0].toUpperCase() +
						originSplit[i].slice(1);
				}
				const origin = originSplit.join(' ');
				sectionTitle += ` from ${origin}`;
			}
		}

		return (
			<React.Fragment>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>

				<LayoutDashboard>
					<AdminFixedBar
						label="Event Management"
						url={`/admin?type=${EAdminPages.EVENTS}`}
						urlAs={`/admin?type=${EAdminPages.EVENTS}`}
					/>

					<header className="hero theme--accent">
						<HeroHeading title={sectionTitle} subInfo={heroInfo} />
					</header>

					<div className="admin_content">
						<AdminTable {...tableParams} />
					</div>
				</LayoutDashboard>
			</React.Fragment>
		);
	}

	protected displayUser(user: IApiUser) {
		this.props.dispatch(userPanelDisplay(user));
	}

	protected initialise(
		props: IProps,
		prevProps?: IProps,
		prevState?: IState,
	) {
		if (
			(!prevState && this.state.eventSlug) ||
			(prevState && this.state.eventSlug !== prevState.eventSlug)
		) {
			this.loadEvent(this.state.eventSlug);

			if (
				(props &&
					!prevProps &&
					props.userAuthenticated &&
					props.userAdmin) ||
				(props &&
					prevProps &&
					props.userAuthenticated !== prevProps.userAuthenticated &&
					props.userAdmin)
			) {
				this.loadData(false);
			}
		}
	}

	protected loadEvent(eventSlug: string | null) {
		if (eventSlug) {
			const url = `/api/events/${eventSlug}`;
			this.setState(
				{
					event: null,
				},
				() => {
					this.props.apiService
						.queryGET(url, 'event')
						.then((event: IApiEvent) => {
							this.setState({
								event,
							});
						});
				},
			);
		}
	}

	protected loadData(append: boolean = true) {
		if (
			!this.state.isLoadingUsers &&
			this.state.currentPage < this.state.maxPages &&
			this.state.eventSlug
		) {
			let params: any = {};

			if (append) {
				params.page = this.state.currentPage + 1;
			}

			if (this.state.origin) {
				if (origin) {
					params.origin = this.state.origin;
				}
			}

			const url = buildUrl(null, {
				path: `/api/events/${this.state.eventSlug}/list`,
				queryParams: params,
			});

			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'users' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								let data: IApiUser[] = response.data.users;

								if (append) {
									data = JSON.parse(
										JSON.stringify(this.state.data),
									);
									(response.data
										.user_activities as IApiUser[]).forEach(
										item => {
											data.push(item);
										},
									);
								}

								this.setState({
									data,
									isLoadingUsers: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
								});
							}
						})
						.catch(() => {});
				},
			);
		}
	}
}

export default withAdminPage(EventRegisteredUsers);
