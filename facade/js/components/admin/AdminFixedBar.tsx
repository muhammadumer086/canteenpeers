import React from 'react';
import Icon from '@material-ui/core/Icon';
import { ButtonBaseLink } from '../general/ButtonBaseLink';

interface IProps {
	label?: string;
	url?: string;
	urlAs?: string;
}

const AdminFixedBar: React.FC<IProps> = props => {
	const url = props.url || '/admin';
	const label = props.label || 'Admin Area';

	return (
		<div className="fixed_bar theme--accent_dark">
			<ButtonBaseLink
				focusRipple={true}
				className="fixed_bar-button"
				 
				href={url}
				hrefAs={props.urlAs}
			>
				<Icon className="fixed_bar-back_icon">arrow_left</Icon>
				{label}
				<span className="fixed_bar-button-border" />
			</ButtonBaseLink>
		</div>
	);
};

export default AdminFixedBar;
