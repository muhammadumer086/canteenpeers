import React from 'react';
import { connect } from 'react-redux';
import  algoliasearch from 'algoliasearch';
import { Async } from 'react-select';

import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import CancelIcon from '@material-ui/icons/Add';

import { IStoreState } from '../../redux/store';

import { IApiSettings, IApiUser } from '../../interfaces';

interface IProps {
	settings: IApiSettings | null;
	userData: IApiUser | null;
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	placeholder: string;
	disabled?: boolean;
	multi?: boolean;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
}

interface IState {
	disabled: boolean;
	selectValue: string | number | boolean | (string | number | boolean)[];
}

class Option extends React.Component<any, {}> {
	constructor(props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	public render() {
		const { isFocused, isSelected, onFocus, option } = this.props;

		return (
			<MenuItem
				onFocus={onFocus}
				selected={isFocused}
				onClick={this.handleClick}
				component="div"
				style={{
					fontWeight: isSelected ? 500 : 400,
				}}
				className="autocomplete_user-item"
			>
				<ListItemIcon className="autocomplete_user-item-icon">
					<Avatar
						src={option.value.image}
						alt={option.value.title}
						className="autocomplete_user-avatar"
					/>
				</ListItemIcon>
				<ListItemText
					inset
					className="autocomplete_user-username"
					primary={option.value.title}
				/>
			</MenuItem>
		);
	}

	protected handleClick(event) {
		this.props.onSelect(this.props.option, event);
	}
}

const SelectWrapped: React.FC<any> = (props: any) => {
	const { classes, value, ...other } = props;

	return (
		<Async
			optionComponent={Option}
			noResultsText={<Typography>{'No users found'}</Typography>}
			arrowRenderer={arrowProps => {
				return arrowProps.isOpen ? (
					<ArrowDropUpIcon className="input-select-icon" />
				) : (
					<ArrowDropDownIcon className="input-select-icon" />
				);
			}}
			className="autocomplete_user-select"
			clearable={false}
			cache={false}
			value={value}
			valueComponent={valueProps => {
				const { value, children, onRemove } = valueProps;

				const onDelete = event => {
					event.preventDefault();
					event.stopPropagation();
					onRemove(value);
				};

				if (onRemove) {
					return (
						<Chip
							className="autocomplete_user-chip"
							avatar={
								<Avatar
									className="autocomplete_user-avatar"
									src={value.value.image}
									alt={value.value.title}
								/>
							}
							tabIndex={-1}
							label={
								<span className="autocomplete_user-label">
									{value.value.title}
								</span>
							}
							deleteIcon={
								<CancelIcon
									className="autocomplete_user-icon"
									onTouchEnd={onDelete}
								/>
							}
							onDelete={onDelete}
						/>
					);
				}

				return <div className="Select-value">{children}</div>;
			}}
			{...other}
		/>
	);
};

class AutocompleteUserFormikComponent extends React.Component<IProps, IState> {
	protected algoliaClient: algoliasearch.Client = null;
	protected algoliaIndex: algoliasearch.Index = null;
	protected selectedUsernames: string[] = [];

	constructor(props) {
		super(props);

		this.state = {
			disabled: false,
			selectValue:
				props.name in props.values ? props.values[props.name] : null,
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleQuery = this.handleQuery.bind(this);
	}

	public componentDidMount() {
		if (this.props.settings) {
			this.algoliaClient = algoliasearch(
				this.props.settings.algolia_app_id,
				this.props.settings.algolia_search_key,
			);
			this.algoliaIndex = this.algoliaClient.initIndex(
				this.props.settings.search_index,
			);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.settings &&
			JSON.stringify(prevProps.settings) !==
				JSON.stringify(this.props.settings)
		) {
			this.algoliaClient = algoliasearch(
				this.props.settings.algolia_app_id,
				this.props.settings.algolia_search_key,
			);
			this.algoliaIndex = this.algoliaClient.initIndex(
				this.props.settings.search_index,
			);
		}
	}

	public render() {
		return (
			<TextField
				className="form-input form-input--autocomplete autocomplete_user"
				value={''}
				onChange={this.handleChange}
				name={this.props.name}
				label={this.props.label}
				error={
					this.props.touched[this.props.name] &&
					!!this.props.errors[this.props.name]
				}
				helperText={
					this.props.touched[this.props.name]
						? this.props.errors[this.props.name]
						: null
				}
				InputLabelProps={{
					shrink: true,
				}}
				autoComplete="off"
				InputProps={{
					inputComponent: SelectWrapped,
					inputProps: {
						instanceId: 'react-select-chip-label',
						className: 'autocomplete_user-select',
						id: 'react-select-chip-label',
						loadOptions: this.handleQuery,
						disabled: this.state.disabled,
						value: this.state.selectValue,
						placeholder: this.props.placeholder,
						multi: 'multi' in this.props ? this.props.multi : false,
					},
				}}
				disabled={this.state.disabled}
			/>
		);
	}

	public handleChange(option) {
		if (this.props.multi) {
			this.setState(
				{
					selectValue: option,
				},
				() => {
					this.props.setFieldValue(
						this.props.name,
						option.map(singleOption => {
							return singleOption.label;
						}),
					);
					this.props.setFieldTouched(this.props.name, true, true);
					this.updateSelectedUsernames();
				},
			);
		} else {
			if (option && option.hasOwnProperty('label')) {
				this.setState(
					{
						selectValue: option,
					},
					() => {
						this.props.setFieldValue(this.props.name, [
							option.label,
						]);
						this.props.setFieldTouched(this.props.name, true, true);
						this.updateSelectedUsernames();
					},
				);
			} else {
				this.setState(
					{
						selectValue: null,
					},
					() => {
						this.props.setFieldValue(this.props.name, null);
						this.props.setFieldTouched(this.props.name, true, true);
						this.updateSelectedUsernames();
					},
				);
			}
		}
	}

	public handleQuery(inputValue) {
		if (inputValue) {
			return new Promise((resolve, reject) => {
				if (!this.props.settings || !this.algoliaIndex) {
					resolve({
						options: [],
					});
					return;
				}

				this.algoliaIndex
					.search({
						query: inputValue,
						facetFilters: ['type:user'],
					})
					.then(data => {
						const results = {
							options: [],
						};
						if ('hits' in data && data.hits instanceof Array) {
							results.options = data.hits
								.filter(res => {
									return (
										res.id !==
											this.props.userData.username &&
										this.selectedUsernames.indexOf(res.id) <
											0
									);
								})
								.map(res => {
									return {
										label: res.id,
										value: res,
									};
								});
						}

						resolve(results);
					})
					.catch(error => {
						console.warn(`User autocomplete error:`, error);
						reject(error);
					});
			});
		} else {
			return Promise.resolve({ options: [] });
		}
	}

	protected updateSelectedUsernames() {
		this.selectedUsernames = [];

		if (this.props.multi && this.state.selectValue instanceof Array) {
			this.selectedUsernames = this.state.selectValue.map(res => {
				return (res as any).label;
			});
		} else {
			if (this.state.selectValue) {
				this.selectedUsernames.push(
					(this.state.selectValue as any).label,
				);
			}
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { settings } = state.settings;
	const { userData } = state.user;
	return {
		settings,
		userData,
	};
}

export const AutocompleteUserFormik = connect(mapStateToProps)(
	AutocompleteUserFormikComponent,
);
