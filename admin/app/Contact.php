<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model {

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'contact';

	/**
	 * The user that created this contact request
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

}
