import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button     from '@material-ui/core/Button';
import Icon       from '@material-ui/core/Icon';

import { IApiUser, IApiLanguage, IApiCountry, ISelectOption, IApiHeardAbout } from '../../interfaces';
import { ETabs, EAccountScreens } from './UserPanelAccount';
import { validationsUser } from '../../helpers/validations';
import { AutocompleteSelectFormik, RadioGroupFormik } from '../inputs';

interface IProps {
	userData: IApiUser|null;
	submitting: boolean;
	countries: IApiCountry[];
	languages: IApiLanguage[];
	heardAbouts: IApiHeardAbout[];
	changeScreen(tab: ETabs, screen: EAccountScreens);
	onSubmit(values: any, resetForm?: () => void);
}

const UserAccountOptional: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		country_of_birth: validationsUser.country_of_birth,
		indigenous_australian: validationsUser.indigenous_australian,
		languages: validationsUser.languages,
		heard_abouts: validationsUser.heard_abouts,
	});

	const countriesOptions: ISelectOption[] = props.countries.map((country): ISelectOption => {
		return {
			value: country.slug,
			label: country.name,
		};
	});

	const languagesOptions: ISelectOption[] = props.languages.map((language): ISelectOption => {
		return {
			value: language.slug,
			label: language.name,
		};
	})
	.sort((a, b) => {
		if (a.label === 'None') {
			return -1;
		}
		if (a.label > b.label) {
			return 1;
		} else if (a.label < b.label) {
			return -1;
		}
		return 0;
	});

	const heardAboutsOptions: ISelectOption[] = props.heardAbouts.map((heardAbout): ISelectOption => {
		return {
			value: heardAbout.slug,
			label: heardAbout.name
		};
	});

	const initialValues = {
		country_of_birth: null,
		indigenous_australian: props.userData.indigenous_australian,
		languages: [],
		heard_abouts: [],
	};

	if (props.userData.country_of_birth) {
		initialValues.country_of_birth = props.userData.country_of_birth.slug;
	}

	if (props.userData.languages.length) {
		initialValues.languages = props.userData.languages.map((language) => {
			return language.slug;
		});
	}

	if (props.userData.heard_abouts.length) {
		initialValues.heard_abouts = props.userData.heard_abouts.map((heardAbout) => {
			return heardAbout.slug;
		});
	}

	return (
		<div className="user_account_panel user_account_panel--account_optional">
			<div className="user_panel-back">
				<ButtonBase
					focusRipple={true}
					className="user_panel-back-button"
					onClick={() => {
						props.changeScreen(ETabs.MY_PROFILE, EAccountScreens.OVERVIEW)
					}}
					disabled={props.submitting}
				>
					<Icon className="user_panel-back-back_icon">arrow_back</Icon>
					Profile Overview
				</ButtonBase>
			</div>
			<div className="user_panel-heading">
				<h4 className="font--h4 theme-title">Optional Details</h4>
			</div>
			<Formik
				initialValues={initialValues}
				validationSchema={validationFormat}
				onSubmit={(values) => {
					props.onSubmit(values);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>
							<div className="form-row">
								<div className="form-column">
									<AutocompleteSelectFormik
										label="Country of Birth"
										name="country_of_birth"
										options={countriesOptions}
										disabled={props.submitting}
										emptyLabel="Please select"
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<RadioGroupFormik
										label="Are you Aboriginal or Torres Straight Islander?"
										name="indigenous_australian"
										options={[
											{
												label: 'Yes',
												value: true
											}, {
												label: 'No',
												value: false
											}
										]}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<AutocompleteSelectFormik
										label="Do you speak a language other than English at home?"
										name="languages"
										options={languagesOptions}
										disabled={!props.submitting}
										emptyLabel="Please select"
										multi={true}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<AutocompleteSelectFormik
										label="How did you hear about us?"
										name="heard_abouts"
										options={heardAboutsOptions}
										disabled={!props.submitting}
										emptyLabel="Please select all that apply"
										multi={true}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit_left">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || !touched || props.submitting}
									>
										Update
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
}

export default UserAccountOptional;
