import React from 'react';
import { connect } from 'react-redux';
// import Link from 'next/link';
import { SingletonRouter, withRouter } from 'next/router';
import ReactSVG from 'react-svg';

// import ButtonBase from '@material-ui/core/ButtonBase';

import { IStoreState } from '../../redux/store';
import { menuPanelHide } from '../../redux/actions';
import { GTMDataLayer } from '../../../js/helpers/dataLayer';
import { ButtonBaseLink } from './ButtonBaseLink';
import { IApiUser } from '../../../js/interfaces';
import { Button } from '@material-ui/core';

interface IProps {
	userAuthenticated: boolean | null;
	userAdmin: boolean | null;
	userData: IApiUser | null;
	notificationMessagesCount: number;
	dispatch(action: any): void;
}

interface ILinks {
	label: string;
	url: string;
	icon: string;
	disabled?: boolean;
	counter?: number;
}

interface IState {
	links: ILinks[];
}

interface IActiveLinkProps {
	children?: React.ReactNode;
	router?: SingletonRouter;
	href: string;
	dispatch(action: any): void;
}

const ActiveLinkBase: React.SFC<IActiveLinkProps> = (
	props: IActiveLinkProps,
) => {
	const classNames = ['dashboard_sidebar-link'];
	if (
		(props.href === '/' && props.router.pathname === props.href) ||
		(props.href !== '/' && props.router.pathname.indexOf(props.href) >= 0)
	) {
		classNames.push('dashboard_sidebar-link--active');
	}

	const closeMenu = () => {
		props.dispatch(menuPanelHide());
	};

	return (
		<ButtonBaseLink
			href={props.href}
			prefetch
			onClick={closeMenu}
			className={classNames.join(' ')}
		>
			{props.children}
		</ButtonBaseLink>
	);
};

const ActiveLink: any = connect()(withRouter(ActiveLinkBase as any));

class DashboardSidebar extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();
	constructor(props: IProps) {
		super(props);

		this.state = {
			links: this.generateLinksList(
				props.userAuthenticated,
				props.userAdmin,
			),
		};

		this.handleChatToConsellor = this.handleChatToConsellor.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.notificationMessagesCount !==
				prevProps.notificationMessagesCount ||
			this.props.userData !== prevProps.userData
		) {
			this.setState({
				links: this.generateLinksList(
					this.props.userAuthenticated,
					this.props.userAdmin,
				),
			});
		}
	}

	public render() {
		return (
			<div className="dashboard_sidebar">
				{/* <header className="dashboard_sidebar-header">
					<Link prefetch href="/dashboard">
						<ButtonBase
							focusRipple={true}
							className="dashboard_sidebar-header-title"
						>
							<img
								className="dashboard_sidebar-header-logo"
								src={`${
									process.env.STATIC_PATH
								}/images/canteen-connect-logo-updated.svg`}
								alt={process.env.APP_NAME}
							/>
						</ButtonBase>
					</Link>
					<Button
						color="primary"
						variant="contained"
						className="consellor_chat"
						href={`mailto:${process.env.INTERCOM_APP_ID}@intercom-mail.com`}
					>
						{this.props.userAuthenticated
							? 'Chat To A Counsellor'
							: 'Questions? Let’s Chat'}
					</Button>
				</header> */}
				<div className="dashboard_sidebar-content">
					<div className="dashboard_sidebar-content-links">
						{this.state.links.map((linkData: ILinks, index) => {
							const classNames = [
								'dashboard_sidebar-link-content',
							];

							if (linkData.counter) {
								classNames.push(
									'dashboard_sidebar-link-content--counter',
								);
							}

							if (linkData.disabled) {
								return (
									<span
										className="dashboard_sidebar-link dashboard_sidebar-link--disabled"
										key={index}
									>
										<span className={classNames.join(' ')}>
											<span className="dashboard_sidebar-link-icon">
												<ReactSVG
													path={linkData.icon}
												/>
											</span>
											<span className="dashboard_sidebar-link-label">
												{linkData.label}
											</span>
											{linkData.counter && (
												<span className="dashboard_sidebar-link-counter">
													{this.handleCounter(
														linkData.counter,
													)}
												</span>
											)}
										</span>
									</span>
								);
							}

							return (
								<ActiveLink href={linkData.url} key={index}>
									<span className={classNames.join(' ')}>
										<span className="dashboard_sidebar-link-icon">
											<ReactSVG path={linkData.icon} />
										</span>
										<span className="dashboard_sidebar-link-label">
											{linkData.label}
										</span>
										{linkData.counter > 0 && (
											<span className="dashboard_sidebar-link-counter">
												{this.handleCounter(
													linkData.counter,
												)}
											</span>
										)}
									</span>
								</ActiveLink>
							);
						})}
						<div className="dashboard_sidebar-chat_to_counsellor">
							<Button
								color="primary"
								variant="contained"
								className="consellor_chat consellor_chat_button"
								href={`mailto:${process.env.INTERCOM_APP_ID}@intercom-mail.com`}
							>
								{this.props.userAuthenticated
									? 'Chat To A Counsellor'
									: 'Questions? Let’s Chat'}
							</Button>
						</div>
					</div>
				</div>
			</div>
		);
	}

	protected generateLinksList(
		userAuthenticated: boolean,
		userAdmin: boolean,
	): ILinks[] {
		const result: ILinks[] = [];

		result.push({
			label: 'Home',
			url: '/dashboard',
			icon: `${process.env.STATIC_PATH}/icons/home.svg`,
		});
		if (userAuthenticated) {
			result.push({
				label: 'People Like Me',
				url: '/people-like-me',
				icon: `${process.env.STATIC_PATH}/icons/people-like-me.svg`,
			});
		} else {
			result.push({
				label: 'People Like Me',
				url: '/people-like-me',
				icon: `${process.env.STATIC_PATH}/icons/people-like-me.svg`,
				disabled: true,
			});
		}

		result.push({
			label: 'Discussions',
			url: '/discussions',
			icon: `${process.env.STATIC_PATH}/icons/discussions.svg`,
		});
		if (
			userAuthenticated &&
			this.props.userData &&
			!this.props.userData.underage
		) {
			result.push({
				label: 'Messages',
				url: '/messages',
				icon: `${process.env.STATIC_PATH}/icons/messages.svg`,
				counter: this.props.notificationMessagesCount,
			});
		}
		result.push({
			label: 'Blogs',
			url: '/blogs',
			icon: `${process.env.STATIC_PATH}/icons/blogs.svg`,
		});
		result.push({
			label: 'Resources',
			url: '/resources',
			icon: `${process.env.STATIC_PATH}/icons/resources.svg`,
		});
		result.push({
			label: 'Events',
			url: '/events',
			icon: `${process.env.STATIC_PATH}/icons/events.svg`,
		});

		if (userAdmin) {
			result.push({
				label: 'Admin',
				url: '/admin',
				icon: `${process.env.STATIC_PATH}/icons/admin.svg`,
			});
		}

		return result;
	}

	protected handleCounter(value: number) {
		if (value > 10) {
			return '10+';
		}
		return value;
	}

	protected handleChatToConsellor() {
		this.props.dispatch(menuPanelHide());

		this.GTM.pushEventToDataLayer({
			event: 'chatToAdmin',
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { notificationMessagesCount } = state.notifications;
	const role_names=userData&&userData.role_names;
		const roleName=role_names&&role_names.length>0&&role_names[0];
		
		const userAdmin=roleName==="user"||!userAuthenticated?false:true;
	return {
		userAuthenticated,
		userAdmin,
		userData,
		notificationMessagesCount,
	};
}

export default connect(mapStateToProps)(DashboardSidebar);
