import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import moment from 'moment';
import Button from '@material-ui/core/Button';

 

import { IApiUser, IApiUserActivity } from '../js/interfaces';
import { processQueryParams } from '../js/helpers/processQueryParams';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import AdminFixedBar from '../js/components/admin/AdminFixedBar';
import HeroHeading from '../js/components/general/HeroHeading';
import AdminTable, {
	IAdminTableColumn,
} from '../js/components/admin/AdminTable';
import withAdminPage, {
	IAdminProps,
	IAdminState,
} from '../js/components/admin/withAdminPage';
import {
	ColumnType,
	ColumnTitle,
	ColumnActivityTime,
	ColumnSituations,
	ColumnTopic,
	ColumnViewsCount,
	ColumnGetItCount,
	ColumnHugCount,
	ColumnRepliesCount,
	ColumnAdditionalData,
} from '../js/components/admin/AdminTableColumns';
import AdminFilter from '../js/components/admin/AdminFilter';
import AdminFilterActivities from '../js/components/admin/AdminFilterActivities';
import { downloadCsvFile } from '../js/helpers/downloadFile';
import { EAdminPages } from './admin';

interface IProps extends IAdminProps {}

interface IState extends IAdminState {
	userId: string | null;
	user: IApiUser | null;
	data: IApiUserActivity[];
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	isLoadingUsers: boolean;
	filterOpen: boolean;
	filterValues: any;
	reportGenerating: boolean;
}

class UserActivities extends React.Component<IProps, IState> {
	protected tableColumns: IAdminTableColumn[] = [
		{
			header: 'Activity Type',
			size: 20,
			renderer: ColumnType,
			sortable: 'type',
		},
		{
			header: 'Title',
			size: 20,
			renderer: ColumnTitle,
		},
		{
			header: 'Date & Time of Activity',
			size: 14,
			renderer: ColumnActivityTime,
			sortable: 'created_at',
		},
		{
			header: 'Posted in Situation(s)',
			size: 15,
			renderer: ColumnSituations,
		},
		{
			header: 'Posted in Topic',
			size: 12,
			renderer: ColumnTopic,
		},
		{
			header: 'Post Views',
			size: 6,
			renderer: ColumnViewsCount,
		},
		{
			header: 'I Get It',
			size: 6,
			renderer: ColumnGetItCount,
		},
		{
			header: 'Hugs',
			size: 6,
			renderer: ColumnHugCount,
		},
		{
			header: 'Replies',
			size: 6,
			renderer: ColumnRepliesCount,
		},
		{
			header: 'Additional Data',
			size: 20,
			renderer: ColumnAdditionalData,
		},
	];

	constructor(props: IProps) {
		super(props);

		const paramName = 'id';
		let userId = null;

		if (props.query && paramName in props.query && props.query[paramName]) {
			userId = props.query[paramName];
		}

		this.state = {
			userId,
			user: null,
			data: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isLoadingUsers: false,
			filterOpen: false,
			filterValues: {
				activities: [],
				sort: 'created_at-desc',
			},
			reportGenerating: false,
		};

		this.loadData = this.loadData.bind(this);
		this.handleOpenFilter = this.handleOpenFilter.bind(this);
		this.handleCloseFilter = this.handleCloseFilter.bind(this);
		this.handleFilter = this.handleFilter.bind(this);
		this.onGenerateReport = this.onGenerateReport.bind(this);
		this.onUpdateSort = this.onUpdateSort.bind(this);
	}

	public componentDidMount() {
		if (this.props.userAdmin) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps, prevState: IState) {
		if (this.props.userAdmin) {
			this.initialise(this.props, prevProps, prevState);
		}
	}

	public render() {
		const tableParams = {
			tableColumns: this.tableColumns,
			data: this.state.data,
			loading: this.state.isLoadingUsers,
			onLoadMore:
				this.state.currentPage < this.state.maxPages
					? this.loadData
					: undefined,
			onGenerateReport: this.onGenerateReport,
			reportGenerating: this.state.reportGenerating,
			totalMatchedRecords: this.state.total,
			sort: this.state.filterValues.sort,
			onUpdateSort: this.onUpdateSort,
		};

		let heroTitle = 'Loading...';
		if (this.state.user) {
			heroTitle = `${this.state.user.full_name}'s Activity Log`;
		}

		let heroInfo = `Showing ${this.state.data.length} of ${
			this.state.total
		} activities`;

		if (this.state.isLoadingUsers && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar
						label="Users Management"
						url={`/admin?type=${EAdminPages.USERS}`}
						urlAs={`/admin?type=${EAdminPages.USERS}`}
					/>
					<header className="hero theme--accent">
						<HeroHeading title={heroTitle} subInfo={heroInfo} />
						<span className="hero-actions">
							<Button
								variant="outlined"
								color="primary"
								onClick={this.handleOpenFilter}
							>
								Filter Activities
							</Button>
						</span>
					</header>
					<div className="admin_content">
						<AdminTable {...tableParams} />
					</div>
					{this.renderFilters()}
				</LayoutDashboard>
			</div>
		);
	}

	protected renderFilters() {
		return (
			<AdminFilter
				isOpen={this.state.filterOpen}
				handleCloseFilter={this.handleCloseFilter}
			>
				<AdminFilterActivities
					initialValues={this.state.filterValues}
					handleFilter={this.handleFilter}
				/>
			</AdminFilter>
		);
	}

	protected initialise(
		props: IProps,
		prevProps?: IProps,
		prevState?: IState,
	) {
		if (
			(!prevState && this.state.userId) ||
			(prevState && this.state.userId !== prevState.userId)
		) {
			this.loadUser(this.state.userId);

			if (
				(props &&
					!prevProps &&
					props.userAuthenticated &&
					props.userAdmin) ||
				(props &&
					prevProps &&
					props.userAuthenticated !== prevProps.userAuthenticated &&
					props.userAdmin)
			) {
				this.loadData(false);
			}
		}
	}

	protected loadData(append: boolean = true) {
		if (
			!this.state.isLoadingUsers &&
			this.state.currentPage < this.state.maxPages &&
			this.state.userId
		) {
			let params: any = processQueryParams(
				JSON.parse(JSON.stringify(this.state.filterValues)),
			);

			if (append) {
				params.page = this.state.currentPage + 1;
			}

			const url = buildUrl(null, {
				path: `/api/admin/users/${this.state.userId}/activities`,
				queryParams: params,
			});
			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'user_activities' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								let data: IApiUserActivity[] =
									response.data.user_activities;

								if (append) {
									data = JSON.parse(
										JSON.stringify(this.state.data),
									);
									(response.data
										.user_activities as IApiUserActivity[]).forEach(
										item => {
											data.push(item);
										},
									);
								}

								this.setState({
									data,
									isLoadingUsers: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
								});
							}
						});
				},
			);
		}
	}

	protected loadUser(userId: string | null) {
		if (userId) {
			this.setState(
				{
					user: null,
				},
				() => {
					this.props.apiService
						.queryGET(`/api/users/${userId}`, 'user')
						.then((user: IApiUser) => {
							this.setState({
								user,
							});
						});
				},
			);
		}
	}

	protected handleOpenFilter() {
		this.setState({
			filterOpen: true,
		});
	}
	protected handleCloseFilter() {
		this.setState({
			filterOpen: false,
		});
	}

	protected handleFilter(values: any) {
		this.setState(
			{
				filterOpen: false,
				filterValues: values,
				data: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadData(false);
			},
		);
	}

	protected onGenerateReport() {
		if (this.state.user) {
			this.setState(
				{
					reportGenerating: true,
				},
				() => {
					const data = Object.assign({}, this.state.filterValues);

					let params: any = processQueryParams(data);

					const url = buildUrl(null, {
						path: `/api/admin/users/${
							this.state.user.username
						}/activities/export`,
						queryParams: params,
					});

					const fileName =
						`canteen-${
							this.state.user.username
						}-activities-report-` +
						moment().format('YYYYMMDD-HHmm') +
						'.csv';

					downloadCsvFile(this.props.apiService, url, fileName).then(
						() => {
							this.setState({
								reportGenerating: false,
							});
						},
					);
				},
			);
		}
	}

	protected onUpdateSort(sortable: string, direction: 'asc' | 'desc') {
		this.setState(
			{
				filterValues: Object.assign(this.state.filterValues, {
					sort: `${sortable}-${direction}`,
				}),
				data: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadData(false);
			},
		);
	}
}

export default withAdminPage(UserActivities);
