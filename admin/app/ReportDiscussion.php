<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportDiscussion extends Model {
	use SoftDeletes;

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'age_sensitive' => 'boolean',
	];

	/**
	 * Get the discussion reported
	 */
	public function discussion() {
		return $this->belongsTo('App\Discussion');
	}

	/**
	 * Get the user that made the report.
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

	/**
	 * Get the user that reviewed the report.
	 */
	public function reviewer() {
		return $this->belongsTo('App\User', 'reviewer_id');
	}
}
