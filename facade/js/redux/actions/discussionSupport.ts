import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { IApiDiscussion } from 'js/interfaces';

export const discussionSupportDisplay = (discussionData?: IApiDiscussion) => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (getState().discussionSupport.discussionSupportDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.DISCUSSION_SUPPORT_DISPLAY,
		discussionData: discussionData ? discussionData : null,
	});
};

export const discussionSupportHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().discussionSupport.discussionSupportDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.DISCUSSION_SUPPORT_HIDE,
	});
};
