<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Http\Resources\Resources as ResourcesResource;
use App\Resource;

class ResourcesCollection extends ResourceCollection {
	use \App\Traits\AuthUser;

	protected $withFeaturedResources = null;

	public function setFeaturedResourceIds($featuredResources) {
		$this->withFeaturedResources = $featuredResources;
	}

	/**
	 * Transform the resource collection into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		return [
			'featured' => $this->when(!is_null($this->withFeaturedResources), function () {
				return ResourcesResource::collection($this->withFeaturedResources);
			}),
			'data' => ['resources' => $this->collection]
		];
	}
}
