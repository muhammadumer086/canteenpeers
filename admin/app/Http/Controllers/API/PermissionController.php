<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Validator;


class PermissionController extends Controller
{
	const PAGINATION_SIZE = 30;
	const RELATED_SIZE = 2;

	/**
	 * Display a listing of paginated roles.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		if (
			$authUser &&
			$authUser->hasRoleName('admin')
		) {
			$permissions = Permission::paginate(self::PAGINATION_SIZE);
			return response()->json(
				[
					'permissions' => $permissions,
				],
				200
			);
		}
	}

	/**
	 * Store a newly created permission in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'name' => 'bail|required|string|max:191'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}
		$permission = Permission::create(['name' => $request->input('name'),'guard_name' => 'web']);

		return response()->json(
			[
				'permission' => $permission,
				'succcess' => 'Permission has been added',
			],
			200
		);
	}

	/**
	 * Update the specified permission from storage.
	 *
	 * @param \App\Permission $permission
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Permission $permission)
	{
		$authUser = $this->getAuthUser($request);

		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'name' => 'bail|required|string|max:191'
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();
			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$permission->name = $request->name;
		$permission->save();
		return response()->json(
			[
				'error' => "Successfully Updated",
			],
			200
		);

	}


	/**
	 * Remove the specified permission from storage.
	 *
	 * @param \App\Permission $permission
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, Permission $permission) {
		$authUser = $this->getAuthUser($request);

		if (!$this->hasAdminPermission()) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$permission->delete();

		return response()->json([
			'success' => true,
		]);
	}

}
