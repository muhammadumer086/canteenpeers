<?php
namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\User;

class ApiResourceStoreHelper {
	protected $resourcesMap = [];
	protected $request;

	/**
	 * Call this method to get singleton
	 *
	 * @return ApiResourceStoreHelper
	 */
	public static function Instance() {
		static $inst = null;
		if ($inst === null) {
			$inst = new ApiResourceStoreHelper();
		}
		return $inst;
	}

	/**
	 * Private ctor so nobody else can instantiate it
	 *
	 */
	private function __construct() {
		$this->request = request();
	}

	/**
	 * Check if an api resource is available
	 *
	 * @param string model name
	 * @param int model id
	 * @param string updated at value
	 * @param boolean check laravel's Cache
	 * @param boolean attach the request to the cache
	 * @return boolean
	 */
	public function hasApiResource($modelName, $id, $updatedAt, $withCache = false, $withRequest = true) {
		$key = $this->getKey($modelName, $id, $updatedAt, $withRequest);

		if ($withCache && Cache::has($key)) {
			$this->resourcesMap[$key] = Cache::get($key);
		}

		return (array_key_exists($key, $this->resourcesMap));
	}

	/**
	 * Get the cached API resource if available
	 *
	 * @param string model name
	 * @param int model id
	 * @param string updated at value
	 * @param boolean check laravel's Cache
	 * @param boolean attach the request to the cache
	 * @return mixed|null
	 */
	public function getApiResource($modelName, $id, $updatedAt, $withCache = false, $withRequest = true) {
		if ($this->hasApiResource($modelName, $id, $updatedAt, $withCache, $withRequest)) {
			$key = $this->getKey($modelName, $id, $updatedAt, $withRequest);
			return $this->resourcesMap[$key];
		}

		return null;
	}

	/**
	 * Set the cached API resource
	 *
	 * @param array API resource data
	 * @param string model name
	 * @param int model id
	 * @param string updated at value
	 * @param boolean check laravel's Cache
	 * @param boolean attach the request to the cache
	 * @return mixed|null
	 */
	public function setApiResource($data, $modelName, $id, $updatedAt, $withCache = false, $withRequest = true) {
		$key = $this->getKey($modelName, $id, $updatedAt, $withRequest);
		$this->resourcesMap[$key] = $data;

		if ($withCache) {
			Cache::put($key, $data, 60);
		}
	}

	protected function getKey($modelName, $id, $updatedAt, $withRequest = false) {
		$authorization = '';

		if (
			$withRequest &&
			$this->request->headers->has('Authorization')
		) {
			$authorization = $this->request->headers->get('Authorization');
		}

		return md5($authorization . '_' . $modelName . '_' . $id . '_' . $updatedAt);
	}

}
