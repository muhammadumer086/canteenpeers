import { IStoreState } from '../store';
import { actionTypes } from '../actions';

export const quickLinksModalDisplay = () => (dispatch, getState: () => IStoreState) => {
	if (getState().quickLinksModal.quickLinksModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.QUICK_LINKS_MODAL_DISPLAY,
	});
}

export const quickLinksModalHide = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().quickLinksModal.quickLinksModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.QUICK_LINKS_MODAL_HIDE,
	});
}
