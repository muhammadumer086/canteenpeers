import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { connect } from 'react-redux';

import { IStoreState } from '../../redux/store';
import { ButtonLink } from '../general/ButtonLink';

interface IProps {
	country: string;
	dispatch(event: any): void;
}

const LandingHero: React.FC<IProps> = (props: IProps) => {
	const [landingHeroClasses, setLandingHeroClasses] = useState(
		'landing_hero',
	);
	useEffect(() => {
		const classes =
			props.country === 'AU'
				? 'landing_hero'
				: 'landing_hero landing_hero_nz';
		setLandingHeroClasses(classes);
	}, [1]);
	const { country } = props;

	const landingHeroText =
		country === 'AU'
			? 'Chat with people who get you.'
			: 'Chat with rangatahi who get you.';
	const landingHeroQoute =
		country === 'AU'
			? `"You're not alone. The Canteen Connect community is amazing." Keiran, Age 20`
			: `Canteen Connect is a welcoming community, free from judgment.” Leaton, Age 21, Auckland`;
	return (
		<section className={landingHeroClasses}>
			<div className="landing_hero-container">
				<h1 className="landing_hero-headline font--h1">
					Cancer changes everything
				</h1>
				<h3 className="landing_hero-subheader font--h4">
					{landingHeroText}
					<br />
					Get time out at our fun events.
					<br />
					Talk to a counsellor (it’s free!).
					<br />
					<br />
					{landingHeroQoute}
				</h3>

				<div className="landing_hero-buttons">
					<ButtonLink
						variant="contained"
						color="primary"
						className="landing_hero-cta"
						href="/auth/register?step=step-1"
						hrefAs="/auth/register/step-1"
					>
						Get Support
					</ButtonLink>
				</div>
			</div>

			<div className="landing_hero-container parents">
				<div className="landing_hero-parents">
					<span>
						We support parents too.
						<Link href="https://parents.canteenconnect.org/">
							<a target="blank">
								Join the community made by parents, for parents.
							</a>
						</Link>
					</span>
				</div>
			</div>

			{/* <div className="landing_hero-gradient landing_hero-gradient--top" />
			<div className="landing_hero-gradient landing_hero-gradient--bottom" /> */}

			{/* <video
				autoPlay
				loop
				className="landing_hero-video"
				muted
				plays-inline="true"
				poster={`${
					process.env.STATIC_PATH
				}/images/landing-page/landing-hero-image.webp`}
			>
				<source
					src={`${
						process.env.STATIC_PATH
					}/videos/canteen_peer_loop.mp4`}
					type="video/mp4"
				/>
			</video> */}
		</section>
	);
};

function mapStateToProps(state: IStoreState) {
	const { landingVideoModalDisplay } = state.landingVideoModal;

	return {
		landingVideoModalDisplay,
	};
}

export default connect(mapStateToProps)(LandingHero);
