import React from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';
import Link from 'next/link';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

import { IStoreState, apiError } from '../../js/redux/store';

import { ApiService } from '../../js/services';
import { IApiUser } from '../../js/interfaces';
import MyError from '../_error';
import Countdown from 'react-countdown';
import { ButtonLink } from '../../js/components/general/ButtonLink';

interface IProps {
	userData: IApiUser | null;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	resend: boolean;
	disabled: boolean;
	time: number;
}

const timeRendrer = ({ minutes, seconds, completed }) => {
	if (completed) {
		return <span>Resend Email Confimation</span>;
	}
	return <span>{`Resend Email( ${minutes} Min : ${seconds} Sec )`}</span>;
};

class RegisterSuccess extends React.Component<IProps, IState> {
	public constructor(props: IProps) {
		super(props);

		this.state = {
			submitting: false,
			resend: false,
			disabled: true,
			time: Date.now() + 2 * 60 * 1000,
		};

		this.resendVerification = this.resendVerification.bind(this);
	}

	public componentDidMount() {
		this.resendVerification();
	}

	public render() {
		if (
			!this.props.userData ||
			!this.props.userData.hasOwnProperty('first_name') ||
			!this.props.userData.hasOwnProperty('email')
		) {
			return <MyError statusCode={401} />;
		}
		const { time, disabled, submitting } = this.state;
		return (
			<div className="auth_wrapper theme--accent_dark auth_wrapper--with_header">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<div className="auth_page-header">
					<img
						className="auth_page-header-logo"
						src={`${process.env.STATIC_PATH}/images/logo-canteen-horizontal.png`}
					/>
				</div>
				<section className="auth">
					<header className="auth-header">
						<h1 className="auth-title font--h4">
							Hi{' '}
							{this.props.userData &&
								this.props.userData.username}
							, thanks for registering to Canteen Connect!
						</h1>
					</header>
					<div className="auth-content theme--main">
						{submitting && (
							<LinearProgress
								color="secondary"
								className="auth-content-progress"
							/>
						)}
						<div className="auth-content-text">
							<div className="font--h5">
								We just need you to confirm your email address.
							</div>
							<div>
								(we’ve sent an email to{' '}
								<u>
									{this.props.userData &&
										this.props.userData.email}
								</u>
								)
							</div>
							<div className="font--h5 hm-t32">
								After that you’re ready to go!
							</div>
						</div>
						<div className="auth-content-footer font--small">
							<div className="hm-b16">
								Didn’t receive the email?
							</div>
							<div>
								<Button
									variant="contained"
									color="primary"
									onClick={this.resendVerification}
									disabled={disabled || submitting}
								>
									<Countdown
										onComplete={() =>
											this.setState({ disabled: false })
										}
										key={Date.now()}
										autoStart={true}
										date={time}
										renderer={timeRendrer}
									/>
								</Button>
							</div>
						</div>
					</div>
					<footer className="auth-footer font--small">
						<div className="auth-footer-line-container">
							<div className="auth-footer-line"></div>
							<p>Or I want to</p>
							<div className="auth-footer-line"></div>
						</div>
						<div>
							<ButtonLink
								variant="outlined"
								color="inherit"
								className="auth-footer-button"
								href="/auth/verifybysms?step=step-1"
								hrefAs="/auth/verifybysms/step-1"
								disabled={!this.props.userData.phone}
							>
								verify via sms
							</ButtonLink>
						</div>
						Back to{' '}
						<Link href="/">
							<a>{process.env.APP_NAME}</a>
						</Link>
					</footer>
				</section>
			</div>
		);
	}

	protected resendVerification() {
		if (this.props.userData === null) {
			return;
		}
		this.setState(
			{
				submitting: true,
				resend: false,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/users/resend-verification', {
						email: this.props.userData.email,
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					})
					.then(() => {
						// Success
						this.setState({
							submitting: false,
							resend: true,
							time: Date.now() + 2 * 60 * 1000,
							disabled: true,
						});
					});
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(RegisterSuccess);
