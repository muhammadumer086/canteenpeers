import React, { useState } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { IApiSupport } from 'js/interfaces';
import LinearProgress from '@material-ui/core/LinearProgress';
import UserAvatar from '../general/UserAvatar';
import ButtonBase from '@material-ui/core/ButtonBase';

enum EFilter {
	ALL,
	LIKE,
	HUG,
	REPLY,
}

interface IProps {
	loading: boolean;
	supports: IApiSupport[];
	totalAll: number;
	totalLike: number;
	totalHug: number;
	totalReply: number;
	displayUser(item: IApiSupport): void;
}

export const SupportDetails: React.FC<IProps> = props => {
	const [filter, setFilter] = useState<EFilter>(EFilter.ALL);
	const tabs = [
		{
			label: 'All',
			value: EFilter.ALL,
			disabled: props.totalAll <= 0 || props.loading,
		},
		{
			label: 'I Get It',
			value: EFilter.LIKE,
			disabled: props.totalLike <= 0 || props.loading,
		},
		{
			label: 'Hug',
			value: EFilter.HUG,
			disabled: props.totalHug <= 0 || props.loading,
		},
		{
			label: 'Replied',
			value: EFilter.REPLY,
			disabled: props.totalReply <= 0 || props.loading,
		},
	];

	return (
		<div className="support_detail">
			<Tabs
				value={filter}
				onChange={handleChange}
				disabled={props.loading}
				className="support_detail-tabs"
			>
				{tabs.map(item => (
					<Tab
						className="support_detail-tab"
						key={item.value}
						{...item}
					/>
				))}
			</Tabs>
			{props.loading && (
				<div className="support_detail-progress">
					<LinearProgress />
				</div>
			)}
			<ul className="support_detail-list">
				{props.supports
					.filter(item => {
						return (
							filter === EFilter.ALL ||
							(filter === EFilter.LIKE && item.like) ||
							(filter === EFilter.HUG && item.hug) ||
							(filter === EFilter.REPLY && item.reply)
						);
					})
					.map(item => (
						<li key={item.user.id} className="support_detail-item">
							<div className="support_detail-item-profile">
								<UserAvatar user={item.user} />
							</div>
							<div className="support_detail-item-details">
								<div className="support_detail-item-details-name">
									<span className="theme-text--title">
										{item.user.full_name}
									</span>
								</div>
								<div className="support_detail-item-details-support theme-text--body_light">
									{getItemSupport(item)}
								</div>
							</div>
							<ButtonBase
								className="support_detail-item-button"
								focusRipple={true}
								onClick={() => props.displayUser(item)}
							/>
						</li>
					))}
			</ul>
		</div>
	);

	function getItemSupport(item: IApiSupport) {
		const result: string[] = [];
		if (item.like) {
			result.push('I Get It');
		}
		if (item.hug) {
			result.push('Hug');
		}
		if (item.reply) {
			result.push('Replied');
		}
		return result.join(', ');
	}

	function handleChange(_event: any, newValue: any) {
		setFilter(newValue as EFilter);
	}
};
