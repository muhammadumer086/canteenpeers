import { actionTypes } from '../actions';
import { EContactModalTypes } from '../../interfaces';

export interface  IStoreState {
	contactModalDisplay: boolean;
	contactModalType: EContactModalTypes;
}

export const contactModal = (state: IStoreState = {
	contactModalDisplay: false,
	contactModalType: EContactModalTypes.GENERAL,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.CONTACT_MODAL_DISPLAY:
			return Object.assign({}, state, {
				contactModalDisplay: true,
				contactModalType: action.contactModalType,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.CONTACT_MODAL_HIDE:
			return Object.assign({}, state, {
				contactModalDisplay: false,
			});
		default:
			return state;
	}
}
