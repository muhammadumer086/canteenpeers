import React from 'react';
import { connect } from 'react-redux';
import { IStoreState } from '../../../redux/store';
import { IApiUser } from 'js/interfaces';

interface IProps {
	userData: IApiUser;
}

const WelcomeModalScreen: React.StatelessComponent<IProps> = props => {
	const welcomeMessage = !!props.userData
		? `${props.userData.first_name}, welcome to Canteen Connect!`
		: `Welcome to Canteen Connect!`;

	return (
		<div className="modal_introduction-item">
			<div className="modal_introduction-item_image modal_introduction-item_image--mobile modal_introduction-item_image--step_one" />

			<div className="modal_introduction-item_content theme--main">
				<h4 className="font--h4 theme-title">{welcomeMessage}</h4>
				<p>
					We get that cancer changes everything. That’s why we created
					this community – you’re not alone!
				</p>
			</div>

			<div className="modal_introduction-item_image modal_introduction-item_image--desktop modal_introduction-item_image--step_one" />
		</div>
	);
};

const mapStateToProps = (state: IStoreState) => {
	const { userData } = state.user;

	return {
		userData,
	};
};

export default connect(mapStateToProps)(WelcomeModalScreen);
