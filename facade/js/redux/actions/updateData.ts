import { actionTypes } from '../actions';

export const updateData = () => dispatch => {
	return dispatch({
		type: actionTypes.UPDATE_DATA,
	});
};
