<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Situation;

use App\Http\Resources\Country as CountryResource;
use App\Http\Resources\HeardAbout as HeardAboutResource;
use App\Http\Resources\Language as LanguageResource;
use App\Http\Resources\LeavingReason as LeavingReasonResource;
use App\Http\Resources\Situation as SituationResource;
use App\Http\Resources\PersonalSetting as PersonalSettingResource;
use UserPermissionHelper;

class User extends JsonResource {
	use \App\Traits\AuthUser;

	public $forceAuth = false;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();

		if (
			$apiStore->hasApiResource(
				'user',
				$this->id,
				$this->updated_at,
				false
			)
		) {
			return $apiStore->getApiResource(
				'user',
				$this->id,
				$this->updated_at,
				false
			);
		}

		$params = [
			'id',
			'email',
			'username',
			'self_identify',
			'active_discussions_count',
			'blogs_count',
			'full_name',
			'avatar_url',
			'about',
			'is_banned',
			'role_names',
			'updated_at',
			'deleted_at',
			'inactive',
			'number_children',
			'country_slug',
			'is_graduate',
			'is_mobile_verified',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		// Add the additional values
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all","users_management_read"]);


		if ($authUser) {
			$result['is_blocked'] = $this->isBlocked($authUser);
		}

		if (
			$this->allowDisplayGender() ||
			($authUser && $authUser->id === $this->id) ||
			($authUser && $authUser->hasRoleName('admin')) ||
			$this->forceAuth || $userHasPermission
		) {
			$result['gender'] = $this->gender;
		} else {
			$result['gender'] = null;
		}

		if (
			$this->allowDisplayCancerType() ||
			($authUser && $authUser->id === $this->id) ||
			($authUser && $authUser->hasRoleName('admin')) ||
			$this->forceAuth  || $userHasPermission
		) {
			// Add the additional relationships
			$result['situations'] = count($this->situations)
				? SituationResource::collection($this->situations)->toArray(
					$request
				)
				: [];
			// Add the situations date if available
			if (sizeof((array) $result['situations'])) {
				foreach ($result['situations'] as &$singleSituation) {
					$singleSituation['date'] = $this->situations()
						->where('situation_id', $singleSituation['id'])
						->value('date');
				}
			}
		} else {
			$formattedSituationIds = [];
			// $situations = count($this->situations)
			// 	? Situation::collection($this->situations)->toArray($request)
			// 	: [];

			if (count($this->situations)) {
				foreach ($this->situations as $situation) {
					if ($situation->parentSituation()->exists()) {
						$formattedSituationIds[] =
							$situation->parentSituation->id;
					} else {
						$formattedSituationIds[] = $situation->id;
					}
				}
			}

			$result['situations'] = SituationResource::collection(
				Situation::whereIn('id', $formattedSituationIds)->get()
			)->toArray($request);
		}

		$result['allow_direct_messages'] = $this->canMessage($authUser);

		$result['age'] = $this->getAge();

		if (
			($authUser &&
				($authUser->id === $this->id ||
					$authUser->hasRoleName('admin'))) ||
			$this->forceAuth  || $userHasPermission
		) {
			$additionalParams = [
				'email',
				'first_name',
				'last_name',
				'location',
				'state',
				'phone',
				'indigenous_australian',
				'role_names',
				'likes_count',
				'hugs_count',
				'following_count',
				'saved_resources_count',
				'messages_count',
				'intercom_hash',
				'guardian_details',
				'age',
				'underage',
				'fcm_token',
				'latitude',
				'longitude',
				'email_verified_at',
			];
			// Add the authenticated values
			foreach ($additionalParams as $key) {
				$result[$key] = $this->$key;
			}

			// Enforce the formatting of location data
			$result['location_data'] = is_string($this->location_data)
				? $this->location_data
				: \json_encode($this->location_data);

			$result['dob'] = $this->dob ? $this->dob->format('Y-m-d') : null;
			$result['country_of_birth'] = $this->countryOfBirth
				? new CountryResource($this->countryOfBirth)
				: null;
			$result['languages'] = count($this->languages)
				? LanguageResource::collection($this->languages)
				: [];
			$result['heard_abouts'] = count($this->heardAbouts)
				? HeardAboutResource::collection($this->heardAbouts)
				: [];
			$result['personal_settings'] = count($this->personalSettings)
				? PersonalSettingResource::collection($this->personalSettings)
				: [];
			$result['has_conversations'] = $this->hasConversations();

			if ($authUser && $authUser->hasRoleName('admin') || $userHasPermission) {
				$adminParams = [
					'verified',
					'is_mobile_verified',
					'ban',
					'last_login',
					'last_login_ip',
					'total_logins',
					'deleted_at',
				];
				// Add the admin values
				foreach ($adminParams as $key) {
					$result[$key] = $this->$key;
				}

				$result['leaving_reasons'] = count($this->leavingReasons)
					? LeavingReasonResource::collection($this->leavingReasons)
					: [];
			}


		}

		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);
		$result['deleted_at'] = UserPermissionHelper::formatCarbonDate($result['deleted_at']);

		$apiStore->setApiResource(
			$result,
			'user',
			$this->id,
			$this->updated_at,
			false
		);


		return $result;
	}
}
