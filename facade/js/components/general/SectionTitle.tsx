import React from 'react';
import ReactSVG from 'react-svg';

interface IProps {
	label: string;
	iconSlug: string;
}

const SectionTitle: React.StatelessComponent<IProps> = props => {
	const iconUrl = `${process.env.STATIC_PATH}/icons/${props.iconSlug}.svg`;

	return (
		<div className="section_title">
			<div className="section_title-icon">
				<ReactSVG path={iconUrl} />
			</div>

			<div className="section_title-label">
				<span>{props.label}</span>
			</div>
		</div>
	);
};

export default SectionTitle;
