<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$permissions = [

			'users_management_read',
			'users_management_add',
			'users_management_delete',
			'users_management_edit',
			'users_management_all',

			'all_users_activities_read',
			'all_users_activities_add',
			'all_users_activities_delete',
			'all_users_activities_edit',
			'all_users_activities_all',

			'topic_management_read',
			'topic_management_add',
			'topic_management_delete',
			'topic_management_edit',
			'topic_management_all',

			'reported_discussions_read',
			'reported_discussions_add',
			'reported_discussions_delete',
			'reported_discussions_edit',
			'reported_discussions_all',

			'events_management_read',
			'events_management_add',
			'events_management_delete',
			'events_management_edit',
			'events_management_all',

			'alert_management_read',
			'alert_management_add',
			'alert_management_delete',
			'alert_management_edit',
			'alert_management_all',

			'staff_management_read',
			'staff_management_add',
			'staff_management_delete',
			'staff_management_edit',
			'staff_management_all',

			'graduate_users_management_read',
			'graduate_users_management_add',
			'graduate_users_management_delete',
			'users_management_edit',
			'graduate_users_management_all'

		];

		foreach ($permissions as $permissionName) {
			if (!Permission::where('name', '=', $permissionName)->exists()) {
				Permission::create(['name' => $permissionName]);
			}
		}

    }
}
