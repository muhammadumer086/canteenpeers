<?php

namespace App\Filters\Event;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class StateFilter extends FilterAbstract
{
    /**
     * Mappings for database values.
     *
     * @return array
     */
    public function mappings()
    {
        return [];
    }

    /**
     * Filter with builder queries.
     *
     * @param  string $access
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function filter(Builder $builder, $value)
    {
        return $builder->whereHas('state', function (Builder $builder) use ($value) {
			$builder->whereIn('slug', $value);
		});
    }
}
