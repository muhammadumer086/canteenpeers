import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { IApiBlog } from '../../interfaces';

export const newBlogModalDisplay = (
	blogData?: IApiBlog,
	predefinedTopic?: string,
) => (dispatch, getState: () => IStoreState) => {
	if (getState().newBlogModal.newBlogModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.NEW_BLOG_MODAL_DISPLAY,
		blogData: blogData ? blogData : null,
		predefinedTopic: predefinedTopic ? predefinedTopic : null,
	});
};

export const newBlogModalHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().newBlogModal.newBlogModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.NEW_BLOG_MODAL_HIDE,
	});
};
