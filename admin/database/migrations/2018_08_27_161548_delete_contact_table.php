<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteContactTable extends Migration {
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up() {
		Schema::dropIfExists('contact');
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down() {
		Schema::create('contact', function (Blueprint $table) {
			$table->increments('id');
			$table->string('enquiry_type')->nullable();
			$table->text('message');
			$table->unsignedInteger('user_id')->nullable();
			$table->string('email')->nullable();
			$table->timestamps();

			$table->foreign('user_id')
			->references('id')->on('users')
			->onDelete('cascade');
		});
	}
}
