<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Filters\Activity\UserActivityFilters;

class UserActivity extends Model {
	use \BinaryCabin\LaravelUUID\Traits\HasUUID;

	protected $uuidFieldName = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'user_activity';

	protected $fillable = ['id'];

	/**
	 * All of the relationships to be touched.
	 *
	 * @var array
	 */
	protected $touches = ['user'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'content' => 'array',
	];

	public function scopeFilter(
		Builder $builder,
		$request,
		array $filters = []
	) {
		return (new UserActivityFilters($request))
			->add($filters)
			->filter($builder);
	}

	/**
	 * The users that belong to this activity
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}
	/**
	 * The model related to the activity
	 */
	public function activity() {
		return $this->morphTo();
	}

	public function getActivityModelNameAttribute() {
		$modelSplit = explode('\\', $this->activity_type);
		if (sizeof($modelSplit)) {
			return strtolower($modelSplit[sizeof($modelSplit) - 1]);
		}
		return null;
	}
}
