import React from 'react';
import { connect } from 'react-redux';
import ClearIcon from '@material-ui/icons/Clear';

import {
	IStoreState,
	apiSuccessDismiss
} from '../../redux/store';

interface IProps {
	apiSuccess: string[];
	apiSuccessShowing: boolean;
	dispatch(action: any): void;
}

const ApiSuccessBanner: React.FC<IProps> = (props: IProps) => {
	const classes = [
		'api_banner api_banner--success',
	];

	if (props.apiSuccessShowing) {
		classes.push('api_banner--active');
	}

	function dismiss() {
		props.dispatch(apiSuccessDismiss());
	}

	return (
		<div className="api_banner_wrapper">
			<div className={classes.join(' ')}>
				<div className="api_banner-content">
					{
						props.apiSuccess.map((error, index) => {
							return (
								<p key={index}>{error}</p>
							);
						})
					}
				</div>
				<button onClick={dismiss} className="close_button" title="Dismiss">
					<ClearIcon className="close_button-icon"/>
				</button>
			</div>
		</div>
	);
}

function mapStateToProps(state: IStoreState) {
	const {
		apiSuccess,
		apiSuccessShowing,
	} = state.apiSuccess;
	return {
		apiSuccess,
		apiSuccessShowing
	};
}

export default connect(mapStateToProps)(ApiSuccessBanner);
