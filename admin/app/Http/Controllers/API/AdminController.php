<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use League\Csv\Writer;

use App\Situation;
use App\User;
use App\UserActivity;
use UserPermissionHelper;

use App\Http\Resources\UserActivityCollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\User as UserResource;

class AdminController extends Controller {
	const PAGINATION_SIZE = 20;

	public function usersList(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_read","users_management_all"]);
		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$users = $this->queryUsers($request)->paginate(self::PAGINATION_SIZE);

		return new UserCollection($users);
	}

	/**
	 * Display a listing of all the activity
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function activities(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["all_users_activities_read","all_users_activities_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission)
			{
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$activities = $this->queryActivities($request)->paginate(
			self::PAGINATION_SIZE
		);

		return new UserActivityCollection($activities);
	}

	/**
	 * Display a listing of all the users over 26
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getGraduateUsers(Request $request)
	{
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["graduate_users_management_read","graduate_users_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$users = User::whereRaw(('floor(DATEDIFF(CURDATE(),dob) /365) > 26 && role_names="user"'))->paginate(
			self::PAGINATION_SIZE
		);
		return new UserCollection($users);
	}


	/**
	 * Display a listing of all the activity by a user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function userActivities(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["all_users_activities_read","all_users_activities_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();

		$activitiesQuery = $this->queryActivities($request, $user);

		return new UserActivityCollection(
			$activitiesQuery->paginate(self::PAGINATION_SIZE)
		);
	}

	/**
	 * Generate Users Report CSV
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function userActivityExport(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["all_users_activities_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					401
				);
			}
		}

		$usersQuery = $this->queryUsers($request);

		$header = [
			'id',
			'first name',
			'last name',
			'username',
			'email',
			'dob',
			'postcode',
			'state',
			'country',
			'latitude',
			'longitude',
			'phone',
			'gender',
			'self identify',
			'indigenous australian',
			'situation',
			'number children',
			'last login',
			'total logins',
			'active discussions',
			'I get it',
			'Hugs',
			'followed discussions',
			'total blogs',
			'saved resources',
			'created at',
			'migration id',
			'inactive',
			'deleted at',
			'allow contact research opportunities',
			'is_email_verified',
			'is_mobile_verified',
			'email_verified_at',
			'mobile_verified_at'
		];
		$tmpFilePath = tempnam(
			sys_get_temp_dir(),
			'users-report-' . date('Ymdhis')
		);

		$csv = Writer::createFromPath($tmpFilePath, 'w+');
		$csv->insertOne($header);

		$usersQuery->chunk(100, function ($users) use ($csv) {
			foreach ($users as $user) {
				$data = [];
				$data[] = $user->id;
				$data[] = $user->first_name;
				$data[] = $user->last_name;
				$data[] = $user->username;
				$data[] = $user->email;
				$data[] = $user->dob ? $user->dob->format('Y-m-d') : null;
				$data[] = $user->location;
				$data[] = $user->state;
				$data[] = $user->country_slug;
				$data[] = $user->latitude;
				$data[] = $user->longitude;
				$data[] = $user->phone;
				$data[] = $user->gender;
				$data[] =
					$user->gender === 'other' ? $user->self_identify : null;
				$data[] = $this->formatBoolean($user, 'indigenous_australian');
				$data[] = $this->formatSituations($user);
				$data[] = $user->number_children;
				$data[] = $user->last_login;
				$data[] = $user->total_logins;
				$data[] = $user->active_discussions_count;
				$data[] = $user->likes_count;
				$data[] = $user->hugs_count;
				$data[] = $user->following_count;
				$data[] = $user->blogs_count;
				$data[] = $user->saved_resources_count;
				$data[] = $user->created_at;
				$data[] = $user->migration_id;
				$data[] = $user->inactive;
				$data[] = $user->deleted_at;
				$data[] = $user->allowResearchContact() ? 'yes' : 'no';
				$data[] = $user->verified;
				$data[] = $user->is_mobile_verified;
				$data[] = $user->email_verified_at;
				$data[] = $user->mobile_verified_at;
				// Insert the record
				$csv->insertOne($data);
			}
		});

		$title = 'canteen-users-report-' . date('Ymd-Hi') . '.csv';

		return response()
			->download($tmpFilePath, $title, [
				'Content-Type' => 'text/csv;charset=UTF-8',
				'Cache-Control' => 'private',
				'Pragma' => 'private',
				'Expires' => 'Sat, 1 Jan 2000 00:00:00 GTM',
			])
			->deleteFileAfterSend(true);
	}

	/**
	 * Generate a report of all the activity
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function activitiesExport(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["all_users_activities_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$activitiesQuery = $this->queryActivities($request);

		return $this->generateActivitiesCsv($activitiesQuery);
	}

	/**
	 * Generate a report of all the activity by a user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function userActivitiesExport(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["all_users_activities_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();

		$activitiesQuery = $this->queryActivities($request, $user);

		return $this->generateActivitiesCsv($activitiesQuery, $username);
	}

	public function restoreUser(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all","users_management_edit"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::withTrashed()
			->where('username', $username)
			->firstOrFail();
		$user->restore();
		$user->inactive = false;
		$user->save();

		$userResource = new UserResource($user);

		return response()->json([
			'user' => $userResource->toArray($request),
		]);
	}

	protected function generateActivitiesCsv($activitiesQuery, $username = '') {
		$header = [
			'id',
			'first name',
			'last name',
			'username',
			'email',
			'activity type',
			'entity',
			'title',
			'posted in situation(s)',
			'posted in topics',
			'post views',
			'I get it',
			'Hugs',
			'replies',
			'additional data',
			'created at',
		];
		$tmpFilePath = tempnam(
			sys_get_temp_dir(),
			'activities-report-' . $username . date('Ymdhis')
		);

		$csv = Writer::createFromPath($tmpFilePath, 'w+');
		$csv->insertOne($header);

		$activitiesQuery->chunk(100, function ($activities) use ($csv) {
			foreach ($activities as $activity) {
				$data = [];
				$data[] = $activity->id;
				$data[] = $activity->user->first_name;
				$data[] = $activity->user->last_name;
				$data[] = $activity->user->username;
				$data[] = $activity->user->email;
				$data[] = $activity->type;
				$data[] = $activity->activity_model_name;
				$data[] = $this->formatTitle($activity);
				$data[] = $this->formatActivitySituations($activity);
				$data[] = $this->formatActivityTopic($activity);
				$data[] = $this->formatActivityViewsCount($activity);
				$data[] = $this->formatActivityGetIt($activity);
				$data[] = $this->formatActivityHugs($activity);
				$data[] = $this->formatActivityReplies($activity);
				$data[] = $activity->content
					? json_encode($activity->content)
					: null;
				$data[] = $activity->created_at;
				// Insert the record
				$csv->insertOne($data);
			}
		});

		$titleAddition = $username ? "{$username}-" : null;

		$title =
			"canteen-{$titleAddition}activities-" . date('Ymd-Hi') . '.csv';

		return response()
			->download($tmpFilePath, $title, [
				'Content-Type' => 'text/csv;charset=UTF-8',
				'Cache-Control' => 'private',
				'Pragma' => 'private',
				'Expires' => 'Sat, 1 Jan 2000 00:00:00 GTM',
			])
			->deleteFileAfterSend(true);
	}

	protected function formatBoolean($object, $value) {
		if ($object->$value) {
			return 'yes';
		} elseif ($object->$value === false) {
			return 'no';
		}
		return 'not defined';
	}

	protected function formatSituations(User $user) {
		$result = [];
		foreach ($user->situations as $situation) {
			$situationData = $situation->name;
			if ($situation->cancer_type) {
				$situationData .= " ({$situation->cancer_type})";
			}
			$result[] = $situationData;
		}
		return implode(', ', $result);
	}

	protected function formatTitle(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if (
				$activity->activity_model_name === 'blog' ||
				$activity->activity_model_name === 'discussion' ||
				$activity->activity_model_name === 'resource'
			) {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				return $activityModel->title;
			}
		}
		return null;
	}

	protected function formatActivitySituations(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if (
				$activity->activity_model_name === 'blog' ||
				$activity->activity_model_name === 'discussion' ||
				$activity->activity_model_name === 'resource'
			) {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				$situations = $activityModel->situations;
				$situationsData = [];
				foreach ($situations as $singleSituation) {
					$situationsData[] = $singleSituation->name;
				}

				return implode(', ', $situationsData);
			}
		}
		return null;
	}

	protected function formatActivityTopic(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if (
				$activity->activity_model_name === 'blog' ||
				$activity->activity_model_name === 'discussion' ||
				$activity->activity_model_name === 'resource'
			) {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				$topic = $activityModel->topic;
				if ($topic) {
					return $topic->title;
				}
			}
		}
		return null;
	}

	protected function formatActivityViewsCount(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if (
				$activity->activity_model_name === 'blog' ||
				$activity->activity_model_name === 'discussion' ||
				$activity->activity_model_name === 'resource'
			) {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				return $activityModel->views_count;
			}
		}
		return null;
	}

	protected function formatActivityGetIt(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if ($activity->activity_model_name === 'discussion') {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				return $activityModel->likes_count;
			}
		}
		return null;
	}

	protected function formatActivityHugs(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if ($activity->activity_model_name === 'discussion') {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				return $activityModel->hugs_count;
			}
		}
		return null;
	}

	protected function formatActivityReplies(UserActivity $activity) {
		if (
			$activity
				->activity()
				->withTrashed()
				->exists()
		) {
			if ($activity->activity_model_name === 'discussion') {
				$activityModel = $activity
					->activity()
					->withTrashed()
					->first();
				return $activityModel->replies_count;
			}
		}
		return null;
	}

	protected function queryUsers(Request $request) {
		$usersQuery = User::whereNotNull('id');

		$simpleQueryParams = ['id', 'gender'];

		foreach ($simpleQueryParams as $queryParam) {
			if (
				$request->has($queryParam) &&
				!empty($request->input($queryParam))
			) {
				$usersQuery->whereIn($queryParam, $request->input($queryParam));
			}
		}

		if ($request->has('user_id') && !empty($request->input('user_id'))) {
			$usersQuery->where('id', $request->input('user_id'));
		}

		// Special case for NZ
		if ($request->has('state') && !empty($request->input('state'))) {
			$input = $request->input('state');

			if (in_array('NZ', $input)) {
				// Split NZ from the rest of the input
				$key = array_search('NZ', $input);
				unset($input[$key]);

				$usersQuery->where(function ($query) use ($input) {
					$query
						->where('country_slug', 'NZ')
						->orWhereIn('state', $input);
				});
			} else {
				// Process the states normally
				$usersQuery->whereIn('state', $input);
			}
		}

		if (
			$request->has('lastLoginFrom') &&
			$request->input('lastLoginFrom')
		) {
			$usersQuery->where(
				'last_login',
				'>',
				$request->input('lastLoginFrom')
			);
		}

		if ($request->has('lastLoginTo') && $request->input('lastLoginTo')) {
			$usersQuery->where(
				'last_login',
				'<',
				$request->input('lastLoginTo')
			);
		}

		if (
			$request->has('userCreatedFrom') &&
			$request->input('userCreatedFrom')
		) {
			$usersQuery->where(
				'created_at',
				'>=',
				$request->input('userCreatedFrom')
			);
		}

		if ($request->has('userCreatedTo') &&
			$request->input('userCreatedTo')
		) {
			$usersQuery->where(
				'created_at',
				'<=',
				$request->input('userCreatedTo')
			);
		}



		if ($request->has('age')) {
			$usersQuery->whereNotNull('dob');
			// Generate the age array
			$ages = [];

			foreach ($request->input('age') as $age) {
				if ($age === 'under12') {
					for ($i = 0; $i < 12; $i++) {
						$ages[] = $i;
					}
				} else {
					$range = explode('-', $age);
					for ($i = $range[0]; $i <= $range[1]; $i++) {
						$ages[] = $i;
					}
				}
			}

			$usersQuery->whereIn(DB::raw('YEAR(CURDATE()) - YEAR(dob)'), $ages);
		}

		if ($request->has('situations') || $request->has('cancerTypes')) {
			$situationIds = [];

			if ($request->has('situations')) {
				if ($request->has('cancerTypes')) {
					// Build all the situations slugs
					$situationSlugs = [];
					$cancerSituationsIds = Situation::whereIn(
						'name',
						$request->input('situations')
					)
						->whereIn('cancer_type', $request->input('cancerTypes'))
						->pluck('id');
				} else {
					$cancerSituationsIds = Situation::whereIn(
						'name',
						$request->input('situations')
					)->pluck('id');
				}

				foreach ($cancerSituationsIds as $situationId) {
					$situationIds[] = $situationId;
				}
			} elseif ($request->has('cancerTypes')) {
				// Get all the situations with the corresponding cancer types
				$cancerSituationsIds = Situation::whereIn(
					'cancer_type',
					$request->input('cancerTypes')
				)->pluck('id');

				foreach ($cancerSituationsIds as $situationId) {
					$situationIds[] = $situationId;
				}
			}

			$usersQuery->whereIn(
				'id',
				DB::table('situation_user')
					->whereIn('situation_id', $situationIds)
					->pluck('user_id')
			);


		}

		if ($request->has('role')) {
			$usersQuery->role($request->input('role'));
		}

		if (
			$request->has('excludeAdmin') &&
			$request->input('excludeAdmin') === 'true'
		) {
			$usersQuery->role('user');
		}

		if (
			$request->has('excludeNotVerified') &&
			$request->input('excludeNotVerified') === 'true'
		) {
			$usersQuery->where(function ($query)  {
				$query
					->where('verified', true)
					->orWhere('is_mobile_verified', true);
			});
		}

		if (
			$request->has('includeInactive') &&
			$request->input('includeInactive') === 'true'
		) {
			$usersQuery->withTrashed();
		}

		if($request->has('is_mobile_verified')) {
			$usersQuery->where('is_mobile_verified', $request->has('is_mobile_verified'));
		}

		if($request->has('is_email_verified')) {
			$usersQuery->where('verified', $request->has('is_email_verified'));
		}

		if ($request->has('verified')){
			if($request->input('verified') == true) {
				$usersQuery->where(function ($query)  {
					$query
						->where('verified', true)
						->orWhere('is_mobile_verified', true);
				});
			}
			else
			{
				$usersQuery->where(function ($query)  {
					$query
						->whereNull('verified')->orWhere('verified', false)
						->whereNull('is_mobile_verified')->orWhere('is_mobile_verified', false);
				});
			}
		}

		// Verification Date Check
		if (
			$request->has('userVerifiedFrom') &&
			$request->input('userVerifiedFrom') &&
			$request->has('userVerifiedTo') &&
			$request->input('userVerifiedTo')
		) {
			$userCreatedFrom = Carbon::parse($request->input('userVerifiedFrom'));
			$userCreatedTo = Carbon::parse($request->input('userVerifiedTo'));
			$usersQuery->where(function ($query) use ($userCreatedFrom, $userCreatedTo) {
				$query
					->whereBetween('email_verified_at', [Carbon::parse($userCreatedFrom->startOfDay()->toDateTimeString()), Carbon::parse($userCreatedTo)->endOfDay()->toDateTimeString()])
					->orWhereBetween('mobile_verified_at', [Carbon::parse($userCreatedFrom->startOfDay()->toDateTimeString(),'Australia/Sydney'), Carbon::parse($userCreatedTo)->endOfDay()->toDateTimeString()]);
			});
		}
		return $usersQuery->filter($request);
	}

	protected function queryActivities(Request $request, $user = null) {
		if ($user) {
			$activitiesQuery = $user->userActivities();
		} else {
			$activitiesQuery = UserActivity::whereNotNull('id');
				//->whereNotIn('type', ['User Login', 'Blog Deleted']);


			// Check if performing users query
			if (
				$request->has('name') ||
				$request->has('username') ||
				$request->has('email') ||
				$request->has('age') ||
				$request->has('gender') ||
				$request->has('situations') ||
				$request->has('verified') ||
				$request->has('cancerTypes') ||
				$request->has('state') ||
				$request->has('excludeAdmin')
			) {
				$usersQuery = $this->queryUsers($request);
				$activitiesQuery->whereIn('user_id', $usersQuery->pluck('id'));
			}
		}

		$simpleQueryParams = ['type'];

		foreach ($simpleQueryParams as $queryParam) {
			if (
				$request->has($queryParam) &&
				!empty($request->input($queryParam))
			) {
				$activitiesQuery->whereIn(
					$queryParam,
					$request->input($queryParam)
				);
			}
		}

		if ($request->has('from')) {
			$activitiesQuery->where('created_at', '>=', $request->input('from'));
		}

		if ($request->has('to')) {
			$activitiesQuery->where('created_at', '<=', $request->input('to'));
		}

		if ($request->has('platformFilter')) {
			$activitiesQuery->where(
				'platform',
				$request->input('platformFilter')
			);
		}

		return $activitiesQuery->filter($request);
	}
}
