import React from 'react';
import Dropzone from 'react-dropzone';
import { connect } from 'react-redux';
import { Fab, Icon, CircularProgress } from '@material-ui/core';
import { ApiService, withServices } from '../../../js/services';
import { apiError, IStoreState } from './../../redux/store';
import { IApiBlogImage } from '../../../js/interfaces';

interface IProps {
	id?: string;
	values: any;
	dropzoneClasses: string[];
	apiUrl: string;
	apiService: ApiService;
	label?: string;
	max_size?:number;
	dispatch(action: any): void;
	setFieldValue(key: string, validate: any): void;
}

interface IState {
	headerImageUploading: boolean;
}

class ImageDropzoneFormik extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			headerImageUploading: false,
		};

		this.handleImageUpload = this.handleImageUpload.bind(this);
	}

	public render() {
		let contentClasses='discussion_modal-content';
		const errorIndex=this.props.dropzoneClasses.findIndex(c=>c==='discussion_modal-content-error');
		if(errorIndex>=0)
		{
			contentClasses=contentClasses+" discussion_modal-content-error";
		}
		return (
			<Dropzone
			
				onDrop={files => {
					this.handleImageUpload(files, this.props.setFieldValue);
				}}
				multiple={false}
				accept="image/jpeg, image/png, image/gif"
				className={this.props.dropzoneClasses.join(' ')}
				activeClassName="discussion_modal-dropzone--active"
				rejectClassName="discussion_modal-dropzone--reject"
				disabled={this.state.headerImageUploading}
			>
				<div className={contentClasses}	id={this.props.id}>
					{'feature_image' in this.props.values &&
					this.props.values.feature_image ? (
						<div
							className="discussion_modal-content-image"
							style={{
								backgroundImage: `url(${this.props.values.feature_image.url})`,
							}}
						>
							<Fab
								color="secondary"
								aria-label="Remove"
								className="discussion_modal-content-image-remove"
								onClick={evt => {
									evt.preventDefault();
									this.props.setFieldValue(
										'feature_image',
										null,
									);
									this.props.setFieldValue('image_id', null);
								}}
							>
								<Icon>close</Icon>
							</Fab>
						</div>
					) : (
						<div>
							<div className="discussion_modal-content-text">
								{!!this.props.label
									? this.props.label
									: 'Set the feature image (optional)'}
							</div>
							<div className="theme-text--body">
								<span className="theme-text--underline">
									Click to upload your file
								</span>{' '}
								or drag your image here
								<br />
								Recommended size: 1640x910px
								<br />
								Max {this.props.max_size?this.props.max_size: 4}MB
							</div>
						</div>
					)}
					{this.state.headerImageUploading && (
						<CircularProgress size={15} />
					)}
				</div>
			</Dropzone>
		);
	}

	protected handleImageUpload(
		files: File[],
		setFieldValue: (fieldName: string, value: any) => void,
	) {
		if (files.length && !this.state.headerImageUploading) {
			this.setState(
				{
					headerImageUploading: true,
				},
				() => {
					const params = new FormData();
					params.append('image', files[0], files[0].name);

					this.props.apiService
						.queryPOSTFile(this.props.apiUrl, params, 'image')
						// TODO: Set correct interface
						.then((image: IApiBlogImage) => {
							setFieldValue('feature_image', {
								dimensions: {
									width: image.width,
									height: image.height,
								},
								alt: null,
								copyright: null,
								url: image.image_url,
							});
							setFieldValue('image_id', image.id);
							this.setState({
								headerImageUploading: false,
							});
						})
						.catch(error => {
							let errorMessage :string=error.message;
							errorMessage=errorMessage.trim();
							if(error.message==='The image must be below 4000 pixels width and height')
							{
								this.props.dispatch(apiError(['Image resolution is too large']));
							}
							else{
								this.props.dispatch(apiError([error.message]));
							}
							this.setState({
								headerImageUploading: false,
							});
						});
				},
			);
		}
	}
}
const mapStateToProps = (_state: IStoreState) => {
	return {};
};

export default connect(mapStateToProps)(withServices(ImageDropzoneFormik));
