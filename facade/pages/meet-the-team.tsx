import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import ReactSVG from 'react-svg';
import { connect } from 'react-redux';
import { IStoreState, apiError } from '../js/redux/store';
import { withRouter, SingletonRouter } from 'next/router';
import { ApiService } from '../js/services';

import { Button, Drawer } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import HeroHeading from '../js/components/general/HeroHeading';
import FixedNavgationBarTabs from '../js/components/general/FixedNavgationBarTabs';
import StaffTilesList from '../js/components/meet-the-team/StaffTilesList';
import StaffProfile from '../js/components/meet-the-team/StaffProfile';

import { GTMDataLayer } from '../js/helpers/dataLayer';

import { IApiStaff, IApiTeam, IApiUser } from '../js/interfaces';

 

interface IProps {
	staffData: IStaffRequestData;
	teamData: {
		data: {
			teams: IApiTeam[];
		};
	};
	queryType: string | null;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	apiService: ApiService;
	router?: SingletonRouter;
	dispatch(action: any): void;
}

interface IStaffRequestData {
	data: {
		staffs: IApiStaff[];
	};
	links: {
		first: string;
		last: string;
		prev: string | null;
		next: string | null;
	};
	meta: {
		current_page: number;
		from: number;
		last_page: number;
		path: string;
		per_page: number;
		to: number;
		total: number;
	};
}

interface IState {
	isLoading: boolean;
	staffData: IApiStaff[];
	teamData: IApiTeam[];
	isDrawerActive: boolean;
	activeTab: string;
	activeStaffMember: IApiStaff | null;
}

class MeetTheTeam extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ query, apiService }) {
		const params: any = {};
		let queryType: string | null = null;

		if ('team' in query) {
			params['team'] = query.team;
			queryType = query.team;
		}

		const url = buildUrl(null, {
			path: '/api/staff',
			queryParams: params,
		});

		const staffData = await (apiService as ApiService).queryGET(url);

		const teamData = await (apiService as ApiService).queryGET(
			'/api/teams',
		);

		return {
			staffData,
			teamData,
			queryType,
		};
	}

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			staffData: this.props.staffData.data.staffs,
			teamData: this.props.teamData.data.teams,
			activeTab: this.props.queryType ? this.props.queryType : 'online',
			isDrawerActive: false,
			activeStaffMember: null,
		};

		this.handleTabChange = this.handleTabChange.bind(this);
		this.toggleStaffDrawer = this.toggleStaffDrawer.bind(this);
		this.setStaffData = this.setStaffData.bind(this);
		this.loadData = this.loadData.bind(this);
	}

	public render() {
		// const pageTitle = 'Meet The Team | Canteen Connect';

		const url = buildUrl(process.env.APP_URL, {
			path: `/meet-the-team/`,
		});

		const featuredImageUrl = `${
			process.env.STATIC_PATH
		}/images/og-facebook-default.jpg`;

		const strippedTabData = this.state.teamData.map(team => {
			return {
				label: team.name,
				value: team.slug,
			};
		});
		// const seoDescription =
		// 	'Meet the team that work here at Canteen Connect. Register now to connect with other Australian teenagers &amp; get help with your situation.';

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:image"
						property="og:image"
						content={featuredImageUrl}
					/>
					<meta name="og:url" property="og:url" content={url} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
				
					<meta name="og:type" property="og:type" content="article" />

					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/> 
				</Head>
				<LayoutDashboard>
					<header className="hero theme--accent">
						<HeroHeading title="Meet the team" />

						<span className="hero-actions">
							{this.props.userAuthenticated && (
								<Button
									variant="contained"
									color="primary"
									className="consellor_chat"
									href={`mailto:${
										process.env.INTERCOM_APP_ID
									}@intercom-mail.com`}
								>
									Chat to a counsellor{' '}
									<ReactSVG
										path={`${
											process.env.STATIC_PATH
										}/icons/plus.svg`}
									/>
								</Button>
							)}
						</span>
					</header>

					<FixedNavgationBarTabs
						tabData={strippedTabData}
						activeTab={this.state.activeTab}
						handleChange={this.handleTabChange}
					/>

					<StaffTilesList
						isLoading={this.state.isLoading}
						staffData={this.state.staffData}
						onClick={this.setStaffData}
					/>
				</LayoutDashboard>

				<Drawer
					anchor="right"
					open={this.state.isDrawerActive}
					onClose={this.toggleStaffDrawer}
				>
					<div className="user_panel theme--main">
						<StaffProfile
							staffData={this.state.activeStaffMember}
						/>
					</div>
					<button
						onClick={this.toggleStaffDrawer}
						className="close_button close_button--primary"
						title="Dismiss"
					>
						<ClearIcon className="close_button-icon" />
					</button>
				</Drawer>
			</div>
		);
	}

	protected handleTabChange(_event, activeTab: string) {
		this.setState(
			{
				activeTab,
			},
			() => {
				this.loadData();
			},
		);
	}

	protected toggleStaffDrawer() {
		this.setState({
			isDrawerActive: !this.state.isDrawerActive,
		});
	}

	protected setStaffData(activeStaffMember: IApiStaff | null = null) {
		this.setState(
			{
				activeStaffMember,
			},
			() => {
				this.toggleStaffDrawer();
			},
		);
	}

	protected loadData() {
		if (!this.state.isLoading) {
			this.setState(
				{
					isLoading: true,
				},
				async () => {
					let params: any = {
						team: this.state.activeTab,
					};

					const url = buildUrl(null, {
						path: '/api/staff',
						queryParams: params,
					});

					const {
						data,
						error,
					} = await this.props.apiService.queryGET(url);

					if (error) {
						this.props.dispatch(apiError([error]));

						this.setState({
							isLoading: false,
						});
					}

					if (data && 'staffs' in data) {
						this.setState({
							isLoading: false,
							staffData: data.staffs,
						});
					}
				},
			);
		}
	}
}

const mapStateToProps = (state: IStoreState) => {
	const { userAuthenticated, userData } = state.user;

	return {
		userAuthenticated,
		userData,
	};
};

export default connect(mapStateToProps)(withRouter(MeetTheTeam as any));
