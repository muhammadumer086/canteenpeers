import React from 'react';
import Head from 'next/head';
import Router from 'next/router';
import { connect } from 'react-redux';
import { animateScroll } from 'react-scroll';
import buildUrl from 'build-url';

import { withRouter } from 'next/router';

 

import { IStoreState, apiError, notificationsLoad } from '../js/redux/store';

import { ApiService, AuthService } from '../js/services';

import {
	IApiConversationDetail,
	IApiMessage,
	IApiUser,
	IApiToken,
} from '../js/interfaces';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import MyError from './_error';

import MessageSidebar from '../js/components/messages/MessageSidebar';
import MessageView from '../js/components/messages/MessageView';
import MessageViewNew from '../js/components/messages/MessageViewNew';
import Link from 'next/link';
import { Button } from '@material-ui/core';

interface IProps {
	router: any;
	userConversation: string;
	userAuthenticated: boolean | null;
	userData: IApiUser;
	inboxData: {
		data: {
			conversations: IApiConversationDetail[];
			error: boolean | undefined;
		};
		links:
			| {
					first: string;
					last: string;
					prev: string | null;
					next: string | null;
			  }
			| undefined;
		meta:
			| {
					current_page: number;
					from: number;
					last_page: number;
					path: string;
					per_page: number;
					to: number;
					total: number;
			  }
			| undefined;
	};
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	createNewMessage: boolean;
	conversationInformationActive: boolean;
	inboxData: IApiConversationDetail[];
	activeUsers: IApiUser[];
	activeConversationId: number | null;
	activeConversationMessages: IApiMessage[];
	activeConversationDetail: IApiConversationDetail | null;
	isLoadingMessages: boolean;
	currentPage: number;
	maxPages: number;
	messagesActive: boolean;
	clearMessageData: boolean;
	conversationActive: boolean;
	conversationNewActive: boolean;
	error: boolean | undefined;
}

interface IFormValues {
	content: string;
}

export interface IFormValuesNew {
	users: string[];
	message: string;
}

class Messages extends React.Component<IProps, IState> {
	static async getInitialProps({ apiService }) {
		const inboxData = await (apiService as ApiService).queryGET(
			'/api/conversations',
		);

		return {
			inboxData,
		};
	}

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			createNewMessage: !this.getMostRecentConversationId(),
			conversationInformationActive: false,
			inboxData: this.props.inboxData.data.conversations,
			error: this.props.inboxData.data.error,
			activeConversationId: this.getMostRecentConversationId(),
			activeConversationMessages: [],
			activeConversationDetail: null,
			activeUsers: [],
			isLoadingMessages: false,
			currentPage: null,
			maxPages: null,
			messagesActive: false,
			clearMessageData: false,
			conversationActive: false,
			conversationNewActive: false,
		};

		this.updateData = this.updateData.bind(this);
		this.startNewMessage = this.startNewMessage.bind(this);
		this.chooseConverstaion = this.chooseConverstaion.bind(this);
		this.getMostRecentConversationId = this.getMostRecentConversationId.bind(
			this,
		);
		this.submitReply = this.submitReply.bind(this);
		this.updateMessages = this.updateMessages.bind(this);
		this.getMessageDetails = this.getMessageDetails.bind(this);
		this.updateConversationData = this.updateConversationData.bind(this);
		this.loadMoreMessages = this.loadMoreMessages.bind(this);
		this.closeConversation = this.closeConversation.bind(this);
		this.createNewConversation = this.createNewConversation.bind(this);
		this.toggleConversationDetail = this.toggleConversationDetail.bind(
			this,
		);
		this.updateConversation = this.updateConversation.bind(this);
		this.updateInbox = this.updateInbox.bind(this);
		this.getUpToDateConversation = this.getUpToDateConversation.bind(this);
	}

	public componentDidMount() {
		if (this.props.userAuthenticated) {
			this.updateMessages();
		}

		if (window.innerWidth >= 600) {
			if (this.state.inboxData && this.state.inboxData.length > 1) {
				this.setState({
					conversationActive: true,
					conversationNewActive: false,
				});
			} else {
				this.setState({
					conversationActive: false,
					conversationNewActive: true,
				});
			}
		} else {
			this.setState({
				conversationActive: false,
				conversationNewActive: false,
			});
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.userAuthenticated !== prevProps.userAuthenticated) {
			if (!this.props.userAuthenticated) {
				Router.push('/auth/login');
			} else {
				this.updateData();
			}
		}

		animateScroll.scrollToBottom({
			containerId: 'scroll-container',
			duration: 0,
			delay: 0,
		});
	}

	public render() {
		if (this.props.userAuthenticated === false) {
			return <MyError statusCode={401} />;
		}
		
		const { error } = this.state;

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard>
					{error === true ? (
						<div className="user_messages">
							{/* <h3>
								{errorMessage}
								<Link href="/discussions">
									<a>here.</a>
								</Link>
								or go to{' '}
								<Link href="/dashboard">
									<a>dashboard</a>
								</Link>
							</h3>*/}
							<div
								className="message_error"
							>
								<div className="message_error_content" >
									<p>
										To help keep you safe, direct messaging
										is only available for community members
										aged 16+.<br/> To connect with others,
										<Link href="/discussions">
											<a className="go_to_link link_padding" style={{color:"#03a5fc"}} >join a
										conversation.</a>
										</Link>
										{/* or go to{' '}
										<Link href="/dashboard">
											<a style={{color:"#03a5fc"}}>dashboard</a>
										</Link>. */}
									</p>
									<div className="message_error_actions">
										<Link href="/discussions" as="/discussions">
										<Button
										variant="outlined"
										color="primary"
										className="message_error_button"
										
									
										>
											Join discussion
										</Button>
										</Link>
										{/* <Link href="/dashboard" as="/dashboard">
										<Button
										variant="outlined"
										color="primary"
										className="message_error_button"
										
										>
											dashboard
										</Button>
										</Link> */}
									</div>
								</div>
								<div>
									<img
										className="message_error_image"
										
										src={`${
											process.env.STATIC_PATH
										}/images/unde16.png`}
									/>
								</div>
							</div>
							<div className="message_error_mobile">
									<div className="message_error_image_container">
									<img
										className="message_error_image"
										
										src={`${
											process.env.STATIC_PATH
										}/images/under16_mobile.png`}
									/>
									</div>
									<div className="message_error_content">
									<p>
										To help keep you safe, direct messaging
										is only available for community members
										aged 16+.<br/> To connect with others, 
										<Link href="/discussions">
											<a className="go_to_link link_padding" style={{color:"#03a5fc"}} >join a
										conversation.</a>
										</Link>
										{/* or go to{' '}
										<Link href="/dashboard">
											<a style={{color:"#03a5fc"}}>dashboard</a>
										</Link> */}
									</p>
									</div>
									<div>
									<Link href="/discussions" as="/discussions">
										<Button
										variant="outlined"
										color="primary"
										className="message_error_button"
										
									
										>
											Join discussion
										</Button>
										</Link>
										{/* <Link href="/dashboard" as="/dashboard">
										<Button
										variant="outlined"
										color="primary"
										className="message_error_button"
										
										>
											dashboard
										</Button>
										</Link> */}
									</div>
							</div>
						</div>
					) : (
						<div className="user_messages">
							<MessageSidebar
							    userData = {this.props.userData}
								inboxData={this.state.inboxData}
								activeConversationId={
									this.state.activeConversationId
								}
								startNewMessage={this.startNewMessage}
								chooseConversation={this.chooseConverstaion}
								conversationPanelActive={
									this.state.conversationActive ||
									this.state.conversationNewActive
								}
							/>

							{this.state.conversationActive && (
								<MessageView
									messagesActive={this.state.messagesActive}
									isUpdating={this.state.isUpdating}
									isLoadingMessages={
										this.state.isLoadingMessages
									}
									activeUsers={this.state.activeUsers}
									activeConversationMessages={
										this.state.activeConversationMessages
									}
									conversationDetail={
										this.state.activeConversationDetail
									}
									loadMoreMessages={this.loadMoreMessages}
									submitReply={this.submitReply}
									closeConversation={this.closeConversation}
									toggleConversationDetail={
										this.toggleConversationDetail
									}
									conversationInformationActive={
										this.state.conversationInformationActive
									}
									updateConversation={this.updateConversation}
									onConversationLeave={
										this.getUpToDateConversation
									}
									clearMessageData={
										this.state.clearMessageData
									}
								/>
							)}

							{this.state.conversationNewActive && (
								<MessageViewNew
									messagesActive={this.state.createNewMessage}
									isUpdating={this.state.isUpdating}
									isLoadingMessages={
										this.state.isLoadingMessages
									}
									activeUsers={this.state.activeUsers}
									activeConversation={[]}
									createConversation={
										this.createNewConversation
									}
									closeConversation={this.closeConversation}
									clearMessageData={
										this.state.clearMessageData
									}
								/>
							)}
						</div>
					)}
				</LayoutDashboard>
			</div>
		);
	}

	protected loadMoreMessages(append: boolean = true) {
		if (
			!this.state.isLoadingMessages &&
			(this.state.currentPage < this.state.maxPages || !append) &&
			this.state.activeConversationId
		) {
			this.setState(
				{
					isLoadingMessages: true,
				},
				() => {
					let params: any = null;

					if (append) {
						params = {
							page: this.state.currentPage + 1,
						};
					}

					const url = buildUrl(null, {
						path: `/api/conversations/${
							this.state.activeConversationId
						}/messages`,
						queryParams: params,
					});

					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'messages' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let activeConversationMessages =
									response.data.messages;

								if (append) {
									activeConversationMessages = JSON.parse(
										JSON.stringify(
											this.state
												.activeConversationMessages,
										),
									);
									(response.data
										.messages as IApiMessage[]).forEach(
										message => {
											activeConversationMessages.unshift(
												message,
											);
										},
									);
								}

								this.setState({
									activeConversationMessages,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									isLoadingMessages: false,
								});
							}
						})
						.catch(error => {
							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		}
	}

	protected updateData(): void {}

	protected async updateConversationData(id?: number) {
		const conversationId = id ? id : this.state.activeConversationId;
		if (conversationId) {
			try {
				const [
					activeConversation,
					activeConversationDetail,
				] = await Promise.all([
					this.props.apiService.queryGET(
						`/api/conversations/${conversationId}/messages`,
					),
					this.props.apiService.queryGET(
						`/api/conversations/${conversationId}`,
					),
				]);

				this.setState(
					{
						activeConversationMessages: activeConversation.data.messages.reverse(),
						activeConversationDetail: activeConversationDetail.data,
						activeUsers: activeConversationDetail.data.users,
						isUpdating: false,
						currentPage: activeConversation.meta.current_page,
						maxPages: activeConversation.meta.last_page,
						clearMessageData: false,
					},
					() => {
						const token: IApiToken | null = this.props.authService
							.userToken;
						this.props.dispatch(
							notificationsLoad(
								this.props.apiService,
								token ? token.access_token : null,
							),
						);
					},
				);
			} catch (error) {
				this.props.dispatch(
					apiError([
						'Error retrieving data - please try again later',
					]),
				);
			}
		} else {
			this.setState({
				isUpdating: false,
			});
		}
	}

	protected updateInbox(cb?): void {
		// TODO api call to get the data
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				this.props.apiService
					.queryGET(`/api/conversations`)
					.then(inboxData => {
						this.setState({
							inboxData: inboxData.data.conversations,
							isUpdating: false,
						});

						if (cb) {
							cb(inboxData.data.conversations[0].id);
						}
					})
					.catch(error => {
						this.setState({
							isUpdating: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected updateMessages() {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				this.updateConversationData();
			},
		);
	}

	protected getMessageDetails() {
		if (this.state.activeConversationId) {
			this.props.apiService
				.queryGET(
					`/api/conversations/${this.state.activeConversationId}`,
				)
				.then(response => {
					this.setState({
						activeUsers: response.data.users,
					});
				})
				.catch(error => {
					// dispatch the error message
					this.props.dispatch(apiError([error.message]));
				});
		}
	}

	protected chooseConverstaion(id: number): void {
		this.setState(
			{
				activeConversationId: id,
				isUpdating: true,
				messagesActive: true,
				createNewMessage: false,
				clearMessageData: true,
				conversationActive: true,
				conversationNewActive: false,
			},
			() => {
				this.updateConversationData();
			},
		);
	}

	protected submitReply(values: IFormValues): Promise<boolean> {
		return new Promise(resolve => {
			if (this.state.activeConversationId) {
				this.setState(
					{
						isUpdating: true,
					},
					() => {
						this.props.apiService
							.queryPOST(
								`/api/conversations/${
									this.state.activeConversationId
								}/messages`,
								values,
							)
							.then(() => {
								this.updateMessages();
								this.updateInbox();

								resolve(true);
							})
							.catch(error => {
								this.setState(
									{
										isUpdating: false,
									},
									() => {
										resolve(false);
									},
								);
								// dispatch the error message
								this.props.dispatch(apiError([error.message]));
							});
					},
				);
			}
			resolve(true);
		});
	}

	protected getMostRecentConversationId(): number | null {
		if (
			this.props.inboxData.data.conversations instanceof Array &&
			this.props.inboxData.data.conversations.length
		) {
			return this.props.inboxData.data.conversations[0].id;
		}
		return null;
	}

	protected createNewConversation(values: IFormValuesNew) {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				this.props.apiService
					.queryPOST(`/api/conversations`, values, 'conversation')
					.then((response: any) => {
						this.setState(
							{
								isUpdating: false,
								createNewMessage: false,
								activeConversationId: response.id,
								conversationActive: true,
								conversationNewActive: false,
							},
							() => {
								this.updateInbox();
								this.updateConversationData();
							},
						);
					})
					.catch(error => {
						this.setState({
							isUpdating: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected startNewMessage(): void {
		this.setState({
			createNewMessage: true,
			messagesActive: false,
			activeConversationId: null,
			conversationActive: false,
			conversationNewActive: true,
		});
	}

	protected closeConversation() {
		this.setState({
			messagesActive: false,
			createNewMessage: false,
			conversationActive: false,
			conversationNewActive: false,
		});
	}

	protected getUpToDateConversation() {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				const updateActiveConversation = (id: number) => {
					this.setState(
						{
							activeConversationId: id,
							conversationInformationActive: false,
							messagesActive: false,
						},
						() => {
							this.updateConversationData();
						},
					);
				};

				this.updateInbox(updateActiveConversation);
			},
		);
	}

	protected toggleConversationDetail() {
		this.setState({
			conversationInformationActive: !this.state
				.conversationInformationActive,
		});
	}

	protected updateConversation(payload: {
		name: string;
		users: string[];
	}): Promise<boolean> {
		return new Promise(resolve => {
			this.setState(
				{
					isUpdating: true,
				},
				() => {
					this.props.apiService
						.queryPUT(
							`/api/conversations/${
								this.state.activeConversationId
							}`,
							payload,
						)
						.then((response: any) => {
							this.setState(
								{
									activeConversationDetail: response.data,
									isUpdating: false,
									conversationInformationActive: false,
								},
								() => {
									this.updateInbox();
									this.updateConversationData();

									resolve(true);
								},
							);
						})
						.catch(error => {
							this.setState({
								isUpdating: false,
							});
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(withRouter(Messages as any));