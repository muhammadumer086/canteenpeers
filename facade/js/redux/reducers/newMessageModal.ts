import { actionTypes } from '../actions';
import { IApiUser } from '../../interfaces';

export interface  IStoreState {
	newMessageModalDisplay: boolean;
	userMessageData: IApiUser|null,
}

export const newMessageModal = (state: IStoreState = {
	newMessageModalDisplay: false,
	userMessageData: null,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.NEW_MESSAGE_MODAL_DISPLAY:
			return Object.assign({}, state, {
				newMessageModalDisplay: true,
				userMessageData: action.userMessageData,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.NEW_MESSAGE_MODAL_HIDE:
			return Object.assign({}, state, {
				newMessageModalDisplay: false,
			});
		default:
			return state;
	}
}
