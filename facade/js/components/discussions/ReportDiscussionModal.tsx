import React from 'react';
import { connect } from 'react-redux';

import ReportDiscussionForm, { IFormValues } from './ReportDiscussionForm';

import {
	IStoreState,
	reportModalHide,
	apiError,
	apiSuccess,
} from '../../redux/store';

import { IApiUser, IApiDiscussion } from '../../interfaces';

import { ApiService, withServices } from '../../services';
import { EReportModalType } from '../../redux/reducers/reportModal';
import { ModalDialog } from '../modals/local';

interface IProps {
	modalDisplay: boolean;
	discussion: IApiDiscussion | null;
	modalType: EReportModalType;
	userData: IApiUser;
	userAuthenticated: boolean | null;
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	isSubmitting: boolean;
}

class ReportDiscussionModal extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			isSubmitting: false,
		};

		this.handleClose = this.handleClose.bind(this);
		this.handleSubmitData = this.handleSubmitData.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			(this.props.userAuthenticated !== prevProps.userAuthenticated ||
				this.props.userData !== prevProps.userData) &&
			!this.props.userAuthenticated
		) {
			this.handleClose();
		}
	}

	public render() {
		return (
			// <Modal
			// 	aria-labelledby="report-post-modal"
			// 	open={
			// 		!!(this.props.modalDisplay && this.props.userAuthenticated)
			// 	}
			// 	onClose={this.handleClose}
			// >
			// 	<Paper className="modal theme--main" square={true}>
			// 		<button
			// 			onClick={this.handleClose}
			// 			className="close_button close_button--primary"
			// 			title="Dismiss"
			// 		>
			// 			<ClearIcon className="close_button-icon" />
			// 		</button>
			// 		<div className="modal-content_container">
			// 			<div className="modal-content discussion_modal">
			// 				<h2 className="modal-content_title font--h5 theme-title hm-t16">
			// 					{this.getTextModalTitle()}
			// 				</h2>

			// 			</div>
			// 		</div>
			// 	</Paper>
			// </Modal>

			<ModalDialog
				isActive={
					!!(this.props.modalDisplay && this.props.userAuthenticated)
				}
				modalTitle={this.getTextModalTitle()}
				ariaTag="report-post-modal"
				handleClose={this.handleClose}
			>
				<ReportDiscussionForm
					discussion={this.props.discussion}
					onSubmit={this.handleSubmitData}
					submitting={this.state.isSubmitting}
				/>
			</ModalDialog>
		);
	}

	protected getTextModalTitle() {
		switch (this.props.modalType) {
			case EReportModalType.AGE_SENSITIVE:
				return 'Flag this post as age-sensitive';
			default:
				return 'Report this post as harmful';
		}
	}

	protected getTextApiResponse() {
		switch (this.props.modalType) {
			case EReportModalType.AGE_SENSITIVE:
				return 'Thank you for flagging this post';
			default:
				return 'Thank you for reporting this post';
		}
	}

	protected handleClose() {
		this.props.dispatch(reportModalHide());
	}

	protected handleSubmitData(values: IFormValues) {
		const params = {
			title: values.reason,
			comment: values.comment,
			discussion: this.props.discussion.id,
			age_sensitive:
				this.props.modalType === EReportModalType.AGE_SENSITIVE,
		};
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST(
						'/api/report-discussions',
						params,
						'report_discussion',
					)
					.then(() => {
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.handleClose();
								this.props.dispatch(
									apiSuccess([this.getTextApiResponse()]),
								);
							},
						);
					})
					.catch(error => {
						this.setState({
							isSubmitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { modalDisplay, discussion, modalType } = state.reportModal;
	return {
		userAuthenticated,
		userData,
		modalDisplay,
		discussion,
		modalType,
	};
}

export default connect(mapStateToProps)(withServices(ReportDiscussionModal));
