import React from 'react';
import { connect } from 'react-redux';

import { Button, Menu } from '@material-ui/core';
import { MenuItem } from '@material-ui/core';

import { withServices, ApiService } from '../../services';

import {
	IStoreState,
	apiSuccess,
	apiError,
	userUpdated,
} from '../../redux/store';
import { IApiUser } from 'js/interfaces';

import { GTMDataLayer } from '../../../js/helpers/dataLayer';
import ResourceCommunityShare from '../resources/ResourceCommunityShare';

interface IProps {
	userData: IApiUser | null;
	apiService: ApiService;
	sharingUrl: string;
	userAuthenticated: boolean | null;
	withSaveAction: boolean;
	articleTitle: string;
	articleId?: number;
	articleSaved?: boolean;
	resourceType: string;
	resourceSlug: string;
	openShareModal(): void;
	dispatch(action: any): void;
}

interface IState {
	anchorEl: any | null;
	shareModalActive: boolean;
	articleSaved: boolean | null;
	isUpdating: boolean;
}

class ArticleActions extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
			shareModalActive: false,
			articleSaved: this.props.articleSaved,
			isUpdating: false,
		};

		this.openCommunityShare = this.openCommunityShare.bind(this);
		this.handleShareClick = this.handleShareClick.bind(this);
		this.handleShareClose = this.handleShareClose.bind(this);

		this.copyLink = this.copyLink.bind(this);
		this.shareLinkWithEmail = this.shareLinkWithEmail.bind(this);
		this.shareResource = this.shareResource.bind(this);
		this.saveResource = this.saveResource.bind(this);
		this.closeCommunityShare = this.closeCommunityShare.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.articleSaved !== prevProps.articleSaved) {
			this.setState({
				articleSaved: this.props.articleSaved,
			});
		}
	}

	public render() {
		const { anchorEl } = this.state;

		return (
			<React.Fragment>
				<div className="article_single-sidebar">
					{this.props.withSaveAction && (
						<Button
							color="primary"
							variant={
								this.state.articleSaved
									? 'contained'
									: 'outlined'
							}
							onClick={this.saveResource}
							disableRipple={true}
							className="article_single-button_save"
						>
							{this.state.articleSaved ? (
								<span>Saved</span>
							) : (
								<span>Save</span>
							)}
						</Button>
					)}

					<Button
						color="primary"
						variant="outlined"
						onClick={this.handleShareClick}
						disableRipple={true}
						className="article_single-button_share"
					>
						<span>Share</span>
					</Button>
				</div>

				<Menu
					id="share-action"
					anchorEl={anchorEl}
					open={Boolean(anchorEl)}
					onClose={this.handleShareClose}
				>
					{this.props.userAuthenticated === true && (
						<MenuItem onClick={this.openCommunityShare}>
							Share in Community
						</MenuItem>
					)}

					<MenuItem
						onClick={() => {
							this.shareResource('facebook');
						}}
					>
						Facebook
					</MenuItem>
					<MenuItem
						onClick={() => {
							this.shareResource('twitter');
						}}
					>
						Twitter
					</MenuItem>
					<MenuItem onClick={this.copyLink}>Copy Link</MenuItem>
					<MenuItem onClick={this.shareLinkWithEmail}>Email</MenuItem>
				</Menu>
				<ResourceCommunityShare
					modalOpen={this.state.shareModalActive}
					handleClose={this.closeCommunityShare}
					title={this.props.articleTitle}
					url={this.props.sharingUrl}
					onCommunityShare={() => {
						this.sendShareAction('community');
					}}
				/>
			</React.Fragment>
		);
	}

	protected handleShareClick(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}

	protected handleShareClose() {
		this.setState({
			anchorEl: null,
		});
	}

	protected openCommunityShare() {
		this.setState({
			shareModalActive: true,
			anchorEl: null,
		});
	}

	protected closeCommunityShare() {
		this.setState({
			shareModalActive: false,
			anchorEl: null,
		});
	}

	protected shareResource(network: string): void {
		if (network) {
			if (network === 'facebook') {
				this.facebookShare();
			} else if (network === 'twitter') {
				this.twitterShare();
			}

			// this.GTM.pushEventToDataLayer({
			// 	event: `share${this.props.gtmKey}`,
			// 	sharetype: network,
			// })

			this.sendShareAction(network);
		}
	}

	protected facebookShare() {
		FB.ui(
			{
				method: 'share',
				href: this.props.sharingUrl,
			},
			() => {
				this.handleShareClose();
			},
		);
	}

	protected twitterShare() {
		const tweetWindowName = 'Share Canteen';
		const twitterText = 'Check out this resource that I found at Canteen';
		const strWindowFeatures =
			'menubar=no,location=yes,resizable=no,scrollbars=no,status=yes,width=600,height=300';

		window.open(
			`https://twitter.com/intent/tweet?text=${twitterText} ${this.props.sharingUrl}`,
			tweetWindowName,
			strWindowFeatures,
		);
	}

	protected copyLink() {
		let txt = document.createTextNode(this.props.sharingUrl);
		document.body.appendChild(txt);
		if ((document.body as any).createTextRange) {
			var d = (document.body as any).createTextRange();
			d.moveToElementText(txt);
			d.select();
			document.execCommand('copy');

			this.props.dispatch(apiSuccess(['Message successfully sent']));
		} else {
			var d: any = document.createRange();
			d.selectNodeContents(txt);
			window.getSelection().removeAllRanges();
			window.getSelection().addRange(d);
			document.execCommand('copy');
			window.getSelection().removeAllRanges();

			this.props.dispatch(apiSuccess(['Link copied to clipboard']));
		}
		txt.remove();
		this.handleShareClose();

		this.sendShareAction('copy link');
	}

	protected shareLinkWithEmail() {
		let a = document.createElement('a');
		a.href = `mailto:?subject=${this.props.articleTitle}&body=Check out this link on Canteen - ${this.props.sharingUrl}`;
		a.target = '_blank';
		document.body.appendChild(a);
		a.click();
		// Added code
		document.body.removeChild(a);

		this.handleShareClose();

		this.sendShareAction('email');
	}

	protected saveResource() {
		this.setState(
			{
				isUpdating: true,
				articleSaved: !this.state.articleSaved,
			},
			() => {
				this.updateSavedCount(this.state.articleSaved);
				this.props.apiService
					.queryPOST(
						`/api/resources/${this.props.resourceSlug}/save`,
						{},
					) //use slug instead of id
					.then(() => {
						//Redux handles the updating of the state
					})
					.catch(error => {
						this.props.dispatch(apiError([error.message]));

						this.setState(
							{
								isUpdating: false,
								articleSaved: !this.state.articleSaved,
							},
							() => {
								this.updateSavedCount(this.state.articleSaved);
							},
						);
					});
			},
		);
	}

	protected sendShareAction(platform: string) {
		if (this.props.userAuthenticated) {
			this.props.apiService
				.queryPOST(
					`/api/${this.props.resourceType}/${this.props.resourceSlug}/share?medium=${platform}`,
					{},
				)
				.then(() => {
					//Redux handles the updating of the state
				})
				.catch(error => {
					this.props.dispatch(apiError([error.message]));
				});
		}
	}

	protected updateSavedCount(add: boolean = true) {
		const user: IApiUser = JSON.parse(JSON.stringify(this.props.userData));
		if (
			user &&
			'saved_resources_count' in user &&
			typeof user.saved_resources_count === 'number'
		) {
			if (add) {
				user.saved_resources_count += 1;

				// this.GTM.pushEventToDataLayer({
				// 	event: `favourite${this.props.gtmKey}`,
				// 	sharetype: undefined,
				// });
			} else if (user.saved_resources_count >= 1) {
				user.saved_resources_count -= 1;
			}

			this.props.dispatch(userUpdated(user));
		}
	}
}

const mapStateToProps = (state: IStoreState) => {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
};

export default connect(mapStateToProps)(withServices(ArticleActions));
