<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use League\Csv\Writer;

use App\Helpers\CsvMigration;
use App\User;
use App\Discussion;
use App\Like;
use App\Hug;

class CsvMigrationLikesHugs extends CsvMigration {
	/**
	 * The headers for the CSV
	 *
	 * @var array
	 */
	public $csvHeaders = ['user_id', 'discussion_id', 'type'];

	public function processRecord(Writer $writer, $record) {
		throw new \Exception(
			'No processRecord defined for CsvMigrationLikesHugs'
		);
	}

	protected function formatRecord($data, $record) {
		throw new \Exception(
			'No formatRecord defined for CsvMigrationLikesHugs'
		);
	}

	public function importData($record) {
		$user = User::where('migration_id', $record['user_id'])->first();

		$discussion = Discussion::where(
			'migration_id',
			$record['discussion_id']
		)->first();

		if ($user && $discussion) {
			try {
				$model = null;
				$modelFound = true;
				if ($record['type'] === 'HUG') {
					$model = new Hug();
					$modelFound = $this->hugExists($user, $discussion);
				} elseif ($record['type'] === 'I GET IT') {
					$model = new Like();
					$modelFound = $this->likeExists($user, $discussion);
				}

				if ($model !== null && !$modelFound) {
					$model->user()->associate($user);
					$model->discussion()->associate($discussion);
					$model->save();
				}
			} catch (\Exception $e) {
				echo "\r\n\r\nError creating blog {$record['id']}: User not found {$record['author']}\r\n\r\n";
				return;
			}
		}
	}

	protected function hugExists($user, $discussion) {
		return Hug::where('user_id', $user->id)
			->where('discussion_id', $discussion->id)
			->exists();
	}

	protected function likeExists($user, $discussion) {
		return Like::where('user_id', $user->id)
			->where('discussion_id', $discussion->id)
			->exists();
	}
}
