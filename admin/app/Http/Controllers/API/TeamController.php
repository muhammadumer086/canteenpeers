<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTeamRequest;
use App\Http\Resources\StaffCollection;
use App\Http\Resources\Team as TeamResource;
use App\Http\Resources\TeamCollection;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return new TeamCollection(Team::orderBy('order')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreTeamRequest $request) {
		$validated = $request->validated();

		$team = new Team();
		$team->slug = str_slug($validated['name']);
		$team->fill($validated);
		$team->save();

		return new TeamResource($team);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Team  $team
	 * @return \Illuminate\Http\Response
	 */
	public function update(StoreTeamRequest $request, Team $team) {
		$validated = $request->validated();

		$team->slug = str_slug($validated['name']);
		$team->fill($validated);
		$team->save();

		return new TeamResource($team);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Team  $team
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Team $team) {
		$team->delete();
	}
}
