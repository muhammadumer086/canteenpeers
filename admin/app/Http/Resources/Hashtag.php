<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Hashtag extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'hashtag',
				$this->id,
				$this->updated_at,
				true,
				false
			)
		) {
			return $apiStore->getApiResource(
				'hashtag',
				$this->id,
				$this->updated_at,
				true,
				false
			);
		}

		$params = ['name'];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$apiStore->setApiResource(
			$result,
			'hashtag',
			$this->id,
			$this->updated_at,
			true,
			false
		);

		return $result;
	}
}
