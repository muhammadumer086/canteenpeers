<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;

use Artisan;
use Validator;
use UserActivityHelper;
use UserPermissionHelper;

use Carbon\Carbon;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Http\Controllers\Controller;
use App\Http\Resources\Discussion as DiscussionResource;
use App\Http\Resources\DiscussionCollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\SupportUsersInDiscussionCollection;
use App\Notifications\UserMentionedNotification;

use App\Discussion;
use App\Like;
use App\Hug;
use App\Topic;
use App\User;
use App\Situation;
use App\SituationUser;
use App\SituationTopic;
use App\DiscussionSituationTopic;
use App\Mention;
use App\Hashtag;
use App\SupportUsersInDiscussion;

class DiscussionController extends Controller {
	use \App\Traits\CreateSlug;

	const PAGINATION_SIZE = 20;

	/**
	 * Return list of all discussions
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["discussions_read"]);

		$validator = Validator::make($request->all(), [
			'topics' => 'bail|array|exists:topics,slug',
			'situations' => 'bail|array|exists:situations,name',
			'type' => 'bail|string|in:active,followed',
			'order' => 'bail|string|in:ASC,DESC',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Base of the Query
		$discussionsQuery = Discussion::whereNull(
			'main_discussion_id'
		)->where('for_event', false)->orderBy('last_activity_at', $request->input('order', 'DESC'));

		$filterBySituation = new \App\Helpers\FilterByUserSituation();

		// If querying by followed or active
		if ($authUser && $request->has('type')) {
			$type = $request->input('type');

			if ($type === 'active') {
				// Query all the discussions and replies the user has been active in
				$discussionsQuery = $authUser->activeDiscussions();
			} elseif ($type === 'followed') {
				$discussionsQuery = $authUser->followedDiscussions();
			}
		}

		if (!$request->has('type') && !$userHasPermission) {
			if ($request->has('situations')) {
				// $situationIds = Situation::whereIn(
				// 	'name',
				// 	$request->input('situations')
				// )
				// 	->whereNull('parent_id')
				// 	->pluck('situations.id');
				$situationIds = $filterBySituation->addSituationsMap(
					$request->input('situations')
				);

				$discussionsQuery->whereIn(
					'discussions.id',
					DB::table('discussion_situation')
						->whereIn('situation_id', $situationIds)
						->pluck('discussion_id')
				);
			} elseif (
				$authUser &&
				!$authUser->hasRoleName('admin') &&
				!$request->has('anySituation')
			) {
				// No situation filters selected, use situation of current user
				$discussionsQuery = $filterBySituation->filterByUserSituation(
					$authUser,
					$discussionsQuery,
					'discussions',
					'discussion_situation',
					'discussion_id'
				);
			} elseif (!$authUser) {
				$discussionsQuery
					->where('private', false)
					->where('age_sensitive', false);
			}

			if ($authUser && $authUser->isUnderage()) {
				$discussionsQuery->where('age_sensitive', false);
			}

			if ($request->has('topics')) {
				$discussionsQuery->whereIn(
					'topic_id',
					DB::table('topics')
						->whereIn(
							'id',
							Topic::whereIn(
								'slug',
								$request->input('topics')
							)->pluck('id')
						)
						->pluck('id')
				);
			}
		}

		$discussions = $discussionsQuery->paginate(self::PAGINATION_SIZE);

		return new DiscussionCollection($discussions);
	}

	/**
	 * Create a new discussion.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'title' => 'bail|required|string|max:191',
			'content' => 'bail|required|string|max:16000000',
			'topic' => 'bail|required|string|exists:topics,slug',
			'situations' => 'bail|required|array|exists:situations,slug',
			'ageSensitive' => 'bail|required|boolean',
			'private' => 'bail|required|boolean',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();
		$content = $richTextFilter->filter($request->input('content'));

		if (empty($content)) {
			return response()->json(
				[
					'error' => "Content can't be empty",
				],
				400
			);
		}

		$discussion = new Discussion();
		$discussion->user_id = $authUser->id;
		$discussion->title = $request->input('title');
		$discussion->slug = $this->createSlug(
			$request->input('title'),
			'App\Discussion'
		);
		$discussion->content = $content;
		$discussion->age_sensitive = $request->has('ageSensitive')
			? $request->input('ageSensitive')
			: false;
		$discussion->private = $request->has('private')
			? $request->input('private')
			: false;

		$discussion->feature_image = $request->has('feature_image')
			? $request->input('feature_image')
			: null;

		$topicId = $this->getTopicIdBySlug($request->input('topic'));
		$discussion->topic_id = $topicId;
		$discussion->last_activity_at = Carbon::now();

		$discussion->save();

		foreach ($discussion->getMentions() as $username) {
			$user = User::where('username', $username)->first();

			if ($user) {
				$mention = Mention::create([
					'mentioned_username' => $username,
					'mentioned_user_id' => $user->id,
					'sender_user_id' => auth()->user()->id,
				]);

				if ($mention) {
					$discussion->mentions()->attach($mention->id);
					$user->notify(
						new UserMentionedNotification($mention, $discussion)
					);
				}
			}
		}

		// Attach hashtag(s)
		Hashtag::attachHashtags($discussion);

		// Attach the user to the discussion
		$discussion->usersInDiscussion()->syncWithoutDetaching([$authUser->id]);
		$discussion->supportUsersInDiscussion()->syncWithoutDetaching([
			$authUser->id => ['reply' => true],
		]);

		$situationSlugs = $request->input('situations');

		foreach ($situationSlugs as $slug) {
			$situation = Situation::where('slug', $slug)->first();
			if ($situation->parent_id) {
				$discussion
					->situations()
					->syncWithoutDetaching([$situation->parent_id]);
			} else {
				$discussion
					->situations()
					->syncWithoutDetaching([$situation->id]);
			}
		}

		// Allow to push hashtags to algolia
		$discussion->save();

		$authUser->reportNumbers();
		$authUser->save();

		return response()->json([
			'discussion' => new DiscussionResource($discussion),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Discussion $discussion
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Discussion $discussion) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["reported_discussions_all"]);


		if (
			$authUser &&
			($this->hasAdminPermission() || $userHasPermission ||
				$authUser->id === $discussion->user->id)
		) {
			$isReply = !!$discussion->main_discussion_id;
			// Different validator based on either a main discussion or a reply
			if ($isReply) {
				$validator = Validator::make($request->all(), [
					'content' => 'bail|required|string|max:16000000',
				]);
			} else {
				$validator = Validator::make($request->all(), [
					'title' => 'bail|required|string|max:191',
					'content' => 'bail|required|string|max:16000000',
					'topic' => 'bail|required|string|exists:topics,slug',
					'situations' =>
						'bail|required|array|exists:situations,slug',
					'ageSensitive' => 'bail|required|boolean',
					'private' => 'bail|required|boolean',
				]);
			}

			if ($validator->fails()) {
				$error = $validator->errors()->first();

				return response()->json(
					[
						'error' => $error,
					],
					400
				);
			}

			$richTextFilter = new \App\Helpers\RichTextFilterHelper();
			$content = $richTextFilter->filter($request->input('content'));

			if (empty($content)) {
				return response()->json(
					[
						'error' => "Content can't be empty",
					],
					400
				);
			}

			if (!$isReply) {
				$discussion->title = $request->input('title');
				$discussion->age_sensitive = $request->has('ageSensitive')
					? $request->input('ageSensitive')
					: false;
				$discussion->private = $request->has('private')
					? $request->input('private')
					: false;
				$topicId = $this->getTopicIdBySlug($request->input('topic'));
				$discussion->topic_id = $topicId;
			}

			$discussion->content = $content;

			$discussion->save();

			foreach ($discussion->getMentions() as $username) {
				$user = User::where('username', $username)->first();

				if ($user) {
					$mention = Mention::create([
						'mentioned_username' => $username,
						'mentioned_user_id' => $user->id,
						'sender_user_id' => $discussion->user->id,
					]);

					if ($mention) {
						$discussion->mentions()->attach($mention->id);
						$user->notify(
							new UserMentionedNotification($mention, $discussion)
						);
					}
				}
			}

			if (!$isReply) {
				// Attach hashtag(s)
				Hashtag::attachHashtags($discussion);

				$situationSlugs = $request->input('situations');
				$situationIds = [];

				foreach ($situationSlugs as $slug) {
					$situation = Situation::where('slug', $slug)->first();
					if ($situation->parent_id) {
						$situationIds[] = $situation->parent_id;
					} else {
						$situationIds[] = $situation->id;
					}
				}
				$discussion->situations()->sync($situationIds);
			}

			// Allow to push hashtags to algolia
			$discussion->save();

			$discussion->user->reportNumbers();
			$discussion->user->save();

			return response()->json([
				'discussion' => new DiscussionResource($discussion),
			]);
		} else {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}
	}

	/**
	 * Get a topic by slug.
	 *
	 * @param string $slug
	 * @return topic_id
	 */
	private function getTopicIdBySlug($slug) {
		$topic = Topic::where('slug', $slug)->firstOrFail();
		if ($topic) {
			return $topic->id;
		}
		return false;
	}

	/**
	 * Reply to the discussion
	 *
	 * @param int $replyToId
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function reply($replyToId, Request $request) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'content' => 'bail|required|string|max:16000000',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => 'Missing data',
				],
				400
			);
		}

		$parent = Discussion::where('id', $replyToId)->firstOrFail();

		$mainDiscussionId = $parent->main_discussion_id;
		if (!$mainDiscussionId) {
			$mainDiscussionId = $replyToId;
		}
		$mainDiscussion = Discussion::where(
			'id',
			$mainDiscussionId
		)->firstOrFail();

		//Check is discussion is closed
		if (!is_null($mainDiscussion->closed_at)) {
			return response()->json(
				[
					'error' => 'Discussion Closed',
				],
				400
			);
		}

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();
		$content = $richTextFilter->filter($request->input('content'));

		if (empty($content)) {
			return response()->json(
				[
					'error' => "Content can't be empty",
				],
				400
			);
		}

		$discussion = new Discussion();

		$discussion->user_id = $authUser->id;
		$discussion->main_discussion_id = $mainDiscussionId;
		$discussion->reply_to_id = $replyToId;
		$discussion->content = $content;

		if ($parent->thread_id === null) {
			// Direct reply to a main discussion
			$discussion->thread_id = $parent->id;
			$discussion->discussion_index =
				$mainDiscussion->direct_replies_count;
		} elseif ($parent->reply_to_id === $parent->main_discussion_id) {
			$discussion->thread_id = $parent->id;
			$discussion->discussion_index = $parent->discussion_index;
		} else {
			$discussion->thread_id = $parent->thread_id;
			$discussion->discussion_index = $parent->discussion_index;
		}

		$discussion->save();

		$mainDiscussion
			->usersInDiscussion()
			->syncWithoutDetaching([$authUser->id]);
		$mainDiscussion->supportUsersInDiscussion()->syncWithoutDetaching([
			$authUser->id => ['reply' => true],
		]);

		foreach ($discussion->getMentions() as $username) {
			$user = User::where('username', $username)->first();

			if ($user) {
				$mention = Mention::create([
					'mentioned_username' => $username,
					'mentioned_user_id' => $user->id,
					'sender_user_id' => auth()->user()->id,
				]);

				if ($mention) {
					$discussion->mentions()->attach($mention->id);
					$user->notify(
						new UserMentionedNotification($mention, $discussion)
					);
				}
			}
		}

		// Attach the user to the main discussion
		$mainDiscussion
			->usersInDiscussion()
			->syncWithoutDetaching([$authUser->id]);

		// Notify main discussion author
		if (
			$mainDiscussion->user->allowNotificationDiscussionReply() ||
			$mainDiscussion->user->allowEmailDiscussionReply()
		) {
			$mainDiscussion->user->sendDiscussionReplyNotification(
				$discussion,
				$mainDiscussion,
				$mainDiscussion,
				$authUser
			);
		}

		// Notify parent discussion author
		if (
			($parent->user->allowNotificationDiscussionReply() ||
				$parent->user->allowEmailDiscussionReply()) &&
			$parent->id !== $mainDiscussion->id &&
			$parent->user->id !== $mainDiscussion->user->id
		) {
			$parent->user->sendDiscussionReplyNotification(
				$discussion,
				$parent,
				$mainDiscussion,
				$authUser
			);
		}

		// Notify the users following the discussion
		$mainDiscussion
			->followingUsers()
			->chunk(100, function ($followingUsers) use (
				$discussion,
				$parent,
				$mainDiscussion,
				$authUser
			) {
				foreach ($followingUsers as $singleUser) {
					if (
						$singleUser->id !== $authUser->id &&
						($singleUser->allowNotificationDiscussionFollowed() ||
							$singleUser->allowEmailDiscussionFollowed())
					) {
						$singleUser->sendDiscussionReplyNotification(
							$discussion,
							$parent,
							$mainDiscussion,
							$authUser
						);
					}
				}
			});

		$mainDiscussion->reportNumbers();
		$mainDiscussion->last_activity_at = Carbon::now();
		$mainDiscussion->save();
		$parent->reportNumbers();
		$parent->save();
		$authUser->reportNumbers();
		$authUser->save();

		return response()->json([
			'discussion' => new DiscussionResource(
				Discussion::where('id', $discussion->id)->firstOrFail()
			),
		]);
	}

	/**
	 * Display the discussion.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param string $slug
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $slug) {
		$authUser = $this->getAuthUser($request);

		$discussion = Discussion::where('slug', '=', $slug)
			->whereNull('main_discussion_id')
			->firstOrFail();

		if (!$authUser && $discussion->private) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		if (
			$discussion->age_sensitive &&
			$authUser &&
			$authUser->isUnderage()
		) {
			return response()->json(
				[
					'error' => 'Not of age',
				],
				401
			);
		}

		if ($authUser) {
			UserActivityHelper::create(
				$authUser,
				$discussion,
				'Discussion Read',
				null
			);
		}

		return response()->json([
			'discussion' => new DiscussionResource($discussion),
		]);
	}

	/**
	 * Display the discussion replies.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function replies(Request $request, $id) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'order' => 'bail|string|in:ASC,DESC',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$parent = Discussion::where('id', '=', $id)->firstOrFail();

		if (!$authUser && $parent->private) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		$validator = Validator::make($request->all(), [
			'index' => 'bail|required|integer|min:0',
			'total' => 'bail|required|integer|min:1|max:200',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$discussions = $parent
			->repliesDiscussions()
			->skip($request->input('index'))
			->take($request->input('total'))
			->orderBy('created_at', $request->input('order', 'DESC'))
			->get();

		$discussionsData = [];

		foreach ($discussions as $singleDiscussion) {
			$discussionResource = new DiscussionResource($singleDiscussion);
			$discussionResource->setWithLatestReply();
			$discussionsData[] = $discussionResource;
		}

		return response()->json([
			'discussions' => $discussionsData,
		]);
	}

	/**
	 * Display the discussion thread replies.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function threadReplies(Request $request, $id) {
		$authUser = $this->getAuthUser($request);

		$parent = Discussion::where('id', '=', $id)->firstOrFail();

		if (!$authUser && $parent->private) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				401
			);
		}

		$validator = Validator::make($request->all(), [
			'page' => 'bail|required|integer|min:1',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$pagination = 5;

		$discussions = $parent
			->repliesThread()
			->skip(($request->input('page') - 1) * $pagination)
			->take($pagination)
			->orderBy('created_at', 'DESC')
			->get();

		return response()->json([
			'discussions' => DiscussionResource::collection($discussions),
			'total' => $parent->repliesThread()->count(),
		]);
	}

	/**
	 * Toggle a like/unlike of the discusion
	 *
	 * @param int $id
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function toggleLike($id, Request $request) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$id) {
			return response()->json(
				[
					'error' => 'Invalid request',
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($id);

		//Check if already Liked
		$like = Like::where('user_id', $authUser->id)
			->where('discussion_id', $discussion->id)
			->first();

		//If not Liked then like
		if ($like === null) {
			$like = new Like();
			$like->user_id = $authUser->id;
			$like->discussion_id = $id;
			$like->save();

			$discussion
				->supportUsersInDiscussion()
				->syncWithoutDetaching([$authUser->id => ['like' => true]]);

			$liked = true;

			UserActivityHelper::create(
				$authUser,
				$discussion,
				'Like',
				null
			);
		} else {
			$like->delete();

			$discussion->removeSupport($authUser->id, 'like');

			$liked = false;
			UserActivityHelper::create(
				$authUser,
				$discussion,
				'Unlike',
				null
			);
		}

		UserActivityHelper::create(
			$authUser,
			$discussion,
			$liked ? 'Discussion I Get It' : 'Discussion Removed I Get It',
			null
		);

		$discussion->reportNumbers();
		$discussion->save();

		$authUser->reportNumbers();
		$authUser->save();

		return response()->json([
			'liked' => $liked,
		]);
	}

	/**
	 * Toggle a hug/un-hug of the discusion
	 *
	 * @param int $id
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function toggleHug($id, Request $request) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$id) {
			return response()->json(
				[
					'error' => 'Invalid request',
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($id);

		//Check if already Hugged
		$hug = Hug::where('user_id', $authUser->id)
			->where('discussion_id', $discussion->id)
			->first();

		$hugged = true;
		//If not hugged then hug
		if ($hug === null) {
			$hug = new Hug();
			$hug->user_id = $authUser->id;
			$hug->discussion_id = $id;
			$hug->save();

			$discussion
				->supportUsersInDiscussion()
				->syncWithoutDetaching([$authUser->id => ['hug' => true]]);

		} else {
			$hug->delete();

			$discussion->removeSupport($authUser->id, 'hug');

			$hugged = false;
		}

		UserActivityHelper::create(
			$authUser,
			$discussion,
			$hugged ? 'Discussion Hugged' : 'Discussion Removed Hug',
			null
		);

		$discussion->reportNumbers();
		$discussion->save();

		$authUser->reportNumbers();
		$authUser->save();

		return response()->json([
			'hugged' => $hugged,
		]);
	}

	/**
	 * Toggle a follow/unfollow of the discusion
	 *
	 * @param int $id
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function toggleFollow($id, Request $request) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$id) {
			return response()->json(
				[
					'error' => 'Invalid request',
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($id);

		// Toggle the follow relationship
		$authUser->followedDiscussions()->toggle([$id]);

		$followed = $authUser
			->followedDiscussions()
			->where('discussion_id', $id)
			->exists();

		UserActivityHelper::create(
			$authUser,
			$discussion,
			$followed ? 'Discussion Follow' : 'Discussion Unfollow',
			null
		);

		$authUser->reportNumbers();
		$authUser->save();

		return response()->json([
			'followed' => $followed,
		]);
	}

	public function indexLikes($id, Request $request) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$id) {
			return response()->json(
				[
					'error' => 'Invalid request',
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($id);

		return new UserCollection(
			$discussion->userLikes()->paginate(self::PAGINATION_SIZE)
		);
	}

	public function indexHugs($id, Request $request) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$id) {
			return response()->json(
				[
					'error' => 'Invalid request',
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($id);

		return new UserCollection(
			$discussion->userHugs()->paginate(self::PAGINATION_SIZE)
		);
	}

	public function indexSupports($id, Request $request) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$id) {
			return response()->json(
				[
					'error' => 'Invalid request',
				],
				400
			);
		}

		$discussion = Discussion::findOrFail($id);

		$supports = SupportUsersInDiscussion::where(
			'discussion_id',
			$discussion->id
		);

		return new SupportUsersInDiscussionCollection($supports->paginate(100));
	}

	/**
	 * Get Current user Active Discussions.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function activeDiscussions(Request $request) {
		$authUser = $this->getAuthUser($request);

		$discussions = $authUser
			->activeDiscussions()
			->orderBy('created_at', 'DESC')
			->paginate(self::PAGINATION_SIZE);

		return new DiscussionCollection($discussions);
	}

	/**
	 * Get Current user Followed Discussions.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function followedDiscussions(Request $request) {
		$authUser = $this->getAuthUser($request);

		$discussions = $authUser
			->followedDiscussions()
			->orderBy('created_at', 'DESC')
			->paginate(self::PAGINATION_SIZE);

		return new DiscussionCollection($discussions);
	}

	/**
	 * User discussions Activity of single user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function userDiscussionsActivity(
		Request $request,
		string $username
	) {
		$authUser = $this->getAuthUser($request);
		$user = User::where('username', $username)->firstOrFail();

		$discussions = Discussion::where('user_id', $user->id)
			->orderBy('created_at', 'DESC')
			->paginate(self::PAGINATION_SIZE);

		return new DiscussionCollection($discussions);
	}

	/**
	 * Delete a discussion
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $id) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["discussion reported_discussions_delete","reported_discussions_all"]);


		$discussion = Discussion::where('id', $id)->firstOrFail();

		// Check logged in user has permissions
		if (
			$authUser &&
			($this->hasAdminPermission() || $userHasPermission ||
				$authUser->id === $discussion->user->id)
		) {
			$mainDiscussion = null;
			$parentDiscussion = null;
			$discussionIdToReindex = $discussion->id;
			if ($discussion->main_discussion_id) {
				$discussionIdToReindex = $discussion->main_discussion_id;
				$mainDiscussion = $discussion->mainDiscussion;
			}
			if ($discussion->reply_to_id) {
				$parentDiscussion = $discussion->replyToDiscussion;
			}

			$discussion->delete();

			// Delete all the sub discussions permanently
			if ($discussion->main_discussion_id) {
				$discussion->repliesDiscussions()->forceDelete();
			} else {
				$discussion->childDiscussions()->delete();
			}

			$discussion->reportDiscussions()->delete();

			UserActivityHelper::create(
				$discussion->user,
				$discussion,
				'Discussion Deleted',
				null
			);
			UserActivityHelper::create(
				$authUser,
				$discussion,
				'Deleted Discussion',
				null
			);

			if ($mainDiscussion) {
				$mainDiscussion->reportNumbers();
				$mainDiscussion->save();
			}
			if ($parentDiscussion) {
				$parentDiscussion->reportNumbers();
				$parentDiscussion->save();
			}

			// Reindex the whole discussion
			$exitCode = Artisan::call('canteen:assignIndexInDiscussions', [
				'discussion' => $discussionIdToReindex,
			]);
		} else {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		return response()->json([
			'success' => true,
		]);
	}

	/**
	 * Get relevant discussions
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function relevantDiscussion(Request $request, $id) {
		$authUser = $this->getAuthUser($request);

		$discussion = Discussion::where('id', $id)
			->with('situationTopics')
			->first();
		$situationTopicId = $discussion->situationTopics[0]->id;

		//get discussions
		$discussionSituationTopics = DiscussionSituationTopic::where(
			'situation_topic_id',
			$situationTopicId
		)->get();

		$disucssion_ids = [];
		foreach ($discussionSituationTopics as $discussion) {
			$disucssion_ids[] = $discussion->discussion_id;
		}

		// Determine to show private discussions
		if ($user) {
			$discussions = Discussion::whereIn('id', $disucssion_ids)->paginate(
				20
			);
		} else {
			$discussions = Discussion::whereIn('id', $disucssion_ids)
				->where('private', 0)
				->paginate(20);
		}

		return new DiscussionCollection($discussions);
	}
}
