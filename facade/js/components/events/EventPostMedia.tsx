import React from "react";
import { ModalDialog } from '../modals/local/ModalDialog';
import { IApiEvent } from '../../interfaces';
import ImageDropzoneFormik from '../inputs/ImageDropzoneFormik';
import { TextFieldFormik } from '../inputs/TextFieldFormik';
import { Formik } from 'formik';
import { Button } from '@material-ui/core';

interface IProps {
    event : IApiEvent,
	togglePostModal(any): void;
	saveMedia(any): void;
}
class EventPromoModal extends React.Component<IProps>{
    public render(){
        return(
            <ModalDialog
            modalTitle={'Post Event Media'}
            handleClose={this.props.togglePostModal}
            isActive={true}
        >
            <Formik
                initialValues={{
                    feature_image: '',
                    image_id: '',
                    title: '',
                }}
                onSubmit={formValues => {
                    const values = JSON.parse(
                        JSON.stringify(formValues),
                    );
                    delete values.feature_image,
                        (values['event_id'] = this.props.event.id);
                    this.props.saveMedia(values);
                }}
                render={({
                    values,
                    touched,
                    errors,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    setFieldValue,
                    setFieldTouched,
                }) => {
                    const inputProps = {
                        values,
                        touched,
                        errors,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        setFieldValue,
                        setFieldTouched,
                    };
                    const dropzoneClasses = [
                        'discussion_modal-dropzone',
                    ];
                    if (
                        'feature_image' in values &&
                        values.feature_image
                    ) {
                        dropzoneClasses.push(
                            'discussion_modal-dropzone--image',
                        );
                    }
                    return (
                        <form
                            className="create_event_form form"
                            onSubmit={handleSubmit}
                        >
                            <div className="form-row">
                                <div className="form-column">
                                    <TextFieldFormik
                                        type="text"
                                        label="Title"
                                        name="title"
                                        disabled={false}
                                        {...inputProps}
                                    />
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-column">
                                    <ImageDropzoneFormik
                                        values={inputProps.values}
                                        dropzoneClasses={
                                            dropzoneClasses
                                        }
                                        setFieldValue={
                                            inputProps.setFieldValue
                                        }
                                        apiUrl="/api/event-images"
                                        label="Set Event Image"
                                        max_size={3}
                                    />
                                </div>
                            </div>
                            <div className="form-row form-row--submit">
                                <div className="form-column">
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                        disabled={
                                            !inputProps.values
                                                .feature_image
                                        }
                                    >
                                        save
                                    </Button>
                                </div>
                            </div>
                        </form>
                    );
                }}
            ></Formik>
        </ModalDialog>
    )
    }
}
export default EventPromoModal;