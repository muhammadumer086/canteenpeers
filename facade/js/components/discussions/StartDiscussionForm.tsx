import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import {
	TextFieldFormik,
	SelectFieldFormik,
	CheckboxGroupFormik,
	RadioGroupFormik,
	RichTextEditor,
} from '../inputs';

import { IStoreState } from '../../redux/store';

import Pill from '../general/Pill';

import { IApiSituation, ISelectOption, IApiUser } from '../../interfaces';
import { filterRootSituations } from '../../helpers/filterRootSituations';

interface IProps {
	topics: ISelectOption[];
	situations: IApiSituation[];
	userSituations: IApiSituation[];
	predefinedTopic: string | null;
	predefinedSituations: string[] | null;
	submitting: boolean;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	updatedSituations(situations: string[]): void;
	onSubmit(values: IFormValues);
}

interface IState {
	situations: IApiSituation[];
	userSituations: IApiSituation[];
	activeUserSituation: string[];
}

interface IFormValues {
	title: string;
	content: string;
	topic: string;
}

class StartDiscussionForm extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			situations: filterRootSituations(props.situations),
			userSituations: JSON.parse(JSON.stringify(props.userSituations)),
			activeUserSituation: this.initialiseSituations(props),
		};

		this.handleSituationClick = this.handleSituationClick.bind(this);
	}

	public componentDidUpdate(oldProps: IProps) {
		if (
			JSON.stringify(this.props.userSituations) !==
			JSON.stringify(oldProps.userSituations)
		) {
			this.setState({
				userSituations: this.props.userSituations,
				activeUserSituation: this.initialiseSituations(this.props),
			});
		}

		if (
			JSON.stringify(this.props.situations) !==
			JSON.stringify(oldProps.situations)
		) {
			this.setState({
				situations: filterRootSituations(this.props.situations),
				activeUserSituation: this.initialiseSituations(this.props),
			});
		}
	}

	public render() {
		const validationFormat = Yup.object().shape({
			title: Yup.string()
				.required('A title is required')
				.max(191)
				.min(3)
				.test('check special character','Invalid title',function(item){
					const formatabc = /([a-z]|[A-Z]|[0-9])/;
					const result=formatabc.test(item);
					return result;
				}),
			content: Yup.string()
				.required('A discussion is required')
				.max(16000000),
			topic: Yup.string()
				.required('A topic is required')
				.max(191),
			situations: Yup.array()
				.of(
					Yup.mixed().oneOf(
						this.props.situations.map(singleOption => {
							return singleOption.slug;
						}),
					),
				)
				.required('Please select at least one situation'),
		});

		const discussionDisplayOptions: ISelectOption[] = [
			{
				label:
				'I’d like to keep my discussion private so that only members of the Canteen Connect community can see it',
				value: 'private',
				tooltip:'Please note: This option cannot be changed after the discussion is created.'
			},
			{
				label : 'I’m OK with people outside  the  community  seeing  my  discussion  (including  people searching on Google)',
				value : 'public'
			}
		];
		const discussionAgeSensitiveOption : ISelectOption[]=[];
		if (this.props.userData && !this.props.userData.underage) {
			discussionAgeSensitiveOption.push({
				label:
					'This discussion contains age-sensitive information (thread won’t be seen by users under the age of 16)',
				value: 'ageSensitive',
			});
		}

		return (
			<div>
				<Formik
					initialValues={{
						title: '',
						content: '',
						topic: this.props.predefinedTopic
							? this.props.predefinedTopic
							: '',
						situations: this.props.predefinedSituations
							? this.props.predefinedSituations
							: this.state.activeUserSituation,
						displayOptions: 'private',
						ageSensitiveOption : []

					}}
					//enableReinitialize
					validationSchema={validationFormat}
					onSubmit={values => {
						const formValues = JSON.parse(JSON.stringify(values));

						if(formValues.displayOptions==="private")
						{
							formValues['private'] = true;
						}
						else{
							formValues['private'] = false;
						}
						// If check boxes are checked
						if (formValues.ageSensitiveOption.length > 0) {
							const foundIndex=formValues.ageSensitiveOption.findIndex(val=>val==="ageSensitive");
							if(foundIndex>-1)
							{
								formValues['ageSensitive'] = true;
							}
							else
							{
								formValues['ageSensitive'] = false;
							}
							
						} else {
							formValues['ageSensitive'] = false;
						}

						delete formValues.displayOptions;
						delete formValues.ageSensitiveOption;						
						this.props.onSubmit(formValues);
					}}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};

						return (
							<form
								className="create_discussion_form form"
								onSubmit={handleSubmit}
							>
								<div className="form-row">
									<div className="form-column">
										<div className="form-helper_text theme-title hm-b8">
											<span className="font--body">
												This discussion relates to the
												following cancer experience(s)
											</span>
										</div>
										{this.state.situations.map(
											situation => {
												return this.renderSituation(
													situation,
													setFieldValue,
													values.situations,
												);
											},
										)}
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<SelectFieldFormik
											label="Topic"
											name="topic"
											options={this.props.topics}
											values={inputProps.values.topic}
											disabled={this.props.submitting}
											noEmpty={false}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Discussion title"
											name="title"
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<RichTextEditor
											toolbarId="start-discussion-toolbar"
											name="content"
											value={inputProps.values.content}
											profile={[
												'imageUpload',
												'embedVideo',
												'insertGif',
												'mention',
												'hashtag',
											]}
											bounds=".create_discussion_form"
											scrollingContainer=".modal--blog .modal-content_container"
											{...inputProps}
										/>
									</div>
								</div>
								
								{ !!discussionDisplayOptions.length && (
									<div>
									<div className="form-row">
										<div className="form-column">
											<RadioGroupFormik
												label="Display options"
												name="displayOptions"
												options={
													discussionDisplayOptions
												}
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
										<div className="form-row">
										<div className="form-column">
											<CheckboxGroupFormik
												label=""
												name={'ageSensitiveOption'}
												options={
													discussionAgeSensitiveOption
												}
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									</div>
								)}
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid ||
												this.props.submitting
											}
										>
											Post Discussion
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}

	protected renderSituation(
		userSituation: IApiSituation,
		setFieldValue: (fieldName: string, value: any) => void,
		selectedSituations: string[],
	) {
		return (
			<Pill
				label={userSituation.name}
				active={selectedSituations.indexOf(userSituation.slug) >= 0}
				disabled={
					selectedSituations.length === 1 &&
					selectedSituations.indexOf(userSituation.slug) >= 0
				}
				clickable={true}
				onClick={() => {
					this.handleSituationClick(
						userSituation,
						setFieldValue,
						selectedSituations,
					);
				}}
				key={userSituation.id}
			/>
		);
	}

	protected initialiseSituations(props: IProps) {
		const activeUserSituation = filterRootSituations(
			props.userSituations,
		).map(singleSituation => {
			return singleSituation.slug;
		});

		this.props.updatedSituations(activeUserSituation);

		return activeUserSituation;
	}

	protected handleSituationClick(
		situation: IApiSituation,
		setFieldValue: (fieldName: string, value: any) => void,
		selectedSituations: string[],
	) {
		const situationIndex = selectedSituations.indexOf(situation.slug);
		const activeUserSituation: string[] = JSON.parse(
			JSON.stringify(selectedSituations),
		);

		if (situationIndex >= 0) {
			// Remove the topic
			activeUserSituation.splice(situationIndex, 1);
		} else {
			// Add the topic
			activeUserSituation.push(situation.slug);
		}

		setFieldValue('situations', activeUserSituation);

		this.setState(
			{
				activeUserSituation,
			},
			() => {
				this.props.updatedSituations(selectedSituations);
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(StartDiscussionForm);
