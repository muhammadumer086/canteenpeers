import React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import { ISituationOption } from '../../interfaces';

interface IProps {
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	options: ISituationOption[],
	disabled?: boolean;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(field: string, isTouched: boolean, shouldValidate?: boolean): void;
}

export const CheckboxGroupSituationsFormik: React.FC<IProps> = (props: IProps) => {
	function findOptionIndex(singleOption: ISituationOption, values: ISituationOption[]) {
		return values.findIndex((singleValue) => {
			return (singleValue.slug === singleOption.slug);
		});
	}

	function handleChange(singleOption: ISituationOption, event) {
		let isChanged = false;
		const values = JSON.parse(JSON.stringify(props.values[props.name]));

		const optionIndex = findOptionIndex(singleOption, values);
		if (event.target.checked) {
			// Add option while preventing duplicates
			if (optionIndex < 0) {
				values.push(singleOption);
				isChanged = true;
			}
		} else {
			// remove option
			if (optionIndex >= 0) {
				values.splice(optionIndex, 1);
				isChanged = true;
			}
		}

		if (isChanged) {
			props.setFieldValue(props.name, values);
			props.setFieldTouched(props.name, true, true);
		}
	}

	function isChecked(singleOption: ISituationOption): boolean {
		return findOptionIndex(singleOption, props.values[props.name]) >= 0;
	}

	return (
		<FormControl
			error={props.touched[props.name] && !!props.errors[props.name]}
			className="form-input form-input-checkbox_wrapper"
		>
			<FormLabel className="form-input-label">{props.label}</FormLabel>
			<FormGroup className="form-input-checkbox_group">
				{
					props.options.map((singleOption, index) => {
						const checked = isChecked(singleOption);
						return (
							<FormControlLabel
								className="form-input-checkbox"
								control={
									<Checkbox
										className="form-input-checkbox-tick"
										name={props.name}
										checked={checked}
										onChange={(event) => {
											handleChange(singleOption, event);
										}}
										value={singleOption.slug}
										color="primary"
									/>
								}
								label={singleOption.name}
								key={index}
							/>
						);
					})
				}
			</FormGroup>
			{
				props.touched[props.name] && !!props.errors[props.name] &&
				<FormHelperText>{props.errors[props.name]}</FormHelperText>
			}
		</FormControl>
	);
};
