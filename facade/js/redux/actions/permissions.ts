import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { IApiPermission } from '../../interfaces';

import { ApiService } from '../../services';

import { apiError } from './apiError';

export const permissionsLoad = (apiService: ApiService,role:string) => (dispatch, getState: () => IStoreState) => {
	if (!getState().permission.loading) {
		dispatch({
			type: actionTypes.SITUATIONS_LOAD,
		});
        const url = `/api/roles/getRolePermissions?name=${role}`;
		apiService.queryGET(url)
		.then((res) => {
            if ('role' in res && 'permissions' in res.role) {
                const permissions: IApiPermission[] = res.role.permissions;
			dispatch({
				type: actionTypes.PERMISSIONS_LOADED,
				permissions: permissions,
            });
        }
        else{
            dispatch({
				type: actionTypes.PERMISSIONS_LOAD_ERROR,
			});
			dispatch(apiError([
				'Permissions are not loaded'
			])); 
        }
		})
		.catch((error) => {
			dispatch({
				type: actionTypes.PERMISSIONS_LOAD_ERROR,
			});
			dispatch(apiError([
				error.message
			]));
		});
	}
}
