<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->string('event_type')->nullable();
			$table->string('last_joining_date')->nullable();
			$table->string('event_time_zone')->nullable();
			$table->integer('min_age')->default(0);
			$table->integer('max_age')->default(0);
			$table->string('promo_video_url')->nullable();
			$table->boolean('is_online')->default(false);
			$table->boolean('is_travel_cover')->default(false);
			$table->integer('max_participents')->nullable();
			$table->string('discussion_id')->nullable();
			$table->string('event_admin')->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('events', function (Blueprint $table) {
			$table->dropColumn('event_type');
			$table->dropColumn('last_joining_date');
			$table->dropColumn('event_time_zone');
			$table->dropColumn('min_age');
			$table->dropColumn('max_age');
			$table->dropColumn('promo_video_url');
			$table->dropColumn('is_online');
			$table->dropColumn('is_travel_cover');
			$table->dropColumn('discussion_id');
			$table->dropColumn('max_participents');
			$table->dropColumn('event_admin');
		});
	}
}
