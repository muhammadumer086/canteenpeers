<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Discussion as DiscussionResource;

class ReportDiscussion extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$params = ['id', 'title', 'comment', 'age_sensitive', 'created_at', 'updated_at'];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		// Add the additional relationships
		$result['discussion'] = new DiscussionResource($this->discussion);
		$result['user'] = new UserResource($this->user);

		// Add the additional reports
		$result['reports'] = [];
		$additionalReports = $this->discussion
			->reportDiscussions()
			->where('id', '<>', $this->id)
			->get();
		foreach ($additionalReports as $report) {
			$result['reports'][] = [
				'id' => $report->id,
				'title' => $report->title,
				'comment' => $report->comment,
				'created_at' => $report->created_at,
				'user' => new UserResource($report->user),
			];
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);

		return $result;
	}
}
