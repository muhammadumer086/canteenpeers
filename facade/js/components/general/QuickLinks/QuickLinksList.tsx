import React from 'react';
import ClearIcon from '@material-ui/icons/Clear';
import QuickLinkItem from './QuickLinkItem';
import { IApiLink } from 'js/interfaces';

interface IProps {
	quickLinks: IApiLink[];
	handleClose(): void;
}

const QuickLinkList: React.StatelessComponent<IProps> = props => {
	return (
		<div className="quick_links_list">
			<button
				onClick={props.handleClose}
				className="close_button close_button--primary"
				title="Dismiss"
			>
				<ClearIcon className="close_button-icon" />
			</button>

			{props.quickLinks.map((link, index) => {
				return <QuickLinkItem key={index} staticLink={link} />;
			})}
		</div>
	);
};

export default QuickLinkList;
