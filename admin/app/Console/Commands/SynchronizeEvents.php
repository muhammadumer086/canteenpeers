<?php

namespace App\Console\Commands;

use App\Event;
use App\EventImage;
use App\Situation;
use App\State;
use App\Traits\CreateSlug;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class SynchronizeEvents extends Command {
	use CreateSlug;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:synchronizeEvents';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Event is synchronised with Side of Stage each hour';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		try {
			$client = new Client();

			$response = $client->request(
				'GET',
				url(config('side-of-stage.api') . '/events')
			);

			// FIXME: email_address is required but not in the response
			$rules = [
				'id' => 'bail|required|string|max:191',
				'url' => 'bail|required|string|max:191',
				'title' => 'bail|required|string|max:191',
				'description' => 'bail|required|string|max:16000000',
				'cancer_situations' => 'bail|array|exists:situations,slug',
				'start_date' => 'bail|required|date',
				'end_date' => 'bail|required|date|after_or_equal:start_date',
				'latitude' => 'bail|nullable|string|max:191',
				'longitude' => 'bail|nullable|string|max:191',
				'location' => 'bail|nullable|string|max:191',
				'location_data' => 'bail|nullable',
				'event_state' => 'bail|required|string|exists:states,slug',
				'image' =>
					'bail|url|required_with_all:image_width,image_height',
				'image_width' => 'bail|numeric',
				'image_height' => 'bail|numeric',
			];

			$events = \GuzzleHttp\json_decode($response->getBody());
			$delete_ids = [];
			$event_ids = [];
			$currentTime = Carbon::now();
			foreach ($events as $sosEvent) {
				$event = collect($sosEvent)->toArray();
				$event['situations'] = (array) $event['cancer_situations'];
				$validator = Validator::make($event, $rules);

				if ($validator->fails()) {
					$this->error($validator->errors()->first());
					throw new ValidationException($validator);
				}

				// Store Events.
				if (Carbon::parse($event['end_date'])->gt($currentTime)) {
					$this->store($event);
				} else {
					$delete_ids[] = $event['id'];
				}
				$event_ids[] = $event['id'];
			}
			// delete obsolete events
			Event::whereIn('sos_id', $delete_ids)
				->orWhere(function ($query) use ($event_ids) {
					$query
						->whereNotNull('sos_id')
						->whereNotIn('sos_id', $event_ids);
				})
				->delete();
		} catch (ValidationException $e) {
			$this->error($e->getMessage());
			Log::error($e);
		}
	}

	function store($sosEvent) {
		$situationIds = Situation::whereIn(
			'slug',
			$sosEvent['situations']
		)->pluck('id');

		$state = State::where('slug', $sosEvent['event_state'])->firstOrFail();

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();
		$content = $richTextFilter->filter($sosEvent['description']);

		if (empty($content)) {
			return response()->json(
				[
					'error' => "Content can't be empty",
				],
				400
			);
		}

		$event =
			Event::where('sos_id', $sosEvent['id'])->first() ?? new Event();

		$event->title = $sosEvent['title'];
		$event->sos_url = $sosEvent['url'];
		$event->slug = $this->createSlug($sosEvent['title'], 'App\Event');
		$event->description = $content;
		$event->start_date = $sosEvent['start_date'];
		$event->end_date = $sosEvent['end_date'];
		$event->latitude = $sosEvent['latitude'] ?? null;
		$event->longitude = $sosEvent['longitude'] ?? null;
		$event->location = $sosEvent['location'] ?? null;
		$event->location_data = isset($sosEvent['location_data'])
			? json_encode($sosEvent['location_data'])
			: null;
		$event->email_address = config('side-of-stage.event_email_address');

		$startDate = \Illuminate\Support\Carbon::instance(
			new \DateTime($sosEvent['start_date'])
		);
		$endDate = Carbon::instance(new \DateTime($sosEvent['end_date']));

		$event->overnight = $endDate->day > $startDate->day ? true : false;
		$event->weekend =
			$startDate->isWeekend() || $endDate->isWeekend() ? true : false;
		$event->daytime =
			$startDate->hour >= 9 && $startDate->hour < 17 ? true : false;
		$event->evening =
			$startDate->hour >= 17 && $startDate->hour <= 24 ? true : false;

		$event->state()->associate($state->id);
		$event->sos_id = $sosEvent['id'];
		$event->save();

		if (isset($sosEvent['image'])) {
			$eventImage =
				EventImage::where('event_id', $event->id)->first() ??
				new EventImage();
			$eventImage->path = $sosEvent['image'];
			$eventImage->event_id = $event->id;
			$eventImage->width = $sosEvent['image_width'];
			$eventImage->height = $sosEvent['image_height'];
			$eventImage->save();
		}
		$event->situations()->sync($situationIds);
		$event->save();
	}
}
