import React from 'react';
import { connect } from 'react-redux';
import { ApiService, withServices } from '../../services';

import {
	IStoreState,
	registerEventInterestModalDisplay,
} from '../../redux/store';

import { IApiUser } from '../../interfaces';

// import { ApiService } from '../../services';
import { GTMDataLayer } from '../../helpers/dataLayer';
import { Button } from '@material-ui/core';
import EventErrorModal from './EventErrorModal';

interface IProps {
	eventId: number;
	eventSlug: string;
	eventDate: string;
	eventLocation: string;
	registered: boolean | null; // ??
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	apiService: ApiService; // ??
	classes:string;
	/** Handle the case where the event is an external event */
	registerUrl?: string | null;
	dispatch(action: any): void;
	isDateOver?: boolean;
	disableRegister: boolean;
	resgisterMessage: string;
}

interface IState {
	isUpdating: boolean;
	registered: boolean;
	showErrorModal: boolean;
}

class EventRegister extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isUpdating: false,
			registered: props.registered,
			showErrorModal: false,
		};

		this.handleClick = this.handleClick.bind(this);
		this.handleClose = this.handleClose.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.registered !== prevProps.registered) {
			this.setState({
				registered: this.props.registered,
			});
		}
	}

	public render() {
		const { showErrorModal } = this.state;
		const { resgisterMessage } = this.props;
		let buttonText = this.props.userAuthenticated
			? this.state.registered
				? 'Registered'
				: 'Register my interest'
			: 'Register';

		if (this.props.userAuthenticated) {
			return (
				<React.Fragment>
					<Button
						className={`event_register-button event_register-button-trasparent event_register-button--registered ${this.props.classes}`}
						color="primary"
						variant="outlined"
						onClick={this.handleClick}
					>
						Register
					</Button>
					{showErrorModal && (
						<EventErrorModal
							handleClose={this.handleClose}
							message={resgisterMessage}
							dispatch={this.props.dispatch}
						/>
					)}
				</React.Fragment>
			);
		}

		return (
			<Button
				className={`event_register-button event_register-button-trasparent ${this.props.classes}`}
				color="primary"
				variant="outlined"
				href={`/auth/login?redirect=/events/${this.props.eventSlug}`}
			>
				{buttonText}
			</Button>
		);
	}

	protected handleClick() {
		const { disableRegister } = this.props;
		if (disableRegister) {
			this.setState({ showErrorModal: true });
			//this.props.dispatch(apiError([resgisterMessage]));
			return;
		}
		const eventSlug = this.props.eventSlug;
		const eventDate = this.props.eventDate;
		const eventLocation = this.props.eventLocation;
		this.props.dispatch(
			registerEventInterestModalDisplay(
				eventSlug,
				eventDate,
				eventLocation,
			),
		);
	}
	protected handleClose = () => {
		this.setState({ showErrorModal: false });
	};
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(EventRegister));
