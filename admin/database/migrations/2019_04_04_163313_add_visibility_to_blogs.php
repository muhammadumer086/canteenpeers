<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisibilityToBlogs extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('blogs', function (Blueprint $table) {
			$table
				->boolean('private')
				->default(false)
				->nullable()
				->after('age_sensitive');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->dropColumn('private');
		});
	}
}
