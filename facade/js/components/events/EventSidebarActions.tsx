import React from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import EventRegister from './EventRegister';

import { IStoreState, contactModalDisplay } from '../../redux/store';
import { withServices, ApiService } from '../../services';
import { IApiEvent } from '../../interfaces';
import { formatEventDate } from '../../helpers/formatEventDate';

interface IProps {
	event: IApiEvent;
	eventId: number;
	isRegistered: boolean;
	eventSlug: string;
	apiService: ApiService;
	classes?: string;
	isDateOver?: boolean;
	disableRegister: boolean;
	resgisterMessage: string;

	dispatch(action: any): void;
}

const EventSidebarActions: React.StatelessComponent<IProps> = props => {
	const formattedDate = formatEventDate(
		props.event.start_date.date,
		props.event.end_date.date,
	);
	const { disableRegister, resgisterMessage } = props;
	return (
		<React.Fragment>
			<EventRegister
				registered={props.isRegistered}
				eventId={props.eventId}
				eventSlug={props.eventSlug}
				eventDate={formattedDate}
				eventLocation={props.event.location}
				registerUrl={props.event.sos_url}
				isDateOver={props.isDateOver}
				classes={props.classes}
				disableRegister={disableRegister}
				resgisterMessage={resgisterMessage}
			/>

			<Button
				variant="contained"
				className={`event_single-contact-button ${props.classes}`}
				onClick={() => {
					props.dispatch(contactModalDisplay());
				}}
			>
				Contact Canteen
			</Button>
		</React.Fragment>
	);
};

const mapStateToProps = (_state: IStoreState) => {
	return {};
};

export default connect(mapStateToProps)(withServices(EventSidebarActions));
