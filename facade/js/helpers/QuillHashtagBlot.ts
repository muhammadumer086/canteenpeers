import Quill from 'quill';

const Embed = Quill.import('blots/embed');

export default class QuillHashtagBlot extends Embed {
	static create(data) {
		let node = super.create();
		node.innerHTML = `<span>#${data.label}</span>`;
		return node;
	}
}

QuillHashtagBlot.className = 'hashtag-blot';
QuillHashtagBlot.tagName = 'hashtag';
QuillHashtagBlot.blotName = 'hashtag';

Quill.register(
	{
		'formats/hashtag': QuillHashtagBlot,
	},
	false,
);
