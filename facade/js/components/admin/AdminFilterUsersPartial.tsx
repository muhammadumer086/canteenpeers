import React from 'react';
import { connect } from 'react-redux';

import { ISelectOption, IApiSituation } from '../../interfaces';
import { IStoreState } from '../../redux/store';

import { AutocompleteSelectFormik, TextFieldFormik } from '../inputs';

interface IProps {
	situations: IApiSituation[] | null;
	initialValues: any;
	inputProps: any;
}

const ageOptions: ISelectOption[] = [
	{
		label: 'under 12',
		value: 'under12',
	},
	{
		label: '12 - 14',
		value: '12-14',
	},
	{
		label: '15 - 17',
		value: '15-17',
	},
	{
		label: '18 - 20',
		value: '18-20',
	},
	{
		label: '21 - 25',
		value: '21-25',
	},
];
const stateOptions: ISelectOption[] = [
	{
		label: 'ACT',
		value: 'ACT',
	},
	{
		label: 'NSW',
		value: 'NSW',
	},
	{
		label: 'NT',
		value: 'NT',
	},
	{
		label: 'NZ',
		value: 'NZ',
	},
	{
		label: 'QLD',
		value: 'QLD',
	},
	{
		label: 'SA',
		value: 'SA',
	},
	{
		label: 'TAS',
		value: 'TAS',
	},
	{
		label: 'VIC',
		value: 'VIC',
	},
	{
		label: 'WA',
		value: 'WA',
	},
];

const genderOptions: ISelectOption[] = [
	{
		label: 'Female',
		value: 'female',
	},
	{
		label: 'Female Trans',
		value: 'female-trans',
	},
	{
		label: 'Male',
		value: 'male',
	},
	{
		label: 'Male Trans',
		value: 'male-trans',
	},
	{
		label: 'Other',
		value: 'other',
	},
];

const AdminFilterUsersPartial: React.FC<IProps> = (props: IProps) => {
	const situationOptions: ISelectOption[] = [];
	const cancerOptions: ISelectOption[] = [];

	if (props.situations) {
		let cancers: string[] = [];
		props.situations.forEach(situation => {
			if (situation.parent_situation === null) {
				situationOptions.push({
					label: situation.name,
					value: situation.name,
				});
			}
			if (situation.cancer_type) {
				if (cancers.indexOf(situation.cancer_type) < 0) {
					cancers.push(situation.cancer_type);
				}
			}
		});
		cancers = cancers.sort();

		cancers.forEach(cancer => {
			cancerOptions.push({
				label: cancer,
				value: cancer,
			});
		});
	}

	return (
		<React.Fragment>
			<div className="form-row">
				<div className="form-column">
					<TextFieldFormik
						type="number"
						label="User ID"
						name="user_id"
						values={props.initialValues.user_id}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<TextFieldFormik
						type="text"
						label="Full Name"
						name="name"
						values={props.initialValues.name}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<TextFieldFormik
						type="text"
						label="Username"
						name="username"
						values={props.initialValues.username}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<TextFieldFormik
						type="text"
						label="User Email"
						name="email"
						values={props.initialValues.email}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<AutocompleteSelectFormik
						label="User Age"
						name="age"
						options={ageOptions}
						values={props.initialValues.age}
						emptyLabel="Please select"
						multi={true}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<AutocompleteSelectFormik
						label="User State"
						name="state"
						options={stateOptions}
						values={props.initialValues.state}
						emptyLabel="Please select"
						multi={true}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<AutocompleteSelectFormik
						label="User Gender"
						name="gender"
						options={genderOptions}
						values={props.initialValues.gender}
						emptyLabel="Please select"
						multi={true}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<AutocompleteSelectFormik
						label="User Situation"
						name="situations"
						options={situationOptions}
						values={props.initialValues.situations}
						emptyLabel="Please select"
						multi={true}
						{...props.inputProps}
					/>
				</div>
			</div>
			<div className="form-row">
				<div className="form-column">
					<AutocompleteSelectFormik
						label="User Cancer type"
						name="cancerTypes"
						options={cancerOptions}
						values={props.initialValues.cancerTypes}
						emptyLabel="Please select"
						multi={true}
						{...props.inputProps}
					/>
				</div>
			</div>
			
		</React.Fragment>
	);
};

function mapStateToProps(state: IStoreState) {
	const { situations } = state.situations;
	return {
		situations,
	};
}

export default connect(mapStateToProps)(AdminFilterUsersPartial);
