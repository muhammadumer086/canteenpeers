import { IApiUser } from 'js/interfaces';
import moment from 'moment';
import 'moment-timezone';

const getCountry = (
	userAuthenticated: boolean,
	userData?: IApiUser,
): string => {
	// return "NZ"; //to test nz uncoment this line
	if (userAuthenticated && userData && userData.country_slug) {
		return userData.country_slug;
	}
	const nzZoneArr = moment.tz.zonesForCountry('NZ');
	const currentTz = moment.tz.guess(true);
	const foundInNz = nzZoneArr.findIndex(a => a === currentTz);
	if (foundInNz > -1) {
		return 'NZ';
	} else {
		return 'AU';
	}
};
export { getCountry };
