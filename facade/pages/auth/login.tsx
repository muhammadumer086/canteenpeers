import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';

import { LoginForm } from '../../js/components/auth/LoginForm';
import SelectUserModal from '../../js/components/auth/SelectUserModal';
import { ApiService, AuthService } from '../../js/services';

import { apiError, IStoreState, userRegistered } from '../../js/redux/store';
import { GTMDataLayer } from '../../js/helpers/dataLayer';
import { IApiUser } from '../../js/interfaces';

interface IProps {
	userData: IApiUser | null;
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	selectedUser: IApiUser;
	allUsers: IApiUser[];
	showUserModal: boolean;
}

class Login extends React.Component<IProps, IState> {
	static getInitialProps({ authService }) {
		if (authService) {
			(authService as AuthService).clearAuthCookies();
		}
		return {};
	}

	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
			selectedUser: null,
			allUsers: [],
			showUserModal: false,
		};

		this.onSubmit = this.onSubmit.bind(this);
		this.onSubmitForm2 = this.onSubmitForm2.bind(this);
		this.toggleShowUserModal = this.toggleShowUserModal.bind(this);
		this.selectUser = this.selectUser.bind(this);
	}

	public render() {
		const {
			submitting,
			allUsers,
			showUserModal,
			selectedUser,
		} = this.state;
		return (
			<div className="auth_wrapper theme--accent_dark">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<section className="auth">
					<header className="auth-header">
						<h1 className="auth-title font--h4">
							{this.state.selectedUser
								? `Welcome ${this.state.selectedUser.username}, Log In to Canteen Connect`
								: 'Log In to Canteen Connect'}
						</h1>
					</header>
					<div className="auth-content theme--main">
						{this.state.submitting && (
							<LinearProgress
								color="secondary"
								className="auth-content-progress"
							/>
						)}
						<LoginForm
							key="from1"
							selectedUser={this.state.selectedUser}
							onSubmit={this.onSubmit}
							onSubmitForm2={this.onSubmitForm2}
							submitting={submitting}
							email={
								this.props.userData
									? this.props.userData.email
									: selectedUser
									? selectedUser.email
									: ''
							}
						/>
						<div className="auth-content-footer font--small">
							Don’t have an account?{' '}
							<Link
								href="/auth/register?step=step-1"
								as="/auth/register/step-1"
							>
								<a>Register Now</a>
							</Link>
						</div>
					</div>
					<footer className="auth-footer font--small">
						Back to{' '}
						<Link href="/">
							<a>{process.env.APP_NAME}</a>
						</Link>
					</footer>
				</section>
				{showUserModal && (
					<SelectUserModal
						allUsers={allUsers}
						selectUser={this.selectUser}
						toggleModel={this.toggleShowUserModal}
					/>
				)}
			</div>
		);
	}
	protected onSubmitForm2(values) {
		const obj = {
			email: this.state.selectedUser.email,
			password: values.password,
		};
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/users/authenticate', obj)
					.then((data: any) => {
						if (
							'token_type' in data &&
							'expires_in' in data &&
							'access_token' in data &&
							'refresh_token' in data
						) {
							// Success
							this.props.authService.logIn(data).then(result => {
								if (
									document.location.search.includes(
										'?redirect=',
									)
								) {
									Router.push(
										document.location.search.split(
											'?redirect=',
										)[1],
									);
								} else if (result) {
									Router.push('/dashboard');
								} else {
									this.setState({
										submitting: false,
									});
								}
							});
						} else {
							this.setState({
								submitting: false,
							});
							throw new Error('Invalid data received');
						}
					})
					.catch(error => {
						if (error.message === 'Please verify your account') {
							this.props.dispatch(
								userRegistered(error.user as IApiUser, false),
							);
							Router.push('/auth/verifybyemail');
							return;
						}

						this.setState({
							submitting: false,
						});
						if (error.message === 'user is graduated') {
							//redirect to graduated user page
							Router.push('/graduated-user');
						} else {
							// dispatch the error message

							this.props.dispatch(apiError([error.message]));
						}
					});
			},
		);
	}
	protected onSubmit(values) {
		const obj = { email: '' };
		if (values.email === '') {
			let phoneNumber = values.phone;
			if (values.phone.charAt(0) === '0') {
				phoneNumber = values.phone.substr(1);
			}
			phoneNumber = values.code + phoneNumber;
			obj.email = phoneNumber;
		} else {
			obj.email = values.email;
		}
		const url = '/api/users/validateUser';
		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST(url, obj)
					.then(data => {
						const user: IApiUser = data.user.users[0];
						this.setState({
							submitting: false,
							selectedUser: user,
						});
					})
					.catch(error => {
						if (error.user) {
							this.setState({
								submitting: false,
								allUsers: error.user.users,
								showUserModal: true,
							});
							if (
								error.message !==
								'Multiple accounts registered with this email.'
							) {
								this.props.dispatch(apiError([error.message]));
							}
						} else {
							this.setState({ submitting: false });
							this.props.dispatch(apiError([error.message]));
						}
					});
			},
		);
	}
	protected toggleShowUserModal = () => {
		this.setState({ showUserModal: !this.state.showUserModal });
	};
	protected selectUser = (user: IApiUser) => {
		this.setState({ selectedUser: user, showUserModal: false });
	};
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}

export default connect(mapStateToProps)(Login);
