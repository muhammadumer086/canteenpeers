<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogSituationTopicTable extends Migration {
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up() {
		Schema::create('blog_situation_topic', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('blog_id');
			$table->unsignedInteger('situation_topic_id');

			$table->foreign('blog_id')
			->references('id')->on('blogs')
			->onDelete('cascade');

			$table->foreign('situation_topic_id')
			->references('id')->on('situation_topic')
			->onDelete('cascade');
		});

		Schema::table('blogs', function (Blueprint $table) {
			$table->string('feature_image')->nullable();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down() {
		Schema::dropIfExists('blog_situation_topic');

		Schema::table('blogs', function (Blueprint $table) {
			$table->dropColumn('feature_image');
		});
	}
}
