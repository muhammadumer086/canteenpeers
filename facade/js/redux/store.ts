import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

import reducers from './reducers';

import { IStoreState as apiErrorState } from './reducers/apiError';
import { IStoreState as apiSuccessState } from './reducers/apiSuccess';
import { IStoreState as contactModalState } from './reducers/contactModal';
import { IStoreState as discussionModalState } from './reducers/discussionModal';
import { IStoreState as discussionSupportState } from './reducers/discussionSupport';
import { IStoreState as eventRegisterInterestModalState } from './reducers/registerEventInterestModal';
import { IStoreState as landingVideoModalState } from './reducers/landingVideoModal';
import { IStoreState as leaveConversationModalState } from './reducers/leaveConversationModal';
import { IStoreState as menuPanelState } from './reducers/menuPanel';
import { IStoreState as newBlogModalState } from './reducers/newBlogModal';
import { IStoreState as newMessageModalState } from './reducers/newMessageModal';
import { IStoreState as quickLinksModalState } from './reducers/quickLinksModal';
import { IStoreState as reportModalState } from './reducers/reportModal';
import { IStoreState as searchModalState } from './reducers/searchModal';
import { IStoreState as settingsState } from './reducers/settings';
import { IStoreState as situationsState } from './reducers/situations';
import { IStoreState as topicsState } from './reducers/topics';
import { IStoreState as userState } from './reducers/user';
import { IStoreState as userPanelState } from './reducers/userPanel';
import { IStoreState as updateDataState } from './reducers/updateData';
import { IStoreState as notificationsState } from './reducers/notifications';
import { IStoreState as permissionState } from './reducers/permissions';

export interface IStoreState {
	apiError: apiErrorState;
	apiSuccess: apiSuccessState;
	contactModal: contactModalState;
	discussionModal: discussionModalState;
	discussionSupport: discussionSupportState;
	eventRegisterInterestModal: eventRegisterInterestModalState;
	landingVideoModal: landingVideoModalState;
	leaveConversationModal: leaveConversationModalState;
	menuPanel: menuPanelState;
	newBlogModal: newBlogModalState;
	newMessageModal: newMessageModalState;
	permission:permissionState;
	quickLinksModal: quickLinksModalState;
	reportModal: reportModalState;
	searchModal: searchModalState;
	settings: settingsState;
	situations: situationsState;
	topics: topicsState;
	user: userState;
	userPanel: userPanelState;
	notifications: notificationsState;
	updateData: updateDataState;
}

export * from './actions';

export function initializeStore(initialState) {
	return createStore(
		reducers,
		initialState,
		composeWithDevTools(applyMiddleware(thunkMiddleware)),
	);
}
