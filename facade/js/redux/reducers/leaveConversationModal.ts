import { actionTypes } from '../actions';

export interface IStoreState {
	leaveConversationModalDisplay: boolean;
	conversationId: null | number;
}

export const leaveConversationModal = (
	state: IStoreState = {
		leaveConversationModalDisplay: false,
		conversationId: null,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.LEAVE_CONVERSATION_DISPLAY:
			return Object.assign({}, state, {
				leaveConversationModalDisplay: true,
				conversationId: action.conversationId,
			});
		case actionTypes.LEAVE_CONVERSATION_HIDE:
			return Object.assign({}, state, {
				leaveConversationModalDisplay: false,
			});
		default:
			return state;
	}
};
