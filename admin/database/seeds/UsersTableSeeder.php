<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$email = 'yoann@frankdigital.com.au';

		if (!User::where('email', '=', $email)->exists()) {
			$user = User::create([
				'email' => $email,
				'password' => bcrypt('******99'), 
				'verified' => true,
				'username' => 'yoann',
				'first_name' => 'Yoann',
				'last_name' => 'Gobert',
				'location' => '',
				'location_data' => '',
				'gender' => 'male',
			]);

			// Set the user as admin
			$user->assignRole('admin');
		}
	}
}
