import React from 'react';

interface IProps {
}

const IntroductionText: React.FC<IProps> = (props) => {
	return (
		<div className="introduction_text">
			<div className="introduction_text-container">
				<p className="font--body">
					{props.children}
				</p>
			</div>
		</div>

	)
}

export default IntroductionText;
