import React from 'react';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import { IStoreState } from '../../redux/store';

import { ApiService, withServices } from '../../services';

import { IApiUser } from '../../interfaces';

interface IProps {
	userAuthenticated: boolean;
	userAdmin: boolean;
	userData: IApiUser | null;
	userPanelData: IApiUser | null;
	apiService: ApiService;
	dispatch(action: any): void;
	handleTabChange(tab: EUserPanelState): void;
}

export enum EUserPanelState {
	OVERVIEW,
	BLOCK_USER,
	UNBLOCK_USER,
	REPORT_USER,
}

interface IState {
	anchorEl: any | null;
}

class UserPanelReportUserMenu extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			anchorEl: null,
		};

		this.handleMenuOpen = this.handleMenuOpen.bind(this);
		this.handleMenuClose = this.handleMenuClose.bind(this);
		this.changeTab = this.changeTab.bind(this);
	}

	public render() {
		if (
			!this.props.userAuthenticated ||
			this.props.userPanelData.id === this.props.userData.id
		) {
			return null;
		}

		return (
			<div className="user_report">
				<IconButton
					className="user_report-button"
					onClick={this.handleMenuOpen}
				>
					<Icon>more_vert</Icon>
				</IconButton>
				<Menu
					anchorEl={this.state.anchorEl}
					open={Boolean(this.state.anchorEl)}
					onClose={this.handleMenuClose}
				>
					{!this.props.userPanelData.is_blocked && (
						<MenuItem
							onClick={() =>
								this.changeTab(EUserPanelState.BLOCK_USER)
							}
						>
							Block User
						</MenuItem>
					)}

					{!!this.props.userPanelData.is_blocked && (
						<MenuItem
							onClick={() =>
								this.changeTab(EUserPanelState.UNBLOCK_USER)
							}
						>
							Unblock User
						</MenuItem>
					)}

					<MenuItem
						onClick={() =>
							this.changeTab(EUserPanelState.REPORT_USER)
						}
					>
						Report user to Canteen
					</MenuItem>
				</Menu>
			</div>
		);
	}

	protected changeTab(tab: EUserPanelState) {
		this.props.handleTabChange(tab);
		this.handleMenuClose();
	}

	protected handleMenuOpen(event) {
		this.setState({
			anchorEl: event.currentTarget,
		});
	}

	protected handleMenuClose() {
		this.setState({
			anchorEl: null,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userAdmin, userData } = state.user;
	return {
		userAuthenticated,
		userAdmin,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(UserPanelReportUserMenu));
