<?php

namespace App\Traits;

use Illuminate\Http\Request;

use App\Helpers\AuthUserStoreHelper;

/**
 * Methods to get the auth user
 */
trait AuthUser {

	protected function getAuthUser(Request $request = null) {
		$authUserStore = AuthUserStoreHelper::Instance();
		return $authUserStore->getAuthUser($request);
	}
}
