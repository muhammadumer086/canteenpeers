import * as Yup from 'yup';
import moment from 'moment';

/**
 * Validation properties for a user
 */
export const validationsUser = {
	first_name: Yup.string()
		.required('First name is required')
		.max(191),
	last_name: Yup.string()
		.required('Last name is required')
		.max(191),
	full_name: Yup.string()
		.required('Full name is required')
		.max(191),
	username: Yup.string()
		.required('Username is required')
		.max(191),
	password: Yup.string()
		.required('Password is required')
		.min(8, 'Password must be at least 8 characters')
		.matches(/[0-9]/, 'Password must contain a number')
		.matches(
			/[^a-zA-Z\d]/,
			'Password must contain a special character (ex: @,!,#)',
		),
	email: Yup.string()
		.email('Invalid email address')
		.required('Email address is required'),
	location: Yup.string().required('Location is required'),
	location_data: Yup.mixed().required(),
	gender: Yup.mixed()
		.oneOf(
			['male', 'male-trans', 'female', 'female-trans', 'other'],
			'Invalid option selected',
		)
		.required('Gender is required'),
	self_identify: Yup.string().when('gender', {
		is: 'other',
		then: Yup.string()
			.max(191)
			.required('Please indicate how you self-identify'),
		otherwise: Yup.string()
			.max(191)
			.nullable(true),
	}),
	//phone : Yup.string(),
	phone: Yup.string().matches(
		/^(?:\+?(61|64|92))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?(\d{2}|\d{3}|\d{4}))$/,
		'Expected formats 0X XXXX XXXX or 04XX XXX XXXX',
	),
	email_or_phone :Yup.string()
	.required('Email address or phone number is required').matches(
		/^((?:\+?(61|64|92))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?(\d{2}|\d{3}|\d{4}))|([A-Za-z0-9._%\+\-]+@[a-z0-9.\-]+\.[a-z]{2,3}))$/,
		'Enter valid email or phone number',
	),
	about: Yup.string().max(500),
	indigenous_australian: Yup.bool().nullable(true),
	country_of_birth: Yup.string()
		.max(191)
		.nullable(true),
	languages: Yup.array().of(Yup.string().max(191)),
	heard_abouts: Yup.array().of(Yup.string().max(191)),
	date: Yup.date(),
	dob: Yup.string().nullable(true).required('Date Of birth is required'),
	number_children: Yup.number()
		.integer('Expected integer')
		.min(0, 'Minimum number of children 0'),
	email_null:Yup.string().email('Invalid email address'),
	phone_null:Yup.string().matches(
		/^(?:\+?(61|64|92))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?(\d{2}|\d{3}|\d{4}))$/,
		'Expected formats 0X XXXX XXXX or 04XX XXX XXXX',
	)
};

/**
 * Validation properties for a contact message
 */
export const validationMessage = {
	email: Yup.string()
		.email('Invalid email address')
		.required('Email address is required'),
	type: Yup.string().required('Please select the type of enquiry'),
	message: Yup.string().required('Message is required'),
};

export const ValidationEvent = {
	title: Yup.string().required('Event title is required'),
	description: Yup.string()
		.required('Event description is required')
		.max(5000),
	situations: Yup.array().required(),
	start_date: Yup.string().required(
		'Event start date and time is required',
	),
	end_date: Yup.string().required('Event start date and time is required'),
	last_registration_date : Yup.string().nullable().test('test','Invalid last date for registration',function (items){
		if(!items)
		{
			return true;
		}
		const current=moment().startOf('day');
		let end=this.parent.end_date?this.parent.end_date:this.parent.start_date;
		const last_date=moment(items);
		
		if(end===undefined)
		{
			end=current;
		}
		else{
			end=moment(end).startOf('day');
		}
	
		if(last_date>=current&&last_date<=end)
		{
			return true;
		}
		else{
			return false;
		}
		
	
	}),
	latitude: Yup.string().nullable(),
	longitude: Yup.string().nullable(),
	address: Yup.string().nullable(),
	state: Yup.string().required('Event state is required'),
	email_address: Yup.string()
		.email('Invalid email address')
		.required('Email address is required'),
	promo_url : Yup.string().matches(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
	'Please enter valid youtube video url'),
	eventtype : Yup.string().required('Event type is required'),
	eventAdmin : Yup.string().required('Event admin is required'),
	eventImage : Yup.string().required('Event image is required '),
	topic_id : Yup.string().required('Event discussion topic is required'),

};
