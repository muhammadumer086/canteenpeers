import React from 'react';
import { connect } from 'react-redux';
import buildUrl from 'build-url';
import {
	IStoreState,
	registerEventInterestModalHide,
	apiSuccess,
	apiError,
	updateData,
} from '../../redux/store';
import { withServices, ApiService } from '../../services';
import { ModalDialog } from '../modals/local';

import EventRegisterInterestFormik, {
	IFormValues,
} from './EventRegisterInterestFormik';
import { GTMDataLayer } from '../../helpers/dataLayer';

interface IProps {
	registerEventInterestModalData: {
		registerEventInterestModalDisplay: boolean;
		registerEventInterestModalSlug: string;
		registerEventInterestModalDate: string;
		registerEventInterestModalLocation: string;
	};
	apiService: ApiService;
	dispatch(values: any): void;
}

interface IState {
	isSubmitting: boolean;
}

class EventRegisterInterestModal extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isSubmitting: false,
		};

		this.handleClose = this.handleClose.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	public render() {
		return (
			<ModalDialog
				isActive={
					this.props.registerEventInterestModalData
						.registerEventInterestModalDisplay
				}
				modalTitle="Register your interest"
				ariaTag="event-register-interest"
				handleClose={this.handleClose}
			>
				<EventRegisterInterestFormik
					isSubmitting={this.state.isSubmitting}
					onSubmit={this.onSubmit}
				/>
			</ModalDialog>
		);
	}

	protected handleClose() {
		this.props.dispatch(registerEventInterestModalHide());
	}

	protected onSubmit(values: IFormValues) {
		if (
			this.props.registerEventInterestModalData
				.registerEventInterestModalSlug &&
			values &&
			'content' in values
		) {
			const eventSlug = this.props.registerEventInterestModalData
				.registerEventInterestModalSlug;

			this.setState(
				{
					isSubmitting: true,
				},
				() => {
					const url = buildUrl(null, {
						path: `/api/events/${eventSlug}/register`,
					});

					this.setState(
						{
							isSubmitting: true,
						},
						() => {
							this.props.apiService
								.queryPUT(url, values)
								.then(() => {
									this.setState(
										{
											isSubmitting: false,
										},
										() => {
											this.GTM.pushEventToDataLayer({
												event: 'eventRegister',
												eventSlug: this.props
													.registerEventInterestModalData
													.registerEventInterestModalSlug,
												eventDate: this.props
													.registerEventInterestModalData
													.registerEventInterestModalDate,
												eventLocation: this.props
													.registerEventInterestModalData
													.registerEventInterestModalLocation,
											});

											this.props.dispatch(updateData());
											this.props.dispatch(
												apiSuccess([
													`You have registered to this event.`,
												]),
											);
											this.props.dispatch(
												registerEventInterestModalHide(),
											);
										},
									);
								})
								.catch(error => {
									// dispatch the error message
									this.props.dispatch(
										apiError([error.message]),
									);
								});
						},
					);
				},
			);
		}
	}
}

const mapStateToProps = (state: IStoreState) => {
	const registerEventInterestModalData = state.eventRegisterInterestModal;

	return {
		registerEventInterestModalData,
	};
};

export default connect(mapStateToProps)(
	withServices(EventRegisterInterestModal),
);
