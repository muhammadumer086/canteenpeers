<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\Notification as NotificationResource;

class NotificationController extends Controller {
	const PAGINATION_SIZE = 50;
	/**
	 * Display a listing of the user's read and unread notifications.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);

		$notificationsQuery = $authUser->notifications();

		if ($request->has('filter') && !empty($request->input('filter'))) {
			$notificationsQuery->where(
				'type',
				"App\\Notifications\\{$request->input('filter')}"
			);
		}

		return new NotificationCollection(
			$notificationsQuery->paginate(self::PAGINATION_SIZE)
		);
	}

	/**
	 * Show a user's specific notification.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id) {
		$authUser = $this->getAuthUser($request);

		$notification = $authUser->notifications->where('id', $id)->first();
		$notification->markAsRead();

		return response()->json([
			'notification' => new NotificationResource($notification),
		]);
	}

	/**
	 * Display a listing of the user's read and unread notifications from the last 30 days.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function recent(Request $request) {
		$authUser = $this->getAuthUser($request);

		$notifications = $authUser
			->notifications()
			->where('created_at', '>=', Carbon::today()->subDays(30))
			->paginate(self::PAGINATION_SIZE);

		return new NotificationCollection($notifications);
	}

	/**
	 * Display count of unread notifications.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function count(Request $request) {
		$authUser = $this->getAuthUser($request);

		$count = $authUser->unreadNotifications()->count();

		return response()->json([
			'unreadNotifications' => $count,
		]);
	}
}
