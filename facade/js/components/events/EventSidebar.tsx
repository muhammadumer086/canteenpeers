import React from 'react';
import ReactSVG from 'react-svg';

import { IApiEvent, IApiDate } from '../../../js/interfaces';
import EventSidebarActions from './EventSidebarActions';
import EventAddress from './EventAddress';
// import EventViewMapLink from './EventViewMapLink';
import EventTimeDate from './EventTimeDate';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import Countdown from 'react-countdown';
import moment from 'moment';

interface IProps {
	event: IApiEvent;
	eventId: number;
	eventSlug: string;
	isRegistered: boolean;
	address: string;
	latitude?: string;
	longitude?: string;
	dateStart?: IApiDate;
	dateEnd?: IApiDate;
	classes?: string;
	togglePromoModal?(): void;
	// dispatch(action: any): void;
	disableRegister: boolean;
	resgisterMessage: string;
}

// interface IPositionData {
// 	lat: string;
// 	lng: string;
// }
const timeRendrer = ({ days, hours, minutes, seconds }) => {
	if (days > 0) {
		return (
			<div className="event_single-sidebar-remaining-time">
				{`${days} Days : ${hours} Hours : ${minutes} Min till registrations close`}{' '}
			</div>
		);
	} else if (hours > 0) {
		return (
			<div className="event_single-sidebar-remaining-time">
				{`${hours} Hours : ${minutes} Mins : ${seconds} Sec till registrations close`}{' '}
			</div>
		);
	} else {
		return (
			<div className="event_single-sidebar-remaining-time">
				{`${minutes} Mins : ${seconds} Sec till registrations close`}
			</div>
		);
	}
};
const EventSidebar = (props: IProps) => {
	const { event, disableRegister, resgisterMessage } = props;
	const { last_joining_date, end_date, start_date } = event;
	let lastJoingDateInMiliSeconds = 5000000;
	let formattedJoiningdate = moment(end_date.date).format('dddd, D MMM YYYY');
	if (last_joining_date && last_joining_date) {
		const lastDate = new Date(last_joining_date.date).valueOf();
		const now = new Date().valueOf();
		lastJoingDateInMiliSeconds = lastDate - now;
		formattedJoiningdate = moment(last_joining_date.date).format(
			'dddd, D MMM YYYY',
		);
	} else {
		const lastDate = new Date(start_date.date).valueOf();
		const now = new Date().valueOf();
		lastJoingDateInMiliSeconds = lastDate - now;
		formattedJoiningdate = moment(start_date.date).format(
			'dddd, D MMM YYYY',
		);
	}

	return (
		<div className="event_single-sidebar">
			<div className={`event_single-sidebar-container ${props.classes}`}>
				<div className="event_single-sidebar-inner-container">
					<div className="event_single-sidebar-row">
						<EventTimeDate
							dateStart={props.dateStart}
							dateEnd={props.dateEnd}
						/>
					</div>
					<hr />

					<div className="event_single-sidebar-row event_single-location">
						<EventAddress
							event={props.event}
							address={props.address}
						/>
					</div>

					<hr />
					<div className="event_single-sidebar-row hm-b8">
						<span> Registrations close on</span>
						<div className="event_single-time_date">
							<div className="event_single-icon">
								<ReactSVG
									className="event_single-sidebar-icon"
									path={`${process.env.STATIC_PATH}/icons/time.svg`}
								/>
							</div>

							<div className="event_single-time_details last_joining_date">
								<span>{formattedJoiningdate}</span>
							</div>
						</div>
						<div className="last_joining_date-over">
							{lastJoingDateInMiliSeconds <= 0 ? (
								<span>
									Event registrations closed. Contact Canteen
									below for more details.
								</span>
							) : (
								<Countdown
									date={
										Date.now() + lastJoingDateInMiliSeconds
									}
									renderer={timeRendrer}
								/>
							)}
						</div>
					</div>
					<div className="event_single-sidebar-actions">
						<EventSidebarActions
							event={props.event}
							eventId={props.eventId}
							isRegistered={props.isRegistered}
							eventSlug={props.eventSlug}
							classes={props.classes}
							disableRegister={disableRegister}
							isDateOver={lastJoingDateInMiliSeconds <= 0}
							resgisterMessage={resgisterMessage}
						/>
					</div>
					{!!props.event.sos_url && (
						<div className="event_single-sidebar-row">
							<div>
								<span className="event_single-sidebar-text">
									<a
										href={props.event.sos_url}
										target="_blank"
										rel="noopener"
										className="theme-title event_single-sidebar-link"
									>
										View on Side of Stage
									</a>
								</span>
							</div>
						</div>
					)}
				</div>
			</div>
			{event.promo_video_url && (
				<div
					className="event_single-sidebar-promo"
					onClick={props.togglePromoModal}
				>
					<PlayArrowIcon className="event_single-sidebar-promo-icon" />
					<span>Watch Promo</span>
				</div>
			)}
		</div>
	);
};

export default EventSidebar;
