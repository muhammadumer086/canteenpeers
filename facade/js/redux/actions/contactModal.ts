import { IStoreState } from '../store';
import { actionTypes } from '../actions';
import { EContactModalTypes } from '../../interfaces';

export const contactModalDisplay = (
	contactModalType: EContactModalTypes = EContactModalTypes.ABOUT_CANTEEN_CONNECT,
) => (dispatch, getState: () => IStoreState) => {
	if (getState().contactModal.contactModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.CONTACT_MODAL_DISPLAY,
		contactModalType,
	});
};

export const contactModalHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().contactModal.contactModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.CONTACT_MODAL_HIDE,
	});
};
