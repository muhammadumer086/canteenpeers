<?php

namespace App\Http\Controllers\API;

use App\Blog;
use App\Country;
use App\Discussion;
use App\HeardAbout;
use App\Helpers\CampaignMonitorSubscription;

use App\Http\Controllers\Controller;
use App\Http\Resources\BlogCollection;
use App\Http\Resources\DiscussionCollection;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\WebsiteBlogCollection;
use App\Language;
use App\LeavingReason;
use App\Like;
use App\Message;
use App\Notifications\UserBlockedNotification;
use App\PersonalSetting;
use App\Rules\ContainsNumber;
use App\Rules\SpecialChars;
use App\Situation;
use App\SituationUser;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Lcobucci\JWT\Parser;
use League\Csv\Writer;
use Log;
use Mail;
use Password;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use UserActivityHelper;
use UserPermissionHelper;
use UserVerification;
use Validator;
use Image;

class UserController extends Controller {
	const PAGINATION_SIZE = 24;

	/**
	 * Display a listing of the resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'gender' => 'bail|in:male,female,female-trans,male-trans,other',
			'situations' => 'bail|array',
			'situations.*' => 'bail|string|exists:situations,slug',
			'order_by' => 'bail|string',
			'cancer_type' => 'bail|array',
			'cancer_type.*' => 'bail|string|exists:situations,cancer_type',
			'age' => 'bail|string',
			'inactive' => 'bail|boolean',
			'login_date_from' => 'bail|date',
			'login_date_to' => 'bail|date',
			'name' => 'bail|string',
			'email' => 'bail|string',
			'per_page' => 'bail|string',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Handle filter requests

		$userQuery = User::orderBy('username', 'ASC');

		// Filter order By
		if ($request->filled('order_by')) {
			if ($request->input('order_by') === 'Z-A') {
				$userQuery = User::orderBy('username', 'DESC');
			}
		}

		// User list situations filter
		if ($request->filled('situations')) {
			$situations = DB::table('situations')
				->whereIn('slug', $request->input('situations'))
				->get();

			$situation_ids = [];
			foreach ($situations as $situation) {
				$situation_ids[] = $situation->id;

				if ($situation->parent_id !== null) {
					$situation_ids[] = $situation->parent_id;
				}
			}

			$user_ids = DB::table('situation_user')
				->whereIn('situation_id', $situation_ids)
				->pluck('user_id');

			$userQuery->whereIn('id', $user_ids);
		}

		// User gender filter
		if ($request->filled('gender')) {
			$userQuery->whereIn('gender', $request->input('gender'));
		}

		// Cancer type Filter
		if ($request->filled('cancer_type')) {
			$situations = DB::table('situations')
				->whereIn('cancer_type', $request->input('cancer_type'))
				->get();

			$situation_ids = [];
			foreach ($situations as $situation) {
				$situation_ids[] = $situation->id;

				if ($situation->parent_id !== null) {
					$situation_ids[] = $situation->parent_id;
				}
			}

			$user_ids = DB::table('situation_user')
				->whereIn('situation_id', $situation_ids)
				->pluck('user_id');

			$userQuery->whereIn('id', $user_ids);
		}

		// User age filter
		if ($request->filled('age')) {
			if ($request->input('age') === 'above20') {
				$from = Carbon::today()
					->subYears(20)
					->format('Y-m-d');
				$userQuery->whereDate('dob', '<', $from);
			} elseif ($request->input('age') === 'under20') {
				$from = Carbon::today()
					->subYears(20)
					->format('Y-m-d');
				$userQuery->whereDate('dob', '>', $from);
			} elseif ($request->input('age') === 'under12') {
				$from = Carbon::today()
					->subYears(12)
					->format('Y-m-d');
				$userQuery->whereDate('dob', '>', $from);
			} else {
				$range = explode('-', $request->input('age'));
				$from = Carbon::today()
					->subYears($range[0])
					->format('Y-m-d');
				$to = Carbon::today()
					->subYears($range[1])
					->format('Y-m-d');
				$userQuery->whereBetween('dob', [$to, $from]);
			}
		}

		// User inactive filter
		if ($request->filled('inactive')) {
			if ($request->input('inactive') === 'true') {
				$userQuery->where('inactive', '=', true);
			} else {
				$userQuery->where('inactive', false);
			}
		}

		if ($request->filled('login_date_from')) {
			$from = Carbon::createFromFormat(
				'Y-m-d',
				$request->input('login_date_from')
			)->format('Y-m-d');
			$userQuery->whereDate('last_login', '>', $from);
		}

		if ($request->filled('login_date_to')) {
			$from = Carbon::createFromFormat(
				'Y-m-d',
				$request->input('login_date_to')
			)->format('Y-m-d');
			$userQuery->whereDate('last_login', '<', $from);
		}

		// "whereLike" is a custom query macro in AppServiceProvider
		if ($request->filled('name')) {
			$userQuery->whereLike(
				['first_name', 'last_name', 'username'],
				$request->input('name')
			);
		}

		if ($request->filled('email')) {
			$userQuery->whereLike('email', $request->input('email'));
		}

		if ($request->filled('is_admin')) {
			if ($request->is_admin === true) {
				$userQuery->role('admin');
			}
		}

		// Per page filter
		$perPage = self::PAGINATION_SIZE;
		if ($request->filled('per_page')) {
			$perPage = $request->input('per_page');
		}

		$users = $userQuery->paginate($perPage);

		return new UserCollection($users);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$notAdminEmailRegex = '/\@.+(?<!canteen\.org\.(au|nz))$/';

		$validator = Validator::make($request->all(), [
			'first_name' => 'bail|required|string|max:191',
			'last_name' => 'bail|required|string|max:191',
			'username' =>
				'bail|required|alpha_dash|unique:users|max:191|not_in:me',
			'password' => [
				'bail',
				'required',
				'string',
				'min:8',
				new ContainsNumber(),
				new SpecialChars(),
			],
			'email' => 'bail|required|email|unique:users|max:191',
			'dob' => 'bail|required|date',
			'location' => 'bail|required|string',
			'location_data' => 'bail|required',
			'gender' => 'in:male,female,female-trans,male-trans,other',
			'situations' => 'bail|array',
			'situations.*' => 'bail|required|string|exists:situations,slug',
		]);

		$validator->sometimes(
			'self_identify',
			'bail|required|string|max:191',
			function ($input) {
				return $input->gender === 'other';
			}
		);

		$validator->sometimes('situations', 'required|min:1', function (
			$input
		) use ($notAdminEmailRegex) {
			return is_string($input->email) &&
				preg_match($notAdminEmailRegex, $input->email);
		});

		$validator->sometimes('parent_name', 'required|string', function (
			$input
		) {
			return $input->parent_name;
		});

		$validator->sometimes('parent_email', 'required|string', function (
			$input
		) {
			return $input->parent_email;
		});

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			// Special case for migrated/deleted accounts
			if ($request->has('email') && $request->input('email')) {
				// Try to get the user
				$error = $this->handleInvalidCreatedUser(
					$request->input('email'),
					$error
				);
			}

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		// Process the user's postcode
		// $postcode = $this->processUserPostcode($request->input('postcode_data'));

		$user = new User();
		$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->username = $request->input('username');
		$user->password = Hash::make($request->input('password'));
		$user->email = $request->input('email');
		$user->dob = $request->input('dob');
		$user->location_data = $request->input('location_data');
		$user->gender = $request->input('gender');
		$user->location = $request->input('location');
		$user->phone = $request->input('phone');
		$user->last_login = Carbon::now();
		$user->last_login_ip = $request->ip();
		if ($request->has('fcm_token')) {
			$user->fcm_token = $request->input('fcm_token');
		}
		if ($request->has('sos_registration')) {
			$user->registration_origin = 'side-of-stage';
		}

		if ($request->has('gender')) {
			if ($request->input('gender') === 'other') {
				$user->self_identify = $request->input('self_identify');
			} else {
				$user->self_identify = null;
			}
		}

		if ($request->filled('parent_name')) {
			$user->parent_name = $request->input('parent_name');
		}

		if ($request->filled('parent_email')) {
			$user->parent_email = $request->input('parent_email');
		}

		// Process the postcode data
		if ($request->has('location_data')) {
			$locationData = $this->processGoogleData(
				$request->input('location_data')
			);
			$user->country_slug = $locationData['country'];
			$user->state = $locationData['state'];
			$user->latitude = $locationData['latitude'];
			$user->longitude = $locationData['longitude'];
		}

		if (preg_match($notAdminEmailRegex, $request->input('email'))) {
			if ($user->getAge() < 12 || $user->getAge() > 25) {
				return response()->json(
					[
						'error' =>
							'You must be between the ages of 12 and 25 to use this platform.',
					],
					400
				);
			}

			$user->save();
			$user->assignRole('user');

			// Associate the user with the situations
			$situationIds = Situation::whereIn(
				'slug',
				$request->input('situations')
			)->pluck('id');
			$user->situations()->sync($situationIds);
		} else {
			$user->save();
			$user->assignRole('user');
		}

		// Associate the user with the personal settings
		$personalSettingIds = PersonalSetting::whereIn(
			'slug',
			PersonalSetting::getDefaultSettings()
		)->pluck('id');
		$user->personalSettings()->sync($personalSettingIds);

		$user->generatePreferences();
		$user->generateRoles();
		$user->save();

		// Set the subscription in campaign monitor
		$subscription = new CampaignMonitorSubscription();
		$subscription->subscribeUserToCampaignMonitor(
			$user->email,
			$user->first_name,
			$user->last_name,
			$user->location_data,
			$user->dob,
			$user->situations
		);

		// Generate the verification token for the user
		//UserVerification::generate($user);
		//UserVerification::send($user);

		$userResource = new UserResource($user);
		$userResource->forceAuth = true;

		return response()->json([
			'user' => $userResource->toArray($request),
		]);
	}

	public function register(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_add","users_management_all"]);

		if (!$this->hasAdminPermission()) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$isAdminUserType =
			$request->has('user_type') &&
			$request->input('user_type') === 'admin';

		$validator = Validator::make($request->all(), [
			'first_name' => 'required|string|max:191',
			'last_name' => 'required|string|max:191',
			'username' => 'required|alpha_dash|unique:users|max:191|not_in:me',
			'password' => [
				'required',
				'string',
				'min:8',
				new ContainsNumber(),
				new SpecialChars(),
			],
			'email' => 'required|email|unique:users|max:191',
			'dob' => 'date',
			'location' => 'string',
			'location_data' => '',
			'gender' => 'in:male,female,female-trans,male-trans,other',
			'situations' => 'array',
			'situations.*' => 'string|exists:situations,slug',
			'user_type' => 'required|string',
		]);

		$validator->sometimes(
			'self_identify',
			'bail|required|string|max:191',
			function ($input) {
				return $input->gender === 'other';
			}
		);

		$validator->sometimes('situations', 'required|min:1', function (
			$input
		) use ($isAdminUserType) {
			return !$isAdminUserType;
		});

		$validator->sometimes('parent_name', 'required|string', function (
			$input
		) {
			return $input->parent_name;
		});

		$validator->sometimes('parent_email', 'required|string', function (
			$input
		) {
			return $input->parent_name;
		});

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			// Special case for migrated/deleted accounts
			if ($request->has('email') && $request->input('email')) {
				// Try to get the user
				$error = $this->handleInvalidCreatedUser(
					$request->input('email'),
					$error
				);
			}

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$user = new User();
		$user->first_name = $request->input('first_name');
		$user->last_name = $request->input('last_name');
		$user->username = $request->input('username');
		$user->password = Hash::make($request->input('password'));
		$user->email = $request->input('email');
		$user->dob = $request->input('dob');
		$user->location_data = $request->input('location_data');
		$user->gender = $request->input('gender');
		$user->location = $request->input('location');
		$user->last_login = Carbon::now();
		$user->last_login_ip = $request->ip();
		if ($request->has('fcm_token')) {
			$user->fcm_token = $request->input('fcm_token');
		}
		if ($request->has('sos_registration')) {
			$user->registration_origin = 'side-of-stage';
		}

		if ($request->has('gender')) {
			if ($request->input('gender') === 'other') {
				$user->self_identify = $request->input('self_identify');
			} else {
				$user->self_identify = null;
			}
		}

		if ($request->filled('parent_name')) {
			$user->parent_name = $request->input('parent_name');
		}

		if ($request->filled('parent_email')) {
			$user->parent_email = $request->input('parent_email');
		}

		// Process the postcode data
		if ($request->has('location_data')) {
			$locationData = $this->processGoogleData(
				$request->input('location_data')
			);
			$user->country_slug = $locationData['country'];
			$user->state = $locationData['state'];
			$user->latitude = $locationData['latitude'];
			$user->longitude = $locationData['longitude'];
		}

		$user->save();

		if ($request->input('user_type') === 'admin') {
			//$user->assignRole('admin');
			$user->assignRole('user');
		} else {
			$user->assignRole($request->input('user_type'));

			$situationIds = Situation::whereIn(
				'slug',
				$request->input('situations')
			)->pluck('id');
			$user->situations()->sync($situationIds);
		}

		// Associate the user with the personal settings
		$personalSettingIds = PersonalSetting::whereIn(
			'slug',
			PersonalSetting::getDefaultSettings()
		)->pluck('id');
		$user->personalSettings()->sync($personalSettingIds);

		$user->generatePreferences();
		$user->generateRoles();
		$user->verified = true;
		$user->email_verified_at = Carbon::parse(date("Y-m-d H:i:s"));
		$user->save();

		// Set the subscription in campaign monitor
		$subscription = new CampaignMonitorSubscription();
		$subscription->subscribeUserToCampaignMonitor(
			$user->email,
			$user->first_name,
			$user->last_name,
			$user->location_data,
			$user->dob,
			$user->situations
		);

		$userResource = new UserResource($user);
		$userResource->forceAuth = true;

		return response()->json([
			'user' => $userResource->toArray($request),
		]);
	}

	protected function handleInvalidCreatedUser($email, $error) {
		// Try to get the user
		$userTest = User::where('email', $email)
			->withTrashed()
			->first();
		if ($userTest) {
			if ($userTest->deleted_at) {
				// Deleted account
				$error =
					'Your account has been deactivated. Please contact us to reactivate it.';
			} elseif (!$userTest->verified) {
				// User not activated
				$error =
					'An account with that email is already waiting for you. All you need to do is <a href="/auth/password-forgot">reset your password</a>!';
			}
		}
		return $error;
	}

	/**
	 * Does string contain numbers
	 *
	 * @param string $string
	 * @return boolean
	 */
	protected function stringHasNumbers($string) {
		return preg_match('/\d/', $string);
	}

	/**
	 * Does string contain spcial Characters
	 *
	 * @param string $string
	 * @return boolean
	 */
	protected function stringHasSpecialChars($string) {
		return preg_match('/[^a-zA-Z\d]/', $string);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(string $username) {
		$user = User::where('username', $username)->firstOrFail();

		return response()->json([
			'user' => new UserResource($user),
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function getUserByEmail(string $useremail) {

		$user = User::where('email', $useremail)->firstOrFail();

		return response()->json([
			'user' => new UserResource($user),
		]);
	}

	/**
	 * Display the authenticated user.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showMe(Request $request) {
		$authUser = $this->getAuthUser($request);

		return response()->json([
			'user' => new UserResource($authUser),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $username
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_edit","users_management_all"]);

		if ((!$this->hasAdminPermission()) && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();
		$validator = Validator::make($request->all(), [
			'first_name' => 'bail|string|max:191',
			'last_name' => 'bail|string|max:191',
			'new_username' => [
				'bail',
				'alpha_dash',
				Rule::unique('users', 'username')->ignore($user->id),
				'max:191',
				'not_in:me',
			],
			'new_password' => [
				'bail',
				'string',
				'min:8',
				'confirmed',
				new ContainsNumber(),
				new SpecialChars(),
			],
			'password' => [
				'bail',
				'required_with:new_password',
				'string',
				'min:8',
				new ContainsNumber(),
				new SpecialChars(),
			],
			'email' => [
				'bail',
				'email',
				Rule::unique('users')->ignore($user->id),
				'max:191',
			],
			'location' => 'bail|string',
			'location_data' => 'bail',
			'gender' => 'bail|in:male,female,female-trans,male-trans,other',
			'about' => 'bail|nullable|string|max:500',
			'situations' => 'bail|array',
			'situations.*.slug' => 'bail|string|exists:situations,slug',
			'situations.*.cancer_type' => 'bail|nullable|string',
			'situations.*.date' => 'bail|nullable|date',
			'role' => 'bail|in:admin,user',
			'ban' => 'bail|integer',
			'indigenous_australian' => 'bail|nullable|boolean',
			'country_of_birth' => 'bail|nullable|string|exists:countries,slug',
			'languages' => 'bail|array',
			'languages.*' => 'bail|string|exists:languages,slug',
			'heard_abouts' => 'bail|array',
			'heard_abouts.*' => 'bail|string|exists:heard_abouts,slug',
			'personal_settings' => 'bail|array',
			'personal_settings.*' =>
				'bail|string|exists:personal_settings,slug',
			'dob' => 'bail|nullable|date_format:Y-m-d',
			'number_children' => 'bail|nullable|integer|min:0',
			'event_preference' =>
				'bail|string|in:overnight_events,weekend_events',
		]);

		$validator->sometimes(
			'self_identify',
			'bail|required|string|max:191',
			function ($input) {
				return $input->gender === 'other';
			}
		);

		$validator->sometimes(['situations'], 'min:1', function ($input) use (
			$authUser
		) {
			return !$authUser->hasRoleName('admin');
		});

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		if ($this->hasAdminPermission() || $userHasPermission) {
			// Disable the normal Laravel timestamping
			$user->timestamps = false;

			// Manage role assignment
			if ($request->has('role') && $this->hasAdminPermission()) {
				if ($authUser->username !== $user->username) {
					if ($request->input('role') === 'admin') {
						if (!$user->hasRoleName('admin')) {
							$user->assignRole('admin');
							$user->removeRole('user');
							// Remove the user's situations
							$user->situations()->sync([]);
						}
					} else {
						if (!$user->hasRoleName('user')) {
							$user->assignRole('user');
							$user->removeRole('admin');
						}

						if (
							$user->hasRoleName('admin') &&
							$request->input('role') === 'user'
						) {
							$user->removeRole('admin');
						}
					}
					$user->generateRoles();
				} else {
					return response()->json(
						[
							'error' => "You can't change your own role",
						],
						400
					);
				}
			}
			// Handle the user ban
			if ($request->has('ban') && $request->input('ban') >= 0) {
				if ($request->input('ban') === 0) {
					$user->ban = null;
					UserActivityHelper::create(
						$user,
						$authUser,
						'User Ban Removed',
						null
					);
				} else {
					$user->ban = intval($request->input('ban'));
					// Clear all the user's tokens
					$userTokens = $user->tokens;
					foreach ($userTokens as $token) {
						$token->revoke();
					}
					UserActivityHelper::create(
						$user,
						$authUser,
						'User Banned',
						[
							'days' => $user->ban,
						]
					);
				}
			}

			// Handle graduateUser
			if ($request->has('is_graduate') && $request->input('is_graduate') >= 0) {
				if ($request->input('is_graduate') === 0) {
					$user->is_graduate = 0;
					UserActivityHelper::create(
						$user,
						$authUser,
						'Graduate User Allowed',
						true
					);
				} else {
					$user->is_graduate = intval($request->input('is_graduate'));
					// Clear all the user's tokens
					$userTokens = $user->tokens;
					foreach ($userTokens as $token) {
						$token->revoke();
					}
					UserActivityHelper::create(
						$user,
						$authUser,
						'Restricted Graduate User',
						false
					);
				}
			}
		}

		// Update the user's fields
		$fields = [
			'first_name',
			'last_name',
			'email',
			'location',
			'location_data',
			'gender',
			'phone',
			'about',
			'indigenous_australian',
			'number_children',
		];

		if (
			($request->has('first_name') &&
			$request->input('first_name') !== $user->first_name) ||
			($request->has('last_name') &&
			$request->input('last_name') !== $user->last_name)
		) {
			UserActivityHelper::create($user, $authUser, 'User Name Updated', [
				'old' => "{$user->first_name} {$user->last_name}",
				'new' => "{$request->input('first_name')} {$request->input('last_name')}",
			]);
		}

		if (
			($this->hasAdminPermission() || $userHasPermission) &&
			$request->has('dob') &&
			(!$user->dob ||
				!$request->input('dob') ||
				$request->input('dob') !== $user->dob->format('Y-m-d'))
		) {
			UserActivityHelper::create($user, $authUser, 'User DoB Updated', [
				'old' => $user->dob ? $user->dob->format('Y-m-d') : null,
				'new' => $request->input('dob') ? $request->input('dob') : null,
			]);
			$user->dob = $request->input('dob');
		}

		foreach ($fields as $singleField) {
			if ($request->has($singleField)) {
				$user->$singleField = $request->input($singleField);
			}
		}

		if (
			$request->has('email') &&
			$request->input('email') !== $user->email
		) {
			UserActivityHelper::create($user, $authUser, 'User Email Updated', [
				'old' => $user->email,
				'new' => $request->input('email'),
			]);

			$customFields = [
				'Key' => 'isemailupdated',
				'Value' => 'Yes',
			];
			$subscription = new CampaignMonitorSubscription();
			$subscription->subscribeUserToCampaignMonitor(
				$request->input('email'),
				$user->first_name,
				$user->last_name,
				$user->location_data,
				$user->dob,
				$user->situations,
				true,
				true,
				$customFields
			);

		}

		if ($request->has('new_username') && $request->input('new_username') !== $user->username) {
			$user->username = $request->input('new_username');
			UserActivityHelper::create($user, $authUser, 'User Username Updated', [
				'old' => $user->username,
				'new' => $request->input('new_username'),
			]);
		}

		if ($request->has('gender')) {
			if ($request->input('gender') === 'other') {
				$user->self_identify = $request->input('self_identify');
			} else {
				$user->self_identify = null;
			}
		}

		// Process the postcode data
		if ($request->has('location_data')) {
			$locationData = $this->processGoogleData(
				$request->input('location_data')
			);
			$user->country_slug = $locationData['country'];
			$user->state = $locationData['state'];
			$user->latitude = $locationData['latitude'];
			$user->longitude = $locationData['longitude'];
		}

		if ($request->has('fcm_token')) {
			$user->fcm_token = $request->input('fcm_token');
		}

		// Update the user's password
		if ($request->has('new_password') && $request->has('password')) {
			if (!Hash::check($request->input('password'), $user->password)) {
				return response()->json(
					[
						'error' => 'Invalid password',
					],
					400
				);
			}

			$user->password = Hash::make($request->input('new_password'));
			// Clear all the user's tokens beside the current one
			$currentTokenId = (new Parser())
				->parse($request->bearerToken())
				->getClaim('jti');
			$userTokens = $user
				->tokens()
				->where('id', '<>', $currentTokenId)
				->get();
			foreach ($userTokens as $token) {
				$token->revoke();
			}
			UserActivityHelper::create(
				$user,
				$authUser,
				'User Password Updated',
				null
			);
		}

		// Associate the user with the situations
		if ($request->has('situations')) {
			$rawSituations = $request->input('situations');
			if (is_array($rawSituations)) {
				$situationIds = [];
				$situationIdsSync = [];
				if (sizeof($rawSituations) && !$user->hasRoleName('admin')) {
					foreach ($rawSituations as $singleRawSituation) {
						$situationQuery = Situation::where(
							'slug',
							'like',
							"{$singleRawSituation['slug']}%"
						);
						if ($singleRawSituation['cancer_type']) {
							$situationQuery->where(
								'cancer_type',
								$singleRawSituation['cancer_type']
							);
						} else {
							$situationQuery->whereNull('cancer_type');
						}
						$id = $situationQuery->value('id');

						$date = isset($singleRawSituation['date'])
							? $singleRawSituation['date']
							: null;

						if ($id) {
							$situationIds[] = $id;
							$situationIdsSync[$id] = ['date' => $date];
						}
					}
				}
				UserActivityHelper::create(
					$user,
					$authUser,
					'User Cancer Experience(s) Updated',
					[
						'old' => $user->situations()->pluck('slug'),
						'new' => Situation::whereIn('id', $situationIds)->pluck(
							'slug'
						),
					]
				);
				$user->situations()->sync($situationIdsSync);
			}
		}

		// Associate the user with the country of birth
		if ($request->has('country_of_birth')) {
			if ($request->input('country_of_birth')) {
				$countryId = Country::where(
					'slug',
					$request->input('country_of_birth')
				)->value('id');
				if ($countryId) {
					$user->countryOfBirth()->associate($countryId);
				}
			} else {
				$user->countryOfBirth()->dissociate();
			}
		}

		// Associate the user with the languages
		if ($request->has('languages')) {
			$rawLanguages = $request->input('languages');
			if (is_array($rawLanguages)) {
				$languagesIds = Language::whereIn('slug', $rawLanguages)->pluck(
					'id'
				);
				$user->languages()->sync($languagesIds);
			}
		}

		// Associate the user with the heard abouts
		if ($request->has('heard_abouts')) {
			$rawHeardAbouts = $request->input('heard_abouts');
			if (is_array($rawHeardAbouts)) {
				$heardAboutsIds = HeardAbout::whereIn(
					'slug',
					$rawHeardAbouts
				)->pluck('id');
				$user->heardAbouts()->sync($heardAboutsIds);
			}
		}

		// Associate the user with the personal settings
		if ($request->has('personal_settings')) {
			$rawPersonalSettings = $request->input('personal_settings');
			if (is_array($rawPersonalSettings)) {
				$shouldSubscribe = true;
				$newsletterSubscribeSlug = PersonalSetting::$BLOG_PUBLIC;

				if (
					!array_search(
						$newsletterSubscribeSlug,
						$rawPersonalSettings
					)
				) {
					if (
						$user
							->personalSettings()
							->where(
								'personal_settings.slug',
								$newsletterSubscribeSlug
							)
							->exists()
					) {
						$shouldSubscribe = false;
					} else {
						$shouldSubscribe = null;
					}
				}

				$personalSettingIds = PersonalSetting::whereIn(
					'slug',
					$rawPersonalSettings
				)->pluck('id');
				$user->personalSettings()->sync($personalSettingIds);

				$user->generatePreferences();
				$user->save();

				$subscription = new CampaignMonitorSubscription();

				if ($shouldSubscribe) {
					$subscription->subscribeUserToCampaignMonitor(
						$user->email,
						$user->first_name,
						$user->last_name,
						$user->location_data,
						$user->dob,
						$user->situations,
						true,
						true
					);
				} elseif ($shouldSubscribe === false) {
					$subscription->unsubscribeUserFromCampaignMonitor(
						$user->email
					);
				}
			}
		}

		if ($request->has('event_preferences')) {
			$user->event_preferences = $request->input('event_preferences');
		}



		$user->save();

		return response()->json([
			'user' => new UserResource($user),
		]);
	}

	public function destroy(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission
		($authUser, ["users_management_delete","users_management_all"]);

		if (!$this->hasAdminPermission() && !$userHasPermission) {
			if ($authUser->username !== $username) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();

		$validator = Validator::make($request->all(), [
			'leaving_reasons' => 'bail|nullable|array',
			'leaving_reasons.*' => 'bail|string|max:191',
			'feedbacks' => 'bail|nullable|string|max:2500',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$leavingReasonsData = $request->only(['leaving_reasons', 'feedbacks']);

		// Create the leaving reason
		$leavingReason = new LeavingReason();
		$leavingReason->leaving_reasons_data = $leavingReasonsData;
		$leavingReason->user()->associate($user);
		$leavingReason->save();

		// Unsubscribe the user
		$subscription = new CampaignMonitorSubscription();
		$subscription->unsubscribeUserFromCampaignMonitor($user->email);

		// Delete the user
		$user->delete();

		UserActivityHelper::create($user, $user, 'User Deleted', [
			'reasons' => $request->input('leaving_reasons'),
			'feedbacks' => $request->input('feedbacks'),
		]);

		// Clear all the user access tokens
		$userTokens = $user->tokens()->get();
		foreach ($userTokens as $token) {
			$token->revoke();
		}

		return response()->json([
			'success' => true,
		]);
	}

	public function reportUser(Request $request, $username) {
		$authUser = $this->getAuthUser($request);

		$user = User::where('username', $username)->firstOrFail();

		$validator = Validator::make($request->all(), [
			'report_reason' => 'bail|required|string|max:191',
			'information' => 'bail|nullable|string|max:2500',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$reportReason = $request->input('report_reason');
		$reportInformation = $request->input('information');

		// Notify the admins
		$adminEmail = new \App\Helpers\AdminEmail();
		$adminEmail->sendUserReported(
			$user,
			$authUser,
			$reportReason,
			$reportInformation
		);
		// User::role('admin')->chunk(100, function ($admins) use (
		// 	$user,
		// 	$authUser,
		// 	$reportReason,
		// 	$reportInformation
		// ) {
		// 	foreach ($admins as $adminUser) {
		// 		if ($adminUser->id !== $authUser->id) {
		// 			$adminUser->sendUserReportNotification(
		// 				$user,
		// 				$authUser,
		// 				$reportReason,
		// 				$reportInformation
		// 			);
		// 		}
		// 	}
		// });

		return response()->json([
			'success' => true,
		]);
	}
	/**
	 * Display a listing of users in the same situation as the logged in user
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function similarUsers(Request $request) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'age' => 'bail|array|in:12-14,15-17,18-20,21-25',
			'gender' =>
				'bail|array|in:male,female,female-trans,male-trans,other',
			'situations' => 'bail|array|exists:situations,name',
			'cancerTypes' => 'bail|array|exists:situations,cancer_type',
			'location' =>
				'bail|string|in:australia,new-zealand,state,20radius,5radius',
			'anyCancer' => 'bail|boolean',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		if ($request->has('location')) {
			if (
				$request->input('location') !== 'australia' &&
				$request->input('location') !== 'new-zealand' &&
				(!$authUser->latitude &&
					!$authUser->longitude &&
					!$authUser->country_slug)
			) {
				return response()->json(
					[
						'error' =>
							'You can set your location in your profile panel.',
					],
					400
				);
			}
			if (
				$request->input('location') === 'state' &&
				$authUser->country_slug === 'NZ'
			) {
				return response()->json(
					[
						'error' => 'New Zealand is a unitary state.',
					],
					400
				);
			}
		}

		$situationIds = [];

		if ($request->has('situations')) {
			if ($request->has('cancerTypes')) {
				// Build all the situations slugs
				$situationSlugs = [];
				$cancerSituationsIds = Situation::whereIn(
					'name',
					$request->input('situations')
				)
					->whereIn('cancer_type', $request->input('cancerTypes'))
					->pluck('id');
			} else {
				$cancerSituationsIds = Situation::whereIn(
					'name',
					$request->input('situations')
				)->pluck('id');
			}

			foreach ($cancerSituationsIds as $situationId) {
				$situationIds[] = $situationId;
			}
		} elseif ($request->has('cancerTypes')) {
			// Get all the situations with the corresponding cancer types
			if ($authUser->hasRoleName('admin')) {
				$cancerSituationsIds = Situation::whereIn(
					'cancer_type',
					$request->input('cancerTypes')
				)->pluck('id');

				foreach ($cancerSituationsIds as $situationId) {
					$situationIds[] = $situationId;
				}
			} else {
				foreach ($authUser->situations as $situation) {
					$parentSituationId = $situation->id;
					if ($situation->parent_id) {
						$parentSituationId = $situation->parent_id;
					}

					$cancerSituationsIds = Situation::where(
						'parent_id',
						$parentSituationId
					)
						->whereIn('cancer_type', $request->input('cancerTypes'))
						->pluck('id');

					foreach ($cancerSituationsIds as $situationId) {
						$situationIds[] = $situationId;
					}
				}
			}
		} elseif ($authUser->hasRoleName('admin')) {
			// Case of the admin user
			$allSituations = Situation::all()->pluck('id');
			foreach ($allSituations as $situationId) {
				$situationIds[] = $situationId;
			}
		} elseif (
			$request->has('anyCancer') &&
			$request->input('anyCancer') === false
		) {
			$userSituationsIds = $authUser
				->situations()
				->pluck('situations.id');

			foreach ($userSituationsIds as $situationId) {
				$situationIds[] = $situationId;
			}
		} else {
			// Case for all cancers, grab all the situation ids based on the user's situations
			foreach ($authUser->situations as $situation) {
				$parentSituation = $situation->parentSituation
					? $situation->parentSituation
					: $situation;
				$situationIds[] = $parentSituation->id;

				foreach ($parentSituation->childSituations as $childSituation) {
					$situationIds[] = $childSituation->id;
				}
			}
		}

		$usersQuery = User::whereIn(
			'id',
			DB::table('situation_user')
				->where('user_id', '<>', $authUser->id)
				->whereIn('situation_id', $situationIds)
				->pluck('user_id')
		)->where('verified', true);

		if ($request->has('cancerTypes')) {
			$usersQuery->whereHas('personalSettings', function ($query) {
				$query->where(
					'slug',
					PersonalSetting::$ALLOW_DISPLAY_CANCER_TYPE
				);
			});
		}

		// User age
		if ($request->has('age')) {
			$usersQuery->whereNotNull('dob');
			// Generate the age array
			$ages = [];

			foreach ($request->input('age') as $age) {
				if ($age === 'over-20') {
					for ($i = 20; $i < 150; $i++) {
						$ages[] = $i;
					}
				} else {
					$range = explode('-', $age);
					for ($i = $range[0]; $i <= $range[1]; $i++) {
						$ages[] = $i;
					}
				}
			}

			$usersQuery->whereIn(DB::raw('YEAR(CURDATE()) - YEAR(dob)'), $ages);
		}

		// User gender
		if ($request->has('gender')) {
			$usersQuery
				->whereIn('gender', $request->input('gender'))
				->whereHas('personalSettings', function ($query) {
					$query->where(
						'slug',
						PersonalSetting::$ALLOW_DISPLAY_GENDER
					);
				});
		}
		// User location
		if (
			$request->has('location') &&
			$request->input('location') !== 'state' &&
			$request->input('location') !== 'australia' &&
			$request->input('location') !== 'new-zealand'
		) {
			if ($request->input('location') === 'state' && $authUser->state) {
				$usersQuery->where('state', $authUser->state);
			} elseif (
				($request->input('location') === '20radius' ||
					$request->input('location') === '5radius') &&
				$authUser->latitude !== null &&
				$authUser->longitude !== null
			) {
				$distance = $request->input('location') === '20radius' ? 20 : 5;
				$lats = [
					$this->getNewLat(
						$authUser->latitude,
						$authUser->longitude,
						-$distance
					),
					$this->getNewLat(
						$authUser->latitude,
						$authUser->longitude,
						$distance
					),
				];
				sort($lats);

				$lngs = [
					$this->getNewLng(
						$authUser->latitude,
						$authUser->longitude,
						-$distance
					),
					$this->getNewLng(
						$authUser->latitude,
						$authUser->longitude,
						$distance
					),
				];
				sort($lngs);

				$usersQuery->whereBetween('latitude', $lats);
				$usersQuery->whereBetween('longitude', $lngs);
			}
		} elseif (
			$request->has('location') &&
			($request->input('location') === 'state' ||
				$request->input('location') === 'australia' ||
				$request->input('location') === 'new-zealand')
		) {
			if ($request->input('location') === 'state') {
				if ($authUser->state) {
					$usersQuery->where('state', $authUser->state);
				}
			} elseif ($request->input('location') === 'australia') {
				$usersQuery->where('country_slug', 'AU');
			} else {
				$usersQuery->where('country_slug', 'NZ');
			}
		}

		return new UserCollection($usersQuery->paginate(self::PAGINATION_SIZE));
	}

	protected function processGoogleData($data) {
		$result = [
			'country' => null,
			'state' => null,
			'latitude' => null,
			'longitude' => null,
		];
		if (!is_array($data)) {
			$data = (array)$data;
		}
		if (
			isset($data['address_components']) &&
			is_array($data['address_components']) &&
			sizeof($data['address_components'])
		) {
			foreach ($data['address_components'] as $component) {
				if (
					isset($component['short_name']) &&
					is_string($component['short_name']) &&
					isset($component['types']) &&
					is_array($component['types'])
				) {
					if (
						in_array(
							'administrative_area_level_1',
							$component['types']
						) &&
						strlen($component['short_name']) <= 3
					) {
						$result['state'] = $component['short_name'];
					} elseif (in_array('country', $component['types'])) {
						$result['country'] = $component['short_name'];
					}
				}
			}
		}

		if (
			isset(
				$data['geometry']['location'],
				$data['geometry']['location']['lat'],
				$data['geometry']['location']['lng']
			)
		) {
			$result['latitude'] = floatval($data['geometry']['location']['lat']);
			$result['longitude'] = floatval($data['geometry']['location']['lng']);
		}

		return $result;
	}

	protected function getNewLat($lat, $lng, $km) {
		$earthRadius = 6378;
		return $lat + ($km / $earthRadius) * (180 / pi());
	}

	protected function getNewLng($lat, $lng, $km) {
		$earthRadius = 6378;
		return $lng +
			(($km / $earthRadius) * (180 / pi())) / cos(($lat * pi()) / 180);
	}

	public function uploadAvatar(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all"]);

		if (!$this->hasAdminPermission() && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();

		$messages = [
			'image' => 'An image is required',
			'max' => 'The image maximum size is 6MB',
			'dimensions' =>
				'The image must be between 100x100 and 5000x5000 pixels',
		];

		/*$validator = Validator::make(
			$request->all(),
			[
				'avatar' =>
					'required|image|max:6144|dimensions:min_width=100,min_height=100,max_width=5000,max_height=5000',
			],
			$messages
		);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}*/
		if(!$request->has('avatar'))
		{
			return response()->json(
				[
					'error' => "An image is required",
				],
				400
			);

		}
		$user->processUserAvatar($request->file('avatar'));
		// Refresh the user
		$user->refresh();

		// Return the user's resource
		return response()->json([
			'user' => new UserResource($user),
		]);
	}

	public function blocks(Request $request) {
		$authUser = $this->getAuthUser($request);

		$blockedUsers = $authUser->blockedUsers()->get();

		return new UserCollection($blockedUsers);
	}

	public function blockUser(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$blockUser = User::where('username', $username)->firstOrFail();

		if (
			!$authUser
				->blockedUsers()
				->where('blocked_user_id', $blockUser->id)
				->exists()
		) {
			$authUser->blockedUsers()->attach([$blockUser->id]);
			$authUser->touch();
			$blockUser->touch();

			// Notify all the admins
			$ageSensitive = $request->input('age_sensitive');
			$adminEmail = new \App\Helpers\AdminEmail();
			$adminEmail->sendUserBlocked($authUser, $blockUser);
			// User::role('admin')->chunk(100, function ($admins) use (
			// 	$blockUser,
			// 	$authUser
			// ) {
			// 	foreach ($admins as $adminUser) {
			// 		if ($adminUser->id !== $authUser->id) {
			// 			$adminUser->notify(
			// 				new UserBlockedNotification($authUser, $blockUser)
			// 			);
			// 		}
			// 	}
			// });
		}

		return response()->json([
			'success' => true,
		]);
	}

	public function unblockUser(Request $request, $username) {
		$authUser = $this->getAuthUser($request);

		$unblockUser = User::where('username', $username)->firstOrFail();

		if (
			$authUser
				->blockedUsers()
				->where('blocked_user_id', $unblockUser->id)
				->exists()
		) {
			$authUser->blockedUsers()->detach([$unblockUser->id]);
			$authUser->touch();
			$unblockUser->touch();
		}

		return response()->json([
			'success' => true,
		]);
	}



	public function markActive(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all"]);

		if (!$this->hasAdminPermission() && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();
		$user->inactive = false;
		$user->save();

		return response()->json([
			'active' => true,
		]);
	}

	public function markInactive(Request $request, $username) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all"]);

		if (!$this->hasAdminPermission() && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();
		$user->inactive = true;
		$user->save();

		return response()->json([
			'inactive' => true,
		]);
	}

	public function reSendVerificationEmail(Request $request, $username)
	{
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["users_management_all"]);

		if (!$this->hasAdminPermission() && $authUser->username !== $username) {
			if(!$userHasPermission) {
				return response()->json(
					[
						'error' => 'Not authorised',
					],
					400
				);
			}
		}

		$user = User::where('username', $username)->firstOrFail();

		if (!$user->verification_token) {
			UserVerification::generate($user);
		}

		$auth = [
			'api_key' => config('canteen.campaign_monitor_key'),
		];
		$smartEmailId = '7457548a-2d23-4f9b-b9b7-221ef54c2240';
		$wrap = new \CS_REST_Transactional_SmartEmail($smartEmailId, $auth);
		$message = [
			'To' => "{$user->first_name} {$user->last_name} <{$user->email}>",
			'Data' => [
				'firstname' => $user->first_name,
				'button' => config('canteen.facade_url') . '/auth/verify?' . http_build_query([
						'token' => $user->verification_token,
					])
			],
		];
		$consentToTrack = 'unchanged';
		$wrap->send($message, $consentToTrack);

		return response()->json([
			'success' => true,
		]);

	}

}
