import { IStoreState } from '../store';
import { actionTypes } from '../actions';

import { ApiService } from '../../services';

import { apiError } from './apiError';

export const topicsLoad = (apiService: ApiService, token: string|null) => (dispatch, getState: () => IStoreState) => {
	if (!getState().topics.loading) {
		dispatch({
			type: actionTypes.TOPICS_LOAD,
		});

		apiService.queryAuthGET('/api/topics', token, 'topics')
		.then((data) => {
			dispatch({
				type: actionTypes.TOPICS_LOADED,
				topics: data,
			});
		})
		.catch((error) => {
			dispatch({
				type: actionTypes.TOPICS_LOAD_ERROR,
			});
			dispatch(apiError([
				error.message
			]));
		});
	}
}
