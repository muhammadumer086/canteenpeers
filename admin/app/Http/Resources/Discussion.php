<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Situation as SituationResource;
use App\Http\Resources\Topic as TopicResource;
use App\Http\Resources\Hashtag as HashtagResource;
use App\Http\Resources\Mention as MentionResource;
use Illuminate\Support\Carbon;

class Discussion extends JsonResource {
	use \App\Traits\AuthUser;

	protected $withLatestReply = false;

	public function setWithLatestReply() {
		$this->withLatestReply = true;
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		if (!$this->withLatestReply) {
			$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
			if (
				$apiStore->hasApiResource(
					'discussion',
					$this->id,
					$this->updated_at,
					false
				)
			) {
				return $apiStore->getApiResource(
					'discussion',
					$this->id,
					$this->updated_at,
					false
				);
			}
		}

		$params = [
			'id',
			'slug',
			'title',
			'content',
			'reply_to_id',
			'main_discussion_id',
			'created_at',
			'updated_at',
			'deleted_at',
			'last_activity_at',
			'private',
			'closed',
			'likes_count',
			'hugs_count',
			'replies_count',
			'direct_replies_count',
			'followed',
			'liked',
			'hugged',
			'thread_id',
			'thread_count',
			'discussion_index',
			'age_sensitive',
			'seo',
			'for_event',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$authUser = $this->getAuthUser($request);

		// Add the additional relationships
		$result['topic'] =
			!$this->main_discussion_id && $this->topic
				? new TopicResource($this->topic)
				: null;
		$result['situations'] =
			!$this->main_discussion_id && count($this->situations)
				? SituationResource::collection($this->situations)
				: [];
		$result['user'] = new UserResource(
			$this->user()
				->withTrashed()
				->first()
		);
		$result['reply_to'] =
			$this->main_discussion_id && $this->replyToDiscussion
				? new self($this->replyToDiscussion)
				: null;
		$result['main_discussion'] =
			$this->main_discussion_id && $this->mainDiscussion
				? new self($this->mainDiscussion)
				: null;
		$result['hashtags'] =
			!$this->main_discussion_id && $this->hashtags()->count()
				? HashtagResource::collection($this->hashtags)
				: [];
		$result['mentions'] = $this->mentions()->count()
			? Mention::collection($this->mentions)
			: [];

		// Select the latest I get it user
		$result['user_likes'] = $this->likes_count
			? UserResource::collection(
				$this->userLikes()
					->where(
						'likes.user_id',
						'!=',
						$authUser ? $authUser->id : -1
					)
					->orderBy('likes.created_at', 'DESC')
					->limit(1)
					->get()
			)
			: [];

		// Select the latest hugs user
		$result['user_hugs'] = $this->hugs_count
			? UserResource::collection(
				$this->userHugs()
					->where(
						'hugs.user_id',
						'!=',
						$authUser ? $authUser->id : -1
					)
					->orderBy('hugs.created_at', 'DESC')
					->limit(1)
					->get()
			)
			: [];

		// Select the latest user to reply
		$result['user_replies'] = $this->usersInDiscussion()->count()
			? UserResource::collection(
				$this->usersInDiscussion()
					->where('user_id', '!=', $authUser ? $authUser->id : -1)
					->orderBy('users_in_discussion.id', 'DESC')
					->limit(1)
					->get()
			)
			: [];

		// Get the support count
		$result['support_count'] = $this->supportUsersInDiscussion()->count();

		if ($this->withLatestReply) {
			$result['latest_replies'] = count($this->repliesThread)
				? self::collection(
					$this->repliesThread()
						->orderBy('created_at', 'DESC')
						->limit(5)
						->get()
				)
				: [];
		}

		if ($authUser && $authUser->hasRoleName('admin')) {
			$adminParams = ['deleted_at', 'view_count', 'report_count'];
			// Add the admin values
			foreach ($adminParams as $key) {
				$result[$key] = $this->$key;
			}
		}


		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);
		$result['last_activity_at'] = UserPermissionHelper::formatCarbonDate($result['last_activity_at']);
		$result['deleted_at'] = UserPermissionHelper::formatCarbonDate($result['deleted_at']);


		if (!$this->withLatestReply) {
			$apiStore->setApiResource(
				$result,
				'discussion',
				$this->id,
				$this->updated_at,
				false
			);
		}

		return $result;
	}

}
