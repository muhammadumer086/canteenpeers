import {IApiPermission} from "../interfaces";
const checkPermission = (permissions: IApiPermission[], permission: string,all?:string): boolean => {
   // return true;
    const permissionIndex = permissions.findIndex(p => p.name === permission);
    if (permissionIndex > -1) {
        return true;
    }
    else{
        if(all){
            const allIndex=permissions.findIndex(p=>p.name===all);
            if(allIndex>-1){
                return true;
            }
        }
        return false;
    }
   
};
export { checkPermission };