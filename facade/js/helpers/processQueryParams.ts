/**
 * Process query params
 *
 * Transform entries like
 * `{
 *     'foo': ['bar', 'baz']
 * }`
 * to
 * `{
 *     'foo[0]': 'bar',
 *     'foo[1]': 'baz'
 * }`
 */
export function processQueryParams(filters: any): any {
	const params: any = {};
	if (filters) {
		for (const key in filters) {
			if (filters.hasOwnProperty(key)) {
				if (filters[key] instanceof Array) {
					filters[key].forEach(
						(filterValue: string, index: number) => {
							params[`${key}[${index}]`] = filterValue;
						},
					);
				} else {
					if (filters[key] !== null && filters[key] !== '') {
						params[key] = filters[key];
					}
				}
			}
		}
	}

	return params;
}
