<?php

use Illuminate\Database\Seeder;

use App\State;

class StatesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$states = [
			'NSW' => 'New South Wales',
			'QLD' => 'Queensland',
			'QLD-NORTH' => 'Queensland North',
			'SA' => 'South Australia',
			'TAS' => 'Tasmania',
			'VIC' => 'Victoria',
			'WA' => 'Western Australia',
			'ACT' => 'Australian Capital Territory',
			'NT' => 'Northern Territory',
			'NZ' => 'New Zealand',
			'ONLINE' => 'Online',
		];

		foreach ($states as $abbreviation => $name) {
			$slug = str_slug($abbreviation, '-');

			State::updateOrCreate(
				[
					'slug' => $slug,
				],
				[
					'name' => $name,
					'abbreviation' => $abbreviation
				]
			);
		}
	}
}
