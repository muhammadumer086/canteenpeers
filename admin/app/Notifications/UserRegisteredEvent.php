<?php

namespace App\Notifications;

use App\Event;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserRegisteredEvent extends Notification {
	use Queueable;

	protected $user;
	protected $event;
	protected $phone;
	protected $content;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($user, $event, $phone, $content) {
		$this->user = $user;
		$this->event = $event;
		$this->phone = $phone;
		$this->content = $content;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "{$this->user->first_name} {$this->user->last_name} has registered for {$this->event->title}";

		return (new MailMessage())
			->subject($title)
			->view('emails.user-registered-event', [
				'user' => $this->user,
				'event' => $this->event,
				'phone' => $this->phone,
				'content' => $this->content,
			]);
	}
}
