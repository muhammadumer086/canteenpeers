<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Discussion as DiscussionResource;

class SupportUsersInDiscussion extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'support_users_in_discussion',
				$this->id,
				$this->updated_at,
				false
			)
		) {
			return $apiStore->getApiResource(
				'support_users_in_discussion',
				$this->id,
				$this->updated_at,
				false
			);
		}

		$params = ['reply', 'hug', 'like'];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		if ($this->user) {
			$result['user'] = new UserResource($this->user);
		} else {
			$result['user'] = null;
		}

		if ($this->discussion) {
			$result['discussion'] = new DiscussionResource($this->discussion);
		} else {
			$result['discussion'] = null;
		}

		$apiStore->setApiResource(
			$result,
			'support_users_in_discussion',
			$this->id,
			$this->updated_at,
			false
		);

		return $result;
	}
}
