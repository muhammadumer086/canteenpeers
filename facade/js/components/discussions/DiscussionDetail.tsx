import React from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import ReactSVG from 'react-svg';

import Icon from '@material-ui/core/Icon';

import DiscussionGetIt from './DiscussionGetIt';
import DiscussionFollow from './DiscussionFollow';

import { IStoreState } from '../../redux/store';

import { ApiService, withServices } from '../../services';

import { IApiDiscussion, IApiTopic, IApiUser } from '../../interfaces';
import { ButtonLink } from '../general/ButtonLink';

import DiscussionReplyCount from './DiscussionReplyCount';
import DiscussionReport from './DiscussionReport';
import Pill from '../general/Pill';
import Flag from '../general/Flag';
import TileAuthor from '../general/TileAuthor';
import { DiscussionSupportMeta } from './DiscussionSupportMeta';

interface IProps {
	discussion: IApiDiscussion;
	userAdmin: boolean;
	topic: IApiTopic;
	userData: IApiUser;
	isUpdating: boolean;
	isFollowing: boolean;
	userAuthenticated: boolean | null;
	apiService: ApiService;
	editDiscussion(): void;
	deleteDiscussion(): void;
	replyToDiscussion(event, replyTo?): void;
	dispatch(action: any): void;
	scrollToReplies(): void;
}

interface IState {}

class DiscussionDetail extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {};

		this.updateHTML = this.updateHTML.bind(this);
	}

	public componentDidMount() {
		this.updateHTML();
	}

	public render() {
		return (
			<div className="theme--main">
				<div className="discussion_detail-content_container">
					<div className="discussion_detail">
						{this.props.discussion.topic && (
							<div className="discussion_detail-topic">
								<Pill
									label={this.props.discussion.topic.title}
									active={true}
									disabled={false}
									clickable={true}
									withoutBorder={true}
									href={`/discussions?topic=${
										this.props.discussion.topic.slug
									}`}
								/>
							</div>
						)}

						<div className="discussion_detail-title_container">
							<h1 className="discussion_detail-title font--h4 theme-title hp-r64">
								{this.props.discussion.title}
							</h1>
						</div>

						<div className="discussion_detail-flags">
							{this.props.discussion.age_sensitive && (
								<Flag>Age sensitive</Flag>
							)}

							{this.props.discussion.private && (
								<Flag>
									<ReactSVG
										path={`${
											process.env.STATIC_PATH
										}/icons/locked.svg`}
									/>
									Members
								</Flag>
							)}
						</div>

						<div className="discussion_details-author_details hm-b24">
							<TileAuthor
								userData={this.props.discussion.user}
								discussionData={this.props.discussion}
							/>
						</div>

						<div className="discussion_detail-content font--body">
							<div
								dangerouslySetInnerHTML={{
									__html: this.props.discussion.content,
								}}
							/>
							{!!this.props.discussion.reports_count && (
								<div className="discussion_detail-report hm-t16">
									<Icon>warning</Icon>
									{this.props.discussion.reports_count > 1 ? (
										<span>
											Post reported{' '}
											{
												this.props.discussion
													.reports_count
											}{' '}
											times
										</span>
									) : (
										<span>Post reported once</span>
									)}
								</div>
							)}
						</div>

						<div className="discussion_details-content-discussion_support_meta">
							<DiscussionSupportMeta
								discussion={this.props.discussion}
							/>
						</div>

						<div className="discussion_details-content-meta">
							<div className="discussion_details-content-meta-item">
								<DiscussionGetIt
									type="iGetIt"
									count={this.props.discussion.likes_count}
									discussionId={this.props.discussion.id}
									discussionTitle={
										this.props.discussion.title
									}
									liked={this.props.discussion.liked}
									disabled={!this.props.userAuthenticated}
									apiService={this.props.apiService}
								/>
							</div>

							<div className="discussion_details-content-meta-item">
								<DiscussionGetIt
									type="hug"
									count={this.props.discussion.hugs_count}
									discussionId={this.props.discussion.id}
									discussionTitle={
										this.props.discussion.title
									}
									liked={this.props.discussion.hugged}
									disabled={!this.props.userAuthenticated}
									apiService={this.props.apiService}
								/>
							</div>

							<div className="discussion_details-content-meta-item">
								<DiscussionReplyCount
									count={this.props.discussion.replies_count}
									userAuthenticated={
										this.props.userAuthenticated
									}
									scrollToReplies={this.props.scrollToReplies}
								/>
							</div>

							{this.props.userAuthenticated && (
								<div className="discussion_details-content-meta-item">
									<DiscussionReport
										discussion={this.props.discussion}
										handleDelete={() => {
											Router.push('/discussions');
										}}
										withAgeSensitive={true}
										editDiscussion={
											this.props.editDiscussion
										}
										deleteDiscussion={
											this.props.deleteDiscussion
										}
										canEdit={true}
									/>
								</div>
							)}
						</div>
					</div>

					<div className="discussion_detail-sidebar">
						{this.props.userAuthenticated ? (
							<DiscussionFollow
								className="discussion_detail-button"
								discussionId={this.props.discussion.id}
								discussionTitle={this.props.discussion.title}
								followed={this.props.isFollowing}
								disabled={!this.props.userAuthenticated}
								apiService={this.props.apiService}
							/>
						) : (
							<ButtonLink
								variant="contained"
								color="primary"
								className="discussion_detail-button discussion_detail-button--login"
								href={`/auth/login?redirect=/discussions/${
									this.props.discussion.slug
								}`}
							>
								Log In To Reply
							</ButtonLink>
						)}
					</div>
				</div>
			</div>
		);
	}

	protected updateHTML() {
		const iframes = document.querySelectorAll('.ql-video');

		if (iframes.length) {
			iframes.forEach(element => {
				const wrapper = document.createElement('div');
				wrapper.classList.add('discussion_detail-iframe_wrapper');
				element.classList.add('discussion_detail-iframe');

				element.parentNode.insertBefore(wrapper, element);
				wrapper.appendChild(element);
			});
		}
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(withServices(DiscussionDetail));
