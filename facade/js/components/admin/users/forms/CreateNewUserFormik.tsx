import React from 'react';
import { Formik } from 'formik';
import moment from 'moment';
import * as Yup from 'yup';
import Button from '@material-ui/core/Button';

import {
	//SelectFieldFormik,
	TextFieldFormik,
	InlineDatePickerFormik,
	RadioGroupFormik,
	CheckboxGroupFormik,
	AutocompletePostcodeFormik,
} from '../../../inputs';

import { ISelectOption } from '../../../../interfaces';
import { validationsUser } from '../../../../helpers/validations';

interface IProps {
	submitting: boolean;
	roles: ISelectOption[];
	situations: ISelectOption[];
	onSubmit(values: any): void;
}

interface IState {
	isAdmin: boolean;
}

export interface ICreateUserFormik {
	user_type: string;
	first_name: string;
	last_name: string;
	username: string;
	email: string;
	dob: string;
	location_data: any;
	location: string;
	password: string;
	self_identify?: string;
	situations?: string[];
	gender: string;
}

class CreateNewUserFormik extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			isAdmin: false,
		};

		this.handleAdminChange = this.handleAdminChange.bind(this);
	}

	public render() {
		let validationSchema = Yup.object().shape({
			user_type: Yup.string().required(),
			first_name: validationsUser.first_name,
			last_name: validationsUser.last_name,
			username: validationsUser.username,
			password: validationsUser.password,
			email: validationsUser.email,
			dob: validationsUser.dob,
			location: validationsUser.location,
			location_data: validationsUser.location_data,
			gender: validationsUser.gender,
			self_identify: validationsUser.self_identify,
			situations: Yup.array()
				.of(
					Yup.mixed().oneOf(
						this.props.situations.map(singleOption => {
							return singleOption.value;
						}),
					),
				)
				.required('Please select at least one option'),
		});

		if (this.state.isAdmin) {
			validationSchema = Yup.object().shape({
				user_type: Yup.string().required(),
				first_name: validationsUser.first_name,
				last_name: validationsUser.last_name,
				username: validationsUser.username,
				password: validationsUser.password,
				email: validationsUser.email,
				dob: validationsUser.dob,
				location: validationsUser.location,
				location_data: validationsUser.location_data,
				gender: validationsUser.gender,
				self_identify: validationsUser.self_identify,
				situations: Yup.array(),
			});
		}

		//const { roles: rolesOptions } = this.props;

		return (
			<Formik
				initialValues={{
					user_type: 'user',
					first_name: '',
					last_name: '',
					username: '',
					password: '',
					email: '',
					dob: '',
					location: '',
					location_data: {},
					gender: '',
					self_identify: '',
					situations: [],
				}}
				validationSchema={validationSchema}
				onSubmit={(values: ICreateUserFormik) => {
					if ('dob' in values && values.dob) {
						values.dob = moment(values.dob).format('YYYY-MM-DD');
					}

					if (this.state.isAdmin) {
						values['situations'] = [];
					}
					//console.log(values);
					this.props.onSubmit(values);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
						isValid,
					};

					const withoutHandleChangeProps = {
						values,
						touched,
						errors,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form
							onSubmit={handleSubmit}
							className="create_staff_form form"
						>
							{/* <div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="User Role"
										name="user_type"
										options={rolesOptions}
										values={inputProps.values.user_type}
										disabled={this.props.submitting}
										noEmpty={true}
										{...withoutHandleChangeProps}
										handleChange={event => {
											this.handleAdminChange(event);
										}}
									/>
								</div>
							</div> */}

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="First name"
										name="first_name"
										disabled={this.props.submitting}
										{...inputProps}
									/>
								</div>

								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Last name"
										name="last_name"
										disabled={this.props.submitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Username (not your real name)"
										name="username"
										disabled={this.props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="password"
										label="Create a password"
										name="password"
										disabled={this.props.submitting}
										helperText="Password must be at least 8 characters, must contain a number and a special character (ex: @,!,#)"
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="email"
										label="Email"
										name="email"
										disabled={this.props.submitting}
										{...withoutHandleChangeProps}
										handleChange={e => {
											handleChange(e);
										}}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<InlineDatePickerFormik
										label="Your Date of Birth"
										name="dob"
										format="DD/MM/YYYY"
										disabled={this.props.submitting}
										disableFuture={true}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<AutocompletePostcodeFormik
										type="postcode"
										label="Suburb or postcode"
										name="location"
										name_data="location_data"
										disabled={this.props.submitting}
										{...inputProps}
									/>
								</div>
							</div>

							<div className="form-row">
								<div className="form-column">
									<RadioGroupFormik
										label="Gender"
										name="gender"
										options={[
											{
												label: 'Female',
												value: 'female',
											},
											{
												label: 'Female Trans',
												value: 'female-trans',
											},
											{
												label: 'Male',
												value: 'male',
											},
											{
												label: 'Male Trans',
												value: 'male-trans',
											},
											{
												label:
													'These options don’t describe me',
												value: 'other',
											},
										]}
										disabled={this.props.submitting}
										{...inputProps}
									/>
								</div>
							</div>

							{values['gender'] === 'other' && (
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Please let us know how you self-identify"
											name="self_identify"
											disabled={false}
											{...inputProps}
										/>
										<br />
										<br />
									</div>
								</div>
							)}

							{inputProps.values.user_type !== 'admin' && (
								<div className="form-row">
									<div className="form-column">
										<CheckboxGroupFormik
											label="What best describes the user's cancer experience?"
											name="situations"
											options={this.props.situations}
											disabled={this.props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
							)}

							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid || this.props.submitting
										}
									>
										Create User
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		);
	}

	protected handleAdminChange(event: React.ChangeEvent<any>) {
		this.setState({
			isAdmin: !!(event.target.value as string).match('admin'),
		});
	}
}

export default CreateNewUserFormik;
