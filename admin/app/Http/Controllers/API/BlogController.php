<?php

namespace App\Http\Controllers\API;

use App\Blog;
use App\PrismicLinkResolver;
use App\Situation;
use App\SituationUser;
use App\SituationTopic;
use App\Topic;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use Validator;
use UserActivityHelper;

use Prismic\Dom\RichText;
use Prismic\Api;
use Prismic\LinkResolver;
use Prismic\Predicates;

use App\Http\Resources\Blog as BlogResource;
use App\Http\Resources\BlogCollection;
use App\Notifications\NewBlogArticleNotification;
use App\Hashtag;

use Carbon\Carbon;
use UserPermissionHelper;

class BlogController extends Controller {
	use \App\Traits\CreateSlug;

	const PAGINATION_SIZE = 20;
	const FEATURED_BLOGS = 3;

	/**
	 * Return list of all Blogs
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["blogs_read"]);
		$featureBlogIds = [];
		$situationIds = [];
		$filterBySituation = new \App\Helpers\FilterByUserSituation();

		$validator = Validator::make($request->all(), [
			'topics' => 'bail|array|exists:topics,slug',
			'situations' => 'bail|array|exists:situations,name',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$blogsQuery = Blog::orderBy('last_publication_date', 'DESC');

		if (!$authUser) {
			$blogsQuery->where('private', false)->where('age_sensitive', false);
		} elseif ($authUser->isUnderage()) {
			$blogsQuery->where('age_sensitive', false);
		}

		if ($request->has('situations') && !$userHasPermission) {
			// $situationIds = DB::table('situations')
			// 	->whereIn('name', $request->input('situations'))
			// 	->whereNull('parent_id')
			// 	->pluck('id');
			$situationIds = $filterBySituation->addSituationsMap(
				$request->input('situations')
			);

			$blogsIds = DB::table('blog_situation')
				->whereIn('situation_id', $situationIds)
				->pluck('blog_id');

			$blogsQuery->whereIn('id', $blogsIds);
		} elseif (
			$authUser &&
			!$authUser->hasRoleName('admin') &&
			!$request->has('anySituation')
		) {
			// No situation filters selected, use situation of current user
			$blogsQuery = $filterBySituation->filterByUserSituation(
				$authUser,
				$blogsQuery,
				'blogs',
				'blog_situation',
				'blog_id'
			);
		}

		// Filtering by topic
		if ($request->has('topics')) {
			$blogsQuery->whereIn(
				'topic_id',
				DB::table('topics')
					->whereIn(
						'id',
						Topic::whereIn(
							'slug',
							$request->input('topics')
						)->pluck('id')
					)
					->pluck('id')
			);
		}

		// If no filters hide featured from results list
		if (
			!$request->has('topics') &&
			!$request->has('situations') &&
			!$request->has('type')
		) {
			// Get first the two newest feature blogs
			$featureBlogsQuery = Blog::where('featured', true);

			if (
				$authUser &&
				!$authUser->hasRoleName('admin') &&
				sizeof($situationIds)
			) {
				$featureBlogsQuery->whereIn(
					'blogs.id',
					DB::table('blog_situation')
						->whereIn('situation_id', $situationIds)
						->pluck('blog_id')
				);
			}

			// Small hack to allow editor to push back some blogs to the top
			$featureBlogIds = $featureBlogsQuery
				->orderBy('last_publication_date', 'DESC')
				->limit(self::FEATURED_BLOGS)
				->pluck('id');

			$blogsQuery->whereNotIn('id', $featureBlogIds);
		}

		$blogs = $blogsQuery->paginate(self::PAGINATION_SIZE);

		$collection = new BlogCollection($blogs);

		if (sizeof($featureBlogIds)) {
			$collection->setFeaturedBlogIds(
				Blog::whereIn('id', $featureBlogIds)->get()
			);
		}

		return $collection;
	}

	/**
	 * Create a new Blog.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'title' => 'bail|required|string|max:191',
			'content' => 'bail|required|string|max:16000000',
			'topic' => 'bail|required|string|exists:topics,slug',
			'situations' => 'bail|array|exists:situations,slug',
			'ageSensitive' => 'boolean',
			'private' => 'boolean',
			'featured' => 'boolean',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$richTextFilter = new \App\Helpers\RichTextFilterHelper();
		$blogContent = $richTextFilter->filter($request->input('content'));

		if (empty($blogContent)) {
			return response()->json(
				[
					'error' => "Content can't be empty",
				],
				400
			);
		}

		$blog = new Blog();
		$blog->user_id = $authUser->id;
		$blog->title = $request->input('title');
		$blog->slug = $this->createSlug($request->input('title'), 'App\Blog');
		$blog->content = $blogContent;

		if ($request->input('ageSensitive')) {
			$blog->age_sensitive = $request->input('ageSensitive');
		}

		if ($request->input('private')) {
			$blog->private = $request->input('private');
		}

		if ($request->input('feature_image')) {
			$blog->feature_image = $request->input('feature_image');
		}

		if ($request->has('featured') && $this->hasAdminPermission()) {
			$blog->featured = $request->featured;
		} else {
			$blog->featured = false;
		}

		$blog->topic_id = $this->getBlogTopicIdBySlug($request->input('topic'));

		$blog->first_publication_date = Carbon::now();
		$blog->last_publication_date = Carbon::now();

		$blog->save();

		// Attach hashtag(s)
		Hashtag::attachHashtags($blog);

		// Load the discussion user
		$blog->user;

		// Create situation relationship for every situation the user is in or selected situations
		if ($request->has('situations')) {
			foreach ($request->input('situations') as $slug) {
				$situation = Situation::where('slug', $slug)->first();
				$blog->situations()->syncWithoutDetaching([$situation->id]);
			}
		} else {
			$situations = Situation::whereIn(
				'id',
				SituationUser::where('user_id', $user->id)->pluck('id')
			)->pluck('slug');

			foreach ($situations as $slug) {
				$situation = Situation::where('slug', $slug)->first();
				if ($situation->parent_id) {
					$blog
						->situations()
						->syncWithoutDetaching([$situation->parent_id]);
				} else {
					$blog->situations()->syncWithoutDetaching([$situation->id]);
				}
			}
		}

		// Allow to push hashtags to algolia
		$blog->save();

		UserActivityHelper::create($blog->user, $blog, 'Blog Created', null);

		$users = User::all();
		foreach ($users as $user) {
			if ($user->allowNotificationNewBlog()) {
				Notification::send(
					$user,
					new NewBlogArticleNotification($blog)
				);
			}
		}

		return response()->json([
			'blog' => new BlogResource($blog),
		]);
	}

	/**
	 * Display the blog by slug from the laravel database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  string  $slug
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $slug) {
		$authUser = $this->getAuthUser($request);
		$blog = Blog::where('slug', $slug)->firstOrFail();

		$relatedBlogs = Blog::where([
			['id', '<>', $blog->id],
			['topic_id', '=', $blog->topic_id],
		])
			->inRandomOrder()
			->limit(2)
			->get();

		if (!is_null($authUser)) {
			if ($authUser->isUnderage() && $blog->age_sensitive) {
				return response()->json(
					[
						'error' => 'Not of age',
					],
					400
				);
			}

			UserActivityHelper::create($authUser, $blog, 'Blog Read', null);
		}

		return response()->json([
			'blog' => new BlogResource($blog),
			'related' => BlogResource::collection($relatedBlogs),
		]);
	}

	public function update(Request $request, $slug) {
		$authUser = $this->getAuthUser($request);
		$blog = Blog::where('slug', $slug)->firstOrFail();

		if (!$this->hasAdminPermission() && $authUser->id !== $blog->user->id) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$validator = Validator::make($request->all(), [
			'title' => 'string|max:191',
			'content' => 'string|max:16000000',
			'topic' => 'string|exists:topics,slug',
			'situations' => 'array|exists:situations,slug',
			'ageSensitive' => 'bail|required|boolean',
			'private' => 'bail|required|boolean',
			'featured' => 'boolean',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		if ($request->has('title')) {
			$blog->title = $request->title;
		}

		if ($request->has('content')) {
			$richTextFilter = new \App\Helpers\RichTextFilterHelper();
			$blogContent = $richTextFilter->filter($request->input('content'));

			if (empty($blogContent)) {
				return response()->json(
					[
						'error' => "Content can't be empty",
					],
					400
				);
			}

			$blog->content = $blogContent;
		}

		if ($request->has('private')) {
			$blog->private = $request->private;
		}

		if ($request->has('ageSensitive')) {
			$blog->age_sensitive = $request->ageSensitive;
		}

		if ($request->has('featured') && $this->hasAdminPermission()) {
			$blog->featured = $request->featured;
		}

		$blog->topic_id = $this->getBlogTopicIdBySlug($request->topic)
			? $this->getBlogTopicIdBySlug($request->topic)
			: $blog->topic_id;
		$blog->feature_image = $request->feature_image
			? $request->feature_image
			: null;
		$blog->last_publication_date = Carbon::now();

		// Attach hashtag(s)
		Hashtag::attachHashtags($blog);

		// Load the discussion user
		$user = $blog->user;

		// Create situation relationship for every situation the user is in or selected situations
		if ($request->has('situations')) {
			foreach ($request->input('situations') as $slug) {
				$situation = Situation::where('slug', $slug)->first();
				$blog->situations()->syncWithoutDetaching([$situation->id]);
			}
		} else {
			$situations = Situation::whereIn(
				'id',
				SituationUser::where('user_id', $user->id)->pluck('id')
			)->pluck('slug');

			foreach ($situations as $slug) {
				$situation = Situation::where('slug', $slug)->first();
				if ($situation->parent_id) {
					$blog
						->situations()
						->syncWithoutDetaching([$situation->parent_id]);
				} else {
					$blog->situations()->syncWithoutDetaching([$situation->id]);
				}
			}
		}

		// Allow to push hashtags to algolia
		$blog->save();

		UserActivityHelper::create($blog->user, $blog, 'Blog Updated', null);
		UserActivityHelper::create($authUser, $blog, 'Updated Blog', null);

		return response()->json([
			'blog' => new BlogResource($blog),
		]);
	}

	/**
	 * Show the blogs authored by a user.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function authored(Request $request, $id) {
		$authUser = $this->getAuthUser($request);

		$blog = Blog::where('user_id', '=', $id)
			->with('user')
			->get();

		return response()->json([
			'discussions' => $blog,
		]);
	}

	/**
	 * Delete a blog
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request, $slug) {
		$authUser = $this->getAuthUser($request);
		$blog = Blog::where('slug', $slug)->firstOrFail();

		// Check logged in user has permissions
		if (!$this->hasAdminPermission() && $authUser->id !== $blog->user->id) {
			return response()->json(
				[
					'error' => 'Not authorised',
				],
				400
			);
		}

		$blog->delete();

		UserActivityHelper::create($blog->user, $blog, 'Blog Deleted', null);
		UserActivityHelper::create($authUser, $blog, 'Deleted Blog', null);

		return response()->json([
			'success' => true,
		]);
	}

	public function share(Request $request, Blog $blog) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'medium' => [
				'bail',
				'required',
				Rule::in([
					'community',
					'facebook',
					'twitter',
					'copy link',
					'email',
				]),
			],
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		UserActivityHelper::create($authUser, $blog, 'Shared Blog', [
			'medium' => $request->input('medium'),
		]);

		return response()->json([
			'shared' => true,
		]);
	}

	/**
	 * Get a topic by slug in category blogs.
	 *
	 * @param  string  $slug
	 * @return topic_id
	 */
	protected function getBlogTopicIdBySlug($slug) {
		$topic = Topic::where('slug', $slug)
			->where('category', 'blogs')
			->first();

		if ($topic) {
			return $topic->id;
		}
		return false;
	}
}
