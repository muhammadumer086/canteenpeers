<?php

namespace App;

use App\StaffImage;
use App\Team;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
	protected $guarded = [];

	protected $hidden = [
		'pivot',
		'created_at',
		'updated_at',
	];

    public function team() {
    	return $this->belongsTo(Team::class);
    }

	public function image() {
		return $this->hasOne(StaffImage::class, 'id', 'image_id');
	}
}
