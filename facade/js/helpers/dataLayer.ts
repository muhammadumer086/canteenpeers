// interface IEventData {
// 	event: string;
// }

interface IGTMDataLayer {
	pushEventToDataLayer(event: string): void;
}

export class GTMDataLayer implements IGTMDataLayer {
	// Push custom event to Datalayer for GTM
	// TODO: update interface;
	public pushEventToDataLayer(eventData): void {
		if (typeof window !== 'undefined') {
			if (typeof dataLayer === 'undefined') {
				(window as any).dataLayer = [];
			}
			dataLayer.push(eventData);
		}
	}
}
