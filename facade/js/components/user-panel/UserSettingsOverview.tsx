import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';

import { IApiUser, ISelectOption, IApiPersonalSetting } from '../../interfaces';
import { ETabs, ESettingsScreens } from './UserPanelAccount';
import { CheckboxGroupFormik } from '../inputs';
import { IStoreState } from 'js/redux/store';

interface IProps {
	userData: IApiUser;
	currentUser: IApiUser;
	submitting: boolean;
	personalSettings: IApiPersonalSetting[];
	allPermission:boolean;
	deletePermission:boolean;
	changeScreen(tab: ETabs, screen: ESettingsScreens);
	onSubmit(values: any, resetForm?: () => void);
}

const UserSettingsOverview: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		permissions: Yup.array().of(Yup.string()),
		notifications: Yup.array().of(Yup.string()),
		research: Yup.array().of(Yup.string()),
		newsletter: Yup.array().of(Yup.string()),
	});

	const permissionsOptions: ISelectOption[] = [];
	const atMentionOptions: ISelectOption[] = [];
	const discussionReplyOptions: ISelectOption[] = [];
	const discussionFollowedOptions: ISelectOption[] = [];
	const messagesOptions: ISelectOption[] = [];
	const canteenAnnouncementOptions: ISelectOption[] = [];
	const researchOptions: ISelectOption[] = [];
	const newsletterOptions: ISelectOption[] = [];

	props.personalSettings.forEach(setting => {
		const option: ISelectOption = {
			label: setting.name,
			value: setting.slug,
		};
		// Optionally add tooltip
		if (setting.slug === 'allow-display-gender') {
			option.tooltip =
				'When unchecked you will not show up in any search results with gender as a filter';
		}
		switch (setting.category_slug) {
			case 'permissions':
				permissionsOptions.push(option);
				break;
			case 'at-mentions':
				atMentionOptions.push(option);
				break;
			case 'discussion-replies':
				discussionReplyOptions.push(option);
				break;
			case 'followed-discussions':
				discussionFollowedOptions.push(option);
				break;
			case 'messages':
				messagesOptions.push(option);
				break;
			case 'canteen-alerts':
				canteenAnnouncementOptions.push(option);
				break;
			case 'research':
				researchOptions.push(option);
				break;
			case 'newsletter':
				newsletterOptions.push(option);
				break;
		}
	});

	const initialValues = {
		permissions: [],
		atMentions: [],
		discussionReply: [],
		followedDiscussion: [],
		messages: [],
		canteenAlerts: [],
		research: [],
		newsletter: [],
	};

	props.userData.personal_settings.forEach(setting => {
		const option = setting.slug;
		switch (setting.category_slug) {
			case 'permissions':
				initialValues.permissions.push(option);
				break;
			case 'at-mentions':
				initialValues.atMentions.push(option);
				break;
			case 'discussion-replies':
				initialValues.discussionReply.push(option);
				break;
			case 'followed-discussions':
				initialValues.followedDiscussion.push(option);
				break;
			case 'messages':
				initialValues.messages.push(option);
				break;
			case 'canteen-alerts':
				initialValues.canteenAlerts.push(option);
				break;
			case 'research':
				initialValues.research.push(option);
				break;
			case 'newsletter':
				initialValues.newsletter.push(option);
				break;
		}
	});

	return (
		<div className="user_account_panel user_account_panel--account_settings">
			<Formik
				initialValues={initialValues}
				validationSchema={validationFormat}
				onSubmit={(values, { resetForm }) => {
					// Map together the different values into one
					const updatedValues = {
						personal_settings: [].concat(
							values.permissions,
							values.atMentions,
							values.discussionReply,
							values.followedDiscussion,
							values.messages,
							values.canteenAlerts,
							values.research,
							values.newsletter,
						),
					};

					props.onSubmit(updatedValues, resetForm);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>
							<div className="form-row">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Permissions"
										name="permissions"
										options={permissionsOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							{(props.allPermission||props.userData.id===props.currentUser.id)&&<div className="form-row">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className=""
										type="button"
										onClick={() => {
											props.changeScreen(
												ETabs.SETTINGS,
												ESettingsScreens.BLOCKED_USERS,
											);
										}}
										disabled={props.submitting}
									>
										Blocked Users
									</Button>
								</div>
							</div>}
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="@ mention"
										name="atMentions"
										options={atMentionOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Discussion Replies"
										name="discussionReply"
										options={discussionReplyOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Followed Discussions"
										name="followedDiscussion"
										options={discussionFollowedOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Messages"
										name="messages"
										options={messagesOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Canteen Announcements"
										name="canteenAlerts"
										options={canteenAnnouncementOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Research"
										name="research"
										options={researchOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--space_above">
								<div className="form-column">
									<CheckboxGroupFormik
										label="Newsletter"
										name="newsletter"
										options={newsletterOptions}
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit_left">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={
											!isValid ||
											!touched ||
											props.submitting
										}
									>
										Update
									</Button>
								</div>
							</div>
						{(props.deletePermission||props.userData.id===props.currentUser.id)&&<div className="form-row form-row--submit_left form-row--border_top">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit form-submit--error"
										type="button"
										onClick={() => {
											props.changeScreen(
												ETabs.SETTINGS,
												ESettingsScreens.DELETE_ACCOUNT,
											);
										}}
										disabled={props.submitting}
									>
										{props.userData.id !==
										props.currentUser.id
											? 'Delete this account'
											: 'Delete my account'}
									</Button>
								</div>
							</div>}
						</form>
					);
				}}
			/>
		</div>
	);
};

const mapStateToProps = (state: IStoreState) => {
	const currentUser = state.user.userData;

	return {
		currentUser,
	};
};

export default connect(mapStateToProps)(UserSettingsOverview);
