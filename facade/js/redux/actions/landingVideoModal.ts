import { IStoreState } from '../store';
import { actionTypes } from '../actions';

export const landingVideoModalDisplay = () => (dispatch, getState: () => IStoreState) => {
	if (getState().landingVideoModal.landingVideoModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.LANDING_VIDEO_MODAL_DISPLAY,
	});
}

export const landingVideoModalHide = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().landingVideoModal.landingVideoModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.LANDING_VIDEO_MODAL_HIDE,
	});
}
