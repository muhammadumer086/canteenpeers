import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import buildUrl from 'build-url';

import { IApiDiscussionReport } from '../../../interfaces';

import { IStoreState, apiError } from '../../../redux/store';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import ListLoading from '../../../components/general/ListLoading';
import ReportTile from '../../../components/admin/ReportTile';
import HeroHeading from '../../../components/general/HeroHeading';

import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import {
	IAdminProps,
	IAdminState,
} from '../../../components/admin/withAdminPage';
import { checkPermission } from "../../../helpers/checkPermission";
export enum EReportedDiscussionPermissions{
	READ="reported_discussions_read",
	ADD="reported_discussions_add",
	EDIT="reported_discussions_edit",
	DELETE="reported_discussions_delete",
	ALL="reported_discussions_all",
}
interface IProps extends IAdminProps {
	apiError(message: string[]): void;
}

interface IState extends IAdminState {
	reports: IApiDiscussionReport[];
	currentPage: number;
	maxPages: number;
	isLoading: boolean;
	permissions: string[];
	isLoadingPermission: boolean;
}

class Reports extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			reports: [],
			currentPage: 0,
			maxPages: 1,
			isLoading: false,
			isLoadingPermission: false,
			permissions: [],
		};

		this.loadReports = this.loadReports.bind(this);
		this.initialise=this.initialise.bind(this);

	}

	public componentDidMount() {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EReportedDiscussionPermissions.ALL,EReportedDiscussionPermissions.READ);

		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EReportedDiscussionPermissions.ALL,EReportedDiscussionPermissions.READ);

		if (readPermission) {
			this.initialise(this.props, prevProps);
		}
	}

	public render() {
		const deletePermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EReportedDiscussionPermissions.ALL,EReportedDiscussionPermissions.DELETE);
		const allPermission: boolean =this.props.roleName==="admin"|| checkPermission(this.props.permissions,EReportedDiscussionPermissions.ALL);

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadReports}>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading title="Reported Discussions" />
					</header>
					<div className="admin_content">
						{!this.state.isLoading &&
							!this.state.reports.length && (
								<div className="admin_content-no_result hm-v16 hp-16">
									No discussions reported
								</div>
							)}
						{this.state.reports.map(report => {
							return (
								<ReportTile deletePermission={deletePermission} allPermission={allPermission} report={report} key={report.id} />
							);
						})}
						<ListLoading active={this.state.isLoading} />
					</div>
				</LayoutDashboard>
			</div>
		);
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =this.props.roleName==="admin"|| checkPermission(props.permissions,EReportedDiscussionPermissions.ALL,EReportedDiscussionPermissions.READ);
		const permissions=prevProps?prevProps.permissions:[];
		const prevReadPermission: boolean =this.props.roleName==="admin"|| checkPermission(permissions,EReportedDiscussionPermissions.ALL,EReportedDiscussionPermissions.READ);
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin||(!prevReadPermission&&readPermission))
		) {
			this.loadReports(false);
		}
	}

	protected loadReports(append: boolean = true) {
		if (
			!this.state.isLoading &&
			this.state.currentPage < this.state.maxPages
		) {
			let params: any = null;

			if (append) {
				params = {
					page: this.state.currentPage + 1,
				};
			}

			const url = buildUrl(null, {
				path: '/api/report-discussions',
				queryParams: params,
			});
			this.setState(
				{
					isLoading: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'report_discussions' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let reports: IApiDiscussionReport[] =
									response.data.report_discussions;

								if (append) {
									reports = JSON.parse(
										JSON.stringify(this.state.reports),
									);
									(response.data
										.users as IApiDiscussionReport[]).forEach(
										report => {
											reports.push(report);
										},
									);
								}

								this.setState({
									reports,
									isLoading: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
								});
							}
						})
						.catch(e => {
							this.props.apiError(e);
						});
				},
			);
		}
	}

}

function mapStateToProps(_state: IStoreState) {
	return {};
}

function mapDispatchToProps(dispatch) {
	return {
		apiError: (message: string[]) => dispatch(apiError(message)),
	};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Reports);
