<tr>
	<td style="padding: 10px 0; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #4c4d4f;">
		<div style="font-size: 14px; font-family: Arial,sans-serif; color: #4c4d4f; font-weight: 400; line-height: 20px; padding: 0; margin: 0;">
			{{ $slot }}
		</div>
	</td>
</tr>
