import React from 'react';

import { Avatar } from '@material-ui/core';

import Pill from '../../general/Pill';

import { IApiStaff } from '../../../../js/interfaces';
import { getExcerpt } from '../../../../js/helpers/getExcerpt';

interface IPropsStaffColumn {
	data: IApiStaff;
	action?(staff: IApiStaff): void;
}

export const StaffColumnName: React.StatelessComponent<
	IPropsStaffColumn
> = props => {
	const fullName = `${props.data.first_name} ${props.data.last_name} `;

	return (
		<div className="admin_table-column-user_profile">
			{!!props.data.feature_image && (
				<div className="admin_table-column-user_profile-avatar">
					<Avatar src={props.data.feature_image.url} />
				</div>
			)}
			<div className="admin_table-column-user_profile-content">
				<span> {fullName} </span>
			</div>
		</div>
	);
};

export const StaffColumnRole: React.StatelessComponent<
	IPropsStaffColumn
> = props => {
	return <div>{props.data.role}</div>;
};

export const StaffColumnBiography: React.StatelessComponent<
	IPropsStaffColumn
> = props => {
	const excerpt = getExcerpt(props.data.biography);
	const strippedExcerpt = excerpt.replace(
		/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g,
		'',
	);

	return <div>{strippedExcerpt}</div>;
};

export const StaffColumnTeam: React.StatelessComponent<
	IPropsStaffColumn
> = props => {
	if (props.data.team) {
		return (
			<Pill
				label={props.data.team.name}
				active={true}
				clickable={false}
				disabled={false}
			/>
		);
	}

	return <div />;
};
