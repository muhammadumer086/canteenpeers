<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

use UserVerification;
use App\Helpers\CampaignMonitorSubscription;

class UpdateUsersCampaignMonitor extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:updateUsersCampaignMonitor';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Publish all the users who opted-in to campaign monitor';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->line('');
		$this->info('Publishing users');
		$this->line('');

		$query = User::whereNull('deleted_at');

		$bar = $this->output->createProgressBar($query->count());
		$bar->start();
		$bar->setRedrawFrequency(10);
		$subscription = new CampaignMonitorSubscription();

		$query->chunk(100, function ($users) use ($bar, $subscription) {
			foreach ($users as $user) {
				if (!$user->hasRoleName('admin')) {
					// Test if the user has subscription enabled
					if ($user->allowNews()) {
						$subscription->subscribeUserToCampaignMonitor(
							$user->email,
							$user->first_name,
							$user->last_name,
							$user->location_data,
							$user->dob,
							true,
							$user->verified
						);
					} else {
						$subscription->unsubscribeUserFromCampaignMonitor(
							$user->email
						);
					}
				}

				$bar->advance();
			}
		});

		$bar->finish();
		$this->line('');
		$this->line('');
		$this->info('Users published');
	}
}
