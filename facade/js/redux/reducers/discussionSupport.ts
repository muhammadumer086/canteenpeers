import { actionTypes } from '../actions';
import { IApiDiscussion } from 'js/interfaces';

export interface IStoreState {
	discussionSupportDisplay: boolean;
	discussionData?: IApiDiscussion;
}

export const discussionSupport = (
	state: IStoreState = {
		discussionSupportDisplay: false,
		discussionData: null,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.DISCUSSION_SUPPORT_DISPLAY:
			return Object.assign({}, state, {
				discussionSupportDisplay: true,
				discussionData: action.discussionData,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.DISCUSSION_SUPPORT_HIDE:
			return Object.assign({}, state, {
				discussionSupportDisplay: false,
			});
		default:
			return state;
	}
};
