import React from 'react';
import Drawer from '@material-ui/core/Drawer';

import ClearIcon from '@material-ui/icons/Clear';

interface IProps {
	isOpen: boolean;
	handleCloseFilter(): void;
}

const AdminFilter: React.FC<IProps> = (props) => {
	return (
		<Drawer
			anchor="right"
			open={props.isOpen}
			onClose={props.handleCloseFilter}
		>
			<div className="admin_filter">
				{props.children}
			</div>
			<button
				onClick={props.handleCloseFilter}
				className="close_button close_button--primary"
				title="Dismiss"
			>
				<ClearIcon className="close_button-icon"/>
			</button>
		</Drawer>
	);
}

export default AdminFilter;
