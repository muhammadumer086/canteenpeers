<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'id',
		'created_at',
		'updated_at',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'slug',
		'name',
	];

	/**
	 * The users associated with the language
	 */
	public function users() {
		return $this->hasMany('App\Users');
	}
}
