import fetch from 'isomorphic-unfetch';
import Cookies from 'universal-cookie';

import { apiErrorDismiss } from '../redux/store';

import { AuthService } from './AuthService';
import { IApiToken } from '../interfaces';

export class Error400 extends Error {}
export class Error401 extends Error {}
export class Error403 extends Error {}
export class Error404 extends Error {}

export class ApiService {
	protected _reduxStore = null;
	protected _cookies;

	public constructor(reduxStore, request: any = null) {
		this._reduxStore = reduxStore;
		if (request) {
			this._cookies = new Cookies(request.headers.cookie);
		} else {
			this._cookies = new Cookies();
		}
	}

	/**
	 * GET query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryGET(endpoint: string, returnProperty?: string): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'GET',
				headers: this.getQueryHeaders(),
			}),
			returnProperty,
			endpoint
		);
	}

	/**
	 * Raw GET query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryRawGET(endpoint: string): Promise<any> {
		return fetch(this.getApiUrl(endpoint), {
			method: 'GET',
			headers: this.getQueryHeaders(),
		});
	}

	/**
	 * POST query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryPOST(
		endpoint: string,
		data: any,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'POST',
				headers: this.getQueryHeaders(),
				body: JSON.stringify(data),
			}),
			returnProperty,
		);
	}

	/**
	 * POST file query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryPOSTFile(
		endpoint: string,
		data: any,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'POST',
				headers: this.getQueryHeaders(true, null, {}),
				body: data,
			}),
			returnProperty,
		);
	}

	/**
	 * PUT query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryPUT(
		endpoint: string,
		data: any,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'PUT',
				headers: this.getQueryHeaders(),
				body: JSON.stringify(data),
			}),
			returnProperty,
		);
	}

	/**
	 * DELETE query to the admin
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryDELETE(
		endpoint: string,
		data: any = null,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'DELETE',
				headers: this.getQueryHeaders(),
				body: JSON.stringify(data),
			}),
			returnProperty,
		);
	}

	/**
	 * GET query to the admin that require authentication
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} token: The authentication token
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryAuthGET(
		endpoint: string,
		token: string,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'GET',
				headers: this.getQueryHeaders(false, token),
			}),
			returnProperty,
		);
	}

	/**
	 * POST query to the admin that require authentication
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} token: The authentication token
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryAuthPOST(
		endpoint: string,
		token: string,
		data: any,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'POST',
				headers: this.getQueryHeaders(false, token),
				body: JSON.stringify(data),
			}),
			returnProperty,
		);
	}

	/**
	 * PUT query to the admin that require authentication
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} token: The authentication token
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryAuthPUT(
		endpoint: string,
		token: string,
		data: any,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'PUT',
				headers: this.getQueryHeaders(false, token),
				body: JSON.stringify(data),
			}),
			returnProperty,
		);
	}

	/**
	 * DELETE query to the admin that require authentication
	 *
	 * @param {string} endpoint: API endpoint
	 * @param {string} token: The authentication token
	 * @param {any} data: The data to post
	 * @param {string} returnProperty: Optional property from the API to return
	 */
	public queryAuthDELETE(
		endpoint: string,
		token: string,
		data: any,
		returnProperty?: string,
	): Promise<any> {
		return this.handleApiResponses(
			fetch(this.getApiUrl(endpoint), {
				method: 'DELETE',
				headers: this.getQueryHeaders(false, token),
				body: JSON.stringify(data),
			}),
			returnProperty,
		);
	}

	protected dispatch(action: any): void {
		if (this._reduxStore) {
			this._reduxStore.dispatch(action);
		}
	}

	public getApiUrl(endpoint: string): string {
		let url = endpoint;
		// Add the first slash in the endpoint just in case
		if (endpoint.length) {
			if (
				!endpoint.match(/^https?\:\/\//) &&
				endpoint.charAt(0) !== '/'
			) {
				url = `/${endpoint}`;
			}
		}
		// return url;
		return `${process.env.ADMIN_URL}${url}`;
	}

	protected handleApiResponses(fetchQuery: any, returnProperty?: string,endpoint?:string) {
		// Dismiss the API error
		this.dispatch(apiErrorDismiss());

		return fetchQuery
			.then(response => {
				if (!response.ok && response.status !== 400) {
					if (typeof window !== 'undefined') {
						console.warn('Error response:', response);
					}

					if (response.status === 401) {
						return	response.json().then(res=>{
							if(res.statuscode&&res.statuscode===501)
							{
								//for graduated user
								throw new Error401(res.error)	
							}
							else
							{
								throw new Error401('Authentication required.');
							}
						})
					} else if (response.status === 403) {
						if(endpoint==="/api/conversations")
						{
							const responseObj={
								data : {
									conversations : [],
									error : true
								}
							}
							
							//throw new Error403(message403);
							return responseObj;
						}
						else
						{
							throw new Error403(`You need to be above 16.`);
						}
					} else if (response.status === 404) {
						throw new Error404(`Page not found.`);
					} else {
						throw new Error400(
							`An unknown error happened (${
								response.status
							}). Please try again later.`,
						);
					}
				} else {
					return response.json();
				}
			})
			.then(data => {
				if (data.hasOwnProperty('error')) {
					if (
						data.hasOwnProperty('message') &&
						typeof data.message === 'string'
					) {
						throw new Error(data.message);
					}

					if (typeof data.error === 'string') {
						// throw new Error(data.error); //old one
						let error= new Error();
						const message=data.error;
						delete data.error;
						error={...data,message};//to handle multiple user account
						throw error; 
					}
					console.warn(`Error data:`, data);
					throw new Error('Invalid format when receiving API error.');
				}

				if (returnProperty) {
					if (data.hasOwnProperty(returnProperty)) {
						return data[returnProperty];
					} else {
						throw new Error(
							`Couldn't find property ${returnProperty} in API response`,
						);
					}
				}

				return data;
			});
	}

	/**
	 * Add the API headers
	 *
	 * @param {boolean} autoBearer Automatically set the bearer token if available
	 * @param {string|null} token Manually set the token
	 */
	protected getQueryHeaders(
		autoBearer: boolean = true,
		token: string | null = null,
		defaultHeaders: any = null,
	) {
		let headersData = {
			'Content-Type': 'application/json',
		};

		if (defaultHeaders) {
			headersData = defaultHeaders;
		}

		if (autoBearer && this._cookies.get(AuthService.UserToken)) {
			const userToken: IApiToken = this._cookies.get(
				AuthService.UserToken,
			);
			token =
				userToken && userToken.access_token
					? userToken.access_token
					: null;
		}
		if (token!==null) {
			headersData['Authorization'] = `Bearer ${token}`;
		}
		return headersData;
	}
}
