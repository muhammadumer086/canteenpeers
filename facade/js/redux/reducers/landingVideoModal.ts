import { actionTypes } from '../actions';

export interface  IStoreState {
	landingVideoModalDisplay: boolean;
}

export const landingVideoModal = (state: IStoreState = {
	landingVideoModalDisplay: false,
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.LANDING_VIDEO_MODAL_DISPLAY:
			return Object.assign({}, state, {
				landingVideoModalDisplay: true,
			});
		case actionTypes.LANDING_VIDEO_MODAL_HIDE:
			return Object.assign({}, state, {
				landingVideoModalDisplay: false,
			});
		default:
			return state;
	}
}
