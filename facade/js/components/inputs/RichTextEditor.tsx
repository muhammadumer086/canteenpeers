import React from 'react';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';

import { ApiService, withServices } from '../../services';
import { IStoreState, apiError } from '../../redux/store';
import {
	IApiBlogImage,
	IApiGIFObject,
	IApiSettings,
	IApiUser,
} from '../../interfaces';
import {
	RichTextEditorCustomToolbar,
	IToolbarConfig,
} from './RichTextEditorCustomToolbar';

interface IEditorProps {
	module?: any;
	formats?: any;
	toolbarConfig: IToolbarConfig;
}

interface IProps {
	toolbarId: string;
	settings: IApiSettings | null;
	userData: IApiUser | null;
	name: string;
	value: string;
	profile?: string[];
	bounds?: string | null;
	scrollingContainer?: string | null;
	apiService: ApiService;
	dispatch(action: any): void;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
}

interface IState {
	isClient: boolean;
	isGifActive: boolean;
	quillIndex: number | null;
	quillLength: number | null;
}

let quillLoaded = false;

// Dynamically import the ReactQuill module
const ReactQuillDynamic: any = dynamic(
	{
		modules: () => {
			const components = {
				ReactQuill: import('react-quill').catch(e => {
					if (typeof window !== 'undefined') {
						console.warn('react-quill', e);
					}
					return null;
				}),
				// imageDrop: import('quill-image-drop-module')
				// .then(m => m.ImageDrop)
				imageDrop: import('../../helpers/QuillImageDrop').catch(e => {
					if (typeof window !== 'undefined') {
						console.warn('quill-image-drop-module', e);
					}
					return null;
				}),
				mention: import('../../helpers/QuillMention').catch(e => {
					if (typeof window !== 'undefined') {
						console.warn('quill-mention-module', e);
					}
					return null;
				}),
			};

			return components;
		},
		render: (props: any, { ReactQuill, imageDrop, mention }) => {
			if (ReactQuill && imageDrop && mention) {
				// Initialise the Quill modules only once

				if (!quillLoaded) {
					ReactQuill.Quill.register('modules/imageDrop', imageDrop);
					ReactQuill.Quill.register('modules/mention', mention);
					quillLoaded = true;
				}

				return (
					<React.Fragment>
						<RichTextEditorCustomToolbar
							toolbarId={props.toolbarId}
							config={props.toolbarConfig}
							isGifActive={props.isGifActive}
							onGifSelection={props.onGifSelection}
						/>
						<ReactQuill
							theme="snow"
							onChange={props.onChange}
							bounds={props.bounds}
							modules={props.module}
							formats={props.formats}
							ref={props.setRef}
						/>
					</React.Fragment>
				);
			}

			return <div>Error Loading Editor</div>;
		},
	} as any,
	{
		ssr: false,
		loading: () => {
			return <div>Loading Editor</div>;
		},
	},
);

class RichTextEditor extends React.Component<IProps, IState> {
	protected quillEditorRef: React.RefObject<any>;

	constructor(props: IProps) {
		super(props);

		this.getEditorPropsForProfile = this.getEditorPropsForProfile.bind(
			this,
		);
		this.toggleGifSearch = this.toggleGifSearch.bind(this);
		this.insertGifToHTML = this.insertGifToHTML.bind(this);
		this.clearEditor = this.clearEditor.bind(this);
		this.setContent = this.setContent.bind(this);
		this.handleQullIndex = this.handleQullIndex.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.uploadImage = this.uploadImage.bind(this);
		this.onInsertGif = this.onInsertGif.bind(this);

		this.state = {
			isClient: false,
			isGifActive: false,
			quillIndex: null,
			quillLength: null,
		};

		this.setRef = this.setRef.bind(this);

		this.quillEditorRef = React.createRef();
	}

	public componentDidMount() {
		this.setState({
			isClient: true,
		});

		window.addEventListener('click', this.handleQullIndex);
	}

	public componentDidUpdate() {
		if (this.props.value === '') {
			this.clearEditor();
		}
	}

	public componentWillUnmount() {
		window.removeEventListener('click', this.handleQullIndex);
	}

	public render() {
		if (!this.state.isClient) {
			return null;
		}

		if (!this.props.settings) {
			return null;
		}

		return (
			<div className="rich_text_editor">
				<ReactQuillDynamic
					toolbarId={this.props.toolbarId}
					setRef={this.setRef}
					value={this.props.value}
					onChange={this.handleChange}
					bounds={this.props.bounds}
					scrollingContainer={this.props.scrollingContainer}
					isGifActive={this.state.isGifActive}
					onGifSelection={this.onInsertGif}
					{...this.getEditorPropsForProfile(this.props.profile)}
				/>
			</div>
		);
	}

	protected setRef(ref: React.RefObject<any>) {
		this.quillEditorRef = ref;

		const quill = this.quillEditorRef;

		if (!quill || quill.current === null) {
			return null;
		}

		const editor = (quill as any).getEditor();

		editor.root.innerHTML = this.props.value;
	}

	protected clearEditor() {
		const quill = this.quillEditorRef;

		if (!quill || quill.current === null) {
			return null;
		}

		const editor = (quill as any).getEditor();

		if (editor && editor.getSelection()) {
			editor.setContents([{ insert: '\n' }]);
		}
	}

	protected setContent(content: string) {
		const quill = this.quillEditorRef;

		if (!quill || quill.current === null) {
			return null;
		}

		const editor = (quill as any).getEditor();

		if (editor) {
			editor.pasteHTML(0, content);
		}
	}

	protected handleQullIndex() {
		const quill = this.quillEditorRef;

		if (!quill || quill.current === null) {
			return null;
		}

		const editor = (quill as any).getEditor();

		if (editor && editor.getSelection()) {
			const { index, length } = editor.getSelection();

			this.setState({
				quillIndex: index,
				quillLength: length,
			});
		}
	}

	protected onInsertGif(gifData: IApiGIFObject) {
		const quill = this.quillEditorRef;

		// If there is no gif data or editor ref then cancel
		if (!quill || !gifData) {
			return null;
		}

		// Abstract the url and editor ref
		const gifUrl = gifData.images.fixed_width.url;
		const editor = (quill as any).getEditor();

		// If the editor does not have a index to refer to then use state.quillIndex
		const getSelection = editor.getSelection()
			? editor.getSelection().index
			: this.state.quillIndex;

		// Allow for index === 0
		if (editor && getSelection !== null) {
			const cursorPosition = getSelection;

			this.insertGifToHTML(
				editor,
				gifUrl,
				cursorPosition,
				this.toggleGifSearch,
			);
		}
	}

	protected getEditorPropsForProfile(profile?: string[]): IEditorProps {
		// Return blank editor
		let editorConfig: IEditorProps;
		let toolbarId = this.props.toolbarId;

		if (!toolbarId) {
			toolbarId = 'toolbar';
		}

		editorConfig = {
			module: {
				toolbar: {
					container: `#${toolbarId}`,
					handlers: {
						gifSearch: null,
					},
				},
				imageDrop: {
					upload: null,
				},
				clipboard: {
					matchVisual: false,
				},
				mention: {
					isActive: false,
					prefixCharacter: [],
					maxCharacter: 15,
					bounds: this.props.bounds ? this.props.bounds : null,
					searchConfig: {
						algolia_app_id: this.props.settings.algolia_app_id,
						algolia_search_key: this.props.settings
							.algolia_search_key,
						config: this.props.settings.config,
						search_index: this.props.settings.search_index,
					},
					dataAttributes: ['username', 'label', 'type'],
				},
			},
			formats: [
				'header',
				'bold',
				'italic',
				'strike',
				'blockquote',
				'list',
				'bullet',
				'link',
			],
			toolbarConfig: {
				formatHeading: true,
				formatText: true,
				formatList: true,
				formatVideo: false,
				formatImage: false,
				formatGif: false,
				formatClean: true,
			},
		};

		if (profile.indexOf('imageUpload') > -1) {
			editorConfig.module.toolbar['handlers'][
				'gifSearch'
			] = this.toggleGifSearch;
			editorConfig.module.imageDrop.upload = this.uploadImage;
			editorConfig.toolbarConfig.formatImage = true;

			editorConfig.formats.push('image');
		}

		if (profile.indexOf('embedVideo') > -1) {
			editorConfig.formats.push('video');

			editorConfig.toolbarConfig.formatVideo = true;
		}

		if (profile.indexOf('insertGif') > -1) {
			editorConfig.module.toolbar.handlers.gifSearch = this.toggleGifSearch;

			editorConfig.toolbarConfig.formatGif = true;
		}

		if (profile.indexOf('mention') > -1) {
			editorConfig.module.mention.isActive = true;
			editorConfig.module.mention.prefixCharacter.push('@');
			editorConfig.formats.push('mention');
		}

		if (profile.indexOf('hashtag') > -1) {
			editorConfig.module.mention.isActive = true;
			editorConfig.module.mention.prefixCharacter.push('#');
			editorConfig.formats.push('hashtag');
		}

		return editorConfig;
	}

	protected handleChange(value: string) {
		this.props.setFieldValue(this.props.name, value);
		this.props.setFieldTouched(this.props.name, true, true);
	}

	protected uploadImage(file: File): Promise<string> {
		const promise = new Promise<string>((resolve, reject) => {
			const params = new FormData();
			params.append('image', file, file.name);

			this.props.apiService
				.queryPOSTFile(`/api/blog-images`, params, 'image')
				// TODO: Set correct interface
				.then((image: IApiBlogImage) => {
					resolve(image.image_url);
				})
				.catch(error => {
					this.props.dispatch(apiError([error.message]));
					reject(error.message);
				});
		});
		return promise;
	}

	protected toggleGifSearch() {
		this.setState({
			isGifActive: !this.state.isGifActive,
		});
	}

	protected insertGifToHTML(
		editorRef: any,
		gifUrl: string,
		cursorIndex: number,
		cb: () => void,
	) {
		const cursorPosition = cursorIndex;
		const giphyHTML = `<img className="gif" src="${gifUrl}" alt="Giphy - ${
			process.env.APP_URL
		}">`;

		editorRef.pasteHTML(cursorPosition, giphyHTML);
		editorRef.setSelection(cursorPosition + 1);

		// Set isActive Giphy State
		cb();
	}
}

function mapStateToProps(state: IStoreState) {
	const { settings } = state.settings;
	const { userData } = state.user;

	return {
		settings,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(RichTextEditor));
