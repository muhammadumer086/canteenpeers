<?php

namespace App\Filters\User;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class EmailFilter extends FilterAbstract {
	/**
	 * Mappings for database values.
	 *
	 * @return array
	 */
	public function mappings() {
		return [];
	}

	/**
	 * Filter with builder queries.
	 *
	 * @param  string $access
	 * @return Illuminate\Database\Eloquent\Builder
	 */
	public function filter(Builder $builder, $value) {
		return $builder->where('email', 'LIKE', '%' . $value . '%');
	}
}
