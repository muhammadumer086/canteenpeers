import { IStaticStates } from '../interfaces';

export const generateStates = (): IStaticStates[] => {
	return [
		{
			label: 'New South Wales',
			value: 'nsw',
		},
		{
			label: 'Queensland',
			value: 'qld',
		},
		{
			label: 'South Australia',
			value: 'sa',
		},
		{
			label: 'Tasmania',
			value: 'tas',
		},
		{
			label: 'Victoria',
			value: 'vic',
		},
		{
			label: 'Western Australia',
			value: 'wa',
		},
		{
			label: 'Australian Capital Territory',
			value: 'act',
		},
		{
			label: 'Northen Territory',
			value: 'nt',
		},
		{
			label: 'New Zealand',
			value: 'nz',
		},
	];
};

export const generateStatesEvents = (): IStaticStates[] => {
	const states = generateStates();

	states.splice(2, 0, {
		label: 'North Queensland',
		value: 'qld-north',
	});

	states.push({
		label: 'Online',
		value: 'online',
	});

	return states;
};

export const generateAgeRanges = (): IStaticStates[] => {
	return [
		{
			label: '12 - 14',
			value: '12-14',
		},
		{
			label: '15 - 17',
			value: '15-17',
		},
		{
			label: '18 - 20',
			value: '18-20',
		},
		{
			label: '21 - 25',
			value: '21-25',
		},
	];
};
