import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import {
	TextFieldFormik,
	SelectFieldFormik,
	CheckboxGroupFormik,
	RichTextEditor,
	RadioGroupFormik
} from '../inputs';

import { IStoreState } from '../../redux/store';
import isAdmin from '../../../js/helpers/isAdmin';

import Pill from '../general/Pill';

import {
	IApiSituation,
	ISelectOption,
	IApiUser,
	IApiDiscussion,
} from '../../interfaces';
import { filterRootSituations } from '../../helpers/filterRootSituations';

interface IProps {
	topics: ISelectOption[];
	discussionData: IApiDiscussion | null;
	situations: IApiSituation[];
	userSituations: IApiSituation[];
	submitting: boolean;
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	updatedSituations(situations: string[]): void;
	onSubmit(values: IFormValues);
}

interface IState {
	situations: IApiSituation[];
	userSituations: IApiSituation[];
	activeUserSituation: string[];
}

interface IFormValues {
	title: string;
	content: string;
	topic: string;
}

class EditDiscussionForm extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			situations: filterRootSituations(props.situations),
			userSituations: props.userSituations,
			activeUserSituation: this.getActiveSituations(),
		};

		this.handleSituationClick = this.handleSituationClick.bind(this);
		this.renderActiveSituations = this.renderActiveSituations.bind(this);
		this.getActiveSituations = this.getActiveSituations.bind(this);
	}

	get isReply(): boolean {
		return !!this.props.discussionData.main_discussion_id;
	}

	public componentDidUpdate(oldProps: IProps) {
		if (
			JSON.stringify(this.props.userSituations) !==
			JSON.stringify(oldProps.userSituations)
		) {
			this.setState({
				userSituations: this.props.userSituations,
				activeUserSituation: this.initialiseSituations(this.props),
			});
		}

		if (
			JSON.stringify(this.props.situations) !==
			JSON.stringify(oldProps.situations)
		) {
			this.setState({
				situations: filterRootSituations(this.props.situations),
				activeUserSituation: this.initialiseSituations(this.props),
			});
		}
	}

	public render() {
		const isReply = this.isReply;

		const validationFormat = isReply
			? Yup.object().shape({
					content: Yup.string()
						.required('A discussion is required')
						.max(16000000),
			  })
			: Yup.object().shape({
					title: Yup.string()
						.required('A title is required')
						.max(191),
					content: Yup.string()
						.required('A discussion is required')
						.max(16000000),
					topic: Yup.string()
						.required('A topic is required')
						.max(191),
					situations: Yup.array()
						.of(
							Yup.mixed().oneOf(
								this.props.situations.map(singleOption => {
									return singleOption.slug;
								}),
							),
						)
						.required('Please select at least one situation'),
			  });

		const discussionDisplayOptions: ISelectOption[] = [
			{
				label:
				'I’d like to keep my discussion private so that only members of the Canteen Connect community can see it',
				value: 'private',
				tooltip:'Please note: This option cannot be changed after the discussion is created.'
			},
			{
				label : 'I’m OK with people outside  the  community  seeing  my  discussion  (including  people searching on Google)',
				value : 'public'
			}
		];
		const discussionAgeSensitiveOption : ISelectOption[]=[];
		if (this.props.userData && !this.props.userData.underage) {
			discussionAgeSensitiveOption.push({
				label:
					'This discussion contains age-sensitive information (thread won’t be seen by users under the age of 16)',
				value: 'ageSensitive',
			});
		}

		let displayOptions: string;
		const ageSensitiveOption:string[]=[];
		if (!!this.props.discussionData) {
			if (this.props.discussionData.age_sensitive) {
				ageSensitiveOption.push('ageSensitive');
			}

			if (this.props.discussionData.private) {
				displayOptions= 'private';
			}
			else
			{
				displayOptions= 'public';
			}
		}
		

		return (
			<div>
				<Formik
					initialValues={{
						title: this.props.discussionData.title,
						content: this.props.discussionData.content,
						topic: !isReply
							? this.props.discussionData.topic.slug
							: '',
						situations: this.state.activeUserSituation,
						displayOptions: displayOptions,
						ageSensitiveOption : ageSensitiveOption
					}}
					validationSchema={validationFormat}
					onSubmit={values => {
						const formValues = JSON.parse(JSON.stringify(values));
						if(formValues.displayOptions==="private")
						{
							formValues['private'] = true;
						}
						else{
							formValues['private'] = false;
						}
						// If check boxes are checked
						if (formValues.ageSensitiveOption.length > 0) {
							const foundIndex=formValues.ageSensitiveOption.findIndex(val=>val==="ageSensitive");
							if(foundIndex>-1)
							{
								formValues['ageSensitive'] = true;
							}
							else
							{
								formValues['ageSensitive'] = false;
							}
							
						} else {
							formValues['ageSensitive'] = false;
						}

						delete formValues.displayOptions;
						delete formValues.ageSensitiveOption;
						this.props.onSubmit(formValues);
					}}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};
						return (
							<form
								className="create_discussion_form form"
								onSubmit={handleSubmit}
							>
								{!isReply && (
									<div className="form-row">
										<div className="form-column">
											<div className="form-helper_text theme-title hm-b8">
												<span className="font--body">
													This discussion relates to
													the following cancer
													experience(s)
												</span>
											</div>
											{this.state.situations.map(
												situation => {
													return this.renderSituation(
														situation,
														setFieldValue,
													);
												},
											)}
										</div>
									</div>
								)}
								{!isReply && (
									<div className="form-row">
										<div className="form-column">
											<SelectFieldFormik
												label="Topic"
												name="topic"
												options={this.props.topics}
												values={inputProps.values.topic}
												disabled={this.props.submitting}
												noEmpty={false}
												{...inputProps}
											/>
										</div>
									</div>
								)}
								{!isReply && (
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="text"
												label="Discussion title"
												name="title"
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
								)}

								<div className="form-row">
									<div className="form-column">
										<RichTextEditor
											toolbarId="edit-discussion"
											name="content"
											value={inputProps.values.content}
											profile={[
												'imageUpload',
												'embedVideo',
												'insertGif',
												'mention',
												'hashtag',
											]}
											bounds=".modal-content_container"
											scrollingContainer=".modal-content_container"
											{...inputProps}
										/>
									</div>
								</div>

								{!isReply && !!discussionDisplayOptions.length && (
									<div>
									<div className="form-row">
										<div className="form-column">
											<RadioGroupFormik
												label="Display options"
												name="displayOptions"
												options={
													discussionDisplayOptions
												}
												disabled={this.props.submitting||this.props.discussionData.private}
												{...inputProps}
											/>
										</div>
									</div>
										<div className="form-row">
										<div className="form-column">
											<CheckboxGroupFormik
												label=""
												name={'ageSensitiveOption'}
												options={
													discussionAgeSensitiveOption
												}
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									</div>
								)}
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid ||
												this.props.submitting
											}
										>
											Done
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			</div>
		);
	}

	protected renderSituation(
		userSituation: IApiSituation,
		setFieldValue: (fieldName: string, value: any) => void,
	) {
		return (
			<Pill
				label={userSituation.name}
				active={
					this.state.activeUserSituation.indexOf(
						userSituation.slug,
					) >= 0
				}
				disabled={
					this.state.activeUserSituation.length === 1 &&
					this.state.activeUserSituation.indexOf(
						userSituation.slug,
					) >= 0
				}
				clickable={true}
				onClick={() => {
					this.handleSituationClick(userSituation, setFieldValue);
				}}
				key={userSituation.id}
			/>
		);
	}

	protected initialiseSituations(props: IProps) {
		const activeUserSituation = filterRootSituations(
			props.userSituations,
		).map(singleSituation => {
			return singleSituation.slug;
		});

		this.props.updatedSituations(activeUserSituation);

		return activeUserSituation;
	}

	protected handleSituationClick(
		situation: IApiSituation,
		setFieldValue: (fieldName: string, value: any) => void,
	) {
		const situationIndex = this.state.activeUserSituation.indexOf(
			situation.slug,
		);
		const activeUserSituation: string[] = JSON.parse(
			JSON.stringify(this.state.activeUserSituation),
		);

		if (situationIndex >= 0) {
			// Remove the topic
			activeUserSituation.splice(situationIndex, 1);
		} else {
			// Add the topic
			activeUserSituation.push(situation.slug);
		}

		setFieldValue('situations', activeUserSituation);

		this.setState(
			{
				activeUserSituation,
			},
			() => {
				this.props.updatedSituations(this.state.activeUserSituation);
			},
		);
	}

	protected renderActiveSituations(setFieldValue) {
		const adminSituationsStripped: IApiSituation[] = [];

		this.props.discussionData.situations.forEach(situations => {
			adminSituationsStripped.push(situations);
		});

		if (isAdmin(this.props.userData)) {
			return this.state.userSituations.map(userSituation => {
				return this.renderSituation(userSituation, setFieldValue);
			});
		} else {
			return this.props.discussionData.situations.map(userSituation => {
				return this.renderSituation(userSituation, setFieldValue);
			});
		}
	}

	protected getActiveSituations() {
		return this.props.discussionData.situations.map(singleSituation => {
			return singleSituation.slug;
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(EditDiscussionForm);
