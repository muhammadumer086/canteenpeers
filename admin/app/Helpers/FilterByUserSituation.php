<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Situation;
use App\User;

class FilterByUserSituation {
	public function addSituationsMap($situationNames) {
		// Manage the case where some situations need to be paired with each-other
		$situationsMap = [
			'My parent has Cancer' => ['My parent survived Cancer'],
			'My parent survived Cancer' => ['My parent has Cancer'],
			'My sibling has Cancer' => ['My sibling survived Cancer'],
			'My sibling survived Cancer' => ['My sibling has Cancer'],
			'My parent died from Cancer' => ['My sibling died from Cancer'],
			'My sibling died from Cancer' => ['My parent died from Cancer'],
		];

		// Add the paired situations
		$pairedSituations = [];
		foreach ($situationsMap as $key => $value) {
			if (in_array($key, $situationNames)) {
				$pairedSituations = array_merge($pairedSituations, $value);
			}
		}
		// Add the paired situations and de-duplicate array
		$situationNames = array_merge($situationNames, $pairedSituations);
		$situationNames = array_unique($situationNames);

		return Situation::whereIn('name', $situationNames)
			->whereNull('parent_id')
			->pluck('situations.id');
	}

	public function filterByUserSituation(
		User $user,
		$dbQuery,
		$table,
		$linkTable,
		$resourceIdColumn
	) {
		// Only use the user's situation if not an admin
		if (!$user->hasRoleName('admin')) {
			$situationNames = $user
				->situations()
				->pluck('name')
				->toArray();

			$situationIds = $this->addSituationsMap($situationNames);

			$dbQuery->whereIn(
				"{$table}.id",
				DB::table($linkTable)
					->whereIn('situation_id', $situationIds)
					->pluck($resourceIdColumn)
			);
		}

		return $dbQuery;
	}
}

?>
