<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDurationAndTimeFieldsToEvents extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('events', function (Blueprint $table) {
			$table->boolean('overnight')->default(false);
			$table->boolean('weekend')->default(false);
			$table->boolean('daytime')->default(false);
			$table->boolean('evening')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('events', function (Blueprint $table) {
			$table->dropColumn('overnight');
			$table->dropColumn('weekend');
			$table->dropColumn('daytime');
			$table->dropColumn('evening');
		});
	}
}
