import React          from 'react';
import ReactSVG       from 'react-svg';

import { ButtonBaseLink } from './ButtonBaseLink';

interface IProps {
	icon: string;
	title: string;
	linkData: {
		href: string;
		as: string;
	};
	linkLabel: string;
}

const SubTitleDetail = (props: IProps) => {
	const iconUrl = `${process.env.STATIC_PATH}/icons/${props.icon}.svg`;
	const arrowIconUrl = `${process.env.STATIC_PATH}/icons/arrow.svg`;

	return (
		<div className="sub_title_detail">
			<div className="sub_title_detail-title">
				<span className="sub_title_detail-title_icon">
					<ReactSVG path={iconUrl} />
				</span>

				<span className="sub_title_detail-title_label">
					{props.title}
				</span>
			</div>

			<ButtonBaseLink
				className="sub_title_detail-button"
				href={props.linkData.href}
				hrefAs={props.linkData.as}
			>
				<div className="sub_title_detail-action default_link">
					<span className="sub_title_detail-label">
						{props.linkLabel}
					</span>

					<span className="sub_title_detail-label_icon">
						<ReactSVG path={arrowIconUrl} />
					</span>
				</div>
			</ButtonBaseLink>
		</div>
	)
}

export default SubTitleDetail;
