<?php

namespace App\Traits;

use App\Hashtag;

trait HasHashtags {
	public function getHashtags() {
		preg_match_all('/#([\w\-]+)/', $this->content, $matches);
		return $matches[1];
	}

	public function hashtags() {
		return $this->morphToMany(Hashtag::class, 'hashtaggable');
	}
}
