<?php

namespace App\Http\Resources;

use App\StaffImage;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Team as TeamResource;

class Staff extends JsonResource
{
    use \App\Traits\AuthUser;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('staff', $this->id, $this->updated_at, false)) {
			return $apiStore->getApiResource('staff', $this->id, $this->updated_at, false);
		}

		$params = [
			'id',
			'first_name',
			'last_name',
			'role',
			'biography',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
        }
        
        if ($this->image) {
            $result['feature_image'] = [
                'dimensions' => [
                    'width' => $this->image->width,
                    'height' => $this->image->height,
                ],
                'alt' => null,
                'copyright' => null,
                'url' => $this->image->image_url,
            ];
            $result['image_id'] = $this->image->id;
        } else {
            $result['feature_image'] = null;
            $result['image_id'] = null;
        }

        if ($this->team) {
            $result['team'] = new TeamResource($this->team);
        } else {
            $result['team'] = null;
        }

        $authUser = $this->getAuthUser($request);

        if (
            $authUser &&
            $authUser->hasRoleName('admin')
        ) {
            $adminParams = [
                'order',
            ];

            foreach ($adminParams as $key) {
                $result[$key] = $this->$key;
            }
        }

		$apiStore->setApiResource($result, 'staff', $this->id, $this->updated_at, false);

		return $result;
    }
}
