<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserBlockedNotification extends Notification {
	use Queueable;

	protected $blocker;
	protected $blocked;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($blocker, $blocked) {
		$this->blocker = User::where(
			'username',
			$blocker->username
		)->firstOrFail();
		$this->blocked = User::where(
			'username',
			$blocked->username
		)->firstOrFail();
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [
				// 'mail'
			];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "{$this->blocker->username} has blocked {$this->blocked->username}";

		return (new MailMessage())
			->subject($title)
			->view('emails.user-blocked', [
				'blocker' => $this->blocker,
				'blocked' => $this->blocked,
			]);
	}
}
