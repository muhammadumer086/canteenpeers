import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import Button from '@material-ui/core/Button';

import { IApiUser, IApiSituation } from '../../../interfaces';

import { userPanelDisplay, apiSuccess, apiError } from '../../../redux/store';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
import HeroHeading from '../../../components/general/HeroHeading';
import AdminTable, {
	IAdminTableColumn,
} from '../../../components/admin/AdminTable';
import {
	IAdminProps,
	IAdminState,
} from '../../../components/admin/withAdminPage';
import {
	ColumnNameSituation,
	ColumnAge,
	ColumnState,
	ColumnGender,
	ColumnSituation,
	ColumnLastLogin,
	ColumnTotalLogins,
	//ColumnGraduatedEnable,
	ColumnTotalBlogs,
	ColumnTotalDiscussions,
	ColumnTotalLikes,
	ColumnTotalHugs,
	ColumnTotalFollowedDiscussions,
	ColumnTotalSavedResources,
} from '../../../components/admin/AdminTableColumns';
import AdminFilter from '../../../components/admin/AdminFilter';
import AdminFilterUsers from '../../../components/admin/AdminFilterUsers';
import { processQueryParams } from '../../../helpers/processQueryParams';

import { ICreateUserFormik } from '../users/forms/CreateNewUserFormik';
import { checkPermission } from '../../../helpers/checkPermission';
import { EUsersManagementPermissions } from './users';

export enum EGraduateUserManagementPermissions {
	READ = 'graduate_users_management_read',
	ADD = 'graduate_users_management_add',
	EDIT = 'graduate_users_management_edit',
	DELETE = 'graduate_users_management_delete',
	ALL = 'graduate_users_management_all',
}
interface IProps extends IAdminProps {}

interface IState extends IAdminState {
	data: IApiUser[];
	currentPage: number;
	maxPages: number;
	perPage: number;
	total: number;
	isLoadingUsers: boolean;
	filterOpen: boolean;
	filterValues: any;
	isCreatUserModalActive: boolean;
	situations: IApiSituation[] | null;
	isUpdating: boolean;
	updatingUserId: number;
	isLoadingPermission: boolean;
	permissions: string[];
}

class Users extends React.Component<IProps, IState> {
	protected tableColumns: IAdminTableColumn[] = [
		{
			header: 'Name',
			size: 20,
			renderer: ColumnNameSituation,
			sortable: 'username',
		},
		{
			header: 'Age',
			size: 7,
			renderer: ColumnAge,
			sortable: 'dob',
			sortableReverse: true,
		},
		{
			header: 'State',
			size: 7,
			renderer: ColumnState,
			sortable: 'state',
		},
		{
			header: 'Gender',
			size: 10,
			renderer: ColumnGender,
			sortable: 'gender',
		},
		{
			header: 'Situation',
			size: 20,
			renderer: ColumnSituation,
		},
		{
			header: 'Last Login',
			size: 10,
			renderer: ColumnLastLogin,
			sortable: 'last_login',
		},
		{
			header: 'Total Logins',
			size: 10,
			renderer: ColumnTotalLogins,
			sortable: 'count_logins',
		},
		{
			header: 'Active Discussions',
			size: 12,
			renderer: ColumnTotalDiscussions,
			sortable: 'count_active_discussions',
		},
		{
			header: 'I Get It',
			size: 7,
			renderer: ColumnTotalLikes,
			sortable: 'count_likes',
		},
		{
			header: 'Hugs',
			size: 7,
			renderer: ColumnTotalHugs,
			sortable: 'count_hugs',
		},
		{
			header: 'Followed Discussions',
			size: 14,
			renderer: ColumnTotalFollowedDiscussions,
			sortable: 'count_followed_discussions',
		},
		{
			header: 'Total Blogs',
			size: 10,
			renderer: ColumnTotalBlogs,
			sortable: 'count_blogs',
		},
		{
			header: 'Saved Resources',
			size: 12,
			renderer: ColumnTotalSavedResources,
			sortable: 'count_saved_resources',
		},
	];

	protected originalFilterValues = {
		sort: 'username-asc',
		user_id: '',
		name: '',
		username: '',
		email: '',
		age: [],
		state: [],
		gender: [],
		situations: [],
		cancerTypes: [],
		lastLoginFrom: null,
		lastLoginTo: null,
		excludeAdmin: true,
		excludeNotVerified: true,
		includeInactive: false,
	};

	constructor(props: IProps) {
		super(props);

		this.state = {
			data: [],
			currentPage: 0,
			maxPages: 1,
			perPage: 0,
			total: 0,
			isLoadingUsers: false,
			filterOpen: false,
			filterValues: JSON.parse(JSON.stringify(this.originalFilterValues)),
			isCreatUserModalActive: false,
			situations: null,
			isUpdating: false,
			updatingUserId: 0,
			isLoadingPermission: false,
			permissions: [],
		};

		this.displayUser = this.displayUser.bind(this);
		this.loadUsers = this.loadUsers.bind(this);
		this.loadSituations = this.loadSituations.bind(this);
		this.handleOpenFilter = this.handleOpenFilter.bind(this);
		this.handleCloseFilter = this.handleCloseFilter.bind(this);
		this.handleFilter = this.handleFilter.bind(this);
		this.handleClear = this.handleClear.bind(this);
		this.onUpdateSort = this.onUpdateSort.bind(this);
		this.handleNewUserModal = this.handleNewUserModal.bind(this);
		this.submitNewUserPayload = this.submitNewUserPayload.bind(this);
		this.handleChangeGraduate = this.handleChangeGraduate.bind(this);
		this.initialise = this.initialise.bind(this);
	}

	public componentDidMount() {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EGraduateUserManagementPermissions.ALL,
				EGraduateUserManagementPermissions.READ,
			);

		if (readPermission) {
			this.initialise(this.props);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EGraduateUserManagementPermissions.ALL,
				EGraduateUserManagementPermissions.READ,
			);

		if (readPermission) {
			this.initialise(this.props, prevProps);
		}
	}

	public render() {
		const {
			data,
			isLoadingUsers,
			currentPage,
			maxPages,
			total,
			filterValues,
			isUpdating,
			updatingUserId,
		} = this.state;

		const editPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EUsersManagementPermissions.ALL,
				EUsersManagementPermissions.EDIT,
			);
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				this.props.permissions,
				EGraduateUserManagementPermissions.ALL,
				EGraduateUserManagementPermissions.READ,
			);

		const tableParams = {
			tableColumns: this.tableColumns,
			data: data,
			onRowClick: this.displayUser,
			loading: isLoadingUsers,
			onLoadMore:
				currentPage < maxPages && readPermission
					? this.loadUsers
					: undefined,
			totalMatchedRecords: total,
			sort: filterValues.sort,
			onUpdateSort: this.onUpdateSort,
			onChangeGraduate: editPermission
				? this.handleChangeGraduate
				: undefined,
			isUpdating,
			updatingUserId,
		};

		let heroInfo = `Showing ${this.state.data.length} of ${this.state.total} users`;
		if (this.state.isLoadingUsers && this.state.currentPage === 0) {
			heroInfo = 'Loading...';
		}

		return (
			<div>
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading
							title="Graduated User"
							subInfo={heroInfo}
						/>
						<span className="hero-actions">
							<Button
								variant="outlined"
								color="primary"
								onClick={this.handleOpenFilter}
							>
								Filter Users
							</Button>
						</span>
					</header>
					<div className="admin_content">
						<AdminTable {...tableParams} />
					</div>
					{this.renderFilters()}
				</LayoutDashboard>
			</div>
		);
	}

	protected renderFilters() {
		return (
			<AdminFilter
				isOpen={this.state.filterOpen}
				handleCloseFilter={this.handleCloseFilter}
			>
				<AdminFilterUsers
					initialValues={this.state.filterValues}
					handleFilter={this.handleFilter}
					handleClear={this.handleClear}
				/>
			</AdminFilter>
		);
	}

	protected initialise(props: IProps, prevProps?: IProps) {
		const readPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				props.permissions,
				EGraduateUserManagementPermissions.ALL,
				EGraduateUserManagementPermissions.READ,
			);
		const permissions = prevProps ? prevProps.permissions : [];
		const prevReadPermission: boolean =
			this.props.roleName === 'admin' ||
			checkPermission(
				permissions,
				EGraduateUserManagementPermissions.ALL,
				EGraduateUserManagementPermissions.READ,
			);
		if (
			(props &&
				!prevProps &&
				props.userAuthenticated &&
				props.userAdmin) ||
			(props &&
				prevProps &&
				props.userAuthenticated !== prevProps.userAuthenticated &&
				props.userAdmin) ||
				(!prevReadPermission && readPermission)
		) {
			this.loadUsers(false);
			this.loadSituations();
		}
	}

	protected loadUsers(append: boolean = true) {
		if (
			!this.state.isLoadingUsers &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			let params: any = processQueryParams(
				JSON.parse(JSON.stringify(this.state.filterValues)),
			);

			if (append) {
				params.page = this.state.currentPage + 1;
			}

			const url = buildUrl(null, {
				path: '/api/admin/listGraduateUsers',
				queryParams: params,
			});
			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'users' in response.data &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta &&
								'per_page' in response.meta &&
								'total' in response.meta
							) {
								let data: IApiUser[] = response.data.users;
								if (append) {
									data = JSON.parse(
										JSON.stringify(this.state.data),
									);
									(response.data.users as IApiUser[]).forEach(
										item => {
											data.push(item);
										},
									);
								}

								this.setState({
									data,
									isLoadingUsers: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									perPage: response.meta.per_page,
									total: response.meta.total,
								});
							}
						});
				},
			);
		}
	}

	protected loadSituations() {
		this.setState(
			{
				isLoadingUsers: true,
			},
			() => {
				this.props.apiService
					.queryGET('/api/situations', 'situations')
					.then((situationsList: IApiSituation[]) => {
						const userSituations: IApiSituation[] = [];
						if (situationsList instanceof Array) {
							situationsList.map((situation: IApiSituation) => {
								if (!situation.cancer_type) {
									userSituations.push(situation);
								}
							});
						}

						this.setState({
							situations: userSituations,
							isLoadingUsers: false,
						});
					})
					.catch(error => {
						this.setState({
							situations: null,
							isLoadingUsers: false,
						});

						this.props.dispatch(apiError([error]));
					});
			},
		);
	}

	protected displayUser(user: IApiUser) {
		this.props.dispatch(userPanelDisplay(user));
	}

	protected handleOpenFilter() {
		this.setState({
			filterOpen: true,
		});
	}
	protected handleCloseFilter() {
		this.setState({
			filterOpen: false,
		});
	}

	protected handleFilter(values: any) {
		this.setState(
			{
				filterOpen: false,
				filterValues: values,
				data: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadUsers(false);
			},
		);
	}

	protected handleClear() {
		this.handleFilter(
			JSON.parse(JSON.stringify(this.originalFilterValues)),
		);
	}

	protected handleNewUserModal() {
		this.setState({
			isCreatUserModalActive: !this.state.isCreatUserModalActive,
		});
	}

	protected submitNewUserPayload(values: ICreateUserFormik) {
		if (values) {
			this.setState(
				{
					isLoadingUsers: true,
				},
				() => {
					const url = buildUrl(null, {
						path: '/api/admin/users',
					});

					this.props.apiService
						.queryPOST(url, values)
						.then(() => {
							this.setState({
								isLoadingUsers: false,
								isCreatUserModalActive: false,
							});

							this.props.dispatch(
								apiSuccess([
									'User has successfuly been created',
								]),
							);

							this.loadUsers(false);
						})
						.catch(error => {
							this.setState({
								isLoadingUsers: false,
							});

							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		}
	}

	protected onUpdateSort(sortable: string, direction: 'asc' | 'desc') {
		this.setState(
			{
				filterValues: Object.assign(this.state.filterValues, {
					sort: `${sortable}-${direction}`,
				}),
				data: [],
				currentPage: 0,
				maxPages: 1,
				perPage: 0,
				total: 0,
			},
			() => {
				this.loadUsers(false);
			},
		);
	}

	protected handleChangeGraduate = (user: IApiUser) => {
		this.setState(
			{
				isUpdating: true,
				updatingUserId: user.id,
			},
			() => {
				const url = buildUrl(null, {
					path: `/api/users/${user.username}`,
				});
				const values = { is_graduate: user.is_graduate === 0 ? 1 : 0 };
				console.log('vauie ===> ', values);
				this.props.apiService
					.queryPUT(url, values)
					.then(response => {
						console.log('response ====> ', response);
						this.setState({
							isUpdating: false,
							updatingUserId: 0,
						});

						this.props.dispatch(
							apiSuccess(['User has successfuly been updated']),
						);

						this.loadUsers(false);
					})
					.catch(error => {
						this.setState({
							isUpdating: false,
							updatingUserId: 0,
						});

						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	};
}

export default Users;
