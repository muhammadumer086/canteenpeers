<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Channels\CampaignMonitorTransactional;

class WelcomeNotification extends Notification {
	use Queueable;

	public $user;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($user) {
		$this->user = $user;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [CampaignMonitorTransactional::class];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title =
			'Welcome to ' . config('app.name') . " {$this->user->first_name}";
		return (new MailMessage())
			->bcc('digitech@canteen.org.au')
			->subject($title)
			->view('emails.welcome', [
				'user' => $this->user,
			]);
	}

	public function toSmartTransactional($notifiable) {
		$auth = [
			'api_key' => config('canteen.campaign_monitor_key'),
		];
		$smartEmailId = '247eddb6-363d-43b2-926c-194814204842';
		$wrap = new \CS_REST_Transactional_SmartEmail($smartEmailId, $auth);

		$message = [
			'To' => "{$this->user->first_name} {$this->user->last_name} <{$this->user->email}>",
			'Data' => [
				'firstname' => $this->user->first_name,
				'lastname' => $this->user->last_name,
				'fullname' =>  $this->user->first_name.'%20'.$this->user->last_name,
				'email' => $this->user->email,
			],
		];

		$consentToTrack = 'unchanged';

		$wrap->send($message, $consentToTrack);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
				//
			];
	}
}
