import React from 'react';
import { connect } from 'react-redux';
import TimeAgo from 'react-timeago';

import { ButtonBase, Avatar } from '@material-ui/core';

import UserAvatar from './UserAvatar';
import { userPanelDisplay } from '../../redux/store';

import { IApiUser, IApiBlog, IApiDiscussion } from 'js/interfaces';
import { generateAndFormatTimezoneDate } from '../../../js/helpers/generateTimezoneDate';

interface IProps {
	userData?: IApiUser;
	blogData?: IApiBlog;
	discussionData?: IApiDiscussion;
	resourceData?: IResourceAuthor;
	dispatch(action: any): void;
}

export interface IResourceAuthor {
	authorAvatarUrl: string;
	authorUsername: string;
	timeAgo: string;
	timezone: string;
}

const TileAuthor: React.StatelessComponent<IProps> = props => {
	if (!!props.blogData) {
		const triggerUserDrawer = () => {
			props.dispatch(userPanelDisplay(props.userData));
		};

		return (
			<div
				className={`tile_author ${
					!!props.blogData.prismic_author_image
						? 'tile_author--noaction'
						: ''
				}`}
			>
				{props.blogData.user !== null && (
					<ButtonBase
						className="tile_author-button"
						onClick={triggerUserDrawer}
					/>
				)}

				<div className="tile_author-author_image">
					{!!props.blogData.user && (
						<UserAvatar user={props.blogData.user} />
					)}
					{!!props.blogData.prismic_author_image && (
						<Avatar
							className="tile_author-user_details-avatar"
							src={props.blogData.prismic_author_image}
						/>
					)}
				</div>
				<div className="tile_author-author_details">
					<div className="tile_author-user_name theme-title">
						{props.blogData.prismic_author && (
							<span>{props.blogData.prismic_author}</span>
						)}

						{!!props.blogData.user &&
							props.blogData.user.username && (
								<span>{props.blogData.user.username}</span>
							)}
					</div>

					<div className="tile_author-timeago">
						<TimeAgo
							date={generateAndFormatTimezoneDate(
								props.blogData.first_publication_date.date,
							)}
						/>
					</div>
				</div>
			</div>
		);
	}

	if (!!props.discussionData) {
		const triggerUserDrawer = () => {
			props.dispatch(userPanelDisplay(props.userData));
		};

		return (
			<div className="tile_author">
				{props.discussionData.user !== null && (
					<ButtonBase
						className="tile_author-button"
						onClick={triggerUserDrawer}
					/>
				)}

				<div className="tile_author-author_image">
					{!!props.discussionData.user && (
						<UserAvatar user={props.discussionData.user} />
					)}
				</div>
				<div className="tile_author-author_details">
					<div className="tile_author-user_name theme-title">
						{!!props.discussionData.user &&
							props.discussionData.user.username && (
								<span>
									{props.discussionData.user.username}
								</span>
							)}
					</div>

					<div className="tile_author-timeago">
						<TimeAgo
							date={generateAndFormatTimezoneDate(
								props.discussionData.created_at.date,
							)}
						/>
					</div>
				</div>
			</div>
		);
	}

	if (!!props.resourceData) {
		return (
			<div className="tile_author tile_author--noaction">
				<div className="tile_author-author_image">
					<Avatar
						className="tile_author-user_details-avatar"
						src={props.resourceData.authorAvatarUrl}
					/>
				</div>

				<div className="tile_author-author_details">
					<div className="tile_author-user_name theme-title">
						<span>{props.resourceData.authorUsername}</span>
					</div>

					<div className="tile_author-timeago">
						<TimeAgo
							date={generateAndFormatTimezoneDate(
								props.resourceData.timeAgo,
								props.resourceData.timezone,
							)}
						/>
					</div>
				</div>
			</div>
		);
	}
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(TileAuthor);
