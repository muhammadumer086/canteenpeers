<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\User as UserResource;

class Message extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('message', $this->id, $this->updated_at, true, true)) {
			return $apiStore->getApiResource('message', $this->id, $this->updated_at, true, true);
		}

		$params = [
			'id',
			'message',
			'system_generated',
			'updated_at',
		];


		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);


		// Add the additional relationships
		$result['user'] = new UserResource($this->user);

		// Add the target users
		$result['target_users'] = (count($this->targetUsers)) ?
			UserResource::collection($this->targetUsers) :
			[];

		$apiStore->setApiResource($result, 'message', $this->id, $this->updated_at, true, true);

		return $result;
	}
}
