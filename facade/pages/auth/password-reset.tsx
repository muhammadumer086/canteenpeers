import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';

import { PasswordResetForm }   from '../../js/components/auth/PasswordResetForm';
import MyError from '../_error';
import { ApiService } from '../../js/services';

import {
	apiError,
	IStoreState,
} from '../../js/redux/store';

interface IProps {
	query: any|null,
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
}

class PasswordReset extends React.Component<IProps, IState> {
	static getInitialProps({ query }) {
		return {
			query
		};
	}

	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
		}

		this.onSubmit = this.onSubmit.bind(this);
	}

	public render() {
		if (
			!this.props.query ||
			!('token' in this.props.query)
		) {
			return (
				<MyError statusCode={401} />
			);
		}

		return (
			<div className="auth_wrapper theme--accent_dark">
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<section className="auth">
					<header className="auth-header">
						<h1 className="auth-title font--h4">
							Password Reset
						</h1>
					</header>
					<div className="auth-content theme--main">
						{
							this.state.submitting &&
							<LinearProgress color="secondary" className="auth-content-progress" />
						}
						<PasswordResetForm
							onSubmit={this.onSubmit}
							submitting={this.state.submitting}
						/>
					</div>
					<footer className="auth-footer font--small">
						Back to <Link href="/"><a>{ process.env.APP_NAME }</a></Link>
					</footer>
				</section>
			</div>
		);
	}

	protected onSubmit(values) {
		const submissionValues = JSON.parse(JSON.stringify(values));
		submissionValues.token = this.props.query.token;

		this.setState({
			submitting: true,
		}, () => {
			this.props.apiService.queryPOST('/api/users/password-reset', submissionValues)
			.then(() => {
				// Success
				Router.push('/auth/password-reset-success');
			})
			.catch((error) => {
				this.setState({
					submitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([
					error.message
				]));
			});
		});
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(PasswordReset);
