<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Discussion as DiscussionResource;
use App\Notifications\CanteenNotificationChannel;

class DiscussionReplyNotification extends Notification {
	use Queueable;

	public $reply;
	public $discussion;
	public $mainDiscussion;
	public $user;
	public $author;
	public $followed;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(
		$reply,
		$discussion,
		$mainDiscussion,
		$user,
		$author,
		$followed
	) {
		$this->reply = $reply;
		$this->discussion = $discussion;
		$this->mainDiscussion = $mainDiscussion;
		$this->user = $user;
		$this->author = $author;
		$this->followed = $followed;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		$result = [];
		if ($this->user->is_deleted || $this->user->inactive) {
			return $result;
		}
		if (
			!(
				(!$this->followed &&
					!$this->user->allowEmailDiscussionReply()) ||
				($this->followed &&
					!$this->user->allowEmailDiscussionFollowed())
			)
		) {
			$result[] = 'mail';
		}

		if (
			!(
				(!$this->followed &&
					!$this->user->allowNotificationDiscussionReply()) ||
				($this->followed &&
					!$this->user->allowNotificationDiscussionFollowed())
			)
		) {
			$result[] = 'database';
		}

		/*if (
			!(
				(!$this->followed &&
					!$this->user->allowPushNotificationDiscussionReply()) ||
				($this->followed &&
					!$this->user->allowPushNotificationDiscussionFollowed())
			)
		) {
			$result[] = FcmChannel::class;
		}
		*/
		return $result;
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "Someone replied to {$this->mainDiscussion->title}";
		return (new MailMessage())
			->subject($title)
			->view('emails.discussion-reply', [
				'user' => $this->user,
				'reply' => new DiscussionResource($this->reply),
				'discussion' => $this->discussion,
				'mainDiscussion' => $this->mainDiscussion,
				'author' => $this->author,
				'followed' => $this->followed,
			]);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			'user' => new UserResource($this->user),
			'reply' => new DiscussionResource($this->reply),
			'discussion' => new DiscussionResource($this->discussion),
			'mainDiscussion' => new DiscussionResource($this->mainDiscussion),
			'author' => new UserResource($this->author),
			'followed' => $this->followed,
		];
	}

	public function toFcm($notifiable) {
		/*
		$title = "Someone replied to {$this->mainDiscussion->title}";
		// The FcmNotification holds the notification parameters
		$fcmNotification = FcmNotification::create()
			->setTitle($title)
			->setBody($title);

		// The FcmMessage contains other options for the notification
		return FcmMessage::create()
			->setData([
				'discussion_slug' => $this->mainDiscussion->slug,
				'reply_index' => !empty($this->reply->discussion_index)
					? $this->reply->discussion_index
					: null,
			])
			->setPriority(FcmMessage::PRIORITY_HIGH)
			->setTimeToLive(86400)
			->setNotification($fcmNotification);
		*/
	}
}
