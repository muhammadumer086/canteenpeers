import React from 'react';
import { connect } from 'react-redux';
import TimeAgo from 'react-timeago';

import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
// import Switch from '@material-ui/core/Switch';
import CheckIcon from '@material-ui/icons/CheckCircle';

import {
	apiError,
	apiSuccess,
	IStoreState,
	newMessageDisplay,
	updateData,
	userPanelDisplay,
	userPanelHide,
} from '../../redux/store';

import { generateAndFormatTimezoneDate } from '../../helpers/generateTimezoneDate';
import { IApiUser } from '../../interfaces';
import { ApiService, withServices } from '../../services';
import UserAvatar from '../general/UserAvatar';
import UserStat, { IStat } from '../general/UserStat';
import UserPanelReportUser from './UserPanelReportUser';
import { IUserPanelReportUserFormikValues } from './UserPanelReportUserFormik';
import UserPanelReportUserMenu from './UserPanelReportUserMenu';

export enum EUserPanelState {
	OVERVIEW,
	BLOCK_USER,
	UNBLOCK_USER,
	REPORT_USER,
}

interface IProps {
	userPanelData: IApiUser | null;
	userAuthenticated: boolean;
	userAdmin: boolean;
	userData: IApiUser | null;
	apiService: ApiService;
	onUserUpdated: () => void | null;
	closePane(): void;
	dispatch(action: any): void;
}

interface IState {
	isLoading: boolean;
	isRestoring: boolean;
	isAdmin: boolean;
	isInactive: boolean;
	isAvailableToMessage: boolean | null;
	userBan: number;
	userPanelData: IApiUser | null;
	currentTab: EUserPanelState;
	isSubmitting: boolean;
}

class UserPanelGeneral extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isRestoring: false,
			isAdmin: this.isUserAdmin(props.userPanelData),
			isInactive: this.isUserInactive(props.userPanelData),
			isAvailableToMessage: null,
			userBan: -1,
			userPanelData: props.userPanelData,
			currentTab: EUserPanelState.OVERVIEW,
			isSubmitting: false,
		};

		this.handleToggleRole = this.handleToggleRole.bind(this);
		this.handleBanChange = this.handleBanChange.bind(this);
		this.handleTabChange = this.handleTabChange.bind(this);
		this.updateUser = this.updateUser.bind(this);
		this.openNewConversationModal = this.openNewConversationModal.bind(
			this,
		);
		this.blockUser = this.blockUser.bind(this);
		this.restoreUser = this.restoreUser.bind(this);
		this.unblockUser = this.unblockUser.bind(this);
		this.isAvailableToMessage = this.isAvailableToMessage.bind(this);
		this.reportUserToAdmin = this.reportUserToAdmin.bind(this);
	}

	public componentDidMount() {
		this.setState({
			isAvailableToMessage: this.isAvailableToMessage(
				this.props.userPanelData,
			),
		});
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userPanelData &&
			JSON.stringify(this.props.userPanelData) !==
				JSON.stringify(prevProps.userPanelData)
		) {
			this.setState({
				userPanelData: JSON.parse(
					JSON.stringify(this.props.userPanelData),
				),
				isAvailableToMessage: this.isAvailableToMessage(
					this.props.userPanelData,
				),
				isAdmin: this.isUserAdmin(this.props.userPanelData),
				userBan: -1,
			});
		}
	}

	public render() {
		return (
			<div className="user_panel theme--main">
				{this.state.userPanelData && (
					<section className="user_panel-content">
						{this.state.currentTab === EUserPanelState.BLOCK_USER &&
							this.renderBlockUser()}
						{this.state.currentTab ===
							EUserPanelState.UNBLOCK_USER &&
							this.renderUnblockUser()}
						{this.state.currentTab ===
							EUserPanelState.REPORT_USER && (
							<UserPanelReportUser
								submitting={this.state.isLoading}
								handleTabChange={this.handleTabChange}
								onSubmit={this.reportUserToAdmin}
							/>
						)}
						{this.state.currentTab === EUserPanelState.OVERVIEW &&
							this.renderOverview()}
					</section>
				)}
			</div>
		);
	}

	protected renderOverview() {
		const isAdmin =
			this.props.userPanelData.role_names.indexOf('admin') >= 0;

		const updatedAtFormatTimezoneDate = generateAndFormatTimezoneDate(
			this.props.userPanelData.updated_at.date,
		);

		const userName =
			this.state.userPanelData.username !==
			this.state.userPanelData.full_name
				? this.state.userPanelData.full_name
				: this.state.userPanelData.username;

		return (
			<div className="user_panel-tab user_panel-tab--show-user">
				<div className="user_panel-content-section user_panel-content-section--details">
					<div className="user_panel-content-section-inner">
						<div className="user_panel-details-avatar">
							<div className="user_panel-content-profile-avatar">
								<UserAvatar user={this.state.userPanelData} />
							</div>
						</div>

						<div className="user_panel-details-name">
							<div className="hm-t32 hm-b8">
								<h2 className="font--h5 theme-title">
									{userName}
									{this.state.userPanelData.is_blocked && (
										<> (blocked)</>
									)}
								</h2>
							</div>
						</div>

						{this.props.userPanelData.updated_at && (
							<div className="user_panel-details-time-ago hm-b16">
								<p className="font--small user_panel-content-additional">
									<TimeAgo
										date={updatedAtFormatTimezoneDate}
									/>
								</p>
							</div>
						)}

						{this.state.userPanelData.username !==
							this.state.userPanelData.full_name && (
							<div className="user_panel-details-username">
								<div className="hm-t8 hm-b4">
									<h2 className="theme-title user_panel-content-username">
										<strong>
											{this.state.userPanelData.username}
										</strong>
									</h2>
								</div>
							</div>
						)}

						{this.props.userAdmin && (
							<div className="font--small hm-t16 hm-b16 user_panel-content-additional">
								<div>
									{this.state.userPanelData.first_name}{' '}
									{this.state.userPanelData.last_name}
								</div>
								<div>
									User ID: {this.state.userPanelData.id}
								</div>
								{this.props.userAuthenticated &&
									this.state.userPanelData.role_names &&
									this.state.userPanelData.role_names.length >
										0 && (
										<div>
											Role :{' '}
											{
												this.state.userPanelData
													.role_names[0]
											}
										</div>
									)}
								<div>
									<a
										href={`mailto:${this.state.userPanelData.email}`}
									>
										{this.state.userPanelData.email}
									</a>
								</div>
							</div>
						)}

						{!isAdmin && (
							<div className="user_panel-details-report">
								<div className="hm-b16">
									<UserPanelReportUserMenu
										handleTabChange={this.handleTabChange}
										userPanelData={this.state.userPanelData}
									/>
								</div>
							</div>
						)}
					</div>
				</div>

				{(this.state.userPanelData.gender ||
					this.state.userPanelData.age) && (
					<div className="user_panel-content-section user_panel-content-section--gender_age">
						<div className="user_panel-content-section-inner">
							<div className="user_panel-content-section-user-info">
								{this.state.userPanelData.gender && (
									<div className="user_panel-content-section-user-info-col">
										<p className="user_panel-content-section-user-info-heading">
											Gender
										</p>
										<p className="user_panel-content-section-user-info-description">
											{this.state.userPanelData.gender}
										</p>
									</div>
								)}

								{this.state.userPanelData.age && (
									<div className="user_panel-content-section-user-info-col">
										<p className="user_panel-content-section-user-info-heading">
											Age
										</p>
										<p className="user_panel-content-section-user-info-description">
											{this.state.userPanelData.age}
										</p>
									</div>
								)}
							</div>
						</div>
					</div>
				)}

				{this.props.userAuthenticated &&
					(this.state.userPanelData.situations.length > 0 ||
						!!this.state.isAvailableToMessage) && (
						<div className="user_panel-content-section user_panel-content-section--situations_message">
							<div className="user_panel-content-section-inner">
								{this.state.userPanelData.situations.length >
									0 && (
									<div className="user_panel-user_situations">
										{this.state.userPanelData.situations.map(
											(situation, index) => {
												return (
													<div
														key={index}
														className="user_panel-user_situation"
													>
														<CheckIcon className="user_account_panel-sections-secondary-icon" />
														<span className="user_panel-user_situation_label">
															{situation.name}
														</span>
													</div>
												);
											},
										)}
									</div>
								)}

								{this.renderSendMessage()}
							</div>
						</div>
					)}

				{this.props.userAuthenticated &&
					!!this.state.userPanelData.inactive && (
						<div className="user_panel-content-section user_panel-content-section--user_deactivated">
							<div className="user_panel-content-section-inner">
								<div className="user_panel-user_deactivated-heading">
									User account deactivated
								</div>
								<div className="user_panel-user_deactivated-content">
									If you have any questions about <br />
									this feel free to contact Canteen
								</div>
								{this.props.userAuthenticated &&
									this.props.userAdmin && (
										<div className="user_panel-user_deactivated-footer">
											<Button
												variant="contained"
												color="primary"
												disabled={
													this.state.isRestoring
												}
												onClick={this.restoreUser}
											>
												Restore user
											</Button>
										</div>
									)}
							</div>
						</div>
					)}

				{/* TODO: add the blog count when available */}
				{(this.state.userPanelData.active_discussions_count >= 0 ||
					this.state.userPanelData.blogs_count >= 0) && (
					<div className="user_panel-content-section user_panel-content-section--user_stats">
						<div className="user_panel-content-section-inner">
							{this.state.userPanelData
								.active_discussions_count >= 0 &&
								this.renderUserCount(
									this.state.userPanelData
										.active_discussions_count,
									'Discussion Activity',
									`/discussions/user?id=${this.state.userPanelData.username}`,
									`/discussions/user/${this.state.userPanelData.username}`,
								)}
						</div>
					</div>
				)}

				{this.state.userPanelData.about !== null && (
					<div className="user_panel-content-section user_panel-content-section--about">
						<div className="user_panel-content-section-inner">
							<p className="user_panel-content-section-text">
								{this.props.userPanelData.about}
							</p>
						</div>
					</div>
				)}

				{/* Admin section */}
				{this.props.userAuthenticated &&
					this.props.userAdmin &&
					this.props.userData.email !==
						this.state.userPanelData.email && (
						<div className="user_panel-content-section hp-t32">
							{this.state.isLoading && (
								<LinearProgress
									color="secondary"
									className="user_panel-content-section-loader"
								/>
							)}
							<div className="user_panel-content-section-container">
								<List>
									{/* <ListItem>
										<ListItemText primary="Admin" />
										<ListItemSecondaryAction>
											<Switch
												onChange={this.handleToggleRole}
												checked={this.state.isAdmin}
											/>
										</ListItemSecondaryAction>
									</ListItem> */}
									<ListItem>
										{!this.state.userPanelData
											.is_banned && (
											<ListItemText primary="Ban user for" />
										)}
										{this.state.userPanelData.is_banned && (
											<ListItemText
												primary="Update ban to"
												secondary={
													this.state.userPanelData
														.ban === 1
														? 'Remaining ban: 1 day'
														: this.state
																.userPanelData
																.ban === 999
														? 'Banned permanently'
														: `Remaining ban: ${this.state.userPanelData.ban} days`
												}
											/>
										)}
										<ListItemSecondaryAction>
											<Select
												value={this.state.userBan}
												onChange={this.handleBanChange}
											>
												<MenuItem value={-1}>
													<em>None</em>
												</MenuItem>
												{this.state.userPanelData
													.is_banned && (
													<MenuItem value={0}>
														Cancel Ban
													</MenuItem>
												)}
												<MenuItem value={1}>
													1 Day
												</MenuItem>
												<MenuItem value={7}>
													7 Days
												</MenuItem>
												<MenuItem value={30}>
													30 Days
												</MenuItem>
												<MenuItem value={999}>
													Permanent
												</MenuItem>
											</Select>
										</ListItemSecondaryAction>
									</ListItem>
								</List>

								<Button
									variant="contained"
									color="primary"
									disabled={this.state.isLoading}
									onClick={this.updateUser}
								>
									Update user
								</Button>
							</div>
						</div>
					)}
			</div>
		);
	}

	protected renderUnblockUser() {
		return (
			<div className="user_panel-tab user_panel-tab--unblock-user">
				<div className="user_panel-content-section user_panel-content-section--unblock-user">
					<div className="user_panel-content-section-inner">
						<div className="user_panel-details-avatar">
							<div className="user_panel-content-profile-avatar">
								<UserAvatar user={this.state.userPanelData} />
							</div>
						</div>
						<h2 className="theme-title font--h5 hm-t32 hm-b24">
							Unblock {this.props.userPanelData.username}?
						</h2>
					</div>
				</div>
				<div className="user_panel-content-section user_panel-content-section--are-you-sure">
					<div className="user_panel-content-section-inner">
						<div className="hm-t24">
							<p className="user_panel-content-section-text">
								They will be able to contact you or mention you
								in discussions.
							</p>

							<br />
							<br />

							<p className="user_panel-content-section-text">
								Canteen won't let them know that you've
								unblocked them.
							</p>
						</div>
						<div className="hm-t40">
							<Button
								variant="outlined"
								color="primary"
								disabled={this.state.isSubmitting}
								onClick={this.unblockUser}
							>
								Unblock
							</Button>
						</div>
						<div className="hm-t24">
							<Button
								variant="outlined"
								color="primary"
								className="are-you-sure-button"
								onClick={() =>
									this.handleTabChange(
										EUserPanelState.OVERVIEW,
									)
								}
							>
								Cancel
							</Button>
						</div>
					</div>
				</div>
			</div>
		);
	}

	protected renderBlockUser() {
		return (
			<div className="user_panel-tab user_panel-tab--block-user">
				<div className="user_panel-content-section user_panel-content-section--block-user">
					<div className="user_panel-content-section-inner">
						<div className="user_panel-details-avatar">
							<div className="user_panel-content-profile-avatar">
								<UserAvatar user={this.state.userPanelData} />
							</div>
						</div>
						<h2 className="theme-title font--h5 hm-t32 hm-b24">
							Block {this.props.userPanelData.username}?
						</h2>
					</div>
				</div>
				<div className="user_panel-content-section user_panel-content-section--are-you-sure">
					<div className="user_panel-content-section-inner">
						<div className="hm-t24">
							<p className="user_panel-content-section-text">
								They won’t be able to contact you or mention you
								in discussions.
								<br />
								<br />
								Canteen won’t let them know that you’ve blocked
								them.
							</p>
						</div>
						<div className="hm-t40">
							<Button
								variant="outlined"
								color="primary"
								disabled={this.state.isSubmitting}
								onClick={this.blockUser}
							>
								Block
							</Button>
						</div>
						<div className="hm-t24">
							<Button
								variant="outlined"
								color="primary"
								className="are-you-sure-button"
								onClick={() =>
									this.handleTabChange(
										EUserPanelState.OVERVIEW,
									)
								}
							>
								Cancel
							</Button>
						</div>
					</div>
				</div>
			</div>
		);
	}

	protected renderUserCount(
		count: number,
		label: string,
		href: string = null,
		as: string = null,
	) {
		const stats: IStat = {
			count,
			label,
			inheritBorderColor: true,
		};

		if (this.props.userAuthenticated && href) {
			const linkData = {
				href,
				as,
			};
			return (
				<UserStat
					link={linkData}
					stats={stats}
					action={this.props.closePane}
				/>
			);
		}

		return <UserStat stats={stats} isDisabled={true} />;
	}

	protected renderSendMessage() {
		return (
			!!this.state.isAvailableToMessage && (
				<div className="user_panel-user_message">
					{!!this.state.isInactive ? (
						<></>
					) : (
						<Button
							variant="outlined"
							color="primary"
							onClick={this.openNewConversationModal}
							disabled={
								this.state.isLoading ||
								!this.state.isAvailableToMessage
							}
						>
							Send Message
						</Button>
					)}
				</div>
			)
		);
	}

	protected openNewConversationModal() {
		const userData: IApiUser = JSON.parse(
			JSON.stringify(this.props.userPanelData),
		);

		this.props.dispatch(userPanelHide());
		this.props.dispatch(newMessageDisplay(userData));
	}

	protected handleToggleRole() {
		this.setState({
			isAdmin: !this.state.isAdmin,
		});
	}

	protected handleTabChange(tab: EUserPanelState) {
		this.setState({
			currentTab: tab,
		});
	}

	protected handleBanChange(event) {
		this.setState({
			userBan: event.target.value,
		});
	}

	protected blockUser() {
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/users/block/${this.props.userPanelData.username}`,
						null,
					)
					.then(() => {
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										`You have blocked ${this.props.userPanelData.username}. You can unblock them at any time from their profile panel or your account settings.`,
									]),
								);
								if (this.props.onUserUpdated) {
									this.props.onUserUpdated();
								}

								this.props.closePane();
								this.props.dispatch(updateData());
							},
						);
					})
					.catch(error => {
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(apiError([error.message]));
							},
						);
					});
			},
		);
	}

	protected unblockUser() {
		this.setState(
			{
				isSubmitting: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/users/unblock/${this.props.userPanelData.username}`,
						null,
					)
					.then(() => {
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess([
										`${this.props.userPanelData.username} has been unblocked.`,
									]),
								);
								if (this.props.onUserUpdated) {
									this.props.onUserUpdated();
								}

								this.props.closePane();
								this.props.dispatch(updateData());
							},
						);
					})
					.catch(error => {
						this.setState(
							{
								isSubmitting: false,
							},
							() => {
								this.props.dispatch(apiError([error.message]));
							},
						);
					});
			},
		);
	}

	protected isUserAdmin(user: IApiUser | null) {
		return (
			user &&
			'role_names' in user &&
			user.role_names instanceof Array &&
			user.role_names.indexOf('admin') !== -1
		);
	}

	protected isUserInactive(user: IApiUser | null) {
		if (!user) {
			return true;
		}
		return user.inactive;
	}

	protected updateUser() {
		const data = {
			role: this.state.isAdmin ? 'admin' : 'user',
			ban: this.state.userBan,
		};

		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/users/${this.state.userPanelData.username}`,
						data,
						'user',
					)
					.then((user: IApiUser) => {
						this.setState({
							isLoading: false,
							userPanelData: user,
						});
						this.props.dispatch(userPanelDisplay(user));
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isLoading: false,
						});
					});
			},
		);
	}

	protected restoreUser() {
		this.setState(
			{
				isRestoring: true,
			},
			() => {
				this.props.apiService
					.queryPUT(
						`/api/admin/users/${this.state.userPanelData.username}/restore`,
						{},
						'user',
					)
					.then((user: IApiUser) => {
						this.setState({
							isRestoring: false,
							userPanelData: user,
						});
						this.props.dispatch(userPanelDisplay(user));
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isRestoring: false,
						});
					});
			},
		);
	}

	protected reportUserToAdmin(formValues: IUserPanelReportUserFormikValues) {
		this.setState(
			{
				isLoading: true,
			},
			() => {
				this.props.apiService
					.queryPOST(
						`/api/users/${this.state.userPanelData.username}/report`,
						formValues,
					)
					.then(() => {
						this.setState(
							{
								isLoading: false,
							},
							() => {
								this.props.dispatch(
									apiSuccess(['User has been reported']),
								);

								this.handleTabChange(EUserPanelState.OVERVIEW);
							},
						);
					})
					.catch(error => {
						this.props.dispatch(apiError([error]));
					});
			},
		);
	}

	protected isAvailableToMessage(user: IApiUser) {
		if ('allow_direct_messages' in user) {
			return !!user.allow_direct_messages;
		}
		return false;
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userAdmin, userData } = state.user;
	const { userPanelData, onUserUpdated } = state.userPanel;
	return {
		userPanelData,
		userAuthenticated,
		userAdmin,
		userData,
		onUserUpdated,
	};
}

export default connect(mapStateToProps)(withServices(UserPanelGeneral));
