<?php
namespace App\Traits;

trait HasSeoAttribute {
	public function getSeoAttribute() {
		$result = [
			'title' => $this->seo_title,
			'description' => $this->seo_description,
			'image' =>
				$this->seo_image &&
				is_array($this->seo_image) &&
				isset($this->seo_image['url'])
					? $this->seo_image['url']
					: null,
		];

		// if (!$result['title']) {
		$result['title'] = $this->title;
		// }

		if (!$result['description']) {
			$result['description'] = mb_convert_encoding(
				substr(strip_tags($result['description']), 0, 160),
				'UTF-8',
				'UTF-8'
			);
		}

		if (!$result['image']) {
			$result['image'] =
				$this->feature_image &&
				is_array($this->feature_image) &&
				isset($this->feature_image['url'])
					? $this->feature_image['url']
					: null;
		}

		return $result;
	}
}
