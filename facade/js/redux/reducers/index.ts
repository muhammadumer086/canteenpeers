import { combineReducers } from 'redux';

import { apiError } from './apiError';
import { apiSuccess } from './apiSuccess';
import { contactModal } from './contactModal';
import { discussionModal } from './discussionModal';
import { discussionSupport } from './discussionSupport';
import { eventRegisterInterestModal } from './registerEventInterestModal';
import { landingVideoModal } from './landingVideoModal';
import { leaveConversationModal } from './leaveConversationModal';
import { menuPanel } from './menuPanel';
import { newBlogModal } from './newBlogModal';
import { newMessageModal } from './newMessageModal';
import { notifications } from './notifications';
import { quickLinksModal } from './quickLinksModal';
import { reportModal } from './reportModal';
import { searchModal } from './searchModal';
import { settings } from './settings';
import { situations } from './situations';
import { topics } from './topics';
import { user } from './user';
import { userPanel } from './userPanel';
import { updateData } from './updateData';
import { permission } from './permissions';

export default combineReducers({
	apiError,
	apiSuccess,
	contactModal,
	discussionModal,
	discussionSupport,
	eventRegisterInterestModal,
	landingVideoModal,
	leaveConversationModal,
	menuPanel,
	newBlogModal,
	newMessageModal,
	notifications,
 	permission,
	quickLinksModal,
	reportModal,
	searchModal,
	settings,
	situations,
	topics,
	user,
	userPanel,
	updateData,
});
