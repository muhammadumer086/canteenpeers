<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;

use App\Helpers\CsvMigrationBlogs;
use App\Helpers\CsvMigrationDiscussions;
use App\Helpers\CsvMigrationLikesHugs;
use App\Helpers\CsvMigrationUsers;

class MigrationExecute extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:migration:execute';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Execute Canteen\'s content migration';

	protected $rawCsvData = [];

	protected $fileMigration = null;

	protected function init() {
		$migrationUsers = new CsvMigrationUsers();
		$migrationDiscussions = new CsvMigrationDiscussions();
		$migrationBlogs = new CsvMigrationBlogs();
		$migrationLikesHugs = new CsvMigrationLikesHugs();

		$this->rawCsvData = [
			'users' => [
				'file' => 'users.csv',
				'columns' => $migrationUsers->csvHeaders,
				'reader' => null,
				'migrator' => $migrationUsers,
			],
			'discussions' => [
				'file' => 'discussions.csv',
				'columns' => $migrationDiscussions->csvHeaders,
				'reader' => null,
				'migrator' => $migrationDiscussions,
			],
			'blogs' => [
				'file' => 'blogs.csv',
				'columns' => $migrationBlogs->csvHeaders,
				'reader' => null,
				'migrator' => $migrationBlogs,
			],
			'hugs_likes' => [
				'file' => 'hugs_likes.csv',
				'columns' => $migrationLikesHugs->csvHeaders,
				'reader' => null,
				'migrator' => $migrationLikesHugs,
			],
		];
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$this->init();
		$this->line('');
		$this->displaySectionTitle('Perform tests (1/4)');
		if ($this->performPreTests()) {
			$this->line('');
			$this->displaySectionTitle('Import CSVs (2/4)');
			// Disable the Algolia indexing
			\App\User::disableSearchSyncing();
			\App\Discussion::disableSearchSyncing();
			\App\Blog::disableSearchSyncing();

			// Handle the migration CSVs
			$this->processCSV('users');
			$this->processCSV('discussions');
			$this->processCSV('blogs');
			$this->processCSV('hugs_likes');

			$this->displaySectionTitle('Update counts and threads (3/4)');
			$this->call('canteen:assignUsersInDiscussion');
			$this->call('canteen:assignThreadIdInDiscussion');
			$this->call('canteen:assignIndexInDiscussions');
			$this->call('canteen:updateCounts', [
				'--all' => true,
			]);
			// Important to call this command last
			$this->call('canteen:assignUpdatedAtInDiscussion');

			// Re-enable the algolia search indexing
			\App\User::enableSearchSyncing();
			\App\Discussion::enableSearchSyncing();
			\App\Blog::enableSearchSyncing();

			// Update Algolia
			$this->displaySectionTitle('Update Algolia indexes (4/4)');
			$this->call('canteen:algoliaIndex', ['--private' => true]);
			$this->call('canteen:algoliaIndex');

			$this->line("\r\n");
			$this->info('   ┌──────────────────────────┐');
			$this->info('   │   Migration completed!   │');
			$this->info('   └─  ───────────────────────┘');
			$this->info('     \/');
			$this->line('(ﾉ◕ヮ◕)ﾉ');
			$this->line("\r\n");
		}
	}

	/**
	 * Execute the pre-test making sure everything is ready
	 *
	 * @return boolean
	 */
	protected function performPreTests() {
		$this->info('Starting pre tests');
		$tests = ['testCSVs'];

		foreach ($tests as $testName) {
			if (!$this->$testName()) {
				$this->comment('Stopped pre tests');
				return false;
			}
		}

		$this->info('Finished pre tests');
		return true;
	}

	/**
	 * Test if the CSVs are available and have the required columns
	 *
	 * @return boolean
	 */
	protected function testCSVs() {
		$this->line('Testing CSV files presence:');

		foreach ($this->rawCsvData as &$fileData) {
			$fileName = $fileData['file'];
			$columns = $fileData['columns'];
			$message = "* Testing {$fileName}";
			$fileExists = Storage::disk('migration')->exists($fileName);

			if (!$fileExists) {
				$this->displayStatus($message, false);
				$this->error('File missing');
				return false;
			}

			// Initialise the CSV reader
			$reader = Reader::createFromPath(
				Storage::disk('migration')->path($fileName),
				'r'
			);
			$reader->setHeaderOffset(0);
			$headers = $reader->getHeader();
			// Test the columns
			foreach ($columns as $columnName) {
				if (!in_array($columnName, $headers)) {
					$this->displayStatus($message, false);
					$this->error("Missing column '{$columnName}'");
					return false;
				}
			}

			$fileData['reader'] = $reader;

			$this->displayStatus($message, true);
		}

		return true;
	}

	/**
	 * Display a test status
	 *
	 * @param string $message the name of the test
	 * @param boolean $success set to true if the test failed
	 *
	 * @return void
	 */
	protected function displayStatus(string $message, $success) {
		$statusLength = 50;

		$fullMessage = str_pad($message, $statusLength);
		if ($success) {
			$fullMessage .= '[<fg=green>OK</>]';
		} else {
			$fullMessage .= '[<fg=red>FAIL</>]';
		}

		$this->line($fullMessage);
	}

	protected function displaySectionTitle(string $message) {
		$statusLength = 50;
		$message = str_pad($message, $statusLength, ' ', STR_PAD_BOTH);
		$filling = str_repeat('─', strlen($message));
		$this->line('');
		$this->line("┌──{$filling}──┐");
		$this->line("│  {$message}  │");
		$this->line("└──{$filling}──┘");
		$this->line('');
	}

	protected function processCSV($type) {
		$this->info("Processing {$type}");
		$migrator = $this->rawCsvData[$type]['migrator'];
		$reader = $this->rawCsvData[$type]['reader'];
		$bar = $this->output->createProgressBar(count($reader));

		$records = $reader->getRecords();

		$bar->start();
		foreach ($records as $record) {
			$migrator->importData($record);
			$bar->advance();
		}

		$bar->finish();
		$this->line('');
		$this->info("Finished processing {$type}");
	}
}
