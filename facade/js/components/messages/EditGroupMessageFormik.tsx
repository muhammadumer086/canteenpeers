import React from 'react';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { Formik } from 'formik';

import Button from '@material-ui/core/Button';
import EditGroupUserList from './EditGroupMessageUserList';
import LeaveConversationConfirmModal from './LeaveConversationModal';

import { TextFieldFormik, AutocompleteUserFormik } from '../inputs';
import { IApiConversationDetail, IApiUser } from 'js/interfaces';

import { leaveConversationModalDisplay } from '../../../js/redux/store';

export interface IEditGroupMessageFormikFormValues {
	users: string[];
	currentUsers: IApiUser[];
	name: string;
}

interface IProps {
	isUpdating: boolean;
	activeUsers: IApiUser[];
	conversationDetail: IApiConversationDetail;
	submit(payload: IEditGroupMessageFormikFormValues): Promise<boolean>;
	onLeaveConversation(): void;
	dispatch(action: any): void;
}

const EditGroupMessageFormik: React.StatelessComponent<IProps> = props => {
	const validationFormat = Yup.object().shape({
		users: Yup.array().of(Yup.string().max(191)),
		name: Yup.string(),
	});

	let groupName: string = '';

	if (props.conversationDetail.name) {
		groupName = props.conversationDetail.name;
	}

	return (
		<Formik
			initialValues={{
				users: [],
				currentUsers: props.activeUsers,
				name: groupName,
			}}
			validationSchema={validationFormat}
			onSubmit={(values, { resetForm, setValues }) => {
				let usernames: string[] = [];

				values.currentUsers.map(user => {
					usernames.push(user.username);
				});

				values.users = [...values.users, ...usernames];

				//Remove duplicates from array
				values.users = values.users.reduce((acc, cur) => {
					if (acc.indexOf(cur) < 0) acc.push(cur);
					return acc;
				}, []);

				props.submit(values).then(() => {
					resetForm({
						users: [],
						currentUsers: values.currentUsers,
						name: values.name,
					});

					setValues({
						users: [],
						currentUsers: [],
						name: values.name,
					});
				});
			}}
			render={({
				values,
				touched,
				errors,
				handleChange,
				handleBlur,
				setFieldValue,
				setFieldTouched,
				handleSubmit,
				isValid,
			}) => {
				const inputProps = {
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					setFieldValue,
					handleSubmit,
					setFieldTouched,
					isValid,
				};

				return (
					<React.Fragment>
						<form
							className="edit_group_message_form"
							onSubmit={handleSubmit}
						>
							<div className="form form--border_bottom">
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Enter a group name"
											name="name"
											disabled={props.isUpdating}
											{...inputProps}
										/>
									</div>
								</div>
							</div>

							<div className="form form--no_padding_bottom">
								<div className="form-row">
									<div className="form-column">
										{values.currentUsers &&
											values.currentUsers.length && (
												<EditGroupUserList
													name="currentUsers"
													value={values.currentUsers}
													disabled={props.isUpdating}
													{...inputProps}
												/>
											)}
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<AutocompleteUserFormik
											label="Add members to a group"
											placeholder="Please search for a member"
											name="users"
											disabled={props.isUpdating}
											multi={true}
											{...inputProps}
										/>
									</div>
								</div>
							</div>

							<div className="form form--border_bottom form--no_padding_top">
								<div className="form-row form-row--submit form-row--submit_left">
									<div className="form-column">
										<Button
											variant="contained"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid || props.isUpdating
											}
										>
											Update
										</Button>
									</div>
								</div>
							</div>

							<div className="form">
								<div className="form-row">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit form-submit--error"
											type="button"
											onClick={() => {
												props.dispatch(
													leaveConversationModalDisplay(
														props.conversationDetail
															.id,
													),
												);
											}}
											disabled={props.isUpdating}
										>
											Leave conversation
										</Button>
									</div>
								</div>
							</div>
						</form>

						<LeaveConversationConfirmModal
							onLeaveConversation={props.onLeaveConversation}
						/>
					</React.Fragment>
				);
			}}
		/>
	);
};

function mapStateToProps() {
	return {};
}

export default connect(mapStateToProps)(EditGroupMessageFormik);
