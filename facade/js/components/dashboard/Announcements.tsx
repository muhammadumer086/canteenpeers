import React from 'react';
import { connect } from 'react-redux';

import { IApiAnnouncement } from '../../interfaces';
import { ButtonBase } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

import { IStoreState } from '../../redux/store';
import { ButtonBaseLink } from '../general/ButtonBaseLink';

interface IProps {
	userAuthenticated: boolean | null;
	userAdmin: boolean | null;
	announcement: IApiAnnouncement;
	closeAnnouncement(id: number): void;
}

const Announcements: React.FC<IProps> = (props: IProps) => {
	const classNames = ['announcement-tile', 'theme--main'];

	if (!!props.announcement.feature_image) {
		classNames.push('announcement-tile--with_image');
	}

	const styles = {};

	if (!!props.announcement.feature_image) {
		styles['backgroundImage'] = `url(${
			props.announcement.feature_image.url
		})`;
	}

	return (
		<section className="announcements">
			<div className="announcements-container">
				<div className={classNames.join(' ')}>
					{!!props.announcement.url && (
						<ButtonBaseLink
							className="general_tile-button"
							hrefAs={props.announcement.url.hrefAs}
							href={props.announcement.url.href}
						/>
					)}

					<div className="announcement-tile_image-container">
						<div
							className="announcement-tile_image"
							style={styles}
						/>
					</div>
					<div className="announcement-tile_body theme--main">
						<span className="announcement-tile_title">
							{props.announcement.title}
						</span>
						<div className="announcement-tile_message">
							<div
								className="markdown"
								dangerouslySetInnerHTML={{
									__html: props.announcement.message,
								}}
							/>
						</div>

						{!!props.announcement.url_label &&
							!!props.announcement.url && (
								<div className="announcement-tile_url-label">
									<ButtonBase
										disableRipple={true}
										onClick={() =>
											props.closeAnnouncement(
												props.announcement.id,
											)
										}
									>
										<span className="got_it_link">
											{props.announcement.url_label}
										</span>
									</ButtonBase>
								</div>
							)}

						{!props.announcement.public && !props.userAdmin && (
							<button
								onClick={() =>
									props.closeAnnouncement(
										props.announcement.id,
									)
								}
								className="close_button close_button--primary"
								title="Dismiss"
							>
								<ClearIcon className="close_button-icon" />
							</button>
						)}
					</div>
				</div>
			</div>
		</section>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userAdmin } = state.user;
	return {
		userAuthenticated,
		userAdmin,
	};
}

export default connect(mapStateToProps)(Announcements);
