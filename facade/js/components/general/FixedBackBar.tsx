import React from 'react';
import { Icon } from '@material-ui/core';
import { ButtonBaseLink } from './ButtonBaseLink';

interface IProps {
	href: string;
	label: string;
}

const FixedBackBar: React.StatelessComponent<IProps> = props => {
	return (
		<div className="fixed_bar theme--accent">
			<ButtonBaseLink
				focusRipple={true}
				className="fixed_bar-button"
				 
				href={props.href}
			>
				<Icon className="fixed_bar-back_icon">arrow_left</Icon>
				{props.label}
				<span className="fixed_bar-button-border" />
			</ButtonBaseLink>
		</div>
	);
};

export default FixedBackBar;
