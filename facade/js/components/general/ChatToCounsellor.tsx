import React from 'react';
import ReactSVG from 'react-svg';
import { connect } from 'react-redux';

import { ButtonBase } from '@material-ui/core';

import { menuPanelHide } from '../../../js/redux/store';

interface IProps {
	dispatch(action: any): void;
}

const ChatToCounsellor: React.StatelessComponent<IProps> = props => {
	const iconPath = `${process.env.STATIC_PATH}/icons/counsellor.svg`;
	const svgStyle = {};

	const handleClick = () => {
		props.dispatch(menuPanelHide());
	};

	return (
		<div className="chat-counsellor">
			<ButtonBase
				className="chat-counsellor_button consellor_chat"
				onClick={handleClick}
				href={`mailto:${process.env.INTERCOM_APP_ID}@intercom-mail.com`}
			/>
			<div className="chat-counsellor_icon-container">
				<ReactSVG
					className="chat-counsellor_icon"
					svgStyle={svgStyle}
					path={iconPath}
				/>
			</div>

			<div className="chat-counsellor_label">
				<span>Chat to counsellor</span>
			</div>
		</div>
	);
};

const mapStateToProps = () => {
	return {};
};

export default connect(mapStateToProps)(ChatToCounsellor);
