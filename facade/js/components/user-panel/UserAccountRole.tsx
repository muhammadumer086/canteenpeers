import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button     from '@material-ui/core/Button';
import Icon       from '@material-ui/core/Icon';

import {  ISelectOption,IApiUser  } from '../../interfaces';
import { ETabs, EAccountScreens } from './UserPanelAccount';
import {  SelectFieldFormik } from '../inputs';

interface IProps {
    userData : IApiUser;
	rolesOptions : ISelectOption[];
    
	submitting: boolean;

    isAdminEdit:boolean;
	changeScreen(tab: ETabs, screen: EAccountScreens);
	updateUserRole(role: string);
}

const UserAccountRole: React.SFC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		role: Yup.string().required(),
	
	});

	


	const initialValues = {
		role: props.userData.role_names[0],
	};



	return (
		<div className="user_account_panel user_account_panel--account_optional">
			<div className="user_panel-back">
				<ButtonBase
					focusRipple={true}
					className="user_panel-back-button"
					onClick={() => {
						props.changeScreen(ETabs.MY_PROFILE, EAccountScreens.ROLE)
					}}
					disabled={props.submitting}
				>
					<Icon className="user_panel-back-back_icon">arrow_back</Icon>
					Profile Overview
				</ButtonBase>
			</div>
			<div className="user_panel-heading">
				<h4 className="font--h4 theme-title">Role Details</h4>
			</div>
			<Formik
				initialValues={initialValues}
				validationSchema={validationFormat}
				onSubmit={(values) => {
					props.updateUserRole(values.role);
				}}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form className="form" onSubmit={handleSubmit}>


							<div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="Select Role"
                                        name="role"
                                        noEmpty
										options={props.rolesOptions}
										disabled={props.submitting||!props.isAdminEdit}
										emptyLabel="Please select"
										{...inputProps}
									/>
								</div>
							</div>
			
			
							<div className="form-row form-row--submit_left">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || !touched || props.submitting}
									>
										Update
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
}

export default UserAccountRole;
