<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventImageTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('event_images', function (Blueprint $table) {
			$table->uuid('id')->primary();
			$table->string('path');
			$table->unsignedBigInteger('event_id');
			$table->unsignedBigInteger('width');
			$table->unsignedBigInteger('height');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('event_images');
	}
}
