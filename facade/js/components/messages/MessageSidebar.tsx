import React, { Fragment } from 'react';
import ButtonBase from '@material-ui/core/Button';
import MessageInboxView from './MessageInboxView';
import { IApiConversationDetail, IApiUser } from 'js/interfaces';
import AddIcon from '@material-ui/icons/Add';

interface IProps {
	userData: IApiUser;
	inboxData: IApiConversationDetail[];
	activeConversationId: number | null;
	conversationPanelActive: boolean;
	chooseConversation(id: number): void;
	startNewMessage(): void;
}

const MessageSidebar = (props: IProps) => {
	const classes = ['user_messages-sidebar'];

	if (props.conversationPanelActive) {
		classes.push('user_messages-sidebar--panel_active');
	}

	return ( 
		<div className={classes.join(' ')}>
			<div className="user_messages-message_action">
				{props.userData && !props.userData.underage ? (
					<Fragment>
						<span className="user_messages-message_action-title">
							New chat/group
						</span>
						<ButtonBase
							variant="outlined"
							color="primary"
							onClick={props.startNewMessage}
							className="user_messages-message_action-button"
						>
							<AddIcon className="user_messages-message_action-button-icon" />
						</ButtonBase>
					</Fragment>
				) : (
					<span className="user_messages-message_action-title">
						Chats
					</span>
				)}
			</div>

			<div className="user_messages-inbox_list">
				{!!props.inboxData &&
					props.inboxData.map((messageData, index) => {
						return (
							<MessageInboxView
								key={index}
								isActive={
									props.activeConversationId ===
									messageData.id
								}
								messageData={messageData}
								inboxAction={props.chooseConversation}
							/>
						);
					})}
			</div>
		</div>
	);
};

export default MessageSidebar;
