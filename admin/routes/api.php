<?php

use App\Situation;
use App\State;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

// Prismic webhook endpoint
Route::post('/prismic/webhook', 'API\PrismicController@prismicWebhook');

// Side of Stage pre-population
Route::get(
	'/sideofstage/register/{token}',
	'API\SideOfStageController@register'
);

/////////////////////////////////////////////////////
//
// User
//
/////////////////////////////////////////////////////

// User authentication routes
Route::post(
	'/users/verify-duplicate',
	'API\UserAuthenticationController@verifyDuplicates'
);
Route::post('/users/verify', 'API\UserAuthenticationController@verify');

Route::post(
	'/users/resend-verification',
	'API\UserAuthenticationController@resendVerification'
);

Route::post(
	'/users/password-forgot',
	'API\UserAuthenticationController@passwordForgot'
);

Route::post(
	'/users/password-reset',
	'API\UserAuthenticationController@passwordReset'
);

Route::post(
	'/users/activate-migrated',
	'API\UserAuthenticationController@activateMigrated'
);

Route::post(
	'/users/smscode',
	'API\UserAuthenticationController@getSMSCode'
);

Route::post(
	'/users/validateUser',
	'API\UserAuthenticationController@validateUser'
);


Route::post(
	'/users/verifycode',
	'API\UserAuthenticationController@verifyMobileCode'
);



Route::post(
	'/users/authenticate',
	'API\UserAuthenticationController@authenticate'
);

Route::post('/users/refresh', 'API\UserAuthenticationController@refresh');
Route::middleware(['auth:api'])->group(function () {
	Route::put('/users/{username}/verified', 'API\UserAuthenticationController@markVerified');
	Route::put('/users/{username}/unverified', 'API\UserAuthenticationController@markUnverified');
});
// User
Route::get('/users/me', 'API\UserController@showMe');
Route::get(
	'/users/getUserByEmail/{email}',
	'API\UserController@getUserByEmail'
);


Route::middleware(['auth:api'])->group(function () {
	// User avatar
	Route::post('/users/{username}/avatar', 'API\UserController@uploadAvatar');

	// User avatar
	Route::post('/users/{username}/reSendVerificationEmail', 'API\UserController@reSendVerificationEmail');

	// Get the active and followed posts of the current user
	Route::get(
		'/users/me/active',
		'API\DiscussionController@activeDiscussions'
	);

	Route::get(
		'/users/me/followed',
		'API\DiscussionController@followedDiscussions'
	);

	Route::get('/users/me/saved', 'API\ResourceController@savedResources');

	// Get the discussions authored by a username
	Route::get(
		'/users/{username}/discussions',
		'API\DiscussionController@userDiscussionsActivity'
	);

	Route::post('/users/{username}/report', 'API\UserController@reportUser');

	// Get blocked users
	Route::get('/users/blocks', 'API\UserController@blocks');

	// Toggle the block on a user
	Route::put('/users/block/{username}', 'API\UserController@blockUser');
	Route::put('/users/unblock/{username}', 'API\UserController@unblockUser');

	Route::put('/users/{username}/active', 'API\UserController@markActive');

	Route::put('/users/{username}/inactive', 'API\UserController@markInactive');

	// Get users in the same situation as current user
	Route::get(
		'/users/me/similar-users',
		'API\UserController@similarUsers'
	)->name('similarUsers');

	// Public subscribe to campaign monitor
	Route::post(
		'/users/newsletter/subscribe',
		'API\UserController@publicSubscribeToCampaignMonitor'
	);

	Route::get('/users/me/notifications', 'API\NotificationController@index');
	Route::get(
		'/users/me/notifications/recent',
		'API\NotificationController@recent'
	);
	Route::get(
		'/users/me/notifications/count',
		'API\NotificationController@count'
	);
	Route::get(
		'/users/me/notifications/{id}',
		'API\NotificationController@show'
	);

	Route::apiResource('users', 'API\UserController')->only([
		'index',
		'show',
		'update',
		'destroy',
	]);
});

Route::apiResource('users', 'API\UserController')->only(['store']);


/////////////////////////////////////////////////////
//
// Situations
//
/////////////////////////////////////////////////////

Route::apiResource('situations', 'API\SituationController')->only(['index']);

/////////////////////////////////////////////////////
//
// Dashboard
//
/////////////////////////////////////////////////////

Route::get('/dashboard', 'API\DashboardController@index');

/////////////////////////////////////////////////////
//
// Discussions
//
/////////////////////////////////////////////////////

Route::get('discussions/{id}/replies', 'API\DiscussionController@replies');

Route::get(
	'discussions/{id}/thread-replies',
	'API\DiscussionController@threadReplies'
);

Route::get(
	'discussions/{id}/relevant',
	'API\DiscussionController@relevantDiscussion'
);

Route::middleware(['auth:api'])->group(function () {
	Route::post('discussions/{id}/reply', 'API\DiscussionController@reply');

	Route::post(
		'discussions/{id}/toggle-like',
		'API\DiscussionController@toggleLike'
	);

	Route::post(
		'discussions/{id}/toggle-hug',
		'API\DiscussionController@toggleHug'
	);

	Route::post(
		'discussions/{id}/toggle-follow',
		'API\DiscussionController@toggleFollow'
	);

	Route::get('discussions/{id}/likes', 'API\DiscussionController@indexLikes');

	Route::get('discussions/{id}/hugs', 'API\DiscussionController@indexHugs');

	Route::get(
		'discussions/{id}/supports',
		'API\DiscussionController@indexSupports'
	);

	Route::apiResource(
		'discussion-images',
		'API\DiscussionImageController'
	)->only(['store']);
});

Route::apiResource('discussions', 'API\DiscussionController')->only([
	'index',
	'show',
]);
Route::apiResource('discussions', 'API\DiscussionController')
	->only(['store', 'update', 'destroy'])
	->middleware('auth:api');

/////////////////////////////////////////////////////
//
// Report Discussions
//
/////////////////////////////////////////////////////

Route::apiResource('report-discussions', 'API\ReportDiscussionController')
	->only(['index', 'store', 'update', 'destroy'])
	->middleware('auth:api');

/////////////////////////////////////////////////////
//
// Blog
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::apiResource('blogs', 'API\BlogController')->only([
		'store',
		'update',
		'destroy',
	]);
	Route::apiResource('blog-images', 'API\BlogImageController')->only([
		'store',
	]);
	Route::post('blogs/{blog}/share', 'API\BlogController@share');
});

Route::apiResource('blogs', 'API\BlogController')->only(['index', 'show']);


/////////////////////////////////////////////////////
//
// Messaging
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api', 'checkAgeConversation'])->group(function () {
	Route::delete(
		'conversations/{conversationId}/leave',
		'API\ConversationController@leaveConversation'
	);
	Route::get(
		'conversations/{conversationId}/messages',
		'API\ConversationController@replies'
	);
	Route::post(
		'conversations/{conversationId}/messages',
		'API\ConversationController@storeReply'
	);

	Route::apiResource('conversations', 'API\ConversationController')->only([
		'index',
		'show',
		'store',
		'update',
	]);
});
/////////////////////////////////////////////////////
//
// Resources
//
/////////////////////////////////////////////////////

Route::apiResource('resources', 'API\ResourceController')->only([
	'index',
	'show',
]);
Route::post(
	'resources/{resource}/save',
	'API\ResourceController@toggleSave'
)->middleware('auth:api');
Route::post(
	'resources/{resource}/share',
	'API\ResourceController@share'
)->middleware('auth:api');

/////////////////////////////////////////////////////
//
// Topics
//
/////////////////////////////////////////////////////

Route::get('topics', 'API\TopicController@index');

Route::middleware(['auth:api'])->group(function () {
	Route::put('topics', 'API\TopicController@batchUpdate');
	Route::post('topics', 'API\TopicController@store');
});

/////////////////////////////////////////////////////
//
// Announcements
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::get('announcements/recent', 'API\AnnouncementController@recent');
	Route::get(
		'announcements/{announcement}/close',
		'API\AnnouncementController@closeAnnouncement'
	);

	//Route::middleware(['role:admin'])->group(function () {
		Route::apiResource(
			'announcement-images',
			'API\AnnouncementImageController'
		)->only(['store']);

		Route::apiResource('announcements', 'API\AnnouncementController')->only(
			['index', 'store', 'update', 'destroy']
		);
	//});
});

/////////////////////////////////////////////////////
//
// Staff
//
/////////////////////////////////////////////////////
Route::apiResource('staff', 'API\StaffController')->only(['index', 'show']);

Route::middleware(['auth:api'])->group(function () {
		Route::apiResource('staff', 'API\StaffController')->only([
			'store',
			'update',
			'destroy',
		]);
		Route::apiResource('staff-images', 'API\StaffImageController')->only([
			'store',
		]);
	});

/////////////////////////////////////////////////////
//
// Team
//
/////////////////////////////////////////////////////

Route::apiResource('teams', 'API\TeamController')->only(['index', 'show']);

Route::middleware(['auth:api'])->group(function () {
	Route::middleware(['role:admin'])->group(function () {
		Route::apiResource('teams', 'API\TeamController')->only([
			'store',
			'update',
			'destroy',
		]);
	});
});

/////////////////////////////////////////////////////
//
// Contact Enquiries
//
/////////////////////////////////////////////////////

Route::post('contact', 'API\ContactController@store');
Route::post('reportProblem','API\ContactController@reportProblem');
/////////////////////////////////////////////////////
//
// Languages
//
/////////////////////////////////////////////////////

Route::apiResource('languages', 'API\LanguageController')->only(['index']);

/////////////////////////////////////////////////////
//
// Countries
//
/////////////////////////////////////////////////////

Route::apiResource('countries', 'API\CountryController')->only(['index']);

/////////////////////////////////////////////////////
//
// Heard About
//
/////////////////////////////////////////////////////

Route::apiResource('heard-abouts', 'API\HeardAboutController')->only(['index']);

/////////////////////////////////////////////////////
//
// Personal Settings
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::apiResource(
		'personal-settings',
		'API\PersonalSettingController'
	)->only(['index']);
});

/////////////////////////////////////////////////////
//
// App Settings
//
/////////////////////////////////////////////////////

Route::apiResource('settings', 'API\SettingsController')->only(['index']);

/////////////////////////////////////////////////////
//
// Newsletter
//
/////////////////////////////////////////////////////

Route::post('newsletter/subscribe', 'API\NewsletterController@subscribe');

/////////////////////////////////////////////////////
//
// Events
//
/////////////////////////////////////////////////////

Route::apiResource('events', 'API\EventController')->only(['index', 'show']);
Route::get('/events/{event}/list', 'API\EventController@listUsers');
Route::middleware(['auth:api'])->group(function () {
	Route::put('/events/{event}/register', 'API\EventController@register');
	Route::put('/events/{event}/duplicate', 'API\EventController@duplicate');
	Route::put('/events/{event}/uploadmedia', 'API\EventController@storeEventMedia');

	Route::apiResource('event-images', 'API\EventImageController')->only([
		'store',
	]);

	Route::apiResource('events', 'API\EventController')->only([
		'store',
		'update',
		'destroy',
	]);
});



/////////////////////////////////////////////////////
//
// Roles
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::get('roles/getRolePermissions', 'API\RoleController@getRolePermissionsByName');
	Route::get('roles/{role}', 'API\RoleController@getRolePermissions');
});
Route::middleware(['auth:api', 'role:admin'])->group(function () {
	Route::get('roles', 'API\RoleController@index');
	Route::put('/roles/{role}/assignpermissions', 'API\RoleController@assignPermissionToRole');
	Route::put('/roles/{role}/revokepermissions', 'API\RoleController@removePermissionToRole');
	Route::put('/roles/{user}/{role}/assignrole', 'API\RoleController@assignRoleToUser');

	Route::apiResource('roles', 'API\RoleController')->only([
		'store',
		'update',
		'destroy',
	]);
});

/////////////////////////////////////////////////////
//
// Permissions
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api', 'role:admin'])->group(function () {
	Route::get('permissions', 'API\PermissionController@index');
	Route::apiResource('permissions', 'API\PermissionController')->only([
		'store',
		'update',
		'destroy',
	]);
});

/////////////////////////////////////////////////////
//
// Links
//
/////////////////////////////////////////////////////

Route::apiResource('links', 'API\LinkController')->only(['index']);

Route::middleware(['auth:api', 'role:admin'])->group(function () {
	Route::apiResource('links', 'API\LinkController')->only([
		'store',
		'update',
		'destroy',
	]);

	Route::apiResource('link-images', 'API\LinkImageController')->only([
		'store',
	]);
});

/////////////////////////////////////////////////////
//
// Hashtags
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::apiResource('/hashtags', 'API\HashtagController');
});

/////////////////////////////////////////////////////
//
// Mentions
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::apiResource('/mentions', 'API\MentionController');
});

/////////////////////////////////////////////////////
//
// Admin
//
/////////////////////////////////////////////////////

Route::middleware(['auth:api'])->group(function () {
	Route::get('/admin/activities', 'API\AdminController@activities');
	Route::get('/admin/users', 'API\AdminController@usersList');
	Route::get(
		'/admin/users/{username}/activities',
		'API\AdminController@userActivities'
	);
	Route::put(
		'/admin/users/{username}/restore',
		'API\AdminController@restoreUser'
	);
	Route::get(
		'/admin/activities/export',
		'API\AdminController@activitiesExport'
	);
	Route::get(
		'/admin/listGraduateUsers',
		'API\AdminController@getGraduateUsers'
	);
	Route::get('/admin/users/export', 'API\AdminController@userActivityExport');
	Route::get(
		'/admin/users/{username}/activities/export',
		'API\AdminController@userActivitiesExport'
	);
	Route::get(
		'/admin/users/{username}/export',
		'API\AdminController@userExport'
	);
	Route::post('/admin/users', 'API\UserController@register');
});
