<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\LinkImage;

class Link extends Model {
	protected $fillable = ['title', 'subtitle', 'url'];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'url' => 'array',
	];

	public function image() {
		return $this->hasOne(LinkImage::class);
	}
}
