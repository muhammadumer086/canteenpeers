<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Discussion;

class AssignUsersInDiscussion extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:assignUsersInDiscussion';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Temporary command to re-assign users to discussions';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		\App\User::disableSearchSyncing();
		\App\Discussion::disableSearchSyncing();

		$this->line('');
		$this->info('Assigning the users in discussions');
		$discussionsQuery = Discussion::whereNull('main_discussion_id');

		$bar = $this->output->createProgressBar($discussionsQuery->count());
		// Loop through all the main discussions
		$discussionsQuery->chunk(10, function ($discussions) use ($bar) {
			foreach ($discussions as $singleDiscussion) {
				// Remove all the users in the discussion
				$singleDiscussion->usersInDiscussion()->detach();

				// Attach the user to the discussion
				$singleDiscussion
					->usersInDiscussion()
					->attach($singleDiscussion->user_id);

				// Attach the users in the reply back to the discussion
				$singleDiscussion
					->childDiscussions()
					->chunk(100, function ($replies) use ($singleDiscussion) {
						$userIds = [];
						foreach ($replies as $singleReply) {
							$userIds[] = $singleReply->user_id;
						}
						$singleDiscussion
							->usersInDiscussion()
							->syncWithoutDetaching($userIds);
					});

				$bar->advance();
			}
		});

		$bar->finish();
		$this->info("\r\nFinished assigning users in discussions");
		$this->line('');
	}
}
