import React from 'react';
import { IApiUser } from '../../interfaces';

import { UserProfileCompletion } from '../../helpers/UserProfileCompletion'
import CompletionBar from '../general/CompletionBar';

interface IProps {
	userData: IApiUser;
	showOptionalText: boolean;
}

const UserCompletion: React.FC<IProps> = (props: IProps) => {
	const userProfileCompletion = new UserProfileCompletion(props.userData);

	const profileCompletion: number = Math.min(
		100,
		Math.floor(userProfileCompletion.getUserProfileCompletion() * 100)
	);

	return (
		<React.Fragment>
			<CompletionBar value={profileCompletion} />
			{
				(profileCompletion !== 100) &&
				<div className="user_completion">

					<p className="form-text user_completion-text">Your profile is {profileCompletion}% complete. Get it to 100% by providing some additional information which will help you find more relevant info and people.</p>

					{
						props.showOptionalText &&
						<p className="form-text user_completion-text"><br /><i>(You can always do this part later in your profile).</i></p>
					}
				</div>
			}
		</React.Fragment>
	)
}

export default UserCompletion;
