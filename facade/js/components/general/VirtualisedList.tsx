import React from 'react';
import { connect } from 'react-redux';
import buildUrl from 'build-url';
import dotProp from 'dot-prop-immutable';
import ResizeObserver from 'resize-observer-polyfill';

import { ApiService, withServices } from '../../services';

import { IStoreState, apiError } from '../../redux/store';

enum EStatus {
	INITIALISED = 0,
	LOADING,
	LOADED,
	SIZED,
}

interface IListItem {
	index: number;
	height: number;
	cummulatedHeight: number;
	data: any;
}

interface IPropsItem {
	key: number;
	item: IListItem;
	focussedIndex: number;
	renderCell(data: any | null);
	handleItemResize(height: number, index: number);
}

interface IStateItem {
	refElement: HTMLDivElement | null;
	currentHeight: number;
	focussed: boolean;
}

export type TOrder = 'ASC' | 'DESC';

class VirtualisedListItem extends React.Component<IPropsItem, IStateItem> {
	protected refElement: HTMLDivElement = null;
	protected resizeObserver = null;

	constructor(props: IPropsItem) {
		super(props);

		this.state = {
			refElement: null,
			currentHeight: props.item.height,
			focussed: props.item.index === props.focussedIndex,
		};

		this.resizeObserver = new ResizeObserver(entries => {
			for (const entry of entries) {
				const { height } = entry.contentRect;

				if (height !== this.state.currentHeight) {
					this.setState(
						{
							currentHeight: height,
						},
						() => {
							this.props.handleItemResize(
								this.state.currentHeight,
								this.props.item.index,
							);
						},
					);
				}
			}
		});
	}

	public componentDidUpdate(oldProps: IPropsItem) {
		if (
			oldProps.item.index !== this.props.item.index ||
			oldProps.focussedIndex !== this.props.focussedIndex
		) {
			this.setState({
				focussed: this.props.item.index === this.props.focussedIndex,
			});
		}
	}

	public componentWillUnmount() {
		if (this.refElement) {
			this.resizeObserver.unobserve(this.refElement);
		}
	}

	public render() {
		const classNames = ['virtualised_list-item'];

		if (this.state.focussed) {
			classNames.push('virtualised_list-item--focussed');
		}

		return (
			<div
				className={classNames.join(' ')}
				ref={elem => {
					if (elem !== this.state.refElement) {
						if (this.refElement) {
							this.resizeObserver.unobserve(this.refElement);
						}
						if (elem) {
							this.resizeObserver.observe(elem);
						}
						this.refElement = elem;
					}
				}}
				style={{ top: `${this.props.item.cummulatedHeight}px` }}
			>
				{this.props.renderCell(this.props.item.data)}
			</div>
		);
	}

	protected assignElement(elem) {
		if (elem !== this.state.refElement) {
			if (this.refElement) {
				this.resizeObserver.unobserve(this.refElement);
			}
			if (elem) {
				this.resizeObserver.observe(elem);
			}
			this.refElement = elem;
		}
	}
}
interface IProps {
	apiService: ApiService;
	endpoint: string;
	endpointDefaultParams?: any;
	endpointExpectedParam: string;
	defaultCellHeight: number;
	totalItems: number;
	scrollingElement: HTMLElement;
	scrollToIndex?: number;
	order?: TOrder;
	renderCell(data: any | null);
	dispatch(action: any);
}

interface IState {
	listData: IListItem[];
	listData2: IListItem[] | null;
	totalHeight: number;
	showRowFrom: number;
	showRowTo: number;
	scrollToIndex: number;
	focussedIndex: number;
	scrollWaitForLoading: boolean;
	loading: boolean;
	previosIndex: number;
}
class VirtualisedList extends React.Component<IProps, IState> {
	protected rowsOffset = 10;
	protected chunkSize = 5;
	protected throttleTimeout: any;
	protected averageCellHeight: number;
	protected scrollTicking = false;
	protected resizeTicking = false;
	protected lastScrollPosition: number = 0;
	protected refElement: React.RefObject<HTMLDivElement> = null;
	protected listDataStatus: EStatus[] = [];

	constructor(props: IProps) {
		super(props);

		const listData = [];
		let cummulatedHeight = 0;
		for (let i = 0; i < props.totalItems; i++) {
			listData.push({
				index: i,
				cummulatedHeight,
				height: this.props.defaultCellHeight,
				data: null,
			});
			this.listDataStatus.push(EStatus.INITIALISED);
			cummulatedHeight += props.defaultCellHeight;
		}

		this.refElement = React.createRef();

		this.state = {
			listData,
			listData2: null,
			totalHeight: cummulatedHeight,
			showRowFrom: 0,
			showRowTo: 0,
			scrollToIndex: -1,
			focussedIndex: -1,
			scrollWaitForLoading: false,
			loading: false,
			previosIndex: 0,
		};

		this.handleScroll = this.handleScroll.bind(this);
		this.handleItemResize = this.handleItemResize.bind(this);
		this.updateListScroll = this.updateListScroll.bind(this);
		this.attachScrollEvent = this.attachScrollEvent.bind(this);
		this.detachScrollEvent = this.detachScrollEvent.bind(this);
		this.handleScrollToIndex = this.handleScrollToIndex.bind(this);
		this.reset = this.reset.bind(this);
	}

	public componentDidMount() {
		if (this.props.scrollingElement) {
			this.attachScrollEvent(this.props.scrollingElement);
		}
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.scrollingElement !== prevProps.scrollingElement) {
			if (prevProps.scrollingElement) {
				this.detachScrollEvent(prevProps.scrollingElement);
			}
			if (this.props.scrollingElement) {
				this.attachScrollEvent(this.props.scrollingElement);
			}
		}
		if (this.props.scrollToIndex !== prevProps.scrollToIndex) {
			if (this.props.scrollToIndex !== this.state.scrollToIndex) {
				this.setState(
					{
						scrollToIndex: this.props.scrollToIndex,
					},
					() => {
						this.handleScrollToIndex(this.state.scrollToIndex);
					},
				);
			}
		}
		if (this.props.totalItems !== prevProps.totalItems) {
			this.updateDataList();
		}
		if (this.props.order !== prevProps.order) {
			this.reset();
		}
	}

	public componentWillUnmount() {
		this.detachScrollEvent(this.props.scrollingElement);
	}

	public render() {
		return (
			<div className="virtualised_list" ref={this.refElement}>
				<div
					className="virtualised_list-content"
					style={{ height: `${this.state.totalHeight}px` }}
				>
					{this.state.listData
						.filter(item => {
							return (
								item.index >= this.state.showRowFrom &&
								item.index <= this.state.showRowTo
							);
						})
						.map(item => {
							return (
								<VirtualisedListItem
									key={item.index}
									focussedIndex={this.state.focussedIndex}
									item={item}
									renderCell={this.props.renderCell}
									handleItemResize={this.handleItemResize}
								/>
							);
						})}
				</div>
			</div>
		);
	}

	protected handleScroll() {
		this.lastScrollPosition = this.props.scrollingElement.scrollTop;

		if (!this.scrollTicking) {
			const self = this;
			window.requestAnimationFrame(() => {
				self.updateListScroll(self.lastScrollPosition);
				self.scrollTicking = false;
			});

			this.scrollTicking = true;
		}
	}

	protected handleItemResize(height: number, index: number) {
		this.setState(
			{
				listData: dotProp.set(
					this.state.listData,
					`${index}.height`,
					height,
				),
			},
			() => {
				this.handleListResize();
				if (this.isRowLoaded(index)) {
					this.listDataStatus[index] = EStatus.SIZED;
				}
			},
		);
	}

	protected handleListResize() {
		if (!this.resizeTicking) {
			const self = this;
			window.requestAnimationFrame(() => {
				const listData = dotProp.get(self.state, 'listData');

				let cummulatedHeight = 0;
				listData.forEach(item => {
					item.cummulatedHeight = cummulatedHeight;
					cummulatedHeight += item.height;
				});

				self.setState(
					{
						listData: listData,
						totalHeight: cummulatedHeight,
					},
					() => {
						self.resizeTicking = false;
						window.setTimeout(() => {
							self.handleScrollToIndex(
								self.state.scrollToIndex,
								true,
							);
						}, 1000);
					},
				);
			});

			this.resizeTicking = true;
		}
	}

	protected updateListScroll(scrollPosition: number) {
		let scrollStart = scrollPosition;
		if (this.refElement.current) {
			scrollStart = scrollPosition - this.refElement.current.offsetTop;
		}
		const height = this.props.scrollingElement.offsetHeight;
		const scrollTo = scrollStart + height;

		// Define from and to which row to show
		let tmpShowRowFrom = 0;
		let tmpShowRowTo = this.state.listData.length - 1;
		if (this.state.listData.length > 1) {
			tmpShowRowFrom = this.binarySearch(
				scrollStart,
				0,
				this.state.listData.length - 1,
			);
			tmpShowRowTo = this.binarySearch(
				scrollTo,
				0,
				this.state.listData.length - 1,
			);
		}

		// Chunk the from and to by multiple of `chunkSize`
		tmpShowRowFrom =
			Math.floor((tmpShowRowFrom - this.rowsOffset) / this.chunkSize) *
			this.chunkSize;
		tmpShowRowTo =
			Math.ceil((tmpShowRowTo + this.rowsOffset) / this.chunkSize) *
			this.chunkSize;

		const showRowFrom = Math.max(tmpShowRowFrom, 0);
		const showRowTo = Math.min(tmpShowRowTo, this.props.totalItems);

		if (
			showRowFrom !== this.state.showRowFrom ||
			showRowTo !== this.state.showRowTo
		) {
			this.setState(
				{
					showRowFrom,
					showRowTo,
				},
				() => {
					this.showRows(this.state.showRowFrom, this.state.showRowTo);
				},
			);
		}
	}

	protected showRows(_startIndex: number, _stopIndex: number) {
		if (this.throttleTimeout) {
			window.clearTimeout(this.throttleTimeout);
		}

		this.throttleTimeout = setTimeout(() => {
			let startLoadIndex: number = this.state.previosIndex;
			let total: number = 10;

			// for (let i = startIndex; i < stopIndex; i++) {
			// 	if (!this.isRowLoaded(i) && !this.isRowLoading(i)) {
			// 		if (startLoadIndex === null) {
			// 			startLoadIndex = i;
			// 		}
			// 		++total;
			// 	}
			// }
			if (
				this.state.loading ||
				this.state.previosIndex > this.props.totalItems
			) {
				return;
			}
			const params = Object.assign(
				{},
				{
					index: startLoadIndex,
					total,
					order: this.props.order,
				},
				this.props.endpointDefaultParams,
			);

			const url = buildUrl(null, {
				path: this.props.endpoint,
				queryParams: params,
			});
			//console.log("url ====> ", url);
			if (startLoadIndex !== null) {
				this.setState({ loading: true });
				for (let i = startLoadIndex; i < startLoadIndex + total; i++) {
					if (this.isRowInitialised(i)) {
						this.listDataStatus[i] = EStatus.LOADING;
					}
				}

				this.props.apiService
					.queryGET(url, this.props.endpointExpectedParam)
					.then((data: any[]) => {


						const listData: IListItem[] = dotProp.get(
							this.state,
							'listData',
						);
						for (
							let i = startLoadIndex;
							i < startLoadIndex + total;
							i++
						) {
							if (
								this.isRowInitialised(i) ||
								this.isRowLoading(i)
							) {
								this.listDataStatus[i] = EStatus.LOADED;
								listData[i].data = data[i - startLoadIndex];
							}
						}
						this.setState({
							loading: false,
							previosIndex: this.state.previosIndex + total,
							listData,
						});
					})
					.catch(error => {
						this.setState({ loading: false });

						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			}
		}, 300);
	}

	protected isRowInitialised(index: number): boolean {
		return !!(this.listDataStatus[index] === EStatus.INITIALISED);
	}

	protected isRowLoaded(index: number): boolean {
		return !!(
			this.listDataStatus[index] &&
			this.listDataStatus[index] === EStatus.LOADED
		);
	}

	protected isRowLoading(index: number): boolean {
		return !!(
			this.listDataStatus[index] &&
			this.listDataStatus[index] === EStatus.LOADING
		);
	}

	protected isRowSized(index: number): boolean {
		return !!(
			this.listDataStatus[index] &&
			this.listDataStatus[index] === EStatus.SIZED
		);
	}

	protected attachScrollEvent(element: HTMLElement) {
		this.scrollTicking = false;
		this.lastScrollPosition = 0;
		element.addEventListener('scroll', this.handleScroll);
		//	this.handleScroll();
	}

	protected detachScrollEvent(element: HTMLElement) {
		this.scrollTicking = false;
		this.lastScrollPosition = 0;
		element.removeEventListener('scroll', this.handleScroll);
	}

	protected binarySearch(
		search: number,
		startIndex: number,
		endIndex: number,
	): number {
		const middle = Math.floor((startIndex + endIndex) / 2);

		if (search === this.state.listData[middle].cummulatedHeight) {
			return middle;
		}

		if (endIndex - 1 === startIndex) {
			if (
				Math.abs(
					this.state.listData[startIndex].cummulatedHeight - search,
				) >
				Math.abs(
					this.state.listData[endIndex].cummulatedHeight - search,
				)
			) {
				return endIndex;
			} else {
				return startIndex;
			}
		}
		if (search > this.state.listData[middle].cummulatedHeight) {
			return this.binarySearch(search, middle, endIndex);
		}

		if (search < this.state.listData[middle].cummulatedHeight) {
			return this.binarySearch(search, startIndex, middle);
		}
	}

	protected handleScrollToIndex(
		index: number,
		force: boolean = false,
		iterations: number = 0,
	) {
		if (index >= 0 && index < this.state.listData.length) {
			this.setState(
				{
					focussedIndex: index,
					scrollToIndex: -1,
				},
				() => {
					// Get the cummulated height of the item
					this.state.listData[index].cummulatedHeight;

					this.props.scrollingElement.scrollTo(
						0,
						this.state.listData[index].cummulatedHeight,
					);

					const indexLoading = this.listDataStatus.findIndex(item => {
						return (
							item === EStatus.LOADING || item === EStatus.LOADED
						);
					});

					if (
						(this.listDataStatus[index] === EStatus.SIZED &&
							indexLoading === -1) ||
						force ||
						iterations >= 20 // max number of iterations before stopping the scroll to index
					) {
						this.setState(
							{
								scrollToIndex: -1,
								scrollWaitForLoading: false,
							},
							() => {
								this.props.scrollingElement.scrollTo(
									0,
									this.state.listData[index].cummulatedHeight,
								);
							},
						);
					} else {
						if (!this.state.scrollWaitForLoading) {
							this.setState({
								scrollWaitForLoading: true,
							});
						}
						window.setTimeout(() => {
							this.props.scrollingElement.scrollTo(
								0,
								this.state.listData[index].cummulatedHeight,
							);

							this.handleScrollToIndex(
								index,
								false,
								iterations + 1,
							);
						}, 300);
					}
				},
			);
		}
	}

	protected updateDataList() {
		if (this.props.totalItems === this.state.listData.length) {
			return;
		}

		const listData: IListItem[] = dotProp.get(this.state, 'listData');
		const listDataStatus: EStatus[] = dotProp.get(this, 'listDataStatus');
		const differenceIndex =
			this.props.totalItems - this.state.listData.length;

		let cummulatedHeight = 0;

		if (differenceIndex > 0) {
			// Add items
			if (this.props.order === 'DESC') {
				for (let i = 0; i < differenceIndex; i++) {
					const item: IListItem = {
						index: i,
						cummulatedHeight,
						height: this.props.defaultCellHeight,
						data: null,
					};

					listData.unshift(item);
					listDataStatus.unshift(EStatus.INITIALISED);
				}
				// Reindex all the items in the list data
				for (let i = 0; i < listData.length; i++) {
					cummulatedHeight += listData[i].height;
					listData[i].index = i;
					listData[i].cummulatedHeight = cummulatedHeight;
				}
			} else {
				cummulatedHeight = this.state.listData.length
					? this.state.listData[this.state.listData.length - 1]
						.cummulatedHeight
					: 0;

				for (
					let i = this.state.listData.length;
					i < this.props.totalItems;
					i++
				) {
					cummulatedHeight += this.props.defaultCellHeight;

					const item: IListItem = {
						index: i,
						cummulatedHeight,
						height: this.props.defaultCellHeight,
						data: null,
					};

					listData.push(item);
					listDataStatus.push(EStatus.INITIALISED);
				}
			}
		} else if (differenceIndex < 0) {
			// Remove items
			listData.splice(this.props.totalItems - 1, differenceIndex);
			listDataStatus.splice(this.props.totalItems - 1, differenceIndex);
		}

		this.listDataStatus = listDataStatus;
		this.setState(
			{
				listData,
				totalHeight:
					listData[listData.length - 1].cummulatedHeight +
					listData[listData.length - 1].height,
				showRowFrom:
					this.props.order === 'DESC' ? 0 : this.state.showRowFrom,
				showRowTo:
					this.props.order === 'DESC' ? 0 : this.state.showRowTo,
			},
			() => {
				this.updateListScroll(
					this.props.order === 'DESC' ? 0 : cummulatedHeight,
				);
			},
		);
	}

	protected reset() {
		if (this.state.listData2 !== null) {
			//if list 2 is not null then swap orignalList with reversed list
			const { listData, listData2 } = this.state;
			const cloneListData = [...listData];
			const cloneListData2 = [...listData2];
			this.setState({
				listData: cloneListData2,
				listData2: cloneListData,
			});
			return;
		} //else reverse list
		const listData = [];
		let cummulatedHeight = 0;
		const orignalListData = [...this.state.listData];
		for (let i = this.props.totalItems - 1; i >= 0; i--) {
			listData.push({
				index: i,
				height: this.state.listData[i].height,
				data: { ...this.state.listData[i].data },
				cummulatedHeight: cummulatedHeight,
			});

			cummulatedHeight += this.state.listData[i].height;
		}

		this.setState(
			{
				listData, //reversed list
				listData2: orignalListData, //orignal list
				totalHeight: cummulatedHeight,
				showRowFrom: 0,
				showRowTo: 0,
				scrollToIndex: -1,
				focussedIndex: -1,
				scrollWaitForLoading: false,
			},
			() => {
				this.updateListScroll(0);
			},
		);
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(withServices(VirtualisedList));
