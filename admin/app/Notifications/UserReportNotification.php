<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Http\Resources\User as UserResource;
use App\Notifications\CanteenNotificationChannel;

class UserReportNotification extends Notification {
	use Queueable;

	public $user;
	public $reportedUser;
	public $author;
	public $reportReason;
	public $reportInformation;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(
		$user,
		$reportedUser,
		$author,
		$reportReason,
		$reportInformation
	) {
		$this->user = $user;
		$this->reportedUser = $reportedUser;
		$this->author = $author;
		$this->reportReason = $reportReason;
		$this->reportInformation = $reportInformation;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		return [
				// 'mail'
			];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "User {$this->author->full_name} reported user {$this->reportedUser->full_name}";

		return (new MailMessage())
			->subject($title)
			->view('emails.user-reported', [
				'user' => $this->user,
				'reportedUser' => $this->reportedUser,
				'author' => $this->author,
				'reportReason' => $this->reportReason,
				'reportInformation' => $this->reportInformation,
			]);
	}
}
