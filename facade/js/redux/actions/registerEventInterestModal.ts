import { IStoreState } from '../store';
import { actionTypes } from '../actions';

export const registerEventInterestModalDisplay = (
	eventSlug: string,
	eventDate: string,
	eventLocation: string,
) => (dispatch, getState: () => IStoreState) => {
	if (
		getState().eventRegisterInterestModal.registerEventInterestModalDisplay
	) {
		return;
	}
	const eventData = {
		slug: eventSlug,
		date: eventDate,
		location: eventLocation,
	};
	return dispatch({
		type: actionTypes.REGISTER_EVENT_INTEREST_MODAL_DISPLAY,
		eventData,
	});
};

export const registerEventInterestModalHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (
		!getState().eventRegisterInterestModal.registerEventInterestModalDisplay
	) {
		return;
	}
	return dispatch({
		type: actionTypes.REGISTER_EVENT_INTEREST_MODAL_HIDE,
	});
};
