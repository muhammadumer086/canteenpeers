<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Situation as SituationResource;

class Topic extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if ($apiStore->hasApiResource('topic', $this->id, $this->updated_at, false)) {
			return $apiStore->getApiResource('topic', $this->id, $this->updated_at, false);
		}

		$params = [
			'id',
			'slug',
			'title',
			'category',
			'is_user_topic',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		// Add the additional relationships
		$result['situations'] = (count($this->situations)) ? SituationResource::collection($this->situations) : [];

		$apiStore->setApiResource($result, 'topic', $this->id, $this->updated_at, false);

		return $result;
	}
}
