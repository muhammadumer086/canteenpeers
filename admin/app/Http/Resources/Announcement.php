<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use App\Http\Resources\AgeRange as AgeRangeResource;
use App\Http\Resources\AnnouncementImage;
use App\Http\Resources\Situation as SituationResource;
use App\Http\Resources\State as StateResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Announcement extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'announcement',
				$this->id,
				$this->updated_at,
				false
			)
		) {
			return $apiStore->getApiResource(
				'announcement',
				$this->id,
				$this->updated_at,
				false
			);
		}

		$params = [
			'id',
			'title',
			'message',
			'url_label',
			'public',
			'created_at',
			'updated_at',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['feature_image'] = $this->image
			? [
				'dimensions' => [
					'width' => $this->image->width,
					'height' => $this->image->height,
				],
				'alt' => null,
				'copyright' => null,
				'url' => $this->image->image_url,
			]
			: null;

		$result['url'] =
			$this->url_href && $this->url_href_as && $this->url_type
				? [
					'href' => $this->url_href,
					'hrefAs' => $this->url_href_as,
					'type' => $this->url_type,
				]
				: null;

		$result['situations'] = $this->situations()->count()
			? SituationResource::collection($this->situations)
			: [];

		$result['states'] = $this->states()->count()
			? StateResource::collection($this->states)
			: [];

		$result['age_ranges'] = $this->ageRanges()->count()
			? AgeRangeResource::collection($this->ageRanges)
			: [];

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);

		$apiStore->setApiResource(
			$result,
			'announcement',
			$this->id,
			$this->updated_at,
			false
		);

		return $result;
	}
}
