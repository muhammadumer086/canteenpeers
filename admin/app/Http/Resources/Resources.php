<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Topic as TopicResource;
use App\Http\Resources\Situation as SituationResource;

class Resources extends JsonResource {
	use \App\Traits\AuthUser;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'resource',
				$this->id,
				$this->updated_at,
				true,
				true
			)
		) {
			return $apiStore->getApiResource(
				'resource',
				$this->id,
				$this->updated_at,
				true,
				true
			);
		}

		$params = [
			'id',
			'title',
			'slug',
			'content',
			'feature_image',
			'is_ycs',
			'saved',
			'first_publication_date',
			'last_publication_date',
			'created_at',
			'updated_at',
			'deleted_at',
			'seo',
		];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		// Add the additional relationships
		$result['topic'] = $this->topic()->exists()
			? new TopicResource($this->topic)
			: null;
		$result['situations'] = $this->situations()->count()
			? SituationResource::collection($this->situations)
			: [];
		$result['user'] = new UserResource($this->user);

		// $authUser = $this->getAuthUser($request);
		// $result['saved'] = DB::table('saved_resources')
		// ->where('user_id', $authUser->id)
		// ->where('resource_id', $id)
		// ->first();

		$authUser = $this->getAuthUser($request);
		if ($authUser && $authUser->hasRoleName('admin')) {
			$adminParams = ['deleted_at', 'views_count'];
			// Add the admin values
			foreach ($adminParams as $key) {
				$result[$key] = $this->$key;
			}
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);
		$result['first_publication_date'] = UserPermissionHelper::formatCarbonDate($result['first_publication_date']);
		$result['last_publication_date'] = UserPermissionHelper::formatCarbonDate($result['last_publication_date']);
		$result['deleted_at'] = UserPermissionHelper::formatCarbonDate($result['deleted_at']);

		$apiStore->setApiResource(
			$result,
			'resource',
			$this->id,
			$this->updated_at,
			true,
			true
		);

		return $result;
	}
}
