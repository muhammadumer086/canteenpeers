@extends('layouts.email')

@section('plain_text')
	@component('emails.components.paragraph')
		@if ($user->first_name)
			Hi {{$user->first_name}},
		@else
			Hi,
		@endif
	@endcomponent
	@component('emails.components.paragraph')
		You have {{$count}} {{$messages}} from {{$usernames}} waiting for you.
	@endcomponent
@endsection

@section('content')
	@component('emails.components.paragraph')
		@if ($user->first_name)
			Hi {{$user->first_name}},
		@else
			Hi,
		@endif
	@endcomponent

	@component('emails.components.paragraph')
		You have {{$count}} {{$messages}} from {{$usernames}} waiting for you.
	@endcomponent

	@php
		$link = config('canteen.facade_url') . '/messages';
	@endphp
	@component('emails.components.button', ['url' => $link])
		View {{$messages}}
	@endcomponent

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
