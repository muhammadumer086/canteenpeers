import { actionTypes } from '../actions';
import { IApiDiscussion } from 'js/interfaces';

export interface IStoreState {
	discussionModalDisplay: boolean;
	discussionData?: IApiDiscussion;
	predefinedTopic: string | null;
	predefinedSituations: string[] | null;
}

export const discussionModal = (
	state: IStoreState = {
		discussionModalDisplay: false,
		predefinedTopic: null,
		predefinedSituations: null,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.DISCUSSION_MODAL_DISPLAY:
			return Object.assign({}, state, {
				discussionModalDisplay: true,
				discussionData: action.discussionData,
				predefinedTopic: action.predefinedTopic,
				predefinedSituations: action.predefinedSituations,
			});
		case actionTypes.USER_LOGOUT: // hide modal on logout
		case actionTypes.DISCUSSION_MODAL_HIDE:
			return Object.assign({}, state, {
				discussionModalDisplay: false,
			});
		default:
			return state;
	}
};
