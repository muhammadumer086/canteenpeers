const inLambda = !!process.env.LAMBDA_TASK_ROOT;
const config = !inLambda && require('dotenv').config();

let assetsPrefix = null;
if (!inLambda && config && config.hasOwnProperty('parsed') && config.parsed) {
	for (const key in config.parsed) {
		if (config.parsed.hasOwnProperty(key)) {
			process.env[key] = config.parsed[key];

			if (key === 'STATIC_PATH') {
				assetsPrefix = config.parsed[key];
			}
		}
	}
}

module.exports.deepLinkApple = function(event, context, callback) {
	const response = {
		statusCode: 200,
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			applinks: {
				apps: [],
				details: [
					{
						appID: '5QG7V8R42B.com.canteenConnect.testflight',
						paths: ['*'],
					},
				],
			},
		}),
	};

	callback(null, response);
};

module.exports.deepLinkAndroid = function(event, context, callback) {
	const response = {
		statusCode: 200,
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify([
			{
				relation: ['delegate_permission/common.handle_all_urls'],
				target: {
					namespace: 'android_app',
					package_name: 'com.example.canteenconnect',
					sha256_cert_fingerprints: [
						'C3:C5:67:00:D2:1A:17:F6:2C:12:C8:B7:CA:96:F3:D2:5F:A2:89:72:D1:92:4D:3B:48:84:8C:03:0C:18:10:06',
					],
				},
			},
		]),
	};

	callback(null, response);
};

module.exports.robotsTxt = function(event, context, callback) {
	const noIndex = 'NO_INDEX' in process.env && process.env.NO_INDEX === 'yes';
	const robots = ['User-agent: *'];
	if (noIndex) {
		robots.push('Disallow: /');
	} else {
		robots.push('Disallow: /admin');
	}

	const response = {
		statusCode: 200,
		headers: {
			'Content-Type': 'text/plain',
		},
		body: robots.join('\r\n'),
	};

	callback(null, response);
};

module.exports.webManifest = function(event, context, callback) {
	const response = {
		statusCode: 200,
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			short_name: 'Canteen Connect',
			name: 'Canteen Connect',
			prefer_related_applications: true,
			scope: '/',
			display: 'minimal-ui',
			start_url: '/dashboard',
			icons: [
				{
					src: `https://s3-ap-southeast-2.amazonaws.com/assets.canteenconnect.org.au/static-stg/images/app-icons/CC-icon-192.png`,
					type: 'image/png',
					sizes: '192x192',
				},
				{
					src: `https://s3-ap-southeast-2.amazonaws.com/assets.canteenconnect.org.au/static-stg/images/app-icons/CC-icon-512.png`,
					type: 'image/png',
					sizes: '512x512',
				},
			],
			related_applications: [
				{
					platform: 'play',
					ur:
						'https://play.google.com/store/apps/details?id=canteen.connect.android.app',
					id: 'canteen.connect.android.app',
				},
			],
		}),
	};

	callback(null, response);
};
