import React from 'react';
import { connect } from 'react-redux';
import Link from 'next/link';

import { IStoreState } from '../../redux/store';
import { IApiUser } from '../../interfaces';

interface IProps {
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	dispatch(action: any): void;
}

interface IState {}

class HomeHeroPublic extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
		};
	}

	public render() {
		return (
			<div className="home_hero_public theme--accent">
				<div className="home_hero_public-content">
					<h2 className="font--h2 hm-b8">
						Canteen's community for parents whose family life is
						impacted by cancer
					</h2>
					<p className="font--h4 hm-b32">
						<Link
							href="/auth/register?step=step-1"
							as="/auth/register/step-1"
						>
							<a>Register now</a>
						</Link>{' '}
						to connect with other parents, receive free professional
						counselling and join the community discussion.
					</p>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(HomeHeroPublic);
