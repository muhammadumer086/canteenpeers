<?php

namespace App\Http\Resources;

use App\Helpers\UserPermissionHelper;
use App\Http\Resources\Situation as SituationResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class Event extends JsonResource {
	use \App\Traits\AuthUser;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();

		if (
			$apiStore->hasApiResource(
				'event',
				$this->id,
				$this->updated_at,
				false
			)
		) {
			return $apiStore->getApiResource(
				'event',
				$this->id,
				$this->updated_at,
				false
			);
		}

		$params = [
			'id',
			'title',
			'slug',
			'description',
			'latitude',
			'longitude',
			'location',
			'state',
			'email_address',
			'created_at',
			'updated_at',
			'start_date',
			'end_date',
			'overnight',
			'weekend',
			'daytime',
			'evening',
			'sos_url',
			'event_type',
			'discussion_id',
			'last_joining_date',
			'event_time_zone',
			'min_age',
			'max_age',
			'max_participents',
			'promo_video_url',
			'is_online',
			'show_rsvp',
			'is_travel_cover',
			'admins',
			'discussion',
			'event_admin'
		];

		$result = [];

		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$authUser = $this->getAuthUser($request);
		$result['location_data'] = json_decode($this->location_data);
		$result['registered'] = $this->hasUser($authUser) ? true : false;
		$result['situations'] = $this->situations()->count()
			? SituationResource::collection($this->situations)
			: [];
		//dd($this->image);

		if(isset($this->image) && !empty($this->image))
		{
			$featuredimage = '';
			$nonFeaturedImages = array();
			foreach ($this->image as $img)
			{
				if(!$img->is_featured){
					$featuredimage =$img;
				}
				else{
					$imgDimentions = [
						'dimensions' => [
							'width' => $img->width,
							'height' =>$img->height,
						],
						'alt' => null,
						'copyright' => null,
						'url' => $img->image_url,
						'title'=>$img->title,
					];
					$nonFeaturedImages[] = $imgDimentions;
				}
			}
			if(!empty($nonFeaturedImages))
			{
				$result['user_media'] = $nonFeaturedImages;
			}
			else{
				$result['user_media'] = [];
			}
			if(isset($featuredimage) && isset($featuredimage->width))
			{
				$result['feature_image'] = [
					'dimensions' => [
						'width' => $featuredimage->width,
						'height' =>$featuredimage->height,
					],
					'alt' => null,
					'copyright' => null,
					'url' => $featuredimage->image_url,
				];

			}else {
				$result['feature_image'] = null;
			}
		}else {
			$result['feature_image'] = null;
		}



		$result['registered_users'] = new UserCollection($this->users);;

		if ($authUser) {
			$result['registered_users_count'] = $this->users()->count();

			$result[
				'registered_users_count_sos'
			] = $this->getRegisteredUsersCountByOrigin('side-of-stage');
		}

		$result['created_at'] = UserPermissionHelper::formatCarbonDate($result['created_at']);
		$result['updated_at'] = UserPermissionHelper::formatCarbonDate($result['updated_at']);
		$result['start_date'] = UserPermissionHelper::formatCarbonDate($result['start_date']);
		$result['end_date'] = UserPermissionHelper::formatCarbonDate($result['end_date']);
		$result['last_joining_date'] = UserPermissionHelper::formatCarbonDate($result['last_joining_date']);

		$apiStore->setApiResource(
			$result,
			'event',
			$this->id,
			$this->updated_at,
			false
		);

		return $result;
	}
}
