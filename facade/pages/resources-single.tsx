import React from 'react';
import Head from 'next/head';

import buildUrl from 'build-url';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';

import { IStoreState } from '../js/redux/store';
import { IApiResource } from '../js/interfaces';
import { checkPage404 } from '../js/helpers/checkPage404';

import { ApiService, AuthService } from '../js/services';

import LayoutDashboard from '../js/components/general/LayoutDashboard';
import WYSIWYG from '../js/components/general/WYSIWYG';
import FixedBackBar from '../js/components/general/FixedBackBar';
import TileAuthor from '../js/components/general/TileAuthor';
import ResourceTile from '../js/components/resources/ResourceTile';
import SubTitleDetail from '../js/components/general/SubTitleDetail';
import ArticleActions from '../js/components/general/ArticleActions';
import Footer from '../js/components/general/Footer';

import { GTMDataLayer } from '../js/helpers/dataLayer';
import { ButtonBaseLink } from '../js/components/general/ButtonBaseLink';
import Link from 'next/link';

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	resource: IApiResource;
	related: IApiResource[];
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
}

class ResourceSingle extends React.Component<IProps, IState> {
	static async getInitialProps({ req, query, apiService }) {
		const result = await checkPage404(
			req,
			query,
			'id',
			async (slug: string) => {
				const data = await (apiService as ApiService).queryGET(
					`/api/resources/${slug}`,
				);
				const resource = 'resource' in data ? data.resource : null;
				const related = 'related' in data ? data.related : [];

				return {
					resource,
					related,
				};
			},
		);

		if (!result) {
			return {
				resource: null,
				related: null,
			};
		}

		return result;
	}

	protected GTM = new GTMDataLayer();

	constructor(props) {
		super(props);

		this.state = {
			isUpdating: false,
		};

		this.trackRelatedContent = this.trackRelatedContent.bind(this);
	}

	public render() {
		// let title = `Resource`;
		// if (this.props.resource) {
		// 	title = this.props.resource.title;
		// }

		let hostname;
		if (typeof window !== 'undefined') {
			hostname = window.location.origin;
		}

		const url = buildUrl(hostname, {
			path: `/resources/${this.props.resource.slug}`,
		});

		// const removeHTMLandTruncate = (
		// 	content: string,
		// 	stringLength: number,
		// ) => {
		// 	const replaceTags = content.replace(/<(?:.|\n)*?>/gm, '');
		// 	const limitContent = replaceTags.substring(0, stringLength);

		// 	return limitContent;
		// };

		// const seoTitle = !!this.props.resource.seo.title
		// 	? this.props.resource.seo.title
		// 	: this.props.resource.title;

		// const seoDescription = !!this.props.resource.seo.description
		// 	? this.props.resource.seo.description
		// 	: removeHTMLandTruncate(this.props.resource.content, 300);

		const seoFeaturedImage = !!this.props.resource.seo.image
			? this.props.resource.seo.image
			: `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;
		const sharingUrl = url;
		const resourceTileAuthor = {
			authorAvatarUrl: `${process.env.STATIC_PATH}/icons/canteen-icon.svg`,
			authorUsername: 'Canteen',
			timeAgo: this.props.resource.first_publication_date.date,
			timezone: 'Australia/Sydney',
		};

		return (
			<div className="resources_single">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:url"
						property="og:url"
						content={sharingUrl}
					/>
					<meta name="og:type" property="og:type" content="article" />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/>

					<meta name="twitter:card" content="summary" />
					<meta name="twitter:site" content="Canteen Connect" />
					<meta
						name="twitter:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="twitter:description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta name="twitter:image" content={seoFeaturedImage} />
				</Head>
				<LayoutDashboard>
					{/* The fixed bar */}
					<FixedBackBar href="/resources" label="All Resources" />

					<div className="theme--main">
						<div className="resources_single-content_container">
							<div className="resources_single-wyiwysg">
								<WYSIWYG>
									<div className="resources_single-topic">
										{this.props.resource.topic && (
											<ButtonBaseLink
												className="general_tile-topic_link"
												href={`/blogs?topic=${this.props.resource.topic.slug}`}
												title={
													this.props.resource.topic
														.slug
												}
												disableRipple={true}
											>
												{
													this.props.resource.topic
														.title
												}
											</ButtonBaseLink>
										)}
									</div>

									<div className="article_single-title hm-v32">
										<h1 className="article_single-wyiwysg_title theme-title">
											{this.props.resource.title}
										</h1>
									</div>

									<div className="article_single-meta">
										<div className="article_single-author general_tile-author">
											<TileAuthor
												resourceData={
													resourceTileAuthor
												}
											/>
										</div>

										{this.props.resource.is_ycs && (
											<div className="article_single-ycs">
												<img
													src={`${process.env.STATIC_PATH}/images/ycs-logo.png`}
													alt="Canteen Connect | YCS"
												/>
											</div>
										)}
									</div>

									<div
										dangerouslySetInnerHTML={{
											__html: this.props.resource.content,
										}}
									/>

									<div className="article_single-external_resource">
										<Link href="/resources">
											<a>
												<span>
													For a full list of Canteen
													resources click here
												</span>
											</a>
										</Link>
									</div>
								</WYSIWYG>
							</div>

							<ArticleActions
								sharingUrl={sharingUrl}
								withSaveAction={true}
								articleTitle={this.props.resource.title}
								articleId={this.props.resource.id}
								articleSaved={this.props.resource.saved}
								resourceType="resources"
								resourceSlug={this.props.resource.slug}
							/>
						</div>
					</div>

					{this.props.related &&
						this.props.related.length > 0 &&
						this.renderRelatedTiles()}

					<Footer />
				</LayoutDashboard>
			</div>
		);
	}

	protected trackRelatedContent(title: string) {
		this.GTM.pushEventToDataLayer({
			event: 'nextContent',
			title,
		});
	}

	protected renderRelatedTiles() {
		const subTitleConfig = {
			icon: 'resources',
			title: 'Other resources you may be interested in',
			linkLabel: 'More Popular Resources',
			linkData: {
				prefix: '/resources',
				href: '/resources',
				as: '/resources',
			},
		};

		return (
			<div className="resources_single-related">
				<div className="resources_single-related_container">
					<SubTitleDetail {...subTitleConfig} />
					{this.props.related.map((relatedTile, index) => {
						return (
							<ResourceTile
								key={index}
								disabled={false}
								resourceLinkPrexfix={
									subTitleConfig.linkData.prefix
								}
								resourceData={relatedTile}
								gtm={this.trackRelatedContent}
							/>
						);
					})}
				</div>
			</div>
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated } = state.user;
	return {
		userAuthenticated,
	};
}

export default connect(mapStateToProps)(withRouter(ResourceSingle));
