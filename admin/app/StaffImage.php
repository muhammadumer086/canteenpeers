<?php

namespace App;

use App\Staff;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class StaffImage extends Model {
	use \BinaryCabin\LaravelUUID\Traits\HasUUID;

	protected $uuidFieldName = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $fillable = ['id'];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['image_url'];

	/**
	 * The users that created this image
	 */
	public function user() {
		return $this->belongsTo(User::class);
	}

	/**
	 * The staff member this image is attached to
	 */
	public function staff() {
		return $this->belongsTo(Staff::class);
	}

	public function getImageUrlAttribute() {
		return Storage::disk('s3')->url($this->path);
	}
}
