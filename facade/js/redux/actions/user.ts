import { IStoreState } from '../store';
import { actionTypes } from '../actions';

import { ApiService } from '../../services';

import { apiError } from './apiError';

import { IApiUser, IApiToken } from '../../interfaces';
import { GTMDataLayer } from '../../helpers/dataLayer';

const GTM = new GTMDataLayer();

export const userRegistered = (
	userData: IApiUser | null,
	pushToDataLayer: boolean = true,
) => (dispatch, _getState: () => IStoreState) => {
	if (pushToDataLayer) {
		GTM.pushEventToDataLayer({
			event: 'createAccount',
			situation: userData.situations,
		});
	}

	return dispatch({
		type: actionTypes.USER_REGISTERED,
		userData: userData,
	});
};

export const userVerified = () => (dispatch, _getState: () => IStoreState) => {
	return dispatch({
		type: actionTypes.USER_VERIFIED,
	});
};

export const userVerifiedDisplayed = () => (
	dispatch,
	_getState: () => IStoreState,
) => {
	return dispatch({
		type: actionTypes.USER_VERIFIED_DISPLAYED,
	});
};

export const userAuthenticating = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (getState().user.userAuthenticated === false) {
		return dispatch({
			type: actionTypes.USER_AUTHENTICATING,
		});
	}
};

export const userIsAuthenticated = (
	userToken: IApiToken,
	userData: IApiUser,
	userAdmin: boolean | null,
) => (dispatch, getState: () => IStoreState) => {
	if (
		getState().user.userAuthenticated !== true ||
		JSON.stringify(userData) !== JSON.stringify(getState().user.userData)
	) {
		GTM.pushEventToDataLayer({
			event: 'userLogin',
			login: 'Yes',
			userId: userData.id,
		});
		dispatch({
			type: actionTypes.USER_AUTHENTICATED,
			userToken: userToken,
			userData: userData,
			userAdmin: userAdmin,
		});
	}
};

export const userLogout = () => (dispatch, getState: () => IStoreState) => {
	if (getState().user.userAuthenticated) {
		return dispatch({
			type: actionTypes.USER_LOGOUT,
		});
	}
};

export const userUpdated = (userData: IApiUser) => (
	dispatch,
	_getState: () => IStoreState,
) => {
	return dispatch({
		type: actionTypes.USER_UPDATED,
		userData: userData,
	});
};

export const updateRegisterUserData = userData => (
	dispatch,
	_getState: () => IStoreState,
) => {
	return dispatch({
		type: actionTypes.USER_REGISTER_UPDATE_DATA,
		userData,
	});
};

export const userAvatarUpload = (
	username: string,
	apiService: ApiService,
	file: File,
) => (dispatch, getState: () => IStoreState) => {
	if (!getState().user.userAvatarUploading) {
		dispatch({
			type: actionTypes.USER_AVATAR_UPLOADING,
		});

		const params = new FormData();
		params.append('avatar', file, file.name);

		apiService
			.queryPOSTFile(`/api/users/${username}/avatar`, params, 'user')
			.then((user: IApiUser) => {
				dispatch({
					type: actionTypes.USER_AVATAR_UPLOADED,
					userPanelData: user,
				});
			})
			.catch(error => {
				dispatch({
					type: actionTypes.USER_AVATAR_UPLOAD_ERROR,
				});
				dispatch(apiError([error.message]));
			});
	}
};
