import React from 'react';
import { connect } from 'react-redux';

import ButtonBase from '@material-ui/core/ButtonBase';
import Checkbox from '@material-ui/core/Checkbox';

import { IStoreState, apiError } from '../../../redux/store';

import { IApiSituation, IApiTopic } from '../../../interfaces';

import { ApiService } from '../../../services';
const rolesArr = [
	'Admin',
	'COSS Manager',
	'Online Community',
	'COSS',
	'Programs staff',
	'PSW',
	'RESP',
	'Marcomms',
	'Guest',
	'Canteen Staff not covered elsewhere',
];
const featureAndPermission = [
	{ feature: 'Users Management', permissions: 'Add/Edit' },
	{ feature: 'Users Management', permissions: 'View Only' },
	{ feature: 'Users Management', permissions: 'Delete' },
	{ feature: 'Users Management', permissions: 'All Rights' },
	{ feature: 'All Users Activities', permissions: 'Add/Edit' },
	{ feature: 'All Users Activities', permissions: 'View Only' },
	{ feature: 'All Users Activities', permissions: 'Delete' },
	{ feature: 'All Users Activities', permissions: 'All Rights' },
	{ feature: 'Topic Management', permissions: 'Add/Edit' },
	{ feature: 'Topic Management', permissions: 'View Only' },
	{ feature: 'Topic Management', permissions: 'Delete' },
	{ feature: 'Topic Management', permissions: 'All Rights' },
	{ feature: 'Reported Discussions', permissions: 'Enable/Disable' },
	{ feature: 'Reported Discussions', permissions: 'All Rights' },
	{ feature: 'Events Management', permissions: 'Add/Edit' },
	{ feature: 'Events Management', permissions: 'View Only' },
	{ feature: 'Events Management', permissions: 'Delete' },
	{ feature: 'Events Management', permissions: 'All Rights' },
	{ feature: 'Alert Management', permissions: 'Add/Edit' },
	{ feature: 'Alert Management', permissions: 'View Only' },
	{ feature: 'Alert Management', permissions: 'Delete' },
	{ feature: 'Alert Management', permissions: 'All Rights' },
	{ feature: 'Staff Management', permissions: 'Add/Edit' },
	{ feature: 'Staff Management', permissions: 'View Only' },
	{ feature: 'Staff Management', permissions: 'Delete' },
	{ feature: 'Staff Management', permissions: 'All Rights' },
	{ feature: 'Graduate Users Management', permissions: 'Enable/Disable' },
	{ feature: 'Graduate Users Management', permissions: 'All Rights' },
	{ feature: 'Role Management', permissions: 'Add/Edit' },
	{ feature: 'Role Management', permissions: 'View Only' },
	{ feature: 'Role Management', permissions: 'Delete' },
	{ feature: 'Role Management', permissions: 'All Rights' },

];
interface IProps {
	title: string;
	type: string;
	situations: IApiSituation[];
	topics: IApiTopic[];
	apiService: ApiService;
	handleTopicsUpdate(topics: IApiTopic[]): void;
	dispatch(action: any): void;
}

interface IState {
	loading: boolean;
	changed: boolean;
	topics: IApiTopic[];
}

class RolesGrid extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			changed: false,
			topics: JSON.parse(JSON.stringify(props.topics)),
		};

		this.handleChange = this.handleChange.bind(this);
		this.saveTopics = this.saveTopics.bind(this);
		this.cancelChanges = this.cancelChanges.bind(this);
	}

	public componentDidUpdate(prevProps: IProps) {
		if (this.props.topics !== prevProps.topics) {
			this.setState({
				topics: JSON.parse(JSON.stringify(this.props.topics)),
			});
		}
	}

	public render() {
		return (<table className="admin_topics_grid-content-table" style={{position : "relative",overflow:'scroll',marginTop:"10px"}}>
							<thead >
								<tr style={{position : "relative"}}>
									<th style={{position : 'sticky',top : '0',backgroundColor:"white",zIndex:99}}></th>
									<th style={{position : 'sticky',top : '0',backgroundColor:"white",zIndex:99}}></th>
									{rolesArr.map((situation, index) => {
										if (situation === '') {
											return null;
										}
										return <th style={{position : 'sticky',top : '0',backgroundColor:"white",zIndex:99}} key={index}>{situation}</th>;
									})}
								</tr>
							</thead>
							<tbody>
								{this.state.topics.slice(0,featureAndPermission.length).map((topic, topicIndex) => {
									const situationIds = topic.situations.map(
										situation => {
											return situation.id;
										},
									);

									return (
										<tr key={topicIndex}>
											<th>
												{featureAndPermission[
													topicIndex
												] &&
													featureAndPermission[
														topicIndex
													].feature}
											</th>
											<th>
												{featureAndPermission[
													topicIndex
												] &&
													featureAndPermission[
														topicIndex
													].permissions}
											</th>
											{this.props.situations.map(
												(situation, index) => {
													if (situation.cancer_type) {
														return null;
													}
													const isActive =
														situationIds.indexOf(
															situation.id,
														) >= 0;
													const classNames = [
														'admin_topics_grid-content-table-cell',
													];
													if (isActive) {
														classNames.push(
															'admin_topics_grid-content-table-cell--active',
														);
													}

													return (
														<td
															key={index}
															className={classNames.join(
																' ',
															)}
														>
															<Checkbox
																checked={
																	isActive
																}
																onChange={() => {
																	this.handleChange(
																		topicIndex,
																		situation,
																	);
																}}
																disabled={
																	this.state
																		.loading
																}
															/>
															<ButtonBase
																focusRipple
																className="admin_topics_grid-content-table-cell-button"
																onClick={() => {
																	this.handleChange(
																		topicIndex,
																		situation,
																	);
																}}
																tabIndex={-1}
																disabled={
																	this.state
																		.loading
																}
															></ButtonBase>
														</td>
													);
												},
											)}
											<td className="admin_topics_grid-content-table-cell">
												<Checkbox
													checked={false}
													onChange={() => {}}
													disabled={
														this.state.loading
													}
												/>
												<ButtonBase
													focusRipple
													className="admin_topics_grid-content-table-cell-button"
													onClick={() => {}}
													tabIndex={-1}
													disabled={
														this.state.loading
													}
												></ButtonBase>
											</td>
											<td className="admin_topics_grid-content-table-cell admin_topics_grid-content-table-cell--active">
												<Checkbox
													checked={true}
													onChange={() => {}}
													disabled={
														this.state.loading
													}
												/>
												<ButtonBase
													focusRipple
													className="admin_topics_grid-content-table-cell-button"
													onClick={() => {}}
													tabIndex={-1}
													disabled={
														this.state.loading
													}
												></ButtonBase>
											</td>
										</tr>
									);
								})}
							</tbody>
						</table>
						
			// 		{this.state.changed && (
			// 			<div className="hm-t32">
			// 				<Button
			// 					variant="contained"
			// 					color="primary"
			// 					onClick={this.saveTopics}
			// 					disabled={this.state.loading}
			// 				>
			// 					Save Changes
			// 				</Button>
			// 				<span className="hm-l16">
			// 					<Button
			// 						onClick={this.cancelChanges}
			// 						disabled={this.state.loading}
			// 					>
			// 						Cancel
			// 					</Button>
			// 				</span>
			// 			</div>
			// 		)}
				
			// </section>
		);
	}

	protected handleChange(topicIndex, situation: IApiSituation) {
		// Duplicate the topics
		const topics: IApiTopic[] = JSON.parse(
			JSON.stringify(this.state.topics),
		);

		const situationIds = topics[topicIndex].situations.map(situation => {
			return situation.id;
		});

		const situationIndex = situationIds.indexOf(situation.id);

		if (situationIndex >= 0) {
			topics[topicIndex].situations.splice(situationIndex, 1);
		} else {
			topics[topicIndex].situations.push(
				JSON.parse(JSON.stringify(situation)),
			);
		}

		this.setState({
			topics,
			changed: true,
		});
	}

	protected saveTopics() {
		this.setState(
			{
				loading: true,
			},
			() => {
				// Process the data
				const data = this.state.topics.map(topic => {
					return {
						id: topic.id,
						situationIds: topic.situations.map(situation => {
							return situation.id;
						}),
					};
				});

				this.props.apiService
					.queryPUT(
						'/api/topics',
						{
							topics: data,
						},
						'topics',
					)
					.then((topics: IApiTopic[]) => {
						this.setState({
							loading: false,
							changed: false,
						});
						this.props.handleTopicsUpdate(topics);
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							loading: false,
						});
					});
			},
		);
	}

	protected cancelChanges() {
		this.setState({
			topics: JSON.parse(JSON.stringify(this.props.topics)),
			changed: false,
		});
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(RolesGrid);
