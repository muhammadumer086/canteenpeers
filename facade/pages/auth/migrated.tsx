import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router, { withRouter } from 'next/router';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';

import { apiError, userVerified, IStoreState } from '../../js/redux/store';

import MyError from '../_error';
import { ApiService, AuthService } from '../../js/services';
import { PasswordResetForm } from '../../js/components/auth/PasswordResetForm';

interface IProps {
	query: any;
	apiService: ApiService;
	authService: AuthService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
}

class Verify extends React.Component<IProps, IState> {
	static getInitialProps({ query }) {
		return {
			query,
		};
	}

	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
		};

		this.onSubmit = this.onSubmit.bind(this);
	}

	public render() {
		if (!this.props.query || !('token' in this.props.query)) {
			return <MyError statusCode={401} />;
		}

		let email = '';
		if (this.props.query && 'email' in this.props.query) {
			email = this.props.query.email;
		}

		return (
			<div className="auth_wrapper theme--accent_dark">
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<section className="auth">
					<header className="auth-header">
						<h1 className="auth-title font--h4">
							Welcome to {process.env.APP_NAME}
						</h1>
					</header>
					<div className="auth-content theme--main">
						{this.state.submitting && (
							<LinearProgress
								color="secondary"
								className="auth-content-progress"
							/>
						)}
						<PasswordResetForm
							intro="Please create your new password"
							submitLabel="Create Password"
							onSubmit={this.onSubmit}
							submitting={this.state.submitting}
							email={email}
						/>
					</div>
					<footer className="auth-footer font--small">
						Back to{' '}
						<Link href="/">
							<a>{process.env.APP_NAME}</a>
						</Link>
					</footer>
				</section>
			</div>
		);
	}

	protected onSubmit(values) {
		const submissionValues = {
			email: values.email,
			password: values.password,
			token: this.props.query.token,
		};

		this.setState(
			{
				submitting: true,
			},
			() => {
				this.props.apiService
					.queryPOST('/api/users/activate-migrated', submissionValues)
					.then(() => {
						// Success
						this.login(values);
					})
					.catch(error => {
						this.setState({
							submitting: false,
						});
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
					});
			},
		);
	}

	protected login(values) {
		this.props.apiService
			.queryPOST('/api/users/authenticate', values)
			.then((data: any) => {
				if (
					'token_type' in data &&
					'expires_in' in data &&
					'access_token' in data &&
					'refresh_token' in data
				) {
					// Success
					this.props.authService.logIn(data).then(result => {
						if (document.location.search.includes('?redirect=')) {
							Router.push(
								document.location.search.split('?redirect=')[1],
							);
						} else if (result) {
							this.props.dispatch(userVerified());
							Router.push('/dashboard');
						} else {
							this.setState({
								submitting: false,
							});
						}
					});
				} else {
					this.setState({
						submitting: false,
					});
					throw new Error('Invalid data received');
				}
			})
			.catch(error => {
				this.setState({
					submitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([error.message]));
			});
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(withRouter(Verify));
