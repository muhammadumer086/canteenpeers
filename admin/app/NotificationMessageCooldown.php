<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Filters\Activity\UserActivityFilters;

class NotificationMessageCooldown extends Model {
	use \BinaryCabin\LaravelUUID\Traits\HasUUID;

	protected $uuidFieldName = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'notification_message_cooldowns';

	protected $fillable = ['id'];

	/**
	 * The users that belong to this activity
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

	public function getActivityModelNameAttribute() {
		$modelSplit = explode('\\', $this->activity_type);
		if (sizeof($modelSplit)) {
			return strtolower($modelSplit[sizeof($modelSplit) - 1]);
		}
		return null;
	}
}
