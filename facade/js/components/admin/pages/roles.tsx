import React from 'react';
import { connect } from 'react-redux';
import Head from 'next/head';

import { apiError, apiSuccess, IStoreState } from '../../../redux/store';

import { IApiSituation, IApiTopic, IApiRole } from '../../../interfaces';

import LayoutDashboard from '../../../components/general/LayoutDashboard';
import ListLoading from '../../../components/general/ListLoading';
import AdminFixedBar from '../../../components/admin/AdminFixedBar';
// import RolesGrid from '../roles/rolesGrid';
import RoleOptions from '../roles/RoleOption';
import HeroHeading from '../../../components/general/HeroHeading';
import {
	IAdminProps,
	IAdminState,
} from '../../../components/admin/withAdminPage';
import Button from '@material-ui/core/Button';
import CreateRoleModal from "../roles/CreateRoleModal";
import EditRoleModal from "../roles/EditRoleModal";
import ConfirmDelete from "../roles/ConfrimDelete";
//import ManagePermissionModal from "../roles/ManagePermissionModal";
import PermissionGrid from "../roles/PermissionGrid";
// import { generalPermissions } from "../roles/PermissionGrid";
//import { checkPermission } from "../../../helpers/checkPermission";

import Paper from "@material-ui/core/Paper";
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';



interface IProps extends IAdminProps {
	situations: IApiSituation[];
	topics: IApiTopic[];
}

interface IState extends IAdminState {
	value: number;
	situations: IApiSituation[];
	topics: IApiTopic[];
	showAddModal: boolean;
	showEditModal: boolean;
	showPermissionModal: boolean;
	permissionRole: IApiRole | undefined;
	permissions: string[];
	isLoadingPermission: boolean;
	roles: IApiRole[];
	editRole: undefined | IApiRole;
	showConfirm: boolean;
	roleToDelete: undefined | IApiRole;
}

class Topics extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			value: 0,
			topics: [],
			situations: [],
			showAddModal: false,
			showEditModal: false,
			showPermissionModal: false,
			permissionRole: undefined,
			isLoadingPermission: false,
			permissions: [],
			roles: [],
			editRole: undefined,
			showConfirm: false,
			roleToDelete: undefined,
		};

		this.handleTopicsUpdate = this.handleTopicsUpdate.bind(this);
		this.toggleAddModal = this.toggleAddModal.bind(this);
		this.loadRoles = this.loadRoles.bind(this);
		this.addRole = this.addRole.bind(this);
		this.editRole = this.editRole.bind(this);
		this.handleTabChange = this.handleTabChange.bind(this);
		this.toggleMangePermissionModal = this.toggleMangePermissionModal.bind(this);
		this.deleteRole = this.deleteRole.bind(this);
		this.toggleConfrim = this.toggleConfrim.bind(this);

	}

	public componentDidMount() {
		if (this.props.userAdmin) {
			//	this.initialise(this.props);
			this.loadRoles();
		}
	}

	public componentDidUpdate(_prevProps: IProps) {
		if (this.props.userAdmin) {
			//	this.initialise(this.props, prevProps);
		}
	}

	public render() {
		const { showAddModal, showEditModal, editRole, showPermissionModal, showConfirm, permissionRole } = this.state;
		//	const readPermission: boolean = checkPermission(this.state.permissions, "readPermission");
		///	const addPermission: boolean = checkPermission(this.state.permissions, "addPermission");

		return (
			<div>
				<Head>
					<title>Role Management — {process.env.APP_NAME}</title>
				</Head>
				<LayoutDashboard>
					<AdminFixedBar />
					<header className="hero theme--accent">
						<HeroHeading title="Role & Permission Management" />
						<span className="hero-actions">
							<Button
								className="admin_role-add-button"
								onClick={this.toggleAddModal}
								variant="contained"
								color="primary"
							>
								Add Role
					</Button>
						</span>
					</header>


					{this.state.roles.length === 0 ? (
						<div className="admin_content">
							<ListLoading active={true} />
						</div>
					) : (

							showPermissionModal ? <div className="admin_role-permission-management" >
								<h2>{this.state.permissionRole.name + " permission management"}</h2>
								<Button
									className="admin_role-back-button"
									onClick={() => this.toggleMangePermissionModal()}
									variant="contained"
									color="primary"
								>
									Back to roles
								</Button>
								<PermissionGrid
									apiService={this.props.apiService}
									role={permissionRole}
								// topics={this.state.topics}
								// title="Permission Management"
								// type="discussions"
								// situations={this.state.situations}
								// handleTopicsUpdate={this.handleTopicsUpdate}
								/> </div> : <div className="admin_content role-content">
									{/* {readPermission&&<RolesGrid
								title="Role Management"
								type="discussions"
								topics={this.state.topics}
								situations={this.state.situations}
								apiService={this.props.apiService}
								handleTopicsUpdate={this.handleTopicsUpdate}
							/>} */}
									
									<div className="admin_content-grid hm-t40">
										{this.state.roles.map((role) => {
											return (

												<Paper key={role.id} className="admin_content-link-content" square={true}>
													<div className="hp-16 admin_role-tile-content">
														<h2 className="font--h5 admin_role-tile-heading" >{role.name}</h2>
														<RoleOptions role={role} handleEdit={this.toggleEditModal} handleDelete={this.toggleConfrim} handleManagePermissions={this.toggleMangePermissionModal} />
													</div>
												</Paper>
											);
										})}
									</div>
								</div>

						)}
					{showAddModal && <CreateRoleModal loading={this.state.isLoadingPermission} handleAdd={this.addRole} handleClose={this.toggleAddModal} />}
					{showConfirm && <ConfirmDelete role={this.state.roleToDelete} isLoading={this.state.isLoadingPermission} submitDeleteRole={this.deleteRole} toggleDeleteModal={this.toggleConfrim} />}
					{showEditModal && <EditRoleModal role={editRole} loading={this.state.isLoadingPermission} handleEdit={this.editRole} handleClose={this.toggleEditModal} />}
					{/* {showPermissionModal && <ManagePermissionModal handleClose={this.toggleMangePermissionModal} apiService={this.props.apiService} />} */}
				</LayoutDashboard>
			</div>
		);
	}


	protected toggleAddModal = () => {
		this.setState({ showAddModal: !this.state.showAddModal });
	};
	protected toggleEditModal = (role?: IApiRole) => {
		this.setState({ showEditModal: !this.state.showEditModal, editRole: role });
	};
	protected toggleConfrim = (role?: IApiRole) => {
		this.setState({ showConfirm: !this.state.showConfirm, roleToDelete: role });
	};
	protected toggleMangePermissionModal = (permissionFor?: IApiRole) => {
		this.setState({ showPermissionModal: !this.state.showPermissionModal, permissionRole: permissionFor });
	};
	protected handleTopicsUpdate(topics: IApiTopic[]) {
		this.setState({
			topics,
		});
	}
	protected loadRoles = () => {
		if (
			!this.state.isLoadingPermission
		) {

			const url = '/api/roles';
			this.setState(
				{
					isLoadingPermission: true,
				},
				() => {
					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							this.setState({
								roles: response.roles.data,
								isLoadingPermission: false,
							});

						}).catch(_error => {
							this.setState({ isLoadingPermission: false })
						});
				},
			);
		}
	}
	protected addRole = (name: string) => {
		if (
			!this.state.isLoadingPermission
		) {

			const url = `/api/roles?name=${name}`;
			this.setState(
				{
					isLoadingPermission: true,
				},
				() => {
					this.props.apiService.queryPOST(url, {})
						.then((response: any) => {
							this.props.dispatch(apiSuccess(["Role added successfully"]));
							this.setState({
								roles: [...this.state.roles, response.role],
								isLoadingPermission: false,
								showAddModal: false
							});

						}).catch(_error => {
							this.props.dispatch(apiError([_error.message]));
							this.setState({ isLoadingPermission: false });
						});
				},
			);
		}
	}
	protected editRole = (id: number, name: string) => {
		if (
			!this.state.isLoadingPermission
		) {

			const url = `/api/roles/${id}`;
			this.setState(
				{
					isLoadingPermission: true,
				},
				() => {
					this.props.apiService.queryPUT(url, { name })
						.then((response: any) => {
							this.props.dispatch(apiSuccess(["Role edited successfully"]));
							const roles = [...this.state.roles];
							const data: IApiRole = response.role;
							const index = roles.findIndex(r => r.id === data.id);
							roles[index] = data;
							this.setState({
								roles,
								isLoadingPermission: false,
								showEditModal: false
							});

						}).catch(_error => {
							this.props.dispatch(apiError([_error.message]));
							this.setState({ isLoadingPermission: false });
						});
				},
			);
		}
	}

	protected deleteRole = () => {
		if (
			!this.state.isLoadingPermission
		) {

			const url = `/api/roles/${this.state.roleToDelete.id}`;
			this.setState(
				{
					isLoadingPermission: true,
				},
				() => {
					this.props.apiService.queryDELETE(url)
						.then((_response: any) => {
							if (_response.success) {
								this.props.dispatch(apiSuccess(["Role deleted successfully"]));
								const roles = [...this.state.roles];
								const index = roles.findIndex(r => r.id === this.state.roleToDelete.id);
								roles.splice(index, 1);
								this.setState({
									roles,
									isLoadingPermission: false,
									showConfirm: false
								});
							}
							else {
								this.props.dispatch(apiError(["Failed to delete role"]));
								this.setState({ isLoadingPermission: false });
							}


						}).catch(_error => {
							this.props.dispatch(apiError([_error.message]));
							this.setState({ isLoadingPermission: false });
						});
				},
			);
		}
	}
	protected handleTabChange(_event, value) {
		this.setState({
			value,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { topics } = state.topics;
	const { situations } = state.situations;
	return {
		topics,
		situations,
	};
}

export default connect(mapStateToProps)(Topics);
