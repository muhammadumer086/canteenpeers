import React from 'react';
import { connect } from 'react-redux';

import ButtonBase from '@material-ui/core/ButtonBase';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';

import { IStoreState, apiError, apiSuccess } from '../../../redux/store';

import { IApiPermission, IApiRole } from '../../../interfaces';

import { ApiService } from '../../../services';
import { CircularProgress } from '@material-ui/core';
export enum generalPermissions {
    add = "Add",
    edit = "Edit",
    delete = "Delete",
    read = "View",
    all = "All",
};

export enum adminModules {
    users_management = "Users Management",
    all_users_activities = "All Users Activities",
    topic_management = "Topic Management",
    reported_discussions = "Reported Discussions",
    events_management = "Events Management",
    alert_management = "Alert Management",
    staff_management = "Staff Management",
    graduate_users_management = "Graduate Users Management",
    events= "Events",
    blogs="Blogs",
    discussions="Discussions",
    resources="Resources"
};
const adminModuleArr = Object.keys(adminModules);
const generalPermissionsArr = Object.keys(generalPermissions);

const allPermissionArr = [];
adminModuleArr.map(aM => {
    generalPermissionsArr.map(gp => {
        const name = aM + "_" + gp;
        allPermissionArr.push(name);
    })
});
interface IProps {
    //title: string;
    //type: string;
    //situations: IApiSituation[];
    //topics: IApiTopic[];
    role: IApiRole;
    apiService: ApiService;
    //handleTopicsUpdate?(topics: IApiTopic[]): void;
    dispatch(action: any): void;
}

interface IState {
    loading: boolean;
    changed: boolean;
    // topics: IApiTopic[];
    role_permissions: IApiPermission[];
    received_permissions: IApiPermission[];

}

class RolesGrid extends React.Component<IProps, IState> {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            changed: false,
            //  topics: JSON.parse(JSON.stringify(props.topics)),
            role_permissions: [],
            received_permissions: [],
        };

        this.handleChange = this.handleChange.bind(this);
        this.loadPermissions = this.loadPermissions.bind(this);
        this.savePermissions = this.savePermissions.bind(this);
        this.cancelChanges = this.cancelChanges.bind(this);
        this.createPermission = this.createPermission.bind(this);
    }
    public componentDidMount() {
        this.loadPermissions();
        // this.createPermission(); just create all permission once
    }
    public componentDidUpdate(_prevProps: IProps) {
        // if (this.props.topics !== prevProps.topics) {
        //     this.setState({
        //         topics: JSON.parse(JSON.stringify(this.props.topics)),
        //     });
        // }
    }

    public render() {
        const { role_permissions, loading } = this.state;
        if (loading) {
            return <div><CircularProgress /></div>
        }
        return (
            <div>
                <table className="admin_topics_grid-content-table admin_role-permission-grid-table" >
                    <thead >
                        <tr className="admin_role-table-head-row" >
                            <th className="admin_role-table-header"></th>
                            {generalPermissionsArr.map((permissionKey, index) => {

                                return <th className="admin_role-table-header" key={index}>{generalPermissions[permissionKey]}</th>;
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {adminModuleArr.map((adminModule, topicIndex) => {

                            return (
                                <tr key={topicIndex}>
                                    <th className="admin_role-table-content-module" >
                                        {adminModules[adminModule]}
                                    </th>

                                    {generalPermissionsArr.map(
                                        (_singleGP, index) => {
                                            const pCheck = adminModule + "_" + _singleGP;
                                            const isActive =
                                                role_permissions.findIndex(r => r.name === pCheck) > -1;
                                            const classNames = [
                                                'admin_topics_grid-content-table-cell',
                                            ];
                                            if (isActive) {
                                                classNames.push(
                                                    'admin_topics_grid-content-table-cell--active',
                                                );
                                            }
                                            const disabled=topicIndex>7&&index!==3;//disable blogs_read
                                            console.log(topicIndex,index,disabled)
                                          
                                            // events_read
                                           // discussions_read
                                           // resources_read except these permissions

                                            return (
                                                <td
                                                    key={index}
                                                    className={classNames.join(
                                                        ' ',
                                                    )}
                                                >
                                                    <Checkbox
                                                        checked={
                                                            isActive
                                                        }
                                                        onChange={() => {
                                                            this.handleChange(
                                                                pCheck,
                                                                // "",
                                                            );
                                                        }}
                                                        disabled={
                                                            this.state
                                                                .loading||disabled
                                                        }
                                                    />
                                                    <ButtonBase
                                                        focusRipple
                                                        className="admin_topics_grid-content-table-cell-button"
                                                        onClick={() => {
                                                            this.handleChange(
                                                                pCheck,
                                                                // situation,
                                                            );
                                                        }}
                                                        tabIndex={-1}
                                                        disabled={
                                                            this.state
                                                                .loading||disabled
                                                        }
                                                    ></ButtonBase>
                                                </td>
                                            );
                                        },
                                    )}

                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <section>
                   
                        <div className="hm-16 admin_role-actions" >
                            <Button
                                variant="contained"
                                color="primary"
                                
                                onClick={this.savePermissions}
                                disabled={this.state.loading||!this.state.changed}
                            >
                                Save Changes
            				</Button>
                            <span className="hm-l16">
                                <Button
                                    onClick={this.cancelChanges}
                                    disabled={this.state.loading||!this.state.changed}
                                >
                                    Cancel
            					</Button>
                            </span>
                        </div>
                    

                </section>
            </div>
        );
    }

    protected handleChange(permission:string) {
        const { role_permissions } = this.state;
        const newArr = [...role_permissions];

        const index = this.state.role_permissions.findIndex(rp => rp.name === permission);
        const indexInAll=allPermissionArr.findIndex(p=>p===permission);//to add view option for this permission
        const mod=indexInAll%5;
        
        
        if (index < 0) {//not fount add in to array
            const obj: IApiPermission = {
                id: newArr.length,
                name: permission,
                guard_name: "",
                created_at: "",
                updated_at: ""
            };

            newArr.push(obj);
            if(mod<3){//if permission is add,edit or delete then add view permission for this module
                const diff=3-mod; 
                const viewIndex= role_permissions.findIndex(p=>p.name===allPermissionArr[indexInAll+diff]);
                if(viewIndex<0){//view of this permission is not in all permission so add view permission
                    const obj1: IApiPermission = {
                        id: newArr.length,
                        name: allPermissionArr[indexInAll+diff],
                        guard_name: "",
                        created_at: "",
                        updated_at: ""
                    }
                    newArr.push(obj1);
                }
            }
           console.log(newArr);
            this.setState({ role_permissions: newArr, changed: true })
        }
        else {//permission found remove from array
            if(mod===3){//if permission is view then remove others
                
                  allPermissionArr.slice(indexInAll-3,indexInAll+1).map(p=>{
                      console.log(p);
                      const foundIndex=newArr.findIndex(r=>r.name===p);
                      if(foundIndex>-1){
                          newArr.splice(foundIndex,1);
                      }
                  })
               
            }
            else{
                newArr.splice(index, 1);
            }
            console.log(newArr)
            this.setState({ role_permissions: newArr, changed: true })
        }
        return;
        // Duplicate the topics
        // const topics: IApiTopic[] = JSON.parse(
        //     JSON.stringify(this.state.role_permissions),
        // );

        // const situationIds = topics[topicIndex].situations.map(situation => {
        //     return situation.id;
        // });

        // const situationIndex = situationIds.indexOf(situation.id);

        // if (situationIndex >= 0) {
        //     topics[topicIndex].situations.splice(situationIndex, 1);
        // } else {
        //     topics[topicIndex].situations.push(
        //         JSON.parse(JSON.stringify(situation)),
        //     );
        // }

        // this.setState({
        //     //topics,
        //     changed: true,
        // });
    }
    protected createPermission = async () => {
        const promiseArr = [];
        adminModuleArr.map(aM => {
            generalPermissionsArr.map(gp => {
                const name = aM + "_" + gp;
                const url = '/api/permissions?name=' + name;
                promiseArr.push(
                    this.props.apiService.queryPOST(url, {})
                );
            })
        });
        //const res = await Promise.all(promiseArr);
       // console.log("response of all ===> ", res);
    }
    protected savePermissions() {
        this.setState(
            {
                loading: true,
            },
            () => {
                const data = [];
                this.state.role_permissions.map(singlePermission=>{
                    data.push(singlePermission.name);
                })
                this.props.apiService
                    .queryPUT(
                        `/api/roles/${this.props.role.id}/assignpermissions`,
                        {
                            permissions: data,
                        },
                    )
                    .then(() => {
                        this.props.dispatch(apiSuccess(["Permissions updated"]));

                        this.setState({
                            loading: false,
                            changed: false,
                        });
                        //   this.props.handleTopicsUpdate(topics);
                    })
                    .catch(_error => {
                        // dispatch the error message
                        this.props.dispatch(apiError([_error.message]));
                        this.setState({
                            loading: false,
                        });
                    });
            },
        );
    }

    protected cancelChanges() {
        this.setState({
            role_permissions: JSON.parse(JSON.stringify(this.state.received_permissions)),
            changed: false,
        });
    }

    protected loadPermissions = () => {
        this.setState(
            {
                loading: true,
            },
            () => {
                this.props.apiService
                    .queryGET('/api/roles/' + this.props.role.id)
                    .then((response) => {
                        if ('role' in response && 'permissions' in response.role) {
                            this.setState({
                                loading: false,
                                role_permissions: response.role.permissions,
                                received_permissions: response.role.permissions,
                            });
                        }
                        else {
                            this.props.dispatch(apiError(['Invalid data received']));
                            this.setState({
                                loading: false,
                            });
                        }

                    })
                    .catch(error => {
                        // dispatch the error message
                        this.props.dispatch(apiError([error.message]));
                        this.setState({
                            loading: false,
                        });
                    });
            },
        );
    }
}

function mapStateToProps(_state: IStoreState) {
    return {};
}

export default connect(mapStateToProps)(RolesGrid);
