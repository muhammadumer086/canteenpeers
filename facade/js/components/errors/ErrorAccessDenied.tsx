import React from 'react';

const ErrorAccessDenied: React.FC<{}> = () => {
	return (
		<div className="hp-16">Access Denied</div>
	);
}

export default ErrorAccessDenied;
