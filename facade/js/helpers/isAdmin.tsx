import { IApiUser } from '../interfaces';

 function isAdmin(userData: IApiUser|null): boolean {
	return (
		userData &&
		userData.hasOwnProperty('role_names') &&
		userData.role_names instanceof Array &&
		userData.role_names.indexOf('admin') >= 0
	);
}
export default isAdmin;
