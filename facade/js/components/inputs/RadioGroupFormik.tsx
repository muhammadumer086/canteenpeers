import React from 'react';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

interface IProps {
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	options: Array<{
		label: string;
		value: string | number | boolean;
		tooltip?:string;
	}>;
	disabled?: boolean;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
	handleBlur(e: any): void;
	setRef?(any):void;
}

export const RadioGroupFormik: React.FC<IProps> = (props: IProps) => {
	const optionsMapped = props.options.map(option => {
		return {
			label: option.label,
			value: option.value,
			valueMapped: option.value.toString(),
			tooltip: option.tooltip,
		};
	});

	const optionSelected = optionsMapped.find(element => {
		return element.value === props.values[props.name];
	});

	return (
		<FormControl
			error={props.touched[props.name] && !!props.errors[props.name]}
			className="form-input form-input-radio_wrapper"
			disabled={props.disabled}
		>
			<FormLabel className="form-input-label">{props.label}</FormLabel>
			<RadioGroup
				className="form-input-radio_group"
				aria-label={props.label}
				name={props.name}
				value={optionSelected ? optionSelected.valueMapped : null}
				onChange={handleChange}
				innerRef={props.setRef}
			>
				{optionsMapped.map((singleOption, index) => {
					return (
						<FormControlLabel
							className="form-input-radio"
							value={singleOption.valueMapped}
							control={
								<Radio
									className="form-input-radio-tick"
									color="primary"
								/>
							}
							label={
								singleOption.tooltip ? (
									<span>
										{singleOption.label}
										<br />
										<em className="form-input-checkbox-tooltip">
											{singleOption.tooltip}
										</em>
									</span>
								) : (
									singleOption.label
								)
							}
							key={index}
						/>
					);
				})}
			</RadioGroup>
			{props.touched[props.name] && !!props.errors[props.name] && (
				<FormHelperText className="color-error" >{props.errors[props.name]}</FormHelperText>
			)}
		</FormControl>
	);

	function handleChange(event) {
		const option = optionsMapped.find(element => {
			return element.valueMapped === event.target.value;
		});
		props.setFieldValue(props.name, option.value);
		props.setFieldTouched(props.name, true, true);
	}
};
