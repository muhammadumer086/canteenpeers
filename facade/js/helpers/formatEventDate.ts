import moment from 'moment';

export function formatEventDate(startDate: moment.Moment | string, endDate: moment.Moment | string) {
	const eventDateStart = moment(startDate).format('dddd, D MMM');
	const eventDateEnd = moment(endDate).format('dddd, D MMM');
	return eventDateStart === eventDateEnd
		? eventDateStart
		: `${eventDateStart} - ${eventDateEnd}`;
}
