import React from 'react';

import { InlineDatePicker } from 'material-ui-pickers';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

interface IProps {
	id?: string;
	label: string;
	name: string;
	format: string;
	values: any;
	touched: any;
	errors: any;
	disabled?: boolean;
	disableFuture?: boolean;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
	checkSensitiveAge?(date: string): void;
	setRef?(any): any;
	customError?: boolean;
}

export const InlineDatePickerFormik: React.FC<IProps> = props => {
	const handleChange = event => {
		props.setFieldValue(props.name, event, true);
		props.setFieldTouched(props.name, true, true);
		if (props.name === 'start_date' && !props.values.last_joining_date) {
			//to set last date for registration
			props.setFieldValue('last_joining_date', event, true);
			props.setFieldTouched('last_joining_date', true, true);
		}
		if (event !== '' && !!props.checkSensitiveAge) {
			props.checkSensitiveAge(event);
		}
	};

	const handleInputChange = event => {
		if (event.target.value === '') {
			props.setFieldValue(props.name, null, true);
			props.setFieldTouched(props.name, true, true);
		}
	};

	return (
		<div className="form-input form-input--datepicker">
			<MuiPickersUtilsProvider utils={MomentUtils}>
				<InlineDatePicker
					id={props.id}
					clearable={true}
					keyboard={true}
					autoOk={true}
					disableFuture={props.disableFuture}
					openToYearSelection={true}
					name={props.name}
					className="form--datepicker"
					format="DD/MM/YYYY"
					helperText={`${
						props.name === 'last_joining_date' &&
						props.errors[props.name]
							? 'Invalid last date for registration'
							: `${
									props.customError &&
									props.errors[props.name]
										? props.errors[props.name]
										: ''
							  } Format: ${props.format}`
					} `}
					value={
						props.values[props.name]
							? props.values[props.name]
							: null
					}
					mask={[
						/\d/,
						/\d/,
						'/',
						/\d/,
						/\d/,
						'/',
						/\d/,
						/\d/,
						/\d/,
						/\d/,
					]}
					label={props.label}
					onChange={handleChange}
					onInputChange={handleInputChange}
					inputRef={props.setRef ? props.setRef : undefined}
					error={
						(props.touched[props.name] || props.customError) &&
						!!props.errors[props.name]
					}
				/>
			</MuiPickersUtilsProvider>
		</div>
	);
};
