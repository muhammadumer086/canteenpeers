import React from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import ReactSVG from 'react-svg';

import ButtonBase from '@material-ui/core/ButtonBase';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuDropDown from './Menu';

// import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import {
	IStoreState,
	userPanelDisplay,
	notificationsPanelDisplay,
	searchModalDisplay,
} from '../../redux/store';
import { AuthService, withServices } from '../../services';
import { IApiUser, IApiNotification } from '../../interfaces';

import { menuPanelDisplay } from '../../redux/actions/menuPanel';

import UserAvatar from './UserAvatar';
import { ButtonLink } from './ButtonLink';
import { GTMDataLayer } from '../../helpers/dataLayer';
import { getCountry } from '../../helpers/getCountry';
import Link from 'next/link';
import ReportProblemModal from '../report-problem/ReportProblemModal';

interface IProps {
	userAuthenticated: boolean | null;
	userData: IApiUser | null;
	notificationsData: IApiNotification | null;
	authService: AuthService;
	notificationsPanelDisplay: boolean;
	notificationUnreadCount: number;
	dispatch(action: any): void;
}

interface IState {
	anchorEl: any | null;
	menuOpen: boolean;
	showReportModal: boolean;
	countryLogo: string;
}

class DashboardHeader extends React.Component<IProps, IState> {
	protected GTM = new GTMDataLayer();
	private arrowButtonRef = React.createRef<HTMLDivElement | null>();

	constructor(props) {
		super(props);

		this.state = {
			anchorEl: null,
			menuOpen: false,
			showReportModal: false,
			countryLogo: 'canteen-connect-logo-updated.svg',
		};

		this.handleMenuPanelOpen = this.handleMenuPanelOpen.bind(this);
		this.handleHomePage = this.handleHomePage.bind(this);
		this.handleMenuClick = this.handleMenuClick.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.handleMyAccount = this.handleMyAccount.bind(this);
		this.handleMyNotifications = this.handleMyNotifications.bind(this);
		this.handleLogout = this.handleLogout.bind(this);
		this.handleLink = this.handleLink.bind(this);
		this.toggleReportModal = this.toggleReportModal.bind(this);
		this.openSearchModal = this.openSearchModal.bind(this);
	}
	public componentDidMount() {
		const { userAuthenticated, userData } = this.props;
		let currentCountry: string = getCountry(userAuthenticated, userData);
		const countryLogo =
			currentCountry === 'AU'
				? 'canteen-connect-logo-updated.svg'
				: 'nz/logo_nz_45.svg';
		this.setState({ countryLogo });
	}

	public render() {
		const { anchorEl, menuOpen, countryLogo, showReportModal } = this.state;

		return (
			<div className="dashboard_header theme--accent">
				<div className="dashboard_header-main_wrapper theme--main">
					<div className="dashboard_header-main-content">
						<ButtonBase
							focusRipple={true}
							className="dashboard_header-main dashboard_header-main--menu"
							onClick={this.handleMenuPanelOpen}
						>
							<div className="dashboard_header-main-icon">
								<Icon>menu</Icon>
							</div>
						</ButtonBase>

						<ButtonBase
							focusRipple={true}
							className="dashboard_header-main  dashboard_header-main--logo"
							onClick={this.handleHomePage}
						>
							<div className="dashboard_header-main-logo_wrapper">
								<img
									className="dashboard_header-main-logo"
									src={`${process.env.STATIC_PATH}/images/logo-canteen-mobile-white.png`}
									alt={process.env.APP_NAME}
									width="40"
									height="40"
								/>
							</div>
						</ButtonBase>
					</div>
				</div>
				<div className="dashboard_header-secondary_wrapper">
					<div className="dashboard_header-secondary">
						<div className="dashboard_header-search">
							<Link prefetch href="/dashboard">
								<ButtonBase
									focusRipple={true}
									className="dashboard_header-title"
								>
									<img
										className="dashboard_sidebar-header-logo"
										src={`${process.env.STATIC_PATH}/images/${countryLogo}`}
										alt={process.env.APP_NAME}
									/>
								</ButtonBase>
							</Link>

							<ButtonBase
								focusRipple={true}
								onClick={this.openSearchModal}
								className="dashboard_header-search"
							>
								<div className="dashboard_header-search-content">
									<div className="dashboard_header-search-icon">
										<ReactSVG
											path={`${process.env.STATIC_PATH}/icons/search.svg`}
										/>
									</div>
									<div className="dashboard_header-search-label">
										What can we help you find?
									</div>
								</div>
							</ButtonBase>
						</div>

						{this.props.userAuthenticated === true &&
							this.props.userData !== null && (
								<div className="dashboard_header-side">
									{
										<ButtonBase
											focusRipple={false}
											className="dashboard_header-side-notifications"
											onClick={this.handleMyNotifications}
										>
											{this.props
												.notificationUnreadCount > 0 ? (
												<ReactSVG
													className="dashboard_header-side-notifications-icon dashboard_header-side-notifications-icon--active"
													path={`${process.env.STATIC_PATH}/icons/user-notification-active.svg`}
												/>
											) : (
												<ReactSVG
													className="dashboard_header-side-notifications-icon"
													path={`${process.env.STATIC_PATH}/icons/user-notification.svg`}
												/>
											)}
										</ButtonBase>
									}

									<ButtonBase
										focusRipple={true}
										className="dashboard_header-side-profile"
										onClick={this.handleMenuClick}
									>
										<div className="dashboard_header-side-profile-content">
											<div className="dashboard_header-side-profile-label">
												Welcome,{' '}
												{this.props.userData.full_name}
											</div>
											<div className="dashboard_header-side-profile-avatar">
												<UserAvatar
													user={this.props.userData}
												/>
											</div>
											{/* <div ref={this.arrowButtonRef}>
                        <ArrowDropDownIcon className="dashboard_header-side-profile-icon" />
                      </div> */}
											<MenuDropDown
												userAuthenticated={
													this.props.userAuthenticated
												}
												handleMyAccount={
													this.handleMyAccount
												}
												toggleReportModal={
													this.toggleReportModal
												}
												handleLogout={this.handleLogout}
												handleLink={this.handleLink}
												menuOpen={menuOpen}
												handleClose={this.handleClose}
												handleOpen={
													this.handleMenuClick
												}
											/>
										</div>
									</ButtonBase>
								</div>
							)}
						{this.props.userAuthenticated === false && (
							<div className="dashboard_header-side">
								<ButtonLink
									variant="outlined"
									color="secondary"
									className="dashboard_header-side-button dashboard_header-side-button--outlined"
									href="/auth/login"
								>
									Log In
								</ButtonLink>
								<ButtonLink
									variant="contained"
									color="primary"
									className="dashboard_header-side-button"
									href="/auth/register?step=step-1"
									hrefAs="/auth/register/step-1"
								>
									Join the community
								</ButtonLink>
								<ButtonBase
									focusRipple={true}
									className="dashboard_header-side-profile dashboard_header-side-profile--anonymous"
									onClick={this.handleMenuClick}
								>
									<div className="dashboard_header-side-profile-content">
										<div className="dashboard_header-side-profile-avatar">
											<ReactSVG
												path={`${process.env.STATIC_PATH}/icons/avatar.svg`}
											/>
										</div>
										{/* <div ref={this.arrowButtonRef}>
                        <ArrowDropDownIcon className="dashboard_header-side-profile-icon" />
                      </div> */}
										<MenuDropDown
											userAuthenticated={
												this.props.userAuthenticated
											}
											handleMyAccount={
												this.handleMyAccount
											}
											handleLogout={this.handleLogout}
											handleLink={this.handleLink}
											menuOpen={menuOpen}
											handleClose={this.handleClose}
											handleOpen={this.handleMenuClick}
											toggleReportModal={
												this.toggleReportModal
											}
										/>
									</div>
								</ButtonBase>
							</div>
						)}
					</div>
				</div>
				<Menu
					id="account-menu"
					anchorEl={anchorEl}
					open={Boolean(anchorEl)}
					onClose={this.handleClose}
				>
					{this.props.userAuthenticated === true && (
						<MenuItem onClick={this.handleMyAccount}>
							My account
						</MenuItem>
					)}
					{this.props.userAuthenticated && (
						<MenuItem onClick={this.toggleReportModal}>
							Report A Problem
						</MenuItem>
					)}
					{this.props.userAuthenticated === true && (
						<MenuItem onClick={this.handleLogout}>Logout</MenuItem>
					)}
					{this.props.userAuthenticated === false && (
						<MenuItem
							onClick={() => {
								this.handleLink('/auth/login');
							}}
						>
							Login
						</MenuItem>
					)}
					{this.props.userAuthenticated === false && (
						<MenuItem
							onClick={() => {
								this.handleLink('/auth/register');
							}}
						>
							Register
						</MenuItem>
					)}
				</Menu>
				{showReportModal && (
					<ReportProblemModal
						open={showReportModal}
						handleClose={this.toggleReportModal}
					/>
				)}
			</div>
		);
	}

	protected handleMenuPanelOpen() {
		this.props.dispatch(menuPanelDisplay());
	}

	protected handleHomePage() {
		Router.push('/dashboard');
	}

	protected handleMenuClick() {
		this.setState({
			anchorEl: this.arrowButtonRef.current,
			menuOpen: !this.state.menuOpen,
		});
	}

	protected handleClose() {
		this.setState({
			anchorEl: null,
			menuOpen: false,
		});
	}

	protected handleMyNotifications() {
		this.props.dispatch(notificationsPanelDisplay());
	}

	protected handleMyAccount() {
		this.handleClose();
		this.props.dispatch(userPanelDisplay(this.props.userData));
	}

	protected openSearchModal() {
		this.props.dispatch(searchModalDisplay());
	}

	protected toggleReportModal = () => {
		this.setState({ showReportModal: !this.state.showReportModal });
	};

	protected handleLogout() {
		this.GTM.pushEventToDataLayer({
			event: 'userLogout',
			login: 'No',
		});
		this.handleClose();
		this.props.authService.logOut();
	}

	protected handleLink(url: any, as?: string) {
		Router.push(url, as);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { notificationsData, notificationUnreadCount } = state.notifications;
	return {
		userAuthenticated,
		userData,
		notificationsData,
		notificationUnreadCount,
	};
}

export default connect(mapStateToProps)(withServices(DashboardHeader));
