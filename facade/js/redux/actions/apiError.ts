import { IStoreState } from '../store';
import { actionTypes } from '../actions';

let dismissTimeout = null;

export const apiError = (apiError: string[]) => (
	dispatch,
	_getState: () => IStoreState,
) => {
	// Auto-dismiss message when in browser
	if ((process as any).browser) {
		if (dismissTimeout) {
			window.clearTimeout(dismissTimeout);
		}
		dismissTimeout = window.setTimeout(() => {
			dispatch({
				type: actionTypes.API_ERROR_DISMISS,
			});
		}, 10000);
	}

	if (!(apiError instanceof Array)) {
		apiError = [apiError];
	}

	return dispatch({
		type: actionTypes.API_ERROR,
		apiError: apiError,
	});
};

export const apiErrorDismiss = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().apiError.apiErrorShowing) {
		return;
	}
	return dispatch({
		type: actionTypes.API_ERROR_DISMISS,
	});
};
