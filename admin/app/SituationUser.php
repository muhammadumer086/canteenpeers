<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituationUser extends Model {

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'situation_user';

	/**
	 * The users that belong to this situation
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

	/**
	 * The users that belong to this situation
	 */
	public function situations() {
		return $this->belongsToMany('App\Situation');
	}
}
