import { Quill } from 'quill';

/**
 * Quill plugin allowing to drop images directly in the editor and handle the upload of the image
 */
export default class QuillImageDrop {
	protected quill: Quill;
	protected options: any;
	protected range: any;
	protected fileHolder: HTMLInputElement;
	/**
	 * Instantiate the module given a quill instance and any options
	 * @param {Quill} quill
	 * @param {Object} options
	 */
	constructor(quill: Quill, options: any) {
		// save the quill reference
		this.quill = quill;
		this.options = options;
		this.range = null;

		if (typeof this.options.upload !== 'function') {
			console.warn(
				'[Missing config] upload function that returns a promise is required',
			);
		}

		// bind handlers to this instance
		this.handleDrop = this.handleDrop.bind(this);
		this.handlePaste = this.handlePaste.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSelectImage = this.handleSelectImage.bind(this);
		this.uploadFiles = this.uploadFiles.bind(this);
		// listen for drop and paste events
		this.quill.root.addEventListener('drop', this.handleDrop, false);
		this.quill.root.addEventListener('paste', this.handlePaste, false);

		// Add handler for the image button in the toolbar
		const toolbar = this.quill.getModule('toolbar');
		toolbar.addHandler('image', this.handleSelectImage);
	}

	/**
	 * Handler for drop event to read dropped files from evt.dataTransfer
	 * @param {DragEvent} evt
	 */
	public handleDrop(evt: DragEvent) {
		evt.preventDefault();
		if (
			evt.dataTransfer &&
			evt.dataTransfer.files &&
			evt.dataTransfer.files.length
		) {
			if (document.caretRangeFromPoint) {
				const selection = document.getSelection();
				const range = document.caretRangeFromPoint(
					evt.clientX,
					evt.clientY,
				);
				if (selection && range) {
					selection.setBaseAndExtent(
						range.startContainer,
						range.startOffset,
						range.startContainer,
						range.startOffset,
					);
				}
			}

			const files: File[] = evt.dataTransfer.files as any;
			this.uploadFiles(files);
			// this.readFiles(files, this.insert.bind(this));
		}
	}

	/**
	 * Handler for paste event to read pasted files from evt.clipboardData
	 * @param {ClipboardEvent} evt
	 */
	public handlePaste(evt: ClipboardEvent) {
		if (
			evt.clipboardData &&
			evt.clipboardData.items &&
			evt.clipboardData.items.length
		) {
			if (evt.clipboardData.types.indexOf('Files') >= 0) {
				evt.preventDefault();
			}
			const files: File[] = evt.clipboardData.items as any;
			this.uploadFiles(files);
			// this.readFiles(files, (dataUrl: string) => {
			// 	const selection = this.quill.getSelection();
			// 	if (selection) {
			// 		// we must be in a browser that supports pasting (like Firefox)
			// 		// so it has already been placed into the editor
			// 	}
			// 	else {
			// 		// otherwise we wait until after the paste when this.quill.getSelection()
			// 		// will return a valid index
			// 		setTimeout(() => this.insert(dataUrl), 0);
			// 	}
			// });
		}
	}

	public handleInputChange() {
		const files: File[] = this.fileHolder.files as any;
		this.uploadFiles(files);
	}

	public handleSelectImage() {
		this.range = this.quill.getSelection();
		this.fileHolder = document.createElement('input');
		this.fileHolder.setAttribute('type', 'file');
		this.fileHolder.setAttribute('accept', 'image/*');
		this.fileHolder.addEventListener(
			'change',
			this.handleInputChange,
			false,
		);
		this.fileHolder.click();
	}

	/**
	 * Insert the image into the document at the current cursor position
	 * @param {String} dataUrl  The base64-encoded image URI
	 */
	public insert(dataUrl: string) {
		const index =
			((this.quill.getSelection() || {}) as any).index ||
			this.quill.getLength();

		this.quill.insertEmbed(index, 'image', dataUrl, 'user');
	}

	/**
	 * Extract image URIs a list of files from evt.dataTransfer or evt.clipboardData
	 * @param {File[]} files  One or more File objects
	 * @param {Function} callback  A function to send each data URI to
	 */
	public readFiles(files: File[], callback: Function) {
		// check each file for an image
		[].forEach.call(files, (file: any) => {
			if (
				!file.type.match(
					/^image\/(gif|jpe?g|a?png|svg|webp|bmp|vnd\.microsoft\.icon)/i,
				)
			) {
				// file is not an image
				// Note that some file formats such as psd start with image/* but are not readable
				return;
			}
			// set up file reader
			const reader = new FileReader();
			reader.onload = (evt: any) => {
				callback(evt.target.result);
			};
			// read the clipboard item or file
			const blob = file.getAsFile ? file.getAsFile() : file;
			if (blob instanceof Blob) {
				reader.readAsDataURL(blob);
			}
		});
	}

	public uploadFiles(files: File[]) {
		[].forEach.call(files, (file: any) => {
			if (
				!file.type.match(
					/^image\/(gif|jpe?g|a?png|svg|webp|bmp|vnd\.microsoft\.icon)/i,
				)
			) {
				// file is not an image
				// Note that some file formats such as psd start with image/* but are not readable
				return;
			}

			const blob = file.getAsFile ? file.getAsFile() : file;

			this.options
				.upload(blob)
				.then((url: string) => {
					this.insert(url);
				})
				.catch((e: any) => {
					console.warn(e);
				});
		});
	}
}
