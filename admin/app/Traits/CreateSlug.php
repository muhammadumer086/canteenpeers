<?php
namespace App\Traits;

use Illuminate\Support\Str;
use Log;
/**
 * Create a slug and make sure it's unique
 */
trait CreateSlug {
	/**
	 * @param $title
	 * @param string $model
	 * @param int $id
	 * @return string
	 * @throws \Exception
	 */
	public function createSlug($title, $model, $id = null) {
		// Normalize the title
		$originalSlug = Str::slug($title, '-');

		$slug = $originalSlug;
		$index = 0;
		$slugFound = false;
		// Test if a model already has the slug
		do {
			$query = $model::where('slug', $slug)->withTrashed();
			if (!is_null($id)) {
				$query->where('id', '<>', $id);
			}
			if ($query->exists()) {
				++$index;
				$slug = "{$originalSlug}-{$index}";
			} else {
				$slugFound = true;
			}
		} while (!$slugFound);

		return $slug;
	}
}

?>
