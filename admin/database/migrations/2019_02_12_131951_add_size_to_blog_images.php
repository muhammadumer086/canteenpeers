<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSizeToBlogImages extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('blog_images', function (Blueprint $table) {
			$table->unsignedInteger('width')->nullable();
			$table->unsignedInteger('height')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('blog_images', function (Blueprint $table) {
			$table->dropColumn('width');
			$table->dropColumn('height');
		});
	}
}
