import moment from 'moment-timezone';

export function generateAndFormatTimezoneDate(
	date: string,
	timezone: string='Australia/Sydney',
	format = 'YYYY-MM-DD HH:mm:ssZZ',
) {
	const momentDate = moment.tz(date, timezone);
	const momentDateFormatted = momentDate.format(format);

	return momentDateFormatted;
}
