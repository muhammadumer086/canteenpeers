<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancer extends Model {


	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'cancer_types';


	/**
	 * The users part of the conversation
	 */
	public function users() {
		return $this->belongsToMany('App\User');
	}

}
