<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavingReason extends Model {
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'leaving_reasons_data',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'leaving_reasons_data' => 'array',
	];

	/**
	 * The user associated with the leaving reason
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}
}
