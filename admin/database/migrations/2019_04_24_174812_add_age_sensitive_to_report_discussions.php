<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAgeSensitiveToReportDiscussions extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('report_discussions', function (Blueprint $table) {
			$table->boolean('age_sensitive')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('report_discussions', function (Blueprint $table) {
			$table->dropColumn('age_sensitive');
		});
	}
}
