import React from 'react';
import { ModalDialog } from '../modals/local/ModalDialog';
import Button from '@material-ui/core/Button';
import { contactModalDisplay } from '../../redux/store';

interface IProps {
	message: string;
	handleClose(any): void;
	dispatch(action: any): void;
}
interface IState {
	name?: string;
}
class EventErrorModal extends React.Component<IProps, IState> {
	constructor(p: IProps) {
		super(p);
	}
	render() {
		return (
			<ModalDialog
				isActive={true}
				modalTitle="Error"
				ariaTag="edit-event-modal"
				handleClose={this.props.handleClose}
			>
				<div
					style={{
						display: 'flex',
						alignItems: 'center',
						flexDirection: 'column',
					}}
				>
					<h4>{this.props.message}</h4>
					<Button
						variant="contained"
						color='primary'
						onClick={(e) => {
                            e.stopPropagation();
							this.props.dispatch(contactModalDisplay());
						}}
					>
						Contact Canteen
					</Button>
				</div>
			</ModalDialog>
		);
	}
}
export default EventErrorModal;
