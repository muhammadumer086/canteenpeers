import React from 'react';
import { connect } from 'react-redux';
import Div100vh from 'react-div-100vh';

import Button from '@material-ui/core/Button';
import Drawer from '@material-ui/core/Drawer';

import ClearIcon from '@material-ui/icons/Clear';

import { IStoreState, userPanelHide } from '../../redux/store';

import { IApiUser,IApiPermission } from '../../interfaces';
import UserPanelGeneral from './UserPanelGeneral';
import UserPanelAccount from './UserPanelAccount';
import { withServices, ApiService, AuthService } from '../../services';
import {EUsersManagementPermissions} from "../admin/pages/users";
import {checkPermission} from "../../helpers/checkPermission";

interface IProps {
	userPanelDisplay: boolean;
	userPanelData: IApiUser | null;
	userAuthenticated: boolean;
	userAdmin: boolean;
	userData: IApiUser | null;
	apiService: ApiService;
	authService: AuthService;
	permissions : IApiPermission[];
	roleName : string;
	dispatch(action: any): void;
}

interface IState {
	displayUserAccount: boolean;
}

class UserPanel extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			displayUserAccount: false,
		};

		this.toggleEditUser = this.toggleEditUser.bind(this);
		this.closePane = this.closePane.bind(this);
	}

	componentDidMount() {
		this.setState({
			displayUserAccount:
				this.props.userData &&
				this.props.userPanelData &&
				this.props.userData.id === this.props.userPanelData.id,
		});
	}

	componentDidUpdate(prevProp: IProps) {
		if (
			JSON.stringify(this.props.userPanelData) !==
			JSON.stringify(prevProp.userPanelData)
		) {
			this.setState({
				displayUserAccount:
					this.props.userData &&
					this.props.userPanelData &&
					this.props.userData.id === this.props.userPanelData.id,
			});
		}
	}

	public render() {
		const {roleName,permissions}=this.props;
		const editPermission=roleName==="admin"||checkPermission(permissions,EUsersManagementPermissions.EDIT,EUsersManagementPermissions.ALL);
		const deletePermission=roleName==="admin"||checkPermission(permissions,EUsersManagementPermissions.ALL,EUsersManagementPermissions.DELETE);
		const allPermission=roleName==="admin"||checkPermission(permissions,EUsersManagementPermissions.ALL);
		return (
			<Drawer
				anchor="right"
				open={this.props.userPanelDisplay}
				onClose={this.closePane}
			>
				{this.props.userPanelData && (
					<Div100vh>
						{this.state.displayUserAccount ? (
							<UserPanelAccount
								closePane={this.closePane}
								deletePermission={deletePermission}
								allPermission={allPermission}
								apiService={this.props.apiService}
								authService={this.props.authService}
							/>
						) : (
							<UserPanelGeneral
								currentTab="showUser"
								closePane={this.closePane}
							/>
						)}
						{this.props.userAdmin && editPermission&&(
							<div className="user_panel-switch">
								<Button
									onClick={this.toggleEditUser}
									variant="contained"
								>
									{this.state.displayUserAccount
										? 'Show User'
										: 'Edit User'}
								</Button>
							</div>
						)}
					</Div100vh>
				)}
				<button
					onClick={this.closePane}
					className="close_button close_button--primary"
					title="Dismiss"
				>
					<ClearIcon className="close_button-icon" />
				</button>
			</Drawer>
		);
	}

	protected toggleEditUser() {
		this.setState({
			displayUserAccount: !this.state.displayUserAccount,
		});
	}

	protected closePane() {
		this.props.dispatch(userPanelHide());
	}
}

function mapStateToProps(state: IStoreState) {
	const {  userData } = state.user;
	const {permissions}=state.permission;
	const { userPanelDisplay, userPanelData } = state.userPanel;
	const role_names=userData&&userData.role_names;
		const roleName=role_names&&role_names.length>0&&role_names[0];
		const userAdmin=roleName==="user"?false:true;
	return {
		userPanelDisplay,
		userPanelData,
		userAdmin,
		userData,
		permissions,
		roleName
	};
}

export default connect(mapStateToProps)(withServices(UserPanel));
