import React from 'react';

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Select from '@material-ui/core/Select';
import { ISelectOption } from '../../interfaces';

interface IProps {
	id?: string;
	type: string;
	label: string;
	name: string;
	values: any;
	touched: any;
	errors: any;
	disabled?: boolean;
	multiline?: boolean;
	autoComplete?: string;
	helperText?: string;
	setRef?(any): any;
	handleChange(e: React.ChangeEvent<any>): void;
	handleBlur(e: any): void;
	countryCodes: ISelectOption[];
}
interface IState {
	code: string;
}
export class PhoneFieldFormik extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);
		this.state = {
			code: '+61',
		};
	}
	public render() {
		return (
			<TextField
				className="form-input"
				id={this.props.id}
				type={this.props.type}
				label={this.props.label}
				name={this.props.name}
				onChange={this.props.handleChange}
				onBlur={this.props.handleBlur}
				multiline={this.props.multiline}
				disabled={this.props.disabled}
				value={this.props.values[this.props.name]}
				error={
					this.props.touched[this.props.name] &&
					!!this.props.errors[this.props.name]
				}
				helperText={
					this.props.touched[this.props.name] &&
					this.props.errors[this.props.name]
						? this.props.errors[this.props.name]
						: this.props.helperText
				}
				autoComplete={this.props.autoComplete}
				inputRef={this.props.setRef ? this.props.setRef : undefined}
				InputProps={{
					startAdornment: (
						<InputAdornment position="start">
							<Select
								native
								name="code"
								defaultValue={this.props.values['code']}
								onChange={this.props.handleChange}
								style={{ borderLeft: 'none', width: '85px' }}
							>
								{this.props.countryCodes.map(c => {
									return (
										<option value={c.value} key={c.value}>
											{c.label}
										</option>
									);
								})}
							</Select>
						</InputAdornment>
					),
				}}
			/>
		);
	}
}
