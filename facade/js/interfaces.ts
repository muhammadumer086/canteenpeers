/**
 * Interface for a situation
 */
export interface IApiSituation {
	id: number;
	slug: string;
	name: string;
	cancer_type: string | null;
	main_slug: string;
	parent_situation: IApiSituation | null;
	date?: string | null;
}

/**
 * Interface for a date
 */
export interface IApiDate {
	date: string;
	timezone_type: number;
	timezone: string;
}

/**
 * Interface for the pagination meta
 */
export interface IApiPagination {
	current_page: number;
	from: number;
	last_page: number;
	path: string;
	per_page: number;
	to: number;
	total: number;
}

/**
 * Interface for a discussion
 */
export interface IApiDiscussion {
	id: number;
	slug: string | null;
	title: string | null;
	content: string;
	reply_to: IApiDiscussion | null;
	main_discussion: IApiDiscussion | null;
	main_discussion_id: number | null;
	latest_replies?: IApiDiscussion[];
	created_at: IApiDate;
	updated_at: IApiDate;
	last_activity_at: IApiDate | null;
	private: boolean;
	closed: boolean | null;
	situations: IApiSituation[];
	topic: IApiTopic;
	likes_count: number;
	hugs_count: number;
	support_count: number;
	replies_count: number;
	direct_replies_count: number;
	thread_count: number;
	followed: boolean;
	liked: boolean;
	hugged: boolean;
	user: IApiUser;
	user_likes: IApiUser[];
	user_hugs: IApiUser[];
	user_replies: IApiUser[];
	discussion_index: number;
	reports_count?: number;
	deleted_at?: IApiDate;
	views_count?: number;
	age_sensitive: boolean;
	for_event?: number;
}

/**
 * Interface for a discussion support
 */
export interface IApiSupport {
	reply: boolean;
	hug: boolean;
	like: boolean;
	user: IApiUser;
	discussion: IApiDiscussion;
}

export interface IApiEvent {
	id: number;
	title: string;
	slug: string;
	description: string;
	situations: IApiSituation[];
	latitude: string;
	longitude: string;
	location: string;
	state?: IApiState;
	email_address?: string;
	created_at: IApiDate;
	start_date: IApiDate;
	end_date: IApiDate;
	feature_image: IImage | null;
	registered: boolean;
	registered_users_count?: number;
	registered_users_count_sos?: number;
	sos_url?: string | null;
	event_type?: string;
	event_admin?: number;
	event_time_zone?: string;
	is_travel_cover?: boolean;
	max_age?: number;
	min_age?: number;
	promo_video_url?: string;
	max_participents?: number;
	last_joining_date?: IApiDate;
	registered_users?: { users: IApiUser[] };
	discussion_id?: string;
	user_media?: IImage[];
	show_rsvp?: number;
	topic_id?: number;
}

export interface IApiLink {
	id: number;
	subtitle: string;
	title: string;
	feature_image: IImage;
	url: IApiUrlData;
}

export interface IApiUrlData {
	type: 'custom' | 'link';
	data: string | EStaticLinkType;
}

export interface IApiState {
	id: number;
	name: string;
	abbreviation: string;
	slug: string;
}

export interface IReport {
	id: number;
	title: string;
	comment: string | null;
	created_at: IApiDate;
	updated_at: IApiDate;
	user: IApiUser;
	age_sensitive: boolean;
}

export interface IStaticStates {
	label: string;
	value: string;
}

export interface IStaticAgeRange {
	label: string;
	value: string;
}

/**
 * Interface for a discussion report
 */
export interface IApiDiscussionReport extends IReport {
	discussion: IApiDiscussion;
	reports: IReport[];
}

/**
 * Interface for a topic
 */
export interface IApiTopic {
	id: number;
	slug: string;
	title: string;
	category: string;
	is_user_topic: boolean;
	situations: IApiSituation[];
}

export interface IApiAgeRange {
	id: number;
	slug: string;
	min: number;
	max: number;
	name: string;
}

export interface ISeo {
	title: string;
	description: string;
	image: string;
}

/**
 * Interface for a user
 */
export interface IApiToken {
	token_type: string;
	expires_in: number;
	access_token: string;
	refresh_token: string;
}

/**
 * Interface for a user
 */
export interface IApiUser {
	id?: number;
	email?: string;
	first_name?: string;
	last_name?: string;
	full_name: string;
	username: string;
	avatar_url: string;
	location?: string;
	location_data?: any;
	state?: string;
	country_slug?: string;
	phone?: string;
	dob?: string;
	role_names?: string[];
	is_banned: boolean;
	is_blocked?: boolean;
	allow_direct_messages?: boolean;
	ban?: number;
	updated_at: IApiDate | null;
	gender?: string;
	self_identify?: string | null;
	situations?: IApiSituation[];
	blogs_count?: number;
	active_discussions_count?: number;
	likes_count?: number;
	hugs_count?: number;
	following_count?: number;
	saved_resources_count?: number;
	messages_count?: number;
	about?: string;
	indigenous_australian?: boolean | null;
	country_of_birth?: IApiCountry | null;
	languages?: IApiLanguage[];
	heard_abouts?: IApiHeardAbout[];
	personal_settings?: IApiPersonalSetting[];
	intercom_hash?: string;
	last_login?: string;
	total_logins?: number;
	number_children?: number;
	underage?: boolean;
	guardian_details?: IApiGuardian;
	age?: number;
	inactive?: boolean;
	has_conversations?: boolean;
	is_graduate?: number;
	verified?: boolean;
	is_mobile_verified?: boolean;
}

/**
 * Interface for a guardian
 */
export interface IApiGuardian {
	email: string;
	name: string;
}

export interface IApiStaff {
	id: number;
	first_name: string;
	last_name: string;
	role: string;
	ordr: number;
	feature_image: IImage;
	image_id: string;
	biography: string;
	team: IApiTeam;
}

export interface IApiTeam {
	id: number;
	name: string;
	slug: string;
	order: number;
}

export interface IApiUserActivity {
	id: string;
	user: IApiUser | null;
	type: string;
	content: any | null;
	activity_model_name: string | null;
	activity: IApiUser | IApiDiscussion | IApiResource | IApiBlog;
	platform: string;
	created_at: IApiDate;
	updated_at: IApiDate;
}

export interface IApiConversationDetail {
	id: number;
	name: string;
	group_conversation: boolean;
	created_at: IApiDate;
	updated_at: IApiDate;
	users: IApiUser[];
	last_message: IApiMessage;
}

export interface IApiMessage {
	id: number;
	message: string;
	system_generated: boolean;
	updated_at: IApiDate;
	user: IApiUser;
	target_users: IApiUser[];
}

/**
 * Interface for a country
 */
export interface IApiCountry {
	slug: string;
	name: string;
}

/**
 * Interface for a language
 */
export interface IApiLanguage {
	slug: string;
	name: string;
}

/**
 * Interface for heard about items
 */
export interface IApiHeardAbout {
	slug: string;
	name: string;
}

/**
 * Interface for personal settings
 */
export interface IApiPersonalSetting {
	slug: string;
	name: string;
	category_slug: string;
	category: string;
}

/**
 * Interface for app settings
 */
export interface IApiSettings {
	config: 'private' | 'public';
	search_index: string;
	algolia_app_id: string;
	algolia_search_key: string;
}

/**
 * Interface for the dashboard data
 */
export interface IApiDashboard {
	// TODO: to be complted once data available
	announcement: IApiAnnouncement | null;
	discussions: IApiDiscussion[];
	resources: IApiResource[];
	blogs: IApiBlog[];
	events: IApiEvent[];
	links: IApiLink[];
}

/**
 * Interface for the Announcement data
 */
export interface IApiAnnouncement {
	id: number;
	user_id: number;
	message: string;
	created_at: IApiDate;
	updated_at: IApiDate;
	title: string;
	feature_image: IImage | null;
	url: {
		href: string;
		hrefAs: string;
		type: string;
	} | null;
	url_label: string;
	situations: IApiSituation[];
	states: IApiState[];
	age_ranges: IApiAgeRange[];
	public: boolean;
}

export interface IStaticStates {
	label: string;
	value: string;
}

/**
 * Interface for the blog post data
 */
export interface IApiBlog {
	id: number;
	title: string;
	slug: string;
	content: string;
	user_id: string;
	source: string;
	prismic_id: string;
	prismic_author?: string;
	prismic_author_image?: string;
	first_publication_date: IApiDate;
	last_publication_date: IApiDate;
	created_at: IApiDate;
	updated_at: IApiDate;
	feature_image: IImage | null;
	topic_id: any;
	user: IApiUser;
	topic: IApiTopic;
	situations: IApiSituation[];
	deleted_at?: IApiDate;
	views_count?: number;
	age_sensitive: boolean;
	private: boolean;
	seo: ISeo;
}

/**
 * Interface describing a blog image
 */
export interface IApiBlogImage {
	id: string;
	image_url: string;
	width: number;
	height: number;
	created_at: IApiDate;
	updated_at: IApiDate;
}

export interface IApiResourceResponse {
	current_page: number;
	data: IApiResource[];
	first_page_url: string;
	from: number;
	last_page: number;
	last_page_url: string;
	next_page_url: string | null;
	path: string;
	per_page: number;
	prev_page_url: string | null;
	to: number;
	total: number;
}

/**
 * Interface for the Resources data
 */
export interface IApiResource {
	id: number;
	title: string;
	slug: string;
	content: string;
	prismic_id: string;
	first_publication_date: IApiDate;
	last_publication_date: IApiDate;
	created_at: IApiDate;
	updated_at: IApiDate;
	feature_image: IImage;
	situations: IApiSituation[];
	topic_id: any;
	topic: IApiTopic;
	saved: boolean;
	source: string;
	attachment: string;
	user: IApiUser | null;
	deleted_at?: IApiDate;
	views_count?: number;
	is_ycs: boolean;
	seo: ISeo;
}

/**
 * Interface for the dashboard data
 */
export interface IApiTopic {
	id: number;
	title: string;
	slug: string;
	category: string;
	is_user_topic: boolean;
	situations: IApiSituation[];
}

/**
 * Interface for a notification
 */
export interface IApiNotification {
	id: string;
	type: string;
	notifiable_type: string;
	notifiable_id: number;
	data: any;
	read_at: IApiDate | null;
	created_at: IApiDate;
	updated_at: IApiDate;
}

export interface ISelectOption {
	label: string;
	value: string;
	tooltip?: string;
}

export interface ISituationOption {
	slug: string;
	name: string;
	cancer_type: string | null;
	date: string | null;
}

/**
 * Interface for a reply
 */
export interface IReplyTo {
	id: number;
	user: string;
}

/**
 * Interface for a image dimensions
 */
export interface IDimension {
	width: number;
	height: number;
}

/**
 * Interface for a featured images
 */
export interface IImage {
	dimensions: IDimension;
	alt: string | null;
	copyright: string | null;
	url: string | null;
	title?: string;
}

/**
 * Interface for the link object
 */
export interface ILinkData {
	prefix?: string;
	href: string;
	as: string;
}

/**
 * Interface for the image dimensions
 */
export interface IDimension {
	width: number;
	height: number;
}

export interface IRelevantMeta {
	icon: string;
	title: string;
	linkLabel: string;
	linkData: ILinkData;
}

export interface IRelevantContent {
	id: number;
	title: string;
	slug: string;
	content: string;
	user_id: string;
	source: string;
	prismic_id: string;
	first_publication_date: IApiDate;
	last_publication_date: IApiDate;
	created_at: IApiDate;
	updated_at: IApiDate;
	feature_image: IImage;
	topic_id: number | null;
	user: IApiUser;
	topic: IApiTopic;
}

export interface IRelevantData {
	relevantMeta: IRelevantMeta;
	relevantContent: IRelevantContent[];
}

export interface IContentTileData {
	type?: 'blog' | 'resource';
	imageUrl: string;
	id: number;
	title: string;
	topic: IApiTopic | null;
	linkPrefix: string;
	slug: string;
	source: string;
	user: IApiUser | null;
}

export enum EContactModalTypes {
	GENERAL = 'General Enquiry',
	SUGGEST_TOPIC = 'Suggest a New Topic',
	REPORT_PROBLEM = 'ReportProblem',
	SUPPORT_FOR_YOUNG_PEOPLE = 'Support for Young People',
	ABOUT_CANTEEN_CONNECT = 'About Canteen Connect',
	DONATIONS_OR_OTHER_ENQUIRIES = 'Donations or other Enquiries',
	I_AM_FROM_AOTEAROA_NEW_ZEALAND = 'I’m from Aotearoa / New Zealand',
}

export enum EStaticLinkType {
	OPEN_COUNSELLOR_CHAT = 'OPEN_COUNSELLOR_CHAT',
	OPEN_NEW_BLOG_MODAL = 'OPEN_NEW_BLOG_MODAL',
	OPEN_NEW_DISCUSSION_MODAL = 'OPEN_NEW_DISCUSSION_MODAL',
}

export interface IApiGIFObject {
	type: string;
	id: string;
	slug: string;
	url: string;
	bitly_gif_url: string;
	bitly_url: string;
	embed_url: string;
	username: string;
	source: string;
	rating: string;
	caption: string;
	content_url: string;
	source_tld: string;
	source_post_url: string;
	import_datetime: string;
	trending_datetime: string;
	images: ImageObject;
	meta: MetaObject;
}

export interface ImageObject {
	fixed_height: {
		url: string;
		width: string;
		height: string;
		size: string;
		mp4: string;
		mp4_size: string;
		webp: string;
		webp_size: string;
	};
	fixed_height_still: {
		url: string;
		width: string;
		height: string;
	};
	fixed_height_downsampled: {
		url: string;
		width: string;
		height: string;
		size: string;
		webp: string;
		webp_size: string;
	};
	fixed_width: {
		url: string;
		width: string;
		height: string;
		size: string;
		mp4: string;
		mp4_size: string;
		webp: string;
		webp_size: string;
	};
	fixed_width_still: {
		url: string;
		width: string;
		height: string;
	};
	fixed_width_downsampled: {
		url: string;
		width: string;
		height: string;
		size: string;
		webp: string;
		webp_size: string;
	};
	fixed_height_small: {
		url: string;
		width: string;
		height: string;
		size: string;
		webp: string;
		webp_size: string;
	};
	fixed_height_small_still: {
		url: string;
		width: string;
		height: string;
	};
	fixed_width_small: {
		url: string;
		width: string;
		height: string;
		size: string;
		webp: string;
		webp_size: string;
	};
	fixed_width_small_still: {
		url: string;
		width: string;
		height: string;
	};
	downsized: {
		url: string;
		width: string;
		height: string;
		size: string;
	};
	downsized_still: {
		url: string;
		width: string;
		height: string;
	};
	downsized_large: {
		url: string;
		width: string;
		height: string;
		size: string;
	};
	original: {
		url: string;
		width: string;
		height: string;
		size: string;
		frames: string;
		mp4: string;
		mp4_size: string;
		webp: string;
		webp_size: string;
	};
	original_still: {
		url: string;
		width: string;
		height: string;
	};
}

export interface MetaObject {
	msg: string;
	status: number;
	response_id: string;
}

export type FilterOrder = 'ASC' | 'DESC';

export interface IApiPermission {
	id: number;
	name: string;
	guard_name: string;
	created_at: string;
	updated_at: string;
}

export interface IApiRole {
	id: number;
	name: string;
	guard_name: string;
	created_at: string;
	updated_at: string;
}
