import React from 'react';
import { connect } from 'react-redux';
import ClearIcon from '@material-ui/icons/Clear';

import { IStoreState, apiErrorDismiss } from '../../redux/store';

interface IProps {
	apiError: string[];
	apiErrorShowing: boolean;
	dispatch(action: any): void;
}

const ApiErrorBanner: React.FC<IProps> = (props: IProps) => {
	const classes = ['api_banner api_banner--error'];

	if (props.apiErrorShowing) {
		classes.push('api_banner--active');
	}

	function dismiss() {
		props.dispatch(apiErrorDismiss());
	}

	return (
		<div className="api_banner_wrapper">
			<div className={classes.join(' ')}>
				<div className="api_banner-content">
					{props.apiError.map((error, index) => {
						if (typeof error === 'string') {
							return (
								<p
									key={index}
									dangerouslySetInnerHTML={{ __html: error }}
								/>
							);
						} else {
							console.log('Error not displayed in banner', error);
						}
					})}
				</div>
				<button
					onClick={dismiss}
					className="close_button"
					title="Dismiss"
				>
					<ClearIcon className="close_button-icon" />
				</button>
			</div>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { apiError, apiErrorShowing } = state.apiError;
	return {
		apiError,
		apiErrorShowing,
	};
}
export default connect(mapStateToProps)(ApiErrorBanner);
