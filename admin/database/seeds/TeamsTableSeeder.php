<?php

use Illuminate\Database\Seeder;
use App\Team;

class TeamsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$teams = [
			[
				'name' => 'Our Online Team',
				'slug' => 'online',
				'order' => 0,
			],
			[
				'name' => 'National Team',
				'slug' => 'national',
				'order' => 1,
			],
			[
				'name' => 'NSW Team',
				'slug' => 'nsw',
				'order' => 2,
			],
			[
				'name' => 'QLD Team',
				'slug' => 'qld',
				'order' => 3,
			],
			[
				'name' => 'VIC Team',
				'slug' => 'vic',
				'order' => 4,
			],
			[
				'name' => 'SA Team',
				'slug' => 'sa',
				'order' => 5,
			],
			[
				'name' => 'ACT Team',
				'slug' => 'act',
				'order' => 6,
			],
			[
				'name' => 'WA Team',
				'slug' => 'wa',
				'order' => 7,
			],
			[
				'name' => 'NT Team',
				'slug' => 'nt',
				'order' => 8,
			],
			[
				'name' => 'TAS Team',
				'slug' => 'tas',
				'order' => 9,
			],
			[
				'name' => 'NZ Team',
				'slug' => 'nz',
				'order' => 10,
			],
		];

		foreach ($teams as $team) {
			if (!Team::where('slug', '=', $team['slug'])->exists()) {
				Team::create([
					'slug' => $team['slug'],
					'name' => $team['name'],
					'order' => $team['order'],
				]);
			}
		}
	}
}
