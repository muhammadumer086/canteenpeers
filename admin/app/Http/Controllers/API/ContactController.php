<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\Contact;
use App\Notifications\ContactUsNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Validator;

class ContactController extends Controller {
	/**
	 * Create a new contact message
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
			'type' => 'bail|string',
			'message' => 'bail|required|string|max:16000000',
			'email' => 'bail|required|string|max:254',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$type = $request->input('type');
		$message = $request->input('message');
	    $email = $request->input('email');

		//GENERAL = 'General Enquiry',
		//SUGGEST_TOPIC = 'Suggest a New Topic',
		//REPORT_PROBLEM = 'ReportProblem',
		//SUPPORT_FOR_YOUNG_PEOPLE = 'Support for Young People',
		//ABOUT_CANTEEN_CONNECT = 'About Canteen Connect',
		//DONATIONS_OR_OTHER_ENQUIRIES = 'Donations or other Enquiries',
		//I_AM_FROM_AOTEAROA_NEW_ZEALAND = 'I’m from Aotearoa / New Zealand',


		//'address' => env('MAIL_FROM_ADDRESS', 'online@canteen.org.au'),
		//		'addressSupport' => env('MAIL_FROM_ADDRESS', 'support@canteen.org.au'),
		//		'addressProblem' => env('MAIL_FROM_ADDRESS', 'digitech@canteen.org.au'),
		//		'addressTopic' => env('MAIL_FROM_ADDRESS', 'online@canteen.org.au'),
		//		'addressAboutCanteenConnect' => env('MAIL_FROM_ADDRESS', 'online@canteen.org.au'),
		//		'addressDonation' => env('MAIL_FROM_ADDRESS', 'candofamily@canteen.org.au'),
		//		'addressNZ' => env('MAIL_FROM_ADDRESS', 'info@canteen.org.nz'),

		if($type == 'General Enquiry')
		{
			$result = Notification::route('mail', config('mail.from.addressSupport'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else if($type == 'Suggest a New Topic')
		{
			$result = Notification::route('mail', config('mail.from.addressTopic'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else if($type == 'ReportProblem')
		{
			$result = Notification::route('mail', config('mail.from.addressProblem'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else if($type == 'Support for Young People')
		{
			$result = Notification::route('mail', config('mail.from.addressSupport'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else if($type == 'About Canteen Connect')
		{
			$result = Notification::route('mail', config('mail.from.addressAboutCanteenConnect'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else if($type == 'Donations or other Enquiries')
		{
			$result = Notification::route('mail', config('mail.from.addressDonation'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else if($type == "I’m from Aotearoa / New Zealand")
		{
			$result = Notification::route('mail', config('mail.from.addressNZ'))
				->notify(new ContactUsNotification($type, $message, $email));
		}
		else
		{
			$result = Notification::route('mail', config('mail.from.address'))
				->notify(new ContactUsNotification($type, $message, $email));
		}

		return response()->json([
			'success' => true,
		]);
	}

	public function reportProblem(Request $request) {
		$validator = Validator::make($request->all(), [
			'message' => 'bail|required|string|max:16000000',
			'email' => 'bail|required|string|max:254',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}
		$type = $request->input('type');
		$message = $request->input('message');
	    $email = $request->input('email');
		
		$result = Notification::route('mail', config('mail.from.email-problem'))
			->notify(new ContactUsNotification($type, $message, $email));

		return response()->json([
			'success' => true,
		]);
	}

}
