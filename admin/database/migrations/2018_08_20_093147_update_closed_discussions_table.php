<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateClosedDiscussionsTable extends Migration {
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::table('discussions', function ($table) {
			$table->boolean('closed')->nullable()->change();
		});

	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down() {
		Schema::table('discussions', function ($table) {
			$table->unsignedInteger('closed')->nullable()->change();
		});
	}
}
