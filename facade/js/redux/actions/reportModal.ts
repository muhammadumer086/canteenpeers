import { IStoreState } from '../store';
import { actionTypes } from '../actions';

import { IApiDiscussion } from '../../interfaces';
import { EReportModalType } from '../reducers/reportModal';

export const reportModalDisplay = (
	discussion: IApiDiscussion,
	modalType: EReportModalType = EReportModalType.HARMFUL,
) => (dispatch, getState: () => IStoreState) => {
	if (getState().reportModal.modalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.REPORT_MODAL_DISPLAY,
		discussion,
		modalType,
	});
};

export const reportModalHide = () => (
	dispatch,
	getState: () => IStoreState,
) => {
	if (!getState().reportModal.modalDisplay) {
		return;
	}

	return dispatch({
		type: actionTypes.REPORT_MODAL_HIDE,
	});
};
