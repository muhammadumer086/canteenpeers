import React from 'react';

import Tooltip from '@material-ui/core/Tooltip';

import StarIcon from '@material-ui/icons/Stars';

import { IApiUser } from '../../interfaces';

interface IProps {
	user: IApiUser | null;
}

const UserAvatar: React.FC<IProps> = (props: IProps) => {

	let isBlocked = false;
	let isAdmin = false;
	let styles = {}
	let classes = ['user_avatar']

		if (
			props.user &&
			typeof props.user.is_blocked !== 'undefined' &&
			'is_blocked' in props.user &&
			props.user.is_blocked &&
			!!props.user.is_blocked
		) {
			isBlocked = true
		}

		if (
			props.user &&
			typeof props.user.role_names !== 'undefined' &&
			'is_blocked' in props.user &&
			!!props.user.role_names.find((roleName) => {
				return (roleName === 'admin')
			})
		) {
			isAdmin = true
		}

		if (
			props.user &&
			typeof props.user.avatar_url !== 'undefined' &&
			'avatar_url' in props.user &&
			!!props.user.avatar_url
		) {
			styles = {
				backgroundImage: `url(${props.user.avatar_url})`
			}
		}

		if (isBlocked) {
			classes.push('user_avatar--blocked')
		}

	return (
		<div className={classes.join(' ')}>
			<div
				className="user_avatar-avatar"
				style={styles}
			/>
			{
				isAdmin &&
				<div className="user_avatar-icon">
					<Tooltip title="Admin User" placement="top">
						<StarIcon className="user_avatar-icon-content"/>
					</Tooltip>
				</div>
			}
		</div>
	);
}

export default UserAvatar;
