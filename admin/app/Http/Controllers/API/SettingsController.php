<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Language;
use App\Http\Resources\Language as LanguageResource;

class SettingsController extends Controller {

	use \App\Traits\AlgoliaIndex;

	/**
	 * Display a listing of the resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);

		// Public config
		$config = [
			'config'             => 'public',
			'search_index'       => $this->publicAlgoliaIndex(),
			'algolia_app_id'     => config('canteen.algolia_app_id'),
			'algolia_search_key' => config('canteen.algolia_search_key'),
		];

		if ($authUser) {
			$config['config'] = 'private';
			$config['search_index'] = $this->privateAlgoliaIndex();
		}

		return response()->json($config);
	}
}
