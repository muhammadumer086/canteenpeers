<?php

namespace App\Notifications;

use App\Announcement;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\FcmNotification;

class NewAnnouncementNotification extends Notification {
	use Queueable;

	protected $announcement;
	protected $user;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Announcement $announcement, $user) {
		$this->announcement = $announcement;
		$this->user = $user;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable) {
		$result = [];
		if ($this->user->is_deleted || $this->user->inactive) {
			return $result;
		}
		if ($this->user->allowEmailNewAnnouncement()) {
			$result[] = 'mail';
		}
		if ($this->user->allowNotificationNewAnnouncement()) {
			$result[] = 'database';
		}
		if ($this->user->allowPushNotificationNewAnnouncement()) {
			$result[] = FcmChannel::class;
		}
		return $result;
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		$title = "Canteen posted a new announcement {$this->announcement->title}";
		return (new MailMessage())
			->subject($title)
			->view('emails.announcement-new', [
				'user' => $this->user,
				'announcement' => $this->announcement,
			]);
	}

	public function toFcm($notifiable) {
		$title = "Canteen posted a new announcement {$this->announcement->title}";
		// The FcmNotification holds the notification parameters
		$fcmNotification = FcmNotification::create()
			->setTitle($title)
			->setBody($title);

		// The FcmMessage contains other options for the notification
		return FcmMessage::create()
			->setPriority(FcmMessage::PRIORITY_HIGH)
			->setTimeToLive(86400)
			->setNotification($fcmNotification);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable) {
		return [
			'announcement' => $this->announcement,
		];
	}
}
