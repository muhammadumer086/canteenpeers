<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class Mention extends JsonResource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();
		if (
			$apiStore->hasApiResource(
				'mention',
				$this->id,
				$this->updated_at,
				false
			)
		) {
			return $apiStore->getApiResource(
				'mention',
				$this->id,
				$this->updated_at,
				false
			);
		}

		$params = ['mentioned_username'];

		$result = [];

		// Add the default values
		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		$result['mentioned'] = new UserResource(
			$this->mentioned()
				->withTrashed()
				->first()
		);

		$result['sender'] = new UserResource(
			$this->sender()
				->withTrashed()
				->first()
		);

		$apiStore->setApiResource(
			$result,
			'mention',
			$this->id,
			$this->updated_at,
			false
		);

		return $result;
	}
}
