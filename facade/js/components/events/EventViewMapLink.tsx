import React from 'react';

interface IProps {
	position: {
		lat: string;
		lng: string;
	};
}

const EventViewMapLink: React.StatelessComponent<IProps> = props => {
	return (
		<div>
			<span className="event_single-sidebar-text">
				<a
					data-latitude={props.position.lat}
					data-longitude={props.position.lng}
					title="View map"
					target="_blank"
					className="theme-title event_single-sidebar-link event_single-sidebar-text"
					href={`https://google.com/maps/place/${
						props.position.lat
					},${props.position.lng}`}
				>
					View map
				</a>
			</span>
		</div>
	);
};

export default EventViewMapLink;
