<?php

namespace App\Filters\User;

use App\Filters\FilterAbstract;
use Illuminate\Database\Eloquent\Builder;

class NameFilter extends FilterAbstract {
	/**
	 * Mappings for database values.
	 *
	 * @return array
	 */
	public function mappings() {
		return [];
	}

	/**
	 * Filter with builder queries.
	 *
	 * @param  string $access
	 * @return Illuminate\Database\Eloquent\Builder
	 */
	public function filter(Builder $builder, $value) {
		// Explode the value by space
		$explodedValue = explode(' ', preg_replace('/\s+/', ' ', $value));
		if (sizeof($explodedValue)) {
			foreach ($explodedValue as $singleValue) {
				$builder = $builder
					->where('first_name', 'LIKE', '%' . $singleValue . '%')
					->orWhere('last_name', 'LIKE', '%' . $singleValue . '%');
			}
		}
		return $builder;
	}
}
