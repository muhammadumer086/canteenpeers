import React from 'react';
import { connect } from 'react-redux';
import dotProp from 'dot-prop-immutable';
import TimeAgo from 'react-timeago';

import ButtonBase from '@material-ui/core/ButtonBase';
import Icon from '@material-ui/core/Icon';

import { IStoreState, userPanelDisplay, apiError } from '../../redux/store';

import { IApiDiscussion, IApiUser } from '../../interfaces';

import { ApiService, withServices } from '../../services';

import { getExcerpt } from '../../helpers/getExcerpt';

import DiscussionFollow from './DiscussionFollow';
import DiscussionGetIt from './DiscussionGetIt';
import DiscussionReplyCount from './DiscussionReplyCount';
import DiscussionReplyAction from './DiscussionReplyAction';
import DiscussionReplyName from './DiscussionReplyName';
import DiscussionReport from './DiscussionReport';
import Pill from '../general/Pill';
import UserAvatar from '../general/UserAvatar';
import TileTitle from '../general/TileTitle';
import { ButtonBaseLink } from '../general/ButtonBaseLink';
import { generateAndFormatTimezoneDate } from '../../../js/helpers/generateTimezoneDate';
import { DiscussionSupportMeta } from './DiscussionSupportMeta';

interface IProps {
	discussion: IApiDiscussion;
	disabled?: boolean;
	noActions?: boolean;
	noLink?: boolean;
	hideGetIt?: boolean;
	hideReplyCount?: boolean;
	hideReport?: boolean;
	withRepliedTo?: string;
	withReplyAction: string;
	withRepliesList?: boolean;
	userAuthenticated: boolean;
	userData: IApiUser;
	isNarrow?: boolean;
	apiService: ApiService;
	multiline?: boolean;
	useLastActivity?: boolean;
	discussionReplyData?: IApiDiscussion;
	openReply?(event, replyTo?): Promise<boolean>;
	dispatch(action: any): void;
	scrollToReplies(): void;
	editDiscussion?(discussion: IApiDiscussion): void;
	deleteDiscussion?(discussion: IApiDiscussion | null): void;
}

interface IState {
	repliesLoading: boolean;
	currentReplyPage: number;
	numberReplies: number;
	loadedReplies: IApiDiscussion[];
	deleted: boolean;
	deletedReplies: number[];
}

class DiscussionTile extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			repliesLoading: false,
			currentReplyPage: 1,
			numberReplies: props.discussion.replies_count,
			loadedReplies: props.discussion.latest_replies
				? this.sortReplies(props.discussion.latest_replies)
				: [],
			deleted: false,
			deletedReplies: [],
		};

		this.handleReply = this.handleReply.bind(this);
		this.handleDeleteDiscussion = this.handleDeleteDiscussion.bind(this);
		this.handleDeleteDiscussionReply = this.handleDeleteDiscussionReply.bind(
			this,
		);
	}

	public render() {
		if (this.state.deleted) {
			return null;
		}

		const formatTimezoneDate = generateAndFormatTimezoneDate(
			this.props.useLastActivity
				? this.props.discussion.last_activity_at
					? this.props.discussion.last_activity_at.date
					: this.props.discussion.updated_at.date
				: this.props.discussion.created_at.date,
		);

		const excerpt = getExcerpt(this.props.discussion.content);

		// console.log('content', this.props.discussion.content);
		// console.log('excerpt', excerpt);

		const filteredHastagExcept = excerpt.replace(
			/(^|\s)(#[a-z\d-]+)/gi,
			"$1<span class='blot-preview'>$2</span>",
		);

		const filteredMentionExcerpt = filteredHastagExcept.replace(
			/(^|\s)(@[a-z\d-]+)/gi,
			"$1<span class='blot-preview'>$2</span>",
		);

		const finalExcerpt = filteredMentionExcerpt;

		const classNames = ['discussion_tile', 'theme--main'];

		const replyDetails = {
			id: this.props.discussion.id,
			user: this.props.discussion.user.full_name,
		};

		if (this.props.disabled) {
			classNames.push('discussion_tile--disabled');
		}

		if (this.props.noActions) {
			classNames.push('discussion_tile--no_actions');
		}

		if (this.props.isNarrow) {
			classNames.push('discussion_tile--narrow');
		}

		const discussionLinkId = this.props.discussion.main_discussion
			? this.props.discussion.main_discussion.id
			: this.props.discussion.slug;
		const discussionIndex = this.props.discussion.main_discussion
			? `#${this.props.discussion.discussion_index}`
			: '';

		const markdownClasses = ['markdown'];

		if (!this.props.multiline) {
			markdownClasses.push('markdown--inline');
		}
		return (
			<div className={classNames.join(' ')}>
				<div className="discussion_tile-author">
					<ButtonBase
						className="discussion_tile-author-button"
						focusRipple={true}
						onClick={() => {
							this.props.dispatch(
								userPanelDisplay(this.props.discussion.user),
							);
						}}
						disabled={this.props.disabled}
					/>
					<div className="discussion_tile-author-content">
						<div className="discussion_tile-author-avatar">
							<UserAvatar user={this.props.discussion.user} />
						</div>
						<div className="discussion_tile-author-text font--small">
							<div className="discussion_tile-author-name">
								<span className="discussion_tile-author-name-link theme-title">
									{this.props.discussion.user.full_name}
								</span>
							</div>
							{!!this.props.withRepliedTo &&
							this.props.discussion.replies_count > 0 &&
							this.props.discussionReplyData ? (
								<div />
							) : (
								<div className="discussion_tile-author-time">
									{/*
													!!this.props.useLastActivity &&
													<span className="discussion_tile-author-time-label">Last activity:&nbsp;</span>
												*/}
									<span className="discussion_tile-author-time-label theme-text--body_light">
										{this.props.useLastActivity && (
											<span className="discussion_tile-author-time-label-part">
												Last activity:&nbsp;
											</span>
										)}
										<span className="discussion_tile-author-time-label-part">
											<TimeAgo
												date={formatTimezoneDate}
											/>
										</span>
									</span>
								</div>
							)}
						</div>
						{this.props.userAuthenticated &&
							!this.props.noActions &&
							!this.props.hideReport &&
							this.props.isNarrow && (
								<div className="discussion_tile-author-report">
									<DiscussionReport
										discussion={this.props.discussion}
										handleDelete={
											this.handleDeleteDiscussion
										}
										canEdit={false}
									/>
								</div>
							)}
					</div>
				</div>
				<div className="discussion_tile-content">
					{!this.props.noLink && (
						<ButtonBaseLink
							focusRipple={true}
							className="discussion_tile-link"
							disabled={this.props.disabled}
							hrefAs={`/discussions/${discussionLinkId}${discussionIndex}`}
							href={`/discussions-single?id=${discussionLinkId}`}
						/>
					)}
					{!!this.props.discussion.topic && (
						<div className="hm-b8 discussion_tile-content-topic">
							<Pill
								withoutBorder={true}
								label={this.props.discussion.topic.title}
								active={true}
								disabled={false}
								clickable={true}
								href={`/discussions?topic=${this.props.discussion.topic.slug}`}
							/>
						</div>
					)}
					{!!this.props.discussion.title && (
						<TileTitle
							content={this.props.discussion.title}
							clamp={this.props.isNarrow}
							clampLines={3}
						/>
					)}

					{this.props.discussionReplyData ? (
						<div />
					) : (
						<div className="discussion_tile-content-content font--body">
							{this.props.discussion.reply_to &&
								this.props.discussion.reply_to.reply_to && (
									<DiscussionReplyName>
										{
											this.props.discussion.reply_to.user
												.full_name
										}
									</DiscussionReplyName>
								)}

							<div
								className={markdownClasses.join(' ')}
								dangerouslySetInnerHTML={{
									__html: this.props.multiline
										? this.props.discussion.content
										: finalExcerpt,
								}}
							/>
						</div>
					)}

					{!!this.props.discussion.reports_count && (
						<div className="discussion_tile-content-report hm-t16">
							<Icon>warning</Icon>
							{this.props.discussion.reports_count > 1 ? (
								<span>
									Post reported{' '}
									{this.props.discussion.reports_count} times
								</span>
							) : (
								<span>Post reported once</span>
							)}
						</div>
					)}

					{!this.props.hideGetIt && (
						<div className="discussion_tile-content-discussion_support_meta">
							<DiscussionSupportMeta
								discussion={this.props.discussion}
							/>
						</div>
					)}

					<div className="discussion_tile-content-meta">
						{!this.props.hideGetIt && (
							<React.Fragment>
								<div className="discussion_tile-content-meta-item">
									<DiscussionGetIt
										type="iGetIt"
										count={
											this.props.discussion.likes_count
										}
										discussionId={this.props.discussion.id}
										discussionTitle={
											this.props.discussion.title
										}
										liked={this.props.discussion.liked}
										disabled={!this.props.userAuthenticated}
										apiService={this.props.apiService}
									/>
								</div>
								<div className="discussion_tile-content-meta-item">
									<DiscussionGetIt
										type="hug"
										count={this.props.discussion.hugs_count}
										discussionId={this.props.discussion.id}
										discussionTitle={
											this.props.discussion.title
										}
										liked={this.props.discussion.hugged}
										disabled={!this.props.userAuthenticated}
										apiService={this.props.apiService}
									/>
								</div>
							</React.Fragment>
						)}

						{this.props.discussion.replies_count > 0 &&
							!this.props.hideReplyCount && (
								<div className="discussion_tile-content-meta-item">
									<ButtonBaseLink
										focusRipple={true}
										hrefAs={`/discussions/${this.props.discussion.slug}#replies`}
										href={`/discussions-single?id=${this.props.discussion.slug}#replies`}
									>
										<DiscussionReplyCount
											count={
												this.props.discussion
													.replies_count
											}
											userAuthenticated={
												this.props.userAuthenticated
											}
											scrollToReplies={
												this.props.scrollToReplies
											}
										/>
									</ButtonBaseLink>
								</div>
							)}
						{!!this.props.withReplyAction &&
							this.props.userAuthenticated && (
								<div className="discussion_tile-content-meta-item">
									<DiscussionReplyAction
										replyTo={replyDetails}
										action={this.handleReply}
										count={
											this.props.discussion
												.direct_replies_count
										}
									/>
								</div>
							)}
						{this.props.userAuthenticated &&
							this.props.noActions &&
							!this.props.hideReport && (
								<div className="discussion_tile-content-meta-item">
									<DiscussionReport
										discussion={this.props.discussion}
										handleDelete={
											this.handleDeleteDiscussion
										}
										editDiscussion={
											!!this.props.editDiscussion
												? () => {
														this.props.editDiscussion(
															this.props
																.discussion,
														);
												  }
												: undefined
										}
										deleteDiscussion={
											!!this.props.deleteDiscussion
												? () => {
														this.props.deleteDiscussion(
															this.props
																.discussion,
														);
												  }
												: undefined
										}
										canEdit={true}
									/>
								</div>
							)}
					</div>
					{this.props.withReplyAction &&
						this.state.numberReplies > 0 && (
							<section className="discussion_tile-replies">
								<header className="discussion_tile-replies-header">
									<h3 className="discussion_tile-replies-title">
										{this.state.numberReplies === 1 && (
											<React.Fragment>
												Answer
											</React.Fragment>
										)}
										{this.state.numberReplies > 1 && (
											<React.Fragment>
												Replies
											</React.Fragment>
										)}
									</h3>
								</header>
								<div className="discussion_tile-replies-list">
									{this.state.loadedReplies.length <
										this.state.numberReplies && (
										<ButtonBase
											className="discussion_tile-replies-list-load"
											focusRipple={true}
											disabled={this.state.repliesLoading}
											onClick={() => {
												this.loadThreadReplies(
													this.state
														.currentReplyPage + 1,
												);
											}}
										>
											{this.state.repliesLoading && (
												<React.Fragment>
													Loading...
												</React.Fragment>
											)}
											{!this.state.repliesLoading && (
												<React.Fragment>
													Load previous replies
												</React.Fragment>
											)}
										</ButtonBase>
									)}
									{this.state.loadedReplies.map(
										discussion => {
											return (
												<div
													className="discussion_tile-reply"
													key={discussion.id}
												>
													{this.renderReply(
														discussion,
													)}
												</div>
											);
										},
									)}
								</div>
							</section>
						)}
					{!!this.props.withRepliedTo &&
						this.props.discussion.replies_count > 0 &&
						this.props.discussionReplyData && (
							<div className="discussion_tile-isReply">
								{this.props.withRepliedTo ===
									this.props.userData.username &&
									this.props.discussion.user.username !==
										this.props.userData.username && (
										<DiscussionReplyName>
											<div>
												You replied to the message{' '}
												{!!this.props
													.discussionReplyData
													.created_at && (
													<TimeAgo
														date={generateAndFormatTimezoneDate(
															this.props
																.discussionReplyData
																.created_at
																.date,
														)}
													/>
												)}
											</div>
										</DiscussionReplyName>
									)}
								{this.props.withRepliedTo !==
									this.props.userData.username && (
									<DiscussionReplyName>
										<div>
											{this.props.withRepliedTo} replied
											to this discussion{' '}
											{!!this.props.discussionReplyData
												.created_at && (
												<TimeAgo
													date={generateAndFormatTimezoneDate(
														this.props
															.discussionReplyData
															.created_at.date,
													)}
												/>
											)}
										</div>
									</DiscussionReplyName>
								)}
							</div>
						)}
				</div>
				{!this.props.noActions &&
					this.props.userAuthenticated &&
					!this.props.withRepliedTo && (
						<div className="discussion_tile-actions">
							<div className="discussion_tile-actions-item">
								<DiscussionFollow
									discussionId={this.props.discussion.id}
									discussionTitle={
										this.props.discussion.title
									}
									followed={this.props.discussion.followed}
									disabled={!this.props.userAuthenticated}
									apiService={this.props.apiService}
								/>
							</div>
						</div>
					)}
			</div>
		);
	}

	protected renderReply(discussion: IApiDiscussion) {
		const createdAtFormatTimezoneDate = generateAndFormatTimezoneDate(
			discussion.created_at.date,
		);

		const replyDetails = {
			id: discussion.id,
			user: discussion.user.full_name,
		};

		if (
			this.state.deletedReplies.findIndex(
				value => discussion.id === value,
			) >= 0
		) {
			return null;
		}

		return (
			<div className="discussion_tile_reply">
				<div className="discussion_tile_reply-author">
					<ButtonBase
						className="discussion_tile_reply-author-button"
						focusRipple={true}
						onClick={() => {
							this.props.dispatch(
								userPanelDisplay(discussion.user),
							);
						}}
					>
						<div className="discussion_tile_reply-author-content font--small">
							<div className="discussion_tile_reply-author-avatar">
								<UserAvatar user={discussion.user} />
							</div>
							<div className="discussion_tile_reply-author-info">
								<div className="discussion_tile_reply-author-name">
									<span className="discussion_tile-author-name-link theme-title">
										{discussion.user.full_name}
									</span>
								</div>
								<div className="discussion_tile_reply-author-time theme-text--body_light">
									<TimeAgo
										date={createdAtFormatTimezoneDate}
									/>
								</div>
							</div>
						</div>
					</ButtonBase>
				</div>
				<div className="discussion_tile_reply-content font--small">
					<div className="discussion_tile_reply-content-author">
						<DiscussionReplyName>
							{discussion.reply_to.user.full_name}
						</DiscussionReplyName>
					</div>
					{/* <ReactMarkdown
						className="markdown markdown--inline"
						source={discussion.content}
						linkTarget="_blank"
					/> */}

					<div
						className="markdown"
						dangerouslySetInnerHTML={{
							__html: discussion.content,
						}}
					/>
				</div>
				<div className="discussion_tile_reply-content-meta hm-t16 hp-t8">
					{!this.props.hideGetIt && (
						<React.Fragment>
							<div className="discussion_tile_reply-content-meta-item">
								<DiscussionGetIt
									type="iGetIt"
									count={discussion.likes_count}
									discussionId={discussion.id}
									discussionTitle={discussion.title}
									liked={discussion.liked}
									disabled={!this.props.userAuthenticated}
									apiService={this.props.apiService}
								/>
							</div>
							<div className="discussion_tile_reply-content-meta-item">
								<DiscussionGetIt
									type="hug"
									count={discussion.hugs_count}
									discussionId={discussion.id}
									discussionTitle={discussion.title}
									liked={discussion.hugged}
									disabled={!this.props.userAuthenticated}
									apiService={this.props.apiService}
								/>
							</div>
						</React.Fragment>
					)}

					{!!this.props.withReplyAction &&
						this.props.userAuthenticated && (
							<div className="discussion_tile_reply-content-meta-item">
								<DiscussionReplyAction
									replyTo={replyDetails}
									action={this.handleReply}
									count={discussion.direct_replies_count}
								/>
							</div>
						)}
					{this.props.userAuthenticated &&
						this.props.noActions &&
						!this.props.hideReport && (
							<div className="discussion_tile-content-meta-item">
								<DiscussionReport
									discussion={discussion}
									handleDelete={this.handleDeleteDiscussion}
									editDiscussion={
										!!this.props.editDiscussion
											? () => {
													this.props.editDiscussion(
														discussion,
													);
											  }
											: undefined
									}
									deleteDiscussion={
										!!this.props.deleteDiscussion
											? () => {
													this.props.deleteDiscussion(
														discussion,
													);
											  }
											: undefined
									}
									canEdit={true}
								/>
							</div>
						)}
				</div>
			</div>
		);
	}

	protected handleReply(event, replyTo?): Promise<boolean> {
		return new Promise(resolve => {
			this.props.openReply(event, replyTo).then(result => {
				if (result) {
					this.loadThreadReplies(1);
				}

				resolve(result);
			});
		});
	}

	protected loadThreadReplies(page: number) {
		this.setState(
			{
				repliesLoading: true,
			},
			() => {
				const url = `/api/discussions/${this.props.discussion.id}/thread-replies?page=${page}`;

				this.props.apiService
					.queryGET(url)
					.then(data => {
						if ('discussions' in data && 'total' in data) {
							// Add the replies to the list of replies
							let loadedReplies: IApiDiscussion[] = dotProp.get(
								this.state,
								'loadedReplies',
							);
							data.discussions.forEach(
								(discussion: IApiDiscussion) => {
									if (
										!loadedReplies.find(
											singleLoadedReplies => {
												return (
													discussion.id ===
													singleLoadedReplies.id
												);
											},
										)
									) {
										loadedReplies.push(discussion);
									}
								},
							);
							// Sort the array
							loadedReplies = this.sortReplies(loadedReplies);

							this.setState({
								repliesLoading: false,
								loadedReplies,
								numberReplies: data.total,
								currentReplyPage: Math.max(
									this.state.currentReplyPage,
									page,
								),
							});
						} else {
							this.props.dispatch(
								apiError(['Invalid data received']),
							);
							this.setState({
								repliesLoading: false,
							});
						}
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							repliesLoading: false,
						});
					});
			},
		);
	}

	protected sortReplies(replies: IApiDiscussion[]) {
		const repliesCopy: IApiDiscussion[] = JSON.parse(
			JSON.stringify(replies),
		);
		repliesCopy.sort((a, b) => {
			if (a.created_at < b.created_at) {
				return -1;
			}
			if (a.created_at > b.created_at) {
				return 1;
			}
			return 0;
		});
		return repliesCopy;
	}

	protected handleDeleteDiscussion() {
		this.setState({
			deleted: true,
		});
	}

	protected handleDeleteDiscussionReply(replyId: number) {
		const deletedReplies: number[] = JSON.parse(
			JSON.stringify(this.state.deletedReplies),
		);
		deletedReplies.push(replyId);
		this.setState({
			deletedReplies,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(withServices(DiscussionTile));
