import React from 'react';
import Link from 'next/link';
import ButtonBase, { ButtonBaseProps } from '@material-ui/core/ButtonBase';

interface IProps extends ButtonBaseProps {
	href: string;
	hrefAs?: string;
	children?: any;
	prefetch?: boolean;
	disableRipple?: boolean;
}

export const ButtonBaseLink: React.SFC<IProps> = (props: IProps) => {
	return (
		<Link href={props.href} as={props.hrefAs} prefetch={props.prefetch} >
			<ButtonBase
				className={props.className}
				href={props.hrefAs || props.href}
				onClick={props.onClick}
				title={props.title}
				focusRipple={true}
				disableRipple={props.disableRipple}
				disabled={props.disabled}
			>
				{props.children}
			</ButtonBase>
		</Link>
	);
};
