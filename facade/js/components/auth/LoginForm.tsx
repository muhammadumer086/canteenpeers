import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Link from 'next/link';
import Button from '@material-ui/core/Button';

import { TextFieldFormik, PhoneFieldFormik } from '../inputs';

import { validationsUser } from '../../helpers/validations';
import { IApiUser } from '../../../js/interfaces';

interface IProps {
	submitting: boolean;
	selectedUser: IApiUser;
	onSubmit(values: IFormValues);
	onSubmitForm2(values: IFormValues1);
	email: string;
}

interface IFormValues {
	email: string;
	phone: string;
	code: string;
}
interface IFormValues1 {
	password: string;
}

export const LoginForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		email: validationsUser.email_null,
		// email: validationsUser.email_null.when('phone', (phone, schema) => {
		// 	if (!phone) {
		// 		return schema.required('email is required');
		// 	}
		// }),
		phone: validationsUser.phone_null.when('email', (email, schema) => {
			if (!email) {
				return schema.required('email or phone is required');
			}
		}),
	});
	const validationFormat1 = Yup.object().shape({
		password: validationsUser.password,
	});

	return (
		<div>
			{props.selectedUser === null ? (
				<Formik
					initialValues={{
						email: props.email,
						phone: '',
						code: '+61',
					}}
					validationSchema={validationFormat}
					onSubmit={props.onSubmit}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};
						return (
							<form
								className="auth_form form"
								onSubmit={handleSubmit}
							>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="text"
											label="Email"
											name="email"
											disabled={props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column hp-v0">
										<p className="hm-0 auth-form-text-center">
											or
										</p>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<PhoneFieldFormik
											type="text"
											label="Mobile Phone"
											name="phone"
											disabled={props.submitting}
											countryCodes={[
												{
													label: 'AU +61',
													value: '+61',
												},
												{
													label: 'NZ +64',
													value: '+64',
												},
												/*{
													label: 'PAK +92',
													value: '+92',
												},*/
											]}
											{...inputProps}
										/>
									</div>
								</div>

								<div className="form-row">
									<div className="form-column">
										<Link href="/auth/password-forgot">
											<a className="font--small">
												Forgot your password?
											</a>
										</Link>
									</div>
								</div>
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={props.submitting}
										>
											Next
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			) : (
				<Formik
					initialValues={{
						password: '',
					}}
					validationSchema={validationFormat1}
					enableReinitialize
					onSubmit={props.onSubmitForm2}
					render={({
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isValid,
					}) => {
						const inputProps = {
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							setFieldValue,
							setFieldTouched,
						};

						return (
							<form
								className="auth_form form"
								onSubmit={handleSubmit}
							>
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="password"
											label="Password"
											name="password"
											disabled={props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-column">
										<Link href="/auth/password-forgot">
											<a className="font--small">
												Forgot your password?
											</a>
										</Link>
									</div>
								</div>
								<div className="form-row form-row--submit">
									<div className="form-column">
										<Button
											variant="outlined"
											color="primary"
											className="form-submit"
											type="submit"
											disabled={
												!isValid || props.submitting
											}
										>
											Login
										</Button>
									</div>
								</div>
							</form>
						);
					}}
				/>
			)}
		</div>
	);
};
