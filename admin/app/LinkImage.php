<?php

namespace App;

use App\Link;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

use BinaryCabin\LaravelUUID\Traits\HasUUID;

class LinkImage extends Model {
	use HasUUID;

	protected $uuidFieldName = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $fillable = ['id'];

	/**
	 * The event associated with this image
	 */
	public function link() {
		return $this->belongsTo(Link::class);
	}

	public function getImageUrlAttribute() {
		return Storage::disk('s3')->url($this->path);
	}
}
