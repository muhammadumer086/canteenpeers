<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventChangeDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('events', function (Blueprint $table) {
			$table->integer('min_age')->nullable()->change();
			$table->integer('max_age')->nullable()->change();
			$table->boolean('is_online')->nullable()->change();
			$table->boolean('is_travel_cover')->nullable()->change();

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('events', function (Blueprint $table) {
			$table->integer('min_age')->default(0)->change();
			$table->integer('max_age')->default(0)->change();
			$table->boolean('is_online')->default(false)->change();
			$table->boolean('is_travel_cover')->default(false)->change();

		});
    }
}
