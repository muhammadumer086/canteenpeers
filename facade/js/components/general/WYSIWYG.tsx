import React from 'react';

interface IProps {
	children: any
}

const WYSIWYG: React.FC<IProps> = (props: IProps) => {

	return (
		<div className="wysiwyg">
			<div className="wysiwyg-container">
				{props.children}
			</div>
		</div>
	);
}

export default WYSIWYG;
