<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Country;
use App\Http\Resources\Country as CountryResource;

class CountryController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$countries = Country::orderBy('slug', 'ASC')->get();

		return response()->json([
			'countries' => CountryResource::collection($countries),
		]);
	}
}
