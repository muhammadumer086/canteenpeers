import React from 'react';

import {
	IApiUser,
	IApiMessage,
	IApiConversationDetail,
} from '../../interfaces';

import MessageConversation from './MessageConversation';
import { IEditGroupMessageFormikFormValues } from './EditGroupMessageFormik';
import MessageInformation from './MessageInformation';

interface IProps {
	isUpdating: boolean;
	isLoadingMessages: boolean;
	activeUsers: IApiUser[];
	activeConversationMessages: IApiMessage[];
	conversationDetail: IApiConversationDetail;
	messagesActive: boolean;
	clearMessageData: boolean;
	conversationInformationActive: boolean;
	loadMoreMessages(): void;
	submitReply(values: any): Promise<boolean>;
	closeConversation(): void;
	toggleConversationDetail(): void;
	updateConversation(
		payload: IEditGroupMessageFormikFormValues,
	): Promise<boolean>;
	onConversationLeave(): void;
}

const MessageView: React.StatelessComponent<IProps> = (props: IProps) => {
	const classes = ['user_messages-view'];

	if (props.messagesActive) {
		classes.push('user_messages-view--isActive');
	}

	return (
		<div className="user_messages-view_container">
			<div className={classes.join(' ')}>
				<MessageConversation {...props} />
			</div>
			{props.conversationDetail &&
				props.conversationDetail.group_conversation && (
					<MessageInformation
						isUpdating={props.isUpdating}
						activeUsers={props.activeUsers}
						toggleConversationDetail={
							props.toggleConversationDetail
						}
						conversationInformationActive={
							props.conversationInformationActive
						}
						conversationDetail={props.conversationDetail}
						updateConversation={props.updateConversation}
						onConversationLeave={props.onConversationLeave}
					/>
				)}
		</div>
	);
};

export default MessageView;
