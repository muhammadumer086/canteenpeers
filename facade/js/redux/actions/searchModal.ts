import { IStoreState } from '../store';
import { actionTypes } from '../actions';

export const searchModalDisplay = () => (dispatch, getState: () => IStoreState) => {
	if (getState().searchModal.searchModalDisplay) {
		return;
	}
	return dispatch({
		type: actionTypes.SEARCH_MODAL_DISPLAY,
	});
}

export const searchModalHide = () => (dispatch, getState: () => IStoreState) => {
	if (!getState().searchModal.searchModalDisplay) {
		return;
	}

	return dispatch({
		type: actionTypes.SEARCH_MODAL_HIDE,
	});
}
