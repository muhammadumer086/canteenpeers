import { IStoreState, apiError } from '../store';
import { actionTypes } from '../actions';

import { ApiService } from '../../services';

export const settingsLoad = (apiService: ApiService, token: string|null) => (dispatch, getState: () => IStoreState) => {
	if (!getState().settings.loading) {
		dispatch({
			type: actionTypes.SETTINGS_LOADING
		});

		apiService.queryAuthGET('/api/settings', token)
		.then((settings) => {
			dispatch({
				type: actionTypes.SETTINGS_LOADED,
				settings
			});
		})
		.catch((error) => {
			dispatch(apiError([
				error.message
			]));
			dispatch({
				type: actionTypes.SETTINGS_LOADED,
				settings: null
			});
		});
	}
}
