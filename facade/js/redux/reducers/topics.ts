import { actionTypes } from '../actions';
import { IApiTopic } from '../../interfaces';

export interface IStoreState {
	loading: boolean;
	topics: IApiTopic[];
}

export const topics = (state: IStoreState = {
	loading: false,
	topics: [],
}, action): IStoreState => {
	switch (action.type) {
		case actionTypes.TOPICS_LOAD:
			return Object.assign({}, state, {
				loading: true,
			});
		case actionTypes.TOPICS_LOADED:
			return Object.assign({}, state, {
				topics: action.topics,
				loading: false,
			});
		case actionTypes.TOPICS_LOAD_ERROR:
			return Object.assign({}, state, {
				loading: false,
			});
		default:
			return state;
	}
}
