<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Topic;
use App\Annoucement;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Controller extends BaseController {
	use AuthorizesRequests;
	use DispatchesJobs;
	use ValidatesRequests;
	use \App\Traits\AuthUser;

	protected function hasAdminPermission() {
		$request = request();
		if ($request->headers->has('Authorization')) {
			$user = Auth::guard('api')->authenticate($request);
			return $user->hasRoleName('admin');
		}

		return false;
	}

}
