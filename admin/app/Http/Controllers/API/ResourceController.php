<?php

namespace App\Http\Controllers\API;

use App\PrismicLinkResolver;
use App\Topic;
use App\User;
use App\Resource;
use App\Situation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use Validator;
use UserActivityHelper;
use UserPermissionHelper;

use Prismic\Dom\RichText;
use Prismic\Api;
use Prismic\LinkResolver;
use Prismic\Predicates;

use App\Http\Resources\Resources as ResourcesResource;
use App\Http\Resources\ResourcesCollection;

class ResourceController extends Controller {
	const PAGINATION_SIZE = 20;
	const FEATURED_RESOURCES = 3;

	/**
	 * Show the specified content.
	 *
	 * @param  string  $method
	 * @param  string  $content_id
	 * @return Response
	 */
	public function index(Request $request) {
		$authUser = $this->getAuthUser($request);
		$userHasPermission = UserPermissionHelper::userHasPermission($authUser, ["resources_read"]);

		$featureResourceIds = [];
		$situationIds = [];

		$validator = Validator::make($request->all(), [
			'topics' => 'bail|array|exists:topics,slug',
			'situations' => 'bail|array|exists:situations,name',
			'type' => 'bail|string|in:saved',
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		$resourcesQuery = Resource::orderBy('first_publication_date', 'DESC');

		$filterBySituation = new \App\Helpers\FilterByUserSituation();

		if (
			$authUser &&
			$request->has('type') &&
			$request->input('type') === 'saved'
		) {
			// Querying the saved resources
			$resourcesQuery->whereIn(
				'id',
				$authUser->savedResources()->pluck('resource_id')
			);
		} elseif ($request->has('situations') && !$userHasPermission) {
			// $situationIds = DB::table('situations')
			// 	->whereIn('name', $request->input('situations'))
			// 	->whereNull('parent_id')
			// 	->pluck('id');
			$situationIds = $filterBySituation->addSituationsMap(
				$request->input('situations')
			);

			$resourcesIds = DB::table('resource_situation')
				->whereIn('situation_id', $situationIds)
				->pluck('resource_id');

			$resourcesQuery->whereIn('id', $resourcesIds);
		} elseif (
			$authUser &&
			!$authUser->hasRoleName('admin') &&
			!$request->has('anySituation')
		) {
			// No situation filters selected, use situation of current user
			$resourcesQuery = $filterBySituation->filterByUserSituation(
				$authUser,
				$resourcesQuery,
				'resources',
				'resource_situation',
				'resource_id'
			);
		}

		// Filtering by topic
		if ($request->has('topics')) {
			$resourcesQuery->whereIn(
				'topic_id',
				DB::table('topics')
					->whereIn(
						'id',
						Topic::whereIn(
							'slug',
							$request->input('topics')
						)->pluck('id')
					)
					->pluck('id')
			);
		}

		// If no filters hide featured from results list
		if (
			!$request->has('topics') &&
			!$request->has('situations') &&
			!$request->has('type')
		) {
			// Get first the two newest feature blogs
			$featureResourcesQuery = Resource::where('featured', true);

			if (
				$authUser &&
				!$authUser->hasRoleName('admin') &&
				sizeof($situationIds)
			) {
				$featureResourcesQuery->whereIn(
					'resources.id',
					DB::table('resource_situation')
						->whereIn('situation_id', $situationIds)
						->pluck('resource_id')
				);
			}

			// Small hack to allow editor to push back some resources to the top
			$featureResourceIds = $featureResourcesQuery
				->orderBy('last_publication_date', 'DESC')
				->limit(self::FEATURED_RESOURCES)
				->pluck('id');

			$resourcesQuery->whereNotIn('id', $featureResourceIds);
		}

		$resources = $resourcesQuery->paginate(self::PAGINATION_SIZE);

		$collection = new ResourcesCollection($resources);

		if (sizeof($featureResourceIds)) {
			$collection->setFeaturedResourceIds(
				Resource::whereIn('id', $featureResourceIds)->get()
			);
		}

		return $collection;
	}

	/**
	 * Display the resource by slug from the laravel database.
	 *
	 * @param  int  $id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function show($slug, Request $request) {
		$authUser = $this->getAuthUser($request);
		$resource = Resource::where('slug', $slug)
			->with('topic')
			->firstOrFail();

		// Get the related resources
		$relatedResources = Resource::where([
			['id', '<>', $resource->id],
			['topic_id', '=', $resource->topic_id],
		])
			->inRandomOrder()
			->limit(2)
			->get();

		if ($authUser) {
			UserActivityHelper::create(
				$authUser,
				$resource,
				'Resource Read',
				null
			);
		}

		return response()->json([
			'resource' => new ResourcesResource($resource),
			'related' => ResourcesResource::collection($relatedResources),
		]);
	}

	/**
	 * Toggle a like/unlike of the discusion
	 *
	 * @param  int  $id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function toggleSave(Request $request, Resource $resource) {
		$authUser = $this->getAuthUser($request);

		//Verify valid request
		if (!$authUser || !$resource) {
			return response()->json(
				[
					'error' => 'Invaild request',
				],
				400
			);
		}

		//Check if already saved
		$saved = false;
		$saved = DB::table('saved_resources')
			->where('user_id', $authUser->id)
			->where('resource_id', $resource->id)
			->first();

		//If not saved then save
		if ($saved === null && $resource) {
			if ($resource) {
				$authUser
					->savedResources()
					->syncWithoutDetaching([$resource->id]);
				$saved = true;
			}
		} else {
			if ($resource) {
				$authUser->savedResources()->detach([$resource->id]);
				$saved = false;
			}
		}

		// Update the user's stats
		$authUser->reportNumbers();
		$authUser->save();
		$resource->touch();

		return response()->json([
			'saved' => $saved,
		]);
	}

	/**
	 * Get the user's saved resources
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function savedResources(Request $request) {
		$authUser = $this->getAuthUser($request);

		$resources = $authUser
			->savedResources()
			->orderBy('created_at', 'DESC')
			->paginate(self::PAGINATION_SIZE);

		return new ResourcesCollection($resources);
	}

	public function share(Request $request, Resource $resource) {
		$authUser = $this->getAuthUser($request);

		$validator = Validator::make($request->all(), [
			'medium' => [
				'bail',
				'required',
				Rule::in([
					'community',
					'facebook',
					'twitter',
					'copy link',
					'email',
				]),
			],
		]);

		if ($validator->fails()) {
			$error = $validator->errors()->first();

			return response()->json(
				[
					'error' => $error,
				],
				400
			);
		}

		UserActivityHelper::create($authUser, $resource, 'Shared Resource', [
			'medium' => $request->input('medium'),
		]);

		return response()->json([
			'shared' => true,
		]);
	}
}
