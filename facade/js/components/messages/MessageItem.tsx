import React from 'react';
import { connect } from 'react-redux';
import TimeAgo from 'react-timeago';

import { IApiUser, IApiMessage } from '../../interfaces';
import { IStoreState } from '../../../js/redux/store';

import UserAvatar from '../general/UserAvatar';
import { generateAndFormatTimezoneDate } from '../../../js/helpers/generateTimezoneDate';

interface IProps {
	userData: IApiUser;
	message: IApiMessage;
}

const MessageItemRender = (props: { message: IApiMessage }) => {
	const { message } = props.message;
	const messageItemClasses = ['message_item-content'];

	return (
		<div
			className={messageItemClasses.join(' ')}
			dangerouslySetInnerHTML={{
				__html: message,
			}}
		/>
	);
};

const MessageItem = (props: IProps) => {
	const classes = ['message_item-container'];

	const updatedAtFormatTimezoneDate = generateAndFormatTimezoneDate(
		props.message.updated_at.date,
	);

	if (props.message.system_generated) {
		classes.push('message_item-container--system_generated');
	} else if (props.message.user.email === props.userData.email) {
		classes.push('message_item-container--is_user_message');
	}

	return (
		<div className={classes.join(' ')}>
			<div className="message_item">
				<div className="message_item-avatar_container">
					<div className="message_item-avatar">
						<UserAvatar user={props.message.user} />
					</div>
				</div>
				<div className="message_item-content_container">
					<span className="message_item-name">
						{props.message.user.full_name}
					</span>
					<div>
						{props.message.system_generated ? (
							processSystemGenerated(props.message)
						) : (
							<MessageItemRender message={props.message} />
						)}
					</div>
					<span className="message_item-timeago">
						<TimeAgo date={updatedAtFormatTimezoneDate} />
					</span>
				</div>
			</div>
		</div>
	);
};

function processSystemGenerated(message: IApiMessage): string {
	let messageContent = message.message;

	// Process the message targets
	const targets = message.target_users.map(user => {
		return user.full_name;
	});
	let targetNames = '';
	if (targets.length >= 2) {
		const lastTarget = targets.pop();
		targetNames = targets.join(', ');
		targetNames += ' and ' + lastTarget;
	} else {
		targetNames = targets.join(' and ');
	}

	messageContent = messageContent.replace('%author%', message.user.full_name);
	messageContent = messageContent.replace('%targets%', targetNames);

	return messageContent;
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	return {
		userAuthenticated,
		userData,
	};
}

export default connect(mapStateToProps)(MessageItem);
