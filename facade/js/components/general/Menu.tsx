import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

interface IProps {
	userAuthenticated: boolean | null;
	handleMyAccount: any;
	handleLogout: any;
	handleLink: any;
	handleClose: any;
	handleOpen: any;
	menuOpen: Boolean;
	toggleReportModal():void; 
}

const StyledMenu = withStyles({})((props: MenuProps) => (
	<Menu
		elevation={0}
		getContentAnchorEl={null}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'center',
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'center',
		}}
		{...props}
	/>
));

export default function CustomizedMenus(props: IProps) {
	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
	let ref: null | HTMLElement = null;
	useEffect(() => {
		if (props.menuOpen) {
			setAnchorEl(ref);
		} else {
			setAnchorEl(null);
		}
	}, [props.menuOpen]);

	const handleClick = (event: React.MouseEvent<HTMLElement>) => {
		props.handleOpen();
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		props.handleClose();
		setAnchorEl(null);
	};

	return (
		<div>
			<div ref={c => (ref = c)} onClick={handleClick}>
				<ArrowDropDownIcon className="ml-1" />
			</div>
			<div className={`arrow ${anchorEl ? 'arrow-show' : ''}`}></div>
			<StyledMenu
				id="customized-menu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				{props.userAuthenticated === true && (
					<MenuItem onClick={props.handleMyAccount}>
						My account
					</MenuItem>
				)}
				{props.userAuthenticated  && (
					<MenuItem onClick={props.toggleReportModal}>
						Report a problem
					</MenuItem>
				)}
				{props.userAuthenticated === true && (
					<MenuItem onClick={props.handleLogout}>Logout</MenuItem>
				)}
				{props.userAuthenticated === false && (
					<MenuItem
						onClick={() => {
							props.handleLink('/auth/login');
						}}
					>
						Login
					</MenuItem>
				)}
				{props.userAuthenticated === false && (
					<MenuItem
						onClick={() => {
							props.handleLink(
								'/auth/register?step=step-1',
								'/auth/register/step-1',
							);
						}}
					>
						Register
					</MenuItem>
				)}
			</StyledMenu>
		</div>
	);
}
