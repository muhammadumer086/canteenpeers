import React from 'react';

import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Async } from 'react-select';

interface IProps {
	type: string;
	label: string;
	name: string;
	name_data: string;
	values: any;
	touched: any;
	errors: any;
	disabled?: boolean;
	setRef?(any):any;
	setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
	setFieldTouched(
		field: string,
		isTouched: boolean,
		shouldValidate?: boolean,
	): void;
}

interface IState {
	disabled: boolean;
	selectValue: string | number | boolean;
}

class Option extends React.Component<any, {}> {
	constructor(props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	public render() {
		const { children, isFocused, onFocus } = this.props;

		return (
			<MenuItem
				onFocus={onFocus}
				selected={isFocused}
				onClick={this.handleClick}
				component="div"
			>
				{children}
			</MenuItem>
		);
	}

	protected handleClick(event) {
		this.props.onSelect(this.props.option, event);
	}
}

const SelectWrapped: React.FC<any> = (props: any) => {
	const { classes, value, ...other } = props;

	let valueData = {
		label: value,
		value,
	};
	if (typeof value === 'object') {
		valueData = value;
	}

	return (
		<Async
			optionComponent={Option}
			noResultsText={<Typography>{'No results found'}</Typography>}
			arrowRenderer={arrowProps => {
				return arrowProps.isOpen ? (
					<ArrowDropUpIcon className="input-select-icon" />
				) : (
					<ArrowDropDownIcon className="input-select-icon" />
				);
			}}
			cacheOptions
			clearable={false}
			value={valueData}
			{...other}
		/>
	);
};

export class AutocompletePostcodeFormik extends React.Component<
	IProps,
	IState
> {
	constructor(props) {
		super(props);

		this.state = {
			disabled: false,
			selectValue:
				props.name in props.values ? props.values[props.name] : null,
		};

		this.handleChange = this.handleChange.bind(this);
		this.handlePostcodeQuery = this.handlePostcodeQuery.bind(this);
	}

	public render() {
		return (
			<TextField
				className="form-input form-input--autocomplete form-input--select"
				value={this.props.values[this.props.name]}
				onChange={this.handleChange}
				name={this.props.name}
				label={this.props.label}
				error={
					this.props.touched[this.props.name] &&
					!!this.props.errors[this.props.name]
				}
				helperText={
					this.props.touched[this.props.name]
						? this.props.errors[this.props.name]
						: null
				}
				InputLabelProps={{
					shrink: true,
				}}
				autoComplete="off"
				InputProps={{
					inputComponent: SelectWrapped,
					inputProps: {
						instanceId: 'react-select-chip-label',
						id: 'react-select-chip-label',
						loadOptions: this.handlePostcodeQuery,
						disabled: this.state.disabled || this.props.disabled,
						value: this.state.selectValue,
						placeholder: 'Please enter your postcode',
					},
					

				}}
				disabled={this.state.disabled || this.props.disabled}
				inputRef={this.props.setRef?this.props.setRef:undefined}

			/>
		);
	}

	public handleChange(option) {
		if (option && option.hasOwnProperty('value')) {
			this.setState(
				{
					disabled: true,
					selectValue: option,
				},
				() => {
					const service = new google.maps.Geocoder();
					service.geocode(
						{
							placeId: option.value,
						},
						(results, status) => {
							if (status !== google.maps.GeocoderStatus.OK) {
								this.setState({
									disabled: false,
								});
								return;
							}
							const place = results[0];
							// Find the postcode
							let formattedAddress = null;

							if ('formatted_address' in place) {
								formattedAddress = place.formatted_address;
							}

							if (formattedAddress) {
								this.props.setFieldValue(
									this.props.name,
									formattedAddress,
								);
								this.props.setFieldTouched(
									this.props.name,
									true,
									true,
								);
								this.props.setFieldValue(
									this.props.name_data,
									place,
								);
								this.props.setFieldTouched(
									this.props.name_data,
									true,
									true,
								);
							}

							this.setState({
								disabled: false,
							});
						},
					);
				},
			);
		} else {
			this.setState({
				selectValue: null,
			});
			this.props.setFieldValue(this.props.name, '');
			this.props.setFieldTouched(this.props.name, true, true);
			this.props.setFieldValue(this.props.name_data, null);
			this.props.setFieldTouched(this.props.name_data, true, true);
		}
	}

	public handlePostcodeQuery(inputValue) {
		if (inputValue) {
			return new Promise((resolve, reject) => {
				const service = new google.maps.places.AutocompleteService();
				const options = {
					componentRestrictions: { country: ['au', 'nz'] },
					input: inputValue,
					types: ['(regions)'],
				};
				const handlePlaceQuery = (predictions, status) => {
					const results = {
						options: [],
					};
					if (status === google.maps.places.PlacesServiceStatus.OK) {
						results.options = predictions.map(res => {
							return {
								label: res.description,
								value: res.place_id,
							};
						});
					} else {
						console.warn(`Google autocomplete status:`, status);
						reject(results);
					}

					resolve(results);
				};

				return service.getPlacePredictions(options, handlePlaceQuery);
			});
		} else {
			return Promise.resolve({ options: [] });
		}
	}
}
