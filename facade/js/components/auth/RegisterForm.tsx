import React from 'react';
import * as Yup from 'yup';
import { Formik, FormikErrors } from 'formik';
import moment from 'moment';

import Button from '@material-ui/core/Button';

import {
	AutocompletePostcodeFormik,
	// CheckboxGroupFormik,
	// RadioGroupFormik,
	AutocompleteSelectFormik,
	TextFieldFormik,
	SelectFieldFormik,
	InlineDatePickerFormik,
	PhoneFieldFormik,
} from '../inputs';

import { validationsUser } from '../../helpers/validations';
import { ISelectOption } from '../../interfaces';
import { ApiService } from '../../services';
import CircularProgress from '@material-ui/core/CircularProgress';
// import EditIcon from '@material-ui/icons/Edit';
// import { Fab } from '@material-ui/core';
import { connect } from 'react-redux';
import { IApiUser } from '../../interfaces';
import { IStoreState } from '../../redux/store';
const PTC_HREF =
	'https://parentingthroughcancer.org.au/?utm_medium=content-text&utm_source=ccyp+website&utm_campaign=canteen+connect+registration&utm_content=over+25+ptc';
const CANTEEN_HREF =
	'https://www.canteen.org.au/contact/?utm_medium=content-text&utm_source=ccyp+website&utm_campaign=canteen+connect+registration&utm_content=under+12+contact+us';
interface IFormValues {
	first_name: string;
	last_name: string;
	email: string;
	phone: string;
	username: string;
	password: string;
	dob: string;
	location: string;
	location_data: any;
	gender: string;
	self_identify: string | null;
	situations: ISelectOption[];
	parent_name?: string;
	parent_email?: string;
	//verification: string;
}
interface IProps {
	userRawData: any | null;
	situations: Array<{
		value: string;
		label: string;
	}>;
	submitting: boolean;
	emailTaken?: boolean;
	userNameTaken?: boolean;
	registered?: boolean;
	onSubmit(values: IFormValues): void;
	dispatchError(message: string, email?: string, age?: number): void;
	apiService: ApiService;
	verifyByEmail?(): void;
	verifyByPhone?(): void;
	userData: IApiUser | null;
	updateRoute(url: {}, as?: string): void;
}

interface IFormValues1 {
	first_name: string;
	last_name: string;
	email: string;
	phone: string;
}
interface IFormValues2 {
	username: string;
	password: string;
	dob: string;
}
interface IFormValues3 {
	location: string;
	location_data: any;
	gender: string;
	self_identify: string | null;
	situations: ISelectOption[];
	parent_name?: string;
	parent_email?: string;
}

interface IState {
	isAdmin: boolean;
	showAgeWarning: boolean;
	showAgeUnder16: boolean;
	age: number | null;
	currentPage: number;
	isLoading: boolean;

	first_name: string;
	last_name: string;
	email: string;
	phone: string;
	code: string;

	username: string;
	password: string;
	dob: string;

	location: string;
	location_data: any;
	gender: string;
	self_identify: string | null;
	situations: ISelectOption[];
	parent_name?: string;
	parent_email?: string;
}

const UnderTwelveWarningComponent: React.StatelessComponent = () => {
	return (
		<div className="auth_form-show_warning theme--main">
			<h3 className="theme--main">It looks like you're under 12</h3>

			<p>Canteen Connect is for those aged between 12-25</p>
			<p>
				Please{' '}
				<a href={CANTEEN_HREF} target="_blank">
					contact Canteen
				</a>{' '}
				directly to continue the set up of your account.
			</p>
		</div>
	);
};

const OverTwentySixWarningComponent: React.StatelessComponent = () => {
	return (
		<div className="auth_form-show_warning theme--main">
			<h3 className="theme--main">It looks like you're over 25</h3>

			<p>Canteen Connect is for those aged between 12-25</p>
			<p>
				Did you know we have a{' '}
				<a href={PTC_HREF} target="_blank">
					Parenting through Cancer community - An Online Cancer
					Community For Parents
				</a>
			</p>
		</div>
	);
};

class RegisterForm extends React.Component<IProps, IState> {
	protected adminRegex = /\@canteen\.org\.(au|nz)$/;
	protected fristNameRef;
	protected lastNameRef;
	protected passwordRef;
	protected dobRef;
	protected locationRef;
	protected userNameRef;
	protected emailRef;
	protected genderRef;
	protected phoneRef;
	constructor(props) {
		super(props);

		this.state = {
			isAdmin: false,
			showAgeWarning: false,
			showAgeUnder16: false,
			age: null,
			currentPage: 1,
			isLoading: false,

			//form 1
			first_name: '',
			last_name: '',
			email: '',
			phone: '',
			code: '+61',

			//form 2
			username: '',
			password: '',
			dob: '',

			//form 3
			location_data: null,
			location: '',
			gender: '',
			self_identify: '',
			situations: [],
			parent_name: '',
			parent_email: '',
		};

		this.checkSensitiveAge = this.checkSensitiveAge.bind(this);
		this.handleEmailChange = this.handleEmailChange.bind(this);
		this.setUserNameRef = this.setUserNameRef.bind(this);
		this.setEmailRef = this.setEmailRef.bind(this);
		this.setPhoneRef = this.setPhoneRef.bind(this);
		this.setFristNameRef = this.setFristNameRef.bind(this);
		this.setlastNameRef = this.setlastNameRef.bind(this);
		this.setPasswordRef = this.setPasswordRef.bind(this);
		this.setDOBRef = this.setDOBRef.bind(this);
		this.setLocationRef = this.setLocationRef.bind(this);
		this.sumbitFirstPage = this.sumbitFirstPage.bind(this);
		this.sumbitSecondPage = this.sumbitSecondPage.bind(this);
		this.handleFristBack = this.handleFristBack.bind(this);
		this.handleSecondBack = this.handleSecondBack.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	public componentDidMount() {
		if (this.props.userData === null) {
			this.props.updateRoute(
				{
					pathname: '/auth/register',
					query: { step: 'step-1' },
				},
				'/auth/register/step-' + this.state.currentPage,
			);
		}
	}

	public componentDidUpdate(prevP: IProps) {
		if (this.props !== prevP) {
			if (!prevP.userNameTaken && this.props.userNameTaken) {
				this.userNameRef && this.userNameRef.focus();
			}
			if (!prevP.emailTaken && this.props.emailTaken) {
				this.emailRef && this.emailRef.focus();
			}
		}
	}

	public render() {
		let validationFormat1 = Yup.object().shape({
			first_name: validationsUser.first_name,
			last_name: validationsUser.last_name,
			email: validationsUser.email,
			phone: validationsUser.phone,
		});

		let validationFormat2 = Yup.object().shape({
			username: validationsUser.username,
			password: validationsUser.password,
			dob: validationsUser.dob,
		});
		const {
			currentPage,
			isLoading,
			first_name,
			last_name,
			email,
			phone,
			code,
			username,
			password,
			dob,
			showAgeWarning,
			age,
		} = this.state;
		const { registered } = this.props;
		let validationFormat3 = Yup.object().shape({
			location: validationsUser.location,
			location_data: validationsUser.location_data,
			gender: validationsUser.gender,
			self_identify: validationsUser.self_identify,
			situations: Yup.array()
				.of(
					Yup.mixed().oneOf(
						this.props.situations.map(singleOption => {
							return singleOption.value;
						}),
					),
				)
				.required('Please select at least one option'),
		});

		if (this.state.isAdmin) {
			validationFormat3 = Yup.object().shape({
				location: validationsUser.location,
				location_data: validationsUser.location_data,
				gender: validationsUser.gender,
				self_identify: validationsUser.self_identify,
				situations: Yup.array(),
			});
		}

		if (this.state.showAgeUnder16) {
			validationFormat3 = Yup.object().shape({
				parent_name: validationsUser.full_name,
				parent_email: Yup.string()
					.notOneOf(
						[Yup.ref('email'), null],
						'Parent or Guardians email address cannot be the same as one already provided',
					)
					.required('Parent or Guardians email address is required'),
				location: validationsUser.location,
				location_data: validationsUser.location_data,
				gender: validationsUser.gender,
				self_identify: validationsUser.self_identify,
				situations: Yup.array(),
			});
		}

		let ageComponent: any;

		if (this.state.showAgeWarning && this.state.age < 12) {
			ageComponent = <UnderTwelveWarningComponent />;
		} else if (this.state.showAgeWarning && this.state.age > 25) {
			ageComponent = <OverTwentySixWarningComponent />;
		}
		return (
			<div>
				{currentPage === 1 && registered === false && (
					<Formik
						initialValues={{
							first_name: this.getUserRawParam<string>(
								'firstname',
								first_name,
							),
							last_name: this.getUserRawParam<string>(
								'lastname',
								last_name,
							),
							email: this.getUserRawParam<string>('email', email),
							phone: this.getUserRawParam<string>('phone', phone),
							code: code,
						}}
						enableReinitialize={true}
						validationSchema={validationFormat1}
						onSubmit={values => {
							console.log('values', values);
						}}
						render={({
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							// handleSubmit,
							setFieldValue,
							setFieldTouched,
							setErrors,
							//isValid,
						}) => {
							const inputProps = {
								values,
								touched,
								errors,
								handleChange,
								handleBlur,
								setFieldValue,
								setFieldTouched,
							};

							const emailProps = {
								values,
								touched,
								errors,
								handleBlur,
								setFieldValue,
								setFieldTouched,
							};
							return (
								<form
									className="auth_form form"
									onSubmit={e => {
										e.preventDefault();
										[
											'first_name',
											'last_name',
											'email',
											'phone',
										].map((name: keyof IFormValues1) => {
											setFieldTouched(name, true, true);
										});
										// const { showAgeWarning } = this.state;
										const {
											first_name,
											last_name,
											phone,
											email,
										} = errors;
										if (
											first_name ||
											values.first_name === ''
										) {
											this.fristNameRef &&
												this.fristNameRef.focus();
											return;
										} else if (last_name) {
											this.lastNameRef &&
												this.lastNameRef.focus();
											return;
										} else if (email) {
											this.emailRef &&
												this.emailRef.focus();
											return;
										} else if (phone) {
											return;
										}

										this.sumbitFirstPage(values, setErrors);
									}}
								>
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="text"
												label="First Name"
												name="first_name"
												setRef={this.setFristNameRef}
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="text"
												label="Last Name"
												name="last_name"
												setRef={this.setlastNameRef}
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row">
										<div className="form-column">
											<TextFieldFormik
												type="email"
												label="Email"
												name="email"
												setRef={this.setEmailRef}
												disabled={this.props.submitting}
												{...emailProps}
												handleChange={e => {
													handleChange(e);
													this.handleEmailChange(e);
												}}
											/>
										</div>
									</div>

									<div className="form-row">
										<div className="form-column">
											<PhoneFieldFormik
												type="phone"
												label="Mobile Phone"
												name="phone"
												setRef={this.setPhoneRef}
												disabled={this.props.submitting}
												countryCodes={[
													{
														label: 'AU +61',
														value: '+61',
													},
													{
														label: 'NZ +64',
														value: '+64',
													},
													{
														label: 'PAK +92',
														value: '+92',
													},
												]}
												{...inputProps}
											/>
										</div>
									</div>

									<div className="form-row form-row--submit">
										<div className="form-column">
											<Button
												variant="outlined"
												color="primary"
												className="form-submit"
												type="submit"
												disabled={isLoading}
											>
												{isLoading ? (
													<CircularProgress
														size={14}
													/>
												) : (
													'Next'
												)}
											</Button>
										</div>
									</div>
								</form>
							);
						}}
					/>
				)}
				{currentPage === 2 && registered === false && (
					<Formik
						initialValues={{
							username: username,
							password: password,
							dob: this.getUserRawParam<string>(
								'date_of_birth',
								dob,
							),
						}}
						validationSchema={validationFormat2}
						onSubmit={values => console.log(values)}
						render={({
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							// handleSubmit,
							setFieldValue,
							setFieldTouched,
							setErrors,
						}) => {
							const inputProps = {
								values,
								touched,
								errors,
								handleChange,
								handleBlur,
								setFieldValue,
								setFieldTouched,
							};
							return (
								<form
									className="auth_form form"
									onSubmit={e => {
										e.preventDefault();
										['username', 'password', 'dob'].map(
											(name: keyof IFormValues2) => {
												setFieldTouched(
													name,
													true,
													true,
												);
											},
										);
										// const { showAgeWarning } = this.state;
										const {
											username,
											password,
											dob,
										} = errors;
										if (
											username ||
											values.username === ''
										) {
											this.userNameRef &&
												this.userNameRef.focus();
											return;
										} else if (password) {
											this.passwordRef &&
												this.passwordRef.focus();
											return;
										} else if (dob) {
											this.dobRef && this.dobRef.focus();
											return;
										} else if (showAgeWarning) {
											if (showAgeWarning) {
												console.log(
													'called age wariing',
												);
												const message =
													'You must be between the ages of 12 and 25 to use this platform.';
												this.props.dispatchError(
													message,
													email,
													age,
												);
											}
											this.dobRef && this.dobRef.focus();
											return;
										}

										if ('dob' in values && values.dob) {
											values.dob = moment(
												values.dob,
											).format('YYYY-MM-DD');
										}
										this.sumbitSecondPage(
											values,
											setErrors,
										);
										//handleSubmit(e);
									}}
								>
									{currentPage === 2 && registered === false && (
										<div>
											<div className="form-row">
												<div className="form-column">
													<TextFieldFormik
														type="text"
														label="Username (not your real name)"
														name="username"
														setRef={
															this.setUserNameRef
														}
														disabled={
															this.props
																.submitting
														}
														{...inputProps}
													/>
												</div>
											</div>
											<div className="form-row">
												<div className="form-column">
													<TextFieldFormik
														type="password"
														label="Create a password"
														name="password"
														setRef={
															this.setPasswordRef
														}
														disabled={
															this.props
																.submitting
														}
														helperText="Password must be at least 8 characters, must contain a number and a special character (ex: @,!,#)"
														{...inputProps}
													/>
												</div>
											</div>
											<div className="form-row">
												<div className="form-column">
													<InlineDatePickerFormik
														label="Your Date of Birth"
														name="dob"
														format="DD/MM/YYYY"
														disabled={
															this.props
																.submitting
														}
														disableFuture={true}
														checkSensitiveAge={
															this
																.checkSensitiveAge
														}
														{...inputProps}
														setRef={this.setDOBRef}
													/>
												</div>
											</div>

											{showAgeWarning && (
												<React.Fragment>
													{ageComponent}
												</React.Fragment>
											)}
											<div className="form-row form-row--submit">
												<div className="form-column">
													<Button
														variant="outlined"
														color="primary"
														className="form-submit hm-r8"
														onClick={() =>
															this.handleFristBack(
																values,
															)
														}
													>
														Back
													</Button>
													<Button
														variant="outlined"
														color="primary"
														className="form-submit"
														disabled={isLoading}
														type="submit"
														//onClick={() => { this.sumbitSecondPage(values, setErrors) }}
													>
														{isLoading ? (
															<CircularProgress
																size={14}
															/>
														) : (
															'Next'
														)}
													</Button>
												</div>
											</div>
										</div>
									)}
								</form>
							);
						}}
					/>
				)}

				{currentPage === 3 && registered === false && (
					<Formik
						onSubmit={_ => console.log('')}
						initialValues={{
							location: this.state.location,
							location_data: this.state.location_data,
							parent_name: this.state.parent_name,
							parent_email: this.state.parent_email,
							gender: this.getUserRawParam<any>(
								'gender',
								this.state.gender,
								value => {
									if (value === 'M') {
										return 'male';
									} else if (value === 'F') {
										return 'female';
									}
									return '';
								},
							),
							self_identify: this.state.self_identify,
							situations: this.getUserRawParam<any>(
								'cancer_affected_you',
								this.state.situations,
								value => {
									// Search for a situation with the same name
									const situation = this.props.situations.find(
										item => {
											return (
												item.label.toLowerCase() ===
												value.toLowerCase()
											);
										},
									);

									if (situation) {
										return [situation.value];
									}
									return [];
								},
							),
						}}
						validationSchema={validationFormat3}
						render={({
							values,
							touched,
							errors,
							handleChange,
							handleBlur,
							// handleSubmit,
							setFieldValue,
							setFieldTouched,
							//isValid,
						}) => {
							const inputProps = {
								values,
								touched,
								errors,
								handleChange,
								handleBlur,
								setFieldValue,
								setFieldTouched,
							};

							const emailProps = {
								values,
								touched,
								errors,
								handleBlur,
								setFieldValue,
								setFieldTouched,
							};
							return (
								<form
									className="auth_form form"
									onSubmit={e => {
										e.preventDefault();
										[
											'location',
											'situations',
											'gender',
										].map((name: keyof IFormValues3) => {
											setFieldTouched(name, true, true);
										});
										// const { showAgeWarning } = this.state;
										const { location, gender } = errors;
										if (
											location ||
											values.location === ''
										) {
											this.locationRef &&
												this.locationRef.focus();
											return;
										} else if (gender) {
											this.genderRef &&
												this.genderRef.focus();
											return;
										}

										const obj: IFormValues = {
											first_name,
											last_name,
											email,
											dob,
											phone,
											username,
											password,
											...values,
										};
										if (phone) {
											let phoneNumber = phone;
											if (phone.charAt(0) === '0') {
												phoneNumber = phone.substr(1);
											}
											phoneNumber = code + phoneNumber;
											obj['phone'] = phoneNumber;
										}

										this.props.onSubmit(obj);

										//	handleSubmit(e);
									}}
								>
									<div className="form-row">
										<div className="form-column">
											<AutocompletePostcodeFormik
												type="postcode"
												label="Suburb or postcode"
												name="location"
												name_data="location_data"
												setRef={this.setLocationRef}
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									<div className="form-row">
										<div className="form-column">
											<SelectFieldFormik
												label="Gender"
												name="gender"
												options={[
													{
														label: 'Female',
														value: 'female',
													},
													{
														label: 'Female Trans',
														value: 'female-trans',
													},
													{
														label: 'Male',
														value: 'male',
													},
													{
														label: 'Male Trans',
														value: 'male-trans',
													},
													{
														label:
															'These options don’t describe me',
														value: 'other',
													},
												]}
												noEmpty
												disabled={this.props.submitting}
												{...inputProps}
											/>
										</div>
									</div>
									{values['gender'] === 'other' && (
										<div className="form-row">
											<div className="form-column">
												<TextFieldFormik
													type="text"
													label="Please let us know how you self-identify"
													name="self_identify"
													disabled={false}
													{...inputProps}
												/>
												<br />
												<br />
											</div>
										</div>
									)}
									{!this.state.isAdmin && (
										<div className="form-row">
											<div className="form-column">
												<AutocompleteSelectFormik
													label="What best describes your cancer experience?"
													name="situations"
													options={
														this.props.situations
													}
													multi
													disabled={
														this.props.submitting
													}
													{...inputProps}
												/>
											</div>
										</div>
									)}

									{this.state.showAgeUnder16 && (
										<React.Fragment>
											<div className="form-row">
												<div className="form-column">
													<TextFieldFormik
														type="text"
														label="Parent or Guardian Full Name"
														name="parent_name"
														disabled={
															this.props
																.submitting
														}
														{...emailProps}
														handleChange={e => {
															handleChange(e);
															this.handleEmailChange(
																e,
															);
														}}
													/>
												</div>
											</div>

											<div className="form-row">
												<div className="form-column">
													<TextFieldFormik
														type="email"
														label="Parent or Guardian Email"
														name="parent_email"
														disabled={
															this.props
																.submitting
														}
														{...emailProps}
														handleChange={e => {
															handleChange(e);
															this.handleEmailChange(
																e,
															);
														}}
													/>
												</div>
											</div>
										</React.Fragment>
									)}

									<div className="form-row">
										<div className="form-column">
											<p className="form-helper-text">
												By registering you accept our{' '}
												<a
													href="https://www.canteen.org.au/terms-conditions/"
													target="_blank"
													rel="noopener noreferrer"
												>
													Terms & Conditions
												</a>{' '}
												and{' '}
												<a
													href="https://www.canteen.org.au/privacy-policy/"
													target="_blank"
													rel="noopener noreferrer"
												>
													Privacy Policy
												</a>
											</p>
										</div>
									</div>

									<div className="form-row form-row--submit">
										<div className="form-column">
											<Button
												variant="outlined"
												color="primary"
												className="form-submit hm-r8"
												disabled={this.props.submitting}
												onClick={() =>
													this.handleSecondBack(
														values,
													)
												}
											>
												Back
											</Button>
											<Button
												variant="outlined"
												color="primary"
												className="form-submit"
												type="submit"
												disabled={this.props.submitting}
											>
												Register
											</Button>
										</div>
									</div>
								</form>
							);
						}}
					/>
				)}
				{registered === true && (
					<div>
						<form className="auth_form form">
							<div className="register_verify">
								<h3 className="register_verify-heading">
									Please verify your account
								</h3>
								<p className="register_verify-text">
									We need to verify your account. After that
									you're ready to go!
								</p>
								<Button
									variant="contained"
									color="primary"
									fullWidth
									className="form-submit register_verify-phone-buuton"
									onClick={() => {
										this.handleSubmit('phone');
									}}
									disabled={
										this.props.submitting ||
										(this.props.userData &&
											!this.props.userData.phone)
									}
								>
									<span>Verify by mobile phone</span>
								</Button>

								<Button
									variant="contained"
									fullWidth
									color="inherit"
									onClick={() => {
										this.handleSubmit('email');
									}}
									className="form-submit hm-b16 register_verify-email-buuton"
									disabled={this.props.submitting}
								>
									<span>Verify by email</span>
								</Button>
							</div>
						</form>
					</div>
				)}
			</div>
		);
	}

	protected checkSensitiveAge(event) {
		const dob = event;
		const age = moment().diff(dob, 'years');
		if (!this.state.isAdmin) {
			if (age < 12 || age > 25) {
				this.setState({
					showAgeWarning: true,
					showAgeUnder16: false,
					age,
				});
			} else if (age >= 12 && age < 16) {
				this.setState({
					showAgeWarning: false,
					showAgeUnder16: true,
					age,
				});
			} else {
				this.setState({
					showAgeWarning: false,
					showAgeUnder16: false,
					age,
				});
			}
		}
	}

	protected handleEmailChange(e) {
		this.setState({
			isAdmin: !!(e.target.value as string).match(this.adminRegex),
		});
	}

	protected getUserRawParam<T>(
		paramName: string,
		defaultValue: T,
		postProcessValue?: (value: any) => T,
	): T {
		if (
			this.props.userRawData &&
			paramName in this.props.userRawData &&
			this.props.userRawData[paramName]
		) {
			const value: any = this.props.userRawData[paramName];
			if (postProcessValue) {
				return postProcessValue(value);
			}
			return value;
		}

		return defaultValue;
	}

	protected setUserNameRef = ref => {
		this.userNameRef = ref;
	};

	protected setPhoneRef = ref => {
		this.phoneRef = ref;
	};
	protected setEmailRef = ref => {
		this.emailRef = ref;
	};

	protected setFristNameRef = ref => {
		this.fristNameRef = ref;
	};

	protected setlastNameRef = ref => {
		this.lastNameRef = ref;
	};

	protected setPasswordRef = ref => {
		this.passwordRef = ref;
	};

	protected setLocationRef = ref => {
		this.locationRef = ref;
	};
	protected setDOBRef = ref => {
		this.dobRef = ref;
	};
	protected setGenderRef = ref => {
		this.genderRef = ref;
	};

	protected sumbitFirstPage = (
		values: IFormValues1,
		setErrors: (errros: FormikErrors<IFormValues1>) => void,
	) => {
		const email = values.email;
		const url = '/api/users/verify-duplicate';
		this.setState({ isLoading: true });
		this.props.apiService
			.queryPOST(url, { email })
			.then(_res => {
				this.setState(
					{ ...values, currentPage: 2, isLoading: false },
					() => {
						this.props.updateRoute(
							{
								pathname: '/auth/register',
								query: { step: 'step-2' },
							},
							'/auth/register/step-2',
						);
					},
				);
			})
			.catch(e => {
				setErrors({ email: e.message });
				this.setState({ isLoading: false });
			});
	};
	protected sumbitSecondPage = (
		values: IFormValues2,
		setErrors: (errros: FormikErrors<IFormValues2>) => void,
	) => {
		const username = values.username;
		const url = '/api/users/verify-duplicate';
		this.setState({ isLoading: true });
		this.props.apiService
			.queryPOST(url, { username })
			.then(_res => {
				this.setState({ ...values, currentPage: 3, isLoading: false });
				this.props.updateRoute(
					{
						pathname: '/auth/register',
						query: { step: 'step-3' },
					},

					'/auth/register/step-3',
				);
			})
			.catch(e => {
				setErrors({ username: e.message });
				this.setState({ isLoading: false });
			});
	};
	protected handleFristBack = (values: IFormValues2) => {
		this.setState({ currentPage: 1, ...values });
		this.props.updateRoute(
			{
				pathname: '/auth/register',
				query: { step: 'step-1' },
			},
			'/auth/register/step-1',
		);
	};
	protected handleSecondBack = (values: IFormValues3) => {
		this.setState({ currentPage: 2, ...values });
		this.props.updateRoute(
			{
				pathname: '/auth/register',
				query: { step: 'step-2' },
			},

			'/auth/register/step-2',
		);
	};
	protected handleSubmit = (verification?: string) => {
		if (verification === 'phone') {
			this.props.verifyByPhone();
		} else {
			this.props.verifyByEmail();
		}
	};
}

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;
	return {
		userData,
	};
}
export default connect(mapStateToProps)(RegisterForm);
