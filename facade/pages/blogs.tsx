import React from 'react';
import Head from 'next/head';
import buildUrl from 'build-url';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';

// import Button from '@material-ui/core/Button';
// import { Icon } from '@material-ui/core';

import { IStoreState, newBlogModalDisplay, apiError } from '../js/redux/store';

import { ApiService, AuthService } from '../js/services';

import Footer from '../js/components/general/Footer';
import LayoutDashboard from '../js/components/general/LayoutDashboard';
import FilterResults from '../js/components/general/FilterResults';
import FeaturedItemsList from '../js/components/blogs/FeaturedItemsList';
import BlogTile from '../js/components/blogs/BlogTile';
import HeroHeading from '../js/components/general/HeroHeading';
import ListLoading from '../js/components/general/ListLoading';
import SectionTitle from '../js/components/general/SectionTitle';
import isAdmin from '../js/helpers/isAdmin';

import {
	IFilters,
	IFiltersSelected,
	IFiltersGroup,
	FilterResultsHelpers,
} from '../js/components/general/FilterResults';

import {
	IApiBlog,
	IApiUser,
	IApiSituation,
	IApiTopic,
	IApiPagination,
} from 'js/interfaces';

import { GTMDataLayer } from '../js/helpers/dataLayer';

 

interface IProps {
	router: any;
	blogData: {
		featured: IApiBlog[];
		data: IApiBlog[];
		meta: IApiPagination;
	};
	userData: IApiUser;
	topicsLoading: boolean;
	topics: IApiTopic[];
	situationsLoading: boolean;
	situations: IApiSituation[];
	userAuthenticated: boolean | null;
	apiService: ApiService;
	authService: AuthService;
	updateDataCount: number;
	newBlogModalDisplay(): void;
	dispatch(action: any): void;
}

interface IState {
	isUpdating: boolean;
	isLoadingBlogs: boolean;
	currentPage: number;
	maxPages: number;
	featuredBlog: IApiBlog[];
	listingBlog: IApiBlog[];
	totalBlogs: number;
	filters: IFilters;
	selectedFilters: IFiltersSelected;
}

class Blogs extends React.Component<IProps, IState> {
	protected filterHelpers: FilterResultsHelpers = new FilterResultsHelpers();
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ query, apiService }) {
		const params: any = {};

		if ('topic' in query) {
			params[`topics[0]`] = query.topic;
		}

		const url = buildUrl(null, {
			path: '/api/blogs',
			queryParams: params,
		});

		const blogData = await (apiService as ApiService).queryGET(url);

		return {
			blogData,
		};
	}

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			isLoadingBlogs: false,
			featuredBlog: props.blogData.featured,
			listingBlog: props.blogData.data,
			totalBlogs: props.blogData.meta.total,
			filters: this.generateFilters(),
			currentPage: props.blogData.meta.current_page,
			maxPages: props.blogData.meta.last_page,
			selectedFilters: {
				filters: [],
			},
		};

		this.loadBlogs = this.loadBlogs.bind(this);
		this.handleFiltersToggle = this.handleFiltersToggle.bind(this);
		this.handleFiltersUpdate = this.handleFiltersUpdate.bind(this);
		this.handleFiltersReset = this.handleFiltersReset.bind(this);
		this.handleFiltersSubmit = this.handleFiltersSubmit.bind(this);
		this.handleCreateNewBlog = this.handleCreateNewBlog.bind(this);
		this.trackFilterUpdate = this.trackFilterUpdate.bind(this);
	}

	public componentDidMount() {
		this.updateFiltersBasedOnUrl();
	}

	public componentDidUpdate(prevProps: IProps) {
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.situations !== prevProps.situations ||
			this.props.topics !== prevProps.topics ||
			this.props.updateDataCount !== prevProps.updateDataCount
		) {
			this.setState(
				{
					filters: this.generateFilters(),
					selectedFilters: {
						filters: [],
					},
				},
				() => {
					this.updateData();
				},
			);
		}

		if (
			JSON.stringify(this.props.router.query) !==
			JSON.stringify(prevProps.router.query)
		) {
			this.updateFiltersBasedOnUrl();
		}
	}

	public render() {
		// const pageTitle =
		// 	'Cancer Blog - Stories For young adult & Teens | Canteen Connect';

		const url = buildUrl(process.env.APP_URL, {
			path: `/blogs/`,
		});

		// const seoDescription =
		// 	'Read personal stories from teenagers in Australia whose lives have been affected by cancer. Register now and connect with other Australian teens.';
		const seoFeaturedImage = `${
			process.env.STATIC_PATH
		}/images/og-facebook-default.jpg`;

		return (
			<div>
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
					<meta name="og:url" property="og:url" content={url} />
					

					<meta
						property="fb:app_id"
						content={process.env.FB_APP_ID}
					/> 
				</Head>
				<LayoutDashboard scrollBottomReached={this.loadBlogs}>
					<header className="hero theme--accent">
						<HeroHeading
							iconSlug="blogs"
							title="Blogs"
							subInfo={
								<span>
									Interested in writing a Blog post? Contact
									us{' '}
									<a
										href="mailto:online@canteen.org.au?subject=Canteen Connect - Interested in writing a Blog post"
										target="_blank"
									>
										HERE
									</a>{' '}
									with your ideas!
								</span>
							}
						/>
						{/*
						{this.props.userAuthenticated && (
							<span className="hero-actions">
								<Button
									variant="contained"
									color="primary"
									onClick={this.handleCreateNewBlog}
								>
									Write A Blog Post <Icon>add</Icon>
								</Button>
							</span>
						)}
						*/}
					</header>

					<FilterResults
						filters={this.state.filters}
						selectedFilters={this.state.selectedFilters}
						suggestTopic={true}
						onToggle={this.handleFiltersToggle}
						onUpdateFilters={this.handleFiltersUpdate}
						onReset={this.handleFiltersReset}
						onSubmit={this.handleFiltersSubmit}
						total={this.state.totalBlogs}
						totalString={{
							noResults: 'No articles',
							singular: '1 article',
							plural: 'articles',
						}}
						gtmDataLayer={this.trackFilterUpdate}
					/>

					{!!this.state.featuredBlog &&
						this.state.featuredBlog.length &&
						this.state.selectedFilters.filters.length === 0 && (
							<div className="featured_items-wrapper">
								<SectionTitle
									label="Featured Blogs"
									iconSlug="star-outline"
								/>
								<FeaturedItemsList
									featuredBlogs={this.state.featuredBlog}
									disabled={this.state.isUpdating}
								/>
							</div>
						)}

					{!!this.state.listingBlog &&
						this.state.listingBlog.length > 0 && (
							<div className="blog_listings-container">
								<SectionTitle
									label="Latest Blogs"
									iconSlug="blogs"
								/>

								{this.state.listingBlog.map((data, index) => {
									let blogAuthor: IApiUser = data.user;
									const disableBlogAuthorButton =
										data.source === 'prismic' ||
										data.user === null
											? true
											: false;

									if (
										data &&
										'user' in data &&
										data.user !== null
									) {
										blogAuthor = data.user;
									} else {
										blogAuthor = {
											full_name: data.prismic_author,
											username: data.prismic_author,
											avatar_url:
												data.prismic_author_image,
											is_banned: false,
											updated_at: null,
										};
									}

									return (
										<BlogTile
											key={index}
											blogData={data}
											disabled={this.state.isUpdating}
											blogAuthor={blogAuthor}
											disabledAuthorButton={
												disableBlogAuthorButton
											}
										/>
									);
								})}
							</div>
						)}
					<ListLoading active={this.state.isLoadingBlogs} />
					<Footer />
				</LayoutDashboard>
			</div>
		);
	}

	protected updateData() {
		// TODO api call to get the data
		// Scroll to the top (browser only)
		if (!!(process as any).browser) {
			const dashboard = window.document.querySelector(
				'.layout_dashboard-content_container',
			);
			dashboard.scroll({
				top: 0,
				left: 0,
			});
		}

		this.loadBlogs(false);
	}

	protected updateFiltersBasedOnUrl() {
		if ('topic' in this.props.router.query) {
			const selectedFilters: IFiltersSelected = {
				filters: [],
			};

			// find the topic
			const topicFromUrl: IApiTopic | null = this.props.topics.find(
				singleTopic => {
					return (
						singleTopic.slug ===
							(this.props.router.query.topic as string) &&
						singleTopic.category === 'blogs'
					);
				},
			);

			if (topicFromUrl) {
				selectedFilters.filters.push({
					paramName: 'topics',
					filterValues: [
						{
							label: topicFromUrl.title,
							value: topicFromUrl.slug,
							active: true,
						},
					],
				});
			}

			this.setState(
				{
					selectedFilters,
					filters: this.generateFilters(topicFromUrl),
				},
				() => {
					this.updateData();
				},
			);
		}
	}

	protected buildQueryParams(withPage: boolean = false) {
		const params: any = {};

		this.state.selectedFilters.filters.forEach(singleFilter => {
			if (singleFilter.filterValues instanceof Array) {
				singleFilter.filterValues.forEach((filterValue, index) => {
					params[`${singleFilter.paramName}[${index}]`] =
						filterValue.value;
				});
			} else {
				params[singleFilter.paramName] =
					singleFilter.filterValues.value;
			}
		});

		if (!this.state.filters.filterToggle) {
			params.anySituation = true;
		}

		if (withPage) {
			params.page = this.state.currentPage + 1;
		}

		return buildUrl(null, {
			path: '/api/blogs',
			queryParams: params,
		});
	}

	protected loadBlogs(append: boolean = true) {
		if (
			!this.state.isLoadingBlogs &&
			(this.state.currentPage < this.state.maxPages || !append)
		) {
			this.setState(
				{
					isUpdating: !append,
					isLoadingBlogs: true,
				},
				() => {
					const url = this.buildQueryParams(append);

					this.props.apiService
						.queryGET(url)
						.then((response: any) => {
							if (
								'data' in response &&
								'meta' in response &&
								'current_page' in response.meta &&
								'last_page' in response.meta
							) {
								let blogs: IApiBlog[] = response.data;

								if (append) {
									blogs = JSON.parse(
										JSON.stringify(this.state.listingBlog),
									);
									(response.data as IApiBlog[]).forEach(
										singleBlog => {
											blogs.push(singleBlog);
										},
									);
								}

								this.setState({
									isUpdating: false,
									isLoadingBlogs: false,
									currentPage: response.meta.current_page,
									maxPages: response.meta.last_page,
									totalBlogs: response.meta.total,
									listingBlog: blogs,
								});
							} else {
								this.props.dispatch(
									apiError(['Invalid data returned by API']),
								);
							}
						})
						.catch(error => {
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
							this.setState({
								isUpdating: false,
								isLoadingBlogs: false,
							});
						});
				},
			);
		}
	}

	protected handleFiltersToggle() {
		const filters: IFilters = JSON.parse(
			JSON.stringify(this.state.filters),
		);

		filters.filterToggle = !filters.filterToggle;

		if (filters.filterToggle) {
			if (this.props.userAuthenticated) {
				const cancerTypesIndex = filters.filterGroups.findIndex(
					value => {
						return value.paramName === 'situations';
					},
				);
				if (cancerTypesIndex >= 0) {
					filters.filterGroups.splice(cancerTypesIndex, 1);
				}

				// Remove the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					filters.filterGroups[
						topicsIndex
					].filters = filters.filterGroups[
						topicsIndex
					].filters.filter(filterValue => {
						const result = this.props.topics.find(topic => {
							return (
								topic.category === 'blogs' &&
								topic.slug === filterValue.value &&
								(topic.is_user_topic ||
									isAdmin(this.props.userData))
							);
						});
						return !!result;
					});
				}
			}
		} else {
			if (this.props.userAuthenticated) {
				// Add the situations at the beginning
				const situationsGroup: IFiltersGroup = {
					label: null,
					noBorder: true,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: !!this.props.userData.situations.find(
									userSituation => {
										return (
											userSituation.name ===
											situation.name
										);
									},
								),
							};
						}),
				};
				filters.filterGroups.unshift(situationsGroup);

				// add the topics that aren't the user's
				const topicsIndex = filters.filterGroups.findIndex(value => {
					return value.paramName === 'topics';
				});
				if (topicsIndex >= 0) {
					this.props.topics.forEach(topic => {
						if (
							!topic.is_user_topic &&
							!isAdmin(this.props.userData) &&
							topic.category === 'blogs'
						) {
							filters.filterGroups[topicsIndex].filters.push({
								label: topic.title,
								value: topic.slug,
								active: false,
							});
						}
					});
				}
			}
		}

		this.setState({
			filters,
		});
	}

	protected handleFiltersUpdate(filters: IFilters) {
		this.setState({
			filters,
		});
	}

	protected handleFiltersReset() {
		this.setState({
			selectedFilters: {
				filters: [],
			},
			filters: this.generateFilters(),
		});
	}

	protected handleFiltersSubmit(selectedFilters: IFiltersSelected) {
		this.setState(
			{
				selectedFilters,
				filters: this.filterHelpers.updateFiltersBasedOnSelectedFilters(
					selectedFilters,
					this.state.filters,
				),
			},
			() => {
				this.updateData();
			},
		);
	}

	protected handleCreateNewBlog() {
		this.props.dispatch(newBlogModalDisplay());
	}

	protected generateFilters(topicFromUrl?: IApiTopic | null): IFilters {
		const filters: IFilters = {
			filtersTitle: 'Filter',
			withToggle: this.props.userAuthenticated,
			filterToggleLabel:
				'Only show blogs directly related to my cancer experience',
			filterToggle: true,
			filterGroups: [],
		};

		if (!this.props.situationsLoading && !this.props.topicsLoading) {
			// Add the situations for the un-authenticated users
			if (!this.props.userAuthenticated) {
				filters.filterGroups.push({
					label: null,
					paramName: 'situations',
					filters: this.props.situations
						.filter(situation => {
							return situation.parent_situation === null;
						})
						.map(situation => {
							return {
								label: situation.name,
								value: situation.name,
								active: false,
							};
						}),
				});
			}

			// Add the topics
			filters.filterGroups.push({
				label: null,
				paramName: 'topics',
				filters: this.props.topics
					.filter(topic => {
						return (
							topic.category === 'blogs' &&
							(!this.props.userAuthenticated ||
								isAdmin(this.props.userData) ||
								topic.is_user_topic)
						);
					})
					.map(topic => {
						let active = false;
						if (topicFromUrl) {
							active = topic.slug === topicFromUrl.slug;
						}

						return {
							label: topic.title,
							value: topic.slug,
							active,
						};
					}),
			});
		}

		return filters;
	}

	protected trackFilterUpdate(value: string) {
		this.GTM.pushEventToDataLayer({
			event: 'filterBlogs',
			filter: value,
		});
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData } = state.user;
	const { newBlogModalDisplay } = state.newBlogModal;
	const { situations } = state.situations;
	const { topics } = state.topics;
	const { updateDataCount } = state.updateData;

	return {
		userData,
		userAuthenticated,
		newBlogModalDisplay,
		situations,
		situationsLoading: state.situations.loading,
		topics,
		topicsLoading: state.topics.loading,
		updateDataCount,
	};
}

export default connect(mapStateToProps)(withRouter(Blogs as any));
