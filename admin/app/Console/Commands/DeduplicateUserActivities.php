<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\UserActivity;

class DeduplicateUserActivities extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:deduplicateUserActivities {activityType : The type of activity}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Deduplicate user activities';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$activityType = $this->argument('activityType');
		$activitiesToDelete = [];
		// The activity threshold in seconds
		$activityDuplicateThreshold = 5;

		// Loop through all the activities
		$this->line('');
		$this->info('Looping through all activities');

		$activitiesQuery = UserActivity::where('type', $activityType)->orderBy(
			'created_at',
			'ASC'
		);

		$bar = $this->output->createProgressBar($activitiesQuery->count());
		$bar->setRedrawFrequency(10);

		$activitiesQuery->chunk(10, function ($activities) use (
			$bar,
			$activityDuplicateThreshold,
			&$activitiesToDelete
		) {
			foreach ($activities as $activity) {
				$this->deduplicateActivity(
					$activity,
					$activityDuplicateThreshold,
					$activitiesToDelete
				);
				$bar->advance();
			}
		});
		$bar->finish();
		$this->info(
			"\r\nFinished looping through activities, deleting activities"
		);
		UserActivity::whereIn('id', $activitiesToDelete)->delete();
		$this->info(count($activitiesToDelete) . ' activities deleted');
	}

	protected function deduplicateActivity(
		UserActivity $activity,
		$activityDuplicateThreshold,
		&$activitiesToDelete
	) {
		// Look for similar activities in the $activityDuplicateThreshold
		$activityIds = UserActivity::where('type', $activity->type)
			->where('id', '!=', $activity->id)
			->where('user_id', $activity->user_id)
			->where('activity_type', $activity->activity_type)
			->where('activity_id', $activity->activity_id)
			->whereBetween('created_at', [
				$activity->created_at,
				$activity->created_at->addSeconds($activityDuplicateThreshold),
			])
			->pluck('id')
			->toArray();

		if (sizeof($activityIds)) {
			$activitiesToDelete = array_merge(
				$activitiesToDelete,
				$activityIds
			);
		}
	}
}
