import React from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import Router from 'next/router';

import { withRouter } from 'next/router';

import ReactSVG from 'react-svg';

import {
	IStoreState,
	apiError,
	apiErrorDismiss,
	discussionModalDisplay,
	apiSuccess,
	updateData,
} from '../js/redux/store';

import {
	IApiDiscussion,
	IApiTopic,
	IApiUser,
	IReplyTo,
} from '../js/interfaces';

import { ApiService, AuthService } from '../js/services';

import { checkPage404 } from '../js/helpers/checkPage404';
import LayoutDashboard from '../js/components/general/LayoutDashboard';
import DiscussionDetail from '../js/components/discussions/DiscussionDetail';
import DiscussionReply from '../js/components/discussions/DiscussionReply';
import DiscussionsList from '../js/components/discussions/DiscussionsList';
import FixedBackBar from '../js/components/general/FixedBackBar';
import ModalDialogDeleteConfirm from '../js/components/modals/ModalDialogDeleteConfirm';
import { ButtonBaseLink } from '../js/components/general/ButtonBaseLink';
import { ModalDialog } from '../js/components/modals/local';

import { GTMDataLayer } from '../js/helpers/dataLayer';

import { TOrder } from 'js/components/general/VirtualisedList';

interface IProps {
	router: any;
	userAuthenticated: boolean | null;
	userData: IApiUser;
	discussion: IApiDiscussion | null;
	apiService: ApiService;
	authService: AuthService;
	updateDataCount: number;
	userAdmin: boolean;
	dispatch(action: any): void;
}

export interface IAdditionalActions {
	label: string;
	action(): void;
}

interface IState {
	isUpdating: boolean;
	submitting: boolean;
	discussion: IApiDiscussion | null;
	userAuthenticated: boolean | null;
	topic: IApiTopic | null;
	replyTo: IReplyTo | null;
	scrollingElement: Element | null;
	isServer: boolean;
	scrollToIndex: number;
	deleteConfirmModalActive: boolean;
	displaReplyTextarea: boolean;
	discussionToDelete: IApiDiscussion | null;
	order: TOrder;
}

const getReplyIdFromParam = () => {
	const urlParams = new URLSearchParams(window.location.search);
	const replyId = urlParams.get('replyId');

	return parseInt(replyId);
};

const DISCUSSION_ORDER_KEY = 'discussion_order';
const DEFAULT_ORDER: TOrder = 'DESC';

class DiscussionSingle extends React.Component<IProps, IState> {
	protected discussionListRef = null;
	protected oldReplyTo: IReplyTo | null = null;
	protected replyResolve: any = null;
	protected GTM = new GTMDataLayer();

	static async getInitialProps({ req, query, apiService }) {
		const result = await checkPage404(
			req,
			query,
			'id',
			async (discussionId: string) => {
				const discussion = await (apiService as ApiService).queryGET(
					`/api/discussions/${discussionId}`,
					'discussion',
				);
				// const repliesData = await (apiService as ApiService).queryGET(`/api/discussions/${discussionId}/replies`);

				return {
					discussion,
					// repliesData,
				};
			},
		);

		if (!result) {
			return {
				discussion: null,
				// repliesData: null,
			};
		}

		return result;
	}

	constructor(props: IProps) {
		super(props);

		this.state = {
			isUpdating: false,
			submitting: false,
			userAuthenticated: props.userAuthenticated,
			discussion: props.discussion,
			topic: null,
			replyTo: null,
			scrollingElement: null,
			isServer: true,
			scrollToIndex: -1,
			deleteConfirmModalActive: false,
			displaReplyTextarea: true,
			discussionToDelete: null,
			order: DEFAULT_ORDER,
		};

		this.handleOnUrlChange = this.handleOnUrlChange.bind(this);
		this.replyToDiscussion = this.replyToDiscussion.bind(this);
		this.submitReply = this.submitReply.bind(this);
		this.editDiscussion = this.editDiscussion.bind(this);
		this.deleteDiscussion = this.deleteDiscussion.bind(this);
		this.toggleDeleteConfirmModal = this.toggleDeleteConfirmModal.bind(
			this,
		);
		this.handleOrderChange = this.handleOrderChange.bind(this);
		this.reloadPage = this.reloadPage.bind(this);
	}

	public componentDidMount() {
		if (typeof window !== 'undefined') {
			this.setState(
				{
					isServer: false,
					scrollingElement: document.querySelector(
						'#scroll-container',
					),
					order: this.getOrderFromLocalStorage(),
				},
				() => {
					let scrollToIndex = -1;

					const replyId = getReplyIdFromParam();

					if (replyId) {
						scrollToIndex = replyId;
					}

					this.setState({
						scrollToIndex,
					});
				},
			);

			Router.events.on('routeChangeComplete', url => {
				if (url) {
					const urlSplit = url.split('/');
					if (urlSplit.length >= 3) {
						this.updateData(urlSplit[2]);
					}
				}
			});
		}
	}

	public componentWillUnmount() {
		window.removeEventListener('hashchange', this.handleOnUrlChange);
	}

	public handleOnUrlChange() {
		let scrollToIndex = -1;

		if (
			window.location.hash &&
			!isNaN(parseInt(window.location.hash.substr(1)))
		) {
			scrollToIndex = parseInt(window.location.hash.substr(1));
		}

		this.setState({
			scrollToIndex,
		});
	}

	public componentDidUpdate(prevProps: IProps) {
		if (!this.props.userAuthenticated && this.props.discussion.private) {
			Router.push(
				`/auth/login?redirect=/discussions/${this.props.discussion.slug}`,
			);
		}
		if (
			this.props.userAuthenticated !== prevProps.userAuthenticated ||
			this.props.updateDataCount !== prevProps.updateDataCount
		) {
			this.updateData();
		}

		const replyId = getReplyIdFromParam();

		if (replyId && replyId !== this.state.scrollToIndex) {
			this.setState({
				scrollToIndex: replyId,
			});
		}
	}

	public replyToDiscussion(_event, replyTo?: IReplyTo): Promise<boolean> {
		if (
			this.oldReplyTo &&
			(!replyTo || this.oldReplyTo.id !== replyTo.id)
		) {
			this.oldReplyTo = null;
			if (this.replyResolve) {
				this.replyResolve(false);
				this.replyResolve = null;
			}
		}
		return new Promise(resolve => {
			if (replyTo) {
				this.replyResolve = resolve;
				this.oldReplyTo = replyTo;
				this.setState({
					replyTo: replyTo,
				});
			} else {
				this.setState({
					replyTo: null,
				});
			}
		});
	}

	public render() {
		const { for_event = 0 } = this.state.discussion;
		const url = `${process.env.APP_URL}/blogs/${this.props.discussion.slug}`;
		console.log(this.state.discussion);
		//Meta Formatting
		// const removeHTMLandTruncate = (
		// 	content: string,
		// 	stringLength: number,
		// ) => {
		// 	const replaceTags = content.replace(/<(?:.|\n)*?>/gm, '');
		// 	const limitContent = replaceTags.substring(0, stringLength);

		// 	return limitContent;
		// };

		const discussionReplyClasses = ['discussion_single-content'];

		if (this.props.userAuthenticated) {
			discussionReplyClasses.push(
				'discussion_single-content--reply_active',
			);
		}

		// const seoTitle = this.props.discussion.title;
		// const seoDescription = removeHTMLandTruncate(
		// 	this.props.discussion.content,
		// 	300,
		// );
		const seoFeaturedImage = `${process.env.STATIC_PATH}/images/og-facebook-default.jpg`;

		return (
			<div className="discussion_single">
				<Head>
					<title>{process.env.SEO_TITLE}</title>
					<meta
						name="description"
						content={process.env.SEO_DESCRIPTION}
					/>
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>

					<meta name="og:url" property="og:url" content={url} />
					<meta name="og:type" property="og:type" content="article" />

					<meta
						name="og:image"
						property="og:image"
						content={seoFeaturedImage}
					/>
				</Head>
				<LayoutDashboard>
					<div className={discussionReplyClasses.join(' ')}>
						<div className="discussion_single-content-list_container">
							<div
								className="discussion_single-content-list"
								id="scroll-container"
							>
								{/* The fixed bar */}
								<FixedBackBar
									href="/discussions"
									label="All Discussions"
								/>
								{!!this.state.discussion && (
									<div>
										{for_event === 1 && (
											<ButtonBaseLink
												hrefAs={`/events/${this.state.discussion.slug}`}
												href={`/events-single/?id=${this.state.discussion.slug}`}
											>
												<span className="discussion_single-back-to-event">
													Back to event
												</span>
											</ButtonBaseLink>
										)}
										{/* The main discussion */}
										<DiscussionDetail
											topic={this.state.topic}
											isUpdating={this.state.isUpdating}
											isFollowing={
												this.state.discussion.followed
											}
											discussion={this.state.discussion}
											replyToDiscussion={
												this.replyToDiscussion
											}
											scrollToReplies={
												this.scrollToReplies
											}
											editDiscussion={() => {
												this.editDiscussion();
											}}
											deleteDiscussion={() => {
												this.toggleDeleteConfirmModal();
											}}
										/>

										{/* The discussion replies */}
										<div
											className="discussion_single-replies hp-64"
											id="replies"
										>
											{!this.state.isServer &&
												this.state.discussion
													.replies_count > 0 && (
													<DiscussionsList
														ref={discussionList => {
															this.discussionListRef = discussionList;
														}}
														mainDiscussion={
															this.state
																.discussion
														}
														scrollingElement={
															this.state
																.scrollingElement
														}
														order={this.state.order}
														openReply={
															this
																.replyToDiscussion
														}
														scrollToIndex={
															this.state
																.scrollToIndex
														}
														editDiscussion={
															this.editDiscussion
														}
														deleteDiscussion={
															this
																.toggleDeleteConfirmModal
														}
														handleOrderChange={
															this
																.handleOrderChange
														}
													/>
												)}
										</div>
									</div>
								)}
							</div>
						</div>
						<div className="discussion_single-content-reply">
							{!!this.props.userData &&
								!!this.props.userAuthenticated && (
									<DiscussionReply
										user={this.props.userData}
										isReplyOpen={
											this.props.userAuthenticated
										}
										replyTo={this.state.replyTo}
										replyToDiscussion={
											this.replyToDiscussion
										}
										submitReply={this.submitReply}
										reloadPage={this.reloadPage}
									/>
								)}
						</div>
					</div>
				</LayoutDashboard>

				{this.state.deleteConfirmModalActive && (
					<ModalDialog
						isActive={true}
						modalTitle={
							this.state.discussionToDelete
								? 'Are you sure you want to delete the reply?'
								: 'Are you sure you want to delete the below discussion?'
						}
						ariaTag="delete-alert-modal"
						handleClose={() => {
							this.toggleDeleteConfirmModal();
						}}
					>
						<ModalDialogDeleteConfirm
							discussionToDelete={this.state.discussionToDelete}
							name={this.props.discussion.title}
							loading={this.state.isUpdating}
							submit={this.deleteDiscussion}
							cancel={this.toggleDeleteConfirmModal}
						/>
					</ModalDialog>
				)}
			</div>
		);
	}

	protected scrollToReplies() {
		if (document.querySelector('.discussion_detail')) {
			let element = document.querySelector<HTMLInputElement>(
				'.discussion_detail',
			);
			let elementHeight = element.offsetHeight;
			document.querySelector(
				'#scroll-container',
			).scrollTop = elementHeight;
		}
	}

	protected renderFixedBar() {
		return (
			<div className="fixed_bar theme--accent_dark">
				<ButtonBaseLink
					className="fixed_bar-button fixed_bar-button--move-left"
					href="/discussions"
				>
					<ReactSVG
						className="fixed_bar-back_icon"
						path={`${process.env.STATIC_PATH}/icons/back-arrow-2.svg`}
					/>
					All Discussions
					<span className="fixed_bar-button-border" />
				</ButtonBaseLink>
			</div>
		);
	}

	protected updateData(slug?) {
		// API call to get the data
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				const slugValue = slug ? slug : this.props.router.query.id;

				this.props.apiService
					.queryGET(`/api/discussions/${slugValue}`)
					.then((data: any) => {
						this.setState({
							isUpdating: false,
							discussion: data.discussion,
						});
					})
					.catch(error => {
						// dispatch the error message
						this.props.dispatch(apiError([error.message]));
						this.setState({
							isUpdating: false,
						});
					});
			},
		);
	}

	protected submitReply(values, replyId?: number): Promise<boolean> {
		this.props.dispatch(apiErrorDismiss());
		let commentId = replyId ? replyId : this.props.discussion.id;

		return new Promise(resolve => {
			this.setState(
				{
					submitting: true,
				},
				() => {
					this.props.apiService
						.queryPOST(
							`/api/discussions/${commentId}/reply`,
							values,
							'discussion',
						)
						.then((discussion: IApiDiscussion) => {
							this.setState(
								{
									submitting: false,
									discussion: discussion.main_discussion,
								},
								() => {
									this.GTM.pushEventToDataLayer({
										event: 'replyDiscussion',
										id: commentId,
									});
									this.props.dispatch(
										apiSuccess(['Replied successfully']),
									);
									if (!replyId) {
										window.setTimeout(() => {
											this.setState(
												{
													scrollToIndex:
														this.state.order ===
															'ASC'
															? discussion.discussion_index
															: 0,
												},
												() => {
													resolve(true);
													this.props.dispatch(
														updateData(),
													);
												},
											);
										}, 300);
									} else {
										if (this.replyResolve) {
											this.replyResolve(true);
											this.replyResolve = null;
											if (this.oldReplyTo) {
												this.oldReplyTo = null;
											}
										}
										resolve(true);
									}
								},
							);
						})
						.catch(error => {
							this.setState(
								{
									submitting: false,
								},
								() => {
									resolve(false);
								},
							);
							// dispatch the error message
							this.props.dispatch(apiError([error.message]));
						});
				},
			);
		});
	}

	protected editDiscussion(discussion?: IApiDiscussion) {
		this.props.dispatch(
			discussionModalDisplay(
				discussion ? discussion : this.state.discussion,
			),
		);
	}

	protected toggleDeleteConfirmModal(
		discussion: IApiDiscussion | null = null,
	) {
		this.setState({
			deleteConfirmModalActive: !this.state.deleteConfirmModalActive,
			discussionToDelete: discussion,
		});
	}

	//TODO: Update after fixed API
	protected deleteDiscussion() {
		this.setState(
			{
				isUpdating: true,
			},
			() => {
				const discussionId = this.state.discussionToDelete
					? this.state.discussionToDelete.id
					: this.props.discussion.id;
				this.props.apiService
					.queryDELETE(`/api/discussions/${discussionId}`)
					.then(() => {
						this.setState(
							{
								isUpdating: false,
								deleteConfirmModalActive: false,
							},
							() => {
								if (this.state.discussionToDelete) {
									this.props.dispatch(
										apiSuccess([
											'Reply successfully deleted',
										]),
									);
									this.reloadPage(
										this.state.discussionToDelete
											? this.state.discussionToDelete
												.discussion_index
											: null,
									);
								} else {
									this.props.dispatch(
										apiSuccess([
											'Discussion successfully deleted',
										]),
									);
									Router.push('/discussions');
								}
							},
						);
					})
					.catch(error => {
						this.props.dispatch(apiError(error));

						this.setState({
							deleteConfirmModalActive: false,
							isUpdating: false,
						});
					});
			},
		);
	}

	protected reloadPage = (index?: number) => {
		if (typeof window !== 'undefined') {
			if (typeof index === 'undefined' || index === null) {
				window.location.reload();
			} else {
				console.log(index, this.state.discussion.direct_replies_count);
				if (index >= this.state.discussion.direct_replies_count - 2) {
					index = this.state.discussion.direct_replies_count - 2;
				}
				if (index < 0) {
					window.location.reload();
				} else {
					window.location.href = `/discussions/${this.state.discussion.slug}?replyId=${index}`;
				}
			}
		}
	};

	protected getOrderFromLocalStorage(): TOrder {
		if (typeof window !== 'undefined') {
			const rawOrder = localStorage.getItem(DISCUSSION_ORDER_KEY);
			if (rawOrder === 'ASC' || rawOrder === 'DESC') {
				return rawOrder;
			}
		}
		return DEFAULT_ORDER;
	}

	protected handleOrderChange(order: TOrder) {
		this.setState(
			{
				order,
			},
			() => {
				if (typeof window !== 'undefined') {
					localStorage.setItem(DISCUSSION_ORDER_KEY, order);
				}
			},
		);
	}
}

function mapStateToProps(state: IStoreState) {
	const { userAuthenticated, userData, userAdmin } = state.user;
	const { updateDataCount } = state.updateData;

	return {
		userAuthenticated,
		userData,
		updateDataCount,
		userAdmin,
	};
}

export default connect(mapStateToProps)(withRouter(DiscussionSingle as any));
