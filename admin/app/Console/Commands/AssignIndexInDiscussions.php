<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Discussion;

class AssignIndexInDiscussions extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:assignIndexInDiscussions {discussion? : The discussion ID to index}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		if ($this->argument('discussion')) {
			$discussionId = intval($this->argument('discussion'));
			try {
				$discussion = Discussion::findOrFail($discussionId);
				// Index only the specified discussion
				$this->indexDiscussion($discussion);
			} catch (\Exception $e) {
				$this->error("No discussion with ID: {$discussionId}");
			}
		} else {
			$this->line('');
			$this->info('Indexing all discussions');
			$bar = $this->output->createProgressBar(
				Discussion::whereNull('main_discussion_id')->count()
			);
			// Index all the discussions
			Discussion::whereNull('main_discussion_id')->chunk(10, function (
				$discussions
			) use ($bar) {
				foreach ($discussions as $discussion) {
					$this->indexDiscussion($discussion);
					$bar->advance();
				}
			});
			$bar->finish();
			$this->info("\r\nFinished indexing all discussions");
			$this->line('');
		}
	}

	protected function indexDiscussion(Discussion $discussion) {
		if (!is_null($discussion->main_discussion_id)) {
			$this->warn(
				"\r\nDiscussion with ID: {$discussion->id} is not a main discussion"
			);
			return;
		}
		// Get all the direct child discussions
		$index = 0;
		$discussion
			->repliesDiscussions()
			->orderBy('created_at', 'ASC')
			->chunk(10, function ($discussions) use (&$index) {
				foreach ($discussions as $discussion) {
					$discussion->discussion_index = $index;
					$discussion->save(['timestamps' => false]);
					// Loop through all the discussions in the thread
					$discussion
						->repliesThread()
						->chunk(10, function ($discussions) use ($index) {
							foreach ($discussions as $discussion) {
								$discussion->discussion_index = $index;
								$discussion->save(['timestamps' => false]);
							}
						});
					++$index;
				}
			});
	}
}
