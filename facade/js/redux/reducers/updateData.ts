import { actionTypes } from '../actions';

export interface IStoreState {
	updateDataCount: number;
}

export const updateData = (
	state: IStoreState = {
		updateDataCount: 0,
	},
	action,
): IStoreState => {
	switch (action.type) {
		case actionTypes.UPDATE_DATA:
			return Object.assign({}, state, {
				updateDataCount: state.updateDataCount + 1,
			});

		default:
			return state;
	}
};
