import Quill from 'quill';
import algoliasearch from 'algoliasearch';
import { IApiSettings } from '../interfaces';

import './QuillMentionBlot';
import './QuillHashtagBlot';

interface IOptions {
	isActive: boolean;
	prefixCharacter: string[];
	maxCharacter: number;
	dataAttributes: string[];
	bounds: string | null;
	searchConfig: IApiSettings;
}

export interface ISearchResult {
	id: number;
	label: string;
	username: string;
}

interface IAlgoliaUserResults {
	options: ISearchResult[] | null;
}

interface IHashtagResult {
	label: string;
	type: string;
}

interface IAlgoliaHashtagResults {
	options: IHashtagResult[] | null;
}

export default class QuillMention {
	protected quill: Quill;
	protected options: IOptions;

	protected algoliaClient: algoliasearch.Client = null;
	protected algoliaIndex: algoliasearch.Index = null;

	protected cursorPosition: number;
	protected mentionCharIndex: number;
	protected mentionContainer: HTMLDivElement;
	protected mentionContainerRef: HTMLElement;
	protected mentionListElement: HTMLUListElement;
	protected editorElement: Element;
	protected isMentionAppended: boolean = false;
	protected textAfterLength: number;
	protected keyCharacter: string;

	protected itemIndex: number = 0;

	constructor(quill: Quill, options: any) {
		// save the quill reference
		this.quill = quill;
		this.options = options;

		// bind handlers to this instance
		this.onTextChange = this.onTextChange.bind(this);
		this.renderListElement = this.renderListElement.bind(this);
		this.attachDataValues = this.attachDataValues.bind(this);

		if (
			this.options.searchConfig.algolia_app_id &&
			this.options.searchConfig.algolia_search_key &&
			this.options.searchConfig.config &&
			this.options.searchConfig.search_index
		) {
			//Init algolia index
			this.algoliaClient = algoliasearch(
				this.options.searchConfig.algolia_app_id,
				this.options.searchConfig.algolia_search_key,
			);
			this.algoliaIndex = this.algoliaClient.initIndex(
				this.options.searchConfig.search_index,
			);
		} else {
			console.warn(
				'[Missing config] Algolia search details have not been passed to module',
			);

			return;
		}

		// Appends created div if active prop has been passed from the config;
		if (this.options.isActive) {
			//window.addEventListener('keydown', this.keyboardListen, false);
			this.quill.on('editor-change', this.onTextChange);

			this.createMentionContainer();
		} else {
			console.warn(
				'[Missing config] isActive property needs to be passed to initialise the module',
			);
		}
	}

	public onTextChange(_delta, _oldDelta, _source): void {
		this.actionMention();
	}

	private actionMention(): void {
		// Get Quill Range
		const range = this.quill.getSelection();

		//If no quill range then return and kill
		if (!range) return;

		// Append to editor
		this.mentionContainerRef = document.querySelector(
			`${this.options.bounds} .mention_list`,
		);

		// Assign cursor posiution to scope
		this.cursorPosition = range.index;

		const startPosition = Math.max(
			0,
			this.cursorPosition - this.options.maxCharacter,
		);

		const beforeCursorPosition = this.quill.getText(
			startPosition,
			this.cursorPosition - startPosition,
		);

		const mentionCharIndex = this.options.prefixCharacter.reduce(
			(prev, cur) => {
				const previousIndex = prev;
				const mentionIndex = beforeCursorPosition.lastIndexOf(cur);

				return mentionIndex > previousIndex
					? mentionIndex
					: previousIndex;
			},
			-1,
		);

		if (mentionCharIndex > -1) {
			this.mentionCharIndex =
				this.cursorPosition -
				(beforeCursorPosition.length - mentionCharIndex);

			const mentionCharPos =
				this.cursorPosition -
				(beforeCursorPosition.length - mentionCharIndex);
			this.mentionCharIndex = mentionCharPos;

			const textAfter = beforeCursorPosition.substring(
				mentionCharIndex + 1,
			);

			this.keyCharacter = beforeCursorPosition[mentionCharIndex];

			if (textAfter.length > 0) {
				this.textAfterLength = textAfter.length;

				if (this.keyCharacter === '@') {
					this.handleAtMention(textAfter);
				} else if (this.keyCharacter === '#') {
					this.handleHashtag(textAfter);
				}
			}
		} else {
			this.hideMentionList();

			return;
		}
	}

	private handleAtMention(textAfter: string): void {
		this.handleUserSearch(textAfter)
			.then((results: IAlgoliaUserResults) => {
				if (
					results &&
					'options' in results &&
					results.options.length > 0
				) {
					this.renderListElement(results.options);
				} else {
					this.renderNoResults(textAfter);
				}
			})
			.catch(() => {
				this.hideMentionList();
			});
	}

	private handleHashtag(textAfter: string): void {
		this.handleHashtagSearch(textAfter)
			.then(results => {
				if (
					results &&
					'options' in results &&
					results.options.length > 0
				) {
					this.renderListElement(results.options);
					if (textAfter && !this.matchesRegex(textAfter)) {
						this.injectNewHashtag(textAfter);
					}
				} else {
					if (textAfter && !this.matchesRegex(textAfter)) {
						this.injectNewHashtag(textAfter);
					}
				}
			})
			.catch(() => {
				this.hideMentionList();
			});
	}

	private handleUserSearch(inputValue: string): Promise<IAlgoliaUserResults> {
		if (inputValue) {
			return new Promise((resolve, reject) => {
				this.algoliaIndex
					.search({
						query: inputValue,
						facetFilters: ['type:user'],
						hitsPerPage: 3,
					})
					.then(data => {
						const results: IAlgoliaUserResults = {
							options: null,
						};

						if ('hits' in data && data.hits instanceof Array) {
							results.options = data.hits.map(hit => {
								return {
									label: hit.title,
									id: hit.id,
									username: hit.id,
								};
							});
						}

						resolve(results);
					})
					.catch(error => {
						console.warn(`User autocomplete error:`, error);
						reject(error);
					});
			});
		} else {
			return;
		}
	}

	private handleHashtagSearch(
		inputValue: string,
	): Promise<IAlgoliaHashtagResults> {
		if (inputValue) {
			return new Promise((resolve, reject) => {
				this.algoliaIndex
					.search({
						query: inputValue,
						facetFilters: ['type:hashtag'],
						hitsPerPage: 10,
					})
					.then(data => {
						const results: IAlgoliaHashtagResults = {
							options: null,
						};

						if ('hits' in data && data.hits instanceof Array) {
							results.options = data.hits.map(hit => {
								return {
									type: hit.type,
									label: hit.title,
								};
							});
						}

						resolve(results);
					})
					.catch(error => {
						console.warn(`User hashtag error:`, error);
						reject(error);
					});
			});
		} else {
			return;
		}
	}

	private showMentionList(): void {
		this.mentionContainerRef.style.display = 'flex';

		this.setMentionContainerPosition();
	}

	private hideMentionList(): void {
		this.mentionContainerRef.style.display = 'none';

		this.isMentionAppended = false;
	}

	private setMentionContainerPosition(): void {
		const paddingOffset = 4;
		const mentionCharPos = this.quill.getBounds(this.mentionCharIndex);

		this.mentionContainer.style.left = `${mentionCharPos.left}px`;
		this.mentionContainer.style.top = `${mentionCharPos.bottom +
			paddingOffset}px`;
	}

	private renderListElement(data: ISearchResult[] | IHashtagResult[]): void {
		if (data && data.length) {
			this.mentionListElement.innerHTML = '';
			this.mentionListElement.id = 'mention_list-list';

			for (let i = 0; i < data.length; i += 1) {
				const listItem: HTMLLIElement = document.createElement('li');
				listItem.className = 'mention_list-item';

				listItem.dataset.index = String(i);
				listItem.innerHTML = this.renderItem(data[i]);

				listItem.setAttribute('tabindex', String(i));

				listItem.onclick = this.onItemClick.bind(this);

				this.mentionListElement.appendChild(
					this.attachDataValues(listItem, data[i]),
				);
			}

			this.showMentionList();
		}
	}

	private injectNewHashtag(textAfter: string): void {
		this.quill.deleteText(this.mentionCharIndex, this.textAfterLength + 1);

		const newHashtag = textAfter;

		// Embed blot
		this.quill.insertEmbed(
			this.cursorPosition,
			'hashtag',
			{ label: newHashtag },
			'user',
		);

		this.quill.insertText(this.cursorPosition + 1, ' ');

		// Reset cursor position so the user can continue typing
		this.quill.setSelection((this.mentionCharIndex + 2) as any, 'user');
	}

	private renderNoResults(inputValue: string): void {
		this.mentionListElement.innerHTML = '';
		this.mentionListElement.id = 'mention_list-list';

		const listItem: HTMLLIElement = document.createElement('li');
		listItem.className = 'mention_list-item';
		listItem.innerHTML = `<span>There a no results matching: ${inputValue}</span>`;

		this.mentionListElement.appendChild(listItem);
	}

	protected renderItem(value: ISearchResult | IHashtagResult): string {
		return `${this.keyCharacter === '#' ? this.keyCharacter : ''}${
			value.label
		}`;
	}

	private onItemClick(event): void {
		// Prevent default actions
		event.stopImmediatePropagation();
		event.preventDefault();

		// Get index from dataset
		this.itemIndex = event.currentTarget.dataset.index;

		// Check what is the key character
		if (this.keyCharacter === '@') {
			// Get username from dataset
			const username = (event as any).target.dataset['username'];
			this.quill.insertText(this.cursorPosition, ' ');

			this.quill.insertEmbed(
				this.cursorPosition,
				'mention',
				{ username },
				'user',
			);
		} else if (this.keyCharacter === '#') {
			// Get username from dataset
			const label = (event as any).target.dataset['label'];

			this.quill.insertEmbed(
				this.cursorPosition,
				'hashtag',
				{ label },
				'user',
			);
		}

		// Reset cursor position so the user can continue typing
		this.quill.setSelection(this.mentionCharIndex as any, 'user');

		// new index();
		const { index } = this.quill.getSelection();

		this.quill.insertText(index, '', 'silent');

		// Set amount of caharacters to delete with special character
		const amountToDelete = this.textAfterLength + 1;

		// Delete text search query
		this.quill.deleteText(index, amountToDelete);

		// Set selection to after blot
		this.quill.setSelection((this.mentionCharIndex + 1) as any, 'user');
	}

	private createMentionContainer(): void {
		// Create mention container element
		this.mentionContainer = document.createElement('div');
		this.mentionContainer.classList.add('mention_list');
		this.mentionContainer.id = 'mention_list';
		this.mentionContainer.style.position = 'absolute';
		this.mentionContainer.style.zIndex = '99';

		// Get editor
		const editor = document.querySelector(
			`${this.options.bounds} .ql-container`,
		);

		if (!!editor) {
			editor.appendChild(this.mentionContainer);

			// Create UL Element to hold dynamic li elements
			this.createMentionUnorderedList();
		}
	}

	private createMentionUnorderedList(): void {
		this.mentionListElement = document.createElement('ul');
		this.mentionListElement.className = 'mention_list-list';
		this.mentionContainer.appendChild(this.mentionListElement);
	}

	private attachDataValues(element: HTMLLIElement, data): HTMLLIElement {
		const keys = Object.keys(data);
		const mention = element;

		if (keys && keys.length && this.options.dataAttributes.length) {
			keys.forEach(key => {
				if (this.options.dataAttributes.indexOf(key) > -1) {
					mention.dataset[key] = data[key];
				} else {
					delete mention.dataset[key];
				}
			});
		}

		return mention;
	}

	private matchesRegex(value: string) {
		const regex = new RegExp('^[a-zA-Z]+$');

		return regex.test(value);
	}
}
