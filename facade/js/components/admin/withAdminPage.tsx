import React       from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';

import { IStoreState } from '../../redux/store';
import { IApiPermission } from '../../interfaces';

import { ApiService, AuthService } from '../../services';

import MyError from '../../../pages/_error';

export interface IAdminProps {
	userAuthenticated: boolean|null;
	userAdmin: boolean;
	apiService: ApiService;
	authService: AuthService;
	query: any;
	roleName : string;
	permissions : IApiPermission[];
	dispatch(action: any): void;
}

export interface IAdminState {
}

export default function withAdminPage(WrappedComponent: any) {
	class AdminPage extends React.Component<IAdminProps, IAdminState> {
		static async getInitialProps({query}) {
			return {
				query,
			};
		}

		public componentDidMount() {
		
			if (!this.props.userAdmin||!this.props.userAuthenticated) {
				Router.push('/auth/login');
			}
		}

		public componentDidUpdate() {
			if (!this.props.userAdmin||!this.props.userAuthenticated) {
				Router.push('/auth/login');
			}
		}

		public render() {
			if (!this.props.userAdmin) {
				return (
					<MyError statusCode={401} />
				);
			}

			return (
				<WrappedComponent {...this.props}/>
			);
		}
	}

	function mapStateToProps(state: IStoreState) {
		const {
			userAuthenticated,
			userData
		} = state.user;
		const role_names=userData!==null&&userData.role_names;
		const roleName=role_names&&role_names.length>0&&role_names[0];
		const userAdmin=roleName==="user"?false:true;

		return {
			userAuthenticated,
			userAdmin,
			userData,
			roleName
		};
	}

	return connect(mapStateToProps)(AdminPage);
}
