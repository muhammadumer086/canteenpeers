import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';

import { PasswordForgotForm } from '../../js/components/auth/PasswordForgotForm';
import { ApiService } from '../../js/services';

import {
	apiError,
	IStoreState,
} from '../../js/redux/store';

interface IProps {
	apiService: ApiService;
	dispatch(action: any): void;
}

interface IState {
	submitting: boolean;
	showRegister : boolean;
}

class PasswordForgot extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			submitting: false,
			showRegister : false,
		}

		this.onSubmit = this.onSubmit.bind(this);
	}

	public render() {
		return (
			<div className="auth_wrapper theme--accent_dark">
				<Head>
				<title>{process.env.SEO_TITLE}</title>
					<meta name="description" content={process.env.SEO_DESCRIPTION} />
					<meta
						name="og:title"
						property="og:title"
						content={process.env.SEO_TITLE}
					/>
					<meta
						name="og:description"
						property="og:description"
						content={process.env.SEO_DESCRIPTION}
					/>
				</Head>
				<section className="auth">
					<header className="auth-header">
						<h1 className="auth-title font--h4">
							Forgot Password
						</h1>
					</header>
					<div className="auth-content theme--main">
						{
							this.state.submitting &&
							<LinearProgress color="secondary" className="auth-content-progress" />
						}
						<PasswordForgotForm
							onSubmit={this.onSubmit}
							showRegister={this.state.showRegister}
							submitting={this.state.submitting}
						/>
						<div className="auth-content-footer font--small">
							<Link href="/auth/login"><a>Login</a></Link>
						</div>
					</div>
					<footer className="auth-footer font--small">
						Back to <Link href="/"><a>{ process.env.APP_NAME }</a></Link>
					</footer>
				</section>
			</div>
		);
	}

	protected onSubmit(values) {
		this.setState({
			submitting: true,
			showRegister : false,
		}, () => {
			this.props.apiService.queryPOST('/api/users/password-forgot', values)
			.then((res) => {
				if(res.success)
				{
				// Success
				Router.push('/auth/password-forgot-success');
				}
				else{
					this.setState({
						submitting: false,
						showRegister:true,
					});
				}
				
				
			})
			.catch((error) => {
				this.setState({
					submitting: false,
				});
				// dispatch the error message
				this.props.dispatch(apiError([
					error.message
				]));
			});
		})
	}
}

function mapStateToProps(_state: IStoreState) {
	return {};
}

export default connect(mapStateToProps)(PasswordForgot);
