<?php

namespace App\Http\Middleware;

use App\User;

use Closure;

class CheckAgeConversation {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (
			auth()
				->user()
				->isUnderage() &&
			!auth()
				->user()
				->hasConversations()
		) {
			return response()->json(
				[
					'error' => 'You need to be above 16.',
				],
				403
			);
		}

		return $next($request);
	}
}