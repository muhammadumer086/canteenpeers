<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Illuminate\Support\Facades\Auth;

class Link extends JsonResource {
	use \App\Traits\AuthUser;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request) {
		$apiStore = \App\Helpers\ApiResourceStoreHelper::Instance();

		if (
			$apiStore->hasApiResource(
				'link',
				$this->id,
				$this->updated_at,
				true,
				true
			)
		) {
			return $apiStore->getApiResource(
				'link',
				$this->id,
				$this->updated_at,
				true,
				true
			);
		}

		$params = ['id', 'title', 'subtitle'];

		$result = [];

		foreach ($params as $key) {
			$result[$key] = $this->$key;
		}

		if ($this->image) {
			$result['feature_image'] = [
				'dimensions' => [
					'width' => $this->image->width,
					'height' => $this->image->height,
				],
				'alt' => null,
				'copyright' => null,
				'url' => $this->image->image_url,
			];
		} else {
			$result['feature_image'] = null;
		}

		if ($this->url) {
			$result['url'] = $this->url;
		} else {
			$result['url'] = null;
		}

		$apiStore->setApiResource(
			$result,
			'link',
			$this->id,
			$this->updated_at,
			true,
			true
		);

		return $result;
	}
}
