<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableLocationForEvents extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('events', function (Blueprint $table) {
			$table->string('latitude')->nullable()->change();
			$table->string('longitude')->nullable()->change();
			$table->string('location')->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('events', function (Blueprint $table) {
			$table->string('latitude')->change();
			$table->string('longitude')->change();
			$table->string('location')->change();
		});
	}
}
