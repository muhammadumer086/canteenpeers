import React from 'react';

interface IProps {}

const WelcomeModalChat: React.StatelessComponent<IProps> = () => {
	return (
		<div className="modal_introduction-item">
			<div className="modal_introduction-item_image modal_introduction-item_image--mobile modal_introduction-item_image--step_three" />

			<div className="modal_introduction-item_content theme--main">
				<h4 className="font--h4 theme-title">
					Extra support when you need it
				</h4>
				<p>
					If cancer’s messing with your life, our specialist
					counsellors are here to help you cope. And just listen.
				</p>
			</div>

			<div className="modal_introduction-item_image modal_introduction-item_image--step_three" />
		</div>
	);
};

export default WelcomeModalChat;
