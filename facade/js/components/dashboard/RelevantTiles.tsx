import React from 'react';
import ContentTile from './ContentTile';
import {
	IRelevantContent,
	IContentTileData,
	IApiResource,
	IApiBlog,
} from '../../interfaces';

interface IProps {
	linkPrefix: string;
	type?: 'blog' | 'resource';
	content: IRelevantContent[] | IApiResource[] | IApiBlog[];
}

const RelevantTiles: React.FC<IProps> = (props: IProps) => {
	const itemsLimit = 3;
	/** Use the empty content to do a simple map in the component */
	const emptyContent: string[] = Array(itemsLimit).fill('');

	return (
		<React.Fragment>
			{emptyContent.map((_data, index) => {
				if (props.content.length > index) {
					const contentSingle = props.content[index];
					const contentTileData: IContentTileData = {
						type: props.type,
						imageUrl: contentSingle.feature_image
							? contentSingle.feature_image.url
							: null,
						id: contentSingle.id,
						title: contentSingle.title,
						topic: contentSingle.topic ? contentSingle.topic : null,
						user: contentSingle.user ? contentSingle.user : null,
						source: contentSingle.source,
						linkPrefix: props.linkPrefix,
						slug: contentSingle.slug,
					};

					return (
						<ContentTile
							key={index}
							hasAction={true}
							contentTileData={contentTileData}
						/>
					);
				} else {
					return <div key={index} />;
				}
			})}
		</React.Fragment>
	);
};

export default RelevantTiles;
