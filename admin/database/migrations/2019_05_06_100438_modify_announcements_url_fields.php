<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAnnouncementsUrlFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('url_label');

            $table->string('url_href')->nullable();
            $table->string('url_href_as')->nullable();
            $table->string('url_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->dropColumn('url_href');
            $table->dropColumn('url_href_as');
            $table->dropColumn('url_type');

            $table->string('url')->nullable();
            $table->string('url_label')->nullable();
        });
    }
}
