import React from 'react';
import { Checkbox, FormControlLabel } from '@material-ui/core';

interface IProps {
	inputProps: any;
}

const researchSlug = 'allow-contaced-research';

const WelcomeModalResearch: React.StatelessComponent<IProps> = props => {
	const personalSettingIndex = props.inputProps.values.personal_settings.indexOf(
		researchSlug,
	);
	// Get the personal setting for research
	const hasResearchEnabled = personalSettingIndex >= 0;

	return (
		<div className="modal_introduction-item">
			<div className="modal_introduction-item_image modal_introduction-item_image--mobile modal_introduction-item_image--step_four" />

			<div className="modal_introduction-item_content theme--main">
				<h4 className="font--h4 theme-title">Support our research</h4>
				<p>
					Are you happy to share your views and experiences to help
					Canteen provide the best possible support for young people
					affected by cancer?
				</p>
				<div className="modal_introduction-checkbox form">
					<FormControlLabel
						className="form-input-checkbox"
						control={
							<Checkbox
								className="form-input-checkbox-tick"
								onChange={handleChange}
								checked={hasResearchEnabled}
								color="primary"
							/>
						}
						label="Yes, I'd like to help"
					/>
				</div>
			</div>

			<div className="modal_introduction-item_image modal_introduction-item_image--step_four" />
		</div>
	);

	function handleChange() {
		let updatedSettings: string[] = JSON.parse(
			JSON.stringify(props.inputProps.values.personal_settings),
		);
		if (hasResearchEnabled) {
			updatedSettings.splice(personalSettingIndex, 1);
		} else {
			updatedSettings.push(researchSlug);
		}
		props.inputProps.setFieldValue('personal_settings', updatedSettings);
	}
};

export default WelcomeModalResearch;
