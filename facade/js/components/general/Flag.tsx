import React from 'react';

interface IProps {}

const Flag: React.StatelessComponent<IProps> = props => {
	return (
		<div className="flag">
			<span>{props.children}</span>
		</div>
	);
};

export default Flag;
