<?php

namespace App;

use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Model;

class DiscussionImage extends Model {
	use \BinaryCabin\LaravelUUID\Traits\HasUUID;

	protected $uuidFieldName = 'id';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */
	public $incrementing = false;

	protected $fillable = ['id'];

	/**
	 * The accessors to append to the model's array form.
	 *
	 * @var array
	 */
	protected $appends = ['image_url'];

	/**
	 * The users that created this image
	 */
	public function user() {
		return $this->belongsTo('App\User');
	}

	/**
	 * The discussion this image is attached to
	 */
	public function discussion() {
		return $this->belongsTo('App\Discussion');
	}

	public function getImageUrlAttribute() {
		return Storage::disk('s3')->url($this->path);
	}
}
