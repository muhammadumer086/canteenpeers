import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import { TextFieldFormik } from '../inputs';

import { validationsUser } from '../../helpers/validations';

interface IProps {
	intro?: string;
	submitLabel?: string;
	submitting: boolean;
	email?: string;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	email: string;
	password: string;
}

export const PasswordResetForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		email: validationsUser.email,
		password: validationsUser.password,
	});

	return (
		<div>
			<Formik
				initialValues={{
					email: props.email ? props.email : '',
					password: '',
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form
							className="auth_form form"
							onSubmit={handleSubmit}
						>
							{!!props.intro && (
								<div className="form-row">
									<div className="form-column">
										{props.intro}
									</div>
								</div>
							)}
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="email"
										label="Email"
										name="email"
										disabled={props.submitting}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="password"
										label="Password"
										name="password"
										disabled={props.submitting}
										autoComplete="new-password"
										helperText="Password must be at least 8 characters, must contain a number and a special character (ex: @,!,#)"
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="contained"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || props.submitting}
									>
										{!!props.submitLabel
											? props.submitLabel
											: 'Reset Password'}
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
};
