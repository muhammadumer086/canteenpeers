<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Discussion;

class AssignThreadIdInDiscussion extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'canteen:assignThreadIdInDiscussion';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Temporary command to re-assign thread id for discussions';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		\App\Discussion::disableSearchSyncing();

		$this->line('');
		$this->info('Re-assigning thread ids for discussions');

		$this->line('Clearing the thread ids');
		$bar = $this->output->createProgressBar(Discussion::all()->count());
		// Remove all the thread id
		$bar->start();
		$bar->setRedrawFrequency(10);
		Discussion::chunk(50, function ($discussions) use ($bar) {
			foreach ($discussions as $singleDiscussion) {
				$singleDiscussion->thread_id = null;
				$singleDiscussion->save(['timestamps' => false]);
				$bar->advance();
			}
		});

		$bar->finish();
		$this->line("\r\nUpdating first level replies");

		$bar = $this->output->createProgressBar(
			Discussion::whereRaw('main_discussion_id = reply_to_id')->count()
		);
		$bar->start();
		$bar->setRedrawFrequency(10);

		// Get all the first level discussions
		Discussion::whereRaw('main_discussion_id = reply_to_id')->chunk(
			10,
			function ($discussions) use ($bar) {
				foreach ($discussions as $singleDiscussion) {
					$singleDiscussion->thread_id =
						$singleDiscussion->main_discussion_id;
					$singleDiscussion->save(['timestamps' => false]);
					$bar->advance();
				}
			}
		);

		$bar->finish();

		$discussionsRepliesQuery = Discussion::whereNull(
			'thread_id'
		)->whereNotNull('reply_to_id');

		$discussionsCount = $discussionsRepliesQuery->count();

		if ($discussionsCount) {
			$this->line("\r\nUpdating replies of replies");
			$bar = $this->output->createProgressBar($discussionsCount);
			$bar->start();
			$bar->setRedrawFrequency(10);

			// Get all the replies of replies
			Discussion::whereIn(
				'id',
				$discussionsRepliesQuery->pluck('id')
			)->chunk(10, function ($discussions) use ($bar) {
				foreach ($discussions as $singleDiscussion) {
					$parent = Discussion::where(
						'id',
						$singleDiscussion->reply_to_id
					)->firstOrFail();

					if ($parent->thread_id === null) {
						$singleDiscussion->thread_id = $parent->id;
					} elseif (
						$parent->reply_to_id === $parent->main_discussion_id
					) {
						$singleDiscussion->thread_id = $parent->id;
					} else {
						$singleDiscussion->thread_id = $parent->thread_id;
					}

					$singleDiscussion->save(['timestamps' => false]);
					$bar->advance();
				}
			});

			$bar->finish();
		} else {
			$this->line("\r\nNo replies of replies to update");
		}

		// Set the last_activity_at based on the most recent comment
		$this->line('');
		$this->info('Updating the last activity at');
		$mainDiscussionQuery = Discussion::whereNull('main_discussion_id');
		$bar = $this->output->createProgressBar($mainDiscussionQuery->count());
		$bar->start();
		$mainDiscussionQuery->chunk(10, function ($discussions) use ($bar) {
			foreach ($discussions as $singleDiscussion) {
				// get the most recent reply
				$mostRecentReply = Discussion::where(
					'main_discussion_id',
					$singleDiscussion->id
				)
					->orderBy('created_at', 'DESC')
					->first();
				if ($mostRecentReply) {
					$singleDiscussion->last_activity_at =
						$mostRecentReply->created_at;
				} else {
					$singleDiscussion->last_activity_at =
						$singleDiscussion->created_at;
				}
				$singleDiscussion->save();

				$bar->advance();
			}
		});
		$bar->finish();

		$this->info("\r\nFinished re-assigning thread ids for discussions");
		$this->line('');
	}
}
