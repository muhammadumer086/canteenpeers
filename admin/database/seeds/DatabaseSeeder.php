<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		// $this->call(LanguagesTableSeeder::class);
		// $this->call(StatesTableSeeder::class);
		// $this->call(AgeRangeTableSeeder::class);
		// $this->call(LinksTableSeeder::class);
		// $this->call(CountriesTableSeeder::class);
		// $this->call(PermissionTableSeeder::class);
		// $this->call(PersonalSettingsTableSeeder::class);
		// $this->call(SituationsTableSeeder::class);
		// $this->call(TopicsTableSeeder::class);
		// $this->call(UsersTableSeeder::class);
		// $this->call(HeardAboutsSeeder::class);
		// $this->call(AuthApplicationTableSeeder::class);
		 $this->call(PermissionsTableSeeder::class);
	}
}
