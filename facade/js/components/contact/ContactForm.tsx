import React from 'react';
import { connect } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';

import { IStoreState } from '../../redux/store';

import { TextFieldFormik, SelectFieldFormik } from '../inputs';

import { validationMessage } from '../../helpers/validations';
import { IApiUser, ISelectOption, EContactModalTypes } from '../../interfaces';

interface IProps {
	userData: IApiUser | null;
	submitting: boolean;
	contactModalType: EContactModalTypes;
	onSubmit(values: IFormValues);
}

interface IFormValues {
	email: string;
	type: string;
	message: string;
}

export const ContactForm: React.FC<IProps> = (props: IProps) => {
	const validationFormat = Yup.object().shape({
		email: validationMessage.email,
		type: validationMessage.type,
		message: validationMessage.message,
	});

	const typeOptions: ISelectOption[] = [
		// 	{
		// 	label: EContactModalTypes.GENERAL,
		// 	value: EContactModalTypes.GENERAL,
		// },
		// {
		// 	label: EContactModalTypes.SUGGEST_TOPIC,
		// 	value: EContactModalTypes.SUGGEST_TOPIC,
		// },
		{
			label: EContactModalTypes.SUPPORT_FOR_YOUNG_PEOPLE,
			value: EContactModalTypes.SUPPORT_FOR_YOUNG_PEOPLE,
		},
		{
			label: EContactModalTypes.ABOUT_CANTEEN_CONNECT,
			value: EContactModalTypes.ABOUT_CANTEEN_CONNECT,
		},
		{
			label: EContactModalTypes.DONATIONS_OR_OTHER_ENQUIRIES,
			value: EContactModalTypes.DONATIONS_OR_OTHER_ENQUIRIES,
		},
		{
			label: EContactModalTypes.I_AM_FROM_AOTEAROA_NEW_ZEALAND,
			value: EContactModalTypes.I_AM_FROM_AOTEAROA_NEW_ZEALAND,
		},
	];

	return (
		<div>
			<Formik
				initialValues={{
					email:
						props.userData && 'email' in props.userData
							? props.userData.email
							: '',
					type: props.contactModalType,
					message: '',
				}}
				validationSchema={validationFormat}
				onSubmit={props.onSubmit}
				render={({
					values,
					touched,
					errors,
					handleChange,
					handleBlur,
					handleSubmit,
					setFieldValue,
					setFieldTouched,
					isValid,
				}) => {
					const inputProps = {
						values,
						touched,
						errors,
						handleChange,
						handleBlur,
						setFieldValue,
						setFieldTouched,
					};

					return (
						<form
							className="auth_form form"
							onSubmit={handleSubmit}
						>
							{(!props.userData ||
								!('email' in props.userData)) && (
								<div className="form-row">
									<div className="form-column">
										<TextFieldFormik
											type="email"
											label="Email"
											name="email"
											disabled={props.submitting}
											{...inputProps}
										/>
									</div>
								</div>
							)}
							<div className="form-row">
								<div className="form-column">
									<SelectFieldFormik
										label="Enquiry Type"
										name="type"
										options={typeOptions}
										values={inputProps.values.type}
										disabled={props.submitting}
										noEmpty={true}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									<TextFieldFormik
										type="text"
										label="Message"
										name="message"
										disabled={props.submitting}
										multiline={true}
										{...inputProps}
									/>
								</div>
							</div>
							<div className="form-row">
								<div className="form-column">
									Fill in the form or call us on 1800 226 833
								</div>
							</div>
							<div className="form-row form-row--submit">
								<div className="form-column">
									<Button
										variant="outlined"
										color="primary"
										className="form-submit"
										type="submit"
										disabled={!isValid || props.submitting}
									>
										Get in Touch
									</Button>
								</div>
							</div>
						</form>
					);
				}}
			/>
		</div>
	);
};

function mapStateToProps(state: IStoreState) {
	const { userData } = state.user;

	const { contactModalType } = state.contactModal;
	return {
		userData,
		contactModalType,
	};
}

export default connect(mapStateToProps)(ContactForm);
