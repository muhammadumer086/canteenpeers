@extends('layouts.email')

@section('plain_text')
	@if (isset($user->username))
		Hi {{$user->username}},
	@else
		Hi,
	@endif

	Canteen posted a new announcement <b>{{$announcement->title}}</b>
@endsection

@section('content')
	@component('emails.components.paragraph')
		@if (isset($user->username))
			Hi {{$user->username}},
		@else
			Hi,
		@endif
	@endcomponent

	@component('emails.components.paragraph')
		Canteen posted a new announcement
	@endcomponent

	@component('emails.components.paragraph')
		<b>{{$announcement->title}}</b>
	@endcomponent

	@component('emails.components.raw-text')
		{!!$announcement->message !!}
	@endcomponent

	@if ($announcement->url_href)
		@php
		$url = $announcement->url_href;
		if (strpos($url, 'http') === false) {
			$url = config('canteen.facade_url') . $url;
		}
		@endphp
		@component('emails.components.button', ['url' => $url])
			@if ($announcement->url_label)
				{{$announcement->url_label}}
			@else
				Read
			@endif
		@endcomponent
	@endif

	@component('emails.components.paragraph')
		Best wishes,
	@endcomponent
@endsection
