import React from 'react';
import UserPanelReportUserFormik, {
	IUserPanelReportUserFormikValues,
} from './UserPanelReportUserFormik';
import { EUserPanelState } from './UserPanelGeneral';

interface IProps {
	submitting: boolean;
	handleTabChange(tab: EUserPanelState): void;
	onSubmit(values: IUserPanelReportUserFormikValues): void;
}

const UserPanelReportUser: React.StatelessComponent<IProps> = props => {
	return (
		<div className="user_panel-tab user_panel-tab--report theme--main">
			<div className="user_panel-content-section user_panel-content-section--report">
				<div className="user_panel-content-section-inner">
					<div className="hm-t24 hm-b40">
						<h2 className="user_panel-report-heading font--h5 theme-title">
							Report user to Canteen
						</h2>
					</div>
					<div className="user_panel-report-form">
						<UserPanelReportUserFormik
							submitting={props.submitting}
							handleTabChange={props.handleTabChange}
							onSubmit={props.onSubmit}
						/>
					</div>
				</div>
			</div>
		</div>
	);
};

export default UserPanelReportUser;
