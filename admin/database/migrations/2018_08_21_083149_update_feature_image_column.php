<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFeatureImageColumn extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->text('feature_image')->nullable()->change();
		});

		Schema::table('resources', function (Blueprint $table) {
			$table->text('feature_image')->nullable();
		});

		Schema::create('resource_situation_topic', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('resource_id');
			$table->unsignedInteger('situation_topic_id');

			$table->foreign('resource_id')
			->references('id')->on('resources')
			->onDelete('cascade');

			$table->foreign('situation_topic_id')
			->references('id')->on('situation_topic')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('blogs', function (Blueprint $table) {
			$table->string('feature_image')->nullable()->change();
		});

		Schema::table('resources', function (Blueprint $table) {
			$table->dropColumn('feature_image');
		});

		Schema::dropIfExists('resource_situation_topic');
	}
}
