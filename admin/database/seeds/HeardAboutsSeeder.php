<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use App\HeardAbout;

class HeardAboutsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		HeardAbout::truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		$heardAbouts = [
			'From my child',
			'My partner',
			'My friend',
			'Extended family member',
			'Other young person impacted by cancer',
			'School staff',
			'Hospital staff',
			'Oncologist',
			'Nurse',
			'Counsellor/therapist',
			'GP',
			'Online search',
			'Facebook',
			'Other social media',
			'Advertising – radio',
			'Advertising – TV',
			'Advertising – online',
			'Advertising - newspaper',
			'Canteen event',
			'Canteen staff member',
			'Canteen newsletter',
			'Other newsletter',
			'Flyer',
			'From another cancer support organisation',
			'Other',
		];

		foreach ($heardAbouts as $heardAbout) {
			if (!HeardAbout::where('name', '=', $heardAbout)->exists()) {
				HeardAbout::create([
					'slug' => str_slug($heardAbout, '-'),
					'name' => $heardAbout,
				]);
			}
		}
	}
}
