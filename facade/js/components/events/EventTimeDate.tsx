import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import { IApiDate } from './../../../js/interfaces';

import { formatEventDate } from '../../helpers/formatEventDate';

interface IProps {
	dateStart?: IApiDate;
	dateEnd?: IApiDate;
}

const EventTimeDate: React.StatelessComponent<IProps> = props => {
	const startTime = moment(props.dateStart.date).format('hh:mm a');
	const endTime = moment(props.dateEnd.date).format('hh:mm a');
	// const startDate = moment(props.dateStart.date).format('dddd, D MMM YYYY');
	// const endDate = moment(props.dateEnd.date).format('dddd, D MMM YYYY');
	// const formatWithTime = 'dddd, D MMM YYYY [from] hh:mm a';
	// const formatWithoutTime = 'dddd, D MMM YYYY';
	// const start = moment(props.dateStart.date).format(
	// 	startTime === '00:00' ? formatWithoutTime : formatWithTime,
	// );
	// const end = moment(props.dateEnd.date).format(
	// 	endTime === '00:00' ? formatWithoutTime : formatWithTime,
	// );
	const formattedDate = formatEventDate(
		props.dateStart.date,
		props.dateEnd.date,
	);
	return (
		<div className="event_single-time_date">
			<div className="event_single-icon">
				<ReactSVG
					className="event_single-sidebar-icon"
					path={`${process.env.STATIC_PATH}/icons/time.svg`}
				/>
			</div>

			<div className="event_single-time_details">
				<p className="font--body theme-title event_single-time_details_text">
					{formattedDate}
				</p>
				<p className="font--body theme-title event_single-time_details_text">
					{startTime} {' - '} {endTime}
				</p>
				{/* {start === end ? (
					<span className="font--body theme-title event_single-time_details_text">
						{start}
					</span>
				) : startDate === endDate ? (
					<span className="font--body theme-title event_single-time_details_text">
						{start}
						<br />
						to {moment(props.dateEnd.date).format('hh:mm a')}
					</span>
				) : (
					<Fragment>
						<span className="font--body theme-title event_single-time_details_text">
							{start}
						</span>
						<span className="font--body theme-title event_single-time_details_text">
							{end}
						</span>
					</Fragment>
				)} */}
			</div>
		</div>
	);
};

export default EventTimeDate;
