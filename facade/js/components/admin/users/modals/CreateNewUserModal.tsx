import React from 'react';
import { Modal } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

import CreateNewUserFormik from '../forms/CreateNewUserFormik';

import { IApiSituation,ISelectOption } from '../../../../interfaces';

interface IProps {
	isActive: boolean;
	isSubmitting: boolean;
	situations: IApiSituation[];
	roles:ISelectOption[];
	handleClose(): void;
	onSubmit(values: any): void;
}

interface IState {
	createRoleType: ECreateRole;
}

enum ECreateRole {
	ADMIN,
	USER,
}

class CreateNewUserModal extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			createRoleType: ECreateRole.USER,
		};
	}

	public render() {
		let options = [];

		if (this.props.situations && this.props.situations.length) {
			options = this.props.situations
				.filter(situation => {
					return situation.parent_situation === null;
				})
				.map(situation => {
					return {
						value: situation.slug,
						label: situation.name,
					};
				});
		}

		return (
			<Modal
				aria-labelledby="create-user-modal"
				open={this.props.isActive}
				onClose={this.props.handleClose}
			>
				<Paper className="modal modal--create_staff theme--main">
					<button
						onClick={this.props.handleClose}
						className="close_button close_button--primary"
						title="Dismiss"
					>
						<ClearIcon className="close_button-icon" />
					</button>

					<div className="modal-content_container">
						<div className="modal-content create_staff_modal">
							<h2 className="modal-content_title font--h4 theme-title hm-t16">
								Create a new user
							</h2>

							<CreateNewUserFormik
								submitting={this.props.isSubmitting}
								situations={options}
								roles={this.props.roles}
								onSubmit={this.props.onSubmit}
							/>
						</div>
					</div>
				</Paper>
			</Modal>
		);
	}
}

export default CreateNewUserModal;
