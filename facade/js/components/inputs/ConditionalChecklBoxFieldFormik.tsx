import React from 'react';
import { FormControlLabel, Checkbox } from '@material-ui/core';

interface IProps {
	label: string;
	name: string;
	isChecked: boolean;
	conditionAction(name: string): void;
}

const ConditionalCheckBoxFieldFormik: React.StatelessComponent<
	IProps
> = props => {
	const handleChange = () => {
		props.conditionAction(props.name);
	};

	return (
		<React.Fragment>
			<FormControlLabel
				control={
					<Checkbox
						name="condition"
						checked={props.isChecked}
						onChange={handleChange}
						className="conditional-checkbox"
					/>
				}
				label={props.label}
			/>
		</React.Fragment>
	);
};

export default ConditionalCheckBoxFieldFormik;
